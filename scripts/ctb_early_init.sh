#!/bin/bash

# Put here any early initializations that don't require
# network to be connected

source /sn/util.sh

# This is the Laird fix for LWB5 Wi-Fi driver's SDIO bus timeout problem
echo "99000000" > /sys/kernel/debug/mmc0/clock

# GPIOs:
# GPIO1_0 (Input: SW1: AP/Station Switch)
# GPIO1_1 (Input: Factory Reset Button)
# GPIO1_4 (Output: Blue LED)
# GPIO1_5 (Output: Orange LED)
for n in 0 1; do
    cd /sys/class/gpio
    echo $n > export
    cd gpio${n}
    echo in > direction
done

for n in 4 5; do
    cd /sys/class/gpio
    echo $n > export
    cd gpio${n}
    echo out > direction
    echo 1 > value
done

## lets make /var/log a real directory instead of a symlink into /volatile to
## preserve log files across reboots

link=$(readlink "/var/log")
snlink=$(readlink "/sn")

# where does /sn point to
if [[ $snlink == *"p0"* ]]
then
	snlink="p0"
elif [[ $snlink == *"p1"* ]]
then
	snlink="p1"
fi


## now change the link for /var/log
if [[ $link != *"data"* ]]
then
	mkdir -p /data/$snlink/log

	cd /var/log

	for i in *
	do
		mv $i /data/$snlink/log
	done

	cd /var
	rm -rf log
	ln -s /data/$snlink/log log
fi



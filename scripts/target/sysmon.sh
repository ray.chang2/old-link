#!/bin/bash

#
# Usage: net_check [wifi|eth]
#
net_check() {
    if [[ $# -ne 1 || ( $1 != "wifi") && ($1 != "eth" ) ]]; then
        return 1
    fi

    local dev="$1"
    local last_status
    local cur_status
    local status_file

    if [[ $dev == 'wifi' ]]; then
        status_file="/var/run/sn/wifi-status"
        interface="wlan0"
        devname="Wi-Fi"
    else
        status_file="/var/run/sn/eth-status"
        interface="eth1"
        devname="Ethernet"
    fi

    if [[ ! -d /var/run/sn ]]; then
        mkdir -p /var/run/sn
    fi

    if [[ -f $status_file ]]; then
        last_status=$(cat $status_file)
        last_status=${last_status:0:1}    # only use the first char
    else
        last_status=0
        echo "Creating $status_file"
        echo $last_status > $status_file
    fi
    if (ifconfig $interface|grep 'inet addr') &>/dev/null; then
        cur_status=1
    else
        cur_status=0
    fi
    if [[ ! $cur_status -eq $last_status ]]; then
        echo "$devname status changed from $last_status to $cur_status"
        echo $cur_status > $status_file
    fi
}

while true; do
    net_check eth
    net_check wifi
    sleep 3
done

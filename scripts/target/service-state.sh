#!/bin/bash

#
# Usage: service_state <service_name> <true/false>
#

is_enabled=$(systemctl is-enabled $1)

if [[ $2 == "true" ]]; then
    if [[ $is_enabled = "disabled" ]]; then
        systemctl enable $1 &> /dev/null
        systemctl start $1 &> /dev/null
    fi    
else
    if [[ $is_enabled = "enabled" ]]; then
        systemctl stop $1 &> /dev/null
        systemctl disable $1 &> /dev/null
    fi
fi

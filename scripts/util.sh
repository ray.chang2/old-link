#!/bin/bash

#####################################################################
#   Utility Shell Functions
#   Copyright 2018, Smith & Nephew
#####################################################################

if [[ -d /sys/fsl_otp ]]; then
    # We are running on bridge
    imx_id=$(cat /sys/fsl_otp/HW_OCOTP_CFG2)    # like 0xee68b418
    hw_signature=${imx_id:6:4}  # like b418
    assigned_hostname=bridge-$hw_signature
    export imx_id
    export hw_signature
    export assigned_hostname
fi

##########################################
#    Log functions
##########################################
function timestamp() {
    date +"%Y-%m-%d %H:%M:%S"
}

##########################################
#    Color functions
##########################################

red="\033[0;31m"
green="\033[0;32m"
yellow="\033[0;33m"
gray="\033[1;30m"
reset="\033[0m"

# Example:
#     _general_echo $green "ERROR" arg1 arg2 $reset
function _general_echo() {
    declare -a args=("$@")
    declare -a body
    argc=${#args[@]}

    color=${args[0]}
    prefix=${args[1]}
    body=${args[*]:2:$((argc-3))}
    postfix=${args[$argc-1]}

    echo -e ${color}${body}${postfix}

    if [[ -e "$LOG" ]]; then
        body="$(timestamp) - $body"
        str="$prefix $body"
        echo -e $str >> $LOG
        sync
    fi
}

function success_echo {
    _general_echo $green '[SUCCESS]' "$@" $reset
}

function error_echo() {
    _general_echo $red '[ERROR]' "$@" $reset
}

function panic() {
    _general_echo $red '[FATAL]' "$@" $reset
    exit 1
}

function warning_echo() {
    _general_echo $yellow '[WARNING]' "$@" $reset
}

function info_echo() {
    _general_echo $reset '[INFO]' "$@" $reset
}

function debug_echo() {
    if [[ $DEBUG -eq 1 ]]; then
        _general_echo $gray '[DEBUG]' "$@" $reset
    fi
}

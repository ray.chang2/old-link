#!/bin/bash -e
#
###############################################################################
#
# Build script for Connected Tower SD Card image
#
# Copyright [2018] Smith-Nephew Inc.
#
###############################################################################

set -e

SELF_DIR="${BASH_SOURCE%/*}"
source $SELF_DIR/util.sh

export DEBUG=1
yocto_build_dir_name=bld-xwayland
do_minor_steps=1
cfg_full_build=1

function usage() {
    prog=$(basename $0)
    echo """
Usage:
    $prog <build_dir_name> <platform_dir> <rootfs_dir> [start_step] [end_step] [0/1]

Options:
    build_dir_name:  the build directory that's going to be created or re-used
    platform_dir: the directory under which \"build_dir_name\" will be located
    rootfs_dir: the target rootfs directory for installing artifacts
    start_step/end_step: used to pick a slice of the step sequence

Examples:
    $prog imx6ulevk_build . ../rootfs
    $prog imx6ulevk_build . ../rootfs 2   # do step 2 and all further steps
    $prog imx6ulevk_build . ../rootfs 2 2 # only do step 2
    $prog imx6ulevk_build . ../rootfs 2 5 # only do step 2-5
    $prog imx6ulevk_build . ../rootfs 2 5 1 # only major steps
"""
}

###############################################################################

if ! [[ $# -eq 3 || $# -eq 4 || $# -eq 5 || $# -eq 6 ]]; then
    usage "$@"
    exit 1
fi

if [[ -d "$1" ]]; then
    warning_echo "$1 already exists. Re-using it"
else
    mkdir $1
fi

if [[ $# -eq 6 && $6 -eq 1 ]]; then
    warning_echo "Skipping minor steps"
    do_minor_steps=0
fi

build_dir_name=$1
platform_dir=$2
rootfs_dir=$3
start_step=1
end_step=10

if [[ $# -ge 4 ]]; then
    start_step=$4
fi
if [[ $# -ge 5 ]]; then
    end_step=$5
fi

# settings for driver build
yocto_build_dir=$platform_dir/$build_dir_name/$yocto_build_dir_name
base_sd="$yocto_build_dir/tmp/deploy/images/imx6ulevk/core-image-base-imx6ulevk.sdcard.bz2"
klib_dir="$yocto_build_dir/tmp/\
work/imx6ulevk-poky-linux-gnueabi/linux-imx/4.9.88-r0"
export ARCH="arm"
export CROSS_COMPILE="$yocto_build_dir/tmp/work/imx6ulevk-poky-linux-gnueabi/\
core-image-base/1.0-r0/recipe-sysroot-native/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-"
export KLIB_BUILD="$klib_dir/build"
laird_driver_dir=$platform_dir/kernel-modules/laird_backport-930-0075-3.5.5.18

LOG="$platform_dir/$build_dir_name/build.log"

echo "Log file: $LOG"
echo "ARCH: $ARCH"
echo "CROSS_COMPILE: $CROSS_COMPILE"
echo "KLIB_BUILD: $KLIB_BUILD"

# Set up log file before doing anything else
rm -f "$LOG" && touch "$LOG"

success_echo "Build started at $(date).\n"
info_echo "All artifacts are in ./$build_dir_name."
cd "$build_dir_name"

#
# idempotent
# Need network access
# Minor step
#
step=1
[[ $do_minor_steps -eq 1 ]] && [[ $start_step -le $step ]] && [[ $end_step -ge $step ]] && {
    success_echo "Step $step: Installing the Freescale BSP and Its Dependencies"
    if [[ -d .repo/ ]] ; then
        warning_echo "repo already exists. Assuming it's already inited"
    else
        #
        # This just download a manifest xml file, which contains pointers to
        # all other repos. The original branch of imx-4.1.15-1.0.0_ga that's
        # referred in Laird's docs has issues with Ethernet. Changing it to a
        # later one.
        #
        # cmd="repo init -u git://git.freescale.com/imx/fsl-arm-yocto-bsp.git \
        #   -b imx-4.1-krogoth"
        #
        repo init -u https://source.codeaurora.org/external/imx/imx-manifest \
            -b imx-linux-rocko -m imx-4.9.88-2.0.0_ga.xml
        # ignore repo init errors
        repo sync
    fi
}

#
# idempotent
#
step=2
[[ $start_step -le $step ]] && [[ $end_step -ge $step ]] && {
    success_echo "Step $step: Setting up i.mx6ul building environment"

    EULA=1 DISTRO=fsl-imx-xwayland MACHINE=imx6ulevk \
        source fsl-setup-release.sh -b "$yocto_build_dir_name"

    # Now we are in $yocto_build_dir

    if grep zeromq conf/local.conf >/dev/null; then
        warning_echo "repo already configured with additional packages"
    else
        {
            # SD Size:
            echo ''
            echo '# CT Bridge SD Card Configurations:'
            echo 'IMAGE_ROOTFS_SIZE = "3145728"'
            echo 'IMAGE_OVERHEAD_FACTOR  = "1.0"'
        } >> conf/local.conf

        {
            echo 'PACKAGECONFIG_append_pn-php = " apache2"'
        } >> conf/local.conf

        {
            echo ''
            echo '# Basic Packages for CT Bridge:'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "bash"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "bash-completion"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "coreutils"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "less"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "openssh"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "procps"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "tar"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "tree"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "tzdata-americas"'
            echo 'CORE_IMAGE_EXTRA_INSTALL += "tzdata-asia"'
        } >> conf/local.conf

        if [[ $cfg_full_build -eq 1 ]]; then
            {
                echo '# Additional Packages for CT Bridge:'
				echo 'CORE_IMAGE_EXTRA_INSTALL += "u-boot-fw-utils"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "packagegroup-core-buildessential"'
                echo 'EXTRA_IMAGE_FEATURES += "package-management"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "apache2"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "curl"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "gcc"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "gdb"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "git"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "i2c-tools"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "lrzsz"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "make"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "man"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "ncurses"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "ncurses-tools"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "netcat"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "nodejs"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "nodejs-npm"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "ntp"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "python3-pip"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "python3-pymongo"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "python-pip"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "rsync"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "screen"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "sqlite3"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "vim"'
                echo 'CORE_IMAGE_EXTRA_INSTALL += "zeromq"'
            } >> conf/local.conf

            sed -i '/gnome/d' conf/bblayers.conf
            echo 'BBLAYERS += " ${BSPDIR}/sources/meta-openembedded/meta-webserver "' \
                >> conf/bblayers.conf
        fi
    fi
}

#
# idempotent
#
step=3
[[ $start_step -le $step ]] && [[ $end_step -ge $step ]] && {
    src_kernel_config=$platform_dir/kernel-patches/imx_v7_defconfig
    dst_kernel_config=$yocto_build_dir/tmp/work-shared/imx6ulevk/kernel-source/arch/arm/configs/imx_v7_defconfig
    src_linux_dts=$platform_dir/kernel-patches/bridge.dts
    dst_linux_dts=$platform_dir/build-yocto/bld-xwayland/tmp/work-shared/imx6ulevk/kernel-source/arch/arm/boot/dts/imx6ul-14x14-evk.dts
    src_uboot_dts=$platform_dir/kernel-patches/uboot.dts
    dst_uboot_dts=$platform_dir/build-yocto/bld-xwayland/tmp/work/imx6ulevk-poky-linux-gnueabi/u-boot-imx/2017.03-r0/git/arch/arm/dts/imx6ul-14x14-evk.dts

    cd $yocto_build_dir

    if [[ -f $dst_kernel_config ]] && \
        diff $src_kernel_config $dst_kernel_config && \
        diff $src_linux_dts $dst_linux_dts && \
        diff $src_uboot_dts $dst_uboot_dts ; then
        warning_echo "Skipping Step $step. Assuming no kernel changes"
    else
        success_echo "Step $step-a: Building Yocto image (core-image-base)"
        bitbake core-image-base

        success_echo "Step $step-b: Remove cfg_80211 from kernel to avoid LWB5 conflict"
        cp $src_kernel_config $dst_kernel_config
        bitbake linux-imx -f -c copy_defconfig linux-imx

        success_echo "Step $step-c: Modify the kernel GPIO & Regulator configurations"
        cd "$platform_dir"

        # Linux Kernel DTS fix
        cp $src_linux_dts $dst_linux_dts
        # cp kernel-patches/evk_lwb5.dts build-yocto/bld-xwayland/tmp/work-shared/imx6ulevk/kernel-source/arch/arm/boot/dts/imx6ul-14x14-evk.dts

        # U-Boot DTS fix to avoid Orange LED On(this process has only been partially tested)
        cp $src_uboot_dts $dst_uboot_dts

        cd $yocto_build_dir

        bitbake u-boot-imx -f -c compile

        # bitbake linux-imx -f -c deploy  # this, from LSR doc, doesn't work alone
        bitbake linux-imx -f -c compile
        bitbake linux-imx -f -c deploy  # invoke DTS compiler

        bitbake core-image-base
    fi
}

#
# idempotent
#
step=5  # can be automated
[[ $start_step -le $step ]] && [[ $end_step -ge $step ]] && {
    success_echo "Step $step: Build LWB5 Wi-Fi Driver"
    cd $laird_driver_dir
    info_echo "Configuring for FCC compilance..."
    make defconfig-lwb-fcc
    info_echo "Building the LWB5 Wi-Fi Driver..."
    make
}

#
# idempotent
# Minor step
#
step=6
[[ $do_minor_steps -eq 1 ]] && [[ $start_step -le $step ]] && [[ $end_step -ge $step ]] && {
    success_echo "Step $step: Building DNF package index & SDK"
    cd $yocto_build_dir
    bitbake package-index
    bitbake core-image-base -c populate_sdk
}

#
# idempotent
#
step=7  # can be automated
[[ $start_step -le $step ]] && [[ $end_step -ge $step ]] && {
    success_echo "Step $step: Copy the LWB5 driver ko's to target root file system"
    cd $laird_driver_dir
    ko_list="""
        ./drivers/net/wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko
        ./drivers/net/wireless/broadcom/brcm80211/brcmutil/brcmutil.ko
        ./net/wireless/cfg80211.ko
        ./compat/compat.ko
        """
    uname_r=$(ls $platform_dir/$build_dir_name/$yocto_build_dir_name/tmp/work/imx6ulevk-poky-linux-gnueabi/core-image-base/1.0-r0/rootfs/lib/modules)
    copy_dir="$rootfs_dir/lib/modules/$uname_r/kernel/"
    [[ -d $copy_dir ]] || mkdir -p $copy_dir
    echo $ko_list | xargs cp -v --parents -t $copy_dir
}

#
# idempotent
#
step=8  # can be automated
[[ $start_step -le $step ]] && [[ $end_step -ge $step ]] && {
    success_echo "Step $step: Build LWB5 Bluetooth Driver"
    # brcm_bt_driver_dir=$platform_dir/kernel-modules/brcm_patchram_plus_1.1
    # cd $brcm_bt_driver_dir
    warning_echo "skip Bluetooth for now since it needs to bitbake a devshell"
}

#
# idempotent
#
step=9  # can be automated
[[ $start_step -le $step ]] && [[ $end_step -ge $step ]] && {
    success_echo "Step $step: Installing FCC Compilance Firmware"
    fcc_firmware_dir=$platform_dir/kernel-data/Sterling_LWB5_FW_FCC_480-0081-3.5.5.18
    cd $fcc_firmware_dir
    info_echo "Copying FCC firmware to target root file system ..."
    cp -v -a -t $rootfs_dir lib
}

if ! [[ -e "$base_sd" ]]; then
    warning_echo "$base_sd not found. Forcing rebuild it."
    bitbake u-boot-imx -c deploy -f
    bitbake linux-imx -c deploy -f
    bitbake core-image-base -c rootfs -f
    bitbake core-image-base -f
fi

if [[ -e "$base_sd" ]]; then
    success_echo "Build base_sd successfully. Next: build ctb_sd"
else
    error_echo "Failed to generate base SD at $base_sd"
    exit 1
fi

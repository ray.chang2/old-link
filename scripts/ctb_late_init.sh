#!/bin/bash
# Put here initializations that occur in the end of power up

source /sn/util.sh

sleep 1    # wait for ws_st or ws_ap to start if they are already configured
chmod 600 /home/root/.ssh/id_rsa

systemctl status ws_st &>/dev/null || systemctl status ws_ap &>/dev/null
wifi_running=$?
[[ $assigned_hostname == $(hostname) ]]
hostname_correct=$?
TRUE=0

if [[ $wifi_running -eq $TRUE && $hostname_correct -eq $TRUE ]]; then
    debug_echo "AP/Station already configured correctly"
    :
    info_echo "Try to set up system time"
    systemctl stop ntpd
    ntpd -gq
    systemctl start ntpd
	### ifconfig wlan0 down
else
	echo "Reconfiguring WiFi"

    if [[ $wifi_running != $TRUE ]]; then
        warning_echo "Need to config Wi-Fi"
    fi

    if [[ $hostname_correct != $TRUE ]]; then
        echo "Need to set hostname using hardware ID"
    fi

    systemctl enable ntpd
    systemctl enable bdmd 
    systemctl disable psplash-basic
    systemctl disable psplash-network
    systemctl disable psplash-quit
    systemctl disable psplash-start
    systemctl disable systemd-hostnamed
    systemctl disable netdata
    systemctl disable lighttpd
    systemctl enable apache2
    systemctl enable webserver
    systemctl enable system-check
    systemctl enable sysmon

    cd /sn || exit 1

	# need to move the database into place before we fire up gpiod
 	if [[ ! -e /sn/webserver/db/ctb.db ]]
 	then
		if [[ -e /etc/D2 ]]
		then
 			mkdir -p /sn/webserver/db
 			cp /sn/webserver/db/D2-ctb-factory.db /sn/webserver/db/ctb.db
		fi

		if [[ -e /etc/WW ]]
		then
 			mkdir -p /sn/webserver/db
 			cp /sn/webserver/db/WW-ctb-factory.db /sn/webserver/db/ctb.db
		fi

 	fi                  

echo "Starting gpiod"
    ## info_echo "gpiod will configure CTB's network role..."
    /sn/gpiod
fi

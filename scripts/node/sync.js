const defaults = require('lodash.defaults')
const fs = require('fs')
const path = require('path')
const colors = require('colors');
const ProgressBar = require('progress');
const osHomedir = require('os-homedir');
const { exec } = require('child_process');

const DEFAULT_CONFIG = path.join(__dirname, "../..", 'config.json');
const CUSTOM_CONFIG = path.join(osHomedir(), '.ctbrc');

let config = JSON.parse(fs.readFileSync(DEFAULT_CONFIG));

if (fs.existsSync(CUSTOM_CONFIG)) {
    let custom_config = {};
    try {
        custom_config = JSON.parse(fs.readFileSync(CUSTOM_CONFIG))
    } catch (ex) {
        console.log("ERROR: ".red + `parsing ${CUSTOM_CONFIG} error`);
        process.exit(1);
    }
    config = defaults(custom_config, config);
}

if (!config.sync) {
    console.log("ERROR: ".red + '"sync" section not found in config files');
}

const bar = new ProgressBar('Syncing [:bar] :percent :etas', {
    complete: '=',
    incomplete: '.',
    width: 20,
    total: config.sync.devices.length
});

if (process.argv.length != 3) {
    console.log(`Usage:  node ${process.argv[0]} <ctb_root_dir>`.red);
    return 1;
}

const ctb_root = process.argv[2];

config.sync.devices.forEach(function(item) {

    // build commands to be executed on remote device
    remote_cmd = `
echo "Last synced on \$(date)" > /sync.txt
if [[ -e /sn.bak ]]; then
    rm -rf /sn.bak
fi
if [[ -e /sn ]]; then
    cp -a /sn /sn.bak
fi
rsync -a -e "ssh -p ${config.sync.host_port}" ${config.sync.host_user}@${config.sync.host}:${config.sync.host_path} ${config.sync.device_path} && \\
for process in ${config.sync.processes_to_kill.join(" ")}; do
    pkill \\\$process || :
done
EOF
`
    cmd = `ssh ${config.sync.device_user}@${item} bash -e <<EOF ${remote_cmd}`;

    // remote-run it
    exec(cmd, (err, stdout, stderr) => {
        if (err) {
            console.error(`exec error: ${err}`.red);
            return 1;
        }
        bar.tick(1);
        if (bar.complete) {
            console.log('Sync complete'.green);
        }
    });
});

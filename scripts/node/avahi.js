const colors = require('colors');
const dnssd = require('dnssd');

function print_device_list(device_list)
{
	for (obj of device_list){
		const keys = Object.keys(obj);
        const name = keys[0];
		const ip = obj[name];
		console.log(`${name}       `.slice(0, 8).green + `\t${ip}`)
	}
    console.log('\n------------------------------------\n');
}

function remove_item(device_list, name_to_remove)
{
    l = []
	for (obj of device_list){
		var name = Object.keys(obj);
        if (name != name_to_remove) {
            l.push(obj)
        }
	}
    return l;
}


if (process.argv.length != 2) {
    const path = process.argv[1];
    const filename = path.replace(/^.*[\\\/]/, '')
    console.log(`Usage:  node ${filename}`.red);
    return 1;
}

devices = [];

// find all ifull devices
const browser = dnssd.Browser(dnssd.tcp('SSH'))
  .on('serviceUp', service => {
      devices.push({[service.name]: service.addresses[0]});
      print_device_list(devices);
  })
  .on('serviceDown', service => {
      console.log(`${service.name} is down`.red);
      devices = remove_item(devices, service.name);
      print_device_list(devices);
  })
  .start();

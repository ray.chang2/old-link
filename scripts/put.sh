#!/bin/bash

if [[ -z $REMOTEHOST ]]; then
    echo "REMOTEHOST must be defined"
    exit 1
fi

scp -P 8022 $1 peng@$REMOTEHOST:/tftpboot/$1

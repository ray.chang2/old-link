#!/usr/bin/env bats

source util.sh

@test "_general_echo should output \$1 \$3 ... " {
  run _general_echo color prefix arg1 arg2 reset
  [[ "$output" = "colorarg1 arg2reset" ]]
}

@test "_general_echo should add prefix when LOG is set" {
  LOG=/dev/null
  run _general_echo color prefix arg1 arg2 reset
  [[ "$output" =~ prefix ]]
}

@test "_general_echo should output to log when LOG is set" {
    LOG=/tmp/tmpxxx
    rm -f "$LOG"
    touch "$LOG"

    run _general_echo "" "" arg1 arg2 ""
    [[ "${lines[0]}" =~ arg1 ]]
    run _general_echo "" "" arg3 arg4 ""
    [[ `wc -l $LOG` =~ ^2 ]]
    cat $LOG
# rm -f $LOG
}

@test "_general_echo shouldn't include color in log file" {
  LOG=/tmp/tmpxxx
  run _general_echo color prefix arg1 arg2 ""
  ! [[ $(cat $LOG) =~ color ]]
  rm -f $LOG
}

# skipped unit testing for success_echo etc since:
#   1. they are very simple and easy to verify
#   2. testing color code is complicated in BATS

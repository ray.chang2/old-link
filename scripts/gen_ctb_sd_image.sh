#!/bin/bash -e

#######################################################################
# Combine the base SD image and VCS contents to produce an updated
# SD image.
#######################################################################

SELF_DIR="${BASH_SOURCE%/*}"
source $SELF_DIR/util.sh

usage() {
    prog=$(basename $0)
    echo """
Usage:
    $prog <base_sd_image> <sd_fs> <ctb_sd_image>
"""
}

if [ $# -ne 3 ]; then
    usage
    exit 1
fi

base_sd_image=$1
local_boot=$2/bootfs
local_root=$2/rootfs
ctb_sd_image=$3

if [[ -z $(ls $local_root/sn) ]] ; then
    error_echo "$2 doesn't seem like a sd_fs directory"
    exit 1
fi

boot_mnt_dir=mnt_boot
root_mnt_dir=mnt_root

cp $base_sd_image $ctb_sd_image  # make a copy

[[ -d $boot_mnt_dir ]] || mkdir -p $boot_mnt_dir
[[ -d $root_mnt_dir ]] || mkdir -p $root_mnt_dir

# umount first for previous unclean exits
while mount |grep $boot_mnt_dir; do
    umount $boot_mnt_dir
done
while mount |grep $root_mnt_dir; do
    umount $root_mnt_dir
done

# mount boot and root partitions
dev=$(losetup --find --show --partscan $ctb_sd_image)
if [[ -z $dev ]]; then
    error_echo  "Loop mounting failed. Check root previledge"
    exit 1
fi

if [[ -n $(ls $local_boot) ]]; then
    mount ${dev}p1 $boot_mnt_dir
    cp -a --no-preserve=ownership $local_boot/* $boot_mnt_dir
    umount $boot_mnt_dir
    rmdir $boot_mnt_dir
fi

if [[ -n $(ls $local_root) ]]; then
    mount ${dev}p2 $root_mnt_dir
    cp -a --no-preserve=ownership $local_root/* $root_mnt_dir
    echo $ctb_sd_image > $root_mnt_dir/buildinfo.txt
    umount $root_mnt_dir
    rmdir $root_mnt_dir
fi

losetup -d $dev

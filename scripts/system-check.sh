#!/bin/bash

source sn/version.txt 

WIFI_STATUS_FILE="/var/run/sn/wifi-status"
ETH_STATUS_FILE="/var/run/sn/eth-status"

g_wifi_status=0
g_eth_status=0


if [[ $# -eq 0 ]]; then
    g_target_file="/etc/issue"
else
    g_target_file=$1
fi

print_header() {
    cat << EOT
Bridge Bootup System Check Result:
---------------------------------------------------------
EOT
}

print_footer() {
    cat << EOT
---------------------------------------------------------
EOT
}

print_wifi() {
    if [[ $g_wifi_status -eq 1 ]]; then
        echo "WiFi:     OK"
    else
        echo "WiFi:     Failed"
    fi
}

print_eth() {
    if [[ $g_eth_status -eq 1 ]]; then
        echo "Ethernet: OK"
    else
        echo "Ethernet: Failed"
    fi
}

print_version()
{
	if [[ -e /etc/D2 ]]
	then
		echo "D2 Link SW Version: $major.$minor.$build"
		echo "D2 Link SW BitBucket Commit ID: $gitCommitId"
        	echo "D2 Link SW Build Date: $date"
		echo "D2 Link HW Version: $hardwareVersion"
	elif [[ -e /etc/WW ]]
	then
		echo "WW Link SW Version: $major.$minor.$build"
		echo "WW Link SW BitBucket Commit ID: $gitCommitId"
        	echo "WW Link SW Build Date: $date"
		echo "WW Link HW Version: $hardwareVersion"
	fi
}

print_status() {
    print_header

    print_wifi
    print_eth
	print_version

    print_footer
}

main() {
    echo "System Check Ongoing..." > "$g_target_file"
    check_interval=3
    tries=5
    while [[ $tries -ne 0 ]]; do
        if [[ -f $WIFI_STATUS_FILE ]]; then
            t=$(cat $WIFI_STATUS_FILE)    # 0 or 1 in the status file
            g_wifi_status=${t:0:1}
        fi

        if [[ -f $ETH_STATUS_FILE ]]; then
            t=$(cat $ETH_STATUS_FILE)    # 0 or 1 in the status file
            g_eth_status=${t:0:1}
        fi

        if [[ $g_wifi_status -eq 1 && $g_eth_status -eq 1 ]]; then
            # all good
            print_status | tee "$g_target_file"
            break
        fi

        let tries=$tries-1
        print_status | tee "$g_target_file"
        sleep $check_interval
    done
}

main

#!/bin/bash -e

source util.sh



systemd_network_dir=/etc/systemd/network

usage() {
    info_echo "Usage:\n"
    info_echo "Show this help:"
    info_echo "\t$1 help"
    info_echo ""
    info_echo "To set up the system:"
    info_echo "\t$1 setup {0/1} - 0-AP mode, 1-normal mode"
    info_echo ""
    info_echo "To show current settings:"
    info_echo "\t$1"
    info_echo "\t$1 show"
    info_echo ""
    info_echo "To reset settings to factory default:"
    info_echo "\t$1 reset"
}

set_eth_mac() {
    echo 0x25 > /sys/fsl_otp/HW_OCOTP_MAC1
    cat /sys/fsl_otp/HW_OCOTP_CFG0 > /sys/fsl_otp/HW_OCOTP_MAC0
}

##########################################
#    Utility Functions
##########################################
ko1=/lib/modules/$(uname -r)/kernel/compat/compat.ko
ko2=/lib/modules/$(uname -r)/kernel/net/wireless/cfg80211.ko
ko3=/lib/modules/$(uname -r)/kernel/drivers/net/wireless/broadcom/brcm80211/brcmutil/brcmutil.ko
ko4=/lib/modules/$(uname -r)/kernel/drivers/net/wireless/broadcom/brcm80211/brcmfmac/brcmfmac.ko

setup_wifi_modules() {
    if ! lsmod | grep brcmfmac >/dev/null; then
        # LWB5 module not loaded
        info_echo "Loading Wi-Fi kernel modules"
        for ko in $ko1 $ko2 $ko3 $ko4; do
            echo loading $ko
            [[ -e $ko ]] || (error_echo "$ko not found!" && return 1)
            local ko_name
            ko_name=$(basename -s .ko $ko)
            if ! lsmod | grep $ko_name &> /dev/null; then
                insmod $ko
                sleep 1      # workaround for timing problems
            fi
        done
        if lsmod | grep brcmfmac >/dev/null; then
            depmod -a
        else
            error_echo "module install failed" && return 1
        fi
    fi
}

remove_wifi_modules() {
    for ko in $ko4 $ko3 $ko2 $ko1; do
        echo Unloading $ko
        [[ -e $ko ]] || (error_echo "$ko not found!" && return 1)
        local ko_name;
        ko_name=$(basename -s .ko $ko) 
        if lsmod | grep $ko_name &> /dev/null; then
            rmmod $ko_name || :
        fi
    done
    depmod -a
}

print_var() {
    var_name=$1
    var=$(eval "echo \$$var_name")
    dscr=$(eval "echo \$${var_name}_DSCR")

    if [[ -z $dscr ]]; then
        dscr=$var_name
    fi
    if [[ -z $var ]]; then
        info_echo "$var_name not found"
    else
        info_echo "$dscr: \r\t\t\t\t\t $var"
    fi
}

clean_ctb_setup() {
    info_echo "Stopping Wi-Fi services"
    for svc in \
        ws_ap \
        ws_st \
        udhcpd \
        udhcpc \
        bdm \
        dsc-manager \
        dsc-bdm-client \
        dsc-standalone-client \
        ; do
        info_echo "Stopping service $svc ..."
        systemctl stop $svc.service &> /dev/null || :
        info_echo "Disabling service $svc ..."
        systemctl disable $svc.service &> /dev/null || :
    done
    if ifconfig wlan0 &>/dev/null; then
        info_echo "Disabling Wi-Fi interface ..."
        ifconfig wlan0 down
    fi
    rm -f $systemd_network_dir/05-wlan0.network
    cp /etc/hostname.unconfigured /etc/hostname
}

##########################################
#    Read configuration files
##########################################

if ! [[ -f ./ctb.conf.default ]]; then
    error_echo "ctb.conf.default not found"
    exit 1
else
    source ctb.conf.default
fi
if [[ -f /etc/wifi.config ]]; then
    source /etc/wifi.config
fi

##########################################
# Main setup
##########################################

if [[ $# -eq 0 ]] || [[ $1 == "show" ]]; then
    echo ""

    # print_var "ETHERNET_ONLY"
    # print_var "WIFI_ROLE"
    if [[ $WIFI_ROLE -eq $ENUM_WIFI_AP ]]; then
        success_echo "Wi-Fi AP / Dsc-Manager"
        print_var "WIFI_AP_SSID"
        print_var "WIFI_AP_PASSWORD"
    else
        success_echo "Wi-Fi Station / dsc-client"
        print_var "WIFI_ST_SSID"
        print_var "WIFI_ST_PASSWORD"
    fi

    echo ""
    exit 0
fi

WIFI_ROLE=$2

if [[ $# -eq 1 ]] && [[ $1 == "help" ]] ; then
    usage "$(basename "$0" /)"
    exit 0
fi


if [[ $1 == "reset" || $1 == "setup" ]]; then

    if [[ $# -ne 1 && $1 == "reset" ]]; then
        usage "$(basename "$0" /)"
        exit 1
    fi

    if [[ $# -ne 2 && $1 == "setup" ]]; then
        usage "$(basename "$0" /)"
        exit 1
    fi

    clean_ctb_setup
    remove_wifi_modules
    set_eth_mac

    success_echo "CTB has been reset to factory setting."

    [[ $1 == "reset" ]] && exit 0

    if [[ $# -ne 2 ]]; then
        usage "$(basename "$0" /)"
    fi

    setup_wifi_modules || panic "Setting up Wi-Fi module failed"

    systemctl enable led-control

	echo "WIFI ROLE = $WIFI_ROLE"

    if [[ "$WIFI_ROLE" -eq 0 ]]; then
        if [[ "$ETHERNET_ONLY" == "no" ]]; then
            ln -s $systemd_network_dir/05-wlan0.network.ap $systemd_network_dir/05-wlan0.network
        fi

        WIFI_AP_SSID=$(cat /etc/wifi.config | grep SSID | cut -d= -f2)
        WIFI_AP_PASSWORD=$(cat /etc/wifi.config | grep PASS | cut -d= -f2)

		rm -f /etc/network/HOST
		touch /etc/network/AP

		echo "****** LINK DEVICE IS PUT IN CONFIG MODE"
		echo "****** WIFI_AP_SSID = $WIFI_AP_SSID  $WIFI_AP_PASSWORD"

        sed -i "s/ssid=.*/ssid=\"$WIFI_AP_SSID\"/" /etc/wpa_supplicant_ap.conf
        sed -i "s/psk=.*/psk=\"$WIFI_AP_PASSWORD\"/" /etc/wpa_supplicant_ap.conf

        systemctl enable broker
        systemctl enable ws_ap
        systemctl enable udhcpd
        systemctl enable bdm
        systemctl enable gpiod
        systemctl enable upgraded
        systemctl enable dsc-manager
        systemctl enable dsc-standalone-client
        systemctl disable osd
    else
        if [[ "$ETHERNET_ONLY" == "no" ]]; then
            ln -s $systemd_network_dir/05-wlan0.network.station $systemd_network_dir/05-wlan0.network
        fi
        WIFI_ST_SSID=$(cat /etc/wifi.config | grep SSID | cut -d= -f2)
        WIFI_ST_PASSWORD=$(cat /etc/wifi.config | grep PASS | cut -d= -f2)

		echo "****** LINK DEVICE IS PUT IN NORMAL MODE"
		echo "****** WIFI_ST_SSID = $WIFI_ST_SSID  $WIFI_ST_PASSWORD"

        sed -i "s/ssid=.*/ssid=\"$WIFI_ST_SSID\"/" /etc/wpa_supplicant_station.conf
        sed -i "s/psk=.*/psk=\"$WIFI_ST_PASSWORD\"/" /etc/wpa_supplicant_station.conf

		rm -f /etc/network/AP
		touch /etc/network/HOST

        systemctl enable broker
        systemctl enable ws_st
        systemctl enable udhcpc
        systemctl enable bdm
        systemctl enable gpiod
        systemctl enable upgraded
        systemctl enable dsc-bdm-client
        systemctl disable dsc-manager   
        systemctl enable osd
    fi

    echo $assigned_hostname > /etc/hostname
    info_echo "Configuration finished. Rebooting..."
    sync
    reboot
else
    error_echo "Invalid command line"
    usage "$(basename "$0" /)"
fi

# TODO Features

## platform

### Bugs

- Soft resetting board not functioning correctly
- Connecting to a fresh AP might cause "incorrect password" error initially

### Not imeplemented

- Bluetooth (sec. 6.7, 9)
- ETSI (sec. 6.6.3, 6.8.2)

## Discovery Service

- IPC server to provide node list

## ctbutils

- syslog

## General

- remote update SD image programatically
- git submodules for dependencies
- use `ninja` instead of Makefile for CMake
- use `Unity` and `CMock` (suited for C) instead of `GoogleTest`

/**
 * @file commonMiscUtils.c
 * @brief    utilities for additional usb stuff
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 7/28/16
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   7/28/16   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <execinfo.h>
#include <assert.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#define _FILE_OFFSET_BITS 64
#include <sys/resource.h>
#include <sys/types.h>
#include <linux/types.h>

#include "commonTypes.h"
#include "commonMiscUtils.h"
#include "logger.h"

/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

static bool stop_exit = false;
void common_stop_exit(int exit_code)
{
   char exitr[32];

   // check if we have called this already //
   if (stop_exit)
      return;

   stop_exit = true;
   bzero(exitr,sizeof(exitr));

   snprintf(exitr,sizeof(exitr),"EXITING(%d)",exit_code);
   DBG("%s\n",exitr);
   YIELD_250MS;;
   sleep(1);
   _exit(-1);
}

/**
 * @fn      void reset_xxxx_arrayX(void)
 * @brief   resets buffers 4 bytes at a time
 * @note    assumes that the passed in variables are 4 byte aligned
 ****************************************************************************/
/**
 * reset simple array
 */
void reset_array(void *p, u32 size)
{
   int i, k=(size%4), l=(size/4);
   u32 *j = (u32 *)p;
  
   for (i = 0 ; i < l ; i++)
      j[i] = 0;

   // if there are bytes that aren't reset - reset them there...
   if (k)
   {
      u8 *b = (u8 *)&j[i];
      bzero(b,k);
   }
}

/**
 * @fn      void segfault_handler(int sig) 
 * @brief   signal handler to handle segfault events
 * @param   [in] signal - signal that is active
 * @retval  none
 ****************************************************************************/
static void segfault_handler(int sig) 
{
   void *array[10];
   int numBack;
    
   // get void*'s for all entries on the stack
   numBack = backtrace(array, 10);
    
   // print out all the frames to stderr
   fprintf(stderr, "\n\nSegfault(%d):\n--------\n",sig);
   backtrace_symbols_fd(array, numBack, fileno(stderr));
   YIELD_250MS;
   sleep(1);
   exit(-3);
}


/**
 * @fn      void sig_handler(int signal)
 * @brief   signal handler to receive almost all signal events
 * @param   [in] signal - signal that is active
 * @retval  none
 ****************************************************************************/
static int quitting = 0;
bool isSignalTerm = false;
static void sig_handler(int signal) 
{
   switch(signal)
   {
   case SIGTERM:
      logger.highlight("SIGTERM Received (%d/%d)", isSignalTerm,quitting);
      isSignalTerm = true;
      if (quitting)
      {
         logger.critical("Got SIGTERM AND WE WERE STOPPING - exit NOW");
         YIELD_100MS;
         exit(2);
      }
      common_stop_exit(-666);
      quitting  = true;
      YIELD_250MS;
      return;

   case SIGINT:
      logger.warning("SIGINT Received (%d)",quitting);
      if (quitting)
      {
         logger.error("Got SIGINT AND WE WERE STOPPING - exit NOW");
         YIELD_100MS;
         exit(1);
      }

      common_stop_exit(-666);
      quitting  = true;
      YIELD_250MS;
      return;
      break;

   case SIGPIPE:
      //      logger.info("SigPipe - ignore");
      break;

   default:
      logger.fatal("Unexpected signal <%d>",signal);
      break;
   } // switch //

}

/**
 * @fn      void init_common_signal_handler(void)
 * @brief   signal handler to receive all interrupt signal
 ****************************************************************************/
void init_common_signal_handler(void)
{
   // install signint handler 
   struct sigaction sa;
   memset (&sa, 0, sizeof (sa));
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sa.sa_handler = &sig_handler;
   sigaction (SIGINT, &sa, NULL);

   memset (&sa, 0, sizeof (sa));
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sa.sa_handler = &sig_handler;
   sigaction (SIGTERM, &sa, NULL);

   // install sigpipe handler 
   memset (&sa, 0, sizeof (sa));
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sa.sa_handler = &sig_handler;
   sigaction (SIGPIPE, &sa, NULL);
    
   // seg fault handler 
   memset (&sa, 0, sizeof (sa));
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sa.sa_handler = &segfault_handler;
   sigaction (SIGSEGV, &sa, NULL);
}

/**
 * @fn      void convert_miliSecs(u32 ms, u32 *sec, u32 *ns); 
 * @brief   converts milisecs to secs/nanosecs 
 * @param   [in]  ms  - milisecs to convert
 * @param   [out] sec - ptr to returned seconds value
 * @param   [out] dst - ptr to returned nanosecs value
 ***************************************************************************/
void convert_miliSecs(u32 ms, u32 *sec, u32 *ns)
{
   *sec = ms / MILLISECS_PER_SEC;
   *ns  = MILLI2NANO((ms - (SECS2MILLI(*sec))));
}

/**
 * @fn      bool isFileExist(const char *fullFileName)
 * @brief   determines if video file exists 
 * @param   [in] fullFileName - path/filename to file to validate if exists
 * @retval  returns (1) if exists, otherwise false (0)
 ****************************************************************************/
bool isFileExist(const char *fullFileName)
{
   if (fullFileName[0] == 0)
      return false;

   if (strstr(fullFileName,".."))
      return false;

   if ((access(fullFileName, F_OK)))
      return false;
   return true;
}

/**
 * @fn      u32 get_file_size(char *fullFileName,bool *fileExists)
 * @brief   detects if file exists and returns size
 * @param   [in]  fullFileName - filename to crc check
 * @param   [out] fileExists   - output indicating if file exists
 * @retval  Actual Filesize
 ****************************************************************************/
u32 get_file_size(char *fullFileName, bool *fileExists)
{
   struct stat buf;
   if ((!isFileExist(fullFileName)))
   {
      *fileExists  = false;
      logger.error("File <%s> does NOT exist",fullFileName);
      return 0;
   }

   // indicate file exists
   *fileExists  = true;

   ///////////////////////////////////////////////////////////////////////////////////
   // Must open file to get size (because STAT doesn't work for CIFS mounted drives)
   ///////////////////////////////////////////////////////////////////////////////////
   int fd = -1;
   if ((fd = open(fullFileName,O_RDONLY)) <= 0)
   {
      logger.error("Get Size - Fail Open <%s> - <%s>", fullFileName, COMMON_ERR_STR);
      return 0;
   }

   if ((fstat(fd,&buf)))
   {
      logger.error("Get Size - Fail fstat (%d) of  <%s> - <%s>", fd, fullFileName, COMMON_ERR_STR);
      buf.st_size = 0;
   }

   close(fd);
   return ((u32)buf.st_size);
}

/**
 * @fn      unsigned char calculate_array_crc(u32 size, u8 *array)
 * @brief   Does a Checksum of an array of characters and returns two's complement CRC
 * 
 * @param	[in] size  - size of array
 * @param        [in] array - array to be checksumed 
 *
 * @retval 	2's complement of the checksum of the received message
 ****************************************************************************/
u8 calculate_array_crc(u32 size, u8 *array)
{
   char chksum;
   u8   accum=0;
   u32  i;

   for (i = 0 ; i < size ; i++)
      accum += array[i];
   chksum = ~((char)accum) + 1;
   return (u8)chksum;
}


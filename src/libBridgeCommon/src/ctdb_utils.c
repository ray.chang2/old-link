/* routine name : get_services_mask
 *  *
 *  * description  : fetches the services configuration information from database and packs into bit mask
 *  *
 *  * Returns      : bit mask of services; negative on database errors
 *  */

#include <sys/syslog.h>
#include "ctdb_utils.h"
#include "ctb_db_table_api.h"

//Note: setup_blob service is also known as custom surgeon profile.

int get_services_mask(eConnectedTowerEnums device_type)
{
    int discovery_mgr, remote_app_control, remote_app_display;
    int surgeon_profile, ext_triggers, auto_upgrade;
    int retval, bit_mask;
    int osd_service;
   
    
    if ((retval = getField_discovery_mgr(&discovery_mgr)) < 0) return retval;
    if ((retval = getField_remote_control_app(&remote_app_control)) < 0) return retval;
    //Connected Tower GET/SET Service(Remote display)NOTE:webserver keeps this bit always one, so ignore.
    //if ((retval = getField_remote_app_dsp(&remote_app_display)) < 0) return retval;
    if ((retval = getField_surgeon_profile(&surgeon_profile)) < 0) return retval;
    if ((retval = getField_ext_trigger(&ext_triggers)) < 0) ext_triggers = 0;
    if ((retval = getField_auto_updates(&auto_upgrade)) < 0) return retval;

    switch(device_type) {
    case eConnectedTowerResection:
        osd_service = 1;
        bit_mask = (((surgeon_profile ? 1 : 0) << eFB_Resection_Setup_BLOB) |
                    ((auto_upgrade ? 1 : 0) << eFB_Resection_Software_Update) |
                    ((ext_triggers ? 1 : 0) << eFB_Resection_Triggers) |
                    ((remote_app_control ? 1 : 0) << eFB_Resection_CTGETSET) |
                    ((discovery_mgr ? 1 : 0) << eFB_Resection_CTManager) |
                    ((osd_service ? 1 : 0) << eFB_Resection_OSD)
                   );
        break;
    case eConnectedTowerCoblation:
        osd_service = 1;
        bit_mask = (((surgeon_profile ? 1 : 0) << eFB_Coblation_Setup_BLOB) |
                    ((auto_upgrade ? 1 : 0) << eFB_Coblation_Software_Update) |
                    ((ext_triggers ? 1 : 0) << eFB_Coblation_Triggers) |
                    ((remote_app_control ? 1 : 0) << eFB_Coblation_CTGETSET) |
                    ((discovery_mgr ? 1 : 0) << eFB_Coblation_CTManager) |
                    ((osd_service ? 1 : 0) << eFB_Coblation_OSD)
                   );
        break;
    default:
            syslog(LOG_ERR, "invalid device type");
    }

    syslog(LOG_INFO, "services bit_mask constructed from db fields:[0X%0X]", bit_mask);
    return bit_mask;
}


#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "gtest/gtest.h"
#include "timer.h"

void* timer_func(void* arg) {
    if (arg) {
        int* n = (int*)arg;
        *n = 111;
    }
    return NULL;
}

class TimerTest : public ::testing::Test {
  protected:
    int test_int;
    TimerTest() {
        test_int = 0;
    }
};

TEST_F(TimerTest, create_timer) {
    unsigned long timer_id = create_timer(100, timer_func, &test_int);
    EXPECT_NE(timer_id, NULL);
    Timer* timer = (Timer*) timer_id;
    EXPECT_EQ(timer->magic, TIMER_MAGIC);
    EXPECT_NE(timer->timer_thread, NULL);
    EXPECT_EQ(timer->timeout_ms, 100);
    EXPECT_EQ(timer->active, true);
    EXPECT_EQ(timer->started, false);
    EXPECT_EQ(timer->cb, timer_func);
    EXPECT_EQ(timer->cb_arg, (void*)&test_int);
}

TEST_F(TimerTest, start_timer) {
    unsigned long timer_id = create_timer(50, timer_func, NULL);
    Timer* timer = (Timer*) timer_id;

    int ret;
    ret = start_timer(timer_id);
    EXPECT_NE(timer->debug_val, 111);
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(timer->active, true);
    EXPECT_EQ(timer->started, true);
    EXPECT_GT(timer->remaining_ms, 5);
}

TEST_F(TimerTest, cancel_timer) {
    unsigned long timer_id = create_timer(5, timer_func, &test_int);
    Timer* timer = (Timer*) timer_id;

    int ret;
#if 0
    uint32_t r1, r2;
#endif

    ret = start_timer(timer_id);
    EXPECT_EQ(ret, 0);
#if 0  // TO_ENHANCE: this test fails randomly
    r1 = timer->remaining_ms;
    usleep(2000);
    r2 = timer->remaining_ms;
    EXPECT_NE(r1, r2);
#endif

    cancel_timer(timer_id);
    EXPECT_EQ(timer->active, true);
    EXPECT_EQ(timer->started, false);
#if 0  // TO_ENHANCE: this test fails randomly
    r1 = timer->remaining_ms;
    usleep(2000);
    r2 = timer->remaining_ms;
    EXPECT_EQ(r1, r2);
#endif

    start_timer(timer_id);
    EXPECT_EQ(timer->active, true);
    EXPECT_EQ(timer->started, true);
    EXPECT_GT(timer->remaining_ms, 2);

#if 0  // TO_ENHANCE: this test fails randomly
    usleep(10*1000);
    // timer function should have been called
    EXPECT_EQ(test_int, 111);
#endif
}

TEST_F(TimerTest, delete_timer) {
    unsigned long timer_id = create_timer(2000, timer_func, NULL);
    Timer* timer = (Timer*) timer_id;

    start_timer(timer_id);
    EXPECT_EQ(timer->active, true);
    delete_timer(timer_id);
    EXPECT_EQ(timer->active, false);
}

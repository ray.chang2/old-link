/** Logging functionality
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <inttypes.h>
#include <sys/types.h>
#include <linux/types.h>
#include <sys/stat.h>

#include "logger.h"

const uint32_t MAX_ERRORS = 10000;
uint32_t g_errors = 0;

void logger_general(const char* prefix, const char* postfix,
                    const char* format, va_list args)
{
    printf("%s", prefix);
    vprintf(format, args);    
    printf("%s", postfix);
    fflush(stdout);
}

void logger_debug(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    logger_general("\033[1;30m[DEBUG] ", "\033[0m\n", format, args);
    va_end(args);
}

void logger_info(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    logger_general("[INFO] ", "\n", format, args);
    va_end(args);
}

void logger_highlight(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    logger_general("\033[0;32m[INFO] ", "\033[0m\n", format, args);
    va_end(args);
}

void logger_warning(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    logger_general("\033[0;33m[WARNING] ", "\033[0m\n", format, args);
    va_end(args);
}

void logger_error(const char* format, ...)
{
    va_list args;
    va_start(args, format);

    // we will abort execution after too many hours to avoid
    // flooding all logs with same errors
    if (++g_errors >= MAX_ERRORS) {
        fflush(NULL);
        if (logger.enable_exit) {
//            exit(1);
        }
    }

    logger_general("\033[0;31m[ERROR] ", "\033[0m\n", format, args);
    va_end(args);
}

void logger_critical(const char* format, ...) {
    va_list args;
    va_start(args, format);
    logger_general("\033[0;31m[CRITICAL] ", "\033[0m\n", format, args);
    va_end(args);
}

void logger_fatal(const char* format, ...) {
    va_list args;
    va_start(args, format);
    logger_general("\033[0;31m[FATAL] ", "\033[0m\n", format, args);
    fflush(NULL);
    if (logger.enable_exit) {
//        exit(1);
    }
    va_end(args);
}

void logger_error_internal(const char* format, ...) {
    va_list args;
    va_start(args, format);
    logger_general("\033[0;31m[ERROR_INTERNAL] ", "\033[0m\n", format, args);
    fflush(NULL);
    if (logger.enable_exit) {
//        exit(1);
    }
    va_end(args);
}

Logger logger = {
//    true,  // enable_exit
    false,  // enable_exit

    logger_debug,
    logger_info,
    logger_highlight,
    logger_warning,
    logger_error,
    logger_critical,
    logger_fatal,
    logger_error_internal,
};

/**
 * @file msgSerial.c
 * @brief Handles all S&N Serial Protocol Message processing
 * @details 
 *    Handles reception and transmission (re-assembles message - builds transmit structure)
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/15/18
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/15/18 Initial Creation.
 *
 * </pre>
 **************************************************************************************************/

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <linux/types.h>
#include <arpa/inet.h>

#include "commonTypes.h"
#include "commonMiscUtils.h"
#include "logger.h"
#include "msgCommon.h"

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions ********************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions **************************/
#define DEBUG_PRINTF
void display_blob(pBlobDataHdr pHeader, u8 *blobData, u8 count)
{
#ifdef DEBUG_PRINTF
   u16 blobSize = ntohs(pHeader->blob_data_size);
   char *name = (char *)pHeader->blob_name;
   u8 *blobEnd = &blobData[blobSize-5];   
   
   DBGF("\t---------------------------------------------\n");
   DBGF("\t...Blob Data <%p> Display[%d]...\n",blobData, count);
   DBGF("\t   Dev Type/Sub <%02X/%02X> CRC <%02X>\n",
        pHeader->deviceType, pHeader->subDeviceType, pHeader->header_crc);
   DBGF("\t   Prot<%d> H/B_Size(%d/%d)\n",
        pHeader->prot_version,
        pHeader->blob_header_size,
        blobSize);
       
   DBGF("\t   Blob Name<%s>\n",name);
  
   DBGF("\t   Blob Data Start <%p>: ",&blobData[0]);
   int i;
   for (i = 0 ; i < 4 ; i++)
   {
      DBGF("<%02X> ",blobData[i]);
   }
    
   DBGF("\n");

   DBGF("\t   blobDataEnd: ");
   for (i = 0 ; i < 4 ; i++)
   {
      DBGF("<%02X> ",blobEnd[i]);
   }
   u8 ccrc = calculate_array_crc(blobSize, blobData);

   DBGF("\n\t----------------data crc-<%02X/%02X>------------------\n",blobData[blobSize], ccrc);
#else
   logger.debug("display_blob needs DEBUG_PRINTF defined in compile");
#endif
}

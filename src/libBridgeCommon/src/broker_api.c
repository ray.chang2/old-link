#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <linux/types.h>

#include "zmq.h"
#include "broker_api.h"
//#include "D2_channels.h"
#include "logger.h"

#define	ACCESS_POINT_MAX_BYTES 128
#define	BROKER_DEBUG 1


///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

void *broker_publisher()
{
   pubsub_handle_t *handle;
   uint8_t access_point[ACCESS_POINT_MAX_BYTES];
   uint32_t opt = 1;
   bzero(access_point,sizeof(access_point));

#if defined(BROKER_DEBUG)
   printf("ENTER: broker_publisher\n");
#endif

   handle = malloc(sizeof(pubsub_handle_t));

   if(handle == NULL)
      return NULL;

#if defined(BROKER_DEBUG)
   printf("broker_publisher: calling zmq_ctx_new()\n");
#endif
   handle->context = zmq_ctx_new();
   if(handle->context == NULL)
   {
      free(handle);
      return NULL;
   }
	
   snprintf((char*) access_point, ACCESS_POINT_MAX_BYTES, "tcp://*:%d", SUBSCRIBER_ACCESS_PORT);

#if defined(BROKER_DEBUG)
   printf("Broker Publisher: access point: %s\n", (char *)(access_point));
#endif

   if((handle->socket = zmq_socket(handle->context, ZMQ_XPUB)) == 0)
   {
      zmq_ctx_destroy(handle->context);
      handle->context = NULL;
      free(handle);
      return NULL;
   }

#if defined(BROKER_DEBUG)
   printf("Broker Publisher: back from zmq_socket\n");
#endif
   if(zmq_bind(handle->socket, (const char *)access_point) != 0)
   {
      zmq_ctx_destroy(handle->context);
      zmq_close(handle->socket);
      free(handle);
      return NULL;
   }

#if defined(BROKER_DEBUG)
   printf("Broker Publisher: back from zmq_bind\n");
#endif

   if((zmq_setsockopt(handle->socket, ZMQ_XPUB_VERBOSE, &opt, sizeof(opt)))  < 0)
   {
      printf("ERROR: zmq_setsockopt(controller, ZMQ_XPUB_VERBOSE)\n");
      free(handle);
      return NULL;
   }
	
#if defined(BROKER_DEBUG)
   printf("Broker Publisher: returning\n");
#endif

   return handle;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

void *bm_publisher()
{
   pubsub_handle_t *handle;
   uint8_t	    access_point[ACCESS_POINT_MAX_BYTES];
   char	nothing2;


   handle = malloc(sizeof(pubsub_handle_t));

   if(handle == NULL)
      return NULL;

   handle->context = zmq_ctx_new();
   if(handle->context == NULL)
   {
      free(handle);
      return NULL;
   }
	
   snprintf((char*) access_point, ACCESS_POINT_MAX_BYTES, "tcp://localhost:%d", PUBLISHER_ACCESS_PORT);

#if defined(BROKER_DEBUG)
   printf("Publisher: access point: %s\n", (char *)(access_point));
#endif

   if((handle->socket = zmq_socket(handle->context, ZMQ_PUB)) == 0)
   {
      zmq_ctx_destroy(handle->context);
      handle->context = NULL;
      free(handle);
      return NULL;
   }

#if 0
   int sockopt = 2;

   //if(zmq_setsockopt(handle->socket, ZMQ_CONFLATE, &sockopt, 4) < 0)
   if(zmq_setsockopt(handle->socket, ZMQ_SNDHWM, &sockopt, 4) < 0)
   {
      printf("zmq_setsockopt: ZMQ_CONFLATE FAILED: errno: %d\n", errno);
      zmq_ctx_destroy(handle->context);
      zmq_close(handle->socket);
      free(handle);
      return NULL;
   }
#endif

   if(zmq_connect(handle->socket, (const char *)access_point) != 0)
   {
      zmq_ctx_destroy(handle->context);
      zmq_close(handle->socket);
      free(handle);
      return NULL;
   }

#if 0
   if(getenv("PUB_USLEEP"))
   {
#if defined(BROKER_DEBUG)
      printf("publisher sleeping ...\n");
#endif

      // keep this as it can be used to determine if the timing needs 
      // to change to a different value
      usleep(atoi(getenv("PUB_USLEEP")));   
   }
   else
   {
      //
      // sleep for 10ms to allow any subscription messages to arrive
      //

      usleep(1000 * 10);
   }
#endif

#if 1
   if ((bm_send(handle, (uint8_t*) BROKER_PING, &nothing2, sizeof(nothing2))) < 0)
   {
      logger.error("Failed to send ping1");
   }
   sleep(1);
   if ((bm_send(handle, (uint8_t*) BROKER_PING, &nothing2, sizeof(nothing2))) < 0)
   {
      logger.error("Failed to send ping2");
   }
#endif


   return handle;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

void *broker_subscriber()
{
   pubsub_handle_t		*handle;
   uint8_t				access_point[ACCESS_POINT_MAX_BYTES];


#if defined(BROKER_DEBUG)
   printf("ENTER broker_subscriber\n");
#endif

   handle = malloc(sizeof(pubsub_handle_t));

   if(handle == NULL)
      return NULL;

#if defined(BROKER_DEBUG)
   printf("broker_subscriber: calling zmq_ctx_new\n");
#endif
   handle->context = zmq_ctx_new();
   if(handle->context == NULL)
   {
      free(handle);
      return NULL;
   }

   snprintf((char*) access_point, ACCESS_POINT_MAX_BYTES, "tcp://*:%d", PUBLISHER_ACCESS_PORT);

#if defined(BROKER_DEBUG)
   printf("Broker Subscriber: access point: %s\n", (char *)(access_point));
#endif

   handle->socket = zmq_socket(handle->context, ZMQ_SUB);
   if(handle->socket == NULL)
   {
      zmq_ctx_destroy(handle->context);
      handle->context = NULL;
      free(handle);
      return NULL;
   }

   if(zmq_bind(handle->socket, (const char *)access_point) != 0)
   {
      zmq_ctx_destroy(handle->context);
      handle->context = NULL;
      zmq_close(handle->socket);
      free(handle);
      return NULL;
   }

   return handle;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

void *bm_subscriber()
{
   pubsub_handle_t *handle;
   uint8_t access_point[ACCESS_POINT_MAX_BYTES];
   bzero(access_point, sizeof(access_point));


   handle = malloc(sizeof(pubsub_handle_t));

   if(handle == NULL)
      return NULL;

   handle->context = zmq_ctx_new();
   if(handle->context == NULL)
   {
      free(handle);
      return NULL;
   }

   snprintf((char*) access_point, ACCESS_POINT_MAX_BYTES, 
            "tcp://localhost:%d", SUBSCRIBER_ACCESS_PORT);

#if defined(BROKER_DEBUG)
   printf("Subscriber: access point: %s\n", (char *)(access_point));
#endif

   handle->socket = zmq_socket(handle->context, ZMQ_SUB);
   if(handle->socket == NULL)
   {
      zmq_ctx_destroy(handle->context);
      handle->context = NULL;
      free(handle);
      return NULL;
   }
   if(zmq_connect(handle->socket, (const char *)access_point) != 0)
   {
      zmq_ctx_destroy(handle->context);
      handle->context = NULL;
      zmq_close(handle->socket);
      free(handle);
      return NULL;
   }

   return handle;
}


///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

int bm_subscribe(void *fd, char *id, size_t nbytes)
{
   pubsub_handle_t *handle = fd;
   printf("SUBSCRIBE: %s\n", id);
   return ((int)(zmq_setsockopt(handle->socket, ZMQ_SUBSCRIBE, id, nbytes)));
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

int32_t bm_receive(void *fd, uint8_t *filter, void **data, uint32_t *nbytes, int flags)
{
   int zflags=0, count=0;
   int n;
   pubsub_handle_t *handle = fd;

   if(flags == BROKER_NONBLOCKING)
      zflags |= ZMQ_DONTWAIT;

   memset(filter, 0, BROKER_FILTER_MAX);

   // read the 1st frame
   while(1)
   {
      if((n=zmq_recv(handle->socket, filter, (size_t) BROKER_FILTER_MAX, zflags)) < 0)
      {
         if(errno == EAGAIN)
            return 0;
      }
      else break;
   }

   if(n < 0)
      return -1;

   filter[n] = (uint8_t) 0;

   // read the number of bytes in the next frame
   while(1)
   {
      if((n=zmq_recv(handle->socket, &count, sizeof(count), 0)) == -1)
      {
         if(errno != EAGAIN)
            break;
      }
      else break;
   }

   if(n < 0)
      return -2;

   *nbytes = (uint32_t)count;
   *data= malloc((size_t)(count));
   if(*data == NULL)
      return -3;

   // read the data
   while(1)
   {
      if((n=zmq_recv(handle->socket, *data, (size_t) count, 0)) == -1)
      {
         if(errno != EAGAIN)
            break;
      }
      else break;
   }

   if(n < 0)
   {
      free(*data);
      *data = NULL;
      return -4;
   }

   return ((int32_t)(n));
}

#if 0
///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

int32_t  bm_receive(void *fd, uint8_t *filter, void *data, uint32_t nbytes, uint32_t flags)
{
   pubsub_handle_t		*handle = fd;
   uint32_t			count=0, zflags=0;
   int32_t  			n;


   if(flags == BROKER_NONBLOCKING)
      zflags |= ZMQ_DONTWAIT;

   // read the 1st frame
   while(1)
   {
      if((n=zmq_recv(handle->socket, filter, (size_t) BROKER_FILTER_MAX, zflags)) < 0)
      {
         if(errno == EAGAIN)
            return -1;
      }
      else break;
   }

   if(n < 0)
      return -1;

   // read the number of bytes in the next frame
   while(1)
   {
      if((n=zmq_recv(handle->socket, &count, sizeof(count), zflags)) == -1)
      {
         if(errno != EAGAIN)
            break;
      }
      else break;
   }

   if(n < 0)
      return -1;

   // read the data
   while(1)
   {
      if((n=zmq_recv(handle->socket, data, (size_t) count, zflags)) == -1)
      {
         if(errno != EAGAIN)
            break;
      }
      else break;
   }

   return n;
}
#endif


///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

int bm_send(void *controller, uint8_t *filter, void *data, uint32_t nbytes)
{
   pubsub_handle_t *handle = controller;
   int n;

   n = zmq_send(handle->socket, filter, strlen((char*) filter), ZMQ_SNDMORE);

   if(n < 0)
      return -1;

   n = zmq_send(handle->socket, &nbytes, sizeof(nbytes), ZMQ_SNDMORE);

   if(n < 0)
      return -2;

   n = zmq_send(handle->socket, data, nbytes, 0);

   if(n < 0)
      return -3;

   return n;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

void
bm_shutdown(void *handle)
{
   pubsub_handle_t		*phandle = handle;

   zmq_close(phandle->socket);
   zmq_ctx_destroy(phandle->context);
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:                     
//
// DESCRIPTION:         
//
//

int32_t bm_broker(bm_broker_t *pbroker)
{
   //
   // Ok, so we have publishers and subscribers. The message broker will 
   // subscribe to receive messages from a publisher. The message broker 
   // will turn around and publish any messages it gets to all subscribers. 
   // Make sense?  
   // 
   // Well then let me explain it this way. The broker subscribes to 
   // messages from a publisher. The broker then publishes those messages 
   // to some number of subscribers.  So the broker is both a subscriber 
   // and a publisher. Make sense? I sure hope so...  
   //

   pbroker->publishers = broker_subscriber();
   if(pbroker->publishers == NULL) return -1;

   pbroker->subscribers = broker_publisher();
   if(pbroker->subscribers == NULL) return -1;

   return 0;
}

/**
 * @fn      void *bm_init_sub_handle(char *subscribeID)
 * @brief   initializes handle for subscriber
 * @param   [in]  subscribeID = subscriber ID used in subscribe to
 * @retval  success pointer to the handle, error otherwise (NULL)
 **************************************************************/
void *bm_init_sub_handle(char *subscribeID)
{
   void *pHandleSub = NULL;
   
   // create a handle
   if ((pHandleSub = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return NULL;
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSub, subscribeID, strlen(subscribeID))))
   {
      logger.error("Failed to subscribe to <%s>");
      return NULL;
   }

   return pHandleSub;
}

/**
 * @fn      int bdm_subcribe_byte(void *handle, u8 *pByte)
 * @brief   receives byte from publisher
 * @param   [in]  handle = subscriber handle (already initialized)
 * @param   [out] pByte  = where to return byte to caller
 * @retval  0 == success, error otherwise
 **************************************************************/
int bm_get_sub_byte(void *handle, u8 *pByte)
{
   int status;
   u32 bytesRead;   
   u8 filter[BROKER_FILTER_MAX];
   void *pB;
   
   if ((status = bm_receive(handle, filter, (void*) &pB, &bytesRead, 0)) <= 0)
   {
      logger.error("Failed (%d) bm_receive sub_byte",status);
      return -1;
   }
   else if (bytesRead == 0)
   {
      logger.error("did not receive any data in sub_byte");
      return -2;
   }

   *pByte = *(u8 *)pB;
   free(pB);
   return 0;
}

/**
 * @fn      void bm_send_capDev_heartbeat(void *pHandle, u8 state, u32 options)
 * @brief   sends the capDev heartbeat (instead of D2/WW_Heartbeat)
 * @param   [in]  pHandle - broker publisher handle
 * @param   [in]  state   - state to send
 * @param   [in]  options - options (placeholder for future)
 * @retval  n/a
 **************************************************************/
void bm_send_capDev_heartbeat(void *pHandle, bool state, u32 options)
{
   // validate heartbeat
   if (pHandle == NULL)
   {
      logger.error("tried to publish capDev heartbeat with invalid handle");
      return;
   }
   capDev_disc_heartbeat_t capDevInfo = {options, state};
   if ((bm_send(pHandle, (u8 *)BROKER_CAPDEV_HEARTBEAT, &capDevInfo, sizeof(capDevInfo)) < 0))
   {
      logger.error("Failed to send capdev heartbeat");
   }
}

/**
 * @fn      void bm_send_capDev_connected(void *pHandle, u8 state)
 * @brief   sends the capDev heartbeat (instead of D2/WW_Heartbeat)
 * @param   [in]  pHandle - broker publisher handle
 * @param   [in]  state   - state to send
 * @retval  n/a
 **************************************************************/
void bm_send_capDev_connected(void *pHandle, bool state)
{
   // validate heartbeat
   if (pHandle == NULL)
   {
      logger.error("tried to publish capDev Connected state with invalid handle");
      return;
   }
   logger.highlight("Publishing CapDev State(%d)",state);
   if ((bm_send(pHandle, (u8 *)BROKER_CAPDEV_CONNECTED, &state, sizeof(state))) < 0)
   {
      logger.error("Failed to send D2 connected");
   }
}


/**
 * @fn      int bdm_poll(void *h, int32_t msecs)
 * @brief   is there data to be read from the socket
 * @param   [in]  handle = subscriber handle (already initialized)
 * @param   [in]  msecs = milliseconds to wait before timing out
 * @retval  0 == nothing to read, <0 for error or >0 something to read
 **************************************************************/
int32_t
bm_poll(void *h, int32_t msecs)
{
pubsub_handle_t     *handle = (pubsub_handle_t*) h;
zmq_pollitem_t      pollitem;
int32_t             status=0;


    pollitem.socket = handle->socket;
    pollitem.events = ZMQ_POLLIN;
    pollitem.revents = 0;

    status = zmq_poll(&pollitem, 1, msecs);

    return status;
}




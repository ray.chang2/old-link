/**
 * @file connUtils.c
 * @brief    utilities used for connection oriented functions
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 1/25/13
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   1/25/13   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include <sys/types.h>
#include <linux/types.h>

#include <netdb.h>
#include <sys/socket.h>
#include <sys/sendfile.h>
#include <linux/tcp.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include "commonTypes.h"
#include "logger.h"
#include "connUtils.h"

/**
 * max file sizes for videos
 */

/**
 * global/public variables
 */
#define SOCKET_BUFFER_SIZE_MESSAGE (1500)            /**< max size read/write socket   */
u32  max_socket_msg_size         = SOCKET_BUFFER_SIZE_MESSAGE;/**< general socket tx/rx size    */


/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions ********************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions **************************/

bool isConnectionValid(int fd)
{
   int error = 0;
   socklen_t len = sizeof (error);
   int retval = getsockopt (fd, SOL_SOCKET, SO_ERROR, &error, &len);
   if (retval)
   {
      logger.error("Failed (%d) getSockOpt - %s\n",retval, COMMON_ERR_STR);
      return false;
   }
   else if (error)
   {
      logger.error("Error (%d) getSockOpt - %s\n",error, COMMON_ERR_STR);
      return false;
   }

   return true;
}

/**
 * @fn      wait_network_bytes_available(int inPutFd, int size)
 * @brief   helper function to waiting for the amount of bytes requested 
 * @param   [in] inPutFd - fd to poll
 * @param   [in] size    - number bytes to receive
 * @param   [in] stopFlag - ptr to flag indicating stop
 * @retval  2 --> socket closed/tablet disconnected
 *          1 --> Stopping
 *          0 --> success
 *         -1 --> system error  
 ****************************************************************************/
int wait_network_bytes_available(int *inPutFd, int size, bool *stopFlag)
{
   int bytesAvailable = 0;
   if (*inPutFd < 0)
   {
      logger.critical("%s(%d)\n",__FUNCTION__,*inPutFd);
      return 3;
   }

   if ((ioctl(*inPutFd,FIONREAD,&bytesAvailable)) < 0)
   {
      logger.error("Failed first ioctl (fd:%d) number of bytes(%d) - %s\n",*inPutFd, size, COMMON_ERR_STR);
      return -1;
   }

   while ((bytesAvailable < size) && (*stopFlag == 0))
   {
      // sleep a little while //
      YIELD_5MS;

      if (*inPutFd < 0)
         return 2;

      if ((ioctl(*inPutFd,FIONREAD,&bytesAvailable)) < 0)
      {
         logger.error("Failed cont ioctl (fd:%d) number of bytes(%d) - %s\n",*inPutFd, size, COMMON_ERR_STR);
         return -1;
      }

   }

   if (*stopFlag)
   {
      logger.warning("Stopping Waiting for %d bytes from network and (%d) bytes available\n",size,bytesAvailable);
      return 1;
   }
   return 0;
}

/**
 * @fn      int create_tcp_server_socket(u16 port)
 * @brief   creates a tcp server socket to accept connections on
 * @param   [in] port - port to setup
 * @retval  server fd to accept conns on(IFF >0 success, otherwise error)
 */
int create_tcp_server_socket(u16 port)
{
   int server_fd = -1;
   struct sockaddr_in servaddr;
   int on = 1;
   // create a socket
   if ( (server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
   {
      logger.critical("Create TCP/IP Server socket <%s>",COMMON_ERR_STR);
      return -1;
   }

   // set socket options //
   if ((setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on))))
   {
      logger.critical("Create TCP/IP Set Socket Option <%s>",COMMON_ERR_STR);
      return -2;
   }

    
   // set linger to only 1 second 
   struct linger lin;
   lin.l_onoff = 1;
   lin.l_linger = 0;
   if ((setsockopt(server_fd, SOL_SOCKET, SO_LINGER, (const char *)&lin, sizeof(lin))))
   {
      logger.critical("Create TCP/IP Set Socket Linger Option <%s>",COMMON_ERR_STR);
      return -22;
   }

   // bind server address to port //
   memset(&servaddr, 0, sizeof(servaddr));
   servaddr.sin_family = AF_INET;
   servaddr.sin_port = htons(port);
   servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
   if ((bind(server_fd, (struct sockaddr *)&servaddr, sizeof(servaddr))))
   {
      logger.critical("Create TCP/IP Server Bind <%s>",COMMON_ERR_STR);
      return -3;
   }

   // listen on socket //
   if ((listen(server_fd, 1)))
   {
      logger.error("Create TCP/IP Server Listen <%s>",COMMON_ERR_STR);
      return -3;
   }

   // set nodelay
   if ((setsockopt(server_fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on))))
      logger.error("Create TCP/IP Server - Set Socket option NODELAY <%s>",COMMON_ERR_STR);

   // set mk
   on = 128*1024; // 128k
   if ((setsockopt(server_fd, SOL_SOCKET, SO_RCVBUF, &on, sizeof(on))))
      logger.error("Create TCP/IP Server - Set RCV Buf (%d) <%s>",on, COMMON_ERR_STR);

   return server_fd;
}

/**
 * @fn          int accept_tcp_socket_connection(int server_socket_fd)
 * @brief       accepts tcp/ip connections on created tcp server socket from clients 
 * @details     Handles the process of accepting and checking results of accept (and handle accordingly) 
 * @param	[in] server_socket_fd   - socket to accept connection upon
 * @retval 	>0 --> new connection socket, otherwise error
 * @warning     blocking call 
 ****************************************************************************/
int accept_tcp_socket_connection(int server_socket_fd)
{
   struct sockaddr_in address;
   socklen_t address_length = sizeof(address);
   int connection_fd, on = 1;

   if (server_socket_fd < 0)
      return -1;

   // reset address struct & call blocking accept() //
   reset_array(&address,address_length);
   if ((connection_fd = accept(server_socket_fd, (struct sockaddr *)&address, &address_length))  <= 0)
   {
      logger.error("Accepting TCP/IP Connection <%s>\n",COMMON_ERR_STR);
      return -2;
   }

//   logger.debug("Accepted a connection from Client %s:%u",inet_ntoa(address.sin_addr),address.sin_port);

   // don't linger 
   struct linger lin;
   lin.l_onoff = 0;
   lin.l_linger = 1;

   if ((setsockopt(connection_fd, SOL_SOCKET, SO_LINGER, (const char *)&lin, sizeof(lin))))
      logger.error("Setting TCP/IP Server Linger of new Connection <%s>",COMMON_ERR_STR);

   // set tcp_nodelay
   if ((setsockopt(connection_fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on))))
      logger.error("Accept TCP/IP Connection - Set New Socket option NODELAY <%s>",COMMON_ERR_STR);

   // set rcv/send socket data properly
   on = 128*1024;

   if ((setsockopt(connection_fd, SOL_SOCKET, SO_RCVBUF, &on, sizeof(on))))
      logger.error("Accept TCP/IP Connection - Set RCV Buf (%d) <%s>",on, COMMON_ERR_STR);

   if ((setsockopt(connection_fd, SOL_SOCKET, SO_SNDBUF, &on, sizeof(on))))
      logger.error("Accept TCP/IP Connection - Set Send Buf (%d) <%s>",on, COMMON_ERR_STR);
    
   // return with new connection fd 
   return connection_fd;
}

/**
 * @fn          int receive_socket_data_simple(int socket_fd, u8 *input_data, u32 input_size)
 * @brief       receives data from bi-directional socket   (blocking call)
 * @param	[in] socket_fd   - fd of the socket to receive data from
 * @param	[in] input_data  - where to put receive data
 * @param	[in] input_size  - maximum size of data to receive
 * @retval 	>0 --> returns number of bytes receivede
 *              =0 --> client disconnected
 *              <0 --> error
 * @warning     Blocking call
 ****************************************************************************/
int receive_socket_data_simple(int socket_fd, u8 *input_data, u32 input_size)
{
   int nbytes = 0;
   if ((nbytes = read(socket_fd, input_data, input_size)) < 0)
   {
      logger.error("Read Socket Simple (%d) Data <%d> - %s\n",socket_fd, nbytes, COMMON_ERR_STR);
   }
   return nbytes;
}

/**
 * @fn          int receive_socket_data_message(int socket_fd, u8 *input_data, u32 input_size)
 * @brief       receives data from bi-directional socket   (blocking call)
 * @param	[in] socket_fd   - fd of the socket to receive data from
 * @param	[in] input_data  - where to put receive data
 * @param	[in] input_size  - maximum size of data to receive
 * @retval 	>0 --> returns number of bytes receivede
 *              =0 --> client disconnected
 *              <0 --> error
 * @warning     Blocking call
 ****************************************************************************/
int receive_socket_data_message(int socket_fd, u8 *input_data, u32 input_size)
{
   if (input_size <= max_socket_msg_size)
      return (receive_socket_data_simple(socket_fd, input_data, input_size));

   u32 bytes2Read, bytesRead; 
   u32 bytesLeft = input_size;
   u32 dataIndex = 0;
   while(bytesLeft)
   {
      if (bytesLeft < max_socket_msg_size)
         bytes2Read = bytesLeft;
      else 
         bytes2Read = max_socket_msg_size;

      if ((bytesRead = receive_socket_data_simple(socket_fd, &input_data[dataIndex], bytes2Read)) != bytes2Read)
      {
         logger.error("Failed to Read <%d> bytes (only got <%d> bytes)\n",bytes2Read, bytesRead);
         return dataIndex;
      }
	
      dataIndex += bytesRead;
      bytesLeft -= bytesRead;
   } // end while //

   return dataIndex;
}


/**
 * @fn          int send_socket_simple(int socket_fd, u8 *output_data, u32 output_size)
 * @brief       Simple Sending of data on socket (blocking call (we hope))
 * @param	[in] socket_fd    - fd of socket to use to send data upon
 * @param	[in] output_data  - data to be sent
 * @param	[in] output_size  - size of data being transmited
 *
 * @retval 	>0  --> returns number of bytes sent
 *              <=0 --> error/disconnect
 ****************************************************************************/
u32 txSocketCnt = 0;
static int send_socket_simple(int socket_fd, u8 *output_data, u32 output_size)
{
   int nbytes;
   if (socket_fd < 0)
   {
      logger.error("%s(%d)\n",__FUNCTION__,socket_fd);
      return -1;
   }
    
   if ((nbytes = write(socket_fd, output_data, output_size)) <= 0)
   {
      if (nbytes < 0)
      {
         if (errno == EPIPE)
         {
            logger.debug("send_socket_simple(%d): EPIPE - %s",socket_fd, COMMON_ERR_STR);
         }
      }
      else
      {
         logger.debug("No Bytes Written to FD <%d> Wanted(%d) - %s\n", socket_fd, output_size, COMMON_ERR_STR);
      }
   }

   return nbytes;

}
/**
 * @fn           int send_socket_data(int socket_fd, u8 *output_data, u32 output_size)
 * @brief        Sends data on a bidirectional socket (blocking call)
 * @param	[in] socket_fd    - fd of socket to use to send data upon
 * @param	[in] output_data  - data to be sent
 * @param	[in] output_size  - size of data being transmited
 *
 * @retval 	>0  --> returns number of bytes sent
 *              <=0 --> error/disconnect
 ****************************************************************************/
int send_socket_data(int socket_fd, u8 *output_data, u32 output_size)
{
   // send simple way if nothing to do
   if (output_size <= SOCKET_BUFFER_SIZE_MESSAGE)
   {
      return (send_socket_simple(socket_fd, output_data, output_size));
   }
  
   u32 dataIndex = 0;
   u32 bytesLeft = output_size;
   while(bytesLeft)
   {
      int bytes2Write, bytesWritten; 
      bytes2Write = bytesLeft;

      if ((bytesWritten = send_socket_simple(socket_fd, &output_data[dataIndex], bytes2Write)) != bytes2Write)
      {
         logger.error("Failed to Write <%d> bytes (only got <%d> bytes)\n",bytes2Write, bytesWritten);
         if (bytesWritten < 0)
         {
            return bytesWritten;
         }

         return dataIndex;
      }

      dataIndex += bytesWritten;
      bytesLeft -= bytesWritten;
   }

   return dataIndex;
}

/**
 * @fn          int connect_tcp_socket_server(char *host, u16 port)
 * @brief       Connects to tcp socket server & validates protocol
 * @details     Handles the connect process to a server 
 * @param	[in] addr     - address of d25 
 * @param       [in] port     - port to connect to
 * @retval 	file descriptor of the socket (>0), otherwise, error
 * @warning     Blocking call
 ****************************************************************************/
int connect_tcp_socket_server(u32 addr, u16 port)
{
    struct sockaddr_in serv_addr;
    int sockfd, on = 1;

    // create socket //
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) <= 0)
    {
       logger.error("Create TCP/IP Socket <%s>",COMMON_ERR_STR);
       return -1;
    }

    // set linger to only 1 second 
    struct linger lin;
    lin.l_onoff = 1;
    lin.l_linger = 0;

    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family      = AF_INET;
    serv_addr.sin_addr.s_addr = addr;
    serv_addr.sin_port        = htons(port);

    if ((setsockopt(sockfd, SOL_SOCKET, SO_LINGER, (const char *)&lin, sizeof(lin))))
    {
       logger.error("TCP/IP Client Connect Linger error <%s>", COMMON_ERR_STR);
       common_close_socket(&sockfd);
       return -2;
    }
    
    // do actual connect (blocking)
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
    {
       logger.warning("TCP/IP Client Connect  <%s>", COMMON_ERR_STR);
       common_close_socket(&sockfd);
       return -3;
    }
    
    // set sock opt
    if ((setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char *) &on, sizeof(on))))
    {
       logger.warning("Connect TCP/IP Server - Set New Socket option NODELAY <%s>",COMMON_ERR_STR);
    }
    // setup tx/rx buffers
    on = 16*1024;
    if ((setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &on, sizeof(on))))
    {
      logger.warning("Error Set RCV Buf (%d) <%s>",on, COMMON_ERR_STR);
    }       
    if ((setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &on, sizeof(on))))
    {
       logger.error("Error Set Send Buf (%d) <%s>",on, COMMON_ERR_STR);
    }    
    // good return 
    return sockfd;
}

/**
 * @fn      void common_close_socket(int *fd)
 * @brief   Handles closing a socket and resetting 
 * @param   [in] fd - fd for data to be read in 
 ****************************************************************************/\
void common_close_socket(int *fd)
{
   if (*fd > 0)
   {
      shutdown(*fd, SHUT_RDWR);
      usleep(20);
      close(*fd);
   }
   *fd = -1;
}

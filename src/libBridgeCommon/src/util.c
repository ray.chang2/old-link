/** Discovery Service Utility Functions
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#include <assert.h>
#include <stdlib.h>

#include "util.h"
#include "logger.h"

#ifdef __arm__

void turn_captital_device_led(int led_status) {
    if (led_status == OFF) {
        logger.info("Capital Device LED turned off\n");
        system("/sn/device-led-off");
    } else {
        logger.info("Capital Device LED turned on\n");
        system("/sn/device-led-on");
    }
}

void turn_tower_network_led(int led_status) {
    if (led_status == OFF) {
        logger.info("Tower Network LED turned off\n");
        system("/sn/network-led-off");
    } else {
        logger.info("Tower Network LED turned on\n");
        system("/sn/network-led-on");
    }
}

#else
void turn_captital_device_led(UNUSED_ATTRIBUTE int led_status) {}
void turn_tower_network_led(UNUSED_ATTRIBUTE int led_status) {}
#endif

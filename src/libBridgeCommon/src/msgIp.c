/**
 * @file msgIp.c
 * @brief Handles all generic ip message processing
 * @details 
 *    Handles reception and transmission (re-assembles message - builds transmit structure)
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 * </pre>
 **************************************************************************************************/

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <assert.h>

#include <sys/types.h>
#include <linux/types.h>
#include <arpa/inet.h>

#include "commonTypes.h"
#include "logger.h"
#include "connUtils.h"
#include "msgIp.h"

/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions ********************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions **************************/

/********************/
/********************/
/** debug utilities */
/********************/
/********************/
void dump_msg_data(void *data, u32 size)
{
   u8 *p = (u8 *)data;
   logger.debug(".....CMD Data Size(%d)....",size);
   if (p == NULL)
   {
      return;
   }

   for (u32 i = 0 ; i < size ; i++)
   {
      logger.debug("[%d](%02X) ",i,p[i]);
   }
   logger.debug("........................");
}

void display_ip_msg_data(pIpMsg rxMsg)
{
   logger.debug("IPMsg: PROT<%02X> CMD<%02X> VER<%02X> SEQ<%d> LEN<%d>(%p)",
                rxMsg->protocol_id,
                rxMsg->command_id,
                rxMsg->version,
                rxMsg->cmd_sequence_number,
                rxMsg->cmd_data_len,
                rxMsg->cmd_data);
   if (rxMsg->cmd_data)
   {
      dump_msg_data(rxMsg->cmd_data, rxMsg->cmd_data_len);
   }
}
/********************/
/********************/


/**
 * @fn      void freeMsgContainerData(pIpMsg msg)
 * @brief   frees any data associated with message
 * @param   [in] msg - msg to be reset
 */
void freeIpMsgContainerData(pIpMsg msg)
{
   if (msg)
   {
      if (msg->cmd_data)
         free(msg->cmd_data);
      RESET_MSG_IP(msg);
   }
}

/**
 *
 * @fn      isExpectedProtocol(u8 rxProtocol, pExpCmds pIpCmds, u32 sizeExpCmds)
 *
 * @brief   seeks through expected protocols to make sure that it is in there
 * 
 * @param	[in]     rxProtocol  - received protocol byte
 * @param   [in]     pIpCmds     - array of expected commands
 * @param   [in]     sizeExpCmds - number of bytes in the expectd array
 * 
 * @retval true(1) if found, otherwise, false(0)
 */
bool isExpectedProtocol(u8 rxProtocol, pExpCmds pIpCmds, u32 sizeExpCmds)
{
   u32 i;
   for (i = 0 ; i < sizeExpCmds ; i++)
   {
      if (rxProtocol == pIpCmds[i].expProt)
      {
         return true;
      }
   }
   return false;
}

/**
 *
 * @fn      bool findCmdIndex(u8 rxCmd, pExpCmds pIpCmds, u32 sizeExpCmds)
 *
 * @brief   seeks through expected protocols to make sure that it is in there
 * 
 * @param	[in]     rxCmd       - received protocol byte
 * @param   [in]     pIpCmds     - array of expected commands
 * @param   [in]     sizeExpCmds - number of bytes in the expectd array
 * 
 * @retval  commandIndex if found, otherwise error (-1)
 */
int findCmdIndex(u8 rxCmd, pExpCmds pIpCmds, u32 sizeExpCmds)
{
   int i;
   for (i = 0 ; i < (int)sizeExpCmds ; i++)
   {
      if (rxCmd == pIpCmds[i].expCmd)
      {
         return i;
      }
   }
   return -1;
}

/**
 * @fn      u8 calculate_bdm_scd_msg_crc(bdm_scd_msg_container_t *Msg)
 * @brief   Does a Checksum of an array of characters and returns two's complement CRC
 * @param	[in] Msg - container object of message that needs to be CRCd
 * @retval 	2's complement of the checksum of the received message -
 * @note	        
 * @bug  	
 * @warning      
 * @pre          describe preconditions here
 ****************************************************************************/
u8 calculate_ip_msg_crc(pIpMsg Msg)
{
   u32 i;
   u8 *msg      = (u8 *)Msg;
   u8 *data     = Msg->cmd_data;
   u32 dataSize = Msg->cmd_data_len;
   u8  accum    = 0;

   for (i = 0 ; i < HEADER_SIZE_IP_MSGS ; i++)
   {
      accum += msg[i];
   }

   for (i = 0 ; i < dataSize ; i++)
   {
      accum += data[i];
   }

   char chksum = ~((char)accum) + 1;
   return (u8)chksum;
}

/**
 *
 * @fn      int read_ip_msg(int *fd, pExpCmds pIpCmds, u32 sizeExpCmds, pIpMsg rxMsg)
 *
 * @brief   reads and processes IP message 
 * 
 * @param	[in]     fd          - ptr to socket to be read from
 * @param   [in]     pIpCmds     - array of expected commands
 * @param   [in]     sizeExpCmds - number of expected commands in array
 * @param	[in/out] rxMsg       - pointer to where to put the message - pointer to array
 *
 * @retval 	Returns status of the call:
 * 
 *               >0   --> number of bytes in the output array
 *               == 0 --> undefined return;
 *               <0   --> error in construction
 *                      -1 == calloc error
 *                      -x == <tbd>
 *
 * @note	        Calling program must free the output array 
 *                  (and command_data array - if necessary)
 *
 * @warning      Free output array & manage command_data array
 ****************************************************************************/
int read_ip_msg(int *fd, pExpCmds pIpCmds, u32 sizeExpCmds, pIpMsg rxMsg)
{
   u8   calcCrc;
   bool continue_read = true;
    
   while(continue_read)
   {
      // reset inputs
      int  cmdIndex, bytes2Read;
      int  cmdDataLen, crc_offset, bytesRead;
      
      cmdDataLen   = 0;
      crc_offset   = 0;
        
      if (!(isConnectionValid(*fd)))
      {
         logger.warning("Invalid FD(%d)",*fd);
         return 1;
      }

      // read the header
      if ((bytesRead = receive_socket_data_simple(*fd, (u8 *)rxMsg, HEADER_SIZE_IP_MSGS)) != HEADER_SIZE_IP_MSGS)
      {
         if (bytesRead < 0)
         {
            return bytesRead;
         }
         else if (bytesRead == 0)
         {
            // logger.debug("IPMsg: 0 bytes read - likely disconnecting\n");
            return 2;
         }
            
         logger.warning("IPMsg: Didn't get all bytes for Header(%d) - throwing away bytes",bytesRead);
         continue;
      } // if didn't get what we want

        // validate the protocol
      if (!isExpectedProtocol(rxMsg->protocol_id, pIpCmds, sizeExpCmds))
      {
         logger.error("<%d> is NOT an expected protocol",rxMsg->protocol_id);
         display_ip_msg_data(rxMsg);
         continue;
      }

      if (rxMsg->version != DEFAULT_VERSION_BYTE)
      {
         logger.debug("Unexpected Version (%02X)\n",rxMsg->version);
         display_ip_msg_data(rxMsg);
      }        

      // calculate real data length
      cmdDataLen = ntohl(rxMsg->cmd_data_len);

      // find the command 
      if ((cmdIndex = findCmdIndex(rxMsg->command_id, pIpCmds, sizeExpCmds)) == -1)
      {
         logger.warning("Did not find command <%02X> in expected commands\n",rxMsg->command_id);
         display_ip_msg_data(rxMsg);
         continue;
      } // if failed to find - return to seeking 

        // validate command
      if (pIpCmds[cmdIndex].expCmdLen != 255)
      {
         if (cmdDataLen != pIpCmds[cmdIndex].expCmdLen)
         {
            logger.warning("Invalid data length (%d) for command (%02X) - expecting len(%d)",cmdDataLen, rxMsg->command_id, pIpCmds[cmdIndex].expCmdLen);
            display_ip_msg_data(rxMsg);
            continue;
         } // if not the right length
      } // variable length expectations

      bytes2Read = (cmdDataLen + 1); // for crc
      if ((rxMsg->cmd_data = (u8 *)calloc(bytes2Read,1)) == NULL)
      {
         logger.fatal("Failed CALLOC while creating cmd data - %s", COMMON_ERR_STR);
         return -66;
      } // failed CALLOC

        // read the bytes in
      if ((bytesRead = receive_socket_data_simple(*fd, rxMsg->cmd_data, bytes2Read)) != bytes2Read)
      {
         logger.warning("Reading cmd data (len<%d>) but only got(%d) Bytes", bytes2Read, bytesRead);
         if (bytesRead < 0)
         {
            return bytesRead;
         }
         else if (bytesRead == 0)
         {
            return 2;
         }
         // otherwise go back and read next packet 
         continue;
      } // not the expected length
        

        // get the crc and validate
      crc_offset = cmdDataLen;
      rxMsg->cmd_data_len = cmdDataLen;
      if ((calcCrc = calculate_ip_msg_crc(rxMsg)) != rxMsg->cmd_data[crc_offset])
      {
         logger.warning("Calc CRC <%02X> != CRC Sent[%d] <%02X>",calcCrc, crc_offset, rxMsg->cmd_data[crc_offset]);
         display_ip_msg_data(rxMsg);
         continue;
      }

      // make sure it is readable by the calling program and set continue off
      continue_read = false;
   } // end while

   // return here with good message
   return 0;
}

/**
 * @fn      static int build_transmit_array(pIpMsg txMsg, u8 **outPtr)
 *
 * @brief   creates array for transmission (based upon a message)
 * 
 * @param	[in]     txMsg - command to be sent 
 * @param	[out]    outPtr - pointer to array
 *
 * @retval 	Returns status of the call:
 * 
 *               >0   --> number of bytes in the output array
 *               == 0 --> undefined return;
 *               <0   --> error in construction
 *                      -1 == calloc error
 *                      -x == <tbd>
 *
 * @note	        Calling program must free the output array 
 *               (and command_data array - if necessary)
 *
 * @warning      Free output array & manage command_data array
 ****************************************************************************/
static int build_transmit_array(pIpMsg txMsg, u8 **outPtr)
{
   u32 crc_offset;
   u32 total_size, command_data_size = txMsg->cmd_data_len;
   u8 *output;

   // setup the cmd_data_len 
   txMsg->cmd_data_len = htonl(txMsg->cmd_data_len);

   // calculate a few things //
   crc_offset   = HEADER_SIZE_IP_MSGS  + command_data_size;
   total_size   = crc_offset + 1;

   // allocate output //
   if ((output = (u8 *)calloc(total_size,1)) == NULL)
   {
      logger.fatal("\n\t...calloc External output array of %d bytes - %s...\n",total_size, COMMON_ERR_STR);
      return -1;
   }
    
   // copy header to output array //
   memcpy(output,txMsg,HEADER_SIZE_IP_MSGS);

   // copy command data out to array;
   if (command_data_size)
   {
      memcpy(&output[HEADER_SIZE_IP_MSGS],txMsg->cmd_data, command_data_size);
   }
    
   // calculate CRC - set it - and return with status //
   output[crc_offset] = calculate_array_crc(crc_offset, output);
   *outPtr = output;
   return total_size;
}

/**
 * @fn      int build_send_ip_msg(int socket_fd, pIpMsg txMsg, pthread_mutex_t *lock)
 * @brief   (PUBLIC API) Sends a cmd message on socket using a specified protocolID with associated command data
 * @param	[in] socket_fd  - socket to send message over
 * @param	[in] txMsg      - message to be sent
 * @param   [in] lock       - mutex for locking 
 * @retval 	Success        == 0
 *               Internal Error  < 0
 *               External Error  > 0   
 ****************************************************************************/
int build_send_ip_msg(int socket_fd, pIpMsg txMsg, pthread_mutex_t *lock)
{
   u8 *outPtr = NULL;
   int bytes2Send, bytesSent;
   int status = 0;

   pthread_mutex_lock(lock);
   txMsg->version = 0x21;
   
//   display_ip_msg_data(txMsg);
   
   // build a byte array (note: this does byte swapping of command data length)
   if ((bytes2Send = build_transmit_array(txMsg, &outPtr)) <= 0)
   {
      status = -1;
   }
   else if ((bytesSent = send_socket_data(socket_fd, outPtr, bytes2Send)) != bytes2Send)
   {
      logger.warning("Failed to send data <%d/%d>",bytesSent,bytes2Send);
      if (bytesSent == 0)
      {
         status = 1;
      }
      else 
      {
         status = -1;
      }
   } // else error sending
   
   if (outPtr != NULL)
   {
      free(outPtr);
   }
   pthread_mutex_unlock(lock);
   return status;
}

/**
 * @fn      u8 getNextTxGetSetSeq(void)
 * @brief   returns next txgetsetseq
 * @retval  n/a
 **************************************************************/
u8 nextTxGetSetSeq     = 1;
u8 getNextTxGetSetSeq(void)
{
   u8 nextNumTx = nextTxGetSetSeq;
   if (nextTxGetSetSeq == 255)
      nextTxGetSetSeq = 1;
   else
      nextTxGetSetSeq++;
   return nextNumTx;
}

/**
 * @fn      u8 getNextTxSeqSetupBlob(void)
 * @brief   deals with next tx sequence
 * @retval  n/a
 **************************************************************/
u8 nextTxSetupBlobSeq     = 1;
u8 getNextTxSeqSetupBlob(void)
{
   u8 retValue = nextTxSetupBlobSeq;
   if (nextTxSetupBlobSeq == 255)
   {
      nextTxSetupBlobSeq = 1;
   }
   else
   {
      nextTxSetupBlobSeq++;
   }
   return (retValue);
}



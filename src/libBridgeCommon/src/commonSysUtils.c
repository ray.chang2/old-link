/**
 * @file commonSysUtils.c
 * @brief    utilities used by the Subsystem
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 3/18/15
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   3/18/15   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>
#include <sys/types.h>

#include <linux/types.h>
#include <linux/sched.h>

#include "commonTypes.h"
#include "connUtils.h"
#include "commonMiscUtils.h"
#include "commonSysUtils.h"
#include "logger.h"

/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//////////////////// Thread related functionality //////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
/**
 * @fn      void common_kill_thread(pthread_t *pThId, const char *whatThread)
 * @brief   forces the exit of threads
 * @param   [in] pThId - thread to be killed
 * @param   [in] whatThread - debug output
 ****************************************************************************/
void common_kill_thread(pthread_t thread, const char *whatThread)
{
   int status;
   logger.highlight("Canceling <%s> thread",whatThread);

   // cancel thread //
   if ((status = pthread_cancel(thread)) != 0) 
   {
      if (status != ESRCH)
         logger.error("Invalid kill of thread(%s) - <%s>",whatThread, COMMON_ERR_STR);
      else
         logger.warning("<%s> thread already has exited",whatThread);
   }

   logger.highlight("Joining <%s> thread",whatThread);
   pthread_join(thread,NULL);
   logger.highlight("Finished Kill of <%s> thread",whatThread);
} 

/**
 * @fn      int  common_create_thread(pthread_t *pTh, COMMON_THREAD_PRIORITY_LEVELS level, void *args, void *(*thread_fn)(void *))
 * @brief   creates a thread for calling program 
 * @param   [in] pTh       - pointer to threadID to use
 * @param   [in] level     - enum priority type
 * @param   [in] args      - arguments for thread
 * @param   [in] thread_fn - actual thread function
 * @retval  0 == success, otherwise, error
 ****************************************************************/
int common_create_thread(pthread_t *pTh, UNUSED_ATTRIBUTE COMMON_THREAD_PRIORITY_LEVELS level, void *args, void *(*thread_fn)(void *))
{
   ////////// If we are not on Baseboard - no priority attributes //////////
   pthread_attr_t tattr;
   pthread_attr_init(&tattr);
   pthread_attr_setstacksize(&tattr,THREAD_STACK_SIZE);
   if ((pthread_attr_setinheritsched(&tattr,PTHREAD_EXPLICIT_SCHED)))
   {
      logger.error("low setinhert init <%s>",COMMON_ERR_STR);
      return -1;
   }

   if(pthread_attr_setschedpolicy(&tattr, SCHED_FIFO) != 0)
   {
      logger.error("Failed to set fifo schedule fifo - %s", COMMON_ERR_STR);
      return -2;
   }
   
   struct sched_param schedParam;   
   switch(level)
   {
   case HIGHEST_PRIORITY:
      schedParam.sched_priority = 99; 
      break;
      
   case HIGHER_HIGH:
      schedParam.sched_priority = 93; 
      break;
      
   case MEDIUM_HIGH:
      schedParam.sched_priority = 86; 
      break;
      
   case LOW_HIGH:
      schedParam.sched_priority = 80; 
      break;

   case HIGH_MEDIUM:
      schedParam.sched_priority = 70; 
      break;
   case MEDIUM_MEDIUM:
      schedParam.sched_priority = 60; 
      break;

   case LOW_MEDIUM:
      schedParam.sched_priority = 40; 
      break;
      
   case HIGH_LOW:
      schedParam.sched_priority = 30; 
      break;
   case MEDIUM_LOW:
      schedParam.sched_priority = 20; 
      break;
   case LOWER_LOW:
      schedParam.sched_priority = 10; 
      break;
   case LOWEST_LOW:
      schedParam.sched_priority = 5; 
      break;
   default:
   case MAINT_PRIORITY:
      schedParam.sched_priority = 1; 
      break;
   }
   
   if ((pthread_attr_setschedparam(&tattr,&schedParam)))
   {
      logger.error("setschedparam <%s>",COMMON_ERR_STR);
      return -3;
   }
   
   if ((pthread_create(pTh, &tattr, thread_fn, args)))
   {
      logger.critical("pthread_create <%s>", COMMON_ERR_STR);
      return -4;
   }

   // let the thread start - if it can //
   YIELD_3MS;
   return 0;
}

void display_policy_priority(void)
{
   pthread_t thId = pthread_self();
   struct sched_param param;
   int priority;
   int policy;
   if ((pthread_getschedparam (thId, &policy, &param)))
   {
      logger.error("Failed to get params - %s",COMMON_ERR_STR);
      return;
   }
   priority = param.sched_priority; 

   logger.highlight("Policy/Priority(%d/%d)", policy, priority);
}

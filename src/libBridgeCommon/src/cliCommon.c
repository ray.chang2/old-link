#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <assert.h>
#include <poll.h>
#include <sys/types.h>
#include <linux/types.h>
#include "commonState.h"
#include "commonMiscUtils.h"
#include "logger.h"
#include "cliCommon.h"

common_state_t common_system_state;

/**
 * @fn     void flushStdIn(void)
 * @brief  flushes stdIn for cli
 * @param  n/a
 ***********************************************************************/
static void flushStdIn(void)
{
   while (1)
   {
      int c = getchar();
      if (c == '\n')
         return;
      else if (c == EOF)
         return;
   }
}


/***********
 * @fn      int wait_for_user(bool dummy_read, bool doNotDisplay)
 * @brief   waits for input
 * @param   [in] dummy_read   - read the input because we do NOT care about it
 * @param   [in] doNotDisplay - do NOT display periodic HB Status
 * @retval  0 == success, otherwise, error and stopping
 ****************************************************************************/
static int wait_for_user(bool dummy_read, bool doNotDisplay)
{
   u32 i = 0;
   struct pollfd ufds[1];      
   ufds[0].fd = 0;
   ufds[0].events = POLLIN;
     
   while(!common_system_state.stopping)
   {
      int rv;
      switch ((rv = poll(ufds,1,1000)))
      {
      case -1:
         logger.error("Pollin stdin error - %s",COMMON_ERR_STR);
         return -1;
         break;  // error

      case 0:
         break; // timeout 
	     
      default:
         if (dummy_read)
            flushStdIn();
         return 0;
         break; // got something //
      } // end switch //
	 
      // do display if appropriate //
      if (!(i++ % 20))
      {
         if (!doNotDisplay)
         {
            // reset these anyways //
            DBGF("\n--->SELECT AN OPTION> ");
               
         } // if OK to display //
      } // if time to do something or something change
   }   // end while 
     
   return -3;
}

#define ENTER2CONTINUE { DBGF("\n\t--- Hit Enter to Continue ---\n"); wait_for_user (true,false); }
/**
 * @fn      int privateGetString(const char *prompt, char *copyTo, u8 maxSize, bool doNotDisplay)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt      - prompt for the string 
 * @param   [out] copyTo      - location to copy string to
 * @param   [in/out] maxSize  - maxSize of copyTo 
 * @param   [in] doNotDisplay - display status while waiting
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
static int privateGetString(const char *prompt, char *copyTo, u8 maxSize, bool doNotDisplay)
{
   int status = 0;
   char *userInput = (char *)malloc((maxSize+1));
   char *result = NULL;
   u32 i;

tryPrivateStringAgain:

   // reset userInput
   reset_array(copyTo,maxSize);
   DBGF("\n%s\n >  ",prompt);

   if ((wait_for_user(false,doNotDisplay)))
   {
      status = -1;
      goto exit_free_now;
   }
   
   // read data in //
   if ((result = fgets(userInput, maxSize, stdin)) == NULL)
   {
      perror("fgets");
      DBGF("Failed to Read Input - Lets try that again\n");
      ENTER2CONTINUE;
      goto tryPrivateStringAgain;
   }

   // remove the '\n' and copy string over //
   char *fn = userInput;
   strsep(&fn,"\n");
   for (i = 0 ; i < strlen(userInput) ; i++)
   {
      int b = (int)userInput[i];
      if ((isalnum(b)))
         continue;
      else if ((userInput[i] == '_') ||(userInput[i] == '-'))
         continue;

      DBGF("Illegal Character [%u]<%c> in input <%s> - only Alpha Numeric Character (or '_'/'-') are accepted\n",i, b, userInput);
      ENTER2CONTINUE;
      goto tryPrivateStringAgain;
   }

   strncpy(copyTo,userInput,maxSize);
exit_free_now:
   if (userInput)
   {
      free(userInput);
   }
   return status;
}

/**
 * @fn      int getStringStatus(const char *prompt, char *copyTo, u8 maxSize)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt     - prompt for the string 
 * @param   [out] copyTo     - location to copy string to
 * @param   [in/out] maxSize - maxSize of copyTo 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
int getStringStatus(const char *prompt, char *copyTo, u8 maxSize)
{
   return (privateGetString(prompt, copyTo, maxSize, false));
}




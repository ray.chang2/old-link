/**
 * @file msgSerial.c
 * @brief Handles all S&N Serial Protocol Message processing
 * @details 
 *    Handles reception and transmission (re-assembles message - builds transmit structure)
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/15/18
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/15/18 Initial Creation.
 *
 * </pre>
 **************************************************************************************************/

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <linux/types.h>

#include <termios.h>

#include "commonTypes.h"
#include "logger.h"
#include "msgSerial.h"
#include "connUtils.h"

/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions ********************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions **************************/
// local list definition
typedef struct serial_msg_list
{
   struct serial_msg_list *next;
   struct serial_msg_list *prev;
   bdm_serial_msg_container_t msg;
   u32 timeoutCount;
   u32 retries;
} serial_msg_list_t, *pList;

// list management
static pList listHead = NULL;
static u32   numList  = 0;
static u8    libPro   = 0xFF;

/**
 * @fn      pList get_next_list_item_timeout(u32 timeout)
 * @brief   retrieves a list items based upon timeout value
 * @param   timeout - time that we are looking for....
 * @retval  pointer to item retrieved or NULL;
 **************************************************************/
static pList get_next_list_item_timeout(u32 timeout)
{
   pList pSeek = listHead;
   while ((pSeek != NULL) && (pSeek->timeoutCount != timeout))
   {
      pSeek = pSeek->next;
   }
   return pSeek;
}

/**
 * @fn      pList get_list_item(u8 requestNumber)
 * @brief   retrieves a list item based upon sequence number
 * @param   requestNumber - the sequence number of message to retrieve
 * @retval  pointer to item retrieved or NULL;
 **************************************************************/
static pList get_list_item(u8 requestNumber)
{
   assert(numList);
   pList pSeek = listHead;
   
   while (pSeek != NULL)
   {
      if (pSeek->msg.request_number == requestNumber)
      {
         return pSeek;
      }
      else if (pSeek->msg.request_number == (requestNumber | 0x80))
      {
         return pSeek;
      }
      
//      logger.highlight("GLIMiss(%02X/%02X)",pSeek->msg.request_number, requestNumber);
      pSeek = pSeek->next;
   }
   
   if (pSeek == NULL)
   {
      logger.error("get_list_item() - failed to find <%02X> in list",requestNumber);
   }
   return pSeek;
}

/**
 * @fn      void remove_item(pList item)
 * @brief   removes list item from double linked list
 * @param   item - item to be removed from list
 * @retval  n/a
 **************************************************************/
static void remove_item(pList item)
{
   if ((numList == 0) || (item == NULL))
   {
      logger.error("Invalid remove item (%d/%p)",numList,item);
   }
   
   pList up   = item->prev;
   pList down = item->next;
   numList--;
   if (numList)
   {
      if (item == listHead)
      {
         listHead = down;
         down->prev = NULL;
      }
      else
      {
         if (up)
            up->next   = down;
         if (down)
            down->prev = up;
      }
   }
   else
   {
      listHead = NULL;
   }
}

/**
 * @fn      void remove_and_free(pList item)
 * @brief   removes and frees list item from double linked list
 * @param   item - item to be removed from list
 * @retval  n/a
 **************************************************************/
static void remove_and_free(pList item)
{
   // remove item from list //
   if (item == NULL)
   {
      logger.warning("trying to free ");
   }
   remove_item(item);
   
   // free data here //
   if (item->msg.cmd_data)
   {
      assert(item->msg.cmd_data_len);
      free(item->msg.cmd_data);
   }
   free(item);
}

/**
 * @fn      void add_backup_serial_msg(pMsgSerial msg, u32 timeoutCount)
 * @brief   adds a msg to a backup list with a timeout associated with it
 * @param   msg - serial msg to be backed upo
 * @param   timeout - time that this message will timeout
 * @retval  n/a
 **************************************************************/
void add_backup_serial_msg(pMsgSerial msg, u32 timeoutCount)
{
   // create local copy of message
   assert(msg);
   
   pList local = calloc(sizeof(serial_msg_list_t),1);
   assert(local);

   // set timeout, next == NULL, and copy serial message body
   local->timeoutCount = timeoutCount;
   local->next = NULL; // set to the end
   memcpy(&local->msg, msg, sizeof(local->msg));

   // copy data over to new one  
   if (msg->cmd_data_len)
   {
      assert(msg->cmd_data);
      local->msg.cmd_data = calloc(msg->cmd_data_len,1);
      assert(local->msg.cmd_data);
      memcpy(local->msg.cmd_data, msg->cmd_data, msg->cmd_data_len);
   }

   // set it as command for sending 
   msg->request_number |= 0x80;
   
   // check if empty - if so this one at the head
   if (listHead == NULL)
   {
      assert(numList == 0);
      listHead = local;
      numList = 1;
   }
   else
   {
      // find the end and put local on the end
      assert(numList);
      pList q = listHead;
      while(q->next != NULL)
      {
         q = q->next;
         // do additional check to make sure we aren't adding twice //
         if (q)
         {
            if (q->msg.request_number == local->msg.request_number)
            {
               logger.error("attempting to add twice (%d)",local->msg.request_number);
               return;
            }
         }
      }

      // put at end, and point back at the end item
      q->next = local;
      local->prev = q;
      numList++; // probably should have a maximum      
   }
}

/**
 * @fn      int clear_backup_serial_msg(u8 requestNumber)
 * @brief   removes a backup list item/msg from list
 * @param   requestNumber - sequence number of the msg to be removed
 * @retval  status of that remove
 **************************************************************/
int clear_backup_serial_msg(u8 requestNumber)
{
   pList listItem = NULL;
   // make sure there is something on the list
   if (numList == 0)
   {
      logger.warning("attemping to remove an entry for <%02X> and none on list");
      return 0;
   }
   u8 rn = requestNumber;
   if (requestNumber & 0x80)
   {
      rn &= 0x7F;
   }
   
   if ((listItem = get_list_item(rn)) == NULL)
   {
      logger.error("Failed to clear msg <%d>",requestNumber);
      return -2;
   }
   
   remove_and_free(listItem);
   return 0;
}

/**
 * @fn      pMsgSerial get_copy_requestNumber_msg(u8 requestNumber, u32 newTimeoutCount, u32 *retryCount)
 * @brief   gets a reference to message in the list to resend and set new timeout 
 * @param   requestNumber - sequence number of the msg to be removed
 * @param   newTimeoutCount - new time for timeout
 * @param   [out] current number of times the message has been retrieved
 * @retval  references to the serial message to resend
 * @note    do not free this message after sending - it is still reference in list
 **************************************************************/
pMsgSerial get_copy_requestNumber_msg(u8 requestNumber, u32 newTimeoutCount, u32 *retryCount)
{
   pList listItem = NULL;
   if (numList == 0)
   {
      logger.warning("Get Copy <%d> but none on list",requestNumber);
      return NULL;
   }

   // 
   if (requestNumber & 0x80)
   {
      requestNumber &= 0x7F;
   }

   if ((listItem = get_list_item(requestNumber)) == NULL)
   {
      logger.warning("GetCopy() - failed to find <%02X> in list",requestNumber);
      return NULL;
   }

   listItem->retries++; // increment 
   *retryCount           = listItem->retries;
   listItem->timeoutCount = newTimeoutCount;
   return (&listItem->msg);
}

/**
 * @fn      void clear_all_backup_msgs(void)
 * @brief   clears all messages in backup list (going back into recovery?)
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void clear_all_backup_msgs(void)
{
   pList local = listHead;
   if (numList == 0)
   {
      return;
   }
   assert(listHead);
   listHead = NULL;
   while (local != NULL)
   {
      // get temp copy, and go to the next item
      pList temp = local;
      local = local->next;
      if (temp->msg.cmd_data_len)
      {
         freeMsgContainerData(&temp->msg);         
      }
      free(temp);
   }
   numList = 0;
}

/**
 * @fn      u32 get_num_msg_backed(void)
 * @brief   returns the number of messages in backup list
 * @param   n/a
 * @retval  number of messages in list
 **************************************************************/
u32 get_num_msg_backed(void)
{
   return numList;
}
   
/**
 * @fn      pMsgSerial get_next_timeout_msg(u32 currentCount, u32 newTimeoutCount, u32 *retryCount)
 * @brief   gets a reference to message in the list which has timed out and sets new timeout
 * @param   currentCount - current time to look for
 * @param   newTimeoutCount - new time for timeout
 * @param   [out] current number of times the message has been retrieved
 * @retval  references to the serial message to resend
 * @note    do not free this message after sending - it is still reference in list
 **************************************************************/
pMsgSerial get_next_timeout_msg(u32 currentCount, u32 newTimeoutCount, u32 *retryCount)
{
   pList listItem = get_next_list_item_timeout(currentCount);

   if (listItem == NULL)
   {
      return NULL;
   }
   
   listItem->timeoutCount = newTimeoutCount;
   listItem->retries++; // increment 
   *retryCount = listItem->retries;
   return (&listItem->msg);
}

/**
 * @fn      void freeMsgContainerData(pMsgSerial msg)
 * @brief   frees the msg data
 * @param   msg - message whose data is going to be freed
 * @retval  n/a
 **************************************************************/
void freeMsgContainerData(pMsgSerial msg)
{
   if (msg)
   {
      if (msg->cmd_data_len)
      {
         if (msg->cmd_data)
         {
            free(msg->cmd_data);
         }
      }
      RESET_MSG(msg);
   }
}

/**
 * @fn      void freeMsgContainerAll(pMsgSerial msg)
 * @brief   frees any data associated with message and then frees the msg container
 * @param   [in] msg - msg to be freed
 ************************************************************************/
void freeMsgContainerAll(pMsgSerial *msg)
{
   if (*msg)
   {
      freeMsgContainerData(*msg);
      free(*msg);
      *msg = NULL;
   }
}

/**
 * @fn      void display_msg(pMsgSerial msg)
 * @brief   displays a serial Message
 * @param   [in] msg - msg to be displayed
 ***********************************************************/
void display_msg(pMsgSerial msg)
{
   if (msg->cmd_data)
   {
      logger.debug("Prot <%02X> CMD<%02X> LEN<%d> Data[0]<%02X>",
                   msg->protocol_id,   msg->command_id,
                   msg->cmd_data_len, *msg->cmd_data);
   }
   else
   {
      logger.debug("Prot <%02X> CMD<%02X> LEN<%d>",
                   msg->protocol_id,   msg->command_id,
                   msg->cmd_data_len);
   }
}

/**
 * @fn      void send_nack_cmd(u8 status, u8 cmdID, u8 reqNum)
 * @brief   sends generic nack to capital device
 * @param   status - error status to send
 * @param   cmdID - command nacking
 * @param   reqNum - sequence number of that nack
 ***********************************************************/
static u8 nackStatus = 0;
static u8 nackCmd = 0;
void send_nack_cmd(u8 status, u8 cmdID, u8 reqNum)
{
   nackStatus = status;
   if (libPro == 0xFF)
   {
      logger.info("cannot send nack <%02X> - we havent' sent one command yet",cmdID);
   }
   else
   {
      capdev_generic_reply_t nack;
      bdm_serial_msg_container_t txMsg;
      u8 lrn = RESPONSE_REQUEST_NUMBER(reqNum);
      logger.highlight("Sending nack cmd <%02X>",cmdID);
      txMsg.command_id     = CAPDEV_GENERIC_REPLY;
      txMsg.protocol_id    = libPro;
      txMsg.request_number = lrn;
      txMsg.cmd_data_len   = sizeof(nack);
      txMsg.cmd_data       = (u8 *)&nack; // ACK
      nack.status          = status;
      nack.cmd_id          = cmdID;
      nack.reqNum          = lrn;
      extern pthread_mutex_t serialMsgLock;
      extern int serial_fd;
      build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
   }
}

/**
 * @fn      u8 calculate_serial_msg_crc(bdm_scd_msg_container_t *Msg)
 * @brief   Does a Checksum of an array of characters and returns two's complement CRC
 * @param	[in] Msg - container object of message that needs to be CRCd
 * @retval 	2's complement of the checksum of the received message -
 ****************************************************************************/
u8 calculate_serial_msg_crc(pMsgSerial Msg)
{
   int i;
   u8 *msg      = (u8 *)Msg;
   u8 *data     = Msg->cmd_data;
   u8  dataSize = Msg->cmd_data_len;
   u8  accum    = 0;

   for (i = 0 ; i < 4 ; i++)
      accum += msg[i];

   for (i = 0 ; i < dataSize ; i++)
      accum += data[i];

   char chksum = ~((char)accum) + 1;
   return (u8)chksum;
}


/**
 * @fn      int wait_and_read_bytes(int *inPutFd, u8 *inBytes)
 * @brief   helper function to do actual waiting/reading of bytes from socket
 * @param   [in] inPutFd - fd to receive data on received
 * @param   [in] inBytes - array to put bytes into
 * @param   [in] stopFlag - indicates application is stopping
 * @retval   3 --> socket read error (just disco)
 *           2 --> tablet disconnect
 *           1 --> stopping 
 *           0 --> success
 *          -1 --> initial ioctl  
 *          -2 --> not used
 *          -3 --> socket read failure (non fatal)
 ****************************************************************************/
u32 totalBytesRead = 0;
static int wait_and_read_bytes(int *inPutFd, u32 size, u8 *inBytes, bool *stopFlag)
{
   int nbytes,status;
   if ((status = wait_network_bytes_available(inPutFd, size, stopFlag)))
   {
      logger.error("Wait Network Bytes failed(%d)",status);
      return status;
   }

   int size2 = size;
   if ((nbytes = receive_socket_data_simple(*inPutFd, inBytes, size)) != size2)
   {
      if (nbytes == 0)
      {
         logger.error("Read '0' bytes after wait_and_readlikely disconnecting");
         return 2;
      }
      logger.error("Failed (%d)to read (%d) bytes from fd(%d)",nbytes, size, *inPutFd);
      return 3;
   }
   totalBytesRead += size;
   return 0;
}

/**
 * @fn      int seek_protocolID(int *fd, pMsgSerial rxMsg, pExpCmds expCmds, u8 numCmds, bool *stopFlag)
 * @brief   helper function to look for the next protocol ID
 * @param   fd - fd to receive data on received
 * @param   rxMsg - place to check for protocol id
 * @param   expCmds - list of expected commands
 * @param   numCmds - number of expected commands in list
 * @param   stopFlag - flag to exit if set
 * @retval   3 --> socket read error (just disco)
 *           2 --> tablet disconnect
 *           1 --> stopping 
 *           0 --> success
 *          -1 --> initial ioctl or general error
 *          -2 --> not usedi
 *          -3 --> socket read failure (non fatal)
 ****************************************************************************/
static int seek_protocolID(int *fd, pMsgSerial rxMsg, pExpCmds expCmds, u8 numCmds, bool *stopFlag)
{
   bool continue_read = true;
   while (continue_read && (*stopFlag == 0))
   {
      int  status = 0;
      if ((status = wait_and_read_bytes(fd, 1, &rxMsg->protocol_id, stopFlag)))
      {
         logger.error("Read protocol failure (%d)",status);
         return status;
      }
      for (status = 0 ; status < numCmds ; status++)
      {
         if (rxMsg->protocol_id == expCmds[status].expProt)
         {
            return 0;
         }
      } // for all possible protocols

      continue;
   } // while waiting for protocol byte

   // should never get here 
   return -1;
}

/**
 * @fn      int read_cmdID(int *fd, pMsgSerial rxMsg, pExpCmds expCmds, u8 numCmds, u8 *cmdIndex, bool *stopFlag)
 * @brief   helper function to receive and validate the command
 * @param   fd - fd to receive data on received
 * @param   rxMsg - place to check for protocol id
 * @param   expCmds - list of expected commands
 * @param   [out]cmdIndex - index where the command was found (used to validate other things)
 * @param   numCmds - number of expected commands in list
 * @param   stopFlag - flag to exit if set
 * @retval   3 --> socket read error (just disco)
 *           2 --> tablet disconnect
 *           1 --> stopping 
 *           0 --> success
 *          -1 --> initial ioctl or general error
 *          -2 --> not usedi
 *          -3 --> socket read failure (non fatal)
 *        111  --> invalid command ID - non fatal
 ****************************************************************************/
static int read_cmdID(int *fd, pMsgSerial rxMsg, pExpCmds expCmds, u8 numCmds, u8 *cmdIndex, bool *stopFlag)
{
   int i, status;
   if ((status = wait_and_read_bytes(fd, 1, &rxMsg->command_id, stopFlag)))
   {
      logger.error("Read command Status(%d)",status);
      return status;
   }
   for (i = 0 ; i < numCmds ; i++)
   {
      if (rxMsg->command_id == expCmds[i].expCmd)
      {
         *cmdIndex = i;
         return 0;
      }
   }

   return 111;
}

/**
 * @fn      int read_rest_header(int *fd, pMsgSerial rxMsg, bool *stopFlag)
 * @brief   helper function to receive and validate the command
 * @param   fd - fd to receive data on received
 * @param   rxMsg - place to check for protocol id
 * @param   stopFlag - flag to exit if set
 * @retval   3 --> socket read error (just disco)
 *           2 --> tablet disconnect
 *           1 --> stopping 
 *           0 --> success
 *          -1 --> initial ioctl or general error
 *          -2 --> not usedi
 *          -3 --> socket read failure (non fatal)
 ****************************************************************************/
static int read_rest_header(int *fd, pMsgSerial rxMsg, bool *stopFlag)
{
   int status;
   if ((status = wait_and_read_bytes(fd, READ_SIZE_REMAINING_HEADER, &rxMsg->request_number, stopFlag)))
   {
      logger.error("read rest Status(%d)",status);
      return status;
   }
   return 0;
}

/**
 * @fn      int read_serial_msg(int *fd, pMsgSerial *rxMsg, pExpCmds expCmds, u8 numCmds, bool *stopFlag)
 * @brief   public interface to block and read a serial message
 * @param   fd - fd to receive data on received
 * @param   [in/out]rxMsg - place to check for protocol id
 * @param   expCmds - list of expected commands
 * @param   numCmds - number of expected commands in list
 * @param   stopFlag - flag to exit if set
 * @retval   3 --> socket read error (just disco)
 *           2 --> tablet disconnect
 *           1 --> stopping 
 *           0 --> success
 *          -1 --> initial ioctl or general error
 *          -2 --> not usedi
 *          -3 --> socket read failure (non fatal)
 *        1234 --> error detected?
 *       -6666 --> malloc errors (and -333)
 * @note     calling routine is responsible to freeing the rxMsg returned
 ****************************************************************************/
int read_serial_msg(int *fd, pMsgSerial *rxMsg, pExpCmds expCmds, u8 numCmds, bool *stopFlag)
{
   pMsgSerial localMsg = NULL;
   u8 index = 0;
   u8 crc_offset, total2Read;
    
   // allocate rxMsg //
   if (!(*rxMsg = (pMsgSerial)calloc(sizeof(bdm_serial_msg_container_t),1)))
   {
      logger.fatal("%s: Malloc Error rxMsg: <%s>",__FUNCTION__, COMMON_ERR_STR);
      return -333;
   }
   else
   {
      localMsg = *rxMsg;
   }

   if (nackStatus)
   {
      DBG("Re-Starting seek of protocol ID after nack/cmd<%d/%d>\n",nackStatus, nackCmd);
      nackStatus = 0;
      nackCmd = 0;
   }
   localMsg->cmd_data_len = 0;
   localMsg->cmd_data = NULL;
   // start looking for protocol ID and go from there
   int status;
   while (*stopFlag == false)
   {
      if ((seek_protocolID(fd, localMsg, expCmds, numCmds, stopFlag)) == 0)
      {
         // got success 
         if ((status = read_cmdID(fd, localMsg, expCmds, numCmds, &index, stopFlag)) == 0)
         {
            if (localMsg->command_id < CAPDEV_GENERIC_REPLY)
            {
               logger.warning("WEIRD COMMAND ID got through - maybe somebody is trying to mess with me?");
               display_msg(localMsg);
               send_nack_cmd(NACK_CMD_NOT_SUPPORTED, localMsg->command_id, localMsg->request_number);
               return 113;
            }
            
            if ((status = read_rest_header(fd,localMsg,stopFlag)))
            {
               logger.warning("Failed to read rest of header");
               return status;
            }
            else
            {
               // check if expected length or variable length
               if (localMsg->cmd_data_len == expCmds[index].expCmdLen)
               {
                  goto read_command_data;
               }
                    
               if (expCmds[index].expCmdLen == (u8)(-1))
               {
                  goto read_command_data;
               }
               
               logger.warning("Got Command <%02X>(%d) - but not expected cmd data length(%d)",
                    localMsg->command_id,
                    localMsg->cmd_data_len,
                    expCmds[index].expCmdLen);

               localMsg->cmd_data_len = 0;
               localMsg->cmd_data     = NULL;
               display_msg(localMsg);
               send_nack_cmd(NACK_CMD_DATA_LEN_ERROR, localMsg->command_id, localMsg->request_number);
               return 112;
            }
         }
         else if (status == 111)
         {
            logger.warning("Mismatch prot/cmd (%02X/%02X)",localMsg->protocol_id, localMsg->command_id);
            display_msg(localMsg);
            send_nack_cmd(NACK_CMD_NOT_SUPPORTED, localMsg->command_id, 0);
            return status;
         }
         else
         {
            logger.warning("Failed Reading of command(%d)",status);
            return status;
         }
      }
   } // end while //

   // error return 
   return 1234;

read_command_data:
   total2Read = localMsg->cmd_data_len + 2; // for crc and 0xFC
   crc_offset = localMsg->cmd_data_len;

   if ((localMsg->cmd_data = (u8 *)calloc(total2Read,1)) == NULL)
   {
      logger.fatal("calloc command data (%d) failure - %s\n",total2Read, COMMON_ERR_STR);
      return (-6666);
   } // if failed
   
   // wait for data to be received
   if ((status = wait_network_bytes_available(fd,total2Read,stopFlag)))
   {
      free(localMsg->cmd_data);
      localMsg->cmd_data = NULL;
      return status;
   }

   // now read bytes in //
   int nbytes;
   if ((nbytes = receive_socket_data_message(*fd, localMsg->cmd_data, total2Read)) != total2Read)
   {
      if (nbytes == 0)
      {
         logger.error("Read data from Connection(%d) - likely disconnecting",*fd);
         status = 2;
      }
      else
      {
         logger.error("Failed (%d) to read all expected bytes(%d) from fd(%d)",nbytes, total2Read, *fd);
         status = 3;
      }      
      send_nack_cmd(NACK_GENERAL_ERROR, localMsg->command_id, localMsg->request_number);
      return status;
   } // if error in read //

   // do CRC calculation and check against - no affect on returned message
   u8 calcCrc = calculate_serial_msg_crc(localMsg);
   if (calcCrc != localMsg->cmd_data[crc_offset])
   {
      logger.warning("Failed CRC Calc<%02X> Got <%02X> - Send NACK",calcCrc, localMsg->cmd_data[crc_offset]);
      display_msg(localMsg);
      send_nack_cmd(NACK_CMD_CRC_ERROR, localMsg->command_id, localMsg->request_number);
      status = 4;
   }
   else if (localMsg->cmd_data[crc_offset+1] != 0xFC)
   {
      logger.warning("No Framing 0xFC got <%02X> - send NACK",localMsg->cmd_data[crc_offset+1]);
      display_msg(localMsg);
      send_nack_cmd(NACK_FRAMING_ERROR, localMsg->command_id, localMsg->request_number);
      status = 5;
   }
   return status;
}

/**
 * @fn      static int build_transmit_array(pMsgSerial txMsg, u8 **outPtr)
 *
 * @brief   creates array for transmission (based upon a message)
 * 
 * @param	[in]     txMsg - command to be sent 
 * @param	[out]    outPtr - pointer to array
 *
 * @retval 	Returns status of the call:
 * 
 *               >0   --> number of bytes in the output array
 *               == 0 --> undefined return;
 *               <0   --> error in construction
 *                      -1 == calloc error
 *                      -x == <tbd>
 *
 * @note	        Calling program must free the output array 
 *               (and command_data array - if necessary)
 *
 * @warning      Free output array & manage command_data array
 ****************************************************************************/
static int build_transmit_array(pMsgSerial txMsg, u8 **outPtr)
{
   u32 frame_offset, crc_offset;
   u32 total_size, command_data_size = txMsg->cmd_data_len;
   u8 *output;

   // calculate a few things //
   crc_offset   = HEADER_SIZE_SERIAL_MSGS  + command_data_size;
   frame_offset = crc_offset   + 1;
   total_size   = frame_offset + 1;

   // allocate output //
   if ((output = (u8 *)calloc(total_size+8,1)) == NULL)
   {
      logger.fatal("calloc External output array of %d bytes - %s\n",total_size, COMMON_ERR_STR);
      return -1;
   }
    
   // copy header to output array //
   memcpy(output,txMsg,HEADER_SIZE_SERIAL_MSGS);

   // copy command data out to array;
   if (command_data_size)
   {
      memcpy(&output[HEADER_SIZE_SERIAL_MSGS],txMsg->cmd_data, command_data_size);
   }
   
   // calculate CRC and put it in the last byte//
   output[crc_offset]   = calculate_array_crc(crc_offset, output);
   output[frame_offset] = 0xFC;
    
   // init output ptr & return with number of bytes created //
   *outPtr = output;
   return total_size;
}

/**
 * @fn      int build_send_serial_msg(int socket_fd, pMsgSerial txMsg, pthread_mutex_t *plock)
 * @brief   (PUBLIC API) Sends a cmd message on socket using a specified protocolID with associated command data
 * @param	[in] socket_fd  - socket to send message over
 * @param	[in] txMsg      - message to be sent
 * @retval 	Success        == 0
 *               Internal Error  < 0
 *               External Error  > 0   
 ****************************************************************************/
int build_send_serial_msg(int socket_fd, pMsgSerial txMsg, pthread_mutex_t *pLock)
{
   u8 *outPtr = NULL;
   int bytes2Send;
   int status = 0;

   pthread_mutex_lock(pLock);
   if (libPro == 0xFF)
   {
      DBG("first send <%02X>\n",txMsg->protocol_id);
      libPro = txMsg->protocol_id;
   }
   
   // pack message
   if ((bytes2Send = build_transmit_array(txMsg, &outPtr)) <= 0)
   {
      status = -1;
   }
   else
   {
      // send it now
      usleep(20000);
      int bytesSent = send_socket_data(socket_fd, outPtr, bytes2Send);
      if (bytesSent != bytes2Send)
      {
         logger.debug("Failed to Send message (cmdID:%02X)",txMsg->command_id);
         status = 1;
      }
      free(outPtr);
   } // got serial array
   
   pthread_mutex_unlock(pLock);
   return status;
}

/**
 * @fn      int init_serial_connection(int  *serial_fd)
 * @brief   initializes serial port for sending/receiving data
 * @param   [in] serial_fd - pointer to serial fd for serial port 
 * @param   [in] comport   - serial device to initialize
 * @param   [in] speed     - 0 == 115200, 1 == 57600, 2 == 19200

 * @retval  0 == good initialization, otherwise, error code
 ****************************************************************************/
int init_serial_connection(int *serial_fd, char *comport, u8 speed)
{
   logger.info("Initializing Serial connection on ComPort (%s)",comport);

   // open serial port //
   if ((*serial_fd = open (comport, O_RDWR | O_NOCTTY | O_SYNC)) <= 0)
   {
      logger.error("Serial Init (%s) Error(%d): %s", comport, *serial_fd, COMMON_ERR_STR);
      return -1;
   }

   // fflush the pipe
   fsync(*serial_fd);
   fflush(NULL);

   // setup serial port signals 
   struct termios tty;
   memset (&tty, 0, sizeof tty);
   cfmakeraw(&tty);
   
   tty.c_cflag     = CS8|CREAD|CLOCAL;
   tty.c_cc[VMIN]  = 0;            // read doesn't block
   tty.c_cc[VTIME] = 0;            // 0.5 seconds read timeout

   // set input/output speed & bitsize //
   switch(speed)
   {
   default:
      cfsetospeed (&tty, B115200);
      cfsetispeed (&tty, B115200);
      break;
   case 1:
      cfsetospeed (&tty, B57600);
      cfsetispeed (&tty, B57600);
      break;
   case 2:
      cfsetospeed (&tty, B19200);
      cfsetispeed (&tty, B19200);
      break;
   }
    
   // write configuration into this port's configuration
   if ((tcsetattr (*serial_fd, TCSANOW, &tty) != 0))
   {
      logger.critical("tcsetattr: <%s>", COMMON_ERR_STR);
      return -2;
   }
   // logger.debug("\t...Good Init <%s> @ <%04X>....",comport, B115200);

   return 0;
}

/**
 * @fn      void *serial_rx(void *args)
 * @brief   RCM Serial Rx Thread for receiving data from serial connection
 * @param   [in] args - pointer to serialRxArgs structure containing serial_fd & dispatch routine
 * @retval  NULL (exits when told to)
 ****************************************************************************/
void *serial_rx(void *args)
{
   pSerialRxArgs         pSerial       = (pSerialRxArgs)args;
   dispatch_function_t2  processfn     = pSerial->serialProcessData;
   int                  *serialPortFd  = pSerial->serial_fd;

   display_policy_priority();
      
   // entering to read //
   while(*pSerial->stopFlag == 0)
   {
      int status = 0;
      pMsgSerial rxMsg;
      if ((status = read_serial_msg(serialPortFd, &rxMsg, pSerial->pCmds, pSerial->numCmds, pSerial->stopFlag)))
      {
         logger.warning("RX MSG build Status <%d>(%d)",status,*pSerial->stopFlag);
         if (status < 0)
         {
            goto  serial_exit_serial_in;
         }
      }
      else
      {
         if ((rxMsg->command_id >= CAPDEV_GENERIC_REPLY) && (rxMsg->protocol_id) && (status == 0))
         {
            if ((status = processfn(rxMsg)))
            {
               logger.error("Process Serial Error(%d)",status);
               // goto  serial_exit_serial_in; 
            }
         }
         else
         {
            logger.warning("command/protocol(%02X/%02X) ids wasn't rational!", rxMsg->command_id, rxMsg->protocol_id);
         }
      }
      freeMsgContainerAll(&rxMsg);
   } // end while !usb stopping

serial_exit_serial_in:
   logger.highlight("Exiting Serial Rx Task(%d)",*pSerial->stopFlag);
   free(pSerial);
   common_close_socket(serialPortFd);
   common_stop_exit(2);
   return NULL;
}


/**
 * @fn      u8 getNextTxSeqSerial(void)
 * @brief   deals with next tx sequence
 * @retval  n/a
 **************************************************************/
static u8 nextTxSeqSerial     = 1;
extern u8 doPrint;
u8 getNextTxSeqSerial(void)
{
   u8 retValue = nextTxSeqSerial;
   if (nextTxSeqSerial == 127)
   {
      nextTxSeqSerial = 1;
   }
   else
   {
      nextTxSeqSerial++;
   }
   return (retValue);
}


/** Timer API Implementation
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <linux/types.h>

#include "logger.h"
#include "timer.h"

bool timer_is_started(Timer* timer) {
    return timer->started;
}

void* timer_thread_func(void* thread_param)
{
    // XXX: look for possible race conditions & fix them
    Timer* timer = (Timer*) thread_param;

    while (timer->active) {
        if (timer->started) {
            if (timer->remaining_ms <= 0) {
                timer->started = false;
                timer->cb(timer->cb_arg);
            } else {
                timer->remaining_ms--;
            }
        }
        usleep(1000);
    }
    return NULL;
}

unsigned long create_timer(uint32_t timeout_ms, TimerCallback cb, void* arg)
{
    Timer* timer = (Timer*) malloc(sizeof(Timer));
    if (!timer) return 0;
    bzero(timer, sizeof(Timer));
    timer->magic = TIMER_MAGIC;
    timer->active = true;
    timer->timeout_ms = timeout_ms;
    timer->cb = cb;
    timer->cb_arg = arg;

    if (pthread_create(&timer->timer_thread, NULL, timer_thread_func,
                (void*)timer)) {
        free(timer);
        return 0;
    }
    return (unsigned long)timer;
}

int start_timer(unsigned long timer_id)
{
    Timer* timer = (Timer*) timer_id;
    if (timer->magic != TIMER_MAGIC) {
        return -1;
    }
    if (timer->started) {
        return 0;
    }
    timer->remaining_ms = timer->timeout_ms;
    timer->started = true;
    return 0;
}

void cancel_timer(unsigned long timer_id)
{
    Timer* timer = (Timer*) timer_id;
    timer->started = false;
}

void delete_timer(unsigned long timer_id)
{
    Timer* timer = (Timer*) timer_id;
    timer->active = false;
    void* retval;
    pthread_join(timer->timer_thread, &retval);
    free(timer);
}

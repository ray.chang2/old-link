/**
 * @file msgIp.h
 * @brief common IP protocol msg processing 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_MSG_IP_H__
#define __MSG_MSG_IP_H__

// local includes
#include "commonTypes.h"
#include "connUtils.h"
#include "msgCommon.h"

/** IP message definitinon sent between all ct devices */
typedef struct ct_ip_msg
{
   u8  protocol_id;                 
   u8  command_id;
   u8  version;
   u8  cmd_sequence_number;
   u32 cmd_data_len;
   u8  *cmd_data;
} __attribute__ ((__packed__))
  ct_ip_msg_t, *pIpMsg;

////////////////////////////////////
///////// common defines ///////////
////////////////////////////////////
#define HEADER_SIZE_IP_MSGS    8

#define RESET_MSG_IP(msg) (reset_array(msg,sizeof(ct_ip_msg_t))) 

typedef enum {
   GET_SET_PROTOCOL_CLIENT_TO_BDM    = 0x01,
   GET_SET_PROTOCOL_BDM_TO_CLIENT    = 0x02,
   SETUP_BLOB_PROTOCOL_CLIENT_TO_BDM = 0x11,
   SETUP_BLOB_PROTOCOL_BDM_TO_CLIENT = 0x22,
} MSG_IP_PROTOCOL_TYPES;

//#define D2_BLOB_IP_PACKET_SIZE (D2_TOTAL_BLOB_DATA_SIZE + BLOB_DATA_HEADER_SIZE)

/*
 *  get data Request 
 */
typedef struct ct_ip_get_req_cmd
{
   u8 clientDeviceType;
   u8 serverDeviceType;
} __attribute__ ((__packed__))
ct_ip_get_req_cmd_t, *pGetReq;

#define IP_GET_DATA_REQUEST_SIZE     (sizeof(ct_ip_get_req_cmd_t))

/////////////////////////////////////////
// lavage related
////////////////////////////////////////

// CMD/REPLY structures
typedef struct lavage_cmd
{
   u8  clientDeviceType;
   u8  clientDevSubType;
   u8  serverDeviceType;
   u8  serverDevSubType;
   u32 triggerBitMask;
} __attribute__ ((__packed__))
lavage_cmd_t, *pLavageCmd;

typedef struct lavage_reply
{
   u8  clientDeviceType;
   u8  clientDevSubType;
   u8  serverDeviceType;
   u8  serverDevSubType;
   u32 triggerBitMask;
   u8  status;
} __attribute__ ((__packed__))
lavage_reply_t, *pLavageReply;

#define LAVAGE_PROTOCOL   0x44
#define LAVAGE_MSG_CMD    0x01 
#define LAVAGE_MSG_REPLY  0x02
#define LAVAGE_CMD_SIZE   (sizeof(lavage_cmd_t))
#define LAVAGE_REPLY_SIZE (sizeof(lavage_reply_t))


// general routines to handle ip messages //
int  read_ip_msg(int *fd, pExpCmds pIpCmds, u32 sizeExpCmds, pIpMsg rxMsg);
int  build_send_ip_msg(int socket_fd, pIpMsg txMsg, pthread_mutex_t *lock);
void dump_msg_data(void *data, u32 size);
void display_ip_msg_data(pIpMsg rxMsg);
void freeIpMsgContainerData(pIpMsg msg);
u8   getNextTxGetSetSeq(void);
u8   getNextTxSeqSetupBlob(void);


#endif // __MSG_MSG_IP_H__



#if !defined(__BROKER_API__)
#define	__BROKER_API__

#include "zmq.h"
#include "commonTypes.h"

// some #defines 
#define BROKER_MESSAGE_MAX                      100
#define	BROKER_FILTER_MAX                       32
#define	BROKER_NONBLOCKING                      1

// Define our port number for various applications for broker messges
#define	PUBLISHER_ACCESS_PORT  4267
#define	SUBSCRIBER_ACCESS_PORT 4925


// #define BROKER_DEBUG				0

//
// Generic Pub/Sub Message ID's for any capital device
//
#define BROKER_REBOOT_BRIDGE                    "bridge reboot"
#define BROKER_DATABASE_MODIFIED                "database modified"
#define BROKER_OSD_COORDS                       "OSD Coords" 
#define BROKER_BDM_COORDS                       "BDM Coords"
#define BROKER_BLINK_LED                        "Blink LED"
#define BROKER_REBOOT                           "Reboot"
#define BROKER_PING                             "Ping"

// bdm related 
#define BROKER_CAPDEV_CONNECTED                 "CapDev Connected"
#define BROKER_CAPDEV_HEARTBEAT                 "CapDev HeartBeat"
#define BROKER_CAPDEV_HEARBEAT                  BROKER_CAPDEV_HEARTBEAT
#define BROKER_DISCOVERY_LIST                   "Discovery List"
#define	PUBSUB_LINK_IN_LIST                     "Link in List"

//discovery-client published msgs
#define BROKER_DSC_WIFI_STATE                    "WIFI state"

// GPIO numbers for LED's on the bridge
#define BRIDGE_LED_D2_NETWORK                   4
#define BRIDGE_LED_D2_DEVICE                    5

// GPIO numbers for buttons on the bridge
#define INTELLIO_CONFIGURATION_GPIO             0
#define INTELLIO_FACTORY_RESET_GPIO             1

//
// Data structure for OSD Coords Message ID
//
typedef struct _osd_coords
{
   time_t    timestamp;
   uint8_t   portA_x;
   uint8_t   portA_y;
   uint8_t   portB_x;
   uint8_t   portB_y;
} __attribute__ ((__packed__))
  OSD_coords_t;

#define OSD_COORDS_SIZE  sizeof(OSD_coords_t)
#define BLOB_COORDS_SIZE (4)

typedef struct capDev_disc_heartbeat
{
   uint32_t capDevFeatures;
   uint8_t  capDevState;
} __attribute__ ((__packed__))
  capDev_disc_heartbeat_t, *pCapDevDiscHB;
#define CAPDEV_SIZE (sizeof(capDev_disc_heartbeat_t))

#if 0

//
// The following define is for testing whether or not we are having timing issues
// between the publishers and the broker as to when the broker is sending over 
// it's subscription message to the publisher. It's a timing issue.
//
// After the publisher call zmq_connect(), at some later time, the broker will 
// detect this and send down a subscription message to the publisher. Between the
// time that the publisher calls connect and the time the subscription message is 
// delivered to the publisher, the publisher should send any mesaages becasue 
// without any subscribers, the publishers messages will never get sent out.
//
// So by setting this constant, the broker will be compiled with some test code 
// to make sure it never loses a message from the publisher. Every message from 
// the publisher has a sequence number in it, starting at 0. If the first message 
// from the publisher to the broker has a sequence number in it other than 0, the
// broker knows that it lost a message and it will do something to notify the
// developer of this.
//
// There is a data structure that the publisher passes to any subscribers. See pubsub.h
// This ia test structure used for testing purposes. The broker has not need to 
// know about this structure, however, when this conditional compilation flag is 
// turned on, the broker is fully aware of this structure and uses it to intercept 
// the messages between the published and the subscriber. This structure caontains 
// a sequence numbe field and a pid field. The broker will keep track of PID's and 
// if it detects a new process send messages, it will check the seq field to see if
// 0 or not and do the appropriate thing.
//

#define	TEST_ZMQ_SUBSCRIPTION_TIMING

#endif


// typedef enum {PUBLISHER, SUBSCRIBER, MESSAGE_BROKER} PROCESS_TYPES;

//
// Publisher API
//
void* bm_publisher();
int   bm_send(void *controller, uint8_t *filter, void *data, uint32_t n);
int32_t bm_poll(void*, int32_t);

//
// Subscriber API
//
void*   bm_subscriber();
int     bm_subscribe(void *context, char *id, size_t nBytes);
int32_t bm_receive(void *, uint8_t *filter, void **data, uint32_t *n, int flags);

// utilities if we are subscribing to a byte
int  bm_get_sub_byte(void *handle, uint8_t *pByte);
void *bm_init_sub_handle(char *subscribeID);

// utility to send capdev heartbeat via broker
void bm_send_capDev_heartbeat(void *pHandle, bool state, u32 options);

// utility to send capDev connected state via broker
void bm_send_capDev_connected(void *pHandle, bool state);

/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Message Broker API -- specific to the broker only.
//

typedef	struct _pubsub_handle
{
   void *context;
   void *socket;
} pubsub_handle_t;

typedef struct	_broker
{
   pubsub_handle_t *subscribers;
   pubsub_handle_t *publishers;
} bm_broker_t;

int32_t bm_broker(bm_broker_t*);
#endif


/**
 * @file connUtils.h
 * @brief   Utilties for connection related functionality (connect/accept/send/receive)
 * @details all the forward declarations 
 * 
 * @version .01
 * @author  Tom Morrison
 * @date 8/25/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   8/25/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __CONN_UTILS_H__
#define __CONN_UTILS_H__

#include "commonMiscUtils.h"

/**
 * 
 * Socket utilities 
 */
int wait_network_bytes_available(int *inPutFd, int size, bool *stopFlag);            /**< waits forever (or stopping) for 'size' bytes  to be available on fd */
int wait_network_bytes_available_timed(int *inPutFd, int size, u32 waitSecs);        /**< waits until timeout or 'size' bytes  to become available on fd      */
int send_socket_data              (int socket_fd, u8 *output,      u32 output_size); /**< sends data to a socket                                              */
int receive_socket_data_message   (int socket_fd, u8 *input,       u32 input_size);  /**< receives data from a socket                                         */
int receive_socket_data_simple(int socket_fd, u8 *input_data, u32 input_size);       /**< receives simple socket data                                         */
int create_tcp_server_socket(u16 port);                                              /**> creates a new tcp/ip socket server to accept connections            */
int accept_tcp_socket_connection (int server_socket_fd);                             /**< accepts new tcp connection                                          */
int connect_tcp_socket_server(u32 addr, u16 port);                                   /**< connects to tcp server                                       */


/**
 * utility socket and resource functions
 */
bool isConnectionValid(int fd);
void common_close_socket(int *fd);                 /**< utility to close socket and reset fd          */   

#endif //__USB_CONN_UTILS_H__

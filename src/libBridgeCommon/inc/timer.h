/** Timer API Header
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#ifndef UTILS_TIMER_H
#define UTILS_TIMER_H

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////

typedef void* (*TimerCallback)(void*);

unsigned long create_timer(uint32_t timeout_ms, TimerCallback cb,
        void* arg);  // return: timer id
int start_timer(unsigned long timer_id);
void cancel_timer(unsigned long timer_id);
void delete_timer(unsigned long timer_id);

////////////////////////////////////////////////////////////////////////////////
// should be private. kept here for unit test
#define TIMER_MAGIC 0xdeadbeaf
typedef struct {
    uint32_t magic;
    pthread_t timer_thread;
    uint32_t timeout_ms;
    int32_t remaining_ms;
    TimerCallback cb;
    void* cb_arg;
    bool started;
    bool active;
    int debug_val;
} Timer;

#ifdef __cplusplus
}
#endif

#endif  // UTILS_TIMER_H

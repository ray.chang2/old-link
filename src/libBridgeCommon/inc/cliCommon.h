/**
 * @file msgIp.h
 * @brief common IP protocol msg processing 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __COMMON_CLI_H__
#define __COMMON_CLI_H__


int getStringStatus(const char *prompt, char *copyTo, u8 maxSize);

#endif // __COMMON_CLI_H__

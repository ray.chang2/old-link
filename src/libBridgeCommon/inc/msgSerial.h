/**
 * @file msgSerial.h
 * @brief common serial message functionalty
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/25/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/25/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_SERIAL_H__
#define __MSG_SERIAL_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgCommon.h"

// 
#define HEADER_SIZE_SERIAL_MSGS       4
#define READ_SIZE_REMAINING_HEADER    2
typedef struct bdm_serial_msg_container {
   u8  protocol_id; 
   u8  command_id;
   u8  request_number;
   u8  cmd_data_len;
   u8 *cmd_data;
} __attribute__ ((__packed__))
  bdm_serial_msg_container_t, *pMsgSerial;

void display_msg(pMsgSerial msg);

#define RESET_MSG(msg) (reset_array(msg,sizeof(bdm_serial_msg_container_t)))

u8 getNextTxSeqSerial(void);

#define REQUEST_NUMBER_CMD_BIT_MASK      (0x80)
#define REQUEST_NUMBER_RESPONSE_BIT_MASK (0x7F)
#define SET_CMD_REQUEST_NUMBER(x)        (x | REQUEST_NUMBER_CMD_BIT_MASK)
#define GET_CMD_REQUEST_NUMBER()         (getNextTxSeqSerial() | REQUEST_NUMBER_CMD_BIT_MASK)
#define RESPONSE_REQUEST_NUMBER(x)       (x & REQUEST_NUMBER_RESPONSE_BIT_MASK)

/**
 * more prototypes for utility commands
 */
int  clear_backup_serial_msg(u8 requestNumber);
u32  get_num_msg_backed(void);
void add_backup_serial_msg(pMsgSerial msg, u32 timeoutCount);
void clear_all_backup_msgs(void);
pMsgSerial get_copy_requestNumber_msg(u8 requestNumber, u32 newTimeoutCount, u32 *retryCount);
pMsgSerial get_next_timeout_msg(u32 currentCount, u32 newTimeoutCount, u32 *retryCount);
   
void freeMsgContainerAll(pMsgSerial *msg);
void freeMsgContainerData(pMsgSerial msg); 
int  read_serial_msg(int *fd, pMsgSerial *rxMsg, pExpCmds expCmds, u8 numCmds, bool *stopFlag);
int  build_send_serial_msg(int socket_fd, pMsgSerial txMsg, pthread_mutex_t *pLock);
void display_blob(pBlobDataHdr pHeader, u8 *blobData, u8 count);

///////////////////////////////////////
///////////////////////////////////////
// serial rx thread 
///////////////////////////////////////
///////////////////////////////////////
int  init_serial_connection(int *serial_fd, char *comport, u8 speed);  /**< inits the serial port */
void *serial_rx(void *args);                  /**< main serial rx task   */

/**
 * dispatch function called
 */
typedef int (*dispatch_function_t2)(pMsgSerial rxMsg);  /**< function prototype for dispatch function */

/////////////////////////////////////////////////
/// global with 100ms heartbeat (@ 10/tick) ///
/////////////////////////////////////////////////
#define TIMEOUT_HEARTBEAT          (YIELD_10MS)
#define MAX_NUMBER_RX_TIMEOUT_MISS (90)
#define COMMAND_TIMEOUT_HB_COUNT   (25)
#define SEND_HEARTBEAT_COUNT       (10)
/////////////////////////////////////////////////
/////////////////////////////////////////////////

/**
 * serial RX args for serial rx thread
 */
typedef struct serial_rx_args 
{
   dispatch_function_t2 serialProcessData;
   int     *serial_fd;
   u8       numCmds;
   pExpCmds pCmds;
   bool    *stopFlag;
} serial_rx_args_t, *pSerialRxArgs;


///////////////////////////////
/// Generic Reply
///////////////////////////////
typedef enum {
   ACK_GENERAL_SUCCESS,
   NACK_GENERAL_ERROR,
   NACK_VERSION_NOT_SUPPORTED,
   NACK_PID_NOT_SUPPORTED,
   NACK_CMD_NOT_SUPPORTED,
   NACK_CMD_DATA_LEN_ERROR,
   NACK_CMD_CRC_ERROR,
   NACK_FRAMING_ERROR,
   MAX_REPLY_TYPES,
} GENERIC_REPLY_TYPES;

#define CAPDEV_GENERIC_REPLY 0x30
typedef struct capdev_generic_reply {
   u8 status;
   u8 cmd_id;
   u8 reqNum;    
}  __attribute__ ((__packed__))
  capdev_generic_reply_t, *pGenReply;

#define GENERIC_REPLY_DATA_LENGTH (sizeof(capdev_generic_reply_t))
void send_nack_cmd(u8 status, u8 cmdID, u8 reqNum);

#define CAPDEV_GENERAL_DISCOVERY (0x31)
/******************************
////////////////////////////////
/// General Error Status sent //
//  in generic response       //
//  (from OSD_Manager.h)      //
////////////////////////////////
*******************************/
typedef enum {
   SCD_DEVICE_READY,
   SCD_FATAL_ERROR1, // need some clarification
   SCD_FATAL_ERROR2,
   SCD_FATAL_ERROR3,
   SCD_FATAL_ERROR4,
   SCD_FATAL_ERROR_TABLE_1,
   SCD_FATAL_ERROR_TABLE_2,
   SCD_FATAL_ERROR_TABLE_3,
   SCD_FATAL_ERROR_TABLE_4,
   SCD_FATAL_ERROR_TABLE_5,
   SCD_FATAL_ERROR_TABLE_6,
   SCD_FATAL_ERROR_TABLE_7,
   SCD_FATAL_ERROR_TABLE_8,
   SCD_FATAL_ERROR_TABLE_9,
   SCD_FATAL_ERROR_TABLE_10,
   SCD_FATAL_ERROR_TABLE_11,
   SCD_UNABLE_TO_SAVE_CUSTOM_SETTINGS,
   MAX_DEVICE_ERRORS
} SCD_ERROR_TYPES;

/******************************
///////////////////////////////
/// Discovery
///////////////////////////////
*******************************/
typedef struct bdm_to_capdev_discover {
   u8 version_number;
} __attribute__ ((__packed__))
  bdm_to_capdev_discovery_t, *pBdm2CapdevDisc;

#define BDM_TO_CAPDEV_DISCOVERY_REQ_LEN (sizeof(bdm_to_capdev_discovery_t))

typedef struct capdev_to_bdm_discovery {
   u8 version_number;
   u8 device_state;
   u8 devType;
   u8 devSubType;
} __attribute__ ((__packed__))
  capdev_to_bdm_discovery_t, *pCapdev2BdmDisc;

#define CAPDEV_TO_BDM_DISCOVERY_RESP_LENGTH (sizeof(capdev_to_bdm_discovery_t))

typedef struct common_blob_top_msg {
   u8  blobNum;
} __attribute__ ((__packed__))
  common_blob_top_msg_t, *pBlobTop;

#define FIRST_DATA_BLOB_NUMBER  (1)
#define BLOB_DATA_OFFSET (sizeof(common_blob_top_msg_t))
#define MAX_OPAQUE_DATA_PACKET  (48)

typedef struct blob_data_ack {
   u8 packetNumber;
   u8 status; // ack/nack error
}  __attribute__ ((__packed__))
  blob_data_ack_t, *pBlobDataAck;

#define GET_BLOB_DATA_REQUEST_DATA_LEN      (1)
#define SET_BLOB_DATA_RESPONSE_DATA_LEN     (sizeof(blob_data_ack_t))

#endif // __MSG_SERIAL_H__

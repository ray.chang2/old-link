#if !defined(__WW_MESSAGES_H__)
#define	__WW_MESSAGES_H__

//
// Define our port number for various applications for broker messges
//

#define	WW_PUBLISHER_ACCESS_PORT  5557
#define	WW_SUBSCRIBER_ACCESS_PORT 5558

//
// Publish specific channel names. These string names must be unique
// These channel names can be a MAXIMUM of BROKER_FILTER_MAX characters in length. 
//
#define	WW_LVC_CLEAR    "ClearLVC"    // to instruct the broker to clear the cache
#define	WW_PORT_STATUS  "WWStatus"    // for getting channel status messages from the BDM
#define WW_CONNECTED    "WWConnected" // used to tell get/set servers to 
#define WW_SET_CMD      "WWSetCmd"    // used to publish set commands

#endif




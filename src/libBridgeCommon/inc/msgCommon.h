/**
 * @file msgCommon.h
 * @brief common message utilities
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/25/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/25/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_COMMON_H__
#define __MSG_COMMON_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */

// blob definition used with IP Protocols as well as devices //
#define BLOB_NAME_SIZE               16
typedef struct ct_blob_data_header {
   u8  prot_version;
   u8  blob_header_size; // fixed length - should be 23?
   u16 blob_data_size;   // fixed length ==> should be 232
   u8  deviceType;
   u8  subDeviceType;
   u8  blob_name[BLOB_NAME_SIZE];
   u8  header_crc;  
} __attribute__ ((__packed__))
ct_blob_data_header_t, *pBlobDataHdr;

#define BLOB_DATA_HEADER_SIZE (sizeof(ct_blob_data_header_t))
#define BLOB_DATA_HEADER_CRC_SIZE (BLOB_DATA_HEADER_SIZE - 1)

typedef struct blob_header_msg {
   u8  blobNum;
   ct_blob_data_header_t blobHeader;
} __attribute__ ((__packed__))
  blob_header_msg_t, *pBlobHdrMsg;

#define BLOB_RESPONSE_HEADER_MSG_SIZE (sizeof(blob_header_msg_t))
#define SET_BLOB_HEADER_DATA_LEN      (sizeof(blob_header_msg_t))

// Expected command data 
typedef struct ct_exp_cmds {
   u8 expProt;
   u8 expCmd;
   u8 expCmdLen;
} __attribute__ ((__packed__))
ct_exp_cmds_t, *pExpCmds;

#define ZERO_LENGTH_MSG_DATA           (0)
#define ONE_BYTE_ACK_RESPONSE_DATA_LEN (1)
#define VARIABLE_LENGTH_MSG_DATA_SIZE  (-1)
/**
 * more prototypes for utility commands
 */
void display_blob(pBlobDataHdr pHeader, u8 *blobData, u8 count);

#define SETUP_BLOB_PROTOCOL_SOCKET 55000

typedef enum {
   // section 4.1
   SETUP_BLOB_GET_CMD      = 1,
   SETUP_BLOB_SET_CMD      = 2,

   // section 4.2 
   SETUP_BLOB_GET_RESPONSE = 0x01,
   SETUP_BLOB_SET_RESPONSE = 0x02,

} SETUP_BLOB_CMD_TYPES;

#define NUM_IP_GET_SET_CMDS 2
#define SET_SETUP_BLOB_RESPONSE_SIZE 1
#endif // __MSG_COMMON_H__

/** Logging functionality Header
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>
#include "commonTypes.h"

// Trying to simulate Python `logging` library
#define LV_NOTSET 0
#define LV_DEBUG 10
#define LV_INFO 20
#define LV_HIGHLIGHT 25
#define LV_WARNING 30
#define LV_ERROR 40
#define LV_CRITICAL 50
#define LV_FATAL 60
#define LV_ERROR_INTERNAL 70

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*logger_func)(const char* format, ...);

typedef struct Logger {
    bool enable_exit;

    logger_func debug;
    logger_func info;
    logger_func highlight;
    logger_func warning;

    logger_func error;
    logger_func critical;
    logger_func fatal;
    logger_func error_internal;
} Logger;

extern Logger logger;

#ifdef UNITTEST
void stub_func(const char* format, ...) {}
Logger logger = {
    1,

    stub_func,
    stub_func,
    stub_func,
    stub_func,

    stub_func,
    stub_func,
    stub_func,
    stub_func
};
#endif

#ifdef __cplusplus
}
#endif

#endif  // LOGGER_H

/** Header File for Discovery Service Utility Functions
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#ifndef UTIL_H
#define UTIL_H

#include <inttypes.h>

enum LED_STATUS {
    OFF = 0,
    ON = 1
};

#define do_assert(x) if (!(x)) { \
    logger.error_internal("assertion %s failed\n", #x); \
    exit(1); \
    }

#define do_assert2(x, s) if (!(x)) { \
    logger.error_internal("assertion %s failed: %s\n", #x, s); \
    exit(1); \
    }

void turn_captital_device_led(int led_status);
void turn_tower_network_led(int led_status);

#endif

/**
 * @file commonSysUtils.h
 * @brief   System Utilties functionality
 * 
 * @version .01
 * @author  Tom Morrison
 * @date 3/18/15
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   3/18/15   Initial Creation.
 *
 *</pre>
 */

#ifndef __COMMON_SYS_UTILS_H__
#define __COMMON_SYS_UTILS_H__

/**
 * Thread priority types
 */
typedef enum {
   MIN_COMMON_PRIORITY_LEVEL = 0,
   MAINT_PRIORITY,
   LOWEST_LOW,
   LOWER_LOW,
   MEDIUM_LOW,
   HIGH_LOW,
   LOW_MEDIUM,
   MEDIUM_MEDIUM,
   HIGH_MEDIUM,
   LOW_HIGH,
   MEDIUM_HIGH,
   HIGHER_HIGH,
   HIGHEST_PRIORITY,
   MAX_COMMON_PRIORITY_LEVEL,
} COMMON_THREAD_PRIORITY_LEVELS;

/**
 * thread creation
 */
int common_create_thread(pthread_t *pTh, COMMON_THREAD_PRIORITY_LEVELS level, void *args, void *(*thread_fn)(void *));  
void common_kill_thread(pthread_t thread, const char *whatThread);
void display_policy_priority(void);

#endif //__COMMON_SYS_UTILS_H__

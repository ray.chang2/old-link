#if !defined(__D2_MESSAGES_H__)
#define	__D2_MESSAGES_H__

//
// Publish specific channel names. These string names must be unique
// These channel names can be a MAXIMUM of BROKER_FILTER_MAX characters in length. 
//
#define	D2_LVC_CLEAR    "ClearLVC"	// to instruct the broker to clear the cache
#define	D2_STATUS       "D2Status"	// used to get status D2 Connection
#define D2_CONNECTED    "D2Connected"   // used to tell get/set servers to 
#define D2_SET_SCD_CMD  "D2SetScdCmd"   // used to publish set commands
#define D2_SET_HPBF_CMD "D2SetHPBFCmd"  // used to publish configuration setting
#define D2_SERIAL_NUM   "D2SerialNum"   // used to publish serial number 
#endif // __D2_MESSAGES_H__




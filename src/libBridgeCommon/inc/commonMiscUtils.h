/**
 * @file commonMiscUtils.h
 * @brief   Miscellaneous Utilties functionality
 * @details all the forward declarations 
 *             thread creation/deletion
 *             semaphores
 * 
 * @version .01
 * @author  Tom Morrison
 * @date 8/25/13
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   8/25/13   Initial Creation.
 *
 *</pre>
 *
 * @todo  future: timers
 * @todo  list to do items
 */

#ifndef __COMMON_MISC_UTILS_H__
#define __COMMON_MISC_UTILS_H__

#include "commonSysUtils.h"

/**
 * time related
 */
void   convert_miliSecs(u32 ms, u32 *sec, u32 *ns); 

/**
 * signal handler
 */
void init_common_signal_handler(void);             /**< initializes common signal handler                  */
/**
 * File Utilities
 */
void reset_array(void *p, u32 size);                                  /**< resets array                           */
/**
 * validation utilities
 */
bool isFileExist(const char *fullFileName);                           /**< checks if file/folder exists             */
u8  calculate_array_crc(u32 size, u8 *array);                         /**< public function for calculating crc      */

void common_stop_exit(int exit_code);

#endif //__COMMON_Misc_UTILS_H__


#define	_XOPEN_SOURCE

#include <stdio.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syslog.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <linux/if_link.h>

#include    "commonTypes.h"
#include    "ctb_db_api.h"
#include	"ctb_db_table_api.h"
#include	"ctdb_utils.h"
#include	"gpio.h"
#include	"i2c-dev.h"
#include	"broker_api.h"
#include	"D2_channels.h"
#include	"WW_channels.h"

static const char* version = "01.01.00"; //follows major.minor.patch scheme(https://semver.org)

#define	GPIO_0_NORMAL_AP_FILENAME		"/etc/network/AP"
#define	GPIO_0_NORMAL_HOST_FILENAME		"/etc/network/HOST"

#define	WIFI_CONFIG_FILE				"/etc/wifi.config"
#define	GPIO_LED_COUNT					6

#define		xxxDEBUG_WIFI_ISSUE	1


#define		BRIDGE_NETWORK_DOWN		0
#define		BRIDGE_NETWORK_UP		1

#define		CAPITAL_DEVICE_CONNECTED		1
#define		LINK_IN_DISCOVERY_LIST			2


// Undefine this when we fix the Blue LED problem
#define	MASK_THE_BLUE_LED_PROBLEM			1


// static information on services offered by connected link devices
//  Note1: service_unit name field below should match with file name of the service unit file under /etc/systemd/system
//  Note2: Order of these entries follow enum order in CT_defines.h
//  Note3: systemctl controlled services have their type flag defined accordingly

typedef struct LinkServices 
{
    const char* service_unit;
    bool isSystemCtlType;
} LinkServices;

typedef	struct	_gpio_thread_data
{
	uint32_t				gpio;			// The GPIO to monitor
	uint32_t				sampling_msecs;	// ms to wait before taking the next sample
	uint32_t				trigger_msecs;	// ms to wait before triggering the event
	void 					*(*func) (void*);
	uint32_t				verbose:1;
	uint32_t				current_value:1;	// current value of the GPIO
	uint8_t					broadcast:1;	// do we want to broadcast when this is triggered?
	uint8_t					broadcast_value:1;	// when the GPIO has this value, then broadcast
	uint8_t					broadcast_secs:3;	// when broadcasting, how often to send
	uint32_t				triggered:1;
} GPIO_thread_data_t;

void* factory_reset_thread(void *data);
void* configuration_thread(void *data);


typedef struct	_state
{
	uint32_t	reboot:1;
	uint32_t	value:1;
} network_state_t;

GPIO_thread_data_t	gpio_data[GPIOS_TO_MONITOR] = 
{
	{0, 500, 5000, (void*) configuration_thread, 0, 1, 0, 0, 0, 0},
	{1, 50,  5000, (void*) factory_reset_thread, 0, 0, 0, 0, 0, 0}
};

uint32_t	verbose = 0, reboot = 1;
int			g_mode=-1; 
void		*pubhandle;
char		saved_filename[256];
led_control_t		led_data[GPIO_LED_COUNT];

pthread_mutex_t	factory_reset_lock = PTHREAD_MUTEX_INITIALIZER;

uint8_t		blue_led_controller = 0;

#if defined(MASK_THE_IP_ADDRESS_PROBLEM) || defined(DEBUG_WIFI_ISSUE)
///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

int32_t
check_ipaddress(uint8_t *interface)
{
int 			fd;
char			buffer[192];
struct ifreq 	ifr;


	fd = socket(AF_INET, SOCK_DGRAM, 0);

	/* I want to get an IPv4 IP address */
	ifr.ifr_addr.sa_family = AF_INET;

	/* I want IP address attached to "eth0" */
	strncpy(ifr.ifr_name, (char*) interface, IFNAMSIZ-1);

	ioctl(fd, SIOCGIFADDR, &ifr);

	close(fd);

	/* display result */
	snprintf(buffer, sizeof(buffer), "%s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));

	printf("check_ipaddress: %s\n", buffer);

	if(strlen(buffer) > 10)
		return strlen(buffer);
	else
		return 0;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void*
ipaddress_monitor(void *p)
{
uint32_t	count=40;
char		buffer[256];


	snprintf(buffer, sizeof(buffer), "ENTER ipaddress_monitor()");
	blog(buffer, NULL, 0);

	while(count--)
	{
		sleep(1);

		if(check_ipaddress((uint8_t*) "wlan0") > 0)
			break;
	}

	if(check_ipaddress((uint8_t*) "wlan0") == 0)
	{
		snprintf(buffer, sizeof(buffer), "NO IP Address ... exiting");
		blog(buffer, NULL, 0);

		system("mv /sn/gpiod /sn/g");
		exit(60);
	}

	snprintf(buffer, sizeof(buffer), "ipaddress_monitir: rebooting");
	blog(buffer, NULL, 0);

	system("/sbin/reboot");

	return p;
}
#endif


#if defined(DEBUG_WIFI_ISSUE)
///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void*
debug_wifi_issue(void *p)
{
uint32_t	count=30;
char		buffer[256];


	sleep(20);

	system("/sn/check_wifi");
	

	while(count--)
	{
		if(check_ipaddress((uint8_t*) "wlan0") > 0)
		{
			snprintf(buffer, sizeof(buffer), "debug_wifi_issue: we have an IP address");
			blog(buffer, NULL, 0);
			break;
		}

		sleep(1);
	}

	if(check_ipaddress((uint8_t*) "wlan0") == 0)
	{
		snprintf(buffer, sizeof(buffer), "gpiod: NOIP address ... exiting ");
		blog(buffer, NULL, 0);

		chdir("/sn");
		system("mv gpiod g");
		exit(60);
	}

	snprintf(buffer, sizeof(buffer), "gpiod: rebooting ...");
	blog(buffer, NULL, 0);
	system("/sbin/reboot");
}
#endif

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

#if !defined(NI_MAXHOST)
#define	NI_MAXHOST	1025
#endif

#if !defined(NI_NUMERICHOST)
#define	NI_NUMERICHOST	1
#endif



#if defined(NOT_YET)
int
is_network_up(char *interface)
{
struct ifaddrs *ifaddr, *ifa;
int family, s, n;
char host[NI_MAXHOST];

    if(getifaddrs(&ifaddr) < 0)
	{
        return -1;
	}

	// Walk through linked list, maintaining head pointer so we
	// can free list later */

	for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++)
	{
		if (ifa->ifa_addr == NULL)
			continue;

		if(strcmp(ifa->ifa_name, interface))
			continue;

		family = ifa->ifa_addr->sa_family;

		// Display interface name and family (including symbolic
		// form of the latter for the common families) */

        // For an AF_INET* interface address, display the address */

		if (family == AF_INET || family == AF_INET6)
		{
			s = getnameinfo(ifa->ifa_addr,
				(family == AF_INET) ? sizeof(struct sockaddr_in) :
				sizeof(struct sockaddr_in6),
				host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
			if (s != 0) {
				// printf("getnameinfo() failed: %s\n", (char*) gai_strerror(s));
				exit(EXIT_FAILURE);
			}

			freeifaddrs(ifaddr);
			return 1;
		}
	}

	freeifaddrs(ifaddr);
	return 0;
}
#endif


///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void
blog(void *ff, void *data, uint32_t n)
{
uint8_t		*filter = ff;
uint8_t     *fmt = (uint8_t*) "%x ";
uint8_t     tbuf[128];
uint32_t    i, m;
int         fd;
time_t      t;
uint8_t     *p;
uint8_t     buffer[512];

    memset(tbuf, 0, sizeof(tbuf));
    memset(buffer, 0, sizeof(buffer));

    if((fd = open("/var/log/gpiod.log", O_APPEND|O_RDWR|O_CREAT, 0744)) < 0)
    {
        perror("open");
        return;
    }

    if(!strcmp((char*) filter, "INFO"))
        fmt = (uint8_t*) "%c";

    float   f = sizeof(buffer) * 0.8;

    t = time(NULL);
    p = (uint8_t*) ctime(&t);
    p[strlen((char*) p) - 1] = 0;

    snprintf((char*) buffer, sizeof(buffer), "%s: %s (%d): ", p, filter, n);

    p = data;

    m = (n>30?16:n);

    for(i=0; i<m; i++)
    {
        snprintf((char*) tbuf, sizeof(tbuf), (char*) fmt, (uint8_t) p[i]);
        strcat((char*) buffer, (char*) tbuf);

        if((float) strlen((char*) buffer) > f)
        {
            snprintf((char*) tbuf, sizeof(tbuf), "  strlen(buffer) = %d",
                (int) strlen((char*) buffer));
            strcat((char*) buffer, (char*) tbuf);

            strcat((char*) buffer, (char*) "  --- BREAK\n");
            break;
        }
    }

    strcat((char*) buffer, "\n");

    write(fd, (char*) buffer, strlen((char*) buffer));
    close(fd);
}


///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

int8_t
read_value(uint8_t *filename)
{
int32_t		fd, status;
int8_t		value;
char		buffer[256];


	// snprintf(buffer, sizeof(buffer), "ENTER: read_value: %s", filename);
	// blog(buffer, NULL, 0);

	if((fd=open((char*) filename, O_RDONLY)) < 0)
	{
		// also should log an error here
		snprintf(buffer, sizeof(buffer), "open(%s) failed, fd: %d, errno: %d", filename, fd, errno);
		blog(buffer, NULL, 0);

		perror("open gpio");
		value = fd;
	}
	else
	{
		value = 0;
		if((status=read(fd, &value, 1)) != 1)
		{
			// also should log an error here
			snprintf(buffer, sizeof(buffer), "read_value: read failed: %d, errno: %d", status, errno);
			blog(buffer, NULL, 0);

			if(status < 0) perror("read");
			value = -2;
		}
		else value = value & 1;
	}

	close(fd);

	return value;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

int
GPIO_read(int gpio)
{
char	filename[256];


	snprintf((char*) filename, sizeof(filename), "%s/gpio%d/value", GPIO_BASEPATH, gpio);

	return read_value((uint8_t *) filename);
}


///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void
create_wifi_config()
{
device_config_st	creds;
int					value, fd, status, mode;
char				buffer[256];


	snprintf(buffer, sizeof(buffer), "ENTER: create_wifi_config");
	blog(buffer, NULL, 0);

	status = getMode(&mode);

	if(status >= 0)
	{
		value = GPIO_read(INTELLIO_CONFIGURATION_GPIO);

		if(value == GPIO_CONFIGURATION_MODE_CONFIG)
		{
			// The eeprom executable creates the /etc/wifi.config just as we need it.
			// So just return after creating it
			system("/sn/eeprom -r > /etc/wifi.config");
			return;
		}
		else if(mode == MODE_OPERATION_AP_VALUE)
		{
			creds.mode = MODE_OPERATION_AP_VALUE;	
			status = getCredentials(&creds);

			if(status == 0)
				snprintf(buffer, sizeof(buffer), "SSID=%s\nPASS=%s\n", 
					creds.ssid, creds.password);
			else
			{
				snprintf(buffer, sizeof(buffer), "Error reading AP credentials from database: %d", status);
				blog(buffer, NULL, 0);
			}
		}
		else if(mode == MODE_OPERATION_HOST_VALUE)
		{
			creds.mode = MODE_OPERATION_HOST_VALUE;	
			status = getCredentials(&creds);

			if(status == 0)
				snprintf(buffer, sizeof(buffer), "SSID=%s\nPASS=%s\n", 
					creds.ssid, creds.password);
			else
			{
				snprintf(buffer, sizeof(buffer), "Error reading HOST credentials from database: %d", status);
				blog(buffer, NULL, 0);
			}
		}
	}

	unlink(WIFI_CONFIG_FILE);
	if((fd=open(WIFI_CONFIG_FILE, O_WRONLY|O_CREAT)) < 0)
	{
		perror("open WIFI_CONFIG_FILE");
	}

	status = write(fd, buffer, strlen(buffer));
	if(status < 0)
	{
		snprintf(buffer, sizeof(buffer), "create_wifi_config: error writing to wifi.conf: status = %d", status);
		blog(buffer, NULL, 0);
	}

	close(fd);

	snprintf(buffer, sizeof(buffer), "exit:create_wifi_config: check_status: wpa file");
	blog(buffer, NULL, 0);
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

int
check_state(network_state_t *pns)
{
int					status=0, dbconfigmode=0;
char				buffer[512];
device_config_st	creds;


	// we have all the info we need to make a decision
	if(pns->value == 0)  // 0 -- config mode
	{
		printf("check_state: value == 0\n");

		if(access("/etc/network/EEPROM", F_OK) < 0)
		{
			system("rm -f /etc/network/AP");
			system("rm -f /etc/network/HOST");
			printf("check_state: dumping eeprom to /etc/wifi.config\n");
			system("/sn/eeprom -r > /etc/wifi.config");
			system("touch /etc/network/EEPROM");

			pns->reboot = 1;
		}

		// hmmm, switch says we're in AP mode, but the file says we are in HOST mode.
		// We'll need to reboot after we straighten this mess out
	}
	else
	{
		create_wifi_config();

		pns->reboot = 0;

		if(access("/etc/network/EEPROM", F_OK) == 0)
			system("rm -f /etc/network/EEPROM");

		// read the database
		if((status = getMode(&dbconfigmode)) < 0)
		{
			printf("database returns %d\n", status);
			exit(1);
		}

		// value must equal 1 then == host mode
		// So the switch dictates that we're in HOST mode, now the database dictates
		// which mode the network will be in and the database dictates which file 
		// should exist in /etc/networks

		if(dbconfigmode == MODE_OPERATION_HOST_VALUE && access(GPIO_0_NORMAL_AP_FILENAME, F_OK) == 0) 
		{
			printf("check_state: HOST\n");
			pns->reboot = 1;
			pns->value = 1;

			creds.mode = MODE_OPERATION_HOST_VALUE;	
			getCredentials(&creds);

			snprintf(buffer, sizeof(buffer), "WIFI_ST_SSID=%s\nWIFI_ST_PASSWORD=%s\n", 
				creds.ssid, creds.password);
		}
		else if(dbconfigmode == MODE_OPERATION_AP_VALUE && access(GPIO_0_NORMAL_HOST_FILENAME, F_OK) == 0)
		{
			printf("check_state: AP\n");
			pns->reboot = 1;
			pns->value = 0;

			creds.mode = MODE_OPERATION_AP_VALUE;	
			getCredentials(&creds);

			snprintf(buffer, sizeof(buffer), "WIFI_AP_SSID=%s\nWIFI_AP_PASSWORD=%s\n", 
				creds.ssid, creds.password);
		}
		else if((access(GPIO_0_NORMAL_AP_FILENAME, F_OK) < 0) && (access(GPIO_0_NORMAL_HOST_FILENAME, F_OK) < 0))
		{
		int		mode;

			printf("no mode stats, check database\n");
			pns->reboot = 1;
			pns->value = 0;

			status = getMode(&mode);

			if(mode == MODE_OPERATION_AP_VALUE)
			{
				creds.mode = MODE_OPERATION_AP_VALUE;	
			
				getCredentials(&creds);

				snprintf(buffer, sizeof(buffer), "WIFI_AP_SSID=%s\nWIFI_AP_PASSWORD=%s\n", 
					creds.ssid, creds.password);

			}
			else if(mode == MODE_OPERATION_HOST_VALUE)
			{
				creds.mode = MODE_OPERATION_HOST_VALUE;	

				getCredentials(&creds);

				snprintf(buffer, sizeof(buffer), "WIFI_ST_SSID=%s\nWIFI_ST_PASSWORD=%s\n", 
					creds.ssid, creds.password);
			}
		}
		else printf("check_state: NO MODE\n");

		if(pns->reboot)
		{
		int		fd;

			unlink(WIFI_CONFIG_FILE);
			if((fd=open(WIFI_CONFIG_FILE, O_WRONLY|O_CREAT)) < 0)
			{
				// log error here
				pns->reboot = 0;
			}

			status = write(fd, buffer, strlen(buffer));
			if(status < 0)
			{
				// log error here
				pns->reboot = 0;
			}

			printf("check_status: wpa file:\n%s\n", buffer);

			close(fd);
		}
	}

	return 0;
}


///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void*
gpio_dispatcher(void *data)
{
GPIO_thread_data_t	*pgpio = (GPIO_thread_data_t*) data;
int8_t				initial_value=0, current_value;
uint32_t			press=0;
uint8_t				filename[128];
pthread_t			tid;



	snprintf((char*) filename, sizeof(filename), "%s/gpio%d/value", GPIO_BASEPATH, pgpio->gpio);
	current_value = initial_value = read_value(filename);

	printf("ENTER: gpio_dispatcher: %d: value: %d\n", pgpio->gpio, initial_value);

	// We are not designed to ever exit from here.
	while(1)
	{
		if(pgpio->verbose)
			printf("TOL: GPIO: %d: press: %d, initial: %d, current: %d\n", 
				pgpio->gpio, press, initial_value, current_value);

		if((current_value = read_value(filename)) < 0)
		{
			printf("gpio_dispatcher: value: %d\n", current_value);
			exit(-1);
		}

		if(initial_value != current_value)
		{
			press += pgpio->sampling_msecs;

			if(press > pgpio->trigger_msecs && pgpio->triggered == 0)
			{
				if(pgpio->verbose)
					printf("BING BING BING ... We have an EVENT: %d - %d: %d\n", press, pgpio->trigger_msecs, current_value);

				press = 0;

				pgpio->current_value = current_value;
				pgpio->triggered = 1;

				if(pthread_create(&tid, NULL, pgpio->func, pgpio))
				{
					printf("gpio_dispatcher: could not create thread for gpio %d\n", pgpio->gpio);
				}
			}
		}
		else if(press && initial_value == current_value)
		{
			press = 0;
		}

		usleep(pgpio->sampling_msecs * 1000);
	}
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void*
database_monitor()
{
uint8_t				filename[256];
struct	stat		original, latest;


	snprintf((char*) filename, sizeof(filename), "/sn/webserver/db/ctb.db");

	if(stat((char*) filename, &original) < 0)
	{
		// log error here, possibly even exit

	}

	while(1)
	{
		if(stat((char*) filename, &latest) < 0)
		{
			// log error here, possibly even exit
		}

		if(memcmp(&original, &latest, sizeof(struct stat)))
		{
			if(bm_send(pubhandle, (uint8_t*) BROKER_DATABASE_MODIFIED, "00000", 5) < 0)
			{
				// try again
				if(bm_send(pubhandle, (uint8_t*) BROKER_DATABASE_MODIFIED, "00000", 5) < 0)
				{
					printf("Failed to pub Database Modified message\n");
				}
			}

			memcpy(&original, &latest, sizeof(struct stat));
		}

		sleep(2);
	}
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void*
factory_reset_thread(void *data)
{
GPIO_thread_data_t	UNUSED_ATTRIBUTE *pgpio = (GPIO_thread_data_t*) data;
device_config_st    creds;
char				buffer[128];
int					value, status;


	blog("factory_reset thread invoked ...", NULL, 0);

	// We can't proceed until we can access the lock.
	pthread_mutex_lock(&factory_reset_lock);
	blog("factory_reset thread got the lock, proceding", NULL, 0);

	// on success, we never return
	value = GPIO_read(GPIO_CONFIGURATION_MODE_CONFIG);

	if(value == 0)
	{
		snprintf(buffer, sizeof(buffer), "/sn/factory_reset.sh  -c");
	}
	else
	{
        status = getCredentials(&creds);
        if(status < 0)
        {
			pthread_mutex_unlock(&factory_reset_lock);
			snprintf(buffer, sizeof(buffer), "factory_reset_thread: database read error: %d", status);
            blog(buffer, NULL, 0);

			snprintf(buffer, sizeof(buffer), "factory_reset_thread: Factroy Reset FAILED");
            blog(buffer, NULL, 0);

			return NULL;
        }

		snprintf(buffer, sizeof(buffer), "/sn/factory_reset.sh  -n");
	}

    blog(buffer, NULL, 0);
	status = system(buffer);

	// we shouldn't have gotten here, something bad happened.
	pthread_mutex_unlock(&factory_reset_lock);

	if(WEXITSTATUS(status) != 0)
	{
		snprintf(buffer, sizeof(buffer), "Factory Default FAIILED: %d - %d", WEXITSTATUS(status), status);
		blog(buffer, NULL, 0);
	}

	return NULL;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void*
configuration_thread(void *data)
{
GPIO_thread_data_t	*pgpio = (GPIO_thread_data_t*) data;
network_state_t		network_state;
uint8_t				command[256];
int					tmode;


	printf("ENTER: configuration_thread: %s, value: %d\n", command, pgpio->current_value);

	getMode(&tmode);



	if(pgpio->current_value == 0)
	{
		printf("configuration_thread: dumping eeprom to /etc/wifi.config\n");
		system("/sn/eeprom -r > /etc/wifi.config");
	}
	else if(pgpio->current_value == 1)
	{
		printf("configuration_thread: cng create_wifi_config() \n");
		create_wifi_config();
	}

	unlink("/etc/network/AP");
	unlink("/etc/network/HOST");
	unlink("/etc/network/EEPROM");

	network_state.value = pgpio->current_value;
	network_state.reboot = 0;

	check_state(&network_state);

	if(pgpio->current_value == GPIO_CONFIGURATION_MODE_CONFIG)
		snprintf((char*) command, sizeof(command), "/sn/ctb_config.sh setup %d", GPIO_CONFIGURATION_MODE_CONFIG);
	else
		snprintf((char*) command, sizeof(command), "/sn/ctb_config.sh setup %d", !tmode);

	// snprintf((char*) command, sizeof(command), "/sn/ctb_config.sh setup %d", pgpio->current_value);

	blog(command, NULL, 0);

	chdir("/sn");

	if(reboot)
		system((char*) command);

	// We are not designed to ever exit from here.
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

int
turn_led_on(uint32_t  led, uint32_t value)
{
char	buffer[256];
char	*formats[GPIO_LED_COUNT] = 
		{
			NULL, NULL, NULL, NULL, 
			"echo %d > /sys/class/gpio/gpio4/value",
			"echo %d > /sys/class/gpio/gpio5/value"
		};

	if(led >= GPIO_LED_COUNT)
		return -1;

	// stop any blinking that may be happening
	led_data[led].active = 0;

	snprintf(buffer, sizeof(buffer), formats[led], !value);
	blog(buffer, NULL, 0);
	system(buffer);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void*
blink_led(void *data)
{
uint32_t			i;
led_control_t		*led=(led_control_t*) data;
int32_t				led_fds[GPIO_LED_COUNT];
int8_t				buffer[256], led_vals[GPIO_LED_COUNT];
char				command[128];
char				*formats[GPIO_LED_COUNT] = 
					{
						NULL, NULL, NULL, NULL, 
						"echo %d > /sys/class/gpio/gpio4/value",
						"echo %d > /sys/class/gpio/gpio5/value"
					};

	snprintf((char*) buffer, sizeof(buffer), "ENTER: blink_led");
	blog(buffer, NULL, 0);

	memset(&led_fds, 0, sizeof(led_fds));
	memset(&led_vals, 0, sizeof(led_vals));

	while(1)
	{
		for(i=0; i<GPIO_LED_COUNT; i++)
		{
			// printf("gpiod: checking led: %d: active: %d\n", i, led[i].active);

			if(led[i].active)
			{
#if 0
				snprintf(buffer, sizeof(buffer), "BLINK LED: [%d], active: %d",
					i, led[i].active);
				blog(buffer, NULL, 0);
#endif
				// get and flip the bits that we will write to the GPIO
				led_vals[i] = ! led_vals[i];

				snprintf((char*) buffer, sizeof(buffer), "BLINK: GPIO[%d], cmd: [%s]", i, command);
				blog(buffer, NULL, 0);

				snprintf(command, sizeof(command), formats[i], led_vals[i]);
				system(command);
#if 0
				blog(command, NULL, 0);
#endif
			}
		}

		usleep(1000*500);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ROUTINE:     configure_services
//
// DESCRIPTION: fetches the services configuration information from database.
//              services will be started/stopped and marked as enabled/
//              disabled based on configuration bytes.
//
// RETURN:      0 on success; non-zero on error
//

static int 
configure_services()
{
    eConnectedTowerEnums device_type;
    int services_count;

    if (access("/etc/WW", F_OK) == 0) {
        device_type = eConnectedTowerCoblation;
        services_count = eFB_Resection_TotalNumberOfServices;
    }
    else if(access("/etc/D2", F_OK) == 0) {
        device_type = eConnectedTowerResection;
        services_count = eFB_Coblation_TotalNumberOfServices;
    }
    else {
        syslog(LOG_ERR, "gpiod: configure services:invalid link device");
        return 1;
    }
    
    //both WW and D2 follow same systemctl process structure.
    LinkServices capitalDeviceLinkServices[eFB_Resection_TotalNumberOfServices] =
    {
        {"osd", false},        // Connected Tower OSD Manager Service
        {"dsc-manager", true}, // Connected Tower Device Manager Service
        {"getSet", true},      // Connected Tower GET/SET Service(Remote control)NOTE: single service process.
        {"triggers",false},    // Connected Tower Trigger Service see trigger enums for Resection
        {"upgraded",true},     // Connected Tower auto upgrade service
        {"setupBlob",false},   // Connected Tower Setup BLOB Service(custom surgeon profile)
        {"deviceBlob",false},  // Connected Tower Device BLOB Service
    };

    uint8_t     buffer[256];
    int         service_conf_from_db;

    service_conf_from_db = get_services_mask(device_type);

    if (service_conf_from_db < 0) {
        snprintf((char*) buffer, sizeof(buffer), "gpiod: configure services value read error from db");
		blog(buffer, NULL, 0);
        return 1;
    }

    for (int i = 0; i < services_count; i++)
 	{
        bool   state = (service_conf_from_db & (0x1 << i));
        char   cmd[128];

        if (capitalDeviceLinkServices[i].isSystemCtlType) 
		{
			snprintf(cmd, sizeof(cmd), "/sn/service-state.sh %s %s", 
				capitalDeviceLinkServices[i].service_unit, 
				state ? "true" : "false");
			blog(cmd, NULL, 0);

            system(cmd);
        }
    }
    return 0;
}

#if defined(MASK_THE_BLUE_LED_PROBLEM)
///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

void*
blue_led_thread(void UNUSED_ATTRIBUTE *v)
{
uint8_t		value=0, buffer[256];

	value = GPIO_read(INTELLIO_CONFIGURATION_GPIO);

	if(value == GPIO_CONFIGURATION_MODE_NORMAL)
		return NULL;

	//
	// Loop forever checking for a 1 in GPIO #4. 
	//

	while(1)
	{
		while(blue_led_controller == 0)
			usleep(1000 * 100);

		snprintf((char*) buffer, sizeof(buffer), "blue_led_thread: blue_led_controller = 1");
		blog(buffer, NULL, 0);

		value = GPIO_read(BRIDGE_LED_BLUE);

		while(value == 0)
		{
			usleep(1000 * 250);
			value = GPIO_read(BRIDGE_LED_BLUE);
		}

		snprintf((char*) buffer, sizeof(buffer), "blue_led_thread: GPIO_read returns %d", value);
		blog(buffer, NULL, 0);

		// got it. turn o nthe Blue LED
		break;
	}	

	snprintf((char*) buffer, sizeof(buffer), "blue_led_thread: turn the Blue LED on");
	blog(buffer, NULL, 0);

	turn_led_on(BRIDGE_LED_BLUE, 1);

	return NULL;
}
#endif

#define USE_AP_CONF 1
#define USE_STATION_CONF 2

static void start_wlan(int conf_type)
{
    if (conf_type == USE_AP_CONF) {
        system("systemctl stop ws_st");
        system("systemctl start ws_ap");
    }
    else if (conf_type == USE_STATION_CONF) {
        system("systemctl stop ws_ap");
        system("systemctl start ws_st");
    }
	system("systemctl start udhcpc");

	// NOTE: it will take a while for wlan0 to be fully up, thus the sleep.
	// The 5 seconds here are a ballpark number that seems working in a first
	// pass test.  It could need more fine tuning.
	// Same applies in stop_wlan()
	system("sleep 5");
}

static void stop_wlan(int conf_type)
{
    if (conf_type == USE_AP_CONF) {
        system("systemctl stop ws_ap");
    }
    else if (conf_type == USE_STATION_CONF) {
        system("systemctl stop ws_st");
    }
	system("systemctl stop udhcpc");
	system("sleep 5");
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:
//
// DESCRIPTION:	
//
//
//

int
main(int argc, char **argv)
{
pthread_attr_t		attrs;
int32_t				tmode, status, i, index, value, running_mode=99;
uint32_t			n, network_status = BRIDGE_NETWORK_DOWN;
pthread_t			tids[GPIOS_TO_MONITOR];
char				filename[256], buffer[256];
network_state_t		network_state;
uint8_t				filter[BROKER_FILTER_MAX];
led_control_t		led;
void				*data, *subhandle;
uint32_t			blue_led_mask = 0;



   if (argc > 1 && !strcmp(argv[1],"-V")) {
       printf("%s\n", version);
       return 0;
   }

#if 0
	value = GPIO_read(BRIDGE_LED_BLUE);
	printf("value before = %d\n", value);
	turn_led_on(BRIDGE_LED_BLUE, 1);
	value = GPIO_read(BRIDGE_LED_BLUE);
	printf("value after= %d\n", value);

	exit(0);
#endif

	pthread_mutex_lock(&factory_reset_lock);
	pthread_attr_init(&attrs);
	pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED);

	pubhandle = bm_publisher();
	subhandle = bm_subscriber();

	bm_subscribe(subhandle, BROKER_BLINK_LED, strlen(BROKER_BLINK_LED));
	bm_subscribe(subhandle, D2_CONNECTED, strlen(D2_CONNECTED));
	bm_subscribe(subhandle, WW_CONNECTED, strlen(WW_CONNECTED));
	bm_subscribe(subhandle, BROKER_REBOOT, strlen(BROKER_REBOOT));
	bm_subscribe(subhandle, PUBSUB_LINK_IN_LIST, strlen(PUBSUB_LINK_IN_LIST));
	bm_subscribe(subhandle, BROKER_DSC_WIFI_STATE, strlen(BROKER_DSC_WIFI_STATE));

	snprintf(buffer, sizeof(buffer), "gpiod: Starting Up");
	blog(buffer, NULL, 0);

	getMode(&g_mode);

	sleep(2);

	memset(&led_data, 0, sizeof(led_data));

	for(i=1; i<argc; i++)
	{
		if(!strcmp(argv[i], "-nb"))
			reboot = 0;
		else if(!strcmp(argv[i], "-v"))
		{
			if(isdigit(argv[i+1][0]))
			{
				index = atoi(argv[++i]);
				if(index >= GPIOS_TO_MONITOR)
				{
					printf("gpio verbosity index >= %d\n", GPIOS_TO_MONITOR);
					exit(1);
				}

				printf("Set gpio %d to verbose\n", index);
				gpio_data[index].verbose = 1;
			}
			else verbose = 1;
		}
	}

    //configure the services to run on this link device
    if (configure_services()) {
        printf("Error configuring features \n");
        return 1;
    }

    //
    // If there is a /data/tmp directory, delete and don't move on until it is deleted
    //

    if(access("/data/tmp", F_OK) >= 0)
    {
        blog((uint8_t*) "rm -rf /data/tmp/*", NULL, 0);
        system("rm -rf /data/tmp/*");
    }

    // 
    // If a factory reset was done and /data/fd2 doesn not exist, create it for the next 
    // time a factory reset is done on the bridge
    //

    if(access("/data/fd2", F_OK) < 0)
    {
        blog((uint8_t*) "Start: cp -rp /data/fd /data/fd2", NULL, 0);

        system("cp -rp /data/fd /data/fd2");

        blog((uint8_t*) "Complete: cp -rp /data/fd /data/fd2", NULL, 0);
    }

	// Now we can unlock the mutex so a factory reset can happen
    blog((uint8_t*) "Unlocking factory_reset_thread. Ready to roll", NULL, 0);
	pthread_mutex_unlock(&factory_reset_lock);

	// 
	// First, we need to check the position of the rear switch to see if it's in the 
	// same position as the last reboot. We do this by comparing the value of the
	// GPIO with the value fr omthe database on how the bridge should use its
	// networking capabilities -- i.e. -- HOST or as an AP.
	//

	// read the value of the GPIO 0
	snprintf((char*) filename, sizeof(filename), "%s/gpio%d/value", GPIO_BASEPATH, 0);
	network_state.value = value = read_value((uint8_t*) filename);

	getMode(&tmode);

	if(value == GPIO_CONFIGURATION_MODE_CONFIG || tmode == MODE_OPERATION_AP_VALUE)
	{
		snprintf(buffer, sizeof(buffer), "gpiod: bring the network UP");
		blog(buffer, NULL, 0);
		start_wlan(USE_AP_CONF);
	}
	else
	{
		snprintf(buffer, sizeof(buffer), "gpiod: bring the network DOWN");
		blog(buffer, NULL, 0);
		stop_wlan(USE_STATION_CONF);
	}

	network_state.reboot = 0;

	check_state(&network_state);

printf("gpiod: main: reboot: %d, value: %d\n", network_state.reboot, network_state.value);

	if(network_state.reboot && reboot)
	{
	uint8_t	command[256];

		snprintf((char*) command, sizeof(command), "/sn/ctb_config.sh setup %d", network_state.value);
		chdir("/sn");
		printf("gpio: system(%s)\n", command);
		system((char*) command);
	}

	if((status=pthread_create(&tids[0], NULL, blink_led, led_data)))
	{
		snprintf(buffer, sizeof(buffer), "Error starting blink_led thread: %d", 
			status);
		blog(buffer, NULL, 0);

		// Should we exit ?
	}

	if((status=pthread_create(&tids[0], NULL, database_monitor, NULL)))
	{
		snprintf(buffer, sizeof(buffer), "Error starting database_monitor thread: %d", 
			status);
		blog(buffer, NULL, 0);

		// Should we exit ?
	}

	// create our threads to monitor specific GPIO's, then forget about them.
	for(i=0; i<GPIOS_TO_MONITOR; i++)
	{
		if(pthread_create(&tids[0], NULL, gpio_dispatcher, &gpio_data[i]))
		{
			snprintf(buffer, sizeof(buffer), "Error starting gpio_dispatcher: %d", 
				status);
			blog(buffer, NULL, 0);

			// Should we exit ?
		}
	}

#if defined(MASK_THE_BLUE_LED_PROBLEM)
	if(pthread_create(&tids[1], NULL, blue_led_thread, NULL))
	{
		snprintf(buffer, sizeof(buffer), "Running in Normal mode");
		blog(buffer, NULL, 0);

		// Shall we exit ... Probably not.
	}
#endif

#if defined(MASK_THE_IP_ADDRESS_PROBLEM)
	if(pthread_create(&tids[1], NULL, ipaddress_monitor, NULL))
	{
	}
#endif

	memset(&led, 0, sizeof(led));
	led.id = BRIDGE_LED_ORANGE;
	led.active = 1;
	memcpy(&led_data[led.id], &led, sizeof(led));

#if defined(DEBUG_WIFI_ISSUE)
	if(pthread_create(&tids[1], NULL, debug_wifi_issue, NULL))
	{
	}
#endif

	if(value == GPIO_CONFIGURATION_MODE_NORMAL)
	{
		running_mode = GPIO_CONFIGURATION_MODE_NORMAL;

		snprintf(buffer, sizeof(buffer), "Running in Normal mode");
		blog(buffer, NULL, 0);

		system("systemctl stop dsc-manager");
		system("systemctl disable dsc-manager");
		system("systemctl stop dsc-standalone-client");
		system("systemctl disable dsc-standalone-client");
		system("systemctl enable dsc-bdm-client");
        system("systemctl start dsc-bdm-client");
        //system("systemctl enable bdm");
        //system("systemctl restart bdm");
	}
	else if(value == GPIO_CONFIGURATION_MODE_CONFIG)
	{
		running_mode = GPIO_CONFIGURATION_MODE_CONFIG;

		snprintf(buffer, sizeof(buffer), "Running in Configuration mode");
		blog(buffer, NULL, 0);

        system("systemctl stop dsc-bdm-client");
        system("systemctl disable dsc-bdm-client");
        system("systemctl enable dsc-manager");
        system("systemctl start dsc-manager");
        system("systemctl enable dsc-standalone-client");
        system("systemctl restart dsc-standalone-client");

		system("systemctl stop osd");
		system("systemctl disable osd");
		
        turn_led_on(BRIDGE_LED_BLUE, 1);

#if defined(MASK_THE_BLUE_LED_PROBLEM)
		blue_led_controller = 1;
#endif
	}

	snprintf((char*) filename, sizeof(filename), "%s/gpio%d/value", GPIO_BASEPATH, 0);
	strcpy(saved_filename, filename);

	// We are not designed to ever exit from here.
	while(1)
	{
		memset(buffer, 0, sizeof(buffer));

		snprintf(buffer, sizeof(buffer), "gpiod: calling bm_receive()");
		blog(buffer, NULL, 0);

		memset((char*) filter, 'X', sizeof(filter));

		status = bm_receive(subhandle, (uint8_t*) filter, (void*) &data, &n, 0);

		snprintf(buffer, sizeof(buffer), "gpiod: got a message: [%s]", filter);
		blog(buffer, NULL, 0);

		if(status < 0) 
		{
			snprintf(buffer, sizeof(buffer), "gpiod: bm_receive ERROR: %d", status);
			blog(buffer, NULL, 0);
			continue;
		}

		if(!strncmp((char*) filter, BROKER_DSC_WIFI_STATE, strlen(BROKER_DSC_WIFI_STATE)))
        {
            uint8_t *state = data;
            snprintf(buffer, sizeof(buffer), "WIFI state message received:%d", *state);
            blog(buffer, NULL, 0);
            if (*state == 0)
                network_status = BRIDGE_NETWORK_DOWN;
            else
                network_status = BRIDGE_NETWORK_UP;
        }
        else if(!strncmp((char*) filter, PUBSUB_LINK_IN_LIST, strlen(PUBSUB_LINK_IN_LIST)))
		{
		uint8_t		*link_in_list = data;

			if(*link_in_list == 0)
				blue_led_mask &= ~LINK_IN_DISCOVERY_LIST;
			else
				blue_led_mask |= LINK_IN_DISCOVERY_LIST;
		}
		else if(!strncmp((char*) filter, D2_CONNECTED, strlen(D2_CONNECTED)) 
			|| !strncmp((char*) filter, WW_CONNECTED, strlen(WW_CONNECTED)))
		{
		uint8_t		*connected = data;

			// read the value of the GPIO 0
		    value = read_value((uint8_t*) filename);

			snprintf(buffer, sizeof(buffer), "gpiod: connected = %d, gpio value = %d", *connected, value);
			blog(buffer, NULL, 0);

			if(*connected == 0)
			{
				printf("D2_CONNECTED before: %d\n", blue_led_mask);
				blue_led_mask &= ~CAPITAL_DEVICE_CONNECTED;
				printf("D2_CONNECTED before: %d\n", blue_led_mask);

				if(value == GPIO_CONFIGURATION_MODE_NORMAL)
				{
					snprintf(buffer, sizeof(buffer), "gpiod: No capital device: Bring the network down");
					blog(buffer, NULL, 0);

					if(network_status != BRIDGE_NETWORK_DOWN)
					{
						stop_wlan(USE_STATION_CONF);
					}

					network_status = BRIDGE_NETWORK_DOWN;
				}

				if(running_mode == GPIO_CONFIGURATION_MODE_NORMAL)
					turn_led_on(BRIDGE_LED_BLUE, 0);

				memset(&led, 0, sizeof(led));
				led.id = BRIDGE_LED_ORANGE;
				led.active = 1;
				memcpy(&led_data[led.id], &led, sizeof(led));
			}
			else if(*connected == 1)
			{
#if 0
				snprintf(buffer, sizeof(buffer), "D2_CONNECTED before: %d", blue_led_mask);
				blog(buffer, NULL, 0);
#endif

				blue_led_mask |= CAPITAL_DEVICE_CONNECTED;

#if 0
				snprintf(buffer, sizeof(buffer), "D2_CONNECTED after: %d", blue_led_mask);
				blog(buffer, NULL, 0);
#endif

				if(value == GPIO_CONFIGURATION_MODE_NORMAL)
				{
					snprintf(buffer, sizeof(buffer), "gpiod: Bring the network up");
					blog(buffer, NULL, 0);

					if(network_status == BRIDGE_NETWORK_DOWN)
					{
						start_wlan(USE_STATION_CONF);
					}

					network_status = BRIDGE_NETWORK_UP;
				}

				printf("gpiod: connected = 1\n");
				turn_led_on(BRIDGE_LED_ORANGE, 1);
			}
			else
			{
					snprintf(buffer, sizeof(buffer), "gpiod: unknown value for connectde: %d", *connected);
					blog(buffer, NULL, 0);
			}
		}
		else if(!strncmp((char*) filter, BROKER_BLINK_LED, strlen(BROKER_BLINK_LED)))
		{
			// blink = data;

			// snprintf(buffer, sizeof(buffer), "Got a %s message: %d %d %d %d", filter, blink->id, blink->active, blink->action, blink->state);
			// blog(buffer, NULL, 0);

			// blink->active = 1;
			// memcpy(&led_data[blink->id], blink, sizeof(led_control_t));
		}
		else if(!strncmp((char*) filter, BROKER_REBOOT, strlen(BROKER_REBOOT)))
		{
		    int		tmode;
		    char	command[256];

			getMode(&tmode);

			snprintf(buffer, sizeof(buffer), "gpiod: processing BROKER_REBOOT: tmode = %d,", tmode);
			blog(buffer, NULL, 0);

			create_wifi_config();

			system("rm -f /etc/network/AP");
			system("rm -f /etc/network/HOST");

			if(value != GPIO_CONFIGURATION_MODE_CONFIG)
			{
				if(g_mode != tmode)
				{
					if(tmode == MODE_OPERATION_AP_VALUE)
						system("touch /etc/network/AP");
					else if(tmode == MODE_OPERATION_HOST_VALUE)
						system("touch /etc/network/HOST");
				}
			}

			snprintf((char*) filename, sizeof(filename), "%s/gpio%d/value", GPIO_BASEPATH, 0);
			value = read_value((uint8_t*) filename);

			if(value == GPIO_CONFIGURATION_MODE_CONFIG)
				snprintf(command, sizeof(command), "/sn/ctb_config.sh setup %d", GPIO_CONFIGURATION_MODE_CONFIG);
			else
				snprintf(command, sizeof(command), "/sn/ctb_config.sh setup %d", !tmode);

			blog(command, NULL, 0);

			chdir("/sn");
			syslog(LOG_ERR, "gpiod: processing broker_reboot msg by instantiating: %s", command);
			system(command);
		}
		else
		{
			printf("gpiod: uknown command\n");
		}

		free(data);

		if(running_mode == GPIO_CONFIGURATION_MODE_NORMAL)
		{
            snprintf(buffer, sizeof(buffer), "blue_led_mask: %d", blue_led_mask);
            blog(buffer, NULL, 0);
			
            if(blue_led_mask == 0 || network_status == BRIDGE_NETWORK_DOWN)
			{
				turn_led_on(BRIDGE_LED_BLUE, 0);
			}
			else if(blue_led_mask == 3)
			{
				turn_led_on(BRIDGE_LED_BLUE, 1);
			}
			else if(blue_led_mask == CAPITAL_DEVICE_CONNECTED)
			{
				memset(&led, 0, sizeof(led));
				led.id = BRIDGE_LED_BLUE;
				led.active = 1;
				memcpy(&led_data[led.id], &led, sizeof(led));
			}
		}
	}
}



	

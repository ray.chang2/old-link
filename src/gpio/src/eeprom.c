

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/syslog.h>
#include <sys/stat.h>

#include "ctb_db_table_api.h"
#include "i2c-dev.h"

static const char* version = "01.00.00"; //follows major.minor.patch scheme(https://semver.org)

#define I2C_VERSION         "0.10"

#define I2C_READ            1
#define I2C_WRITE           2

#define I2C_PAGE_SIZE       8
#define I2c_TOTAL_BYTES     128


uint32_t    verbose = 0;


///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//

int
program_eeprom(char *device, char *buffer)
{
int     		n=0, status, fd, index=0, addr=0;
unsigned int	i;


    fd = open(device, O_RDWR);
    if(fd < 0)
    {
        perror("open");
        close(fd);
        return -1;
    }

    if (ioctl(fd, I2C_SLAVE, 0x50) < 0) 
    {
        perror("ioctl");
        close(fd);
        return -1;
    }

    for(i=0; i<strlen((char*) buffer)/(I2C_PAGE_SIZE - 1) + 1; i++)
    {
        n = (strlen((char*) &buffer[index]) >= I2C_PAGE_SIZE ? 7 : strlen((char*) &buffer[index]));

        status = i2c_smbus_write_block_data(fd, addr, 7, (unsigned char *) &buffer[index]);

        if(status < 0)
        {
            perror("i2c_smbus_write_block_data");
            printf("i2c_smbus_write_block_data error: status: %d, addr: %d\n",
                status, addr);
            return -1;
        }

        //
        // Need to sleep for more than 800ms. So I chose 1000ms.
        // We get erros otherwise -- device not ready 
        // It's a timing issue as to when the EEPROM is ready again after a 
        // read or write.
        //
 
        sleep(1);

        if(verbose)
            printf("i2c write: addr: %d, n: %d, index: %d, status: %d\n", 
                addr, n, index, status);

        addr += I2C_PAGE_SIZE;
        index += I2C_PAGE_SIZE - 1;
    }

    memset(buffer, 0, strlen((char*) buffer));

    status = i2c_smbus_write_block_data(fd, addr, 7, (unsigned char *) &buffer[index + 0]);

    if(status < 0)
    {
        printf("i2c_smbus_write_block_data error: status: %d, addr: %d\n",
            status, addr);

        close(fd);
        return -1;
    }

    if(verbose)
        printf("FINAL/Z i2c write: addr: %d, n: %d, index: %d, status: %d\n", 
            addr, n, index, status);

    close(fd);

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

char *
read_eeprom(char *device)
{
    char nullbyte=0, *buffer, buf[32];
    int  status, fd, i, index=0, addr=0;
    char tmp_buffer[I2c_TOTAL_BYTES];
    char out_buffer[512];

    buffer = calloc(1, I2c_TOTAL_BYTES);
    if(buffer == NULL)
            return NULL;

    fd = open(device, O_RDWR);
    if(fd < 0)
    {
        perror("open");
        printf("open failed: %x\n", fd );
        return NULL;
    }

    if (ioctl(fd, I2C_SLAVE, 0x50) < 0) 
    {
        close(fd);
        perror("ioctl");
        return NULL;
    }

    while(1)
    {
        status = i2c_smbus_read_block_data(fd, addr, (unsigned char *) buf);

        if(status < 0)
        {
            printf("i2c_smbus_read_block_data error: %d, addr: %d\n",
                    status, addr);
            perror("i2c_smbus_read_block_data");
            close(fd);
            return NULL;
        }

        memcpy(&buffer[index], buf, status);

        for(i=0; i<status; i++)
        {
            if(buf[i] == 0)
            {
                nullbyte = 1;
                break;
            }
        }

        if(verbose)
            printf("i2c read: addr: %d, index: %d, status: %d\n", 
                addr, index, status);

        addr += I2C_PAGE_SIZE;
        index += status;

        if(nullbyte) 
            break;

        sleep(1);
    }

    close(fd);

    strcpy(tmp_buffer,buffer);

#if 0
    printf("read_eeprom complete:\n\n");
#endif
    char *sn = NULL, *pd = NULL, *sku = NULL, *pn = NULL, *rev = NULL, *next;
    out_buffer[0] = '\0';
    next = tmp_buffer;
    //format of eeprom data is sn;pd;sku;pn;rev
    for (i = 0; i < 4; i++) { 
        char *token;
        if ((token = strchr(next, ';')) != NULL) {
            switch(i) {
                case 0:
                    *token = '\0';
                    next = ++token;
                    sn = tmp_buffer;
                    pd = next;
                    break;
                case 1:
                    *token = '\0';
                    next = ++token;
                    sku = next;
                    break;
                case 2:
                    *token = '\0';
                    next = ++token;
                    pn = next;
                    break;
                case 3:
                    *token = '\0';
                    next = ++token;
                    rev = next;
                    break;
            }
        } 
    }

    if (verbose)
    {
        if (sn && !strlen(sn))
            printf("serial number is empty\n");
        else if (pd && !strlen(pd))
            printf("password is empty\n");
        else if (sku && !strlen(sku))
            printf("sku is empty\n");
        else if (pn && !strlen(pn))
            printf("assembly PN is empty\n");
        else if (rev && !strlen(rev))
            printf("assembly rev is empty\n");
    }

    if (sn != NULL && pd != NULL && sku != NULL && pn != NULL && rev != NULL) {
        //sync db elements that are tracked with eeprom contents
        int rc = set_devinfo_sku(sku, strlen(sku));
        rc = set_devinfo_serial(sn, strlen(sn));
        if (rc && verbose) printf("error writing sku/sn into db");
        snprintf(out_buffer, sizeof(out_buffer), "SSID=%s\nPASS=%s\nSKU=%s\nAssemblyPN=%s\nAssemblyRev=%s\n", sn, pd, sku, pn, rev);
        printf("%s\n", out_buffer);
    }
    else {
        printf("%s\n",buffer);
    }

    return buffer;
}

/**
 * @fn      void usage(void)
 * @brief   how to use this application (currently not used)
 * @param   n/a
 * @retval  n/a
 **************************************************************/
static void usage(void)
{
   printf("Usage:\n");
   printf("./eeprom -V displays version number\n");
   printf("./eeprom [-h] displays this usage message\n");
   printf("./eeprom -v use for verbose\n");
   printf("./eeprom [-v]"
          "-l <d2|ww> | -k <8-digitSKU>"
          "-s <7char serial number -abc1234>"
          "-p <alphaNumeric-8 chars>"
          "-a <8-digit assemblyPN>"
          "-R<2-letter assemblyRev>}\n");
   printf("./eeprom [-v] -f <file name> [-d <eeprom-path>] reads from file and writes into eeprom device\n");
   printf("./eeprom [-v] [-r] [-d <eeprom-path>] reads from eeprom device path\n");
   printf("\n");
   printf("additonal help:\n");
   printf("if -l <d2|ww> option is specified, predefined sku and serial prefix will be selected\n");
   printf("   no need to specify sku argument and only 4 digit serial number is enough\n");
   printf("\n");
   printf("Use the below example line which listed all mandatory params to update eeprom\n");
   printf("you can change values as you may wish, but follow same length as example\n");
   printf("either you can put a space between param and value or not, both work.\n");
   printf("\n");
   printf("\n");
   printf("./eeprom -ksku12345 -sd2V1234 -p123456AB -a55555555 -RVV \n");
   printf("\n");
   printf("or \n");
   printf("\n");
   printf("./eeprom -k sku12345 -s d2V1234 -p 123456AB -a 55555555 -R VV \n");
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//

#define LINK_ARG_LENGTH 2
#define SERIAL_ARG_LENGTH 4
#define SKU_ARG_LENGTH 8
#define PASSWORD_ARG_LENGTH 8
#define SERIAL_PREFIX_LENGTH 3
#define MAX_EEPROM_BYTES 129
#define ASSEMBLY_PN_ARG_LENGTH 8
#define ASSEMBLY_REV_ARG_LENGTH 2

static const char* sku_d2 = "72205189";
static const char* sku_ww = "72205191";
static const char* assembly_pn_fixed = "90509983";

int
main(int argc, char **argv)
{
    int32_t fd=0, op=0;
    int c;
    char *device = "/dev/i2c-0";
    char filename[256];
    char link[LINK_ARG_LENGTH + 1];
    char serial_number[SERIAL_PREFIX_LENGTH + SERIAL_ARG_LENGTH + 1];
    char password[PASSWORD_ARG_LENGTH + 1];
    char sku[SKU_ARG_LENGTH + 1];
    char assembly_rev[ASSEMBLY_REV_ARG_LENGTH + 1];
    char assembly_pn[ASSEMBLY_PN_ARG_LENGTH + 1];

    if (argc > 1 && !strcmp(argv[1],"-V")) {
        printf("%s\n", version);
        return 0;
    }
    filename[0] = '\0';
    link[0] = '\0';
    sku[0] = '\0';
    serial_number[0] = '\0';
    password[0] = '\0';
    assembly_pn[0] = '\0';
    assembly_rev[0] = '\0';

    op = I2C_WRITE;
    strcpy(assembly_pn, assembly_pn_fixed);

    while ((c = getopt(argc, argv, "l:k:s:p:a:R:f:d:hvVr")) != -1) {
        switch (c) {
        case 'l':
            snprintf(link, sizeof(link), "%s", optarg);
            printf("link type:%s arg value:%s\n", link, optarg);
            break;
        case 'k':
            snprintf(sku, sizeof(sku), "%s", optarg);
            printf("sku:%s arg value:%s\n", sku, optarg);
            break;
        case 's':
            if (!strcmp(link, "d2")) {
                snprintf(serial_number, sizeof(serial_number), "FFP%s", optarg);
                strcpy(sku, sku_d2);
            }
            else if(!strcmp(link, "ww")) {
                snprintf(serial_number, sizeof(serial_number), "FFR%s", optarg);
                strcpy(sku, sku_ww);
            }
            else {
                snprintf(serial_number, sizeof(serial_number), "%s", optarg);
                printf("serial_number:%s arg value:%s\n", serial_number, optarg);
            }
            break;
        case 'p':
            snprintf(password, sizeof(password), "%s", optarg);
            printf("password:%s arg value:%s\n", password, optarg);
            break;
        case 'a':
            snprintf(assembly_pn, sizeof(assembly_pn), "%s", optarg);
            printf("assembly-pn:%s arg value:%s\n", assembly_pn, optarg);
            break;
        case 'R':
            snprintf(assembly_rev, sizeof(assembly_rev), "%s", optarg);
            printf("assembly-rev:%s arg value:%s\n", assembly_rev, optarg);
            break;
        case 'f':
            strcpy(filename, optarg);
            break;
        case 'd':
            strcpy(device, optarg);
            break;
        case 'h':
            usage();
            return 0;
        case 'v':
            verbose = 1;
            break;
        case 'V':
            printf("%s\n",version);
            return 0;
        case 'r':
            op = I2C_READ;
            break;
        default:
            printf("Unknown option: -%c\n", c);
            return 1;
        }
    }

    if(op == 0)
    {
        printf("error: need a read/write operation\n");
        exit(1);
    }

    if(verbose)
        printf("i2c: version: %s,  device = %s\n", I2C_VERSION, device);

    if(op == I2C_READ)
    {
        char    *buffer;

        buffer = read_eeprom((char*) device);

        if(buffer)
        {
            if(verbose)
            {
                printf("read_eeprom returns %d bytes\n", (int) strlen((char*) buffer));
                printf("\n Buffer: \n%s\n", buffer);
                int rc;
                char skudb[20];
                rc = get_devinfo_sku(skudb, 20);
                if (rc != 0) printf("error reading sku from database, err:%d\n", rc);
                skudb[8] = '\0';
                rc = get_devinfo_serial(serial_number, 20);
                if (rc != 0)
                    printf("error reading serial number from database, err:%d\n", rc);
                else {
                    serial_number[7] = '\0';
                    printf("sku from database:%s\n", skudb);
                    printf("serial number from database:%s\n", serial_number);
                }
            }
        }
        else
            printf("read_eeprom failed\n");
    }
    else if(op == I2C_WRITE)
    {
        if (!strlen(filename)) {
            char ee_buffer[256];
            //order of eeprom data is SN;PW;SKU:APN:AREV
            snprintf(ee_buffer, MAX_EEPROM_BYTES, "%s;%s;%s;%s;%s", serial_number, password, sku, assembly_pn, assembly_rev);
            printf("eeprom write %d bytes:[%s]\n", strlen(ee_buffer), ee_buffer);
            if(!program_eeprom((char*) device, (char*) ee_buffer)) {
                //on suucessful write to eeprom, update database fields now
                int rc;
                printf("writing sku into database:%s\n", sku);
                printf("writing serial number into database:%s\n", serial_number);
                rc = set_devinfo_sku(sku, strlen(sku));
                if (rc != 0) printf("error writing sku into database\n");
                rc = set_devinfo_serial(serial_number, strlen(serial_number));
                if (rc != 0)
                    printf("error writing serial number into database\n");
                else
                    printf("sku and serial number written successfully into database:\n");
            }

            return 0;
        }
        struct  stat    s;
        int     n, ifd;
        char    *buffer = NULL;

        if(stat(filename, &s) < 0)
        {
            printf("Could not locate input file: %s\n", filename);
            perror("stat");
            exit(1);
        }

        buffer = calloc(1, s.st_size + 16);
        if(buffer == NULL)
        {
            printf("Could not allocate memory for input file: %s\n", filename);
            perror("malloc");
            exit(1);
        }

        ifd = open(filename, O_RDONLY);
        if(ifd < 0)
        {
            printf("Could not read input file: %s\n", filename);
            perror("open");
            exit(1);
        }

        if((n=read(ifd, buffer, s.st_size)) < 0)
        {
            printf("error reading input file: %s\n", filename);
            perror("open");
            exit(1);
        }

        close(ifd);

        program_eeprom((char*) device, (char*) buffer);

    }

    close(fd);
}








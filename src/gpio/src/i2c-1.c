
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/syslog.h>


#include    "i2c-dev.h"



#define I2C_READ        1
#define I2C_WRITE       2
#define I2C_BLOCK_READ  3
#define I2C_BLOCK_WRITE 4


int
main(int argc, char **argv)
{
int32_t     addr=0, word, fd, i, status, op=0, nbytes, offset;
uint8_t     bytes[256], ch=0, *device="/dev/i2c-0";

    memset(bytes, 0, sizeof(bytes));

    snprintf(bytes, sizeof(bytes), "%s", "xxxxyyyyzzzz");

    for(i=1; i<argc; i++)
    {
        if(!strcmp(argv[i], "-r"))
            op = I2C_READ;
        else if(!strcmp(argv[i], "-a"))
            addr = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-bw"))
            op = I2C_BLOCK_WRITE;
        else if(!strcmp(argv[i], "-w"))
            op = I2C_WRITE;
        else if(!strcmp(argv[i], "-s"))
            memcpy(bytes, argv[++i], strlen(argv[i]));
        else if(!strcmp(argv[i], "-d"))
            device = argv[++i];
        else if(!strcmp(argv[i], "-c"))
            ch = argv[++i][0];
        else
        {
            printf("%s; bd parameter\n", argv[i]);
            exit(1);
        }
    }

    if(op == 0)
    {
        printf("error: need a read/write operation\n");
        exit(1);
    }

    printf("i2c: device = %s\n", device);

    fd = open(device, O_RDWR);
    if(fd < 0)
    {
        perror("open");
        printf("open failed: %x\n", fd );
        exit(2);
    }

    // if (ioctl(fd, I2C_TENBIT, 1) < 0) 
    // {
        // perror("ioctl");
        // exit(1);
    // }

    if (ioctl(fd, I2C_SLAVE, 0x50) < 0) 
    {
        perror("ioctl");
        exit(1);
    }

    if(op == I2C_READ)
    {
        // ch = i2c_smbus_read_byte(fd);
        word = i2c_smbus_read_word_data(fd, addr);
        printf("Read Byte: status: %d, addr: %d, byte: %x\n", 
            status, addr, word);

        addr += 2;
        word = i2c_smbus_read_word_data(fd, addr);
        printf("Read Byte: status: %d, addr: %d, byte: %x\n", 
            status, addr, word);
    
        addr += 2;
        word = i2c_smbus_read_word_data(fd, addr);
        printf("Read Byte: status: %d, addr: %d, byte: %x\n", 
            status, addr, word);
    
        addr += 2;
        word = i2c_smbus_read_word_data(fd, addr);
        printf("Read Byte: status: %d, addr: %d, byte: %x\n", 
            status, addr, word);
    
        // addr = 0;
        // status = write(fd, &addr, 1);
        // printf("Write Addr returns: %d\n", status);

        // uint32_t    w;
        // ch = 0;
        // status = read(fd, &w, 4);
        // printf("Read Byte: status: %d, byte: %x\n", status, ch);
    }
    else if(op == I2C_WRITE)
    {
  
#if 0
        // status = i2c_smbus_write_byte(fd, ch);

        word = (uint32_t) bytes & 0xffff;
        status = i2c_smbus_write_word_data(fd, addr, word);
        printf("i2c_smbus_write_word_data returns: %d\n", status);

        if(status < 0)
            perror("i2c_smbus_write_block_data");

        if (ioctl(fd, I2C_SLAVE, 0x50) < 0) 
        {
            perror("ioctl");
            exit(1);
        }

        addr += 2;
        word = (uint32_t) bytes & 0xffff0000 >> 16;
        status = i2c_smbus_write_word_data(fd, addr, word >> 16);
        printf("i2c_smbus_write_word_data returns: %d\n", status);

        if(status < 0)
            perror("i2c_smbus_write_block_data");
#else
        addr = 0;
        status = write(fd, bytes, 4);
        printf("Write Addr returns: %d\n", status);

        if(status < 0)
            perror("i2c_smbus_write_block_data");

        close(fd);

        usleep(200000);

        fd = open(device, O_RDWR);
        if(fd < 0)
        {
            perror("open");
            printf("open failed: %x\n", fd );
            exit(2);
        }

        if (ioctl(fd, I2C_SLAVE, 0x50) < 0) 
        {
            perror("ioctl");
            exit(1);
        }

        status = write(fd, bytes, 4);
        printf("Write Byte returns: %d\n", status);

        if(status < 0)
            perror("i2c_smbus_write_block_data");
#endif
    }
    else if(op == I2C_BLOCK_WRITE)
    {
        if (ioctl(fd, I2C_SLAVE, 0x50) < 0) 
        {
            perror("ioctl");
            exit(1);
        }

        status = i2c_smbus_write_block_data(fd, addr, 7, bytes);
        printf ("i2c_smbus_write_block_data returns: %d\n", status);

        if(status < 0)
            perror("i2c_smbus_write_block_data");

#if 0
        usleep(300000);

        addr += 8;
        status = i2c_smbus_write_block_data(fd, addr, 8, &bytes[8]);
        printf ("i2c_smbus_write_block_data returns: %d, fd: %d\n", status, fd);

        if(status < 0)
            perror("i2c_smbus_write_block_data");
#endif
    }

    close(fd);
}










#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/syslog.h>
#include <sys/stat.h>


#include    "i2c-dev.h"



#define I2C_READ            1
#define I2C_WRITE           2

#define I2C_PAGE_SIZE       8
#define I2c_TOTAL_BYTES     128


uint32_t    verbose = 0;


///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//

int
program_eeprom(char *device, char *buffer)
{
int     length, n, status, fd, index=0, i, addr=0;
char    buf[I2C_PAGE_SIZE];


    fd = open(device, O_RDWR);
    if(fd < 0)
    {
        perror("open");
        printf("open failed: %x\n", fd );
        exit(2);
    }

    if (ioctl(fd, I2C_SLAVE, 0x50) < 0) 
    {
        perror("ioctl");
        exit(1);
    }

    for(i=0; i<strlen(buffer)/(I2C_PAGE_SIZE - 1) + 1; i++)
    {
        n = (strlen(&buffer[index]) >= I2C_PAGE_SIZE ? 7 : strlen(&buffer[index]));

        status = i2c_smbus_write_block_data(fd, addr, 7, &buffer[index]);

        if(status < 0)
        {
            printf("i2c_smbus_write_block_data error: status: %d, addr: %d\n",
                status, addr);
            perror("i2c_smbus_write_block_data");

            exit(1);
        }

        sleep(1);

        if(verbose)
            printf("i2c write: addr: %d, n: %d, index: %d, status: %d\n", 
                addr, n, index, status);

        addr += I2C_PAGE_SIZE;
        index += I2C_PAGE_SIZE - 1;
    }

    memset(buffer, 0, strlen(buffer));

    status = i2c_smbus_write_block_data(fd, addr, 8, buffer);

    if(status < 0)
    {
#if 0
        printf("i2c_smbus_write_block_data error: status: %d, addr: %d\n",
            status, addr);
#endif

        close(fd);
        exit(1);
    }

#if 0
    printf("FINAL/Z i2c write: addr: %d, n: %d, index: %d, status: %d\n", 
        addr, n, index, status);
#endif

    close(fd);

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

char *
read_eeprom(char *device)
{
char        nullbyte=0, *buffer, buf[32];
int         status, fd, i, index=0, addr=0;


    buffer = calloc(1, I2c_TOTAL_BYTES);
    if(buffer == NULL)
            return NULL;

    fd = open(device, O_RDWR);
    if(fd < 0)
    {
        perror("open");
        printf("open failed: %x\n", fd );
        return NULL;
    }

    if (ioctl(fd, I2C_SLAVE, 0x50) < 0) 
    {
        perror("ioctl");
        return NULL;
    }

    while(1)
    {
        status = i2c_smbus_read_block_data(fd, addr, buf);

        if(status < 0)
        {
            printf("i2c_smbus_read_block_data error: %d, addr: %d\n",
                    status, addr);
            perror("i2c_smbus_read_block_data");
            close(fd);
            return NULL;
        }

        memcpy(&buffer[index], buf, status);

        for(i=0; i<status; i++)
        {
            if(buf[i] == 0)
            {
                nullbyte = 1;
                break;
            }
        }

        if(verbose)
            printf("i2c read: addr: %d, index: %d, status: %d\n", 
                addr, index, status);

        addr += I2C_PAGE_SIZE;
        index += status;

        if(nullbyte) 
            break;

        sleep(1);
    }

    close(fd);

#if 0
    printf("read_eeprom complete:\n\n");
    printf("%s\n", buffer);
#endif

    return buffer;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//

int
main(int argc, char **argv)
{
int32_t     addr=0, word, fd, i, status, op=0, nbytes, offset;
uint8_t     bytes[256], ch=0, *device="/dev/i2c-0";
char        *filename;

    memset(bytes, 0, sizeof(bytes));

    snprintf(bytes, sizeof(bytes), "%s", "xxxxyyyyzzzz");

    for(i=1; i<argc; i++)
    {
        if(!strcmp(argv[i], "-r"))
            op = I2C_READ;
        else if(!strcmp(argv[i], "-w"))
            op = I2C_WRITE;
        else if(!strcmp(argv[i], "-f"))
            filename = argv[++i];
        else if(!strcmp(argv[i], "-a"))
            addr = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-v"))
            verbose = 1;
        else if(!strcmp(argv[i], "-d"))
            device = argv[++i];
        else
        {
            printf("%s; bd parameter\n", argv[i]);
            exit(1);
        }
    }

    if(op == 0)
    {
        printf("error: need a read/write operation\n");
        exit(1);
    }

    if(verbose)
        printf("i2c: device = %s\n", device);

    if(op == I2C_READ)
    {
        char    *buffer;
        
        buffer = read_eeprom(device);

        if(buffer)
        {
            if(verbose)
            {
                printf("read_eeprom returns %d bytes\n", strlen(buffer));
                printf("\n Buffer: \n%s\n", buffer);
            }
        }
        else
            printf("read_eeprom failed\n");
    }
    else if(op == I2C_WRITE)
    {
    struct  stat    s;
    int     n, ifd;
    char    *buffer = NULL;

        if(stat(filename, &s) < 0)
        {
            printf("Could not locate input file: %s\n", filename);
            perror("stat");
            exit(1);
        }

        buffer = calloc(1, s.st_size + 16);
        if(buffer == NULL)
        {
            printf("Could not allocate memory for input file: %s\n", filename);
            perror("malloc");
            exit(1);
        }

        ifd = open(filename, O_RDONLY);
        if(ifd < 0)
        {
            printf("Could not read input file: %s\n", filename);
            perror("open");
            exit(1);
        }

        if((n=read(ifd, buffer, s.st_size)) < 0)
        {
            printf("error reading input file: %s\n", filename);
            perror("open");
            exit(1);
        }

        close(ifd);

        program_eeprom(device, buffer);

#if 0
        usleep(300000);

        addr += 8;
        status = i2c_smbus_write_block_data(fd, addr, 8, &bytes[8]);
        printf ("i2c_smbus_write_block_data returns: %d, fd: %d\n", status, fd);

        if(status < 0)
            perror("i2c_smbus_write_block_data");
#endif
    }

    close(fd);
}











// GPIO numbers for LED's on the bridge
#define BRIDGE_LED_BLUE				4
#define BRIDGE_LED_ORANGE			5

// GPIO numbers for buttons on the bridge
#define INTELLIO_CONFIGURATION_GPIO     0
#define INTELLIO_FACTORY_RESET_GPIO     1


#define	GPIO_BASEPATH	"/sys/class/gpio"

#define	GPIOS_TO_MONITOR				2

#define	GPIO_0							gpio0
#define	GPIO_1							gpio1

#define	GPIO_CONFIGURATION_MODE_CONFIG		0
#define	GPIO_CONFIGURATION_MODE_NORMAL		1



typedef struct  _led_control
{
	uint8_t		id:3;       // led GPIO number
	uint8_t		active:1;   // Are we actively monitoring this GPIO
	uint8_t		value:1;
} led_control_t;




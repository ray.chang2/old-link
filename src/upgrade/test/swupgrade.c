#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "swupgrade.h"

cJSON* load_configuration(char* jsonFileName)
{ 
  int fd = open(jsonFileName, O_RDONLY);
  if (fd < 0) {
    printf(" couldn't open the file for reading\n");
    return NULL;
  }
  ssize_t size;
  struct stat st;
  stat(jsonFileName, &st);
  size = st.st_size;
  char* input = (char*) malloc(size);
  if (NULL == input) {
    printf("could't allocate memory for input buffer\n");
    return NULL;
  }
  size= read(fd, input, size);
  if (size <=0) {
    printf(" couldn't read the file\n");
    return NULL;
  }
 
  cJSON* json = cJSON_Parse(input);
  return json;
}

int test_loaded_configuration(cJSON* json)
{
  return 0;
}

int main(int argc , char* argv[])
{
  printf(" swupgrade json test\n");
  if (argc < 2) {
    printf("usage ./swug <json file>\n");
    exit(1); 
  }
  char* filename = argv[1];
  if (!strlen(filename)) {
    printf("swupgrade: provide json configuration input file\n");
    exit(1);
  }

  if (access(filename, R_OK) < 0) {
    printf("swupgrade: can not open json file for reading\n");
    perror(strerror(errno));
  }

  cJSON* json = load_configuration(filename);
  if (NULL == json) {
    printf("swupgrade: json file parse error\n");
    exit(1);
  }
  
  char* string = cJSON_Print(json);
  if (NULL == string) {
    printf("swupgrade: failed to print parsed json.\n");
  }
  printf ("parsed json result:\n");
  printf ("%s",string);
  cJSON_Delete(json);
}


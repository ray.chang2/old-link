//build
> make

//run
> ./swug <json file>

Currently it prints out back the entire parsed json file.

Depending on the need further APIs can be written to pass
already opened cJSON structure pointer and use other cJSON APIs
to fetch the required information for each module in each group
as part of upgrade process and for all the groups from the
given upgrade json configuration file.

d2.json and lens4k.json files are sample configurations files 
in this test directory.
 


#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


// mmcroot=/dev/mmcblk1p1 rootwait rw

// #define	BLOCK_DEVICE		"/tmp/xxx"
#define	BLOCK_DEVICE		"/dev/mmcblk1"

#define	START_BLOCK			0
#define	BLOCK_COUNT			2048
#define	BLOCK_SIZE			512

// #define	START_BLOCK			1792
// #define	BLOCK_COUNT			7
// #define	BLOCK_SIZE			512



int
main(int argc, char **argv)
{
int			i, j, n, fd, found=0;
char		buffer[BLOCK_COUNT * BLOCK_SIZE];
char		*lhs, *rhs;			// left hand side, right hand side


	if(argc < 3)
	{
		printf("usage:  setenv <variable>  <value>\n");
		exit(-1);
	}

	lhs = argv[1];
	rhs = argv[2];

	if((fd = open(BLOCK_DEVICE, O_RDWR)) < 0)
	{
		perror("open error");
		exit(-1);
	}

	memset(buffer, 0xad, BLOCK_COUNT * BLOCK_SIZE);

	if((j = lseek(fd, START_BLOCK * BLOCK_SIZE, SEEK_SET)) < 0)
	{
		perror("lseek error");
		exit(-1);
	}

	printf("lseek to offset: %d\n", j);

	j = BLOCK_SIZE * BLOCK_COUNT;

	n = read(fd, buffer, BLOCK_SIZE * BLOCK_COUNT);

	if( n != sizeof(buffer))
	{
		printf("error: did not read entire buffer: expected: %d bytes, got %d\n", (int) sizeof(buffer), n);
		exit(-1);
	}

	//
	// search for the string in our buffer
	//

	i = 0;

	while(i < BLOCK_SIZE * BLOCK_COUNT)
	{
		// search for the for character in buffer that has the sams startng letter
		while((i < BLOCK_COUNT * BLOCK_SIZE) && buffer[i] != lhs[0])
			i++;

		// did we find one ?
		if(lhs[0] != buffer[i])
		{
			i++;
			continue;
		}

		// We found the 1st character. See if the rest of them match
		if(memcmp(&buffer[i], lhs, strlen(lhs)))
		{
			i++;
			continue;
		}

		// now make sure this where 'lhs' is defined, and not referenced.
		// we need to find where t is defined. So look to an '=' at the end of thestring
		if(buffer[i + strlen(lhs)] != '=')
		{
			i++;
			continue;
		}

		// if we got here then we found the location whee 'lghs' is defined
		// so lets modify it amd write out the buffer back into the file
		j = i;
		i += strlen(lhs);
		i++;

		printf("Offset: %d:   Changing %s ===> ", j, lhs);
		strcpy(&buffer[i], rhs);
		printf(" %s\n", &buffer[j]);

		found++;
		i += strlen(rhs);

		if(found == 1)
		{
			i++;
			continue;
		}

		if((j = lseek(fd, START_BLOCK * BLOCK_SIZE, SEEK_SET)) < 0)
		{
			perror("lseek error");
			exit(-1);
		}

		printf("lseek to offset: %d\n", j);

		n = write(fd, buffer, sizeof(buffer));

		if(n != sizeof(buffer))
		{
			printf("error writing buffer to disk; expecting %d bytes, got %d\n", (int) sizeof(buffer), n);
			exit(-1);
		}

		close(fd);

		return 0;
	}

	return -1;
}



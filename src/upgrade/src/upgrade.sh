#!/bin/bash


DATA=/data
P0=$DATA/p0
P1=$DATA/p1
UPGRADE=$DATA/upgrade

verbose=1

mkdir -p /data/upgrade/tmp

##############################################################################
## FUNCTION:	
##
## DESCRIPTION :	
##
##
##
##

error()
{
	echo $1
}

##############################################################################
## FUNCTION:	
##
## DESCRIPTION :	
##
##
##
##

message()
{
	if [[ $verbose -eq 0 ]]
	then
		return
	fi

	echo $1
}

##############################################################################
## FUNCTION:	main
##
## DECRIPTION :	
##
##
##
##

echo "`date`: $0 Started"

## we both, echo our exit value (for use with pope(2) and exit with that error code

if [[ ! -e /data/upgrade/upgrade.zip ]]
then
	message "`date`: no zip file in /data//upgrade"
	exit 51
fi

if [[ -e /data/upgrade/tmp ]]
then
	message "`date`: rm /data/upgrade/tmp/*"
	rm -rf /data/upgrade/tmp/*
	message "`date`: DONE rm /data/upgrade/tmp/*"
fi

link=`readlink "/sn"`

if [[ $link == *"p0"* ]]
then
	newdir="p1"
	olddir="p0"
elif [[ $link == *"p1"* ]]
then
	newdir="p0"
	olddir="p1"
else
	## /sn points to something not expected
	message "/sn points to unknown"
	exit 52
fi

echo "`date`:  Upgrading $newdir"

if [[ $? -ne 0 ]]
then
	message "could not create the /sn symlink"
	exit 53
fi

cd /data/$newdir
if [[ $? -ne 0 ]]
then
	message "could not cd into /data/$newdir"
	exit 54
fi


mv sn $UPGRADE/tmp/sn.old
if [[ $? -ne 0 ]]
then
	message "could not move sn to ../upgrade/tmp/sn.old"
	exit 55
fi

echo "`date`:  Unzipping in $newdir"
unzip -q $UPGRADE/upgrade.zip
if [[ $? -ne 0 ]]
then
	message "error unzipping upgrade.zip"
	exit 56
fi
echo "`date`:  DONE Unzipping in $newdir"

echo "`date`:  Flushing the File System Cache"
sync; sync; sync
echo "`date`:  DONE Flushing the File System Cache"

chmod 755 sn/*
if [[ $? -ne 0 ]]
then
	message "could not make executables executable"
	exit 57
fi


## Now copy the configured database to the new directory
echo "`date`:  Copying the old database to the new directory"
cp /data/$olddir/sn/webserver/db/ctb.db /data/$newdir/sn/webserver/db
if [[ $? -ne 0 ]]
then
	message "could not copy configured database to $newdir"
	exit 58
fi

## Now compare the checksums of the database we just copied to the original
db0=$P0/sn/webserver/db/ctb.db
db1=$P1/sn/webserver/db/ctb.db

sum0=`md5sum $db0 | cut -d' ' -f1`
sum1=`md5sum $db1 | cut -d' ' -f1`

if [[ $sum0 != $sum1 ]]
then
    exit 59
fi

## The last thing to do is redirect /sn to piint to the $newdir iff no errors
## and if we got here, then there werent any erros
echo "`date`:  Relink /sn to $newdir"
rm /sn
ln -s $DATA/$newdir/sn /sn

## Now make sure the new version is displayed
## cd $DATA/$newdir
## system-check.sh




exit 0

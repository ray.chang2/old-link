cmake_minimum_required(VERSION 3.12)

project(upgrade, VERSION 1.0.1 LANGUAGES C)

execute_process(COMMAND cppcheck
       ${CPPCHECK_FLAGS}
       ${CPPCHECK_IGNORE_FLAGS}
       ${GENERAL_INCLUDE_FLAGS}
       ${CMAKE_CURRENT_SOURCE_DIR}/upgraded.c)

add_executable(upgraded upgraded.c)
 
target_include_directories(upgraded
	PRIVATE	../inc ../../ctdb/inc)

target_compile_options(upgraded PRIVATE ${WARN_OPTIONS})
target_link_libraries(upgraded ctdb  BridgeCommon sqlite3 see-sqlite3 pthread zmq dl)
install(TARGETS upgraded RUNTIME DESTINATION ${DESTDIR})


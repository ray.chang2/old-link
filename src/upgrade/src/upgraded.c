#include <unistd.h> 
#include <fcntl.h> 
#include <stdio.h> 
#include <stdint.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <poll.h> 
#include <pthread.h> 
#include <sys/stat.h> 
#include <sys/socket.h> 
#include <sys/types.h> 


#define COMMAND_PORT 		27030
#define VIDEO_PORT 			27033

#define	UPGRADE_DIRECTORY	"/data/upgrade"

#define DEBUG_UPGRADE       1


typedef struct   __ip_connection
{
    int port;
    int *fd;
} IP_connection_t;

typedef struct	_cmd0
{
	uint8_t		pid;
	uint8_t		cmd;
	uint16_t	pad0;
	uint32_t	length;
} query_command_t;
// } query_command_t __attribute__((packed));

typedef struct	_file_structue
{
	uint32_t		size;
	uint8_t			target;
	uint8_t			type;
	uint8_t			filename[64];
	uint8_t			directory[64];
	uint8_t			crc_valid;
	uint8_t			crc;
	uint8_t			overwrite;
} file_structure_t; 
// } file_structure_t __attribute__((packed));

typedef	struct	_protocol_reply
{
	query_command_t		header __attribute__((packed));
	uint8_t				type;
	uint8_t				status;
	uint8_t				crc;
}
protocol_reply_t;
// protocol_reply_t __attribute__((packed));

typedef	struct	_initialte_programming
{
	query_command_t		header __attribute__((packed));
	uint8_t				version;
	uint8_t				type;
	uint8_t				subtype;
	uint8_t				hash[32];
	uint8_t				hash_type;
	uint8_t				crc;
} initiate_programming;

typedef struct	_programming_response
{
	query_command_t		header __attribute__((packed));
	uint8_t				status;
	uint8_t				crc;
} programming_response_t;


static const char* version = "01.00.00"; //follows major.minor.patch scheme(https://semver.org)


unsigned int	verbose=0;
int             fdc=0, fdv=0;


/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

void
blog(uint8_t *filter, void *data, uint32_t n)
{
uint8_t     *fmt = (uint8_t*) "%x ";
uint8_t     tbuf[128];
uint32_t    i, m;
int         fd;
time_t      t;
uint8_t     *p;
char		buffer[512];

    memset(tbuf, 0, sizeof(tbuf));
    memset(buffer, 0, sizeof(buffer));

    if((fd = open("/var/log/upgrade.log", O_APPEND|O_RDWR|O_CREAT, 0744)) < 0)
    {
        perror("open");
        return;
    }

    if(!strcmp((char*) filter, "INFO"))
        fmt = (uint8_t*) "%c";

    float   f = sizeof(buffer) * 0.8;

    t = time(NULL);
    p = (uint8_t*) ctime(&t);
    p[strlen((char*) p) - 1] = 0;

    if(data && n > 0)
        snprintf(buffer, sizeof(buffer), "%s: %s (%d): ", p, filter, n);
    else
        snprintf(buffer, sizeof(buffer), "%s: %s", p, filter);

    if(data && n > 0)
    {
        p = data;
        m = (n>30?16:n);

        for(i=0; i<m; i++)
        {
            snprintf((char*) tbuf, sizeof(tbuf), (char*) fmt, (uint8_t) p[i]);
            strcat((char*) buffer, (char*) tbuf);

            if((float) strlen((char*) buffer) > f)
            {
                snprintf((char*) tbuf, sizeof(tbuf), "  strlen(buffer) = %d",
                    (int) strlen((char*) buffer));
                strcat((char*) buffer, (char*) tbuf);

                strcat((char*) buffer, (char*) "  --- BREAK\n");
                break;
            }
        }
    }

    strcat((char*) buffer, "\n");

    write(fd, (char*) buffer, strlen((char*) buffer));
    close(fd);
}



/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

int
wait_for_connection(int32_t *fd)
{
char   buffer[256];
int     count=0;


    snprintf(buffer, sizeof(buffer), "ENTER: wait_for_connection: %d", *fd);
#if defined(DEBUG_UPGRADE)
    blog((uint8_t*) buffer, NULL, (uint32_t) 0);
#endif

    while(*fd == 0)
    {
        if(count++ == 100)
            break;

        usleep(10000);
    }

    if(*fd == 0)
    {
#if defined(DEBUG_UPGRADE)
        snprintf(buffer, sizeof(buffer), "wait_for_connection: %d -- TIMED OUT", *fd);
        blog((uint8_t*) buffer, NULL, (uint32_t) 0);
#endif

        return -1;
    }

#if defined(DEBUG_UPGRADE)
    snprintf(buffer, sizeof(buffer), "wait_for_connection: %d -- SUCCESS", *fd);
    blog((uint8_t*) buffer, NULL, (uint32_t) 0);
#endif

    return 0;
}

/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

int
read_bytes(int fd, char *ip, int n, int timeout)
{
int		m=0, totalbytes=0;
struct	pollfd		fds[1];

	fds[0].fd = fd;
	fds[0].events = POLLIN;
	fds[0].revents = 0;

	while(totalbytes < n)
	{
		if(poll(fds, 1, timeout*1000) <= 0)
		{
			printf("poll: no data\n");
			goto exit;
		}

		m = read(fd, &ip[m], n - totalbytes);

		printf("read_bytes: total: %d, m: %d\n", totalbytes, m);

		if(m > 0)
			totalbytes += m;

		if(m <= 0) break;

		usleep(5000);
	}

exit:

	return totalbytes;
}
	
/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

#if 0
unsigned char 
calc_crc(const char* buf, int len)
{
int 		sum = 0;
int32_t 	i;


    for(i=0; i<len; i++) {
        sum += buf[i];
    }
    unsigned char checksum = (unsigned char) (~(sum % 256) + 1);
    return checksum;
}
#else
uint8_t calc_crc(const uint8_t* buf, uint32_t len)
{
    int sum = 0;
    uint32_t i;
    for(i=0; i<len; i++) {
        sum += buf[i];
    }
    uint8_t checksum = (uint8_t) (~(sum % 256) + 1);
    return checksum;
}
#endif

/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

void
dump_bytes(char *label, char *ptr, int n)
{
int32_t    i;

	if(n < 0) return;

    printf("%s: DUMP (%d): ", label, n);
    for(i=0; i<n; i++, ptr++)
        printf("%x ", *ptr);
    printf("\n");
}


/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

int
download_file(int fd, int size, char *fullpath)
{
struct	pollfd		fds[1];
char				buffer[256];
char				data[1448];
int					interumbytes=0, i=0, status=0, ofd, totalbytes=0, n=0;

	printf("download_file: %s\n", fullpath);

	fds[0].fd = fd;
	fds[0].events = POLLIN;
	fds[0].revents = 0;

	ofd = open(fullpath, O_TRUNC|O_RDWR|O_CREAT, 0600);

	if(ofd < 0)
		return ofd;

	snprintf(buffer, sizeof(buffer), "download_file: downloading n: %d ...", size);
	blog((uint8_t*) buffer, NULL, (uint32_t) 0);

	while(1)
	{

		if((status = poll(fds, 1, 5000)) <= 0)
		{
			snprintf(buffer, sizeof(buffer), "download_file: interum: %d,  got %d of %d bytes ", interumbytes, totalbytes, size);
			blog((uint8_t*) buffer, NULL, (uint32_t) 0);
			blog((uint8_t*) "no data", NULL, (uint32_t) 0);
			goto exit;
		}

		n = status = read(fd, data, sizeof(data));

		if(status <= 0)
		{
			totalbytes = -1;
			goto exit;
		}

		totalbytes += n;
		interumbytes += n;

		if((++i % 1000 == 0))
		{
			snprintf(buffer, sizeof(buffer), "download_file: interum: %d,  got %d of %d bytes ", interumbytes, totalbytes, size);
			blog((uint8_t*) buffer, NULL, 0);
			interumbytes = 0;
		}

		status = write(ofd, data, n);

		if(status < 0)
		{
			totalbytes = -1;
			goto exit;
		}

		if(totalbytes >= size)
			break;
	}

#if 0
	struct	stat		s;
	stat("/data/upgrade/upgrade.zip", &s);
	printf("UPGRADED: COMPLETE: totalbytes: %d, stat: %d, status = %d, n = %d \n", totalbytes, (int) s.st_size, status, n);
#endif

exit:
	close(ofd);

	return totalbytes;
}

/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

int
do_upgrade2()
{
int		status;
char	buffer[256];


	snprintf(buffer, sizeof(buffer), "Firing up upgrade.sh");
	blog((uint8_t*) buffer, NULL, 0);

	status = system("/sn/upgrade.sh > /var/log/upgrade-sh.log");

	snprintf(buffer, sizeof(buffer), "DONE: upgrade.sh");
	blog((uint8_t*) buffer, NULL, 0);

	return WEXITSTATUS(status);
}

/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

int
do_command_protocol(int fdv)
{
int						n, status=0, upgrade_status=0;
char					crc;
char					fullpath[1024], data[128];
uint8_t                 buffer[256];
query_command_t			query;
file_structure_t		file_structure;
protocol_reply_t		reply;

	memset(&query, 0, sizeof(query));
	memset(&file_structure, 0, sizeof(file_structure));

	// read the first command sent from the tablet.
	printf("do_command_protocol: calling read()\n");

	// Step 1a
	n = read(fdv, &query, sizeof(query_command_t));
	dump_bytes("read(1a): ", (char*) &query, n);
	query.length = ntohl(query.length);

	// Step 1a
	n = read(fdv, &file_structure, query.length);
	dump_bytes("read(1b): ", (char*) &file_structure, n);
	file_structure.size = ntohl(file_structure.size);

	snprintf((char*) buffer, sizeof(buffer), "size: %d, target: %d, type: %d, filename: %s, dir: %s, crc_valid: %d, crc: %d, ovw: %d", file_structure.size, 
		file_structure.target, file_structure.type, file_structure.filename, file_structure.directory, 
		file_structure.crc_valid, file_structure.crc, file_structure.overwrite);
	blog(buffer, NULL, (uint32_t) 0);

	if(file_structure.size <= 0)
	{
		snprintf((char*) buffer, sizeof(buffer), "read error: file size = %d", file_structure.size);
		blog((uint8_t*) buffer, NULL, (uint32_t) 0);
		return -1;
	}

	// See if the PID and CMD make and sense ???
	// pquery = (query_command_t*) &file_structure.target;
	// printf("	pid: %x, cmd: %x,  length: %d\n", pquery->pid, pquery->cmd, ntohl(pquery->length));

	// Step 1c
	n = read(fdv, &crc, 1);
	dump_bytes("read(1c crc?): ", (char*) &file_structure, n);

	//
	// From here we switch to using the command port for communication
	//

	memset(&reply, 0, sizeof(reply));

	// Step 2

    if(fdc == 0)
    {
        if(wait_for_connection(&fdc) < 0)
        {
            snprintf((char*) buffer, sizeof(buffer), "Fatal Time Out: no connection on Command port: %d", COMMAND_PORT);
            blog(buffer, NULL, 0);
            return -1;
        }
    }

	reply.header.pid = 0xb6;
	reply.header.cmd = 0xaf;
	reply.header.length = htonl(2);
	reply.type = file_structure.type;
	reply.status = 0xbb;
	reply.crc = calc_crc( (uint8_t*) &reply, sizeof(reply) - 1);

	n = send(fdc, &reply, sizeof(reply), 0);
	dump_bytes("send(2): ", (char*) &reply, n);

	if(n != sizeof(reply))
	{
		snprintf((char*) buffer, sizeof(buffer), "Error from send : %d", n);
		blog((uint8_t*) buffer, NULL, (uint32_t) 0);
		return -1;
	}

	// Step 3 -- Send File
	memset(&query, 0, sizeof(query));
	n = read(fdv, &query, sizeof(query));
	query.length = ntohl(query.length);
	dump_bytes("read(3a): ", (char*) &query, n);

	if(query.pid != 0xd9 || query.cmd != 0x92)
	{
		snprintf((char*) buffer, sizeof(buffer), "No response from tablet -- expecting 0xd9 and 0x92");
		blog((uint8_t*) buffer, NULL, (uint32_t) 0);
		return -1;
	}

	memset(&file_structure, 0, sizeof(file_structure));
	n = read(fdv, &file_structure, query.length);
	if(n < 0)
	{
		snprintf((char*) buffer, sizeof(buffer), "read error: %d\n", n);
		return -1;
	}

	file_structure.size = ntohl(file_structure.size);
	dump_bytes("read(3b): ", (char*) &file_structure, n);
	snprintf((char*) buffer, sizeof(buffer), "	size: %d, target: %d, type: %d, filename: %s, dir: %s, crc_valid: %d, crc: %d, ovw: %d", file_structure.size, 
		file_structure.target, file_structure.type, file_structure.filename, file_structure.directory, 
		file_structure.crc_valid, file_structure.crc, file_structure.overwrite);
	blog((uint8_t*) buffer, NULL, (uint32_t) 0);

	n = read(fdv, data, sizeof(data));
	dump_bytes("read(3c - crc?): ", data, n);

	// n = read(fdv, buffer, sizeof(buffer));
	// dump_bytes("read(3d - data): ", buffer, n);

	//
	// Now download the upgrade file
	//

	snprintf(fullpath, sizeof(fullpath), "%s/%s", UPGRADE_DIRECTORY, "upgrade.zip");
	status = download_file(fdv, file_structure.size, fullpath);

	snprintf((char*) buffer, sizeof(buffer), "back from download_file: status: %d\n", status);
    blog(buffer, NULL, 0);

	// Step 4 - send file response
	reply.header.pad0 = 0;
	reply.header.pid = 0xb6;
	reply.header.cmd = 0xa9;
	reply.header.length = htonl(2);
	reply.type = file_structure.type;
	reply.crc = calc_crc( (uint8_t*) &reply, sizeof(reply) - 1);

	if(status == (int) file_structure.size)
		reply.status = 0x99;
	else
		reply.status = 0;

	n = send(fdv, &reply, sizeof(reply), 0);
	dump_bytes("send(4): ", (char*) &reply, n);

	if(n != sizeof(reply))
	{
		snprintf((char*) buffer, sizeof(buffer), "Error from send (4): %d\n", n);
        blog(buffer, NULL, 0);
		return -1;
	}

	if(status != (int) file_structure.size)
	{
	    snprintf((char*) buffer, sizeof(buffer), "status != file_structure.size: %d - %d\n", status, file_structure.size);
        blog(buffer, NULL, 0);
		return -1;
	}

	// Step 5 - Intiate Programming
	initiate_programming	ip;

	// we're looking for 45 bytes here. 
	n = read_bytes(fdc, (char*) &ip, sizeof(ip), 5);
	dump_bytes("read(5): ", (char*) &ip, n);

	printf("getting checksum ... \n");
	FILE *pfd;
	snprintf((char*) buffer, sizeof(buffer), "%s %s", "md5sum", "/data/upgrade/upgrade.zip");
	blog((uint8_t*) buffer, NULL, (uint32_t) 0);

	pfd = popen((char*) buffer, "r");
	if(pfd <= (FILE*) NULL)
		return -3;

	int nbytes=0;
	memset((char*) buffer, 0, sizeof(buffer));

	while(1)
	{
		n = fread(&buffer[nbytes], sizeof(buffer) - nbytes, 1, pfd);

		if(n <= 0)
			break;
	}

	pclose(pfd);

	printf("MD5: %s\n", (char*) buffer);

	if((upgrade_status=memcmp(ip.hash, (char*) buffer, sizeof(ip.hash))))
		snprintf((char*) buffer, sizeof(buffer), "MD5 ***NO*** MATCH\n");
	else
		snprintf((char*) buffer, sizeof(buffer), "MD5 MATCH\n");

	blog((uint8_t*) buffer, NULL, (uint32_t) 0);

	snprintf((char*) buffer, sizeof(buffer), "upgrade_status = %d", upgrade_status);
	blog((uint8_t*) buffer, NULL, (uint32_t) 0);

	if(upgrade_status == 0)
	{
		// Now actually perform the upgrade
		upgrade_status = do_upgrade2();
	}

	programming_response_t		pr;

#if 0
	// Step 6 - Initiate Programming Response
	pr.header.pid = 0xB6;
	pr.header.cmd = 0xC5;	
	pr.header.pad0 = 0;
	pr.header.length = htonl(1);
	pr.status = 0;
	pr.crc = calc_crc( (uint8_t*) &pr, sizeof(pr) - 1);

	n = send(fdv, &pr, sizeof(pr), 0);
	dump_bytes("send(6): ", (char*) &pr, n);
#endif

	// Step 7 - Initiate Programming Response
	pr.header.pid = 0xB6;
	pr.header.cmd = 0xC6;	
	pr.header.pad0 = 0;
	pr.header.length = htonl(1);
	pr.status = abs(upgrade_status);   // PASS or FAIL
	pr.crc = calc_crc( (uint8_t*) &pr, sizeof(pr) - 1);

	n = send(fdv, &pr, sizeof(pr), 0);
	dump_bytes("send(7): ", (char*) &pr, n);

	return upgrade_status;
}

/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

int
establish_connection(int port)
{
char					buffer[256];
struct sockaddr_in 		address; 
int						fd, fd2, opt=1;
int 					addrlen = sizeof(address); 

#if defined(DEBUG_UPGRADE)
	snprintf(buffer, sizeof(buffer), "establish_connection: port: %d", port);
	blog((uint8_t*) buffer, NULL, 0);
#endif

	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
	{ 
		return -1; 
	}
	 
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) 
	{ 
		close(fd);
		return -1; 
	} 

	address.sin_family = AF_INET; 
	address.sin_addr.s_addr = INADDR_ANY; 
	address.sin_port = htons(port); 
 
	if (bind(fd, (struct sockaddr *)&address,  sizeof(address))<0) 
	{ 
		close(fd);
		return -1; 
	} 

	if (listen(fd, 1) < 0) 
	{ 
		close(fd);
		return -1; 
	} 

	if ((fd2 = accept(fd, (struct sockaddr *)&address,  (socklen_t*)&addrlen)) < 0) 
	{ 
		close(fd);
		return -1; 
	} 

#if defined(DEBUG_UPGRADE)
	snprintf(buffer, sizeof(buffer), "upgraded: Got a connection: port: %d, fd2: %d", port, fd2);
	blog((uint8_t*) buffer, NULL, 0);
#endif

	close(fd);
	return fd2;
}

/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

void*
connection_thread(void *data)
{
IP_connection_t     *ip = (IP_connection_t*) data;
char		        buffer[256];


#if defined(DEBUG_UPGRADE)
	snprintf(buffer, sizeof(buffer), "connection_thread: port = %d ", ip->port);
	blog((uint8_t*) buffer, NULL, 0);
#endif

	*ip->fd = establish_connection(ip->port);
	if(*ip->fd < 0)
	{
#if defined(DEBUG_UPGRADE)
		snprintf(buffer, sizeof(buffer), 
            "connection_thread: establish_connection(%d) Failure: %d", ip->port, *ip->fd);
		blog((uint8_t*) buffer, NULL, 0);
#endif
	    return NULL;
	}

#if defined(DEBUG_UPGRADE)
	snprintf(buffer, sizeof(buffer), 
        "connection_thread: Successful connection on port: fd = %d", ip->port);
	blog((uint8_t*) buffer, NULL, 0);
#endif

	return NULL;
}

/******************************************************************************
** ROUTINE:	
**
** DESCRIPTION:	
**
**
*/

int 
main(int argc, char const **argv) 
{ 
uint8_t         buffer[256];
int				i, status;
pthread_t		tid;
pthread_attr_t	attr;
IP_connection_t command, video;


	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	for(i=1; i<argc; i++)
	{
		if(!strcmp(argv[i], "-v"))
			verbose = 1;
		else if(!strcmp(argv[i], "-V"))
        {
            printf("%s\n", version);
            exit(0);
        }
		else if(!strcmp(argv[i], "-u"))
		{
			do_upgrade2();
			exit(0);
		}
	}

    video.port = VIDEO_PORT;
    video.fd = &fdv;

    command.port = COMMAND_PORT;
    command.fd = &fdc;

	while(1)
	{
#if defined(DEBUG_UPGRADE)
        snprintf((char*) buffer, sizeof(buffer), 
            "================================================================================");
        blog(buffer, NULL, 0);
#endif

        fdc = fdv = 0;
        
		status = pthread_create(&tid, &attr, connection_thread, &command);
		status = pthread_create(&tid, &attr, connection_thread, &video);

        // Now we sit here until a connection is made on one of the ports.

        while(fdc == 0 && fdv == 0)
        {
            sleep(1);
        }

        // TODO: What happens if a FD is -1, need to handle that here. The app will 
        // fail when it attempts to communicate using a FD that is -1, an we'll come 
        // back to the top of the loop above. But this should be handled here first.

#if defined(DEBUG_UPGRADE)
        snprintf((char*) buffer, sizeof(buffer), "main: out of wait loop: %d, %d", fdc, fdv);
        blog(buffer, NULL, 0);
#endif

        // got a connection on one of the ports. if it's the video port
        // then we can proceed to do the protocol. But if it's the command
        // port that just made the connation, then we need to wait here until 
        // we get a connection on the video port

        if(fdv == 0)
        {
            if(wait_for_connection(&fdv) < 0)
            {
                snprintf((char*) buffer, sizeof(buffer), "Fatal Time Out: no connection on video port: %d, %d", fdc, fdv);
                blog(buffer, NULL, 0);
                continue;
            }
        }

		// PROTOCOL and download the file
		status = do_command_protocol(fdv);

	    if(status == 0)
	    {
		    snprintf((char*) buffer, sizeof(buffer), "Upgrade SUCCESSFUL ... rebooting in 5 seconds -- ^C to stop");
		    blog((uint8_t*) buffer, NULL, 0);

		    sleep(5);
		    system("reboot");
	    }
        else
	    {
		    snprintf((char*) buffer, sizeof(buffer), "Upgrade FAILED: %d", status);
		    blog((uint8_t*) buffer, NULL, 0);
	    }

        if(fdc)
        {
            shutdown(fdc, SHUT_RDWR);
		    close(fdc);
        }

        if(fdv)
        {
            shutdown(fdv, SHUT_RDWR);
		    close(fdv);
        }
	}

	return status; 
} 




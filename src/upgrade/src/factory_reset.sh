#!/bin/bash


###############################################################################
## ROUTINE:
##
## DESCRIPTION:	
##
##
##

log()
{
	echo "`date`: $1" >> $logfile
}

###############################################################################
## ROUTINE:
##
## DESCRIPTION:	
##
##
##

error()
{
	if [[ $? -ne 0 ]]
	then
		log "Fatal Error: $1"
		rm -f $fact
		exit $1
	fi
}

###############################################################################
## ROUTINE:
##
## DESCRIPTION:	
##
##
##

fact=/data/FACTORY
logfile=/var/log/factory_reset.log
wapfile=""

while [[ $1 != "" ]]
do
	if [[ $1 == "-c" ]]
	then
		shift
		/sn/eeprom -r > /etc/wifi.conf
		ssid=$(cat /etc/wifi.conf | grep SSID | cut -d= -f2)
		password=$(cat /etc/wifi.conf | grep PASS | cut -d= -f2)
		wpafile=/etc/wpa_supplicant_ap.conf
	elif [[ $1 == "-n" ]]
	then
		shift
		wpafile=/etc/wpa_supplicant_station.conf
	else
		exit 98
	fi
done

if [[ -e $fact ]]
then
	log "Factory Reset $(cat /proc/$PPID/cmdline) : Already in Progress -- Exiting"
	exit 99
fi

log "Factory Reset Started from $PPID: `cat /proc/$PPID/cmdline`"

touch $fact

symlink=$(readlink /sn)

if [[ $symlink == *"p0"* ]]
then
	curdir=p0
else
	curdir=p1
fi

if [[ ! -e /data/$curdir ]]
then
	log "/data/$curdir does not exist"
	error 1
fi

cd /data
error 2

mkdir -p /data/tmp
error 3

mv /data/$curdir/sn /data/tmp
error 4

mv /data/fd2/sn /data/$curdir
error 5

if [[ -e /etc/D2 ]]
then
	cp /data/$curdir/sn/webserver/db/D2-ctb-factory.db /data/$curdir/sn/webserver/db/ctb.db
	error 6
elif [[ -e /etc/WW ]]
then
	cp /data/$curdir/sn/webserver/db/WW-ctb-factory.db /data/$curdir/sn/webserver/db/ctb.db
	error 7
else
	error 8
fi

rm -rf /data/fd2
error 9

rm -rf /data/tmp/*
error 10

###         sed -i "s/ssid=.*/ssid=\"$WIFI_AP_SSID\"/" /etc/wpa_supplicant_ap.conf

sed -i "s/ssid=.*/ssid=\"$ssid\"/" $wpafile
sed -i "s/psk=.*/psk=\"$password\"/" $wpafile

log "Factory Reset: SUCCESS"
rm -f $fact
sync; sync; sync
sleep 2

/sbin/reboot
error 11 	## should never get here ...

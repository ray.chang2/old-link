/** Discovery Service Manager
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <asm/errno.h>
#include <errno.h>
#include <getopt.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "logger.h"
#include "timer.h"
#include "util.h"
#include "dsc.h"

static const char* version = "00.02.00"; //follows major.minor.patch scheme(https://semver.org)

/*
 * enable BRIDGE_DEVICE to run discovery manager on bridge
 */

#define BRIDGE_DEVICE 

#define MAIN_TIMER_TIMEOUT 1000

/*
 * XXX: UDP broadcasting on LENS4K is showing packet loss.
 *
 * Workaround: Send it multiple times to ensure delivery
 */
// #define SEND_UDP_LIST_MULTI_TIMES
#define SEND_TIMES 10


enum {
    STN_STATE_DISCONNECTED,
    STN_STATE_DISCOVERY_REQUEST_RECEIVED,
    STN_STATE_HB_RECEIVED
};

const char* STATE_NAMES[] = {
    "DISCONNECTED",
    "DISCOVERY_REQUEST_RECEIVED",
    "CONNECTED"
};

//
//         Station/Client Connection
//

struct DscManager;
typedef struct DscStationConnection {
    uint8_t type;
    uint8_t subtype;
    struct in_addr addr;
    int state;
    char device_name[MAX_DEVICE_NAME_LEN];
    char ip_addr_str[MAX_IP_LEN];
    pthread_t tcp_thread;

    struct DscManager* mgr;

    int cfg_hb_timeout_ms;
    bool _stop;
    bool _first_tcp_list_sent;
} DscStationConnection;

static bool client_is_disconnected(const DscStationConnection* client) {
    return client->state == STN_STATE_DISCONNECTED;
}

static void clear_stop_signal(DscStationConnection* client) {
    client->_stop = false;
}

#if 0
static void send_stop_signal(DscStationConnection* client, const char* from) {
    client->_stop = true;
}
#endif
static bool received_stop_signal(const DscStationConnection* client) {
    return client->_stop;
}


//
//         Manager
//

struct DscManager {
    int cfg_dr_resp_timeout_ms;
    int cfg_hb_timeout_ms;

    struct DscStationConnection stations[DEVICE_TYPE_MAX];
    pthread_t broadcast_rx_thread;
    pthread_t broadcast_tx_thread;
    pthread_t util_thread;

    pthread_mutex_t mutex;
    pthread_cond_t cond_need_to_broadcast_list;
    bool need_to_broadcast_list;

    char broadcast_addr_str[MAX_IP_LEN];
    char ip_addr_str[MAX_IP_LEN];

    uint32_t main_timer;
};
typedef struct DscManager DscManager;

static bool g_verbose = false;

static void send_broadcast_list(DscManager* mgr, int sockfd,
        const char* network, uint16_t port);

void usage()
{
    logger.info("Usage:\n");
    logger.info("./dsc_server -V displays version number\n");
    logger.info("./dsc_server -h\n");
    logger.info("./dsc_server [-d dr_resp_timeout_ms] [-H hb_timeout_ms]\n");
    logger.info("\n");
    logger.info("Example:\n"
            "(for 500 ms response timeout and 2000 ms heartbeat timeout)\n"
            "./dsc_client -d 500 -H 2000\n");
}

// return:
// 0: OK
// 1: usage
// 2: error or exit condition
int parse_cmd_line(DscManager* mgr, int argc, char *argv[])
{
    opterr = 0;
    int rc = 0;

    int c;
    while ((c = getopt(argc, argv, "D:H:hvV")) != -1) {
        switch (c)
        {
            case 'D':
                mgr->cfg_dr_resp_timeout_ms = atol(optarg);
                break;
            case 'H':
                mgr->cfg_hb_timeout_ms = atol(optarg);
                break;
            case 'h':
                usage();
                rc = 1;
                break;
            case 'V':
                printf("%s\n",version);
                rc = 2;
                break;
            case 'v':
                g_verbose = true;
                break;
            default:
                logger.error("Error: Unknown option: -%c\n", c);
                rc = 2;
                break;
        }
    }

    return rc;
}

void request_broadcast_list(DscManager* mgr)
{
    pthread_mutex_lock(&mgr->mutex);
    mgr->need_to_broadcast_list = true;
    pthread_cond_signal(&mgr->cond_need_to_broadcast_list);
    pthread_mutex_unlock(&mgr->mutex);
}

void* main_timer_cb(void* arg) {
    DscManager* mgr = (DscManager*) arg;

    request_broadcast_list(mgr);

    cancel_timer(mgr->main_timer);
    start_timer(mgr->main_timer);

    return NULL;
}

static int init_dsc_manager(DscManager* mgr)
{
    uint8_t i;

    bzero(mgr, sizeof(DscManager));
    mgr->cfg_dr_resp_timeout_ms = DR_RESP_TIMEOUT_MS;
    mgr->cfg_hb_timeout_ms = HB_TIMEOUT_MS;
    if (pthread_mutex_init(&mgr->mutex, NULL) != 0) {
        logger.fatal("pthread_mutex_init() error: %s", strerror(errno));
        exit(1);
    }

    if (pthread_cond_init(&mgr->cond_need_to_broadcast_list, NULL) != 0) {
        logger.fatal("pthread_cond_init() error: %s", strerror(errno));
        exit(2);
    }

    // init station connections
    for (i = 0; i < DEVICE_TYPE_MAX; i++) {
        mgr->stations[i].type = i;
        mgr->stations[i].mgr = mgr;
        mgr->stations[i].state = STN_STATE_DISCONNECTED;
        mgr->stations[i].cfg_hb_timeout_ms = mgr->cfg_hb_timeout_ms;
        clear_stop_signal(&mgr->stations[i]);
        snprintf(mgr->stations[i].device_name, MAX_DEVICE_NAME_LEN, "---");
    }

    mgr->main_timer = create_timer(MAIN_TIMER_TIMEOUT,
            main_timer_cb, mgr);

    //start_timer(mgr->main_timer);

    return 0;
}

void change_state(DscStationConnection* con, int state)
{
    if (con->state != state) {
        logger.info("%s state changing from %s to %s\n",
                con->device_name,
                STATE_NAMES[con->state],
                STATE_NAMES[state]);
        con->state = state;
    }
}

void* station_tcp_thread_func(void* thread_param)
{
    ssize_t n;

    logger.debug("Entering %s\n", __FUNCTION__);

    struct DscStationConnection *station =
        (struct DscStationConnection*) thread_param;
    logger.debug("%s HB Timeout setting: %d ms\n",
            station->device_name, station->cfg_hb_timeout_ms);
    do_assert(station->state == STN_STATE_DISCOVERY_REQUEST_RECEIVED);

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        logger.error("Error: failed to create socket\n");
        change_state(station, STN_STATE_DISCONNECTED);
        return NULL;
    }

    struct sockaddr_in cli_addr;
    bzero((char *) &cli_addr, sizeof(cli_addr));
    cli_addr.sin_family = AF_INET;
    cli_addr.sin_addr = station->addr;
    cli_addr.sin_port = htons(DSC_CLI_LISTENING_TCP_PORT);

    logger.debug("Connecting to the device.\n");

    if (connect(sockfd, (struct sockaddr *) &cli_addr, sizeof(cli_addr)) < 0) {
        logger.error("Error: failed to connect to %s: %s\n",
                station->device_name,
                strerror(errno));
        close(sockfd);
        change_state(station, STN_STATE_DISCONNECTED);
        return NULL;
    }
    logger.highlight("Connected to %s\n", station->device_name);

    {
        DscMsg_Assign msg;
        int mgr_ip = ip_str_to_int(station->mgr->ip_addr_str);

        DscMsg_Assign_Init(&msg, mgr_ip);
        if (g_verbose) print_msg((DscMsg*)&msg);

        n = write(sockfd, &msg, sizeof(msg));
        if (n < 0) {
            logger.error("Error: failed to writing to client socket\n",
                    strerror(errno));
            close(sockfd);
            change_state(station, STN_STATE_DISCONNECTED);
            return NULL;
        }
    }

    logger.highlight("Dsc_Assign message sent out to %s (type: %s)\n",
            station->device_name, device_type_name(station->type));

    // main loop

    struct timeval tv_recv_timeout = {
        station->cfg_hb_timeout_ms /1000,
        station->cfg_hb_timeout_ms %1000 *1000 };
    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO,
                (const char*)&tv_recv_timeout, sizeof(tv_recv_timeout))) {
        logger.error("Error: failed to configuring client socket: %s\n",
                strerror(errno));
        close(sockfd);
        change_state(station, STN_STATE_DISCONNECTED);
        request_broadcast_list(station->mgr);
        return NULL;
    }

    uint32_t hb_timeouts = 0;
    char buffer[MAX_PACKET_SIZE];
    while(!received_stop_signal(station)) {
        bzero(buffer, MAX_PACKET_SIZE);
        n = recv(sockfd, buffer, MAX_PACKET_SIZE, 0);

        // processing the packet
        if (n < 0) {
            if (errno == EAGAIN) {
                logger.warning("missing heartbeat from %s, missed count %d\n",
                        station->device_name, hb_timeouts);
                hb_timeouts++;
                if (hb_timeouts == MAX_ALLOWED_HB_TIMEOUT) {
                    logger.error("Error: Too many missing heartbeat from %s. \n"
                            "Dropping the connection\n",
                            station->device_name);
                    break;
                } else if (hb_timeouts > MAX_ALLOWED_HB_TIMEOUT) {
                    logger.error_internal("Error: unexpected hb_timeouts value\n");
                    exit(1);
                }
            } else {
                logger.error("ERROR reading TCP connection from %s\n",
                        station->device_name);
                break;
            }
        } else if (n == 0) {  // connection closed
            logger.warning("%s disconnected\n", station->device_name);
            change_state(station, STN_STATE_DISCONNECTED);
            request_broadcast_list(station->mgr);
            break;
        } else if (n >= MAX_PACKET_SIZE) {
            hb_timeouts = 0;
            logger.error("Error: package size exceeding limit\n");
            break;
        } else {
            hb_timeouts = 0;
            DscMsg* msg = (DscMsg*) buffer;
            if (!verify_msg_format(msg)) {
                logger.warning("Client msg format error\n");
                continue;
            }
            if (g_verbose) {
                logger.debug("From %s: ", station->device_name);
                print_msg(msg);
            }

            if (msg->protocol_id != DSC_PROTO_ID_C2S) {
                logger.warning("Unknown protocol : %02X.\n", msg->protocol_id);
                continue;
            }

            uint32_t data_len = msg_data_len(msg);
            if (msg->command == DSC_CMD_CLIENT_HEART_BEAT) {
                change_state(station, STN_STATE_HB_RECEIVED);
                if (!station->_first_tcp_list_sent) {
                    logger.highlight(
                            "Received the first HB. _first_tcp_list_sent:%d\n",
                            station->_first_tcp_list_sent);
#if 0 // no need to send HB through TCP
                    logger.info("Sending first device list through TCP\n");
                    send_broadcast_list(station->mgr, sockfd, NULL, 0);
#else
                    logger.info("Sending first device list through UDP\n");
                    request_broadcast_list(station->mgr);
#endif
                }
                if (data_len != DSCMSG_HB_PAYLOAD_LEN) {
                    logger.warning("Invalid packet length from %s for "
                            "DSC_CMD_CLIENT_HEART_BEAT: %d (expecting %d)\n",
                            station->device_name,
                            data_len,
                            DSCMSG_HB_PAYLOAD_LEN);
                    continue;
                }
            } else if (msg->command == DSC_CMD_CLIENT_LIST_RQST) {
                if (data_len != DSCMSG_LISTRQST_PAYLOAD_LEN) {
                    logger.warning("Invalid packet length from %s for "
                            "DSC_CMD_CLIENT_LIST_RQST: %d (expecting %d)\n",
                            device_type_name(station->type),
                            data_len,
                            DSCMSG_HB_PAYLOAD_LEN);
                    continue;
                }
                request_broadcast_list(station->mgr);
            } else {
                logger.warning("Unknown command : %02X.\n", msg->command);
            }
        }  // packet recv'd
    }  // for(;;)

    clear_stop_signal(station);

    logger.warning("closing remote client connection for %s\n",
                station->device_name);
    close(sockfd);
    change_state(station, STN_STATE_DISCONNECTED);
    request_broadcast_list(station->mgr);

    return NULL;
}

static void send_broadcast_list(DscManager* mgr, int sockfd,
        const char* network, uint16_t port)
{
    int i;
    struct sockaddr_in remote_addr;
    bool using_udp = network ? true : false;

    if (using_udp) {
        bzero(&remote_addr, sizeof(remote_addr));
        remote_addr.sin_family = AF_INET;
        remote_addr.sin_addr.s_addr = inet_addr(network);
        remote_addr.sin_port = htons(port);
        logger.debug("network addr:%s\n", network);
        logger.debug("udp client port:%d\n",port);
    }

    DscMsg_ListDcptVirtual msg;
    DscMsg_ListDcpt_Init(&msg);

    for (i=0; i < DEVICE_TYPE_MAX; i++) {
        DscStationConnection* stn = &mgr->stations[i];
        if (stn->state == STN_STATE_HB_RECEIVED) {
            DscClientInfo info;
            DscClientInfo_init(&info);
            info.ip = stn->addr.s_addr;
            info.device_type = stn->type;
            info.sub_device_type = stn->subtype;
            DscMsg_ListDcpt_Add_Device(&msg, &info);
            logger.debug("%d: %s (%s)\n",
                    i, stn->device_name, inet_ntoa(stn->addr));
        }
    }

    int msg_size = get_msg_len((const DscMsg*)&msg);
    if (g_verbose) print_msg((const DscMsg*)&msg);
    
    int flags;
    ssize_t n;
    if (!using_udp) {
        flags = 0;
        n = send(sockfd, &msg, (size_t)msg_size, flags);
    } else {
        flags = MSG_CONFIRM;
        int cnt = 1;
#ifdef SEND_UDP_LIST_MULTI_TIMES
        cnt = SEND_TIMES;
#endif
        for (i=0; i<cnt; i++) {
            n = sendto(sockfd, &msg, (size_t)msg_size, flags,
                    (const struct sockaddr*) &remote_addr, sizeof(remote_addr));
	    if (n < 0) {
                logger.info("Error: broadcasting Device List to %d: sendto(%d): %s\n",
                port, sockfd, strerror(errno));
		logger.error("retry sending broadcasting Device List:retry count %d:\n",
		        i);
                usleep(1000);
	    } else {
                break;
            }
        }
    }

    if (n < 0) {
        logger.error("Error: broadcasting Device List to %d: sendto(%d): %s\n",
                port, sockfd, strerror(errno));
    } else {
        logger.debug("Device List message sent\n");
        for (i=0; i < DEVICE_TYPE_MAX; i++) {
            DscStationConnection* stn = &mgr->stations[i];
            if (stn->state == STN_STATE_HB_RECEIVED) {
                if (!stn->_first_tcp_list_sent) {
                    stn->_first_tcp_list_sent = true;
                    start_timer(stn->mgr->main_timer);
                }
            }
        }
    }
}

static const char* pthread_error_str(int ret)
{
    switch (ret) {
        case EAGAIN:
            return "Insufficient resources to create another thread\n";
            break;
        case EINVAL:
            return "Error: Invaid settings\n";
            break;
        case EPERM:
            return "Error: Permisssion error\n";
            break;
        default:
            return "";
    }
}

static int received_station_dr_packet(DscManager* mgr, struct in_addr addr,
        const DscMsg_DiscoverRqst* msg)
{
    if (msg->device_type >= DEVICE_TYPE_MAX || !msg->device_type) {
        logger.error("Error: Unknown client device \"%s\" (%s)\n",
                msg->device_dscr, device_type_name(msg->device_type));
        return 1;
    }

    DscStationConnection* cli = &mgr->stations[msg->device_type];
    if (!client_is_disconnected(cli)) {
        logger.warning("Another connection from %s (%s) is already active. ",
                msg->device_dscr,
                device_type_name(msg->device_type));
        return 1;
    } else {
        cli->addr = addr;
        cli->type = msg->device_type;
        cli->subtype = msg->device_sub_type;
        cli->_first_tcp_list_sent = false;
        strncpy(cli->device_name, msg->device_dscr, MAX_DEVICE_NAME_LEN);
        change_state(cli, STN_STATE_DISCOVERY_REQUEST_RECEIVED);
        logger.debug("creating station TCP thread\n");
        int ret = pthread_create(&cli->tcp_thread, NULL,
                station_tcp_thread_func, (void*)cli);
        if (ret != 0) {
            logger.error("Error: pthread_create to %s failed: %s\n",
                    device_type_name(cli->type), pthread_error_str(ret));
            return 2;
        }
        ret = pthread_detach(cli->tcp_thread);
        if (ret != 0) {
            logger.fatal("pthread_detach failed\n");
            return 3;
        }
    }
    return 0;
}

void* udp_rx_thread_func(void* thread_param)
{
    DscManager *mgr = (DscManager*) thread_param;

    logger.debug("Entering %s\n", __FUNCTION__);

    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        logger.error("ERROR opening socket\n");
        return NULL;
    }

    int enable = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,
            (const void *)&enable , sizeof(int));

    /*
     * build the server's Internet address
     */
    struct sockaddr_in listen_addr;
    bzero((char *) &listen_addr, sizeof(listen_addr));
    listen_addr.sin_family = AF_INET;
#if 1
    listen_addr.sin_addr.s_addr = htonl(INADDR_ANY);
#else
    listen_addr.sin_addr.s_addr = inet_addr(mgr->ip_addr_str);
#endif
    listen_addr.sin_port = htons((uint16_t)DSC_SVR_LISTENING_UDP_PORT);
    if (bind(sockfd, (struct sockaddr *)&listen_addr,
                sizeof(listen_addr)) < 0) {
        logger.error("ERROR on binding\n");
        close(sockfd);
        return NULL;
    }

    struct sockaddr_in remote_addr;
    socklen_t client_len = sizeof(remote_addr);
    uint8_t buf[MAX_PACKET_SIZE];
    for (;;) {
        bzero(buf, MAX_PACKET_SIZE);
        ssize_t n = recvfrom(sockfd, buf, MAX_PACKET_SIZE, 0,
                (struct sockaddr *) &remote_addr, &client_len);
        if (n < 0) {
            logger.warning("ERROR in recvfrom\n");
            // ignore the error
        } else if (n == MAX_PACKET_SIZE) {
            logger.warning("Error: package size exceeding limit\n");
            // ignore the packet
        } else {
            DscMsg* msg = (DscMsg*) buf;
            if (g_verbose) print_msg(msg);

            if (!verify_msg_format(msg)) {
                logger.warning("Client msg format error\n");
                continue;
            }

            if (msg->protocol_id != DSC_PROTO_ID_C2S ||
                    msg->command != DSC_CMD_CLIENT_DISCOVER_RQST) {
                logger.warning("Unknown protocol or command: %02X/%02X.\n",
                            msg->protocol_id, msg->command);
                continue;
            }

            DscMsg_DiscoverRqst* msg_dr = (DscMsg_DiscoverRqst*) buf;
            uint32_t data_len = msg_data_len(msg);
            if (data_len != DSCMSG_DISOVERRQST_LEN) {
                logger.warning("Error: Invalid packet length from %s (%s) for"
                        "DSC_CMD_CLIENT_DISCOVER_RQST: %d (expecting %d)\n",
                        msg_dr->device_dscr,
                        device_type_name(msg_dr->device_type),
                        data_len,
                        DSCMSG_DISOVERRQST_LEN);
                continue;
            }

            logger.highlight("Received discovery request from %s (%s)\n",
                    msg_dr->device_dscr, inet_ntoa(remote_addr.sin_addr));
            received_station_dr_packet(mgr, remote_addr.sin_addr, msg_dr);
        }
    }  // for(;;)

    close(sockfd);
    return NULL;
}

void* udp_tx_thread_func(void* thread_param)
{
    DscManager *mgr = (DscManager*) thread_param;

    logger.debug("Entering %s\n", __FUNCTION__);

    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        logger.error("ERROR opening socket\n");
        exit(1);
    }

    int broadcast_enable = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST,
                &broadcast_enable, sizeof(broadcast_enable)) == -1) {
        logger.error("setsockopt (SO_BROADCAST)\n");
        close(sockfd);
        exit(1);
    }

    for (;;) {
        pthread_mutex_lock(&mgr->mutex);
        while (!mgr->need_to_broadcast_list) {
            pthread_cond_wait(&mgr->cond_need_to_broadcast_list, &mgr->mutex);
        }
        send_broadcast_list(mgr, sockfd, mgr->broadcast_addr_str,
                DSC_CLI_LISTENING_UDP_PORT);
        mgr->need_to_broadcast_list = false;
        pthread_mutex_unlock(&mgr->mutex); 

    }
    return NULL;
}

int main(int argc, char *argv[])
{
    int rc;

#ifndef BRIDGE_DEVICE
    FILE* fp;
#endif //BRIDGE_DEVICE
    if (argc > 1 && !strcmp(argv[1],"-V")) {
        printf("%s\n", version);
        return 0;
    }

    //init_logger();

#ifndef BRIDGE_DEVICE
    fp = popen("/opt/ipnc/dm_running.sh", "r");
    if (fp == NULL) {
        printf("Error running dm_running.sh");
        return 1;
    }

    int dm_instances = 0;
#if 1
    fscanf(fp, "%d", &dm_instances);
#else
    char buf[10];
    while (fgets(buf, sizeof(buf)-1, fp) != NULL) {
              printf("%s", buf);
    }
#endif
    pclose(fp);

    if (dm_instances == 2) {
        printf("%d Another instance of dsc manager is already running. Exiting.\n", dm_instances);
        return 1;
    }
#endif //BRIDGE_DEVICE

    logger.info("Discovery Manager started. (build time: %s %s)\n",
            __DATE__, __TIME__);

#ifndef BRIDGE_DEVICE
    logger.info("Waiting for Device Manager (usbMain) to be ready\n");
    wait_for_dm_ready();
    logger.highlight("Device Manager (usbMain) is ready\n");
#endif //BRIDGE_DEVICE

    DscManager* mgr = (DscManager*)malloc(sizeof(DscManager));
    rc = init_dsc_manager(mgr);
    if (rc != 0) {
        logger.fatal("Discovery Server initialization error\n");
        return 1;
    }

    rc = parse_cmd_line(mgr, argc, argv);
    if (rc != 0) {
        if (rc == 1) {
            usage();
            return 0;
        } else if(rc == 2) {
            return 0; //displayed version information on the console
        } else {
            logger.error("command line parsing error\n");
            return 1;
        }
    }
    //if (g_verbose) logger.log_level = LV_DEBUG;

    rc = get_ip_addr_str(mgr->ip_addr_str, MAX_IP_LEN);
    if (rc != 0) {
        logger.fatal("not able to obtain self's IP\n");
        return 1;
    }

    rc = get_bcast_addr_str(mgr->broadcast_addr_str, MAX_IP_LEN);
    if (rc != 0) {
        logger.fatal("not able to obtain broadcast IP address\n");
        return 1;
    }

    logger.info("Discovery Request Response Timeout: %d ms\n",
            mgr->cfg_dr_resp_timeout_ms);
    logger.info("Heartbeat Timeout: %d ms\n", mgr->cfg_hb_timeout_ms);
    logger.highlight("Listening for Discovery Request at %s, Broadcast to: %s\n",
            mgr->ip_addr_str, mgr->broadcast_addr_str);

    // start UDP broadcast listening thread (on port 50000)
    if (pthread_create(&mgr->broadcast_rx_thread, NULL, udp_rx_thread_func,
                mgr)) {
        logger.fatal("failed to start broadcast rx thread\n");
        return 3;
    }

    // start UDP broadcast transmitting thread (to port 50020)
    if (pthread_create(&mgr->broadcast_tx_thread, NULL, udp_tx_thread_func,
                mgr)) {
        logger.fatal("failed to start broadcast tx thread\n");
        return 4;
    }

    // For now, just assuming running forever

    pthread_join(mgr->broadcast_rx_thread, NULL);
    pthread_join(mgr->broadcast_tx_thread, NULL);

    free(mgr);
    return 0;
}

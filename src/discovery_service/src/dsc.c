/** Discovery Service Protocol
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "logger.h"
#include "dsc.h"

// Device type names, follows DSC_DEVICE_TYPE enum order
const char* DEVICE_TYPE_NAMES[] = {
    "Unknown Device",
    "Shaver",
    "Video",
    "Coblation",
    "Tablet",
    "Fluid Management",
    "_MAX"
};

/********************DEVICE SUBTYPE STRINGS ************************************************************/

//follows eConnectedTowerVisSubTypes enum
const char* VIDEO_DEVICE_SUBTYPE_NAMES[] = {
    "unknown subtype",
    "LENS HD",
    "LENS 4K",
    "ARTEMIS",
    "WIRELESS CAMERA"
};

//follows eConnectedTowerResectionSubTypes enum
const char* SHAVER_DEVICE_SUBTYPE_NAMES[] = {
    "unknown subtype",
    "DYONICS POWER II",
};

//follows eConnectedTowerCoblationSubTypes enum
const char* COBLATION_DEVICE_SUBTYPE_NAMES[] = {
    "unknown subtype",
    "WEREWOLF",
};

//follows eConnectedTowerTabletSubEnums enum
const char* TABLET_DEVICE_SUBTYPE_NAMES[] = {
    "unknown subtype",
    "Intellio Touch",
};

//follows eConnectedTowerFluid1SubEnums enum
const char* FLUID_DEVICE_SUBTYPE_NAMES[] = {
    "unknown subtype",
    "DYONICS POWER 25",
    "Hemodia Fluid Management Control Unit",
};

const char* device_subtype_name(int device_type, int device_subtype)
{
    if (device_type < DEVICE_TYPE_RESERVED || device_type >= DEVICE_TYPE_MAX)  {
        device_type = DEVICE_TYPE_RESERVED;
    }

    if (device_type != DEVICE_TYPE_RESERVED) {
        switch (device_type) {
        case DEVICE_TYPE_SHAVER:
            return (const char *)(SHAVER_DEVICE_SUBTYPE_NAMES[device_subtype]);
        case DEVICE_TYPE_VIDEO:
            return (const char *)(VIDEO_DEVICE_SUBTYPE_NAMES[device_subtype]);
        case DEVICE_TYPE_COBLATION:
            return (const char *)(COBLATION_DEVICE_SUBTYPE_NAMES[device_subtype]);
        case DEVICE_TYPE_TABLET:
            logger.info("Tablet device sent subtype value as <%d>", device_subtype);
            if (device_subtype > 1) device_subtype = 1;
            return (const char *)(TABLET_DEVICE_SUBTYPE_NAMES[device_subtype]);
        case DEVICE_TYPE_FLUID_MANAGEMENT:
            return (const char *)(FLUID_DEVICE_SUBTYPE_NAMES[device_subtype]);
        default:
            break;
        }
    }
    return VIDEO_DEVICE_SUBTYPE_NAMES[0];
}

const char* device_type_name(int device_type)
{
    if (device_type < DEVICE_TYPE_RESERVED || device_type >= DEVICE_TYPE_MAX)  {
        device_type = DEVICE_TYPE_RESERVED;
    }
    return (const char *)(DEVICE_TYPE_NAMES[device_type]);
}

uint32_t msg_data_len(const DscMsg* msg) {
    return ntohl(msg->data_len_nw);
}

void msg_set_data_len(DscMsg* msg, uint32_t new_len) {
    msg->data_len_nw = htonl(new_len);
}

uint32_t msg_add_data_len(DscMsg* msg, int increase) {
    msg_set_data_len(msg, msg_data_len(msg) + increase);
    return msg_data_len(msg);
}

uint8_t calc_crc(const uint8_t* buf, int len)
{
    int sum = 0;
    int i;
    for (i = 0; i < len; i++) {
        sum += buf[i];
    }
    uint8_t checksum = (uint8_t) (~(sum % 256) + 1);
    return checksum;
}

bool verify_msg_format_internal(const DscMsg* msg, bool verbose)
{
    uint8_t* buf = (uint8_t*) msg;
    uint32_t data_len = msg_data_len(msg);
    uint32_t len = DSCMSG_HEADER_SIZE + data_len + 1;
    if (len >= MAX_PACKET_SIZE) {
        if (verbose) {
            printf("Message len error: %u\n", len);
            print_msg_short(msg);
        }
        return false;
    }
    uint8_t crc_calc = calc_crc(buf, len -1);
    uint8_t crc_byte = buf[len -1];
    if (crc_calc != crc_byte) {
        if (verbose) {
            printf("Message crc error. Expected: %u(0x%x), Actual: %u(0x%x)\n",
                    crc_calc, crc_calc,
                    crc_byte, crc_byte);
            print_msg(msg);
        }
        return false;
    }

    return true;
}

bool verify_msg_format(const DscMsg* msg) {
    return verify_msg_format_internal(msg, true);
}

static void print_msg_header(const DscMsg* msg)
{
    uint32_t data_len = msg_data_len(msg);

    switch (msg->protocol_id) {
        case DSC_PROTO_ID_C2S:
            switch (msg->command) {
                case DSC_CMD_CLIENT_DISCOVER_RQST:
                    printf("Discovery Request: ");
                    break;
                case DSC_CMD_CLIENT_HEART_BEAT:
                    printf("Heartbeat: ");
                    break;
                case DSC_CMD_CLIENT_LIST_RQST:
                    printf("List Request:");
                    break;
                default:
                    printf("Unknown Command");
            }
            break;
        case DSC_PROTO_ID_S2C:
            switch (msg->command) {
                case DSC_CMD_SERVER_ASSIGN:
                    printf("Dsc_Assign: ");
                    break;
                case DSC_CMD_SERVER_LIST_CHANGE:
                    printf("Dsc_listChnge:");
                    break;
                default:
                    printf("Unknown Command");
            }
            break;
        default:
            printf("Unknown Protocol: %02X\n", msg->protocol_id);
            return;
    }
    printf("%02X %02X %02X %02X %02X - ",
           msg->protocol_id, msg->command,
           msg->version, msg->seq_number,
           data_len);
}

static void print_msg_safe(const DscMsg* msg, uint32_t len_limit)
{
    uint32_t i;
    uint32_t data_len = msg_data_len(msg);

    print_msg_header(msg);
    if (len_limit != 0 && data_len > len_limit) {
        data_len =  len_limit;
    }
    printf("[");
    for (i = 0; i < data_len; i++) {
        printf("%02X ", msg->data[i]);
    }
    printf("]\n");
}

static void print_msg_short_internal(const DscMsg* msg)
{
    print_msg_safe(msg, 20);
}

void print_msg_short(const DscMsg* msg)
{
    printf("\033[1;30m");
    print_msg_short_internal(msg);
    printf("\033[0m");
}

static void print_msg_internal(const DscMsg* msg)
{
    uint32_t i;
    uint32_t data_len = msg_data_len(msg);

    print_msg_header(msg);
    printf("[");
    for (i = 0; i < data_len; i++) {
        printf("%02X ", msg->data[i]);
    }
    printf("] - %02X\n", msg->data[data_len]);  // CRC
}

void print_msg(const DscMsg* msg)
{
    printf("\033[1;30m");
    print_msg_internal(msg);
    printf("\033[0m");
}

uint32_t ip_str_to_int(char* s)
{
    struct in_addr ip;
    char *t = s;
    while(true) {
        if ((*t >= '0' && *t <= '9' ) || *t == '.') {
            t++;
            continue;
        } else {
            *t = '\0';
            break;
        }
    }
    int ret = inet_pton(AF_INET, s, &ip);
    if (ret == 1) {
        return ip.s_addr;
    } else {
//        logger.error("\033[1;31m");
        logger.error("Error converting IP str <%s>, len:%u", s, (uint32_t)strlen(s));
//        logger.error("\033[0m");
        return 0;
    }
}

int full_msg_len(int data_len)
{
    // assumption:
    // * The message is packed
    // * data_len % 4 == 0
    return DSCMSG_HEADER_SIZE + data_len + 1;  // 1 for CRC
}

int get_msg_len(const DscMsg* msg)
{
    int data_len = msg_data_len(msg);
    return full_msg_len(data_len);
}

////////////////////////////////////////////////////////////////////////////////

int _DscMsg_Init(DscMsg* msg)
{
    msg->version = DISCOVERY_PROTOCOL_VERSION;
    msg->seq_number = 0; //FIXME: for link device this field is considered optional at the moment
    return 0;
}

int DscMsg_DiscoverRqst_Init(DscMsg_DiscoverRqst* msg, uint32_t device_ip,
        const char* device_name, int device_type, uint8_t device_sub_type, int feature_mask)
{
    bzero(msg, sizeof(DscMsg_DiscoverRqst));
    _DscMsg_Init((DscMsg*) msg);
    msg->header.protocol_id = 0x33;
    msg->header.command = DSC_CMD_CLIENT_DISCOVER_RQST;
    msg_set_data_len((DscMsg*)msg, 50);
    msg->ip = device_ip;
    msg->device_type = device_type;
    msg->device_sub_type = device_sub_type;
    strncpy(msg->device_dscr, device_name, MAX_DEVICE_NAME_LEN);
    msg->feature_mask = feature_mask;
    msg->crc = calc_crc((uint8_t*)msg, sizeof(DscMsg_DiscoverRqst) - 1);
    return 0;
}

int  DscMsg_HeartBeat_Init(DscMsg_HeartBeat* msg, uint8_t device_state, uint8_t device_type)
{
    bzero(msg, sizeof(DscMsg_HeartBeat));
    _DscMsg_Init((DscMsg*) msg);
    msg->header.protocol_id = 0x33;
    msg->header.command = DSC_CMD_CLIENT_HEART_BEAT;
    msg->device_id = device_type;
    msg->device_state = device_state;
    msg_set_data_len((DscMsg*)msg, 8);
    msg->crc = calc_crc((uint8_t*)msg, sizeof(DscMsg_HeartBeat) - 1);
    return 0;
}

void DscMsg_Assign_Init(DscMsg_Assign* msg, uint32_t ip)
{
    bzero(msg, sizeof(DscMsg_Assign));
    _DscMsg_Init((DscMsg*) msg);
    msg->header.protocol_id = DSC_PROTO_ID_S2C;
    msg->header.command = DSC_CMD_SERVER_ASSIGN;
    msg_set_data_len((DscMsg*)msg, DSCMSG_ASSIGN_PAYLOAD_LEN);

    msg->result = 0;        // Always success since we are already connecting to the client
    msg->ip = ip;
    msg->feature_mask = DEFAULT_FEATURE_MASK;
    msg->crc = calc_crc((uint8_t*)msg, sizeof(DscMsg_Assign) - 1);
}

int DscMsg_ListDcpt_Init(DscMsg_ListDcptVirtual* msg)
{
    bzero(msg, sizeof(DscMsg_ListDcptVirtual));
    _DscMsg_Init((DscMsg*) msg);
    msg->header.protocol_id = DSC_PROTO_ID_S2C;
    msg->header.command = DSC_CMD_SERVER_LIST_CHANGE;
    msg_set_data_len((DscMsg*)msg, DSCMSG_LISTDCP_PAYLOAD_HEADER_SIZE);
    msg->num_of_devices = 0;
    msg->server_dev_type = DEVICE_TYPE_VIDEO;
    msg->server_dev_subtype = 0;
    msg->server_if_spt_bitmask = 0;
    // CRC location (with 0 devices) is right after the 4-byte payload header
    uint8_t* pcrc = (uint8_t*)msg->client_list;
    *pcrc = calc_crc((uint8_t*)msg, sizeof(msg->header) + 
            DSCMSG_LISTDCP_PAYLOAD_HEADER_SIZE);
    return 0;
}

int DscClientInfo_init(DscClientInfo* info)
{
    bzero(info, sizeof(DscClientInfo));
    return 0;
}

int DscMsg_ListDcpt_Add_Device(DscMsg_ListDcptVirtual* msg,
        const DscClientInfo* info)
{
    int device_type = info->device_type;

    if (msg->num_of_devices == MAX_DEVICE_NUM) {
        return 1;  // maximum number of device reached
    }

    if (device_type <= 0 || device_type >= DEVICE_TYPE_MAX) {
        return 2;  // invalid device_type
    }

    // add client info
    DscClientInfo* new_cli = &msg->client_list[msg->num_of_devices];
    memcpy(new_cli, info, sizeof(DscClientInfo));

    // update length fields
    uint32_t data_len = msg_add_data_len((DscMsg*)msg, 16);
    msg->num_of_devices += 1;

    // add crc
    uint8_t* buf = (uint8_t*) msg;
    int msg_len = full_msg_len(data_len);
    uint8_t* crc = &buf[msg_len-1];
    *crc = calc_crc(buf, msg_len-1);

    return 0;
}

void print_msg_device_list(const DscMsg_ListDcptVirtual* msg)
{
    uint32_t data_len = msg_data_len((DscMsg*)msg);
    int client_cnt = (data_len - 4) / DSC_CLIENT_INFO_LEN;
    int i;
    for (i = 0; i < client_cnt; i++) {
        const DscClientInfo* ci = (const DscClientInfo*)&msg->client_list[i];
        struct in_addr ip = {ci->ip};
        uint8_t type = ci->device_type;
        logger.debug("Device #%d: %s (%s)\n",
                type, device_type_name(type), inet_ntoa(ip));
    }
}

bool verify_msg_device_list(const DscMsg_ListDcptVirtual* msg)
{
    uint32_t data_len = msg_data_len((DscMsg*)msg);
    int client_cnt = (data_len - 4) / DSC_CLIENT_INFO_LEN;

    if (client_cnt != msg->num_of_devices) {
        return false;
    }

    if ((data_len - 4) % 16 != 0) {
        logger.warning("Invalid data length: %d "
                "(expecting multiple of 16)\n", data_len);
        return false;
    }
    return true;
}

static int get_cmd_result(char* buf, uint32_t buf_len, const char* cmd)
{
    FILE *fp;

    fp = popen(cmd, "r");
    if (fp == NULL) {
        return 1;
    }

    if (fgets(buf, buf_len-1, fp) != NULL) {
        pclose(fp);
        return 0;
    }

    pclose(fp);
    return 2;
}

int get_ip_addr_str(char* buf, uint32_t buf_len)
{
    return get_cmd_result(buf, buf_len, "/sn/netinfo.sh ip");
}

int get_mask_str(char* buf, uint32_t buf_len)
{
    return get_cmd_result(buf, buf_len, "/sn/netinfo.sh mask");
}

int get_bcast_addr_str(char* buf, uint32_t buf_len)
{
    return get_cmd_result(buf, buf_len, "/sn/netinfo.sh bcast");
}

int get_mac_addr_str(char* buf, uint32_t buf_len)
{
    return get_cmd_result(buf, buf_len, "/sn/netinfo.sh mac");
}

/** Discovery Service Client
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <getopt.h>
#include <assert.h>
#include "commonTypes.h"
#include "logger.h"
#include "timer.h"
#include "util.h"
#include "dsc.h"
#include "broker_api.h"
#include "D2_channels.h"
#include "WW_channels.h"
#include "ctdb_utils.h"
#include "ctb_db_table_api.h"
#include "cJSON.h"

static const char* version = "01.01.00"; //follows major.minor.patch scheme(https://semver.org)

#define MAC_ADDR_STR_LEN 19
#define SW_VER_STR_LEN 12

// discovery client status 
typedef enum {
   STATE_WAIT_CAPITAL_DEVICE,
   STATE_DISCONNECTED,
   STATE_WAITING_FOR_ASSIGN,
   STATE_WAITING_FOR_NETWORK_LIST,
   STATE_CONNECTED,
} DISCOVERY_CLIENT_STATES;

// local context structure 
typedef struct DscClient_Context {
   int state;
   uint32_t client_list_timer;
   bool isClientListTimerStarted;
   bool rx_client_list_time_out;
   int cfg_hb_interval_ms;
   int cfg_dr_interval_ms;
   int device_feature_bitmask;
   int device_type;
   uint8_t device_sub_type;
   char device_name[MAX_DEVICE_NAME_LEN];
   char ip_addr_str[MAX_IP_LEN+1];
   char broadcast_addr_str[MAX_IP_LEN+1];
   char mac_addr_str[MAC_ADDR_STR_LEN];
   char sw_ver_str[SW_VER_STR_LEN];
   bool has_ip_address;
   uint32_t device_ip;
   int manager_sockfd;
   struct in_addr manager_ip;
   int manager_port;
} DscClient_Context;

// private variables 
static bool    g_verbose             = false;
static char*   cmd_device_name       = "d2"; // default is D2
static int     cmd_standalone_no_bdm = false;
static void    *pubhandle            = NULL;
static uint8_t capital_device_state  = STATE_WAIT_CAPITAL_DEVICE;

// local variables 
static DscMsg_ListDcptVirtual localListMsg; 
static uint32_t localClientListSize  = sizeof(localListMsg);
static uint32_t capDev_hb_count = 0;
static uint32_t capDev_rx_hb_miss = 0;
//static uint8_t  capDev_connected_msg = 0;

// #define for quickness
#define g_has_bdm (!cmd_standalone_no_bdm)

/**
 * @fn      void usage(void)
 * @brief   disables how to use this application
 * @param   n/a
 * @retval  n/a
 **************************************************************/
static void usage(void)
{
   printf("Usage:\n");
   printf("./dsc_client -V displays version number\n");
   printf("./dsc_client [-h]\n");
   printf("./dsc_client [-s]\n");     // not talking to BDM
   printf("./dsc_client [-d <device_name>]\n");
   printf("./dsc_client [-H hb_interval_ms] [-D disc_request_interval_ms]\n");
   printf("\n");
   printf("Valid <device_name>: lens, d2, d25, and werewolf\n");
   printf("\n");
   printf("Example (for 600 ms Heartbeat interval and 1000 ms Discovery "
          "Request interval:\n");
   printf("./dsc_client -H 600 -D 1000\n");
   printf("./dsc_client -d d2\n");
   printf("./dsc_client -d lens\n");
}

/**
 * @fn      int parse_cmd_line(DscClient_Context* ctx, int argc, char *argv[])
 * @brief   initializes all local data into ctx
 * @param   ctx  - cxt to put data
 * @param   argc - argc from command line
 * @param   argv - argv from the command line
 * @retval  0: OK; 1: usage; 2: error or exit condition
 **************************************************************/
int parse_cmd_line(DscClient_Context* ctx, int argc, char *argv[])
{
   opterr = 0;
   int rc = 0;

   int c;
   while ((c = getopt(argc, argv, "H:sd:D:hvV")) != -1) {
      switch (c)
      {
      case 's':
         cmd_standalone_no_bdm = true;
         break;
      case 'h':
         rc = 1;
         break;
      case 'v':
         g_verbose = true;
         break;
      case 'V':
         printf("%s\n",version);
         rc = 2;
         break;
      case 'd':
         cmd_device_name = optarg;
         break;
      case 'H':
         ctx->cfg_hb_interval_ms = atol(optarg);
         break;
      case 'D':
         ctx->cfg_dr_interval_ms = atol(optarg);
         break;
      default:
         logger.error("Unknown option: -%c\n", c);
         rc = 2;
         break;
      }
   }

   return rc;
}

/**
 * @fn      bool isClientListChanged(DscMsg_ListDcptVirtual* prev, DscMsg_ListDcptVirtual* current)
 * @brief   checks whether client information in the list response is changed from previous response
 * @param   prev    - previous list response data
 * @param   current - current list response data
 * @retval  true: changed; false: not changed
 **************************************************************/
static bool isClientListChanged(DscMsg_ListDcptVirtual* prev, DscMsg_ListDcptVirtual* current)
{
    int i;
    if (prev->num_of_devices != current->num_of_devices)
        return true;
    for (i = 0; i < current->num_of_devices; i++) {
        if ((prev->client_list[i].device_type != current->client_list[i].device_type) ||
            (prev->client_list[i].sub_device_type != current->client_list[i].sub_device_type) ||
            (prev->client_list[i].ip != current->client_list[i].ip)) {
            return true;
        }
    }
    logger.debug("client list information didn't change");
    return false;
}

/**
 * @fn      void writeJsonFile(DscMsg_ListDcptVirtual* list_msg, int client_cnt)
 * @brief   writes list of devices information into discovery_output.json file
 * @param   list_msg - list response data
 * @param   client_cnt - number of clients present in the response
 * @param   device_type - link device type
 * @param   mac_addr_str - MAC address of link device 
 * @param   sw_ver_str - link device software version in major.minor.build format
 * @retval  none
 **************************************************************/
static void writeJsonFile(DscMsg_ListDcptVirtual* list_msg, int client_cnt, 
                          uint8_t device_type, char* mac_addr_str, char* sw_ver_str)
{
    int i;
    char* file_name = "/sn/webserver/db/discovery_output.json";
    FILE * fp = fopen(file_name, "w");
    if(fp == NULL) {
        logger.debug("json file open error");
        return;
    }

    cJSON* cjsonDeviceArray = cJSON_CreateArray();
    if (cjsonDeviceArray == NULL) {
        logger.debug("CJSON array create error");
        fclose(fp);
        fp = NULL;
        return;
    }

    bool json_err = false;
    int minimum_field_read_size = 20; //database minimum field size to read is 20, less than that will give an error
    char sku[minimum_field_read_size], serial_number[minimum_field_read_size];

    for (i = 0; i < client_cnt; i++) {
        DscClientInfo* ci = &list_msg->client_list[i];
        cJSON* cjsonDevice = cJSON_CreateObject();
        if (cjsonDevice == NULL) {
            logger.debug("CJSON object create error");
            json_err = true;
            break;
        }
        struct in_addr ip = {ci->ip};
        uint8_t type = ci->device_type;
        bool apply_empty_sku = true;
        bool apply_empty_serial = true;
        if (type == device_type) {
            //update sku and serial number values from database to put into json file below
            int rc;
            rc = get_devinfo_sku(sku, 20);
            if (rc != 0) {
                logger.debug("error reading sku from database, err:%d", rc);
            } else {
                sku[8] = '\0';
                apply_empty_sku = false;
            }
            rc = get_devinfo_serial(serial_number, 20);
            if (rc != 0) {
                logger.debug("error reading serial number from database, err:%d", rc);
            }
            else {
                serial_number[7] = '\0';
                apply_empty_serial = false;
                logger.debug("sku from database:%s", sku);
                logger.debug("serial number from database:%s", serial_number);
            }
            rc = set_devinfo_software_version(sw_ver_str, strlen(sw_ver_str));
            rc = set_devinfo_mac_address(mac_addr_str, strlen(mac_addr_str));
        }
        if (apply_empty_sku) strcpy(sku, "--------");
        if (apply_empty_serial) strcpy(serial_number, "-------");
        //logger.debug("writeJson:Device #%d: %s (%s)\n", type, device_type_name(type), inet_ntoa(ip));
        if (cJSON_AddStringToObject(cjsonDevice, "name", device_subtype_name(type, ci->sub_device_type)) == NULL) {
            logger.debug("CJSON device-name create error");
            json_err = true;
            break;
        }
        if (cJSON_AddStringToObject(cjsonDevice, "type", device_type_name(type)) == NULL) {
            logger.debug("CJSON device-type create error");
            json_err = true;
            break;
        }
        if (cJSON_AddStringToObject(cjsonDevice, "ip", inet_ntoa(ip)) == NULL) {
            logger.debug("CJSON ip create error");
            json_err = true;
            break;
        }
        if (cJSON_AddStringToObject(cjsonDevice, "sku", sku) == NULL) {
            logger.debug("CJSON sku create error");
            json_err = true;
            break;
        }
        if (cJSON_AddStringToObject(cjsonDevice, "sn", serial_number) == NULL) {
            logger.debug("CJSON sn create error");
            json_err = true;
            break;
        }
        cJSON_AddItemToArray(cjsonDeviceArray, cjsonDevice);
    }

    if (!json_err) {
        const char* jsonBuffer = cJSON_Print(cjsonDeviceArray);
        if (jsonBuffer != NULL) {
            fprintf(fp, "%s\n",jsonBuffer);
            fflush(fp);
        }
        else {
            logger.debug("Nothing written, CJSON buffer EMPTY!!!");
        }
    }
    fclose(fp);
    cJSON_Delete(cjsonDeviceArray);
}

/**
 * @fn      void set_types(DscClient_Context *ctx, const char* device_name)
 * @brief   set device_type and subtype in the context handle based on device name input.
 * @param   device_name - name from which device type and sub-type byte values are derived
 * @param   ctx - client context
 * @retval  none
 **************************************************************/
static void set_types(DscClient_Context *ctx, const char* device_name)
{
   if (strcmp(device_name, "lens") == 0) {
      ctx->device_type = DEVICE_TYPE_VIDEO;
      ctx->device_sub_type = eConnectedTowerLENS4K;
   } else if (strcmp(device_name, "d2") == 0) {
      ctx->device_type = DEVICE_TYPE_SHAVER;
      ctx->device_sub_type = eConnectedTowerD2Shaver;
   } else if (strcmp(device_name, "d25") == 0) {
      ctx->device_type = DEVICE_TYPE_FLUID_MANAGEMENT;
      ctx->device_sub_type = eConnectedTowerD25;
   } else if (strcmp(device_name, "werewolf") == 0) {
      ctx->device_type = DEVICE_TYPE_COBLATION;
      ctx->device_sub_type = eConnectedTowerCoblationWereWolf;
   } else if (strcmp(device_name, "Tablet") == 0) {
      ctx->device_type = DEVICE_TYPE_TABLET;
      ctx->device_sub_type = eConnectedTowerTabletIntellioTouch;
   } else if (strcmp(device_name, "lensHD") == 0) {
       ctx->device_type = DEVICE_TYPE_VIDEO;
       ctx->device_sub_type = eConnectedTowerLENS;
   } else if (strcmp(device_name, "artemis") == 0) {
       ctx->device_type = DEVICE_TYPE_VIDEO;
       ctx->device_sub_type = eConnectedTowerArtemis;
   } else if (strcmp(device_name, "wirelessCamera") == 0) {
       ctx->device_type = DEVICE_TYPE_VIDEO;
       ctx->device_sub_type = eConnectedTowerWirelessVis;
   } else if (strcmp(device_name, "hemodia") == 0) {
      ctx->device_type = DEVICE_TYPE_FLUID_MANAGEMENT;
      ctx->device_sub_type = eConnectedTowerHemodiaFluid2;
   } else {
       ctx->device_type = DEVICE_TYPE_RESERVED;
       ctx->device_sub_type = DEVICE_SUBTYPE_RESERVED;
   }
}


/**
 * @fn      const char* state_name(int state)
 * @brief   returns a string the discovery state
 * @param   state - name to compare against
 * @retval  state string
 **************************************************************/
static const char* state_name(int state)
{
   switch (state) {
   case STATE_WAIT_CAPITAL_DEVICE:      return "WAIT_CAPITAL_DEVICE";
   case STATE_DISCONNECTED:             return "DISCONECTED";
   case STATE_WAITING_FOR_ASSIGN:       return "WAITING_FOR_ASSIGN";
   case STATE_WAITING_FOR_NETWORK_LIST: return "WAITING_FOR_NETWORK_LIST";
   case STATE_CONNECTED:                return "CONNECTED";
   default:                             return "UNKNOWN STATE";
   }
};

/**
 * @fn      void change_state(DscClient_Context* ctx, int state)
 * @brief   deals with state changes in one place
 * @param   ctx   = local context
 * @param   state = new state
 * @retval  n/a (NULL)
 **************************************************************/
static void change_state(DscClient_Context* ctx, int state)
{
   if (ctx->state == state)  {
      return;
   }
   logger.info("state change: %s --> %s", state_name(ctx->state), state_name(state));
   ctx->state = state;
}

/**
 * @fn      void* rx_client_list_timeout(void* arg)
 * @brief   timeout callback for publishes client list locally
 * @param   arg = local context
 * @retval  n/a (NULL)
 **************************************************************/
void* rx_client_list_timeout(void* arg)
{
   if (!arg) {
      logger.fatal("Null context passed to call back");
      return NULL;
   }
   logger.warning("Did Not Receive Client list in predefined time.");
   DscClient_Context* ctx = (DscClient_Context*) arg;
   ctx->rx_client_list_time_out = true;
   return NULL;
}

/**
 * @fn      void proxy_disconnect(void)
 * @brief   sends a capDev disconnect so other processes 
 * @param   ctx - local context/state structure
 * @retval  n/a 
 **************************************************************/
static void proxy_disconnect(const DscClient_Context* ctx)
{
   if(!g_has_bdm) {
       logger.error("Don't send capdev disconnect message in standalone(a.k.a config mode)");
       return;
   }

   u8 lost = 0;
   if (ctx->device_type == DEVICE_TYPE_SHAVER)
   {
      if((bm_send(pubhandle, (uint8_t *)D2_CONNECTED, &lost, sizeof(lost))) < 0)
      {
         logger.error("Failed to send Proxy Disconnect because CapDev is down");
      }
   } // if d2
   else if (ctx->device_type == DEVICE_TYPE_COBLATION)
   {
      if((bm_send(pubhandle, (uint8_t *)WW_CONNECTED, &lost, sizeof(lost))) < 0)
      {
         logger.error("Failed to send Proxy Disconnect because CapDev is down");
      }
   } // if ww
}

/**
 * @fn      void publish_in_list(u8 inList)
 * @brief   sends the heartbeat to the discovery manager
 * @param   inList  - value to publish
 * @retval  n/a 
 **************************************************************/
static void publish_in_list(u8 inList)
{
   u8 value = inList;
   logger.highlight("Publishing List (%d)",inList);
   if((bm_send(pubhandle, (uint8_t *)PUBSUB_LINK_IN_LIST, &value, sizeof(value))) < 0)
   {
      logger.error("Failed to Publish PUBSUB_LINK_IN_LIST(%d)",value);
   }
}

/**
 * @fn      void publish_wifi_state()
 * @brief   sends the wifi state msg to subscribers
 * @param   st  - 1 - ON; 0 - OFF;
 * @retval  n/a 
 **************************************************************/
static void publish_wifi_state(u8 st)
{
   u8 state = st;
   logger.highlight("Publishing wifi state(%d)", state);
   if((bm_send(pubhandle, (uint8_t *)BROKER_DSC_WIFI_STATE, &state, sizeof(state))) < 0)
   {
      logger.error("Failed to Publish wifi state(%d)",state);
   }
}



/**
 * @fn      void send_heart_beat(const DscClient_Context* ctx)
 * @brief   sends the heartbeat to the discovery manager
 * @param   ctx  - local context
 * @retval  n/a 
 **************************************************************/
static void send_heart_beat(const DscClient_Context* ctx)
{
   // fill in discover request msg
   static unsigned oldRxStatusCnt = 0;
   static unsigned unchangedCount = 0;

   if (oldRxStatusCnt == capDev_hb_count)
   {
      unchangedCount++;
      if (capital_device_state && (unchangedCount >= 5))
      {
         logger.error("Error - looks like we lost capital Device State");
         capital_device_state = 0;
         change_state((DscClient_Context* )ctx, STATE_WAIT_CAPITAL_DEVICE);
         proxy_disconnect(ctx);
      }
   }
   else
   {
      unchangedCount = 0;
   }
    
   oldRxStatusCnt = capDev_hb_count;
   DscMsg_HeartBeat msg;
   DscMsg_HeartBeat_Init(&msg,(capital_device_state ? 0 : 1), ctx->device_type);
   logger.highlight("Sending Heartbeat to Manager with device state <%d>",msg.device_state);
   ssize_t n = write(ctx->manager_sockfd, &msg, sizeof(DscMsg_HeartBeat));
   if (n < 0)
   {
      logger.error("ERROR writing heartbeat to socket - %s",COMMON_ERR_STR);
   }
}

/**
 * @fn      void publish_client_list(void)
 * @brief   publishes client list locally
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void publish_client_list(void)
{
   static void *pHandlePub = NULL;
   if (pHandlePub == NULL)
   {
      if ((pHandlePub = bm_publisher()) == NULL)
      {
         logger.error("Failure to create in ");
      }
   }

   if (pHandlePub != NULL)
   {      
      logger.highlight("Publishing Discovery List to CapDev");
      if ((bm_send(pHandlePub, (u8 *)BROKER_DISCOVERY_LIST, &localListMsg, localClientListSize)) < 0)
      {
         logger.error("Failed to publish discovery list");
      }
   }
}

/**
 * @fn      void* dsc_monitor_ip(void* thread_param)
 * @brief   handles receiving heartbeat from the bdm capital device
 * @param   arg = local context
 * @retval  n/a (NULL)
 **************************************************************/
void reset_connected_state(DscClient_Context *ctx)
{
   ctx->has_ip_address = false;
   ctx->device_ip = 0;
   bzero(ctx->ip_addr_str, sizeof(ctx->ip_addr_str));
   change_state(ctx, STATE_WAIT_CAPITAL_DEVICE);

   if (ctx->manager_sockfd > 0)
   {
      shutdown(ctx->manager_sockfd, SHUT_RDWR);
      ctx->manager_sockfd = -1;
   }

   bzero(&localListMsg, sizeof(localListMsg));
   publish_client_list();
   publish_in_list(0);

}
void* dsc_monitor_ip(void* thread_param)
{
   DscClient_Context *ctx = (DscClient_Context*) thread_param;
   sleep(5);
   logger.debug("--- Entered monitor IP ADDRESS Thread ---");
   int rc = 0;
   u32 iWait = 0;
   u8  state = 1;
   // while (1)
   for(;;)
   {
      if (ctx->has_ip_address)
      {
         char local_addr_str[MAX_IP_LEN];
         bzero(local_addr_str,MAX_IP_LEN);
         rc = get_ip_addr_str(local_addr_str, MAX_IP_LEN);
         if (rc)
         {
            logger.error("lost ip address - go back to basics");
            reset_connected_state(ctx);
            state = 0;
            iWait = 0;
         }
      } // if we are trying to send
      else
      {
         logger.highlight("checking if we have an IP address?");
         
         // get it real //
         rc = get_ip_addr_str(ctx->ip_addr_str, MAX_IP_LEN);
         if (rc != 0) {
            logger.highlight("still don't have IP Address <%d>",++iWait);
            state = 0;
         }
         else  {
            rc = get_bcast_addr_str(ctx->broadcast_addr_str, MAX_IP_LEN);
            if (rc != 0) {
               logger.fatal("not able to obtain self's broadcast IP (%d)");
               exit(-666);
            } //
            ctx->device_ip = ip_str_to_int(ctx->ip_addr_str);
            logger.highlight("RE-FOUND IP address <%s>",ctx->ip_addr_str);
            
            ctx->has_ip_address = true;
            state = 1;
         } 
         
      } // if we have 

      publish_wifi_state(state);  

      usleep(MICROSECS_PER_SEC*5);
   }
   return NULL;
}

/**
 * @fn      void* capDev_rx_heartbeat(void* thread_param)
 * @brief   handles receiving heartbeat from the bdm capital device
 * @param   arg = local context
 * @retval  n/a (NULL)
 **************************************************************/
void* capDev_rx_heartbeat(void* thread_param)
{   
   capDev_disc_heartbeat_t capDevInfo;
   DscClient_Context *ctx = (DscClient_Context*) thread_param;
   void *pHandleSub = NULL;
   u8 filter[BROKER_FILTER_MAX];

   logger.debug("--- Entered BDM Rx Thread ---");
   
   // create a handle
   if ((pHandleSub = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return NULL;
   }
   
   // subscribe to port status
   if ((bm_subscribe(pHandleSub, BROKER_CAPDEV_HEARTBEAT, strlen(BROKER_CAPDEV_HEARTBEAT))))
   {
      logger.error("Failed to subscribe to capdev heartbeat");
      return NULL;
   }

   // while (1)
   for(;;)
   {
      void *capDevHB = NULL;
      u32   bytesRead;
      int status;

      // wait up to 2 seconds for the next heartbeat
      if ((status = bm_poll(pHandleSub, SECS2MILLI(2))) <= 0)
      {         
         if (status < 0)
         {
            logger.error("ZMQ/broker Poll failed - %s",COMMON_ERR_STR);
            return NULL;
         } // if error

         // if we are connected and stopped receiving 
         if (ctx->state > STATE_WAIT_CAPITAL_DEVICE)
         {
            logger.warning("lost capdev heartbeat in <%s>", state_name(ctx->state));
            proxy_disconnect(ctx);
            bzero(&localListMsg, sizeof(localListMsg));
            publish_client_list();
         }
         
         capital_device_state = 0;
      } // if timeout or error in polling
      else
      {
         if ((status = bm_receive(pHandleSub, filter, (void*) &capDevHB, &bytesRead, 0)) <= 0)
         {
            logger.error("Failed (%d) bm_receive bdm heartbeat status",status);
            return NULL;
         }

            // just make sure that we have enough bytes
         do_assert(bytesRead >= sizeof(capDev_disc_heartbeat_t));

         // copy data over - initialize capital_device_state and update hb count
         memcpy(&capDevInfo, capDevHB, sizeof(capDevInfo));
         free(capDevHB);
         capital_device_state = capDevInfo.capDevState;
         capDev_rx_hb_miss = 0;
         capDev_hb_count++;
         if (!(capDev_hb_count % 4))
         {
            logger.highlight("----CapDev HB State <%d>---", capital_device_state );
         }
      } // else got a heartbeat from the capDev
      
      // determine if we change states
      if ((capital_device_state && (ctx->state == STATE_WAIT_CAPITAL_DEVICE)))
      {
         logger.info("CapitalDevice has connected");
         change_state((DscClient_Context* )ctx, STATE_DISCONNECTED);
      } // if connected and local state is waiting for connect
      else if ((!capital_device_state && (ctx->state != STATE_WAIT_CAPITAL_DEVICE)))
      {
         logger.info("CapitalDevice has disconnected in <%s>", state_name(ctx->state));
         publish_in_list(0);
         change_state(ctx, STATE_WAIT_CAPITAL_DEVICE);
         if (ctx->manager_sockfd > 0)
         {
            logger.warning("CapitalDevice has disconnected - shutdown udp socket");
            shutdown(ctx->manager_sockfd, SHUT_RDWR);
         }
         else
         {
            logger.info("CapitalDevice has disconnected - manager connection already not valid ");
         }
         if (ctx->isClientListTimerStarted)
         {
            logger.warning("Cancel List Timer in zmq");
            cancel_timer(ctx->client_list_timer);
         } // if we need to cancel a local timer
      } // if cap device disconnected while capital device is NOT waiting 
   } // end while //
   return NULL;
}

/**
 * @fn      void* tcp_rx_thread_func(void* thread_param)
 * @brief   handles receiving messages from the discovery manager
 * @param   arg = local context
 * @retval  n/a (NULL)
 * @NOTE    should consider using connUtils for this
 **************************************************************/
void* tcp_rx_thread_func(void* thread_param)
{
   DscClient_Context *ctx = (DscClient_Context*) thread_param;

   int listenfd;//, connfd;
   socklen_t cli_len;
   struct sockaddr_in svr_addr, cli_addr;

   logger.debug("Entering %s", __FUNCTION__);

   listenfd = socket(AF_INET, SOCK_STREAM, 0);
   if (listenfd < 0) {
      logger.error("ERROR opening socket");
      return NULL;
   }
   int reuse = 1;
   if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR,
                  &reuse, sizeof(reuse)) < 0) {
      logger.error("setsockopt(SO_REUSEADDR) failed");
      return NULL;
   }

#ifdef SO_REUSEPORT
   if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEPORT,
                  &reuse, sizeof(reuse)) < 0) {
      logger.error("setsockopt(SO_REUSEPORT) failed");
      return NULL;
   }
#endif

   bzero((char *) &svr_addr, sizeof(svr_addr));
   svr_addr.sin_family = AF_INET;
   svr_addr.sin_addr.s_addr = INADDR_ANY;
   svr_addr.sin_port = htons(DSC_CLI_LISTENING_TCP_PORT);
   if (bind(listenfd, (struct sockaddr *) &svr_addr,
            sizeof(svr_addr)) < 0) {
      logger.error("ERROR on bind()");
      return NULL;
   }
   listen(listenfd, 1);

   // FOREVER 
   for (;;) {
      if (ctx->state == STATE_WAIT_CAPITAL_DEVICE)
         YIELD_250MS;
      
      if (ctx->state == STATE_DISCONNECTED)  {
         // 1. When disconnected, wait for a Dsc server connection.
         //
         // 2. Once connected, drop into a receiving loop. Further connections
         //   will be ignored
         //
         // 3. Then if disconnected, return back to step #1
         logger.highlight("TCP_RX accept");
         cli_len = sizeof(cli_addr);
         ctx->manager_sockfd = accept(listenfd,
                                      (struct sockaddr *) &cli_addr,
                                      &cli_len);
         if (ctx->manager_sockfd < 0) {
            logger.error("ERROR on accept()");
            sleep(1);
            continue;
         }

         logger.highlight("Dsc server connected from %s, port %hu.",
                          inet_ntoa(cli_addr.sin_addr),
                          ntohs(cli_addr.sin_port));
         ctx->manager_ip = cli_addr.sin_addr;
         ctx->manager_port = cli_addr.sin_port;

         logger.info("Waiting for Dsc server assign message...");
         change_state(ctx, STATE_WAITING_FOR_ASSIGN);

         ctx->rx_client_list_time_out = false;

         // keep receiving data from the Manager
         for (;;) {
            ssize_t n;
            char buffer[MAX_PACKET_SIZE];

            if (ctx->state == STATE_WAITING_FOR_NETWORK_LIST) {              
               if (ctx->rx_client_list_time_out) {
                  logger.warning("client list timeout - breaking");
                  break;
               }
               YIELD_1MS;
               continue;
            } else if (ctx->state == STATE_DISCONNECTED) {
               // could arrive at state due to other threads
               // we will restart sending DR.
               if (g_has_bdm && capital_device_state == 0)
               {
                  logger.warning("cap device state went inactive");
                  change_state(ctx, STATE_WAIT_CAPITAL_DEVICE);
               }
               break;
            } else if (ctx->state == STATE_WAIT_CAPITAL_DEVICE) {
               // could arrive because loss capital device 
               // we will restart sending DR.
               break;
            }
            
            bzero(buffer, MAX_PACKET_SIZE);
            n = recv(ctx->manager_sockfd, buffer, MAX_PACKET_SIZE, 0);

            // processing the packet
            if (n < 0) {
               logger.error("recv(): ERROR reading from socket");
               if (capital_device_state == 0)
               {
                  change_state(ctx, STATE_WAIT_CAPITAL_DEVICE);
               }
               else
               {
                  change_state(ctx, STATE_DISCONNECTED);
               }
               break;
            } else if (n == 0) {  // loss of connection 
               logger.warning("Loss of Manager connection.");
               publish_in_list(0);

               // send proper state info
               int ls = STATE_DISCONNECTED;
               if (g_has_bdm) {
                  if (capital_device_state == 0) {                     
                     ls = STATE_WAIT_CAPITAL_DEVICE;
                  }
               }
               change_state(ctx, ls);
               
               break;
            } else if (n == MAX_PACKET_SIZE) {
               logger.error("package size exceeding limit.");
            } else {
               DscMsg* msg = (DscMsg*) buffer;
               if (!verify_msg_format(msg)) {
                  logger.error("Server msg format error.");
                  print_msg_short(msg);
                  continue;
               }
               if (g_verbose) print_msg(msg);
               if (msg->protocol_id == DSC_PROTO_ID_S2C &&
                   msg->command == DSC_CMD_SERVER_ASSIGN) {
                  uint32_t data_len = msg_data_len(msg);
                  if (data_len != DSCMSG_ASSIGN_PAYLOAD_LEN) {
                     logger.error("Invalid packet length: %d"
                                  "(expecting %d)",
                                  data_len,
                                  DSCMSG_ASSIGN_PAYLOAD_LEN);
                  } else {
                     logger.highlight("Dsc_Assign message received");
                     DscMsg_Assign* msg_assign = (DscMsg_Assign*) msg;
#define ASSIGN_ACK 0
                     if (msg_assign->result == ASSIGN_ACK) {
                        logger.info("Sending first heartbeat");
                        send_heart_beat(ctx);  // first heartbeat
                        logger.info("Started 2s timer for rx list");
                        start_timer(ctx->client_list_timer);
                        ctx->isClientListTimerStarted = true;
                        change_state(ctx, STATE_WAITING_FOR_NETWORK_LIST);
                     } else {
                        change_state(ctx, STATE_WAITING_FOR_ASSIGN);
                     }
                  }
               } else {
                  if (g_verbose) {
                     logger.error("Unknown protocol or command:"
                                  "%02X/%02X.",
                                  msg->protocol_id, msg->command);
                  }
               }
            }
         }  // recv from Manager

         // last connection ended. We will go back to listening
         logger.warning("closing remote manager connection");
         bzero(&localListMsg, sizeof(localListMsg));
         publish_client_list();
         publish_in_list(0);
         if (ctx->manager_sockfd > 0)
         {
            close(ctx->manager_sockfd);
            ctx->manager_sockfd = -1;
            if (capital_device_state)
               change_state(ctx, STATE_DISCONNECTED);
         }
         else
         {
            logger.warning("remote manager connection already torn down");
         }
      } // end if State is disconnected
   }

   close(listenfd);
   return NULL;
}

/**
 * @fn      void* tcp_tx_thread_func(void* thread_param)
 * @brief   handles sending heartbeat to client manager
 * @param   arg = local context
 * @retval  n/a (NULL)
 * @NOTE    should consider using connUtils for this
 **************************************************************/
void* tcp_tx_thread_func(void* thread_param)
{
   const DscClient_Context *ctx = (const DscClient_Context*) thread_param;
   const int SLEEP_INTERVAL_MS = 10;  // in ms
   int counter = 0;
   uint32_t heartbeats = 0;

   logger.debug("Entering %s", __FUNCTION__);
   for (;;) {
      if (ctx->state == STATE_CONNECTED) {
         if (counter == 0) {
            if (heartbeats % (4 * 1000 / ctx->cfg_hb_interval_ms) == 0) {
               // only showing print every 4 seconds
               logger.debug("[%s] Sending Discovery Manager Heartbeat #%u to %s:%d",
                            device_type_name(ctx->device_type),
                            heartbeats,
                            inet_ntoa(ctx->manager_ip),
                            ctx->manager_port);
               // only showing print every 4 seconds

            }
            send_heart_beat(ctx);
            heartbeats++;
            counter = ctx->cfg_hb_interval_ms / SLEEP_INTERVAL_MS;
         }
         counter--;
      } else {
         counter = 0;
      }
      usleep(SLEEP_INTERVAL_MS*1000);
   }
   return NULL;
}

/**
 * @fn      void* udp_rx_thread_func(void* thread_param)
 * @brief   handles receiving messages from the discovery manager
 * @param   arg = local context
 * @retval  n/a (NULL)
 * @NOTE    should consider using connUtils for this
 **************************************************************/
void* udp_rx_thread_func(void* thread_param)
{
   DscClient_Context *ctx = (DscClient_Context*) thread_param;

   logger.debug("Entering %s", __FUNCTION__);

   int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   if (sockfd < 0) {
      logger.error("ERROR opening socket");
      return NULL;
   }

   int enable = 1;
   setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,
              (const void *)&enable , sizeof(int));

   /*
    * build the server's Internet address
    */
   struct sockaddr_in listen_addr;
   bzero((char *) &listen_addr, sizeof(listen_addr));
   listen_addr.sin_family = AF_INET;
   listen_addr.sin_addr.s_addr = htonl(INADDR_ANY);
   listen_addr.sin_port = htons((uint16_t)DSC_CLI_LISTENING_UDP_PORT);
   if (bind(sockfd, (struct sockaddr *)&listen_addr,
            sizeof(listen_addr)) < 0) {
      logger.error("ERROR on binding");
      close(sockfd);
      return NULL;
   }

   struct sockaddr_in remote_addr;
   socklen_t client_len = sizeof(remote_addr);
   for (;;) {
      uint8_t rxbuf[MAX_PACKET_SIZE];
      bzero(rxbuf, MAX_PACKET_SIZE);
      ssize_t n = recvfrom(sockfd, rxbuf, MAX_PACKET_SIZE, 0,
                           (struct sockaddr *) &remote_addr, &client_len);
      if (n < 0) {
         logger.error("ERROR in recvfrom");
      } else if (n >= MAX_PACKET_SIZE) {
         logger.error("package size exceeding limit");
      } else {
         const char* svr_ip = inet_ntoa(remote_addr.sin_addr);
         logger.debug("Received UDP package at port %d"
                      " from %s (%d). Length: %d in <%s>",
                      DSC_CLI_LISTENING_UDP_PORT, svr_ip,
                      remote_addr.sin_port, n,state_name(ctx->state));

         // we don't only care if we are connected or waiting for this list
         if (ctx->state < STATE_WAITING_FOR_NETWORK_LIST)
         {
               continue;
         } // if we are not in connecteed
         
         DscMsg* msg = (DscMsg*) rxbuf;
         if (!verify_msg_format(msg)) {
            logger.warning("Un-recognized server msg.");
         } else if (msg->protocol_id == DSC_PROTO_ID_S2C) {
            uint32_t data_len;
            if (msg->command == DSC_CMD_SERVER_LIST_CHANGE) {
               DscMsg_ListDcptVirtual* m = (DscMsg_ListDcptVirtual*) rxbuf;
               data_len = msg_data_len(msg);
               if ((data_len - 4) % 16 != 0) {
                  logger.error("Invalid data length: %d "
                               "(expecting multiple of 16)", data_len);
                  break;
               }
               logger.debug("Received (%d bytes)client list (total: %d):",
                            n, m->num_of_devices);

               int i, client_cnt = (data_len - 4) / DSC_CLIENT_INFO_LEN;
               do_assert(client_cnt == m->num_of_devices);

               if (isClientListChanged(&localListMsg, m)) {
                   logger.debug("list response changed, write json file");
                   writeJsonFile(m, client_cnt, ctx->device_type, ctx->mac_addr_str, ctx->sw_ver_str);
               }

               // copy client list
               logger.debug("NEW LIST AVAILABLE(%d) - NUM <%d>",
                            capital_device_state,m->num_of_devices);
               memcpy(&localListMsg, rxbuf, localClientListSize);
               bool self_in_list = false;

               //
               // This is where we determine if we are in the device list
               //

               logger.debug("-------------------------");
               for (i = 0; i < client_cnt; i++)
               {
                  DscClientInfo* ci = &m->client_list[i];
                  struct in_addr ip = {ci->ip};
                  // XXX: determining self_in_list properly.
                  // IP is not good enough.
                  // type is also not ideal.
                  if (ci->device_type == ctx->device_type) {
                     self_in_list = true;
                  }
                  logger.debug("Device #%d: %s (%s) (%d/%d)",
                               ci->device_type,
                               device_type_name(ci->device_type),
                               inet_ntoa(ip),
                               ci->device_type,
                               ci->sub_device_type);
               } // end for list 

               logger.debug("-------------------------");
               cancel_timer(ctx->client_list_timer);
               ctx->isClientListTimerStarted = false;

               if (self_in_list) {
                  if (ctx->state != STATE_CONNECTED)
                  {
                     change_state(ctx, STATE_CONNECTED);
                     logger.highlight("DISCOVERY: <%s> 1st time fully Connected to the Manager",ctx->device_name);
                     publish_in_list(1);
                  }
                  publish_client_list();
               }
               else if (ctx->state >= STATE_WAITING_FOR_NETWORK_LIST)
               {
                  logger.highlight("DISCOVERY: <%s> NOT in list (%d) right now || wrong state (%d)",ctx->device_name, self_in_list, ctx->state);
                  if (ctx->state == STATE_CONNECTED)
                  {
                     change_state(ctx, STATE_WAITING_FOR_NETWORK_LIST);
                  }
                  start_timer(ctx->client_list_timer);
                  ctx->isClientListTimerStarted = true;
               }

            } else {
               logger.warning("Unknown command: %d.", msg->command);
            }
         } else {
            logger.warning("Unknown protocol: %d.", msg->protocol_id);
         }
      }
   }  // for(;;)

   close(sockfd);
   return NULL;
}

/**
 * @fn      void send_discover_request(int sockfd,
 *                                const char* network,
 *                                uint16_t port,
 *                                const DscMsg_DiscoverRqst* discovery_request_msg)
 * @brief   handles sending discovery message to the discovery message
 * @param   arg = local context
 * @retval  n/a (NULL)
 * @NOTE    should consider using connUtils for this
 **************************************************************/
static void send_discover_request(int sockfd,
                                  const char* network,
                                  uint16_t port,
                                  const DscMsg_DiscoverRqst* discovery_request_msg)
{
   struct sockaddr_in remote_addr;
   bzero(&remote_addr, sizeof(remote_addr));
   remote_addr.sin_family = AF_INET;
   remote_addr.sin_addr.s_addr = ip_str_to_int((char*)network);
   remote_addr.sin_port = htons(port);

   int flags = 0;
   ssize_t n = sendto(sockfd, discovery_request_msg,
                      sizeof(DscMsg_DiscoverRqst), flags,
                      (const struct sockaddr*) &remote_addr, sizeof(remote_addr));
   if (n < 0) {
      logger.error("ERROR sendto()");
   }
}


/**
 * @fn      void* udp_tx_thread_func(void* thread_param)
 * @brief   handles sending discovery message to the discovery message
 * @param   arg = local context
 * @retval  n/a (NULL)
 * @NOTE    should consider using connUtils for this
 **************************************************************/
void* udp_tx_thread_func(void* thread_param)
{
   const DscClient_Context *ctx = (const DscClient_Context*) thread_param;
   const int SLEEP_INTERVAL_MS = 10;  // in ms
   int counter = 0;

   logger.debug("Entering %s", __FUNCTION__);

   int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   if (sockfd < 0) {
      logger.error("ERROR opening socket");
      return NULL;
   }

   int broadcast_enable = 1;
   if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST,
                  &broadcast_enable, sizeof(broadcast_enable)) == -1) {
      logger.error("setsockopt (SO_BROADCAST)");
      close(sockfd);
      return NULL;
   }

   DscMsg_DiscoverRqst discovery_request_msg;
   DscMsg_DiscoverRqst_Init(&discovery_request_msg, ctx->device_ip, ctx->device_name,
                            ctx->device_type, ctx->device_sub_type, htonl(ctx->device_feature_bitmask));
   for (;;) {
      if (ctx->state == STATE_DISCONNECTED && (ctx->has_ip_address)) {
         if (counter == 0) {
            // Need this additional delay to avoid a potential race
            // condition.
            // TCP Listen thread: Set_state_to_waiting -> call_accept()
            //                                         ^ Here the thread
            //  might be switched out, and here we send the discovery
            //  packet to the manager and manager trying to connect, all
            //  before the accept() is called, then the server connection
            //  will fail.  For now, we introduced a delay as a workaround
            //  to mitigate it.
            usleep(1000*2000);
            capDev_rx_hb_miss++;
            if (capDev_rx_hb_miss >= 3)
            {
               logger.warning("Loss of capDev state (must've crashed) - go back to WAITING for it to return");
               change_state((DscClient_Context* )ctx, STATE_WAIT_CAPITAL_DEVICE);
               capDev_rx_hb_miss = 0;
               proxy_disconnect(ctx);
            }
            else
            {
               uint16_t port = DSC_SVR_LISTENING_UDP_PORT;
               logger.highlight("[%d]Sending discovery request(%d/%d/%d) in <%s> to %s:%hu",
                                capDev_rx_hb_miss,
                                discovery_request_msg.device_type,
                                discovery_request_msg.device_sub_type,
                                ctx->has_ip_address,
                                state_name(ctx->state), 
                                ctx->broadcast_addr_str, port);
               send_discover_request(sockfd, ctx->broadcast_addr_str,
                                     port, &discovery_request_msg);
               counter = ctx->cfg_dr_interval_ms / SLEEP_INTERVAL_MS;
            }
         }
         counter--;
      } else {
         counter = 0;
      }
      usleep(SLEEP_INTERVAL_MS*1000);
   }
   return NULL;
}

/**
 * @fn      int _get_sw_ver_str(char* buf, uint32_t length)
 * @brief   constructs link-sw-version string by reading from /sn/version.txt file
 * @param   buf = buffer which will contain decorated sw-version string
 * @param   length = maximum length of string expected
 * @retval  non zero on error.
 **************************************************************/
static int _get_sw_ver_str(char* buf, uint32_t length)
{
    FILE *fp;
    char major[16];
    char minor[16];
    char build[16];
    char *majorVal; 
    char *minorVal;
    char *buildVal;

    fp = fopen("/sn/version.txt", "r");
    if (fp == NULL) {
        return 1;
    }
    major[0] = '\0';
    minor[0] = '\0';
    build[0] = '\0';

    if ((fgets(major, sizeof(major), fp)) != NULL) { 
        if ((fgets(minor,sizeof(minor), fp)) != NULL) { 
            if ((fgets(build, sizeof(build), fp)) != NULL) {
            }
        }
    } 
    fclose(fp);
    majorVal = strchr(major,'=');
    minorVal = strchr(minor,'=');
    buildVal = strchr(build, '=');
    buf[0] = '\0';
    if (majorVal && minorVal && buildVal) {
        uint32_t version_length;
        //to go pass '=' char
        majorVal++;
        minorVal++;
        buildVal++;

        version_length = strlen(majorVal) + strlen(minorVal) + strlen(buildVal); 
        if(version_length) {
            //to trim new line char at the end
            majorVal[strlen(majorVal) - 1 ] = '\0';
            minorVal[strlen(minorVal) - 1] = '\0';
            buildVal[strlen(buildVal) - 1] = '\0';
            snprintf(buf, length, "%s.%s.%s", majorVal, minorVal, buildVal);
            logger.info("link-sw-ver: %s", buf);
        }
    }
    if (!strlen(buf)) {
        logger.info("link-sw-version EMPTY!!!");
        return 2;
    }
    return 0;
}

/**
 * @fn      int main(int argc, char *argv[])
 * @brief   main task - performs system initialization and waits until exit
 * @param   argc/argv ==> command line arguments
 * @retval  return status 
 **************************************************************/
int main(int argc, char *argv[])
{
   unsigned int i;
   int rc;

   if (argc > 1 && !strcmp(argv[1],"-V"))
   {
      printf("%s", version);
      return 0;
   }

   logger.highlight("Discovery Service Client. Last modified at %s %s", __DATE__, __TIME__);
   DscClient_Context* ctx = (DscClient_Context*)calloc(sizeof(DscClient_Context),1);
   assert(ctx);

   // create publisher handle
   pubhandle = bm_publisher();

   // not sure why we are doing this? 
   sleep(5);

   logger.highlight("dsc_client: continuing startup");
   publish_in_list(0);

   ctx->cfg_hb_interval_ms = HB_INTERVAL_MS;
   ctx->cfg_dr_interval_ms = DR_INTERVAL_MS;
   rc = parse_cmd_line(ctx, argc, argv);
   if (rc != 0) {
      if (rc == 1) {
         usage();
         return 0;
      } else if(rc == 2) {
         return 0; //displayed version information on the console
      } else {
         logger.error("command line parsing error");
         return 1;
      }
   }
   strncpy(ctx->device_name, cmd_device_name, MAX_DEVICE_NAME_LEN);
   set_types(ctx, ctx->device_name);

   int service_conf_from_db;
   service_conf_from_db = get_services_mask(ctx->device_type);
   if (service_conf_from_db < 0) {
      logger.highlight("dsc_client: configure services value read error from db");
      return 1;
   }
   
   ctx->device_feature_bitmask = service_conf_from_db;

   logger.info("Name: %s", ctx->device_name);
   logger.info("Type: %s", device_type_name(ctx->device_type));
   logger.info("SubType: %s", device_subtype_name(ctx->device_type, ctx->device_sub_type));
   logger.info("Discovery Request Interval: %d ms", ctx->cfg_dr_interval_ms);
   logger.info("Heartbeat Interval: %d ms", ctx->cfg_hb_interval_ms);
   logger.info("feature mask: %0X", ctx->device_feature_bitmask);

   // Using a shell script (netinfo.sh) to get current "primary" IP &
   // broadcast address
#if 0
   strncpy(ctx->ip_addr_str, "192.168.1.105", sizeof(ctx->ip_addr_str));
   strncpy(ctx->broadcast_addr_str, "192.168.1.255", sizeof(ctx->ip_addr_str));
#else
   int waitIp = 1;
   while (waitIp)
   {
      rc = get_ip_addr_str(ctx->ip_addr_str, MAX_IP_LEN);
      if (rc != 0)
      {
         logger.error("not able to obtain self's IP(%d)",waitIp++);
      }
      else
      {
         rc = get_bcast_addr_str(ctx->broadcast_addr_str, MAX_IP_LEN);
         if (rc != 0)
         {
            logger.error("not able to obtain self's broadcast IP (%d)",waitIp++);
         }
         else
         {
            waitIp = 0;
         }
      } // else 
      sleep(5);  
   }
   rc = get_mac_addr_str(ctx->mac_addr_str, MAC_ADDR_STR_LEN);
   if (rc != 0) {
      logger.fatal("not able to obtain MAC address");
      return 1;
   }
   rc = _get_sw_ver_str(ctx->sw_ver_str, SW_VER_STR_LEN);
   if (rc != 0) {
      logger.fatal("not able to obtain LINK SW version");
      return 1;
   }
#endif
   ctx->has_ip_address = true;
   ctx->device_ip = ip_str_to_int(ctx->ip_addr_str);

   logger.info("IP: %s <%X>", ctx->ip_addr_str,ctx->device_ip);
   logger.info("Broadcast: %s", ctx->broadcast_addr_str);

   if (g_has_bdm) {
      ctx->state = STATE_WAIT_CAPITAL_DEVICE;
   } else {
      ctx->state = STATE_DISCONNECTED;
   }

   /////////////////////////////////////////
   // tell everyone its down - because 
   //   proxy_disconnect(ctx);
   /////////////////////////////////////////
   
   ctx->client_list_timer = create_timer(CLIENT_LIST_TIMOUT_MS,
                                         rx_client_list_timeout, ctx);

   pthread_t tcp_rx_thread;
   pthread_t tcp_tx_thread;
   pthread_t udp_rx_thread;
   pthread_t udp_tx_thread;
   pthread_t capDev_rx_thread;
   pthread_t ipMonitorThread;

   typedef struct ThreadStruct {
      pthread_t* thread;
      void* (*func)(void* thread_param);
      const char* name;
      int active;
   } ThreadStruct;

   ThreadStruct a[] = {
      {&tcp_rx_thread, tcp_rx_thread_func, "tcp rx thread", 1},
      {&tcp_tx_thread, tcp_tx_thread_func, "tcp tx thread", 1},
      {&udp_rx_thread, udp_rx_thread_func, "udp rx thread", 1},
      {&udp_tx_thread, udp_tx_thread_func, "udp tx thread", 1},
      {&capDev_rx_thread, capDev_rx_heartbeat, "capDev rx HB", !cmd_standalone_no_bdm},
      {&ipMonitorThread, dsc_monitor_ip, "ipaddr monitor thread",1},
   };

   for (i = 0; i < sizeof(a)/sizeof(ThreadStruct); i++) {
      if(a[i].active) {
         if (pthread_create(a[i].thread, NULL, a[i].func, ctx)) {
            logger.error("Error creating %s", a[i].name);
            return 1;
         }
      }
   }

   // wait for the threads to terminate
   for (i = 0; i < sizeof(a)/sizeof(ThreadStruct); i++) {
      if(a[i].active) {
         if (pthread_join(*(a[i].thread), NULL)) {
            logger.error("Error joining thread %s", a[i].name);
            return 1;
         } else {
            logger.info("%s exited.", a[i].name);
         }
      }
   }

   free(ctx);
   return 0;
}

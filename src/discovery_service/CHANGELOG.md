# Discovery Service Changelog

## v0.1: 2018-06-29

Import from `LENS_iFull` 2018-03-09 version, and

* disable special treat to d2/d25
* disable LENS Dsc manager fakes itslef as a Dsc Client even when there isn't one
* disable LENS client poses as RESERVED device

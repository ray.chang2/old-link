#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "gtest/gtest.h"

#define UNITTEST
#include "discovery_service/util.c"
#include "discovery_service/dsc.c"

class DscTest : public ::testing::Test {
  protected:
    DscTest() {
    }
};

TEST_F(DscTest, device_type_name) {
    const char* s;

    s = device_type_name(DEVICE_TYPE_SHAVER);
    EXPECT_STRNE(s, NULL);

    s = device_type_name(-1);
    EXPECT_STREQ(s, device_type_name(0));

    s = device_type_name(9999);
    EXPECT_STREQ(s, device_type_name(0));
}

TEST_F(DscTest, calc_crc) {
    uint8_t res;

    uint8_t buf[2] = {1, 0};
    res = calc_crc(buf, 2);
    EXPECT_EQ(res, 255);

    uint8_t buf2[2] = {0, 0};
    res = calc_crc(buf2, 2);
    EXPECT_EQ(res, 0);
}

TEST_F(DscTest, ifull_msg) {
    DscMsg_DiscoverRqst msg;

    bzero(&msg, sizeof(DscMsg));
    EXPECT_FALSE(verify_msg_format((DscMsg*)&msg));

    DscMsg_DiscoverRqst_Init(&msg, "test", 1);
    EXPECT_TRUE(verify_msg_format((DscMsg*)&msg));
    EXPECT_STREQ(msg.device_dscr, "test");
    EXPECT_EQ(msg.device_type, 1);
}

TEST_F(DscTest, msg_list) {
    DscMsg_ListDcptVirtual msg;

    DscMsg_ListDcpt_Init(&msg);
    EXPECT_EQ(msg.num_of_devices, 0);

    DscClientInfo info;
    DscClientInfo_init(&info);
    info.device_type = 1;

    DscMsg_ListDcpt_Add_Device(&msg, &info);
    EXPECT_EQ(msg.num_of_devices, 1);

    DscMsg_ListDcpt_Add_Device(&msg, &info);
    EXPECT_EQ(msg.num_of_devices, 2);

    info.device_type = 3;
    DscMsg_ListDcpt_Add_Device(&msg, &info);
    EXPECT_EQ(msg.num_of_devices, 3);

    for (int i=0; i < DEVICE_TYPE_MAX; i++) {
        DscMsg_ListDcpt_Add_Device(&msg, &info);
    }
    int ret = DscMsg_ListDcpt_Add_Device(&msg, &info);
    EXPECT_NE(ret, 0);
    EXPECT_EQ(msg.num_of_devices, DEVICE_TYPE_MAX -1);
}

TEST_F(DscTest, get_netinfo) {
    char buf[99];
    const char* cmd;
    int ret;

    cmd = "foo";
    ret = get_cmd_result(buf, 99, cmd);
    EXPECT_EQ(ret, 2);

    cmd = "echo -n 1";
    ret = get_cmd_result(buf, 99, cmd);
    EXPECT_EQ(ret, 0);
    EXPECT_STREQ(buf, "1");

    cmd = "true";
    ret = get_cmd_result(buf, 99, cmd);
    EXPECT_EQ(ret, 2);
}

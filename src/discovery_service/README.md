# Discovery Protocol Service

## Key Timing Configurations

 Name |    Type  |  Default (ms)
------|----------|--------------
Client Tx Discovery Request Interval | Interval | 1000
Client Tx Heartbeat Interval | Interval | 600
Manager Rx Client Heartbeat Timeout | Timeout | 2000

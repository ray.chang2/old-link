/** Discovery Service Protocol Header
 *
 * Copyright [2018] Smith-Nephew Inc.
 */

#ifndef DSC_H
#define DSC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <sys/types.h>
#include <linux/types.h>
#include "CT_defines.h"
   
#define DISCOVERY_PROTOCOL_VERSION 0x21

#define DSC_SVR_LISTENING_UDP_PORT 50000
#define DSC_CLI_LISTENING_TCP_PORT 50010
#define DSC_CLI_LISTENING_UDP_PORT 50020

#define DSC_PROTO_ID_C2S 0x33
#define DSC_PROTO_ID_S2C 0xCC

#define MAX_DEVICE_DSCR_LEN 16
#define MAX_DEVICE_NAME_LEN (MAX_DEVICE_DSCR_LEN -1)  // - one trailing '\0'
#define MAX_PACKET_SIZE 1500
#define MAX_IP_LEN 16

#define CLIENT_LIST_TIMOUT_MS 2000     // used by Dsc Client

#define DR_INTERVAL_MS 1000     // used by Dsc Client
#define DR_RESP_TIMEOUT_MS 500  // used by Dsc Client & Manager

#define HB_INTERVAL_MS 600   // used by Dsc Client
#define HB_TIMEOUT_MS 2000   // user by Dsc Manager

#define MAX_ALLOWED_HB_TIMEOUT 10

#define SERVER_VERSION 1
#define SERVER_BUILD 0
#define DEFAULT_FEATURE_MASK 0
#define DEVICE_SUBTYPE_RESERVED 0

enum {
    ACK = 1,
    NACK = 0
};

enum {
    DSC_CMD_CLIENT_START = 0,
    DSC_CMD_CLIENT_DISCOVER_RQST,
    DSC_CMD_CLIENT_HEART_BEAT,
    DSC_CMD_CLIENT_LIST_RQST,
    DSC_CMD_CLIENT_MAX
};
enum {
  DEVICE_STATE_CLIENT_NOT_CONNECTED,
  DEVICE_STATE_CLIENT_CONNECTED_NOT_READY,
  DEVICE_STATE_CLIENT_READY,
  DEVICE_STATE_MAX,
};
enum {
    DSC_CMD_SERVER_START = 0,
    DSC_CMD_SERVER_ASSIGN,
    DSC_CMD_SERVER_LIST_CHANGE,
    DSC_CMD_SERVER_MAX
};

enum DSC_DEVICE_TYPE {
    DEVICE_TYPE_RESERVED = 0,
    DEVICE_TYPE_SHAVER = 1,             // D2
    DEVICE_TYPE_VIDEO = 2,
    DEVICE_TYPE_COBLATION = 3,          // Werewolf
    DEVICE_TYPE_TABLET = 4,
    DEVICE_TYPE_FLUID_MANAGEMENT = 5,   // D25 single
    DEVICE_TYPE_FLUID_MANAGEMENT2 = 6,  // future dual pump
    DEVICE_TYPE_ROBOT = 7,              // future robot
    DEVICE_TYPE_Other = 8,              // the rest
    DEVICE_TYPE_MAX
} DISCOVERY_DEVICE_TYPES;


#define NEW_FORMAT
typedef struct  __attribute__((packed)) DscMsgHeader {
    uint8_t protocol_id;
    uint8_t command;
    uint8_t version;
    uint8_t seq_number;
    uint32_t data_len_nw;
    uint8_t data[0];  // stub member, length unknown
    // uint8_t crc;  // after data. Commented out since data length unknown.
                     // BTW, we call it CRC for legacy reasons.
                     // It actually is just a checksum
} DscMsgHeader, DscMsg;
#define DSCMSG_HEADER_SIZE 8
#if __GNUC__ > 5
_Static_assert(DSCMSG_HEADER_SIZE == sizeof(DscMsgHeader),
        "DscMsgHeader size error");
#endif

/*
 * Dsc Messages
 */

// 7.1.1 Dsc Client Discovery (Dsc_DiscoverRqst)
typedef struct  __attribute__((packed)) DscMsg_DiscoverRqst {
    DscMsgHeader header;
    uint32_t ip;
    uint8_t device_type;
    uint8_t device_sub_type;
    uint8_t client_major_ver;
    uint8_t client_minor_ver;
    uint8_t client_build_num;
    uint8_t max_ver_support;
    char device_dscr[MAX_DEVICE_DSCR_LEN];
    uint32_t feature_mask;
    uint8_t reserved[20];
    uint8_t crc;
} DscMsg_DiscoverRqst;
#define DSCMSG_DISOVERRQST_LEN 50
#if __GNUC__ > 5
_Static_assert(DSCMSG_HEADER_SIZE + DSCMSG_DISOVERRQST_LEN + 1 ==
        sizeof(DscMsg_DiscoverRqst), "DscMsg_DiscoverRqst size error");
#endif

// 7.1.2 Dsc Client Heartbeat (Dsc_HeartBeat)
typedef struct  __attribute__((packed)) DscMsg_HeartBeat {
    DscMsgHeader header;
    uint8_t device_id;
    uint8_t device_state;
    uint8_t max_ver_support;
    uint8_t reserved[5];
    uint8_t crc;
} DscMsg_HeartBeat, *pHBMsg;
#define DSCMSG_HB_PAYLOAD_LEN 8
#if __GNUC__ > 5
_Static_assert(DSCMSG_HEADER_SIZE + DSCMSG_HB_PAYLOAD_LEN + 1 ==
        sizeof(DscMsg_HeartBeat), "DscMsg_HeartBeat size error");
#endif

// 7.1.3 Dsc Client Get Network Topology List (Dsc_listRqst)
typedef struct  __attribute__((packed)) DscMsg_ListRqst {
    DscMsgHeader header;
    uint8_t device_id;
    uint8_t crc;
} DscMsg_ListRqst;
#define DSCMSG_LISTRQST_PAYLOAD_LEN 1
#if __GNUC__ > 5
_Static_assert(DSCMSG_HEADER_SIZE + DSCMSG_LISTRQST_PAYLOAD_LEN + 1 ==
        sizeof(DscMsg_ListRqst), "DscMsg_ListRqst size error");
#endif

// 7.2.1 Dsc Discovery Response (Dsc_Assign)
typedef struct  __attribute__((packed)) DscMsg_Assign {
    DscMsgHeader header;
    uint8_t result;
    uint32_t ip;
    uint32_t reserved0;
    uint32_t feature_mask;
    uint8_t reserved1[20];
    uint8_t crc;
} DscMsg_Assign;
#define DSCMSG_ASSIGN_PAYLOAD_LEN 33
#if __GNUC__ > 5
_Static_assert(DSCMSG_HEADER_SIZE + DSCMSG_ASSIGN_PAYLOAD_LEN + 1 ==
        sizeof(DscMsg_Assign), "DscMsg_Assign size error");
#endif

typedef struct  __attribute__((packed)) DscClientInfo {
    uint32_t ip;
    uint8_t sub_device_type;
    uint8_t device_type;
    uint8_t client_major_ver;
    uint8_t client_minor_ver;
    uint8_t client_build_num;
    uint8_t max_ver_support;
    uint8_t pad[2];
    uint32_t feature_mask;
} DscClientInfo;
#define DSC_CLIENT_INFO_LEN 16
#if __GNUC__ > 5
_Static_assert(DSC_CLIENT_INFO_LEN == sizeof(DscClientInfo),
        "DscClientInfo size error");
#endif

// 7.2.2 Dsc Network Topology List (Dsc_listChnge / Dsc_listDcpt)
// This struct is of variable size. We call it virtual because it's not a
// true memory mapping of a List Description message
typedef struct  __attribute__((packed)) DscMsg_ListDcptVirtual {
    DscMsgHeader header;
    uint8_t num_of_devices;
    uint8_t server_dev_type;
    uint8_t server_dev_subtype;
    uint8_t server_if_spt_bitmask;

    // The following field is for only place-holder purpose.
    // The actual number of clients will be determined by num_of_devices above.
    // And the one-byte crc will follow right after.
#define MAX_DEVICE_NUM (DEVICE_TYPE_MAX -1)    // no type 0 device
    DscClientInfo client_list[MAX_DEVICE_NUM];

    uint8_t _crc;  // this will be the true crc only when client_list is full
} DscMsg_ListDcptVirtual, *pListVirt, DscMsg_ListDcptVirtual_t;
#define CLIENT_LIST_MSG_LENGTH (sizeof(DscMsg_ListDcptVirtual_t))
#define DSCMSG_LISTDCP_PAYLOAD_HEADER_SIZE (4)  // the 4 uint8_t fields
#define DSCMSG_LISTDCP_VIRTUAL_SIZE (DSCMSG_HEADER_SIZE + 4 + \
        16 * MAX_DEVICE_NUM + 1)
#if __GNUC__ > 5
_Static_assert(DEVICE_TYPE_MAX >= MAX_DEVICE_NUM, "");
_Static_assert(DSCMSG_LISTDCP_VIRTUAL_SIZE == sizeof(DscMsg_ListDcptVirtual),
        "DscMsg_ListDcptVirtual size error");
#endif

const char* device_type_name(int device_type);
const char* device_subtype_name(int device_type, int device_subtype);


int DscMsg_DiscoverRqst_Init(DscMsg_DiscoverRqst* msg, uint32_t device_ip,
        const char* device_name, int device_type, uint8_t device_sub_type, int feature_bit_mask);
void DscMsg_Assign_Init(DscMsg_Assign* msg, uint32_t ip);
int DscMsg_HeartBeat_Init(DscMsg_HeartBeat* msg,uint8_t device_state, uint8_t device_type);
int DscMsg_ListDcpt_Init(DscMsg_ListDcptVirtual* msg);
int DscClientInfo_init(DscClientInfo* info);
int DscMsg_ListDcpt_Add_Device(DscMsg_ListDcptVirtual* msg,
        const DscClientInfo* info);


uint32_t ip_str_to_int(char* s);
int full_msg_len(int data_size);
int get_msg_len(const DscMsg* msg);

#ifdef NEW_FORMAT
uint32_t msg_data_len(const DscMsg* msg);
void msg_set_data_len(DscMsg* msg, uint32_t new_len);
uint32_t msg_add_data_len(DscMsg* msg, int increase);
#endif
bool verify_msg_format(const DscMsg* msg);
void print_msg_short(const DscMsg* msg);
void print_msg(const DscMsg* msg);

bool verify_msg_device_list(const DscMsg_ListDcptVirtual* msg);
void print_msg_device_list(const DscMsg_ListDcptVirtual* msg);

uint8_t calc_crc(const uint8_t* buf, int len);
int get_ip_addr_str(char* buf, uint32_t buf_len);
int get_bcast_addr_str(char* buf, uint32_t buf_len);
int get_mask_addr_str(char* buf, uint32_t buf_len);
int get_mac_addr_str(char* buf, uint32_t buf_len);

#ifdef __cplusplus
}
#endif

#endif

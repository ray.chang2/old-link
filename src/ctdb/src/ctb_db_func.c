/*###############################################################################
#   Filename: ctb_db_func.c
#
#   Functions to access elements of the table in the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  1/30/2019   Initial Version
#  01.00  CG team  2/21/2019   Version shared with S&N team.
#  01.01  CG team  3/13/2019   Fixed defect in API for mod_time, tval_ref
#                              variable was not getting assigned the time
#                              details and was NULL, instead tval was getting
#			       assigned the time value.
#  01.02  CG team  3/18/2019   verify_query_input function not needed, hence
#                              has been taken out.
##############################################################################*/

#include <string.h>
#include "ctb_db_api.h"
#include "ctb_data.h"

/*------------------------------------------------------------------------------
#   Function definitions
------------------------------------------------------------------------------*/

int get_column_details(char * tablename, char * columnname, 
		                            int * type, int *size)
{
	int i = 0;
	int j = 0;

	if((NULL == tablename) ||(NULL == columnname) || (NULL == type))
		return DB_INVALID_PARAM;

	*type = INVALID_TYPE;

	do{
		if(0 == strcmp(tablename, (char *) ctb_tablenames[i]))
		{
			do
			{
				if(0 == strcmp(columnname, 
						ctb_tables[i].column[j]))
				{
					*type = ctb_tables[i].type[j];
					*size = ctb_tables[i].size[j];
					break;
				}
				j = j + 1;
			} while(ctb_tables[i].column[j] != NULL);
			if (INVALID_TYPE == *type)
			       return DB_INVALID_COLUMNNAME;	
			break;
		}
		i = i + 1;
	} while(NULL != ctb_tablenames[i]);
	if (INVALID_TYPE == *type)
	       return DB_INVALID_COLUMNNAME;	
	
	return DB_OK;
}

int create_generic_select_query(
		char * query,
		char * tablename,
		char * columnname,
		char * column_ref,
		int    ref_type,
		void * column_ref_value)
{
	int * intval = 0;
	long int * tval = 0;
	char buf[PATH_FIELD_SIZE] = "";

	switch (ref_type)
	{
		case INT:
			intval = (int *) column_ref_value;
		sprintf(query, "SELECT %s FROM %s WHERE %s = '%d' ;", 
			    	columnname,
			    	tablename,
			    	column_ref,
			    	*intval);
				break;

		case CHAR:
			strncpy(buf, (char *) column_ref_value, 
					strlen((char *)column_ref_value));
		sprintf(query, "SELECT %s FROM %s WHERE %s = '%s' ;", 
			    	columnname,
			    	tablename,
			    	column_ref,
				buf);
				break;

		case DATETIME:
			tval = (long int *) column_ref_value;
		sprintf(query, "SELECT %s FROM %s WHERE %s = '%ld' ;", 
			    	columnname,
			    	tablename,
			    	column_ref,
			    	*tval);
				break;

		default:
			return DB_INVALID_OPERATION;
		}

	return DB_QUERY_OK;

}

int create_generic_update_query( 
		char * query,
		char * tablename,
		char * columnname,
		int    type,	
		void * value,
		int    value_size,
		char * column_ref,
		void * column_ref_value,
		int    ref_type)
{
	int * intval = 0;
	long int * tval = 0;
	int * intval_ref = 0;
	long int * tval_ref = 0;
	char query_string[MAX_QUERY_SIZE] = "";
	char query_str_2[REF_QUERY_SIZE] = "";
	char col_string[PATH_FIELD_SIZE] = "";
	char buf[PATH_FIELD_SIZE+1] = "";

	switch (type)
	{
		case INT:
	    		intval = (int *)  value;
		    sprintf(query_string, "UPDATE %s SET %s = %d ", 
				tablename,
				columnname,
				*intval);
		    		break;

		case CHAR:
			strncpy(col_string, (char *)value, 
					(size_t) value_size);
		    sprintf(query_string, "UPDATE %s SET %s = '%s' ", 
				tablename,
				columnname,
				col_string);
		    		break;

		case DATETIME:
	    		tval = (long int *) value;
		    sprintf(query_string, "UPDATE %s SET %s = %ld ", 
				tablename,
				columnname,
				*tval);
		    		break;

		default:
	    		return DB_INVALID_OPERATION;
	}

	if(NULL != column_ref) 
	{
		switch(ref_type)
		{
			case INT:
	    			intval_ref = (int *)  column_ref_value;
		    	sprintf(query_str_2, "WHERE %s = %d ; ", 
					column_ref,
					*intval_ref);
		    		break;

			case CHAR:
				strncpy(buf, (char *)column_ref_value,
					(strlen((char *)column_ref_value)));
			    sprintf(query_str_2, "WHERE %s = '%s' ; ", 
					column_ref,
					buf);
		    		break;

			case DATETIME:
		    		tval_ref = (long int *) column_ref_value;
			    sprintf(query_str_2, "WHERE %s = %ld ; ", 
					column_ref,
					*tval_ref);
		    		break;

			default:
		    		return DB_INVALID_OPERATION;
		}
	}
	else
		sprintf(query_str_2, "WHERE id = %d ; ",
				DEFAULT_PRIMARY_KEY_VALUE);

	sprintf(query, "%s", 
		strncat(query_string, query_str_2, strlen(query_str_2)));

	return DB_QUERY_OK;
}

/* END OF FILE: ctb_db_func.c */

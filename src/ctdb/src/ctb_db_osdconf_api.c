/*###############################################################################
#   Filename: ctb_db_osdconf_api.c
#
#   APIs to access elements of the OSD Configuration table in the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  2/12/2019   Initial Version
#  01.00  CG team  2/21/2019   Version shared with S&N team.
#  01.01  CG team  3/19/2019   Added APIs to get x & y coordinates for Port A
#                              and Port B.
#  01.02  CG team  3/25/2019   Added APIs to set x & y coordinates for Port A
#                              and Port B.
#  01.03  CG team  5/14/2019   Made the set_osd_ww_icon_loc_coordinates API
#                              consistent with D2 APIs for OSD. This was taking
#                              x & y as pointer parameters. 
##############################################################################*/

#include "ctb_db_api.h"

/*------------------------------------------------------------------------------
#   APIs for OSD configuration table 
------------------------------------------------------------------------------*/
int get_osd_modby(int * modby)
{
	int retval = 0;

	retval = ctb_getField(OSD_CONF_TABLE, 
			      OSD_CONF_MOD_BY,
			      (void *) modby, 
			      OSD_CONF_MOD_BY_SIZE,
			      NULL, NULL);
	return retval;
}

int set_osd_modby(int modby)
{
	int retval = 0;

	retval = ctb_setField(OSD_CONF_TABLE, 
			      OSD_CONF_MOD_BY,
			      (void *) &modby, 
			      OSD_CONF_MOD_BY_SIZE,
			      NULL, NULL);
	return retval;
}

int get_osd_modtime(long int * modtime)
{
	int retval = 0;

	retval = ctb_getField(OSD_CONF_TABLE, 
			      OSD_CONF_MOD_TIME,
			      (void *) modtime, 
			      OSD_CONF_MOD_TIME_SIZE,
			      NULL, NULL);
	return retval;
}


int set_osd_modtime(long int modtime)
{
	int retval = 0;

	retval = ctb_setField(OSD_CONF_TABLE, 
			      OSD_CONF_MOD_TIME,
			      (void *) &modtime, 
			      OSD_CONF_MOD_TIME_SIZE,
			      NULL, NULL);
	return retval;
}

int get_osd_status(int * status)
{
	int retval = 0;

	retval = ctb_getField(OSD_CONF_TABLE, 
			      OSD_CONF_STATUS,
			      (void *) status, 
			      OSD_CONF_STATUS_SIZE,
			      NULL, NULL);
	return retval;
}

int set_osd_status(int status)
{
	int retval = 0;

	retval = ctb_setField(OSD_CONF_TABLE, 
			      OSD_CONF_STATUS,
			      (void *) &status, 
			      OSD_CONF_STATUS_SIZE,
			      NULL, NULL);
	return retval;
}

int get_osd_port_a_coordinates(int * x, int * y)
{
	int retval = 0;

	retval = ctb_getField(OSD_CONF_TABLE, 
			      OSD_CONF_PORT_A_X, 
			      (void *) x, 
			      OSD_CONF_PORT_A_LOC_SIZE,
			      NULL, NULL);
	if(retval >=  DB_OK)
	{
		retval = ctb_getField(OSD_CONF_TABLE, 
		      		OSD_CONF_PORT_A_Y, 
		      		(void *) y, 
		      		OSD_CONF_PORT_A_LOC_SIZE,
		      		NULL, NULL);
	}
	return retval;
}

int get_osd_port_b_coordinates(int * x, int * y)
{
	int retval = 0;

	retval = ctb_getField(OSD_CONF_TABLE, 
			      OSD_CONF_PORT_B_X, 
			      (void *) x, 
			      OSD_CONF_PORT_B_LOC_SIZE,
			      NULL, NULL);
	if(retval >= DB_OK)
	{
		retval = ctb_getField(OSD_CONF_TABLE, 
			      OSD_CONF_PORT_B_Y, 
			      (void *) y, 
			      OSD_CONF_PORT_B_LOC_SIZE,
			      NULL, NULL);
	}
	return retval;
}

int set_osd_port_a_coordinates(int x, int y)
{
	int retval = 0;

	retval = ctb_setField(OSD_CONF_TABLE, 
			      OSD_CONF_PORT_A_X, 
			      (void *) &x, 
			      OSD_CONF_PORT_A_LOC_SIZE,
			      NULL, NULL);
	if(retval >=  DB_OK)
	{
		retval = ctb_setField(OSD_CONF_TABLE, 
		      		OSD_CONF_PORT_A_Y, 
		      		(void *) &y, 
		      		OSD_CONF_PORT_A_LOC_SIZE,
		      		NULL, NULL);
	}
	return retval;
}

int set_osd_port_b_coordinates(int x, int y)
{
	int retval = 0;

	retval = ctb_setField(OSD_CONF_TABLE, 
			      OSD_CONF_PORT_B_X, 
			      (void *) &x, 
			      OSD_CONF_PORT_B_LOC_SIZE,
			      NULL, NULL);
	if(retval >= DB_OK)
	{
		retval = ctb_setField(OSD_CONF_TABLE, 
			      OSD_CONF_PORT_B_Y, 
			      (void *) &y, 
			      OSD_CONF_PORT_B_LOC_SIZE,
			      NULL, NULL);
	}
	return retval;
}

int get_osd_ww_icon_loc_coordinates(int * x, int * y)
{
	int retval = 0;

	retval = ctb_getField(OSD_CONF_TABLE, 
			      OSD_CONF_WW_ICON_LOC_X, 
			      (void *) x, 
			      OSD_CONF_WW_ICON_LOC_SIZE,
			      NULL, NULL);
	if(retval >= DB_OK)
	{
		retval = ctb_getField(OSD_CONF_TABLE, 
			      OSD_CONF_WW_ICON_LOC_Y, 
			      (void *) y, 
			      OSD_CONF_WW_ICON_LOC_SIZE,
			      NULL, NULL);
	}
	return retval;
}

int set_osd_ww_icon_loc_coordinates(int x, int y)
{
	int retval = 0;

	retval = ctb_setField(OSD_CONF_TABLE, 
			      OSD_CONF_WW_ICON_LOC_X, 
			      (void *) &x, 
			      OSD_CONF_WW_ICON_LOC_SIZE,
			      NULL, NULL);
	if(retval >= DB_OK)
	{
		retval = ctb_setField(OSD_CONF_TABLE, 
			      OSD_CONF_WW_ICON_LOC_Y, 
			      (void *) &y, 
			      OSD_CONF_WW_ICON_LOC_SIZE,
			      NULL, NULL);
	}
	return retval;
}

/* END OF FILE: ctb_db_osdconf_api.c */

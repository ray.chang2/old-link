cmake_minimum_required(VERSION 3.12)

project(ctdb VERSION 1.0.0)

set(LOCAL_SOURCE_FILE_LIST
	ctb_db_api
	ctb_db_commonref_api
       	ctb_db_data_init
	ctb_db_devconf_api
	ctb_db_devinfo_api
	ctb_db_func
	ctb_db_osdconf_api
	ctb_db_serviceconf_api
	ctb_db_usertable_api
        )

foreach(it ${LOCAL_SOURCE_FILE_LIST})
        execute_process(COMMAND cppcheck
                ${CPPCHECK_FLAGS}
                ${GENERAL_INCLUDE_FLAGS}
                ${CMAKE_CURRENT_SOURCE_DIR}/${it}.c)
endforeach()


add_library(ctdb
	ctb_db_api.c
	ctb_db_commonref_api.c
	ctb_db_data_init.c
	ctb_db_devconf_api.c
	ctb_db_devinfo_api.c
	ctb_db_func.c
	ctb_db_osdconf_api.c
	ctb_db_serviceconf_api.c
	ctb_db_usertable_api.c
        )

target_include_directories(ctdb PUBLIC ../inc ../see-sqlite3)

target_link_libraries(ctdb PRIVATE see-sqlite3)



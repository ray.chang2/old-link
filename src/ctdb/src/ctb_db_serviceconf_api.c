/*###############################################################################
#   Filename: ctb_db_serviceconf_api.c
#
#   APIs to access elements of the Service Configuration table in the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  2/12/2019   Initial Version
#  01.00  CG team  2/26/2019   Version shared with S&N team.
##############################################################################*/

#include <sys/syslog.h>
#include "ctb_db_table_api.h"

/*------------------------------------------------------------------------------
#   APIs for Service configuration table 
------------------------------------------------------------------------------*/

int getField_connected_network(int * connected_network)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_CONNECTED_NETWORK, 
			      (void *) connected_network, 
			      SER_CONF_CONNECTED_NETWORK_SIZE,
			      NULL, NULL);
	return retval;
}


int getField_discovery_mgr(int * discovery_mgr)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_DISCOVERY_MGR, 
			      (void *) discovery_mgr, 
			      SER_CONF_DISCOVERY_MGR_SIZE,
			      NULL, NULL);
	return retval;
}

int getField_remote_app_dsp(int * remote_app_dsp)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_REMOTE_APP_DSP,
			      (void *) remote_app_dsp, 
			      SER_CONF_REMOTE_APP_DSP_SIZE,
			      NULL, NULL);
	return retval;
}

int getField_remote_control_app(int * remote_control_app)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_REMOTE_CONTROL_APP,
			      (void *) remote_control_app, 
			      SER_CONF_REMOTE_CONTROL_APP_SIZE,
			      NULL, NULL);
	return retval;
}

int getField_surgeon_profile(int * surgeon_profile)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_SURGEON_PROFILE,
			      (void *) surgeon_profile, 
			      SER_CONF_SURGEON_PROFILE_SIZE,
			      NULL, NULL);
	return retval;
}

int getField_ext_trigger(int * ext_trigger)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_EXT_TRIGGER,
			      (void *) ext_trigger, 
			      SER_CONF_EXT_TRIGGER_SIZE,
			      NULL, NULL);
	return retval;
}

int getField_auto_updates(int * auto_updates)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_AUTO_UPDATES,
			      (void *) auto_updates, 
			      SER_CONF_AUTO_UPDATES_SIZE,
			      NULL, NULL);
	return retval;
}

int get_servconf_modby(int * modby)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_MOD_BY,
			      (void *) modby, 
			      SER_CONF_MOD_BY_SIZE,
			      NULL, NULL);
	return retval;
}

int set_servconf_modby(int modby)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_MOD_BY,
			      (void *) &modby, 
			      SER_CONF_MOD_BY_SIZE,
			      NULL, NULL);
	return retval;
}

int get_servconf_modtime(long int * modtime)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_MOD_TIME,
			      (void *) modtime, 
			      SER_CONF_MOD_TIME_SIZE,
			      NULL, NULL);
	return retval;
}

int set_servconf_modtime(long int modtime)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_MOD_TIME,
			      (void *) &modtime, 
			      SER_CONF_MOD_TIME_SIZE,
			      NULL, NULL);
	return retval;
}

int get_servconf_status(int * status)
{
	int retval = 0;

	retval = ctb_getField(SERVICE_CONF_TABLE, 
			      SER_CONF_STATUS,
			      (void *) status, 
			      SER_CONF_STATUS_SIZE,
			      NULL, NULL);
	return retval;
}

int set_servconf_status(int status)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_STATUS,
			      (void *) &status, 
			      SER_CONF_STATUS_SIZE,
			      NULL, NULL);
	return retval;
}

int enable_disable_servconf_field(char  * fieldname, int value)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      fieldname,
			      (void *) &value, 
			      SER_CONF_FIELD_SIZE,
			      NULL, NULL);
	return retval;
}

int setField_connected_network(int connected_network)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_CONNECTED_NETWORK, 
			      (void *) &connected_network, 
			      SER_CONF_CONNECTED_NETWORK_SIZE,
			      NULL, NULL);
	return retval;
}


int setField_discovery_mgr(int discovery_mgr)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_DISCOVERY_MGR, 
			      (void *) &discovery_mgr, 
			      SER_CONF_DISCOVERY_MGR_SIZE,
			      NULL, NULL);
	return retval;
}

int setField_remote_app_dsp(int remote_app_dsp)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_REMOTE_APP_DSP,
			      (void *) &remote_app_dsp, 
			      SER_CONF_REMOTE_APP_DSP_SIZE,
			      NULL, NULL);
	return retval;
}

int setField_remote_control_app(int remote_control_app)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_REMOTE_CONTROL_APP,
			      (void *) &remote_control_app, 
			      SER_CONF_REMOTE_CONTROL_APP_SIZE,
			      NULL, NULL);
	return retval;
}

int setField_surgeon_profile(int surgeon_profile)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_SURGEON_PROFILE,
			      (void *) &surgeon_profile, 
			      SER_CONF_SURGEON_PROFILE_SIZE,
			      NULL, NULL);
	return retval;
}

int setField_ext_trigger(int ext_trigger)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_EXT_TRIGGER,
			      (void *) &ext_trigger, 
			      SER_CONF_EXT_TRIGGER_SIZE,
			      NULL, NULL);
	return retval;
}

int setField_auto_updates(int auto_updates)
{
	int retval = 0;

	retval = ctb_setField(SERVICE_CONF_TABLE, 
			      SER_CONF_AUTO_UPDATES,
			      (void *) &auto_updates, 
			      SER_CONF_AUTO_UPDATES_SIZE,
			      NULL, NULL);
	return retval;
}

/* END OF FILE: ctb_db_serviceconf_api.c */

/*###############################################################################
#   Filename: ctb_db_usertable_api.c
#
#   APIs to access elements of the User table in the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  1/21/2019   Initial Version
#  01.00  CG team  2/21/2019   Version shared with S&N team.
##############################################################################*/

#include "ctb_db_api.h"

/*------------------------------------------------------------------------------
#   APIs for user table 
------------------------------------------------------------------------------*/

int get_user_code(char * user_code, int size, int primary_key_value)
{
	int retval = 0;

	retval = ctb_getField(USER_TABLE, 
			      USER_CODE, 
			      (void *) user_code, 
			      size,
			      USER_ID,
			      (void *) &primary_key_value);
	return retval;
}

int set_user_code(char * user_code, int size, int primary_key_value)
{
	int retval = 0;

	retval = ctb_setField(USER_TABLE, 
			      USER_CODE, 
			      (void *) user_code, 
			      size,
			      USER_ID,
			      (void *) &primary_key_value);
	return retval;
}

int get_user_name(char *user_name, int size, int primary_key_value)
{
	int retval = 0;

	retval = ctb_getField(USER_TABLE, 
			      USER_NAME, 
			      (void *) user_name, 
			      size,
			      USER_ID,
			      (void *) &primary_key_value);
	return retval;
}

int set_user_name(char *user_name, int size, int primary_key_value)
{
	int retval = 0;

	retval = ctb_setField(USER_TABLE, 
			      USER_NAME, 
			      (void *) user_name, 
			      size,
			      USER_ID,
			      (void *) &primary_key_value);
	return retval;
}

int get_user_type(int * user_type, int primary_key_value)
{
	int retval = 0;

	retval = ctb_getField(USER_TABLE, 
			      USER_TYPE, 
			      (void *) user_type, 
			      USER_TYPE_SIZE,
			      USER_ID,
			      (void *) &primary_key_value);
	return retval;
}

int set_user_type(int user_type, int primary_key_value)
{
	int retval = 0;

	retval = ctb_setField(USER_TABLE, 
			      USER_TYPE, 
			      (void *) &user_type, 
			      USER_TYPE_SIZE,
			      USER_ID,
			      (void *) &primary_key_value);
	return retval;
}

int get_user_password(char * user_password, int size, int primary_key_value)
{
	int retval = 0;

	retval = ctb_getField(USER_TABLE, 
			      USER_PASSWORD, 
			      (void *) user_password, 
			      size,
			      USER_ID,
			      (void *) &primary_key_value);
	return retval;
}

int set_user_password(char * user_password, int size, int primary_key_value)
{
	int retval = 0;

	retval = ctb_setField(USER_TABLE, 
			      USER_PASSWORD, 
			      (void *) user_password, 
			      size,
			      USER_ID,
			      (void *) &primary_key_value);
	return retval;
}

int get_user_status(int * status, int primary_key_value)
{
	int retval = 0;

	retval = ctb_getField(USER_TABLE, 
			  USER_STATUS, 
			  (void *) status, 
			  USER_STATUS_SIZE,
			  USER_ID,
			  (void *) &primary_key_value);
	return retval;
}

int set_user_status(int status, int primary_key_value)
{
	int retval = 0;

	retval = ctb_setField(USER_TABLE, 
			  USER_STATUS, 
			  (void *) &status, 
			  USER_STATUS_SIZE,
			  USER_ID,
			  (void *) &primary_key_value);
	return retval;
}


/* END OF FILE: ctb_db_usertable_api.c */

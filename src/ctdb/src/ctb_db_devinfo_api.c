/*###############################################################################
#   Filename: ctb_db_devinfo_api.c
#
#   APIs to access elements of the Service Configuration table in the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  2/12/2019   Initial Version
#  01.00  CG team  2/26/2019   Version shared with S&N team.
##############################################################################*/

#include "ctb_db_api.h"

/*------------------------------------------------------------------------------
#   APIs for Device Information table 
------------------------------------------------------------------------------*/

int get_devinfo_device_type(int * device_type)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_INFO_TABLE,
                              DEV_INFO_DEVICE_TYPE,
                              (void *) device_type,
                              DEV_INFO_DEVICE_TYPE_SIZE,
                              NULL, 
			      NULL);
        return retval;
}

int set_devinfo_device_type(int device_type)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_INFO_TABLE,
                              DEV_INFO_DEVICE_TYPE,
                              (void *) &device_type,
                              DEV_INFO_DEVICE_TYPE_SIZE,
                              NULL, 
			      NULL);
        return retval;
}

int get_devinfo_device_name(char * device_name, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_DEVICE_NAME, 
			      (void *) device_name, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_device_name(char * device_name, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_DEVICE_NAME, 
			      (void *) device_name, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_software_version(char * software_version, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_SOFTWARE_VERSION, 
			      (void *) software_version, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_software_version(char * software_version, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_SOFTWARE_VERSION, 
			      (void *) software_version, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_sku(char * sku, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_SKU, 
			      (void *) sku, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_sku(char * sku, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_SKU, 
			      (void *) sku, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_serial(char * serial, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_SERIAL, 
			      (void *) serial, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_serial(char * serial, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_SERIAL, 
			      (void *) serial, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_hardware_part(char * hardware_part, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_HARDWARE_PART, 
			      (void *) hardware_part, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_hardware_part(char * hardware_part, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_HARDWARE_PART, 
			      (void *) hardware_part, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_hardware_part_rev(char * hardware_part_rev, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_HARDWARE_PART_REV, 
			      (void *) hardware_part_rev, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_hardware_part_rev(char * hardware_part_rev, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_HARDWARE_PART_REV, 
			      (void *) hardware_part_rev, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_mac_address(char * mac_address, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_MAC_ADDRESS, 
			      (void *) mac_address, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_mac_address(char * mac_address, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_MAC_ADDRESS, 
			      (void *) mac_address, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_fw_icon(char * fw_icon, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_FW_ICON, 
			      (void *) fw_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_fw_icon(char * fw_icon, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_FW_ICON, 
			      (void *) fw_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_rw_icon(char * rw_icon, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_RW_ICON, 
			      (void *) rw_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_rw_icon(char * rw_icon, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_RW_ICON, 
			      (void *) rw_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_osc_icon(char * osc_icon, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_OSC_ICON, 
			      (void *) osc_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_osc_icon(char * osc_icon, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_OSC_ICON, 
			      (void *) osc_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_modby(int * modby)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_INFO_TABLE,
                              DEV_INFO_MOD_BY,
                              (void *) modby,
                              DEV_INFO_MOD_BY_SIZE,
                              NULL, 
			      NULL);
        return retval;
}

int set_devinfo_modby(int modby)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_INFO_TABLE,
                              DEV_INFO_MOD_BY,
                              (void *) &modby,
                              DEV_INFO_MOD_BY_SIZE,
                              NULL, 
			      NULL);
        return retval;
}

int get_devinfo_modtime(long int * modtime)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_INFO_TABLE,
                              DEV_INFO_MOD_TIME,
                              (void *) modtime,
                              DEV_INFO_MOD_TIME_SIZE,
                              NULL, 
			      NULL);
        return retval;
}

int set_devinfo_modtime(long int modtime)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_INFO_TABLE,
                              DEV_INFO_MOD_TIME,
                              (void *) &modtime,
                              DEV_INFO_MOD_TIME_SIZE,
                              NULL, 
			      NULL);
        return retval;
}

int get_devinfo_status(int * status)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_INFO_TABLE,
                              DEV_INFO_STATUS,
                              (void *) status,
                              DEV_INFO_STATUS_SIZE,
                              NULL, 
			      NULL);
        return retval;
}

int set_devinfo_status(int status)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_INFO_TABLE,
                              DEV_INFO_STATUS,
                              (void *) &status,
                              DEV_INFO_STATUS_SIZE,
                              NULL, 
			      NULL);
        return retval;
}

int get_devinfo_ablate_icon(char * ablate_icon, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_ABLATE_ICON, 
			      (void *) ablate_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_ablate_icon(char * ablate_icon, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_ABLATE_ICON, 
			      (void *) ablate_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_coag_icon(char * coag_icon, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_COAG_ICON, 
			      (void *) coag_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_coag_icon(char * coag_icon, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_COAG_ICON, 
			      (void *) coag_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int get_devinfo_ambient_temp_icon(char * ambient_temp_icon, int size)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_INFO_TABLE, 
			      DEV_INFO_AMBIENT_TEMP_ICON, 
			      (void *) ambient_temp_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

int set_devinfo_ambient_temp_icon(char * ambient_temp_icon, int size)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_INFO_TABLE, 
			      DEV_INFO_AMBIENT_TEMP_ICON, 
			      (void *) ambient_temp_icon, 
			      size,
			      NULL, 
			      NULL);
	return retval;
}

/* END OF FILE: ctb_db_devinfo_api.c */

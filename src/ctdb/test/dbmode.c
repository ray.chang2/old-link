
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


#include "ctb_data.h"
#include "ctb_db_table_api.h"



char	*smode[2] = {MODE_OPERATION_HOST, MODE_OPERATION_AP};


int32_t
main(int32_t argc, char **argv)
{
int		mode = -1, ap_mode=0, host_mode=0, status=0;

	if(argc > 1)
	{
		if(!strcmp(argv[1], "ap"))
		{
			printf("dbmode: setting AP mode: %d\n", MODE_OPERATION_AP_VALUE);
			status = setMode(MODE_OPERATION_AP_VALUE);
		}

		if(status < 0)
		{
			printf("setMode(MODE_OPERATION_AP_VALUE) FAIL: %d\n", status);
			exit(1);
		}

		if(!strcmp(argv[1], "host"))
		{
			printf("dbmode: setting HOST mode: %d\n", MODE_OPERATION_HOST_VALUE);
			status = setMode(MODE_OPERATION_HOST_VALUE);
		}

		if(status < 0)
		{
			printf("setMode(MODE_OPERATION_HOST_VALUE) FAIL: %d\n", status);
			exit(1);
		}
	}

	status = getMode(&mode);

	if(mode < MODE_OPERATION_HOST_VALUE || mode > MODE_OPERATION_AP_VALUE)
	{
		printf("Invalid Mode in Database: mode: %d, ap_mode: %d, host_mode: %d\n", 
			mode, ap_mode, host_mode);
		exit(1);
	}

	printf("Current Database Mode: %s\n", smode[mode]);

}




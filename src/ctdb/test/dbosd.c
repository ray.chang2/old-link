

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


#include "ctb_data.h"
#include "ctb_db_table_api.h"


#define 	WW	1
#define		D2	2


int32_t
main(int32_t argc, char **argv)
{
int32_t		i, status=0;
int32_t		x, y, platform=0;


	for(i=1; i<argc; i++)
	{
		if(!strcmp(argv[i], "-a"))
		{
			x = atoi(argv[++i]);
			y = atoi(argv[++i]);
			status = set_osd_port_a_coordinates(x, y);
			if(status < 0)
			{
				printf("error from set_osd_port_a_coordinates: %d\n", status);
				exit(1);
			}
		}
		else if(!strcmp(argv[i], "-b"))
		{
			x = atoi(argv[++i]);
			y = atoi(argv[++i]);
			status = set_osd_port_b_coordinates(x, y);
			if(status < 0)
			{
				printf("error from set_osd_port_b_coordinates: %d\n", status);
				exit(1);
			}
		}
		else if(!strcmp(argv[i], "-p"))
		{
			i++;
			if(!strcmp(argv[i], "WW") || !strcmp(argv[i], "ww"))
				platform = WW;
			else if(!strcmp(argv[i], "d2") || !strcmp(argv[i], "D2"))
				platform = D2;
		}
		else
		{
			x = atoi(argv[i]);
			y = atoi(argv[++i]);
			status = set_osd_ww_icon_loc_coordinates(x, y);
			if(status < 0)
			{
				printf("error from set_osd_ww_icon_loc_coordinates: %d\n", status);
				exit(1);
			}
		}
	}

	if(platform == 0)
	{
		if(access("/etc/WW", F_OK) == 0) platform = WW;
		else if(access("/etc/D2", F_OK) == 0) platform = D2;
		else
		{
			printf("No Platform\n");
			exit(1);
		}
	}

	if(platform == D2)
	{
		status = get_osd_port_a_coordinates(&x, &y);
		if(status < 0)
		{
			printf("error from get_osd_port_a_coordinates: %d\n", status);
			exit(1);
		}
		printf("PORT A Coords: (%d, %d)  status: %d\n", x, y, status);

		status = get_osd_port_b_coordinates(&x, &y);
		if(status < 0)
		{
			printf("error from get_osd_port_b_coordinates: %d\n", status);
			exit(1);
		}
		printf("PORT B Coords: (%d, %d)  status: %d\n", x, y, status);
	}
	else if(platform == WW)
	{
		status = get_osd_ww_icon_loc_coordinates(&x, &y);
		if(status < 0)
		{
			printf("error from get_osd_ww_icon_loc_coordinates: %d\n", status);
			exit(1);
		}
		printf("PORT A Coords: (%d, %d)  status: %d\n", x, y, status);
	}
}



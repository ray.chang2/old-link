cmake_minimum_required(VERSION 3.12)

project(d2BDM, VERSION 1.0.1 LANGUAGES C)

############
#    D2    #
############
execute_process(COMMAND cppcheck
        ${CPPCHECK_FLAGS}
        ${CPPCHECK_IGNORE_FLAGS}
        ${GENERAL_INCLUDE_FLAGS}
        ${CMAKE_CURRENT_SOURCE_DIR}/d2Bdm/bdmD2.c)
        
execute_process(COMMAND cppcheck
        ${CPPCHECK_FLAGS}
        ${CPPCHECK_IGNORE_FLAGS}
        ${GENERAL_INCLUDE_FLAGS}
        ${CMAKE_CURRENT_SOURCE_DIR}/d2Bdm/bdmD2Utils.c)

add_executable(d2Bdm
        d2Bdm/bdmD2.c
        d2Bdm/bdmD2Utils.c)

target_include_directories(d2Bdm  PRIVATE
                           include
                           ../libBridgeCommon/inc
                           ../discovery_service/include
                           ../ctdb/inc)
                
target_compile_options(d2Bdm PRIVATE ${WARN_OPTIONS})

target_link_libraries(d2Bdm PRIVATE m rt pthread BridgeCommon zmq ctdb sqlite3 see-sqlite3 dl)

install(TARGETS d2Bdm RUNTIME DESTINATION ${DESTDIR})

############
# Werewolf #
############
execute_process(COMMAND cppcheck
        ${CPPCHECK_FLAGS}
        ${CPPCHECK_IGNORE_FLAGS}
        ${GENERAL_INCLUDE_FLAGS}
        ${CMAKE_CURRENT_SOURCE_DIR}/wwBdm/bdmWW.c)

execute_process(COMMAND cppcheck
        ${CPPCHECK_FLAGS}
        ${CPPCHECK_IGNORE_FLAGS}
        ${GENERAL_INCLUDE_FLAGS}
        ${CMAKE_CURRENT_SOURCE_DIR}/wwBdm/bdmWWUtils.c)

add_executable(wwBdm
        wwBdm/bdmWW.c
        wwBdm/bdmWWUtils.c)

target_include_directories(wwBdm PRIVATE
                           include
                           ../libBridgeCommon/inc
                           ../discovery_service/include
                           ../ctdb/inc)

target_compile_options(wwBdm PRIVATE ${WARN_OPTIONS})
target_link_libraries(wwBdm PRIVATE m rt pthread zmq BridgeCommon ctdb sqlite3 see-sqlite3 dl)
install(TARGETS wwBdm RUNTIME DESTINATION ${DESTDIR})


############
# getSetD2 #
############
execute_process(COMMAND cppcheck
        ${CPPCHECK_FLAGS}
        ${CPPCHECK_IGNORE_FLAGS}
        ${GENERAL_INCLUDE_FLAGS}
        ${CMAKE_CURRENT_SOURCE_DIR}/getSetD2/getSetD2.c)

add_executable(getSetD2
        getSetD2/getSetD2.c)
        
target_include_directories(getSetD2
                           PRIVATE include
                           ../libBridgeCommon/inc
                           ../discovery_service/include)

target_compile_options(getSetD2 PRIVATE ${WARN_OPTIONS})
target_link_libraries(getSetD2 PRIVATE m rt pthread zmq BridgeCommon )
install(TARGETS getSetD2 RUNTIME DESTINATION ${DESTDIR})

############
# getSetWW #
############
execute_process(COMMAND cppcheck
        ${CPPCHECK_FLAGS}
        ${CPPCHECK_IGNORE_FLAGS}
        ${GENERAL_INCLUDE_FLAGS}
        ${CMAKE_CURRENT_SOURCE_DIR}/getSetWW/getSetWW.c)

add_executable(getSetWW
        getSetWW/getSetWW.c)
        
target_include_directories(getSetWW
                           PRIVATE include
                           ../libBridgeCommon/inc
                           ../discovery_service/include)

target_compile_options(getSetWW PRIVATE ${WARN_OPTIONS})
target_link_libraries(getSetWW PRIVATE m rt pthread zmq BridgeCommon )
install(TARGETS getSetWW RUNTIME DESTINATION ${DESTDIR})


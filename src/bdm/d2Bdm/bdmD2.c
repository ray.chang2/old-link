/**
 * @file d2Bdm.c
 * @brief bdm for D2
 * @details
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/10/18
 *
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/10/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <assert.h>
#include <sys/types.h>
#include <linux/types.h>
#include <sys/syslog.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "commonTypes.h"
#include "commonState.h"
#include "cliCommon.h"
#include "connUtils.h"
#include "broker_api.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgSerial.h"
#include "msgD2.h"
#include "msgIp.h"
#include "msgGetSetD2.h"
#include "bdmD2Private.h"
#include "D2_channels.h"
#include "ctb_db_table_api.h"

#include "dsc.h"

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

/**
 * static/private/local variables
 */
int  serial_fd           = -1;
int  setupBlobClientFd   = -1;

// general serial state
bool isD2Connected       = false;
bool isD2Ready           = false;
u32  rxHeartbeatCount    = 0;
u32  txHeartbeatCount    = 0;
u32  rxHeartbeatMiss     = 0;

// local locks
pthread_mutex_t serialMsgLock;
pthread_mutex_t msgSetupBlobLock;
pthread_mutex_t sendTriggerLock;

static const char* version = "01.00.00"; //follows major.minor.patch scheme(https://semver.org)
char *comport;
int doCli = 1;

// local data
capdev_to_bdm_discovery_t localD2DiscoveryData;
hb_bdm_scd_t              localTxHBStatus;
hb_scd_bdm_t              localRxHBStatus;
scd_bdm_port_status_t     localPortStatus;
scd_bdm_serial_number_t   localSerialNumber;

// osd coordinates - 4 byte field to extract/put into blob
OSD_coords_t localCoords = {0, 0x55, 0x44, 0x33, 0x22};

// discovery 
DscMsg_ListDcptVirtual_t localListMsg; 
bool     isvalidClientList   = false;

// flags for pending //
u32  pendingActions    = 0;
bool waitGetPortStatus = false;

// local blob data
u8 *pBlobData          = NULL;
ct_blob_data_header_t  blobHeader;

// d25 threads //
pthread_t txTrigger;
pthread_t rxTrigger;

// d25 flags
bool isD25Available = false;
bool isD25Connected = false;
bool triggerSent    = false;

// d25 config
char d25IpDotIp[32];
u32  d25IpAddr = 0;
int  d25Socket = -1;

///////////////////
// pub/sub stuff //
///////////////////
void *pHandlePub = NULL;

////////////////
// blob rx/tx //
////////////////
static u32 num_rx_get_data_blobs  = 0;
static u32  num_tx_set_data_blobs = 0;

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
/////////////////////// dispatch tables //////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
// Lavage protocol
///////////////////////////
ct_exp_cmds_t bdmIpLavage = {LAVAGE_PROTOCOL, LAVAGE_MSG_REPLY, LAVAGE_REPLY_SIZE};

////////////////////////////
/// setup blob dispatch  ///
////////////////////////////
ct_exp_cmds_t bdmIpSetupBlobCmds[3] =
{
   {SETUP_BLOB_PROTOCOL_CLIENT_TO_BDM, SETUP_BLOB_GET_CMD,   0},
   {SETUP_BLOB_PROTOCOL_CLIENT_TO_BDM, SETUP_BLOB_SET_CMD,  -1},
};

////////////////////////////
/// dispatch serial msg  ///
////////////////////////////
#define NUM_SUPPORTED_SCD2BDM_CMDS 10
ct_exp_cmds_t bdmRxCmds[NUM_SUPPORTED_SCD2BDM_CMDS] =
{
      // standard stuff /
   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_GENERIC_RESPONSE,          GENERIC_REPLY_DATA_LENGTH           }, // 3 byte status
   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_DISCOVERY_RESPONSE,        CAPDEV_TO_BDM_DISCOVERY_RESP_LENGTH }, // disc resp

   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_HEARTBEAT_CMD,             HB_SCD_BDM_DATA_LENGTH              }, // hb status
    
   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_PORT_STATUS,               SCD_PORT_STATUS_DATA_LEN            }, // port status 

   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_SET_DEVICE_INFO_RESPONSE,  SET_DEVICE_INFO_DATA_LEN_RESP       }, // 1 BYTE ACK
   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_TRIGGER_OUT_EVENT,         ZERO_LENGTH_MSG_DATA                }, // trigger data
   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_SCD_CMD_RESP,              SCD_TO_BDM_SCD_CMD_RESP_LEN         }, // ack/nack
    
   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE,  VARIABLE_LENGTH_MSG_DATA_SIZE       }, // variable length response - ignore length
   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE,  SET_BLOB_DATA_RESPONSE_DATA_LEN     }, // blob data rx ack/nack

   {PROTOCOL_SCD_TO_BDM, SCD_TO_BDM_SERIAL_NUMBER_STATUS,      SCD_SERIAL_NUMBER_DATA_LEN          }, // port status
};
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

/**
 * @fn      void reset_local_data(void)
 * @brief   resets all local data 
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void reset_local_data(void)
{
   // reset all local protocol //
   bzero(&localD2DiscoveryData, sizeof(localD2DiscoveryData));
   bzero(&localTxHBStatus,      sizeof(localTxHBStatus));
   bzero(&localRxHBStatus,      sizeof(localRxHBStatus));
   bzero(&localPortStatus,      sizeof(localPortStatus));
   bzero(&localSerialNumber,    sizeof(localSerialNumber));
   bzero(&localListMsg,         sizeof (localListMsg));
   isvalidClientList = false;
   isD2Ready         = false;
   isD2Connected     = false;
   waitGetPortStatus = false;
   pendingActions    = 0;
}

/**
 * @fn      void init_local_data(void)
 * @brief   initializes local data 
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void init_local_data(void)
{
   // init local data
   reset_local_data();
      
   // publish account
   if ((pHandlePub = bm_publisher()) == NULL)
   {
      logger.error("Failure to create publisher");
   }
}

/**
 * @fn      void dispatch_setupBlob_msg(pIpMsg rxMsg)
 * @brief   processes setup Blob Messages from clients
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void dispatch_setupBlob_msg(pIpMsg rxMsg)
{
   // determine if set or get 
   if (rxMsg->command_id == SETUP_BLOB_SET_CMD)
   {
      u16 total_blob_data_size, totalExpectedSize, osdCoordsOffset;

      // copy header and make sure data blob size is correct
      memcpy(&blobHeader, rxMsg->cmd_data, BLOB_DATA_HEADER_SIZE);
      total_blob_data_size = ntohs(blobHeader.blob_data_size) + 1;
      osdCoordsOffset      = total_blob_data_size + BLOB_DATA_HEADER_SIZE;
      totalExpectedSize    = osdCoordsOffset + BLOB_COORDS_SIZE;
      assert(!(total_blob_data_size % MAX_OPAQUE_DATA_PACKET));
      num_tx_set_data_blobs = (1 + (total_blob_data_size / MAX_OPAQUE_DATA_PACKET));
      
      // make sure its the right size - I trust the app, just not Apple
      if (totalExpectedSize != rxMsg->cmd_data_len)
      {
         logger.error("rxCmdLen(%d) != expected RxBlobSize (%d)", rxMsg->cmd_data_len, totalExpectedSize);
         respond_set_setup_blob_client(1);
         return;
      }

      // copy the coordinates and publish to osd
      memcpy(&localCoords.portA_x, &rxMsg->cmd_data[osdCoordsOffset], BLOB_COORDS_SIZE);
      logger.highlight("OSD COORDS received from get/set client (%02X/%02X/%02X/%02X)",
                       localCoords.portA_x, localCoords.portA_y, 
                       localCoords.portB_x, localCoords.portB_y);

      // send the coordinates received (ignore timestamp)
      if ((bm_send(pHandlePub, (u8 *)BROKER_BDM_COORDS, &localCoords, sizeof(localCoords))) < 0)
      {
         logger.error("Failed to send Coordinates to OSD Client");
      }

      // copy the data blob over 
      pBlobData = calloc(total_blob_data_size+8,1);
      assert(pBlobData);
      memcpy(pBlobData, &rxMsg->cmd_data[BLOB_DATA_HEADER_SIZE], total_blob_data_size);
      send_set_setup_blob_scd();
   }
   else
   {
      send_get_setup_blob_scd();
   }
}


/**
 * @fn      void *bdm_setupBlob_server(void *args)
 * @brief   receives connects/messages from SetupBlob Clients
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
int setupBlobServerFd = -1;
void *bdm_setupBlob_server(UNUSED_ATTRIBUTE void *args)
{
   ct_ip_msg_t rxMsg;
   bool continueClientConnection;
   int status;    
   setupBlobServerFd = create_tcp_server_socket(SETUP_BLOB_PROTOCOL_SOCKET);
   if (setupBlobServerFd <= 0)
   {
      logger.error("Couldn't create setupBlob Server Socket");
      return NULL;
   }
    
   logger.debug("Entering BDM SETUP BLOB Server");
    
   while (!common_system_state.stopping)
   {
      if (common_system_state.state > WAITING_FOR_DISCOVERY_COMPLETION)
      {
         logger.debug("Start Accepting Next Connection from SetupBlob Client");
         // accept next command 
         setupBlobClientFd = accept_tcp_socket_connection(setupBlobServerFd);
         if (setupBlobClientFd < 0)
         {              
            common_close_socket(&setupBlobServerFd);
            return NULL;
         }

         logger.debug("New setupblob Connection <%d>",setupBlobClientFd);

         // process commands while connected
         continueClientConnection = true;
         while (continueClientConnection)
         {
            // wait for next message
            if (isConnectionValid(setupBlobClientFd) && (common_system_state.state > WAITING_FOR_DISCOVERY_COMPLETION))
            {
               bzero(&rxMsg, sizeof(rxMsg));
               if ((status = read_ip_msg(&setupBlobClientFd, bdmIpSetupBlobCmds, 2, &rxMsg)))
               {
                  if (status != 2)
                  {
                     logger.error("Error (%d) in reading msg setupblob - disconnect client",status);
                  }
                  common_close_socket(&setupBlobClientFd);
                  continueClientConnection = false;
                  freeIpMsgContainerData(&rxMsg);
               } // error reading message from server
               else
               {
                  // process if valid setup message //
                  dispatch_setupBlob_msg(&rxMsg);
                  freeIpMsgContainerData(&rxMsg);
               } // process get message
            }
            else
            {
               logger.error("Closing Client Connection (%d) State(%d)",setupBlobClientFd,common_system_state.state);
               continueClientConnection = false;
            } // if closing or the state went back to Discovery process //

         } // while valid connection
      } // if scd is connected
      else
      {
         YIELD_250MS;
      }
   } // while contining
   logger.highlight("Exiting setupblob server");
   return NULL;
}
/*
 * @fn      void bdm_check_d25_connected(void)
 * @brief   utility to check and initialize d25 variables
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void bdm_check_d25_connected(void)
{
   // reset list - so we can continue to discover
   bool wasAvailable = isD25Available;
   isD25Available = false;
   d25IpAddr      = 0;;
   bzero(d25IpDotIp,sizeof(d25IpDotIp));

   // go through list and see whats up
   for (int i = 0 ; ((i < localListMsg.num_of_devices) && (i < MAX_DEVICE_NUM)) ; i++)
   {
      char *dotIp = NULL;
      struct in_addr addrD25 = { localListMsg.client_list[i].ip };
      dotIp = inet_ntoa(addrD25);
      logger.debug("[%d] IP<%08X><%s> DeviceType<%d/%d>",i,
                   localListMsg.client_list[i].ip,
                   dotIp,
                   localListMsg.client_list[i].device_type,
                   localListMsg.client_list[i].sub_device_type);

      // check if its d25  
      if (localListMsg.client_list[i].device_type == FLUID_MANAGEMENT_TYPE)
      {
         d25IpAddr = localListMsg.client_list[i].ip;
         strncpy(d25IpDotIp, dotIp, sizeof(d25IpDotIp));
         if (wasAvailable)
         {
            logger.info("D25 is still connected");
         }
         else
         {
            logger.info("D25 was Found for first time @ <%s><%08X>",d25IpDotIp, d25IpAddr);
         }
         isD25Available = true;
         return;
      } // if d25 connection
   } // for entire list
}

/**
 * @fn      void *bdm_wait_clientList(UNUSED_ATTRIBUTE void * args)
 * @brief   waits for client lists to be received
 * @param   [in] args - Not Used
 * @retval  returns NULL
 **************************************************************/
void *bdm_wait_clientList(UNUSED_ATTRIBUTE void * args)
{
   void *pHandleSub = NULL;
   
   // create a handle and do a subscription
   if ((pHandleSub = bm_init_sub_handle(BROKER_DISCOVERY_LIST)) == NULL)
   {
      logger.error("Failure to create client list subscriber handler");
      return NULL;
   }

   while (1)
   {
      int status;
      u32 bytesRead;   
      u8 filter[BROKER_FILTER_MAX];
      void *list = NULL;
      
      // get client list
      if ((status = bm_receive(pHandleSub, filter, (void*) &list, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive for scd cmd",status);
         return NULL;
      }

      if (!(isD2Connected))
      {
         logger.debug("Got a Client List in Not Ready State(%d/%d)",isD2Connected, isD2Ready);
         continue;
      }

      // make sure we have enough bytes and copy over (free resources)
      assert(bytesRead >= sizeof(localListMsg));
      memcpy(&localListMsg, list, sizeof(localListMsg));
      free(list);

      // debug and set valid client list and check for d25 
      logger.highlight("local list received (%d clients)",localListMsg.num_of_devices);
      isvalidClientList = true;
      bdm_check_d25_connected();
   }
   return NULL;
}

/**
 * @fn      void *bdm_wait_osd(UNUSED_ATTRIBUTE void * args)
 * @brief   waits for scd commands to be received
 * @param   [in] args - Not Used
 * @retval  returns NULL
 **************************************************************/
void *bdm_wait_osd(UNUSED_ATTRIBUTE void * args)
{
   void *pHandleSub = NULL;
   
   // create a handle and do a subscription
   if ((pHandleSub = bm_init_sub_handle(BROKER_OSD_COORDS)) == NULL)
   {
      logger.error("Failure to create OSD COORDS subscriber handler");
      return NULL;
   }

   // forever
   while (1)
   {
      int status;
      u32 bytesRead;   
      u8 filter[BROKER_FILTER_MAX];
      void *coords = NULL;

      // do receive
      if ((status = bm_receive(pHandleSub, filter, (void*) &coords, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive for scd cmd",status);
         return NULL;
      }

      assert(bytesRead >= sizeof(localCoords));
      memcpy(&localCoords, coords, sizeof(localCoords));
      logger.highlight("OSD COORDS received (%02X/%02X/%02X/%02X)",
                       localCoords.portA_x, localCoords.portA_y, 
                       localCoords.portB_x, localCoords.portB_y);

      free(coords);
   }
   return NULL;
}

/**
 * @fn      void *bdm_wait_scdCmd(UNUSED_ATTRIBUTE void * args)
 * @brief   waits for scd commands to be received
 * @param   [in] args - Not Used
 * @retval  returns NULL
 **************************************************************/
void *bdm_wait_scdCmd(UNUSED_ATTRIBUTE void * args)
{
   void *pHandleSub = NULL;
   
   // create a handle and do a subscription
   if ((pHandleSub = bm_init_sub_handle(D2_SET_SCD_CMD)) == NULL)
   {
      logger.error("Failure to create Set SCD Cmd subscriber handler");
      return NULL;
   }

   // forever
   while (1)
   {
      int status;
      u8 scdCmd;

      if ((status = bm_get_sub_byte(pHandleSub, &scdCmd)))
      {
         logger.error("Failed (%d) bm_get_sub_byte for scd cmd",status);
         return NULL;
      }

      send_next_scd_cmd(scdCmd, true);
   }
   return NULL;
}

/**
 * @fn      void *bdm_wait_hbpf(UNUSED_ATTRIBUTE void * args)
 * @brief   waits for hbpf command 
 * @param   [in] args - Not Used
 * @retval  returns NULL
 **************************************************************/
void *bdm_wait_hbpf(UNUSED_ATTRIBUTE void * args)
{
   void *pHandleSub = NULL;

   // create a handle and do a subscription
   if ((pHandleSub = bm_init_sub_handle(D2_SET_HPBF_CMD)) == NULL)
   {
      logger.error("Failure to create Set HPBF Cmd subscriber handler");
      return NULL;
   }

   // forever
   while (1)
   {
      int status;
      u8 hpbf;
      if ((status = bm_get_sub_byte(pHandleSub, &hpbf)))
      {
         logger.error("Failed (%d) bm_get_sub_byte for hpbf cmd",status);
         return NULL;
      }

      send_next_set_dev_info(hpbf, true);
   }
   return NULL;
}

/**
 * @fn      void *bdm_publish_discovery_heartbeat(void *args)
 * @brief   sends heartbeat messages 
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *bdm_publish_discovery_heartbeat(UNUSED_ATTRIBUTE  void *args)
{
   static bool sentFirstOne = false;
   while (!common_system_state.stopping)
   {
      // only 
      YIELD_500MS;
      
      // debug output
      if (isD2Connected)
      {
         if (!sentFirstOne)
         {
            logger.debug("Sending first device ready message");
            sentFirstOne = true;
         }
      }
      else
      {
         sentFirstOne = false;
      }
      
      // send heartbeat
      if (pHandlePub != NULL)
      {      
         capDev_disc_heartbeat_t capDevInfo = {0x69BD33CC, isD2Connected};
         if ((bm_send(pHandlePub, (u8 *)BROKER_CAPDEV_HEARBEAT, &capDevInfo, sizeof(capDevInfo))) < 0)
         {
            logger.error("Failed to send ww discovery == false");
         }
      }
      else
      {
         logger.error("no pub handle to publish discovery heartbeat");
      }
      
   } // end while //
   return NULL;
}

/**
 * @fn      void send_discovery(void)
 * @brief   sends discovery message discovery
 * @param   n/a
 * @retval  n/a
 **************************************************************/
static bdm_to_capdev_discovery_t discReq = { DEFAULT_VERSION_BYTE };
void send_discovery(void)
{
   bdm_serial_msg_container_t txMsg;   
   bzero(&txMsg, sizeof(txMsg));
   txMsg.protocol_id    = PROTOCOL_BDM_TO_SCD;
   txMsg.command_id     = BDM_TO_SCD_DISCOVERY_REQUEST;
   txMsg.cmd_data_len   = BDM_TO_CAPDEV_DISCOVERY_REQ_LEN;
   txMsg.cmd_data       = (u8 *)&discReq;         
   txMsg.request_number = GET_CMD_REQUEST_NUMBER();
   bzero(&localD2DiscoveryData,sizeof(localD2DiscoveryData));
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
//   DBG("sending discovery\n");
}

/**
 * @fn      void publish_connected_status
 * @brief   publishes current connected status to whomever is listening
 * @param   state - state to set
 * @retval  n/a
 **************************************************************/
void publish_connected_status(bool state)
{
   // set connected state properly
   isD2Connected = state;

   // check if handle valid, and if so, publish status
   if (pHandlePub != NULL)
   {      
      logger.highlight("Publishing Connected State(%d)",state);
      if ((bm_send(pHandlePub, (u8 *)D2_CONNECTED, &isD2Connected, sizeof(isD2Connected))) < 0)
      {
         logger.error("Failed to send D2 connected");
      }
   }
   else
   {
      logger.warning("Trying to publish connected status when handle not available");
   }
}


/**
 * @fn      void restart_discovery(void)
 * @brief   restarts discovery
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void restart_discovery(void)
{
   logger.highlight("restarting discovery");

   // reset state
   common_system_state.state = WAITING_FOR_DISCOVERY_COMPLETION;
   reset_local_data();

   // send updated status 
   publish_connected_status(false);
      
   // remove all expected msgs
   clear_all_backup_msgs();
   
   // send command
   send_discovery();
}

/**
 * @fn      void process_timeouts(void)
 * @brief   restarts discovery
 * @param   n/a
 * @retval  n/a
 * @note    DO NOT TRY TO FREE PBACK - its on a list
 **************************************************************/
void process_timeouts(void)
{
   struct timeval tv;
   pMsgSerial pBack = NULL;
   u32 numRetries   = 0;
   while ((get_num_msg_backed()) && (pBack = get_next_timeout_msg(txHeartbeatCount, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT,&numRetries)))
   {
      gettimeofday(&tv,NULL);
      // check if tried more than once 
      if (numRetries >= 3)
      {
         logger.error("Timeout waiting for response to <%s> go back to Discovery(%ld.%ld)",cmdId2TxtD2(pBack->command_id),tv.tv_sec, tv.tv_usec);
         
         restart_discovery();
      }
      else
      {
         logger.highlight("Timeout - Resending Cmd/RN <%s/%02X>",cmdId2TxtD2(pBack->command_id),pBack->request_number);
         pBack->request_number |= REQUEST_NUMBER_CMD_BIT_MASK;
         build_send_serial_msg(serial_fd, pBack, &serialMsgLock);
      }
   } // while there are messages that are timed out //
}

/**
 * @fn      void send_heartbeat
 * @brief   sends heartbeats to capital device
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void send_heartbeat(void)
{
   bdm_serial_msg_container_t txMsg;
   bzero(&txMsg, sizeof(txMsg));
   txMsg.protocol_id    = PROTOCOL_BDM_TO_SCD;
   txMsg.command_id     = BDM_TO_SCD_HEARTBEAT_CMD;
   txMsg.cmd_data_len   = HB_BDM_SCD_DATA_LENGTH;
   txMsg.cmd_data       = (u8 *)&localTxHBStatus; 
   txMsg.request_number = GET_CMD_REQUEST_NUMBER();
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void *rx_trigger(UNUSED_ATTRIBUTE  void *args)
 * @brief   thread to receive trigger response
 * @param   [in] args (NOT USED)
 * @retval  returns NULL
 **************************************************************/
void *rx_trigger(UNUSED_ATTRIBUTE  void *args)
{
   logger.info("Entering RX trigger(%d)",d25Socket);
   if (d25Socket <= 0)
   {
      logger.error("invalid d25 socket>");
   }
   else
   {
      ct_ip_msg_t rxMsg;
      bzero(&rxMsg, sizeof(rxMsg));
      int status;
      if ((status = read_ip_msg(&d25Socket, &bdmIpLavage, 1, &rxMsg)))
      {
         logger.error("Error (%d) in reading d25 socket  - disconnect client",status);
      } // error reading message from server
      else
      {
         pLavageReply pReply = (pLavageReply)rxMsg.cmd_data;
         // validate for now
         assert(triggerSent);
         assert(rxMsg.command_id == LAVAGE_MSG_REPLY);
         assert(pReply->clientDeviceType == SHAVER_CAPITAL_DEVICE);
         assert(pReply->serverDeviceType == FLUID_MANAGEMENT_TYPE);
         assert(pReply->clientDevSubType == eConnectedTowerD2Shaver);
         assert(pReply->serverDevSubType == eConnectedTowerD25);
         logger.info("Lavage Reply <%d>",pReply->status);
      } // process get message
      
      freeIpMsgContainerData(&rxMsg);      
   }

   logger.info("Exiting Rx Trigger");
   common_close_socket(&d25Socket);
   triggerSent    = false;
   isD25Connected = false;
   return NULL;
}

/**
 * @fn      void *tx_trigger(UNUSED_ATTRIBUTE  void *args)
 * @brief   thread to send trigger to D25
 * @param   [in] args (NOT USED)
 * @retval  returns NULL
 **************************************************************/
void *tx_trigger(UNUSED_ATTRIBUTE  void *args)
{
   ct_ip_msg_t  txMsg;
   lavage_cmd_t trig;
   logger.info("Sending tx Trigger");
   triggerSent = false;
   
   // initialize message default (get)
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = LAVAGE_PROTOCOL;
   txMsg.command_id          = LAVAGE_MSG_CMD;
   txMsg.cmd_data_len        = LAVAGE_CMD_SIZE;
   txMsg.cmd_sequence_number = 0x66;
   txMsg.cmd_data            = (u8 *)&trig;
   trig.clientDeviceType     = SHAVER_CAPITAL_DEVICE;
   trig.clientDevSubType     = eConnectedTowerD2Shaver;
   trig.serverDeviceType     = FLUID_MANAGEMENT_TYPE;
   trig.serverDevSubType     = eConnectedTowerD25;

   // send the packet
   triggerSent = true;
   if ((build_send_ip_msg(d25Socket,&txMsg, &sendTriggerLock)))
   {
      logger.error("Failed to send trigger");
      common_close_socket(&d25Socket);
      isD25Connected = false;
      triggerSent    = false;
   }
   
   while(triggerSent && isD25Connected)
   {
      sleep(1);
   }

   logger.info("Exiting tx trigger (%d/%d)",triggerSent, isD25Connected);
   triggerSent = false;
   isD25Connected = false;
   return NULL;
}

/**
 * @fn      void start_d25_trigger(void)
 * @brief   creates trigger threads and returns 
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void start_d25_trigger(void)
{
   if (!isD25Available)
   {
      logger.highlight("No D25 to send to");
      return;
   }
   
   if ((d25Socket = connect_tcp_socket_server(d25IpAddr, 64000)) <= 0)
   {
      logger.error("Failed to Connect to D25 - %s", COMMON_ERR_STR);
      return;
   }
   
   isD25Connected = true;
   logger.info("Start Sending of Trigger <%d>",d25Socket);
   
   //create rx,tx trigger threads //
   if ((common_create_thread(&rxTrigger, MEDIUM_LOW, NULL, rx_trigger)))
   {
      logger.error("Error in creating TX trigger Thread");
      common_close_socket(&d25Socket);
   }
   else if ((common_create_thread(&txTrigger, MEDIUM_LOW, NULL, tx_trigger)))
   {
      logger.error("Error in creating rx trigger thread");
      common_close_socket(&d25Socket);
   }
}

/**
 * @fn      void *bdm_tx_heartbeat(void *args)
 * @brief   sends heartbeats to scd
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
u32 waiting4DiscoveryCount = 0;
void *bdm_tx_heartbeat(UNUSED_ATTRIBUTE  void *args)
{
   while (!common_system_state.stopping)
   {
      txHeartbeatCount++;
      if (common_system_state.state == WAITING_FOR_DISCOVERY_COMPLETION)
      {
         if (!(txHeartbeatCount % SEND_HEARTBEAT_COUNT))
         {
            send_discovery();
         }
      }
      else
      {
         // check if we should send heartbeat 
         if (!(txHeartbeatCount % SEND_HEARTBEAT_COUNT))
         {
            send_heartbeat();
         }
         
         if (get_num_msg_backed())
         {
            process_timeouts();
         } // see if there is anything for me to do //

         // check if we aren't in discovery
         rxHeartbeatMiss++;
         if (rxHeartbeatMiss > MAX_NUMBER_RX_TIMEOUT_MISS)
         {
            if (isD2Connected)
               logger.debug("LOSS of SCD Heartbeat Connection (%d) after Rx(%d)",isD2Ready, rxHeartbeatCount);
            restart_discovery();
         }
         
      } // sending heartbeats
      TIMEOUT_HEARTBEAT;
   }  // while not stopping
   return NULL;
}

/**
 * @fn      int update_local_port_status(u8 *rxData)
 * @brief   processes updates to the local port status to compare 
            against what is already there  
 * @param   [in] rxData - incoming port status to be copied
 * @retval  returns 0 on success, otherwise, it error
 */
int update_local_port_status(u8 *rxData)
{
   bool changes = false;
   u8 *pLocalStatus = (u8 *)&localPortStatus;
   
   // compare and copy over if there 
   for (u32 i = 0 ; i < sizeof(localPortStatus) ; i++)
   {
      if (rxData[i] != pLocalStatus[i])
      {
//         logger.debug("PortStat[%d] change <%02X to %02X>", i,pLocalStatus[i], rxData[i]);
         pLocalStatus[i] = rxData[i];
         changes = true;
      }
   }

   // check if changes
   if (changes && (pHandlePub != NULL))
   {
      logger.highlight("Change in Port Status - publish to get/set");
      if ((bm_send(pHandlePub, (u8 *)D2_STATUS, &localPortStatus, sizeof(localPortStatus))) < 0)
      {
         logger.error("Failed to Publish Port Status");
         return -1;
      }
   }   
   return 0;
}

/**
 * @fn      int update_local_serial_number(u8 *rxData)
 * @brief   processes updates to serial number status to compare 
            against what is already there  
 * @param   [in] rxData - incoming serial number data
 * @retval  returns 0 on success, otherwise, it error
 */
int update_local_serial_number(u8 *rxData)
{
   bool changes            = false;
   u8  *pLocalSerialNumber = (u8 *)&localSerialNumber;
   
   for (u32 i = 0 ; i < sizeof(localSerialNumber) ; i++)
   {
      if (rxData[i] != pLocalSerialNumber[i])
      {
//         logger.debug("SerNum[%d] change <%02X to %02X>", i,pLocalSerialNumber[i], rxData[i]);
         pLocalSerialNumber[i] = rxData[i];
         changes = true;
      }
   }

   // check if changes
   if (changes && (pHandlePub != NULL))
   {
      logger.highlight("Publish Change in Serial Number");
      
//      change local_port_status to something else
      if ((bm_send(pHandlePub, (u8 *)D2_SERIAL_NUM, &localSerialNumber, sizeof(localSerialNumber))) < 0)
      {
         logger.error("Failed to Publish Port Status");
         return -1;
      }
   } // if changes
   
   return 0;
}

/**
 * @fn    void process_nack_cmd(pMsgSerial rxMsg)
 * @brief processes the receipt of nack command 
 * @param   n/a
 * @retval  n/a
 * @note    DO NOT TRY TO FREE PBACK - its on a list
 */
void process_nack_cmd(pMsgSerial rxMsg)
{
   pMsgSerial pBack = NULL;
   u32 numRetries = 0;
   logger.warning("Got NACK for Cmd/Rn<%s/%02X>",cmdId2TxtD2(rxMsg->command_id),rxMsg->command_id);   
   if ((pBack = get_copy_requestNumber_msg(rxMsg->request_number, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT, &numRetries)))
   {
      if (numRetries >= 3)
      {
         logger.warning("too many nack responses for command <%s>",cmdId2TxtD2(rxMsg->command_id));
         restart_discovery();
      }
      else
      {
         logger.highlight("NACK CMD - Resending Cmd/RN <%s/%02X>",cmdId2TxtD2(rxMsg->command_id),rxMsg->request_number);
         pBack->request_number |= REQUEST_NUMBER_CMD_BIT_MASK;
         build_send_serial_msg(serial_fd, pBack, &serialMsgLock);
      }
   }
   else
   {
      logger.warning("Unexpected Nack<%02X> cannot find in backlog",rxMsg->request_number);
      restart_discovery();
      return;
   }
}

/**
 * @fn      int process_scd_to_bdm_messages(pMsg rxMsg)
 * @brief   processes rx messages for rtos 
 * @param   [in]  rxMsg - message to be interpreted
 * @retval  returns 0 on success, otherwise, it error
 */
int process_scd_to_bdm_messages(pMsgSerial rxMsg)
{
   int status = 0;
   // validate protocol 
   if (rxMsg->protocol_id != PROTOCOL_SCD_TO_BDM)
   {
      logger.error("Invalid Protocol ID <%02X> from D2 SCD???",rxMsg->protocol_id);
      send_nack_cmd(NACK_GENERAL_ERROR, rxMsg->command_id, rxMsg->request_number);
      restart_discovery();
      return 0;
   }
   
   // determine if proper protocol (should be)
   u8 requestNumber = rxMsg->request_number;
   u8 ackResponse   = 0;
   bdm_serial_msg_container_t txMsg;
   txMsg.protocol_id  = PROTOCOL_BDM_TO_SCD;
   txMsg.cmd_data_len = ONE_BYTE_ACK_RESPONSE_DATA_LEN;
   txMsg.cmd_data     = &ackResponse; // ACK

   // dispatch command  
   switch(rxMsg->command_id)
   {
   case SCD_TO_BDM_TRIGGER_OUT_EVENT:
   {
      int ext_trigger_from_db;
      if (getField_ext_trigger(&ext_trigger_from_db) < 0)
      {
          syslog(LOG_ERR, "bdmD2: configure services ext_trigger value read error from db");
      }
      else if (ext_trigger_from_db)
      {
         logger.info("Got Lavage Trigger");
         txMsg.request_number = RESPONSE_REQUEST_NUMBER(requestNumber);
         txMsg.command_id     = BDM_TO_SCD_TRIGGER_OUT_EVENT_RESP;
         build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
//         assert (isD25Connected == false);
         start_d25_trigger();
      }
      else
      {
         syslog(LOG_ERR, "bdmD2: external trigger service is disabled - but got trigger!!!");
         logger.info("bdmD2: external trigger service is disabled - but got trigger!!!");
      }
   }
   break;
   
   case SCD_TO_BDM_SET_DEVICE_INFO_RESPONSE:
   {
      logger.debug("Set DevInfo Status <%02X/%02X>", *rxMsg->cmd_data, rxMsg->request_number);
      assert(get_num_msg_backed());
      if (rxMsg->cmd_data[0])
      {
         process_nack_cmd(rxMsg);
      }
      else
      {
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtD2(rxMsg->command_id), get_num_msg_backed());
         }
      }
   }
   break;
            
   case SCD_TO_BDM_SCD_CMD_RESP:
   {
      assert(get_num_msg_backed());
      if (rxMsg->cmd_data[0])
      {
         logger.warning("WARN: scd cmd response <%02X>", *rxMsg->cmd_data);
         process_nack_cmd(rxMsg);
      }
      else
      {
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtD2(rxMsg->command_id), get_num_msg_backed());
         }
      }
   }
   break;
   
   case SCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE:
   {
      u8 whichBlob     = rxMsg->cmd_data[0];
      logger.debug("[%02X] GOT Blob(%d) Response (%d)",requestNumber, whichBlob, num_rx_get_data_blobs);
      if (get_num_msg_backed() == 0)
      {
         logger.warning("unexpected get blob(%d)??",whichBlob);
         restart_discovery();
         return 0;
      }
      
      if ((clear_backup_serial_msg(rxMsg->request_number)))
      {
         logger.error("<%s> Fail Clear(%d)", cmdId2TxtD2(rxMsg->command_id), get_num_msg_backed());
         restart_discovery();
         return 0;
      }

      u8 *pData   = &rxMsg->cmd_data[1];
      u8 dataSize = rxMsg->cmd_data_len-1;
      u8 nextBlob = whichBlob+1;
      
      if (whichBlob == 1)
      {
         assert(dataSize == sizeof(blobHeader));
         memcpy(&blobHeader, pData, dataSize);

         u32 rx_blob_data_size = ntohs(blobHeader.blob_data_size)+1;         
         if (((rx_blob_data_size) % MAX_OPAQUE_DATA_PACKET))
         {
            logger.warning("BlobDataSize+1(%d) isn't even",rx_blob_data_size, MAX_OPAQUE_DATA_PACKET);
            restart_discovery();
            return 0; // don't do anything else
         }
         num_rx_get_data_blobs = (rx_blob_data_size / MAX_OPAQUE_DATA_PACKET) + 1; // for blob header
         if (pBlobData)
         {
            logger.debug("freeing previous version of blob");
            free(pBlobData);
         }
         pBlobData = calloc(rx_blob_data_size,1);            
         send_get_blob_scd(nextBlob);
      }
      else
      {
         u8 blobIndex   = (whichBlob-2);
         u16 dataOffset = (blobIndex * MAX_OPAQUE_DATA_PACKET);
         assert(pBlobData);
         assert(dataSize == MAX_OPAQUE_DATA_PACKET);
         memcpy(&pBlobData[dataOffset], pData, MAX_OPAQUE_DATA_PACKET);
         if (whichBlob == num_rx_get_data_blobs)
         {
            display_blob(&blobHeader, pBlobData, requestNumber);
            logger.highlight("Finished getting blob for client - now send to client");
            if (pendingActions & RESPOND_GET_SETUP_BLOB_CLIENT_MASK)
            {
               respond_get_setup_blob_client();
            }            
         }
         else
         {
            send_get_blob_scd(nextBlob);
         }
      }
   }
   break;
   
   case SCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE:
   {
      assert(get_num_msg_backed());
      pBlobDataAck pAck = (pBlobDataAck)rxMsg->cmd_data;
      u8 whichBlob      = pAck->packetNumber;
      if (pAck->status)
      {
         logger.warning("Got NACK (%d) for Set Blob (%d)",pAck->status, whichBlob);
         process_nack_cmd(rxMsg);
         if (pendingActions & RESPOND_SET_SETUP_BLOB_CLIENT_MASK)
         {
            logger.highlight("Sending error set response to setup blob client");
            respond_set_setup_blob_client(1);
         }
      }
      else 
      {
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtD2(rxMsg->command_id), get_num_msg_backed());
            restart_discovery();
            return 0; // don't do anything else
         }
      
//  logger.debug("[%02X] Set Blob ACK <%d> status <%d>", requestNumber, whichBlob, pAck->status);
         if (whichBlob < num_tx_set_data_blobs)
         {
            send_set_blob_scd(whichBlob+1);
         }
         else if (pendingActions & RESPOND_SET_SETUP_BLOB_CLIENT_MASK)
         {
            logger.highlight("Finished set setupblob from Client - respond");
            respond_set_setup_blob_client(0);
         }
         else
         {
            logger.highlight("completed local set setup blob");
         }
      }
   }
   break;

   case SCD_TO_BDM_GENERIC_RESPONSE:
   {
      pGenReply pReply = (pGenReply)rxMsg->cmd_data;
      if (common_system_state.state != WAITING_CMD_RESPONSE)
      {
         logger.error("Unsolicited GenAck Response <%02X>",pReply->status);
      }

      logger.debug("Got [%02X] Gen Response Status<%d> in <%d> state for Cmd <%02X>",
                   requestNumber, pReply->status, common_system_state.state,pReply->cmd_id);
   }
   break;
            
   case SCD_TO_BDM_DISCOVERY_RESPONSE:
   {
      pCapdev2BdmDisc pDiscResp = (pCapdev2BdmDisc)rxMsg->cmd_data;
      logger.debug("[%02X] DiscResp(%d): Ver<%2X> Device <is %s ready> DevType<%d> DevSubType(%d) Local State<%d> WaitCnt(%d)",
                   requestNumber, common_system_state.state,
                   (pDiscResp->version_number),
                   (pDiscResp->device_state ? "" : "not"),
                   (u8)pDiscResp->devType,
                   pDiscResp->devSubType,
                   common_system_state.state,
                   waiting4DiscoveryCount);

      ////////////////////////////////////////
      ////////////////////////////////////////
      // TAM:FIXME - remove this DEBUG code //
      ////////////////////////////////////////
      ////////////////////////////////////////
#if 1
      if ((pDiscResp->version_number == DEFAULT_VERSION_BYTE) || (pDiscResp->version_number == TEST_VALIDATE_VERSION))
#else
      if (pDiscResp->version_number == DEFAULT_VERSION_BYTE)
#endif         
      ////////////////////////////////////////
      ////////////////////////////////////////
      ////////////////////////////////////////
      ////////////////////////////////////////
      {
         memcpy(&localD2DiscoveryData, rxMsg->cmd_data, CAPDEV_TO_BDM_DISCOVERY_RESP_LENGTH);
         if (common_system_state.state == WAITING_FOR_DISCOVERY_COMPLETION)
         {
            waiting4DiscoveryCount = 0;
            rxHeartbeatMiss        = 0;
            rxHeartbeatCount       = 0;
            waitGetPortStatus      = false;
            common_system_state.state = DISCOVERED_IDLE;
         }
         else
         {
            logger.error("Unexpected Discovery Response in State <%d>",common_system_state.state);
         }
      }
      else
      {
         logger.error("Unsupported version (%02X) from D2",pDiscResp->version_number);
      }
      ////////////////////////////////////////
      ////////////////////////////////////////
   }
   break;
            
   case SCD_TO_BDM_HEARTBEAT_CMD:
   {
      // increment receive right now //
      rxHeartbeatCount++;
      rxHeartbeatMiss = 0;
      if (isD2Connected == false)
      {
         logger.debug("SCD is (RE)connected");
      }
              
      isD2Connected = true;                  
      memcpy(&localRxHBStatus, rxMsg->cmd_data, HB_SCD_BDM_DATA_LENGTH);
      if (!isD2Ready && (localRxHBStatus.devStatus & HB_DEVICE_READY_BIT_MASK))
      {
         isD2Ready = true;
         logger.debug("...SCD HAS BECOME Ready...");
         publish_connected_status(true);
      }
   } // if heartbeat //
   break; 

   case SCD_TO_BDM_SERIAL_NUMBER_STATUS:
   {
      if ((update_local_serial_number(rxMsg->cmd_data)))
      {
         status = 666;
         break;
      }
      
      logger.debug("[%d] Sending Serial number Response",requestNumber);
      txMsg.command_id      = BDM_TO_SCD_SERIAL_NUMBER_RESPONSE;
      txMsg.request_number  = RESPONSE_REQUEST_NUMBER(requestNumber);
      build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
   }
   break;
   
   case SCD_TO_BDM_PORT_STATUS:
   {
      if ((update_local_port_status(rxMsg->cmd_data)))
      {
         status = 6;
         break;
      }
///////////////////////////////////////////
//////////// remove for performance ///////
///////////////////////////////////////////
#if 0         
      logger.debug("[%02X/%d] PortA/B <%s/%s> <%02X/%02X>",
                   requestNumber, waitGetPortStatus,
                   (localPortStatus.porta_speed_run & PORT_RUNNING_MASK)  ?
                   "Running" : "Stopped",
                   (localPortStatus.portb_speed_run & PORT_RUNNING_MASK)  ?
                   "Running" : "Stopped",
                   localPortStatus.porta_speed_run,
                   localPortStatus.portb_speed_run);
#endif 
///////////////////////////////////////////
///////////////////////////////////////////
      if (waitGetPortStatus == false)
      {
//         logger.debug("[%d] Sending Port Status Response",requestNumber);
         txMsg.command_id = BDM_TO_SCD_PORT_STATUS_RESP;
         txMsg.request_number  = RESPONSE_REQUEST_NUMBER(requestNumber);
         build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
      }
      else
      {
         assert(get_num_msg_backed());
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtD2(rxMsg->command_id), get_num_msg_backed());
         }

      }
      waitGetPortStatus = false;
   }
   break;

   default:
      send_nack_cmd(NACK_CMD_NOT_SUPPORTED, rxMsg->command_id, rxMsg->request_number);
      logger.warning("Unexpected SCD to BDM Command (%d) received",rxMsg->command_id);
      break;
   } // end switch //

   return status;
}

/**
 * @fn      int bdmD2_task(void)
 * @brief   bdm task for all bridge processing
 * @retval  NULL (exits when told to)
 */
int bdmD2_task(void)
{
   pthread_t serialThreadID;
   char cmdBuf[4];

   // init common signal handler
   init_common_signal_handler();
     
   // reset structs */
   bzero(&common_system_state,sizeof(common_system_state));
   common_system_state.state = WAITING_FOR_DISCOVERY_COMPLETION;
   
   // initialize serial lock and serial date
   pthread_mutex_init(&serialMsgLock,   NULL);
   pthread_mutex_init(&sendTriggerLock, NULL);
   pthread_mutex_init(&msgSetupBlobLock,NULL);

   init_local_data();
     
   // register setup serial port 
   if ((init_serial_connection(&serial_fd, comport, 0)))
   {
      logger.error("RTOS Error in initializing Serial Connection");
      goto bdm_exit_task;
   } // error in bdm //
     
   logger.debug("Serial Port Initialized <%d>",serial_fd);

   // create data to pass between threads //
   pSerialRxArgs pArgs;
   if ((pArgs = calloc(1,sizeof(serial_rx_args_t))) == NULL)
   {
      logger.error("Failed to calloc rx Serial args: <%s>",COMMON_ERR_STR);
      goto bdm_exit_task;
   }

   // create a generic socket read thread //
   pArgs->serialProcessData = process_scd_to_bdm_messages;
   pArgs->serial_fd         = &serial_fd;
   pArgs->pCmds             = &bdmRxCmds[0];
   pArgs->numCmds           = NUM_SUPPORTED_SCD2BDM_CMDS;
   pArgs->stopFlag          = &common_system_state.stopping;
      
   if ((common_create_thread(&serialThreadID, HIGHEST_PRIORITY, (void *)pArgs, serial_rx)))
   {
      logger.error("Error in creating serial_ messages thread");
      goto bdm_exit_task;
   }

   //create heartbeat // 
   pthread_t hbTxThrd;
   if ((common_create_thread(&hbTxThrd, MEDIUM_LOW, NULL, bdm_tx_heartbeat)))
   {
      logger.error("Error in creating TX CMD SOCKET Thread");
      goto bdm_exit_task;  
   }

   pthread_t dsc_hb_Thrd;
   if ((common_create_thread(&dsc_hb_Thrd, MEDIUM_MEDIUM, NULL, bdm_publish_discovery_heartbeat)))
   {
      logger.error("Error in creating discovery heartbeat message Thread");
      goto bdm_exit_task;  
   }

   pthread_t set_hbpf_thread;
   if ((common_create_thread(&set_hbpf_thread, MEDIUM_LOW, NULL, bdm_wait_hbpf)))
   {
      logger.error("Error in creating Set hbpf thread");
      goto bdm_exit_task;  
   }
   
   pthread_t clientList;
   if ((common_create_thread(&clientList, MEDIUM_LOW, NULL, bdm_wait_clientList)))
   {
      logger.error("Error in creating clientList command thread");
      goto bdm_exit_task;  
   }

   pthread_t set_scdCmd_thread;
   if ((common_create_thread(&set_scdCmd_thread, MEDIUM_LOW, NULL, bdm_wait_scdCmd)))
   {
      logger.error("Error in creating set scdCmd Thread");
      goto bdm_exit_task;  
   }
   
   pthread_t set_osd_thread;
   if ((common_create_thread(&set_osd_thread, MEDIUM_LOW, NULL, bdm_wait_osd)))
   {
      logger.error("Error in creating set osd Thread");
      goto bdm_exit_task;  
   }

   pthread_t bdmSetupBlobThread;
   int surgeon_profile_from_db;
   if (getField_surgeon_profile(&surgeon_profile_from_db) < 0)
   {
      syslog(LOG_ERR, "bdmD2: configure services value read error from db");
      logger.error_internal("bdmD2: configure services value read error from db");
      goto bdm_exit_task;
   }
   else if (surgeon_profile_from_db)
   {
      if ((common_create_thread(&bdmSetupBlobThread, MEDIUM_LOW, NULL, bdm_setupBlob_server)))
      {
         logger.error_internal("Error in creating setupblob server");
         goto bdm_exit_task;
      }
   }
   else
   {
      syslog(LOG_ERR, "bdmD2: setupBlob service is disabled!!!");
      logger.info("bdmD2: setupBlob service is disabled!!!");
   }

   // enter main command loop //
   if (doCli)
   {
      YIELD_500MS;
      YIELD_500MS;
   }
   
   while (!common_system_state.stopping)
   {
      if (doCli == 0)
      {
         sleep(1);
         continue;
      }
      
      bzero(cmdBuf,sizeof(cmdBuf));
      DBG("\t   ------------Serial BDM CLI -----------\n\n"  
          "\t   ---------------------------------------\n"
          "\t   WE ARE DOING CLI (%d)\n"
          "\t   ---------------------------------------\n"
          "\t   6) Get Port Status from SCD\n"
          "\t   7) Send SCD command (from get/set) to SCD\n"
          "\t   8) Get Blob from SCD\n"
          "\t   9) Send Blob to SCD\n"
          "\t   q) Quit - exit entire program", doCli);
      DBG("\n\t----------------------------------------------------------\n");      


      if ((getStringStatus("",cmdBuf,sizeof(cmdBuf))))
         goto bdm_exit_task;


      // determine request //
      //  bool yesNotNo = true;
      //         yesNotNo = true;
      switch (cmdBuf[0])
      {
         ///////////////////////
         // Change Log Levels //
         ///////////////////////
      case '6':
      {
         logger.debug("---Sending GET Port Status Info---");
         send_get_port_status_scd();
      }
      break;

      case '7':
      {
         logger.debug("---Sending SCD command---");
         static u8 scd_cmd = 0x01;
         send_next_scd_cmd(scd_cmd, false);
         scd_cmd++;
         if (scd_cmd >= MAX_SCD_COMMAND)
            scd_cmd = 1;
      }
      break;

      case '8':
      {
         logger.debug("---Get Blob Request---");
         send_get_blob_scd(FIRST_DATA_BLOB_NUMBER);
      }
      break;

      case '9':
      {
         if (pBlobData)
         {
            logger.debug("---Set Blob Request---");
            num_tx_set_data_blobs = num_rx_get_data_blobs;
            send_set_blob_scd(FIRST_DATA_BLOB_NUMBER);
         }
         else
         {
            logger.error("There is no valid blob data to send to capital device - please do a get before trying a set");
         }
      }
      break;
               
      //////////////////////////
      //   Exit Serial RTOS   //
      //////////////////////////
      case 'q':
      case 'Q':
         DBGF("Exiting main program - user prompted\n");
         goto bdm_exit_task;
         break; // 'q' //

      default:
         DBGF("unknown input(%c)\n",cmdBuf[0]);
         break; // default //
      } // end switch command //
         
   } // end while not stopping
       
bdm_exit_task:
   logger.highlight("Exiting BDM(%d) - Close socket & wait for read thread to exit",common_system_state.stopping);
   common_system_state.stopping = true;
   common_close_socket(&serial_fd);
   common_close_socket(&setupBlobServerFd);
   common_close_socket(&d25Socket);
   common_kill_thread(clientList,         "wait_client list");
   common_kill_thread(bdmSetupBlobThread, "setup thread");
   common_kill_thread(serialThreadID,     "Serial Thread");
   common_kill_thread(set_hbpf_thread,    "hbpf");
   common_kill_thread(set_scdCmd_thread,  "scd cmd");
   common_kill_thread( set_osd_thread,    "osd sub");
   common_kill_thread(dsc_hb_Thrd,        "dsc hb");
   common_kill_thread(txTrigger,          "tx trigger");
   common_kill_thread(rxTrigger,          "rx trigger");
   logger.highlight("---- Exiting BDM_TASK -----\n");
   exit(0);
}


int main(int argc, char *argv[])
{
   if (argc > 1 && !strcmp(argv[1],"-V")) {
       printf("%s\n", version);
       return 0;
   }

   // check to make sure yo ugot a host //
   if ((argc < 2) || (argc > 3))
   {
      fprintf(stderr,"usage %s <COM Port> <(optional) 'Do CLI' (0/1(cli is enabled by default))>\n", argv[0]);
      exit(0);
   }
   else
      comport = argv[1];
    
   if (argc == 3)
      doCli = atoi(argv[2]);
  
   return (bdmD2_task()); 
}

/**
 * @file bdmD2Utils.c
 * @brief bdm D2 utils
 * @details
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <linux/types.h>
#include <arpa/inet.h>

#include "commonTypes.h"
#include "commonState.h"
#include "connUtils.h"
#include "msgSerial.h"
#include "logger.h"
#include "msgIp.h"
#include "msgD2.h"
#include "broker_api.h"
#include "msgGetSetD2.h"
#include "bdmD2Private.h"
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

// these are commands
extern pthread_mutex_t serialMsgLock;
extern pthread_mutex_t msgGetSetLock;
extern pthread_mutex_t msgSetupBlobLock;
extern bool isD2Connected;
extern u32 txHeartbeatCount;
extern OSD_coords_t localCoords;
extern u32 blink_led;

/**
 * @fn      void send_next_set_dev_info(void)
 * @brief   sends the next set_dev_info to the SCD
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void send_next_set_dev_info(u8 info, bool remote)
{
   bdm_serial_msg_container_t txMsg;
   set_scd_device_info_t   set_scd;
   bzero((u8 *)&set_scd, sizeof(set_scd));
   
   // get next - reset flag
   set_scd.hand_blade_pump_foot = info;
      
   // set-up msg
   txMsg.protocol_id         = PROTOCOL_BDM_TO_SCD;
   txMsg.command_id          = BDM_TO_SCD_SET_DEVICE_INFO_REQUEST;
   txMsg.cmd_data_len        = SET_DEVICE_INFO_DATA_LEN;
   txMsg.cmd_data            = (u8 *)&set_scd;
   txMsg.request_number      = getNextTxSeqSerial();
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   
   txMsg.request_number = SET_CMD_REQUEST_NUMBER(txMsg.request_number);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
   if (remote)
   {
      pendingActions |= RESPOND_SET_DEV_INFO_CLIENT_MASK;
   }
}

/**
 * @fn      void send_next_scd_cmd(void)
 * @brief   sends the next set_dev_info
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void send_next_scd_cmd(u8 cmd, bool remote)
{
   u8 nextCmd = cmd;

   bdm_serial_msg_container_t txMsg;
   txMsg.protocol_id         = PROTOCOL_BDM_TO_SCD;
   txMsg.command_id          = BDM_TO_SCD_SCD_CMD_REQ;
   txMsg.cmd_data_len        = BDM_TO_SCD_SCD_CMD_LEN;
   txMsg.cmd_data            = (u8 *)&nextCmd;
   txMsg.request_number      = getNextTxSeqSerial();
      
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
   if (remote)
   {
      pendingActions |= RESPOND_SEND_SCD_CMD_CLIENT_MASK;
   }
}

/**
 * @fn      void send_set_blob_scd(u8 whichBlob)
 * @brief   handles sending the next set setup blob segment
 * @param   whichBlob - which part of blob to send
 * @retval  n/a
 **************************************************************/
void send_set_blob_scd(u8 whichBlob)
{  
   bdm_serial_msg_container_t txMsg;
   u8 dataSize;
   u8 *txData  = NULL;
   if (whichBlob == 1)
   {
      dataSize = (sizeof(blobHeader) + 1);
      txData   = (u8 *)calloc(dataSize,1);
      memcpy(&txData[1],&blobHeader, dataSize-1);
   }
   else
   {
      assert(pBlobData);
      u16 blobIndex = ((whichBlob-2)*MAX_OPAQUE_DATA_PACKET);
      u8 *pSource    = &pBlobData[blobIndex];
      dataSize      = MAX_OPAQUE_DATA_PACKET+1;
      txData        = (u8 *)calloc(dataSize,1);
      memcpy(&txData[1],pSource,MAX_OPAQUE_DATA_PACKET);
   }
   
   txData[0]            = whichBlob;
   txMsg.protocol_id    = PROTOCOL_BDM_TO_SCD;
   txMsg.command_id     = BDM_TO_SCD_SET_BLOB_CONFIG_REQUEST;
   txMsg.cmd_data       = txData;
   txMsg.cmd_data_len   = dataSize; // for blob number
   txMsg.request_number = getNextTxSeqSerial();
   
   // build data payload //
   logger.debug("---Send Set Blob <%d> to SCD (size(%d)) ---", whichBlob, dataSize);
   if (whichBlob == FIRST_DATA_BLOB_NUMBER)
   {
      display_blob(&blobHeader, pBlobData, txMsg.request_number);
   }   
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void send_get_blob_scd(u8 whichBlob)
 * @brief   handles sending the next get setupblob request
 * @param   whichBlob - which part of blob to get
 * @retval  n/a
 **************************************************************/
void send_get_blob_scd(u8 whichBlob)
{
   bdm_serial_msg_container_t txMsg;
   txMsg.protocol_id    = PROTOCOL_BDM_TO_SCD;
   txMsg.command_id     = BDM_TO_SCD_GET_BLOB_CONFIG_REQUEST;
   txMsg.cmd_data_len   = 1;
   txMsg.cmd_data       = &whichBlob;
   txMsg.request_number = getNextTxSeqSerial();

   // backup waiting //
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void send_get_port_status(void)
 * @brief   sends a get port status to scd
 * @retval  n/a
 **************************************************************/
extern bool waitGetPortStatus;
void send_get_port_status_scd(void)
{
   bdm_serial_msg_container_t txMsg;
   txMsg.protocol_id    = PROTOCOL_BDM_TO_SCD;
   txMsg.command_id     = BDM_TO_SCD_GET_PORT_STATUS;
   txMsg.request_number = getNextTxSeqSerial();
   txMsg.cmd_data       = 0;
   txMsg.cmd_data_len   = 0;
   waitGetPortStatus    = true;

   // add to backup list and send
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void send_set_setup_blob(void)
 * @brief   utility routine when setup blob clients requests a set
 * @retval  n/a
 **************************************************************/
void send_set_setup_blob_scd(void)
{
   pendingActions |= RESPOND_SET_SETUP_BLOB_CLIENT_MASK;
   send_set_blob_scd(FIRST_DATA_BLOB_NUMBER);
}

/**
 * @fn      void send_get_setup_blob(void)
 * @brief   utility routine when setup blob client requests a get
 * @retval  n/a
 **************************************************************/
void send_get_setup_blob_scd(void)
{
   pendingActions |= RESPOND_GET_SETUP_BLOB_CLIENT_MASK;
   send_get_blob_scd(FIRST_DATA_BLOB_NUMBER);
}

/**
 * @fn      void respond_get_setup_blob_client(void)
 * @brief   responds to the setup blob client the get blob data
 * @retval  n/a
 ******************************x********************************/
void respond_get_setup_blob_client(void)
{
   ct_ip_msg_t txMsg;
   u32 blobCopySize = ntohs(blobHeader.blob_data_size) + 1;
   u32 osdOffset    = blobCopySize + BLOB_DATA_HEADER_SIZE;
   u32 totalTx      = osdOffset + BLOB_COORDS_SIZE;
   
   // initialize message default (get)
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = SETUP_BLOB_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = SETUP_BLOB_GET_RESPONSE;
   txMsg.cmd_data_len        = totalTx;
   txMsg.cmd_sequence_number = getNextTxSeqSetupBlob();
   
   // allocate data to be sent //
   txMsg.cmd_data = calloc(totalTx+8,1); // 
   assert(txMsg.cmd_data != NULL);

   // copy data over
   memcpy(txMsg.cmd_data, &blobHeader, BLOB_DATA_HEADER_SIZE);
   memcpy(&txMsg.cmd_data[BLOB_DATA_HEADER_SIZE], pBlobData, blobCopySize);
   memcpy(&txMsg.cmd_data[osdOffset], &localCoords.portA_x, BLOB_COORDS_SIZE);
   
   // send the packet
   if ((build_send_ip_msg(setupBlobClientFd,&txMsg, &msgSetupBlobLock)))
   {
      logger.error("Failed to send set setup blob data response");
   }

   // free allocated memory //
   freeIpMsgContainerData(&txMsg);
   pendingActions &= ~RESPOND_GET_SETUP_BLOB_CLIENT_MASK;
}

/**
 * @fn      void respond_get_setup_blob_client(u8 setStatus)
 * @brief   responds to the setup blob client with the set setup blob status
 * @retval  n/a
 **************************************************************/
void respond_set_setup_blob_client(u8 setStatus)
{
   ct_ip_msg_t txMsg;
    
   // initialize message default (get)
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = SETUP_BLOB_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = SETUP_BLOB_SET_RESPONSE;
   txMsg.cmd_data_len        = SET_SETUP_BLOB_RESPONSE_SIZE;
   txMsg.cmd_sequence_number = getNextTxSeqSetupBlob();
   txMsg.cmd_data            = &setStatus;
   logger.debug("Sending Set Blob response(%d) to client",setStatus);

   // send the packet
   if ((build_send_ip_msg(setupBlobClientFd,&txMsg, &msgSetupBlobLock)))
   {
      logger.error("Failed to send set setup blob data response");
   }
   else
   {
   }
   // reset the need to respond to set
   pendingActions &= ~RESPOND_SET_SETUP_BLOB_CLIENT_MASK;
}


/**
 * @fn      const char *cmdId2TxtD2(u8 cmdID)
 * @brief   debug utility to return ascii string representation of the binary command
 * @param   cmdID - command id to display
 * @retval  cmdID as a string
 **************************************************************/
const char *cmdId2TxtD2(u8 cmdID)
{
   switch (cmdID)
   {
   case CAPDEV_D2_GENERIC_REPLY:          return "Generic Reply";
   case CAPDEV_D2_DISCOVERY:              return "Discovery";
   case CAPDEV_D2_HEARTBEAT:              return "Heartbeat";
   case CAPDEV_D2_PORT_STATUS:            return "Port Status Info";
   case CAPDEV_D2_GET_PORT_STATUS:        return "Get Port Status";
   case CAPDEV_D2_SET_DEV_INFO:           return "Set Dev Info";
   case CAPDEV_D2_LAVAGE_TOGGLE_EVENT:    return "Lavage Toggle";
   case CAPDEV_D2_SCD_CMD_REQUEST:        return "SCD CMD Req";
   case CAPDEV_D2_GET_CONFIG_BLOB:        return "Get Config Blob";
   case CAPDEV_D2_SET_CONFIG_BLOB:        return "Set Config Blob";
   default:                            return "Unknown";
   }
}



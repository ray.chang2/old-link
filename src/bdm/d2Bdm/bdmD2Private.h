/**
 * @file bdmD2Private.h
 * @brief private include for utils directory
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __BDM_D2_PRIVATE_H__
#define __BDM_D2_PRIVATE_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgGetSetCommon.h"
#include "msgD2.h"

/**
 * private/local variables
 */
// general
extern int  serial_fd, setupBlobClientFd;

// blob related
extern ct_blob_data_header_t blobHeader;
extern u8 *pBlobData;

// this is related to transmit / receive operations 

// pending commands 
extern u32 pendingActions;

/////////////////////////////////
/////////////////////////////////
/// get/set pending info
////////////////////////////////
///////////////////////////////
#define SEND_DEV_INFO_SCD_MASK             BIT_MASK(0)
#define SEND_SCD_CMD_SCD_MASK              BIT_MASK(1)
#define SEND_SET_SETUP_BLOB_SCD_MASK       BIT_MASK(2)
#define SEND_GET_SETUP_BLOB_SCD_MASK       BIT_MASK(3)

#define RESPOND_GET_SETUP_BLOB_CLIENT_MASK BIT_MASK(16)
#define RESPOND_SET_SETUP_BLOB_CLIENT_MASK BIT_MASK(17)
#define RESPOND_SET_DEV_INFO_CLIENT_MASK   BIT_MASK(18)
#define RESPOND_SEND_SCD_CMD_CLIENT_MASK   BIT_MASK(19)

const char *cmdId2TxtD2(u8 cmdID);
int  getStringStatus(const char *prompt, char *copyTo, u8 maxSize);
u8   getNextTxSeqSetGet(void);
u8   getNextTxSeqSetupBlob(void);
void respond_get_setup_blob_client(void);
void respond_set_setup_blob_client(u8 setStatus);
void check_rx_sequence_setupBlob(u8 rxSeq, u8 cmdId);
void send_next_set_dev_info(u8 info, bool remote);
void send_next_scd_cmd(u8 cmd, bool remote);
void send_get_blob_scd(u8 whichBlob);
void send_set_blob_scd(u8 whichBlob);
void send_set_setup_blob_scd(void);
void send_get_setup_blob_scd(void);
void send_get_port_status_scd(void);


#endif // __BDM_D2_PRIVATE_H__

/**
 * @file getSetD2.c
 * @brief getset server for D2
 * @details
 *
 * @version .01
 * @author  Tom Morrison
 * @date 1/12/19
 *
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   1/12/19   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>

#include "zmq.h"
#include "dsc.h"

#include "commonTypes.h"
#include "commonState.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgSerial.h"
#include "msgIp.h"
#include "msgD2.h"
#include "msgGetSetD2.h"
#include "connUtils.h"
#include "broker_api.h"
#include "D2_channels.h"
#include "WW_channels.h"
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

/**
 * static/private/local variables
 */
int  getSetClientFd      = -1;
u8   expectedCmdSequence = 1;

void *pHandlePub = NULL;
void *pHandleSub = NULL;

bool isWerewolf = false;
char *bdmType   = "D2";
/**
 * global variable(s)
 */
common_state_t common_system_state;

/////////////////////////////////
////////// local cache //////////
/////////////////////////////////

bool isBdmConnected = false;
OSD_coords_t localCoords = {0, 0x11, 0x22, 0x33, 0x44 };
bool oldConnected = false;
///////////////////////////////////
// initialize data //
///////////////////////////////////
void init_local_data(void)
{
   // reset both of these //
   isBdmConnected = false;
   if ((pHandlePub = bm_publisher()) == NULL)
   {
      logger.error("Failure to create publisher handler");
   }
   void *context = zmq_ctx_new();
   zmq_ctx_destroy(context);
}

/**
 * @fn      void *osdTest_wait_connected(UNUSED_ATTRIBUTE void *args)
 * @brief   thread to wait BDM connected status
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *osdTest_wait_connected(UNUSED_ATTRIBUTE void *args)
{
   int status;
   u32 bytesRead;
   void *pHandleSub          = NULL;
   u8 filter[BROKER_FILTER_MAX];
   char *sub = D2_CONNECTED;
   if (isWerewolf)
   {
      sub = WW_CONNECTED;
   }
   // create a handle
   if ((pHandleSub = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create subscriber Handler for connection status");
      return NULL;
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSub, sub, strlen(sub))))
   {
      logger.error("Failed to subscribe to %s_Connected",bdmType);
      return NULL;
   }

   u8 *pConn;
   while (1)
   {
      if ((status = bm_receive(pHandleSub, filter, (void*) &pConn, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive for %d", status, bdmType);
         return NULL;
      }

      oldConnected = isBdmConnected;
      isBdmConnected = *pConn;
      logger.debug("%s Connected State  <%d/%d>",bdmType, isBdmConnected, oldConnected);
      free(pConn);
   }
   return NULL;
}

/**
 * @fn      void *osdTest_wait_coords(UNUSED_ATTRIBUTE void *args)
 * @brief   thread to wait d2 connected status
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *osdTest_wait_coords(UNUSED_ATTRIBUTE void *args)
{
   int status;
   u32 bytesRead;
   void *pHandleSub = NULL;
   void *pCoords    = NULL;
   u8 filter[BROKER_FILTER_MAX];
   
   // create a handle
   if ((pHandleSub = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return NULL;
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSub, BROKER_BDM_COORDS, strlen(BROKER_BDM_COORDS))))
   {
      logger.error("Failed to subscribe to D2 coords");
      return NULL;
   }

   while (1)
   {
      if ((status = bm_receive(pHandleSub, filter, (void*) &pCoords, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive for coords",status);
         return NULL;
      }

      assert(bytesRead >= sizeof(localCoords));
      memcpy(&localCoords, pCoords, sizeof(localCoords));
      logger.highlight("%s OSD COORDS received (%02X/%02X/%02X/%02X)",bdmType,
                       localCoords.portA_x, localCoords.portA_y, 
                       localCoords.portB_x, localCoords.portB_y);
      free(pCoords);
   }
   
   return NULL;
}

void send_coords(void)
{
   logger.highlight("SENDING OSD COORDS (%02X/%02X/%02X/%02X)",
                    localCoords.portA_x, localCoords.portA_y, 
                    localCoords.portB_x, localCoords.portB_y);

   if ((bm_send(pHandlePub, (u8 *)BROKER_OSD_COORDS, &localCoords, sizeof(localCoords))) < 0)
   {
      logger.error("Failed to Publish Port Status");
      sleep(1);
      exit(-666);
   }
   
   localCoords.portB_y++;
}

/**
 * @fn      int bdmD2_task(void)
 * @brief   bdm task for all bridge processing
 * @retval  NULL (exits when told to)
 */
int osdTest_task(void)
{
   // init common signal handler
   init_common_signal_handler();
     
   // reset structs */
   bzero(&common_system_state,sizeof(common_system_state));

   init_local_data();

   
   pthread_t waitCoords;
   pthread_t wait_connected;
   if ((common_create_thread(&waitCoords, MEDIUM_HIGH, NULL, osdTest_wait_coords)))
   {
      logger.error("Error in creating %s OSD Coords wait",bdmType);
      goto osdTest_exit_task;  
   }

   if ((common_create_thread(&wait_connected, MEDIUM_HIGH, NULL, osdTest_wait_connected)))
   {
      logger.error("Error in creating %s Wait Connected",bdmType);
      goto osdTest_exit_task;  
   }
   
   // enter main command loop //

   logger.highlight("Entering %s OSD TEST Main Loop",bdmType);
   
   u8 count = 0;
   u32 c2 = 0;
   
   while (!common_system_state.stopping)
   {
      sleep(1);
      if (oldConnected != isBdmConnected)
      {
         count++;
         if (count < 5)
         {
            logger.info("Waiting to send Coords(%d)",count);
         }
         else
         {
            count = 0;
            oldConnected = isBdmConnected;
            send_coords();
         }
      }
      else
      {
         if (c2 && !(c2 % 5))
         {
            logger.debug("Waiting for Action(%d)",isBdmConnected);
         }
         if (c2 && !(c2 % 20) && isBdmConnected)
         {
            send_coords();
         }
         c2++;
      }
   } // end while not stopping
osdTest_exit_task:
   logger.highlight("Exiting %s osdTest(%d) - Close socket & wait for read thread to exit",bdmType, common_system_state.stopping);
   common_system_state.stopping = true;
   logger.highlight("---- Exiting GetSetD2 -----\n");
   return 0;
}

int main(int argc, char *argv[])
{
   if (argc > 2)
   {
      fprintf(stderr,"usage %s [optional argument] - if any argument is presented, then its a werewolf, otherwise, its D2>\n", argv[0]);
      exit(0);
   }
   
   if (argc == 2)
   {
      isWerewolf = true;
      bdmType   = "WW";
   }

   return (osdTest_task()); 
}

/**
 * @file bdmPrivate.h
 * @brief private include for utils directory
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __GET_SET_PRIVATE_H__
#define __GET_SET_PRIVATE_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgGetSetCommon.h"

/**
 * private/local variables
 */
extern common_state_t common_system_state;
extern int  getSetClientFd;
extern scd_bdm_port_status_t  localPortStatus;

void check_rx_sequence_setupBlob(u8 rxSeq, u8 cmdId);

int getStringStatus(const char *prompt, char *copyTo, u8 maxSize);
const char *cmdId2TxtWW(u8 cmdID);
void respond_set_request_client(u8 setStatus, u8 setClient);
void respond_get_request_client(u8 getStatus, u8 getClient);
u8 getNextTxGetSetSeq(void);

#endif // __GET_SET_PRIVATE_H__

/**
 * @file bdmUtils.c
 * @brief bdm utils
 * @details
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <ctype.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>

#include "zmq.h"
#include "dsc.h"

#include "commonTypes.h"
#include "commonState.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgSerial.h"
#include "msgIp.h"
#include "msgD2.h"
#include "msgGetSetD2.h"
#include "connUtils.h"
#include "osdTestPrivate.h"
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

u8 setClientType = 0;
extern pthread_mutex_t msgGetSetLock;


/**
 * @fn      u8 getNextTxGetSetSeq(void)
 * @brief   returns next txgetsetseq
 * @retval  n/a
 **************************************************************/
u8 nextTxGetSetSeq     = 1;
u8 getNextTxGetSetSeq(void)
{
   u8 nextNumTx = nextTxGetSetSeq;
   if (nextTxGetSetSeq == 255)
      nextTxGetSetSeq = 1;
   else
      nextTxGetSetSeq++;
   return nextNumTx;
}
/********************************************************
/////////////////////////////////////////////////////////
*** GET_SET UTILITIES 
/////////////////////////////////////////////////////////
********************************************************/
/**
 * @fn      void respond_set_request_client(u8 setStatus)
 * @brief   n/a
 * @retval  n/a
 **************************************************************/
void respond_set_request_client(u8 setStatus, u8 clientType)
{
   int status;
   ct_ip_msg_t txMsg;
   ctn_set_resp_cmd_t setResp;
        
   // build response 
   RESET_MSG_IP(&txMsg);
   txMsg.command_id          = BDM_CLIENT_SET_RESPONSE;
   txMsg.cmd_data_len        = SET_REQ_RESPONSE_SIZE;
   txMsg.cmd_data            = (u8 *)&setResp;
   txMsg.cmd_sequence_number = getNextTxGetSetSeq();
   setResp.clientDeviceType  = clientType;
   setResp.serverDeviceType  = DEVICE_TYPE_SHAVER;
   setResp.setStatus         = setStatus;
   if ((status = build_send_ip_msg(getSetClientFd,&txMsg, &msgGetSetLock)) != 0)
   {
      logger.error("Error (%d) for sending a SetDeviceResponse to GetSet client",status);
      return;
   }
}

/**
 * @fn      void respond_get_request_client(u8 setStatus)
 * @brief   n/a
 * @retval  n/a
 **************************************************************/
void respond_get_request_client(u8 getStatus, u8 clientType)
{
   int status;
   ct_ip_msg_t txMsg;
   ctn_get_req_response_t respData;
   
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = GET_SET_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = BDM_CLIENT_GET_RESPONSE;
   txMsg.cmd_data            = (u8 *)&respData;
   txMsg.cmd_data_len        = sizeof(respData);
   txMsg.cmd_sequence_number = getNextTxGetSetSeq();
   respData.clientDeviceType = clientType;
   respData.serverDeviceType = DEVICE_TYPE_SHAVER;
   respData.getStatus        = getStatus;
   if (getStatus == 0)
   {
      memcpy(&respData.getData, &localPortStatus, sizeof(localPortStatus));
   }

   // 
   if ((status = build_send_ip_msg(getSetClientFd,&txMsg, &msgGetSetLock)) != 0)
   {
      logger.error("Error (%d) for sending a GetRequestResponse to GetSet client",status);
      return;
   }

}

/**
 * @fn     void flushStdIn(void)
 * @brief  flushes stdIn for cli
 * @param  n/a
 ***********************************************************************/
static void flushStdIn(void)
{
   int c; 
   while (1)
   {
      c = getchar();
      if (c == '\n')
         return;
      else if (c == EOF)
         return;
   }
}


/***********
 * @fn      int wait_for_user(bool dummy_read, bool doNotDisplay)
 * @brief   waits for input
 * @param   [in] dummy_read   - read the input because we do NOT care about it
 * @param   [in] doNotDisplay - do NOT display periodic HB Status
 * @retval  0 == success, otherwise, error and stopping
 ****************************************************************************/
static int wait_for_user(bool dummy_read, bool doNotDisplay)
{
   u32 i = 0;
   struct pollfd ufds[1];      
   ufds[0].fd = 0;
   ufds[0].events = POLLIN;
   int rv;
     
   while(!common_system_state.stopping)
   {
      switch ((rv = poll(ufds,1,1000)))
      {
      case -1:
         logger.error("Pollin stdin error - %s",COMMON_ERR_STR);
         return -1;
         break;  // error

      case 0:
         break; // timeout 
	     
      default:
         if (dummy_read)
            flushStdIn();
         return 0;
         break; // got something //
      } // end switch //
	 
      // do display if appropriate //
      if (!(i++ % 20))
      {
         if (!doNotDisplay)
         {
            // reset these anyways //
            DBG("\nGetSet --->SELECT AN OPTION> ");
               
         } // if OK to display //
         FLUSH_SLEEP; 
      } // if time to do something or something change
   }   // end while 
     
   return -3;
}

#define ENTER2CONTINUE { DBGF("\n\t--- Hit Enter to Continue ---\n"); wait_for_user (true,false); }
/**
 * @fn      int privateGetString(const char *prompt, char *copyTo, u8 maxSize, bool doNotDisplay)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt      - prompt for the string 
 * @param   [out] copyTo      - location to copy string to
 * @param   [in/out] maxSize  - maxSize of copyTo 
 * @param   [in] doNotDisplay - display status while waiting
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
static int privateGetString(const char *prompt, char *copyTo, u8 maxSize, bool doNotDisplay)
{
   char *userInput = (char *)alloca(maxSize+1);
   char *result = NULL;
   u32 i;

tryPrivateStringAgain:

   // reset userInput
   reset_array(copyTo,maxSize);
   DBGF("\n%s\n >  ",prompt);

   if ((wait_for_user(false,doNotDisplay)))
      return -1;

   // read data in //
   if ((result = fgets(userInput, maxSize, stdin)) == NULL)
   {
      perror("fgets");
      DBGF("Failed to Read Input - Lets try that again\n");
      ENTER2CONTINUE;
      goto tryPrivateStringAgain;
   }

   // remove the '\n' and copy string over //
   char *fn = userInput;
   strsep(&fn,"\n");
   for (i = 0 ; i < strlen(userInput) ; i++)
   {
      int b = (int)userInput[i];
      if ((isalnum(b)))
         continue;
      else if ((userInput[i] == '_') ||(userInput[i] == '-'))
         continue;

      DBGF("Illegal Character [%d]<%c> in input <%s> - only Alpha Numeric Character (or '_'/'-') are accepted\n",i, b, userInput);
      ENTER2CONTINUE;
      goto tryPrivateStringAgain;
   }

   strncpy(copyTo,userInput,maxSize);
   return 0;
}

/**
 * @fn      int getStringStatus(const char *prompt, char *copyTo, u8 maxSize)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt     - prompt for the string 
 * @param   [out] copyTo     - location to copy string to
 * @param   [in/out] maxSize - maxSize of copyTo 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
int getStringStatus(const char *prompt, char *copyTo, u8 maxSize)
{
   return (privateGetString(prompt, copyTo, maxSize, false));
}




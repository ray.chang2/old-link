/**
 * @file getSetWW.c
 * @brief getset server for WW
 * @details
 *
 * @version 0.1.0
 * @author  Tom Morrison
 * @date    8/16/19
 *
 *
 *<pre>
 * 
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.00.01 tm   1/12/19   Initial Creation.
 * 00.01.00 tm   8/16/19   Release for code review
 *
 *</pre>
 *
 */
/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include <sys/types.h>
#include <linux/types.h>

#include "commonTypes.h"
#include "commonState.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgIp.h"
#include "msgGetSetWW.h"
#include "broker_api.h"
#include "WW_channels.h"

#include "dsc.h"

/**
 * static/private/local variables
 */
static const char* version = "01.00.00"; //follows major.minor.patch scheme(https://semver.org)
int  getSetClientFd      = -1;
u8   expectedCmdSequence = 1;

void *pHandlePub = NULL;
void *pHandleSub = NULL;

int doCli = 1;

/**
 * global variable(s)
 */
common_state_t common_system_state;

ct_exp_cmds_t bdmIpGetSetCmds[2] =
{
   {GET_SET_PROTOCOL_CLIENT_TO_BDM, CLIENT_BDM_GET_CMD,  IP_GET_DATA_REQUEST_SIZE},
   {GET_SET_PROTOCOL_CLIENT_TO_BDM, CLIENT_BDM_SET_CMD,  SET_REQ_DATA_SIZE_WW},
};

/////////////////////////////////
////////// local cache //////////
/////////////////////////////////
wcd_therapy_settings_t     localTherapySettings;
ctn_ww_get_response_data_t localGetResponseData;
u32 portStatusSize  = sizeof(localGetResponseData);
bool isWWConnected = false;

// local mutex
pthread_mutex_t msgGetSetLock;

/**
 * @fn      void init_local_data(void)
 * @brief   initializes local data
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void init_local_data(void)
{
   // reset both of these //
   bzero(&localGetResponseData, portStatusSize);
   if ((pHandlePub = bm_publisher()) == NULL)
   {
      logger.error("Failure to create publisher handler");
   }
   void *context = zmq_ctx_new();
   zmq_ctx_destroy(context);
}

/**
 * @fn      void respond_get_request_client(u8 setStatus)
 * @brief   sends get response to client
 * @retval  n/a
 **************************************************************/
void respond_get_request_client(u8 getStatus, u8 clientType)
 {
   int status;
   ct_ip_msg_t txMsg;
   ctn_ww_get_response_t respData;
   
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = GET_SET_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = BDM_CLIENT_GET_RESPONSE;
   txMsg.cmd_data            = (u8 *)&respData;
   txMsg.cmd_data_len        = sizeof(respData);
   txMsg.cmd_sequence_number = getNextTxGetSetSeq();
   
   respData.clientDeviceType = clientType;
   respData.serverDeviceType = COBLATION_DEVICE;
   respData.getStatus        = getStatus;
   if (getStatus == 0)
   {
      memcpy(&respData.getData, &localGetResponseData, sizeof(localGetResponseData));
   }

//   logger.highlight("WWBDM Sent GET Response (%d) with Sequence number (%d)", getStatus, txMsg.cmd_sequence_number);
   if ((status = build_send_ip_msg(getSetClientFd,&txMsg, &msgGetSetLock)) != 0)
   {
      logger.error("Error (%d) for sending a GetRequestResponse to GetSet client",status);
      return;
   }
}

/**
 * @fn      void respond_set_request_client(u8 setStatus)
 * @brief   n/a
 * @retval  n/a
 **************************************************************/
void respond_set_request_client(u8 setStatus, u8 clientDevice)
{
   int status;
   ct_ip_msg_t txMsg;
   ctn_set_resp_cmd_t setResp;

   RESET_MSG_IP(&txMsg);
   txMsg.cmd_data          = (u8 *)&setResp;
        
   // build response
   txMsg.protocol_id         = GET_SET_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = BDM_CLIENT_SET_RESPONSE;
   txMsg.cmd_data_len        = SET_REQ_RESPONSE_SIZE;
   txMsg.cmd_sequence_number = getNextTxGetSetSeq();

   setResp.clientDeviceType = clientDevice;
   setResp.serverDeviceType = COBLATION_DEVICE;
   setResp.setStatus        = setStatus;
   logger.highlight("WWBDM Sent SET Response (%d) with Sequence number (%d)", setStatus, txMsg.cmd_sequence_number);
   if ((status = build_send_ip_msg(getSetClientFd,&txMsg, &msgGetSetLock)) != 0)
   {
      logger.error("Error (%d) for sending a SetDeviceResponse to GetSet client",status);
      return;
   }
}

/**
 * @fn      void *getSet_ww_wait_portStatus(UNUSED_ATTRIBUTE void *args)
 * @brief   thread to wait for localPortStatus
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *getSet_ww_wait_portStatus(UNUSED_ATTRIBUTE void *args)
{
   u32 bytesRead;   
   void *portStatus = NULL;
   void *pHandleSubP          = NULL;
   u8 filter[BROKER_FILTER_MAX];
   
   // create a handle
   if ((pHandleSubP = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return NULL;
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSubP, WW_PORT_STATUS, strlen(WW_PORT_STATUS))))
   {
      logger.error("Failed to subscribe to WW Port Status");
      return NULL;
   }

   while (1)
   {
      int status;
      if ((status = bm_receive(pHandleSubP, filter, (void*) &portStatus, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive",status);
         return NULL;
      }
      assert(bytesRead >= portStatusSize);
      memcpy(&localGetResponseData, portStatus, portStatusSize);
      logger.debug("Got (%d) DevMode <%x> from BDM",bytesRead,localGetResponseData.dev_mode);
      free(portStatus);
   }
   
   return NULL;
}

/**
 * @fn      void *getSet_ww_wait_connected(UNUSED_ATTRIBUTE void *args)
 * @brief   thread to wait ww connected status
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *getSet_ww_wait_connected(UNUSED_ATTRIBUTE void *args)
{
   u32 bytesRead;
   void *pHandleSubC          = NULL;
   u8 filter[BROKER_FILTER_MAX];
   
   // create a handle
   if ((pHandleSubC = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return NULL;
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSubC, WW_CONNECTED, strlen(WW_CONNECTED))))
   {
      logger.error("Failed to subscribe to WW Port Status");
      return NULL;
   }

   u8 *pConn;
   while (1)
   {
      int status;
      if ((status = bm_receive(pHandleSubC, filter, (void*) &pConn, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive",status);
         return NULL;
      }
      isWWConnected = *pConn;
      logger.highlight("WW Connected State  <%d>",isWWConnected);
      free(pConn);
   }
   
   return NULL;
}

/**
 * @fn      void dispatch_get_set_msg(int *fd, pIpMsg rxMsg)
 * @brief   parses incoming commands and responds accordingly
 * @param   [in] fd    - ptr to fd to send responses
 * @param   [in] rxMsg - ptr to full received message
 * @retval  n/a
 **************************************************************/
void dispatch_get_set_msg(pIpMsg rxMsg)
{
   int status = 0;
   if (rxMsg->command_id == CLIENT_BDM_GET_CMD)
   {
      pGetReq pGetCmd           = (pGetReq)rxMsg->cmd_data;
      
      // validate that the get is for us //
      if (pGetCmd->serverDeviceType != COBLATION_DEVICE)
      {
         logger.error("Set WW Device Type <%d>  Expected<%d>",pGetCmd->serverDeviceType, COBLATION_DEVICE);
         status = 1;
      }
      
      respond_get_request_client(status,pGetCmd->clientDeviceType);
   } // get cmd //
   else if (rxMsg->command_id == CLIENT_BDM_SET_CMD)
   {
      // get details of what to set add to pending
      pSetReq pReq                          = (pSetReq)rxMsg->cmd_data;
      localTherapySettings.ab_coag_18_sp    = pReq->ab_coag_18_sp;
      localTherapySettings.ab_coag_smart_sp = pReq->ab_coag_smart_sp;
      localTherapySettings.gen_set          = pReq->gen_set;
      localTherapySettings.amb_set          = pReq->amb_set;
      localTherapySettings.therapy_settings = pReq->therapy_settings;
      localTherapySettings.amb_thres_sp     = pReq->amb_thres_sp;
      
      // deal with it
      logger.highlight("Set Therapy Settings Cmd <%02X>",pReq->therapy_settings);
      
      if ((bm_send(pHandlePub, (u8 *)WW_SET_CMD, &localTherapySettings, sizeof(localTherapySettings))) < 0)
      {
         logger.error("Failed to publish therapy settings to BDM");
         status = 1;
      }
      
      respond_set_request_client(status, pReq->clientDeviceType);
      
   } // set cmd //
   else
   {
      logger.error("Unexpected Cmd <%02X> Received",rxMsg->command_id);
   }
}

/**
 * @fn      void *bdm_getSet_server(void *args)
 * @brief   receives connects/messages from GetSet Clients
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *bdm_getSet_server(UNUSED_ATTRIBUTE void *args)
{
   ct_ip_msg_t rxMsg;
   bool continueClientConnection;
   int status;
    
   pthread_mutex_init(&msgGetSetLock,NULL);

   int getSetServerFd = create_tcp_server_socket(GET_SET_PROTOCOL_SOCKET);
   if (getSetServerFd <= 0)
   {
      logger.error("couldn't create getSet Server Socket");
      return NULL;
   }
    
   logger.debug("Entering BDM GET/SET Server(%d)",isWWConnected);
   while (1)
   {
      if (isWWConnected)
      {
         logger.debug("Start accepting next connection from get/set client");
            
         // accept next command 
         getSetClientFd = accept_tcp_socket_connection(getSetServerFd);
         if (getSetClientFd < 0)
         {
            common_close_socket(&getSetServerFd);
            return NULL;
         }

         logger.debug("New GetSet Connection <%d>",getSetClientFd);
         // process commands while connected
         continueClientConnection = true;
         while (continueClientConnection)
         {
            // wait for next message
            if (isConnectionValid(getSetClientFd) && (isWWConnected))
            {
                    
               if ((status = read_ip_msg(&getSetClientFd, bdmIpGetSetCmds, 2, &rxMsg)))
               {
                  logger.error("Error (%d)in read msg - disconnect client",status);
                  common_close_socket(&getSetClientFd);
                  continueClientConnection = false;
               } // error reading message from server
               else
               {
                  dispatch_get_set_msg(&rxMsg);
                  freeIpMsgContainerData(&rxMsg);
               } // process get message
            }
            else
            {
               logger.error("Closing Client Connection (%d) State(%d)",getSetClientFd,common_system_state.state);
               common_close_socket(&getSetClientFd);
               continueClientConnection = false;
            } // if closing or the state went back to Discovery process //

         } // while valid connection
      } // if scd is connected
      else
      {
         YIELD_250MS;
      }
   } // while contining 
   return NULL;
}


/**
 * @fn      int getSetWW_task(void)
 * @brief   getSet Task  for WW Set/Get Clients
 * @retval  returns status
 */
int getSetWW_task(void)
{
   // init common signal handler
   init_common_signal_handler();
     
   // reset structs */
   bzero(&common_system_state,sizeof(common_system_state));

   init_local_data();
     
   pthread_t getSetThread;
   if ((common_create_thread(&getSetThread, MEDIUM_HIGH, NULL, bdm_getSet_server)))
   {
      logger.error("Error in creating Get Set Server Thread");
      goto bdm_exit_task;  
   }

   pthread_t waitPortStatus;
   if ((common_create_thread(&waitPortStatus, MEDIUM_HIGH, NULL, getSet_ww_wait_portStatus)))
   {
      logger.error("Error in creating Waiting for port status");
      goto bdm_exit_task;  
   }

   pthread_t wait_connected;
   if ((common_create_thread(&wait_connected, MEDIUM_HIGH, NULL, getSet_ww_wait_connected)))
   {
      logger.error("Error in creating Waiting for connected status");
      goto bdm_exit_task;  
   }

   // enter main command loop //
   if (doCli == 0)
   {
      logger.highlight("NO CLI Interface");
   }

   // do not exit until killed or ctrl-c
   while (1)
   {
      sleep(1);
   }
       
bdm_exit_task:
   logger.highlight("Exiting getsetd2(%d) - Close socket & wait for read thread to exit",common_system_state.stopping);
   common_system_state.stopping = true;
   logger.highlight("---- Exiting GetSetD2 -----\n");
   exit(0);
   return 0;
}

int main(int argc, char *argv[])
{
   if (argc > 1 && !strcmp(argv[1],"-V")) {
       printf("%s\n", version);
       return 0;
   }

   if (argc > 2)
   {
      fprintf(stderr,"usage %s<(optional) 'Do CLI' (0/1(cli is enabled by default))>\n", argv[0]);
      exit(0);
   }
   
   if (argc == 2)
   {
      doCli = atoi(argv[1]);      
   }

   return (getSetWW_task()); 
}

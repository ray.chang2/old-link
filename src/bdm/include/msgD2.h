/**
 * @file msgD2.h
 * @brief facilities to send/receive bdm/scd messages
 * @details This file contains the following:
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_D2_H__
#define __MSG_D2_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgIp.h"

//////////////////////////////////
// prototocol definitions
//////////////////////////////////
#define PROTOCOL_BDM_TO_SCD        0x35
#define PROTOCOL_SCD_TO_BDM        0x53

typedef enum {
   // chapter 5
   CAPDEV_D2_GENERIC_REPLY = 0x30,
  
   // chapter 6
   CAPDEV_D2_DISCOVERY,

   // Chapter 7
   CAPDEV_D2_HEARTBEAT,

   // chapter 8
   CAPDEV_D2_PORT_STATUS,
   CAPDEV_D2_GET_PORT_STATUS,

   // Chapter 9
   CAPDEV_D2_SET_DEV_INFO,

   // Chapter 10
   CAPDEV_D2_LAVAGE_TOGGLE_EVENT,

   // chapter 11
   CAPDEV_D2_SCD_CMD_REQUEST,

   // Chapter 12/13
   CAPDEV_D2_GET_CONFIG_BLOB,
   CAPDEV_D2_SET_CONFIG_BLOB,

   // chapter 15
   CAPDEV_DEV_SERIAL_NUMBER,
} CAPDEV_D2_CMD_TYPES;


/******************************
///////////////////////////////
/// generic NACK
///////////////////////////////
*******************************/
#define BDM_TO_SCD_GENERIC_RESPONSE CAPDEV_GENERIC_REPLY
#define SCD_TO_BDM_GENERIC_RESPONSE CAPDEV_GENERIC_REPLY

/******************************
///////////////////////////////
/// Discovery
///////////////////////////////
*******************************/
#define BDM_TO_SCD_DISCOVERY_REQUEST  CAPDEV_D2_DISCOVERY
#define SCD_TO_BDM_DISCOVERY_RESPONSE CAPDEV_D2_DISCOVERY

/**********************************
///////////////////////////////////
* HEARTBEAT MESSAGES
///////////////////////////////////
***********************************/
#define BDM_TO_SCD_HEARTBEAT_CMD   CAPDEV_D2_HEARTBEAT
#define SCD_TO_BDM_HEARTBEAT_CMD   CAPDEV_D2_HEARTBEAT 

/***************
 * BDM TO SCD 
 ***************/
#define VIS_DEV_READY_MASK    0x01
#define RES_DEV_READY_MASK    0x02
#define FLUID1_DEV_READY_MASK 0x04
#define FLUID2_DEV_READY_MASK 0x08
#define COLB_DEV_READY_MASK   0x10
typedef struct hb_bdm_scd {
   u8  pump_device_state;  // 0 == OFF, 0x11 is ON
} __attribute__ ((__packed__))
  hb_bdm_scd_t, *pHbBdmScd;

#define HB_BDM_SCD_DATA_LENGTH (sizeof(hb_bdm_scd_t)) // 3

/***************
 * SCD to BDM
 ***************/
#define HB_DEVICE_READY_BIT_MASK     0x01

typedef struct hb_scd_bdm {
   u8 devStatus;
} __attribute__ ((__packed__))
  hb_scd_bdm_t, *pHbScd2Bdm;

#define HB_SCD_BDM_DATA_LENGTH (sizeof(hb_scd_bdm_t)) // 1

////////////////////////////////////////////
//////////////// Timeout ///////////////////
////////////////////////////////////////////

/**********************************
///////////////////////////////////
* PORT_ STATUS(CHANGE - SCD to BDM)
///////////////////////////////////
***********************************/
#define SCD_TO_BDM_PORT_STATUS          CAPDEV_D2_PORT_STATUS
#define BDM_TO_SCD_PORT_STATUS_RESP     CAPDEV_D2_PORT_STATUS    
#define BDM_TO_SCD_GET_PORT_STATUS      CAPDEV_D2_GET_PORT_STATUS
#define SCD_TO_BDM_GET_PORT_STATUS_RESP CAPDEV_D2_PORT_STATUS
#define PORT_DISPLAY_UNITS_MASK     0x03
#define PORT_DISPLAY_BLADE_MASK     0x0C
#define PORT_DISPLAY_BLADE_SHIFT    (2)
#define PORT_DISPLAY_MODE_MASK      0x30
#define PORT_DISPLAY_MODE_SHIFT     (4)
#define PORT_DISPLAY_UP_ARROW_MASK  0x40
#define PORT_DISPLAY_DN_ARROW_MASK  0x80

#define PORT_SPEED_MASK             0x7C
#define PORT_RUNNING_MASK           0x80

typedef enum {
   NO_RRROR,
   TEMP_FAIL,
   UNKNOWN_BLADE,
   UNKNOWN_HANDPIECE,
   HANDPIECE_SENSOR_FAULT,
   BLADE_STALL,
   HANDPIECE_STALL,
   HANDPIECE_MOTOR_FAULT,
   SHORT_CIRCUIT_DETECT,
   HANDPIECE_OVERLOAD,
   HANDPIECE_OVERLOAD_TIMEOUT,
   HANDPIECE_ERROR,
   UNKNOWN_FOOTSWITCH,
   FOOTSWITCH_ERROR,
   FOOTSWITCH_BATTERY_LOW,
   FOOTSWITCH_REQUIRED,
} PORT_WARN_ERRORS;

#define PORTA_ERROR_WARN_MASK   (0x0F)
#define PORTB_ERROR_WARN_MASK   (0xF0)
#define PORTB_ERROR_WARN_SHIFT  (4)

#define CAPDEV_D2_POPUPS_MASK        (0x07)
#define HANDPIECE_OVERRIDE_MASK   (0x08)
#define BLADE_RECALL_MASK         (0x10)
#define PUMP_PORTB_NOT_PORTA_MASK (0x20)
#define FOOTSWITCH_PORT_MASK      (0x40)
#define IN_SETTINGS_SCREEN_MASK   (0x80)

typedef struct scd_bdm_port_status
{
   u8 porta_display;
   u8 porta_speed_run;
   u8 portb_display;
   u8 portb_speed_run;
   u8 port_ab_err_warn;
   u8 settings_popups;
} __attribute__ ((__packed__))
  scd_bdm_port_status_t, *pPortStatus;

#define SCD_PORT_STATUS_DATA_LEN             (sizeof(scd_bdm_port_status_t))
#define BDM_TO_SCD_PORT_STATUS_RESP_DATA_LEN (1) // simple ack/nack byte
#define BDM_TO_SCD_GET_PORT_STATUS_DATA_LEN  (0)

/**********************************
///////////////////////////////////
* PORT_ STATUS(CHANGE - SCD to BDM)
///////////////////////////////////
***********************************/
#define SCD_TO_BDM_SERIAL_NUMBER_STATUS     CAPDEV_DEV_SERIAL_NUMBER
#define BDM_TO_SCD_SERIAL_NUMBER_RESPONSE   CAPDEV_DEV_SERIAL_NUMBER
#define NUM_SERIAL_NUMBER                   (11)

typedef enum {
   SHAVER_SERIAL_TYPE = 0,
   PORTA_SERIAL_TYPE,
   PORTB_SERIAL_TYPE,
   MAX_SERIAL_TYPE,
} SERIAL_NUM_TYPES;

typedef struct scd_bdm_serial_number
{
   u8 serial_number[NUM_SERIAL_NUMBER];
   u8 serialType; 
} __attribute__ ((__packed__))
  scd_bdm_serial_number_t, *pSerNum;

#define SCD_SERIAL_NUMBER_DATA_LEN      (sizeof(scd_bdm_serial_number_t))
#define BDM_SERIAL_NUMBER_DATA_LEN_RESP (1) //simple ack/nack byte

/**********************************
///////////////////////////////////
* Set/Get Dev Info
///////////////////////////////////
***********************************/
#define BDM_TO_SCD_SET_DEVICE_INFO_REQUEST  CAPDEV_D2_SET_DEV_INFO
#define SCD_TO_BDM_SET_DEVICE_INFO_RESPONSE CAPDEV_D2_SET_DEV_INFO

typedef struct set_scd_device_info
{
   u8  hand_blade_pump_foot;
} __attribute__ ((__packed__))
  set_scd_device_info_t, *pSetScdDevInfo;

#define SET_DEVICE_INFO_DATA_LEN      (sizeof(set_scd_device_info_t))
#define SET_DEVICE_INFO_DATA_LEN_RESP (1) // ack/nack

/**********************************
///////////////////////////////////
* trigger out of SCD and ACK
///////////////////////////////////
***********************************/
#define SCD_TO_BDM_TRIGGER_OUT_EVENT      CAPDEV_D2_LAVAGE_TOGGLE_EVENT
#define BDM_TO_SCD_TRIGGER_OUT_EVENT_RESP CAPDEV_D2_LAVAGE_TOGGLE_EVENT

typedef struct scd_bdm_trigger_out_event
{
   u8 lavage_toggle;
} __attribute__ ((__packed__))
  scd_bdm_trigger_out_event_t, *pScdTrigIn;

// data lengths
#define SCD_TO_BDM_TRIGGER_OUT_LENGTH (sizeof(scd_bdm_trigger_out_event_t))


/**********************************
///////////////////////////////////
* SCD Command requests
///////////////////////////////////
***********************************/
#define BDM_TO_SCD_SCD_CMD_REQ   CAPDEV_D2_SCD_CMD_REQUEST
#define SCD_TO_BDM_SCD_CMD_RESP  CAPDEV_D2_SCD_CMD_REQUEST

typedef enum {
   NO_SCD_COMMAND,
   PORTA_UP,
   PORTA_DOWN,
   PORTA_DELTA_OSC_MODE,
   PORTB_UP,
   PORTB_DOWN,
   PORTB_DELTA_OSC_MODE,
   OK_BUTTON,
   EXIT_SETTINGS,
   MAX_SCD_COMMAND,
} SCD_COMMAND_TYPES;

#define BDM_TO_SCD_SCD_CMD_LEN          1 // 1 byte 
#define SCD_TO_BDM_SCD_CMD_RESP_LEN     1 // ack/nack

/**********************************
///////////////////////////////////
* Blob stuff
///////////////////////////////////
***********************************/
#define BDM_TO_SCD_GET_BLOB_CONFIG_REQUEST  CAPDEV_D2_GET_CONFIG_BLOB
#define SCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE CAPDEV_D2_GET_CONFIG_BLOB // data blob

#define BDM_TO_SCD_SET_BLOB_CONFIG_REQUEST  CAPDEV_D2_SET_CONFIG_BLOB // data blob
#define SCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE CAPDEV_D2_SET_CONFIG_BLOB // ack


#endif // __MSG_D2_H__

/**
 * @file msgGetSetWW.h
 * @brief WW specific get/set protocol elements
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_GETSET_WW_H__
#define __MSG_GETSET_WW_H__
#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgGetSetCommon.h"
#include "msgWW.h"

/*
 * get response structure
 */

typedef struct ctn_ww_get_response_data {
   u8 wand_info;               // from port status;
   u8 legacy_therapy_max;      // from port status;
   u8 dev_mode;                // from port status;
   u8 ab_coag_smart_sp;        // from HB
   u8 ab_coag_18_sp;           // from HB
   u8 therapy_set;             // from HB
   u8 amb_thres_sp;            // from HB
   u8 gen_set;                 // from HB
   u8 amb_set;                 // from HB
   u8 amb_temp;                // from HB
   u8 leg_amb_set;             // from HB
   u8 leg_amb_temp;            // from HB
   u8 error_priority_code;     // from HB
   u8 foot_info;               // from port status
   u8 wand_name[12];           // from port status
} ctn_ww_get_response_data_t, *pGetWW;

/*
 * Get Data Response Message data
 */
typedef struct ctn_ww_get_response {
   u8  clientDeviceType;
   u8  serverDeviceType;
   u8  getStatus;                  // ack/nack
   ctn_ww_get_response_data_t getData;
} __attribute__ ((__packed__))
  ctn_ww_get_response_t, *pWWGetResp;

#define GET_DATA_RESPONSE_SIZE  (sizeof(ctn_ww_get_response_t))

typedef struct ctn_ww_set_cmd {
   u8 clientDeviceType;
   u8 serverDeviceType;
   u8 ab_coag_smart_sp;
   u8 ab_coag_18_sp;
   u8 therapy_settings;
   u8 amb_thres_sp;
   u8 amb_set;
   u8 gen_set;
} __attribute__ ((__packed__))
  ctn_ww_set_cmd_t, *pSetReq;

#define SET_REQ_DATA_SIZE_WW      (sizeof(ctn_ww_set_cmd_t))


#endif // __MESSAGE_H__

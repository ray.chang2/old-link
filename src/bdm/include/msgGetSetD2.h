/**
 * @file msgGetSetD2.h
 * @brief d2 specific get/set protocol elements
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_GETSET_D2_H__
#define __MSG_GETSET_D2_H__
#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgGetSetCommon.h"
#include "msgD2.h"

/*
 * Get Data Response Message data
 */
typedef struct ctn_get_req_response {
   u8 clientDeviceType;
   u8 serverDeviceType;
   u8 getStatus;                  // ack/nack
   scd_bdm_port_status_t getData; // from scd to bdm 
} __attribute__ ((__packed__))
  ctn_get_req_response_t, *pGetResp;

#define GET_DATA_RESPONSE_SIZE  (sizeof(ctn_get_req_response_t))

typedef struct ctn_set_req_cmd {
   u8 clientDeviceType;
   u8 serverDeviceType;
   u8 hand_blade_pump_foot;
   u8 scd_cmd_request;
   //  u8 scd_command;
} __attribute__ ((__packed__))
  ctn_set_req_cmd_t, *pSetReq;

#define SET_REQ_DATA_SIZE_D2       (sizeof(ctn_set_req_cmd_t))


#endif // __MESSAGE_H__

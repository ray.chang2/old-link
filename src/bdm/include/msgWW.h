
/**
 * @file msgWW.h
 * @brief contains all Werewolf specific serial protocol messages
 *
 * @version .01
 * @author  Tom Morrison
 * @date 1/15/19
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   1/15/19   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_WW_H__
#define __MSG_WW_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgIp.h"
#include "msgSerial.h"

//////////////////////////////////
// prototocol definitions
//////////////////////////////////
#define PROTOCOL_BDM_TO_WCD        0x36
#define PROTOCOL_WCD_TO_BDM        0x63

typedef enum {
   // chapter 5
   CAPDEV_WW_GENERIC_REPLY = 0x30,
  
   // chapter 6
   CAPDEV_WW_DISCOVERY,

   // Chapter 7
   CAPDEV_WW_HEARTBEAT,

   // chapter 8
   CAPDEV_WW_PORT_STATUS,
   CAPDEV_WW_GET_PORT_STATUS,

   // Chapter 9
   CAPDEV_WW_SET_THERAPY_SETTINGS,

   // Chapter 10
   CAPDEV_WW_THERAPY_SETTINGS_STATUS,
   CAPDEV_WW_GET_THERAPY_SETTINGS_STATUS,

   // chapter 11
   CAPDEV_WW_GET_ERROR_INFO,

   // chapter 11
   CAPDEV_WW_GET_AMBIENT_SETTINGS,

   // chapter 13
   CAPDEV_WW_GET_WAND_LIFE_INFO,

   // chapter 14
   CAPDEV_WW_GET_GENERATOR_SERIAL_INFO,
   
// Chapter 15/16
   CAPDEV_WW_GET_CONFIG_BLOB,
   
   // of one of the blobs
   CAPDEV_WW_SET_CONFIG_BLOB,

} CAPDEV_WW_CMD_TYPES;


#define BDM_TO_WCD_GENERIC_RESPONSE CAPDEV_WW_GENERIC_REPLY
#define WCD_TO_BDM_GENERIC_RESPONSE CAPDEV_WW_GENERIC_REPLY

/******************************
///////////////////////////////
/// Discovery
///////////////////////////////
*******************************/
#define BDM_TO_WCD_DISCOVERY_REQUEST  CAPDEV_WW_DISCOVERY
#define WCD_TO_BDM_DISCOVERY_RESPONSE CAPDEV_WW_DISCOVERY

/**********************************
///////////////////////////////////
* HEARTBEAT MESSAGES
///////////////////////////////////
***********************************/
#define BDM_TO_WCD_HEARTBEAT_CMD   CAPDEV_WW_HEARTBEAT
#define WCD_TO_BDM_HEARTBEAT_CMD   CAPDEV_WW_HEARTBEAT 

/***************
 * BDM TO WCD 
 ***************/
typedef struct hb_bdm_wcd {
   u8  capital_device_ready;  // 0 == OFF, 0x11 is ON
} __attribute__ ((__packed__))
  hb_bdm_wcd_t, *pHbBdmWcd;

#define HB_BDM_WCD_DATA_LENGTH (sizeof(hb_bdm_wcd_t)) // 1

/***************
 * WCD to BDM
 ***************/
#define HB_DEVICE_READY_BIT_MASK     0x01

typedef struct hb_wcd_bdm
{
   u8 devStatus;
   u8 wandInfo;
   u8 ab_coag_smart_sp;
   u8 ab_coag_18_sp;
   u8 therapy_set;
   u8 amb_thres_sp;
   u8 gen_set;
   u8 amb_set;
   u8 amb_temp;
   u8 leg_amb_set;
   u8 leg_amb_temp;
   u8 error_priority_code;
} __attribute__ ((__packed__))
  hb_wcd_bdm_t, *pHbWcdBdm;

#define HB_WCD_BDM_DATA_LENGTH (sizeof(hb_wcd_bdm_t)) 

///////////////////////////
////// Port Status ////////
///////////////////////////
#define WCD_TO_BDM_PORT_STATUS          CAPDEV_WW_PORT_STATUS
#define BDM_TO_WCD_PORT_STATUS_RESP     CAPDEV_WW_PORT_STATUS    
#define BDM_TO_WCD_GET_PORT_STATUS      CAPDEV_WW_GET_PORT_STATUS
#define WCD_TO_BDM_GET_PORT_STATUS_RESP CAPDEV_WW_PORT_STATUS
typedef struct wcd_bdm_port_status
{
   u8  foot_info;
   u8  wand_info;
   u8  legacy_therapy_max;
   u8  dev_mode;
   u8  language_id;
   u8  wand_cap_info;
   u8  wand_name[12];
   u8  wand_cat_num[12];
   u32 wand_lot_num;
   u16 wand_id_num;   
} __attribute__ ((__packed__))
  wcd_bdm_port_status_t, *pWcdPortStat;

#define WCD_PORT_STATUS_DATA_LEN             (sizeof(wcd_bdm_port_status_t))
#define BDM_TO_WCD_PORT_STATUS_RESP_DATA_LEN (1) // simple ack/nack byte
#define BDM_TO_WCD_GET_PORT_STATUS_DATA_LEN  (0)

///////////////////////////////////////////////////////////////////
/////////// this is for both set/get therapy settings /////////////
///////////////////////////////////////////////////////////////////
#define BDM_TO_WCD_SET_THERAPY_SETTINGS    CAPDEV_WW_SET_THERAPY_SETTINGS
#define WCD_TO_BDM_SET_THERAPY_RESPONSE    CAPDEV_WW_SET_THERAPY_SETTINGS

#define WCD_TO_BDM_THERAPY_SETTINGS_STATUS CAPDEV_WW_THERAPY_SETTINGS_STATUS
#define BDM_TO_WCD_THERAPY_STATUS_RESPONSE CAPDEV_WW_THERAPY_SETTINGS_STATUS
#define BDM_TO_WCD_GET_THERAPY_SETTINGS    CAPDEV_WW_GET_THERAPY_SETTINGS_STATUS
#define WCD_TO_BDM_GET_THERAPY_RESPONSE    CAPDEV_WW_THERAPY_SETTINGS_STATUS

typedef struct wcd_therapy_settings
{
   u8 ab_coag_smart_sp;
   u8 ab_coag_18_sp;
   u8 therapy_settings;
   u8 amb_thres_sp;
   u8 pump_settings;
   u8 amb_set;
   u8 gen_set;
   u8 reflex_timer;
} __attribute__ ((__packed__))
  wcd_therapy_settings_t, *pTherapySettings;

#define WCD_THERAPY_SETTINGS_DATA_LEN (sizeof(wcd_therapy_settings_t))

///////////////////////////////////////////////////////////////////
////////////////// ERROR NOTIFICATION RESPONSE ///////////////////
///////////////////////////////////////////////////////////////////
#define BDM_TO_WCD_GET_ERROR_INFO      CAPDEV_WW_GET_ERROR_INFO
#define WCD_TO_BDM_GET_ERROR_INFO_RESP CAPDEV_WW_GET_ERROR_INFO

typedef struct error_notification
{
   u8 errors_priority;
   u8 misc_errors;
} __attribute__ ((__packed__))
  error_notification_t, *pErrorNot;
#define WCD_ERROR_STATUS_DATA_LEN (sizeof(error_notification_t))

///////////////////////////////////////////////////////////////////
////////////////// AMBIENT SETTINGS ///////////////////
///////////////////////////////////////////////////////////////////
#define BDM_TO_WCD_GET_AMBIENT_SETTINGS      CAPDEV_WW_GET_AMBIENT_SETTINGS
#define WCD_TO_BDM_GET_AMBIENT_SETTINGS_RESP CAPDEV_WW_GET_AMBIENT_SETTINGS

typedef struct ambient_settings
{
   u8 amb_set;
   u8 amb_temp;
   u8 handle_temp;
   u8 amb_thresh_sp;
   u8 leg_amb_set;
   u8 leg_amb_temp;
} __attribute__ ((__packed__))
  ambient_settings_t, *pAmbSet;

#define WCD_AMBIENT_STATUS_DATA_LEN (sizeof(ambient_settings_t))

///////////////////////////////////////////////////////////////////
////////////////// WAND INFO SETTINGS ///////////////////
///////////////////////////////////////////////////////////////////
#define BDM_TO_WCD_GET_WAND_LIFE_INFO      CAPDEV_WW_GET_WAND_LIFE_INFO
#define WCD_TO_BDM_GET_WAND_LIFE_INFO_RESP CAPDEV_WW_GET_WAND_LIFE_INFO
typedef struct wand_life_info
{
   u16 wand_life_low;
   u16 wand_life_med;
   u16 wand_life_high;
   u16 wand_life_vac;
   u16 wand_life_coag;
} __attribute__ ((__packed__))
  wand_life_info_t, *pWandInfo;

#define WAND_LIFE_INFO_DATA_LEN (sizeof(wand_life_info_t))

///////////////////////////////////////////////////////////////////
////////////////// GENERATOR SETTINGS ///////////////////
///////////////////////////////////////////////////////////////////
#define BDM_TO_WCD_GET_GENERATOR_INFO      CAPDEV_WW_GET_GENERATOR_SERIAL_INFO
#define WCD_TO_BDM_GET_GENERATOR_INFO_RESP CAPDEV_WW_GET_GENERATOR_SERIAL_INFO
#define BDM_TO_WCD_GENERATOR_INFO_RESP     CAPDEV_WW_GET_GENERATOR_SERIAL_INFO

typedef struct generator_info
{
   u8 serialNum[16];
   u8 born_on_date[4];
   u8 app_version_major;
   u8 app_version_minor;
   u8 gui_version_major;
   u8 gui_version_minor;
} __attribute__ ((__packed__))
  generator_info_t, *pGenInfo;

#define GENERATOR_SERIAL_INFO_DATA_LEN (sizeof(generator_info_t))
/**********************************
///////////////////////////////////
* Blob INFO stuff
///////////////////////////////////
***********************************/

#define BDM_TO_WCD_GET_BLOB_CONFIG_REQUEST  CAPDEV_WW_GET_CONFIG_BLOB
#define WCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE CAPDEV_WW_GET_CONFIG_BLOB // data blob

#define BDM_TO_WCD_SET_BLOB_CONFIG_REQUEST  CAPDEV_WW_SET_CONFIG_BLOB // data blob
#define WCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE CAPDEV_WW_SET_CONFIG_BLOB // ack

#define WW_BLOB_DATA_SIZE       (1391)
#define WW_TOTAL_BLOB_DATA_SIZE (WW_BLOB_DATA_SIZE + 1)
#define WW_BLOB_IP_PACKET_SIZE  (WW_TOTAL_BLOB_DATA_SIZE + BLOB_DATA_HEADER_SIZE)
#define GET_SETUP_BLOB_RESPONSE_SIZE (WW_BLOB_IP_PACKET_SIZE)

#endif // __MSG_WW_H__

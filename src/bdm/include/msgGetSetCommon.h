/**
 * @file msgGetSetCommon.h
 * @brief common msgGetSet information
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_GETSET_COMMON_H__
#define __MSG_GETSET_COMMON_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#define GET_SET_PROTOCOL_SOCKET 60000

typedef enum {
   // section 4.1
   CLIENT_BDM_GET_CMD = 1,
   CLIENT_BDM_SET_CMD = 2,

   // section 4.2 
   BDM_CLIENT_GET_RESPONSE = 0x11,
   BDM_CLIENT_SET_RESPONSE = 0x12,

} GET_SET_CMD_TYPES;

#define NUM_IP_GET_SET_CMDS 2

/*
 * set data resp
 */
typedef struct ctn_set_resp_cmd {
   u8 clientDeviceType;
   u8 serverDeviceType;
   u8 setStatus;
} __attribute__ ((__packed__))
  ctn_set_resp_cmd_t, *pSetResp;

#define SET_REQ_RESPONSE_SIZE (sizeof(ctn_set_resp_cmd_t))

///////////////////////////////////////////////////////////////
#endif // __MESSAGE_GETSET_COMMONH__

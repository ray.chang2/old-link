/**
 * @file commonSerialUtils.c
 * @brief Serial Utils
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 5/25/13
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   5/25/13   Initial Creation.
 *
 *</pre>
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>
#include "commonTypes.h"
#include "commonState.h"
#include "commonLog.h"

#include "connUtils.h"
#include "msgD2.h"
#include "commSerUtils.h"

/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/
/**
 * @fn      int init_serial_connection(int  *serial_fd)
 * @brief   initializes serial port for sending/receiving data
 * @param   [in] serial_fd - pointer to serial fd for serial port 
 * @retval  0 == good initialization, otherwise, error code
 ****************************************************************************/
extern char *comport;  /**< located in ucm.c */
int init_serial_connection(int *serial_fd)
{
   SYS_INFO("Initializing Serial connection on ComPort (%s)",comport);

   // open serial port //
   if ((*serial_fd = open (comport, O_RDWR | O_NOCTTY | O_SYNC)) <= 0)
   {
      SYS_ERR("Serial Init (%s) Error(%d): %s", comport, *serial_fd, COMMON_ERR_STR);
      return -1;
   }

   // fflush the pipe
   fsync(*serial_fd);
   fflush(NULL);

   // setup serial port signals 
   struct termios tty;
   memset (&tty, 0, sizeof tty);
   tty.c_cflag     = CS8|CREAD|CLOCAL;
   tty.c_cc[VMIN]  = 1;            // read doesn't block
   tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

   // set input/output speed & bitsize //
   cfsetospeed (&tty, B115200);
   cfsetispeed (&tty, B115200);
    
   // write configuration into this port's configuration
   if ((tcsetattr (*serial_fd, TCSANOW, &tty) != 0))
   {
      SYS_CRIT("tcsetattr: <%s>", COMMON_ERR_STR);
      return -2;
   }
   // DBGF("\t...Good Init <%s> @ <%04X>....\n",comport, B115200);

   return 0;
}

/**
 * @fn      void *serial_rx(void *args)
 * @brief   RCM Serial Rx Thread for receiving data from serial connection
 * @param   [in] args - pointer to serialRxArgs structure containing serial_fd & dispatch routine
 * @retval  NULL (exits when told to)
 ****************************************************************************/
void *serial_rx(void *args)
{
   int                  status = 0;
   pSerialRxArgs         pSerial       = (pSerialRxArgs)args;
   int                  *serialPortFd  = pSerial->serial_fd;
   dispatch_function_t2  processfn     = pSerial->serialProcessData;

   // entering to read //
   while(!common_system_state.stopping)
   {
      pMsgScd rxMsg;
      if ((status = read_serial_msg(serialPortFd, &rxMsg, pSerial->expectedProtocol, pSerial->pCmds, pSerial->numCmds)))
      {
         SYS_WARN("---> RX MSG build Status <%d>(%d)",status,common_system_state.stopping);
         if (status > 0)
            continue;
         else
            goto  serial_exit_serial_in; 
      }
      else
      {
         int status;
         if ((status = processfn(rxMsg)))
         {
            SYS_ERR("Process Serial Error(%d)",status);
            // goto  serial_exit_serial_in; 
         }
      }
      freeMsgContainerAll(&rxMsg);
   } // end while !usb stopping

serial_exit_serial_in:
   SYS_NOTE("Exiting Serial Rx Task(%d)",common_system_state.stopping);
   common_stop_exit(2);
   common_close_socket(serialPortFd);
   unlink(SERIAL_IPC_SOCKET);
   return NULL;
}

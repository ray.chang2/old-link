/**
 * @file commonState.h
 * @brief Defines state structure for USB subsection 
 * @details Defines state structure that contains
 *          all state for the USB Subsection (used
 *          for heartbeat messages 
 * 
 * @version .01
 * @author  Tom Morrison
 * @date 8/25/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   8/25/13   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __COMMON_STATE_H__
#define __COMMON_STATE_H__

#include "pthread.h"    /**< this defines mutex/lock               */
#include "commonTypes.h"   /**< general type defintions               */

/**
 * COMMON State Structure - contains all USB state information        
 */

typedef enum {
   WAITING_FOR_DISCOVERY_COMPLETION,
   DISCOVERED_IDLE,
   WAITING_CMD_RESPONSE,
} COMMON_STATE_TYPES;

typedef struct common_state {    /**< USB State Structure             */
   // general running state flags //
   u8   state;             /**< general running state of system */
   bool waitCmd;          // waiting for a command response
   u8   waitCmdID;        // cmdID I am waiting for //
   bool stopping;         /**< stop indication flag to threads */
}  __attribute__ ((__packed__))
  common_state_t,               /**< usb state container             */
 *pCommonState;                 /**< usb state container pointer     */

extern common_state_t common_system_state; /**< external reference       */

#define DEFAULT_SYSTEM_NAME        "LENS_SYSTEM"
#define DEFAULT_BROKER_SHARENAME   "IMS"

#endif //__COMMON_STATE_H__

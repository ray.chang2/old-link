/**
 * @file commonSysUtils.h
 * @brief   System Utilties functionality
 * 
 * @version .01
 * @author  Tom Morrison
 * @date 3/18/15
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   3/18/15   Initial Creation.
 *
 *</pre>
 */

#ifndef __COMMON_SYS_UTILS_H__
#define __COMMON_SYS_UTILS_H__

#include "semaphore.h"
#include "commonMiscUtils.h"

/**
 * Thread priority types
 */
typedef enum {
   MIN_COMMON_PRIORITY_LEVEL = 0,
   MAINT_PRIORITY,
   LOWEST_LOW,
   LOWER_LOW,
   MEDIUM_LOW,
   HIGH_LOW,
   LOW_MEDIUM,
   MEDIUM_MEDIUM,
   HIGH_MEDIUM,
   LOW_HIGH,
   MEDIUM_HIGH,
   HIGHER_HIGH,
   HIGHEST_PRIORITY,
   MAX_COMMON_PRIORITY_LEVEL,
} COMMON_THREAD_PRIORITY_LEVELS;

#define HEARTBEAT_PRIORITY  HIGHEST_PRIORITY       /**< redefintion for readability */


/**
 * thread creation
 */
int common_create_thread(pthread_t *pTh, COMMON_THREAD_PRIORITY_LEVELS level, void *args, void *(*thread_fn)(void *)); /**< creates threads         */


typedef struct periodic_info         /**< info structure for timers    */
{
   u32 original_ms;                   /**< original time                */
   int timer_fd;                      /**< timer fd to wait upon        */
   unsigned long long wakeups_missed; /**< number of times missed       */
   bool isRunning;                    /**< boolean indicating running   */
} periodic_info_t,                   /**< typedef periodic info        */
   *periodInfo;                       /**< typedef ptr to periodic info */

#define RESET_PERIODIC_INFO(x) { x->original_ms = 0; x->timer_fd = -1; x->wakeups_missed = 0; x->isRunning = false;}

/**
 * interval timers
 */
int create_periodic_timer(u32 microSecs, periodInfo info);  /**< creates periodic timer */
int wait_periodic_timer(periodInfo info, const char *from); /**< waits for timer        */
int stop_periodic_timer(periodInfo info);                   /**< stops periodic timer   */

#endif //__COMMON_SYS_UTILS_H__

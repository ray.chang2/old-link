/**
 * @file msgD2.h
 * @brief facilities to send/receive bdm/scd messages
 * @details This file contains the following:
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_D2_H__
#define __MSG_D2_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgIp.h"

//////////////////////////////////
// prototocol definitions
//////////////////////////////////
#define PROTOCOL_BDM_TO_SCD        0x35
#define PROTOCOL_SCD_TO_BDM        0x53

typedef enum {
   // chapter 5
   CAPDEV_GENERIC_REPLY = 0x30,
  
   // chapter 6
   CAPDEV_DISCOVERY,

   // Chapter 7
   CAPDEV_HEARTBEAT,

   // chapter 8
   CAPDEV_PORT_STATUS,
   CAPDEV_GET_PORT_STATUS,

   // Chapter 9
   CAPDEV_SET_DEV_INFO,

   // Chapter 10
   CAPDEV_LAVAGE_TOGGLE_EVENT,

   // chapter 11
   CAPDEV_SCD_CMD_REQUEST,

   // Chapter 12/13
   CAPDEV_GET_CONFIG_BLOB,
   CAPDEV_SET_CONFIG_BLOB,

   // chapter 15
   CAPDEV_DEV_SERIAL_NUMBER,

} CAPDEV_CMD_TYPES;

#define HEADER_SIZE_SCD_MSGS          4
#define READ_SIZE_REMAINING_HEADER    2
typedef struct bdm_scd_msg_container {
   u8  protocol_id; 
   u8  command_id;
   u8  cmd_sequence_number;
   u8  cmd_data_len;
   u8 *cmd_data;
} __attribute__ ((__packed__))
  bdm_scd_msg_container_t, *pMsgScd;

void display_msg(pMsgScd msg);

///////////////////////////////
/// Generic Reply
///////////////////////////////
typedef enum {
   ACK_GENERAL_SUCCESS,
   NACK_GENERAL_ERROR,
   NACK_VERSION_NOT_SUPPORTED,
   NACK_PID_NOT_SUPPORTED,
   NACK_CMD_NOT_SUPPORTED,
   NACK_CMD_DATA_LEN_ERROR,
   NACK_CMD_CRC_ERROR,
   NACK_FRAMING_ERROR,
   MAX_REPLY_TYPES,
} GENERIC_REPLY_TYPES;

#define BDM_TO_SCD_GENERIC_RESPONSE CAPDEV_GENERIC_REPLY
#define SCD_TO_BDM_GENERIC_RESPONSE CAPDEV_GENERIC_REPLY
typedef struct capdev_generic_reply {
   u8 status;
   u8 cmd_id;
   u8 cmd_seq_num;    
}  __attribute__ ((__packed__))
  capdev_generic_reply_t, *pGenReply;
#define GENERIC_REPLY_DATA_LENGTH (sizeof(capdev_generic_reply_t))

#define ONE_BYTE_ACK_RESPONSE_DATA_LEN 1

/******************************
////////////////////////////////
/// General Error Status sent //
//  in generic response       //
//  (from OSD_Manager.h)      //
////////////////////////////////
*******************************/
typedef enum {
   SCD_DEVICE_READY,
   SCD_FATAL_ERROR1, // need some clarification
   SCD_FATAL_ERROR2,
   SCD_FATAL_ERROR3,
   SCD_FATAL_ERROR4,
   SCD_FATAL_ERROR_TABLE_1,
   SCD_FATAL_ERROR_TABLE_2,
   SCD_FATAL_ERROR_TABLE_3,
   SCD_FATAL_ERROR_TABLE_4,
   SCD_FATAL_ERROR_TABLE_5,
   SCD_FATAL_ERROR_TABLE_6,
   SCD_FATAL_ERROR_TABLE_7,
   SCD_FATAL_ERROR_TABLE_8,
   SCD_FATAL_ERROR_TABLE_9,
   SCD_FATAL_ERROR_TABLE_10,
   SCD_FATAL_ERROR_TABLE_11,
   SCD_UNABLE_TO_SAVE_CUSTOM_SETTINGS,
   MAX_DEVICE_ERRORS
} SCD_ERROR_TYPES;

/******************************
///////////////////////////////
/// Discovery
///////////////////////////////
*******************************/
#define BDM_TO_SCD_DISCOVERY_REQUEST  CAPDEV_DISCOVERY
#define SCD_TO_BDM_DISCOVERY_RESPONSE CAPDEV_DISCOVERY

typedef struct bdm_to_scd_discover {
   u8 version_number;
} __attribute__ ((__packed__))
  bdm_to_scd_discovery_t, *pBdm2ScdDisc;

#define BDM_TO_SCD_DISCOVERY_REQ_LEN (sizeof(bdm_to_scd_discovery_t))

typedef struct scd_to_bdm_discovery {
   u8 version_number;
   u8 device_state;
   u8 devType;
   u8 devSubType;
} __attribute__ ((__packed__))
  scd_to_bdm_discovery_t, *pScd2BdmDisc;
#define SCD_TO_BDM_DISCOVERY_RESP_LENGTH (sizeof(scd_to_bdm_discovery_t))

/**********************************
///////////////////////////////////
* HEARTBEAT MESSAGES
///////////////////////////////////
***********************************/
#define BDM_TO_SCD_HEARTBEAT_CMD   CAPDEV_HEARTBEAT
#define SCD_TO_BDM_HEARTBEAT_CMD   CAPDEV_HEARTBEAT 

/***************
 * BDM TO SCD 
 ***************/
#define VIS_DEV_READY_MASK    0x01
#define RES_DEV_READY_MASK    0x02
#define FLUID1_DEV_READY_MASK 0x04
#define FLUID2_DEV_READY_MASK 0x08
#define COLB_DEV_READY_MASK   0x10
typedef struct hb_bdm_scd {
   u8  pump_device_state;  // 0 == OFF, 0x11 is ON
} __attribute__ ((__packed__))
  hb_bdm_scd_t, *pHbBdmScd;

#define HB_BDM_SCD_DATA_LENGTH (sizeof(hb_bdm_scd_t)) // 3

/***************
 * SCD to BDM
 ***************/
#define HB_DEVICE_READY_BIT_MASK     0x01

typedef struct hb_scd_bdm {
   u8 devStatus;
} __attribute__ ((__packed__))
  hb_scd_bdm_t, *pHbScd2Bdm;

#define HB_SCD_BDM_DATA_LENGTH (sizeof(hb_scd_bdm_t)) // 1

////////////////////////////////////////////
//////////////// Timeout ///////////////////
////////////////////////////////////////////
#define MAX_NUMBER_RX_TIMEOUT_MISS 10
////////////////////////////////////////////
////////////////////////////////////////////

/**********************************
///////////////////////////////////
* PORT_ STATUS(CHANGE - SCD to BDM)
///////////////////////////////////
***********************************/
#define SCD_TO_BDM_PORT_STATUS          CAPDEV_PORT_STATUS
#define BDM_TO_SCD_PORT_STATUS_RESP     CAPDEV_PORT_STATUS    
#define BDM_TO_SCD_GET_PORT_STATUS      CAPDEV_GET_PORT_STATUS
#define SCD_TO_BDM_GET_PORT_STATUS_RESP CAPDEV_PORT_STATUS
#define PORT_DISPLAY_UNITS_MASK     0x03
#define PORT_DISPLAY_BLADE_MASK     0x0C
#define PORT_DISPLAY_BLADE_SHIFT    (2)
#define PORT_DISPLAY_MODE_MASK      0x30
#define PORT_DISPLAY_MODE_SHIFT     (4)
#define PORT_DISPLAY_UP_ARROW_MASK  0x40
#define PORT_DISPLAY_DN_ARROW_MASK  0x80

#define PORT_SPEED_MASK             0x7C
#define PORT_RUNNING_MASK           0x80

typedef enum {
   NO_RRROR,
   TEMP_FAIL,
   UNKNOWN_BLADE,
   UNKNOWN_HANDPIECE,
   HANDPIECE_SENSOR_FAULT,
   BLADE_STALL,
   HANDPIECE_STALL,
   HANDPIECE_MOTOR_FAULT,
   SHORT_CIRCUIT_DETECT,
   HANDPIECE_OVERLOAD,
   HANDPIECE_OVERLOAD_TIMEOUT,
   HANDPIECE_ERROR,
   UNKNOWN_FOOTSWITCH,
   FOOTSWITCH_ERROR,
   FOOTSWITCH_BATTERY_LOW,
   FOOTSWITCH_REQUIRED,
} PORT_WARN_ERRORS;

#define PORTA_ERROR_WARN_MASK   (0x0F)
#define PORTB_ERROR_WARN_MASK   (0xF0)
#define PORTB_ERROR_WARN_SHIFT  (4)

#define CAPDEV_POPUPS_MASK        (0x07)
#define HANDPIECE_OVERRIDE_MASK   (0x08)
#define BLADE_RECALL_MASK         (0x10)
#define PUMP_PORTB_NOT_PORTA_MASK (0x20)
#define FOOTSWITCH_PORT_MASK      (0x40)
#define IN_SETTINGS_SCREEN_MASK   (0x80)

typedef struct scd_bdm_port_status {
   u8 porta_display;
   u8 porta_speed_run;
   u8 portb_display;
   u8 portb_speed_run;
   u8 port_ab_err_warn;
   u8 settings_popups;
} __attribute__ ((__packed__))
  scd_bdm_port_status_t, *pPortStatus;

#define SCD_PORT_STATUS_DATA_LEN             (sizeof(scd_bdm_port_status_t))
#define BDM_TO_SCD_PORT_STATUS_RESP_DATA_LEN (1) // simple ack/nack byte
#define BDM_TO_SCD_GET_PORT_STATUS_DATA_LEN  (0)

/**********************************
///////////////////////////////////
* PORT_ STATUS(CHANGE - SCD to BDM)
///////////////////////////////////
***********************************/
#define SCD_TO_BDM_SERIAL_NUMBER_STATUS     CAPDEV_DEV_SERIAL_NUMBER
#define BDM_TO_SCD_SERIAL_NUMBER_RESPONSE   CAPDEV_DEV_SERIAL_NUMBER
#define NUM_SERIAL_NUMBER                   (11)

typedef enum {
   SHAVER_SERIAL_TYPE = 0,
   PORTA_SERIAL_TYPE,
   PORTB_SERIAL_TYPE,
   MAX_SERIAL_TYPE,
} SERIAL_NUM_TYPES;

typedef struct scd_bdm_serial_number {
   u8 serial_number[NUM_SERIAL_NUMBER];
   u8 serialType; 
} __attribute__ ((__packed__))
  scd_bdm_serial_number_t, *pSerNum;

#define SCD_SERIAL_NUMBER_DATA_LEN      (sizeof(scd_bdm_serial_number_t))
#define BDM_SERIAL_NUMBER_DATA_LEN_RESP (1) //simple ack/nack byte

/**********************************
///////////////////////////////////
* Set/Get Dev Info
///////////////////////////////////
***********************************/
#define BDM_TO_SCD_SET_DEVICE_INFO_REQUEST  CAPDEV_SET_DEV_INFO
#define SCD_TO_BDM_SET_DEVICE_INFO_RESPONSE CAPDEV_SET_DEV_INFO

typedef struct set_scd_device_info {
   u8  hand_blade_pump_foot;
} __attribute__ ((__packed__))
  set_scd_device_info_t, *pSetScdDevInfo;

#define SET_DEVICE_INFO_DATA_LEN      (sizeof(set_scd_device_info_t))
#define SET_DEVICE_INFO_DATA_LEN_RESP (1) // ack/nack

/**********************************
///////////////////////////////////
* trigger out of SCD and ACK
///////////////////////////////////
***********************************/
#define SCD_TO_BDM_TRIGGER_OUT_EVENT      CAPDEV_LAVAGE_TOGGLE_EVENT
#define BDM_TO_SCD_TRIGGER_OUT_EVENT_RESP CAPDEV_LAVAGE_TOGGLE_EVENT
typedef struct scd_bdm_trigger_out_event {
   u8 lavage_toggle;
} __attribute__ ((__packed__))
  scd_bdm_trigger_out_event_t, *pScdTrigIn;

// data lengths
#define SCD_TO_BDM_TRIGGER_OUT_LENGTH (sizeof(scd_bdm_trigger_out_event_t))


/**********************************
///////////////////////////////////
* SCD Command requests
///////////////////////////////////
***********************************/
#define BDM_TO_SCD_SCD_CMD_REQ   CAPDEV_SCD_CMD_REQUEST
#define SCD_TO_BDM_SCD_CMD_RESP  CAPDEV_SCD_CMD_REQUEST

typedef enum {
   NO_SCD_COMMAND,
   PORTA_UP,
   PORTA_DOWN,
   PORTA_DELTA_OSC_MODE,
   PORTB_UP,
   PORTB_DOWN,
   PORTB_DELTA_OSC_MODE,
   OK_BUTTON,
   EXIT_SETTINGS,
   MAX_SCD_COMMAND,
} SCD_COMMAND_TYPES;

#define BDM_TO_SCD_SCD_CMD_LEN          1 // 1 byte 
#define SCD_TO_BDM_SCD_CMD_RESP_LEN     1 // ack/nack

/**********************************
///////////////////////////////////
* Blob stuff
///////////////////////////////////
***********************************/
#define NUM_BLOB_DATA_DEFAULT               3

#define BDM_TO_SCD_GET_BLOB_CONFIG_REQUEST  CAPDEV_GET_CONFIG_BLOB
#define SCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE CAPDEV_GET_CONFIG_BLOB // data blob

#define BDM_TO_SCD_SET_BLOB_CONFIG_REQUEST  CAPDEV_SET_CONFIG_BLOB // data blob
#define SCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE CAPDEV_SET_CONFIG_BLOB // ack

typedef struct scd_blob_header_msg {
   u8  blobNum;
   ct_blob_data_header_t blobHeader;
} __attribute__ ((__packed__))
  scd_blob_header_msg_t, *pBlobHdrMsg;

#define BLOB_RESPONSE_HEADER_MSG_SIZE (sizeof(scd_blob_header_msg_t))

#define D2_BLOB_DATA_SIZE       (143)
#define D2_TOTAL_BLOB_DATA_SIZE (D2_BLOB_DATA_SIZE + 1) // crc
#define MAX_OPAQUE_DATA_PACKET  (48)

typedef struct scd_blob_data {
   u8  opaqueData[MAX_OPAQUE_DATA_PACKET];
} __attribute__ ((__packed__))
  scd_blob_data_t, *pBlobData;

typedef struct scd_common_blob_top_msg {
   u8  blobNum;
} __attribute__ ((__packed__))
  scd_common_blob_top_msg_t, *pBlobTop;

#define BLOB_DATA_OFFSET (sizeof(scd_common_blob_top_msg_t))

typedef struct scd_blob_data_ack {
   u8 packetNumber;
   u8 status; // ack/nack error
}  __attribute__ ((__packed__))
  scd_blob_data_ack_t, *pBlobDataAck;

#define GET_BLOB_DATA_REQUEST_DATA_LEN      0
#define SET_BLOB_HEADER_DATA_LEN            (sizeof(scd_blob_header_msg_t))
#define GET_BLOB_DATA_RESPONSE_DATA_LEN     (sizeof(scd_blob_data_msg_t))
#define GET_BLOB_DATA_RESPONSE_ACK_DATA_LEN (sizeof(scd_blob_data_ack_t))

#define SET_BLOB_DATA_COMMAND_DATA_LEN      (sizeof(scd_blob_data_msg_t))
#define SET_BLOB_DATA_RESPONSE_DATA_LEN     (sizeof(scd_blob_data_ack_t))


#define NUMBER_D2_DATA_BLOBS    (4)
#define D2_BLOB1_DATA_SIZE      (BLOB_DATA_HEADER_SIZE)
#define D2_BLOB2_DATA_SIZE      (MAX_OPAQUE_DATA_PACKET)
#define D2_BLOB3_DATA_SIZE      (MAX_OPAQUE_DATA_PACKET)
#define D2_BLOB4_DATA_SIZE      (MAX_OPAQUE_DATA_PACKET);

typedef struct blob_data_container
{
   ct_blob_data_header_t blobHeader;
   u8 *pFullBlobData;   
   u8 *pPartialBlob[NUMBER_D2_DATA_BLOBS];
   u8 blobDataSize [NUMBER_D2_DATA_BLOBS];
} blob_data_container_t, *pBlobCont;

/****************************************************
 * exp cmd structure to pass to serial interface 
 ****************************************************/
typedef struct bdm_scd_exp_cmds {
   u8 expCmd;
   u8 expCmdLen; 
} __attribute__ ((__packed__))
  bdm_scd_exp_cmds_t, *pExpCmds;

///////////////////////////////////////////////////////////////
// general message functionality
///////////////////////////////////////////////////////////////
/**
 * offsets for command/data
 */

#define RESET_MSG(msg) (reset_array(msg,sizeof(bdm_scd_msg_container_t))) 

/**
 * more prototypes for utility commands
 */
const char *cmdId2TxtD2(u8 cmdID);
void freeMsgContainerData(pMsgScd msg);      /**< frees/resets message container data */
void freeMsgContainerAll(pMsgScd *msg);      /**< frees/resets message container ptr  */
int read_serial_msg(int *fd, pMsgScd *rxMsg, u8 expProtocol, pExpCmds expCmds, u8 numCmds);
int build_send_d2_msg(int socket_fd, pMsgScd txMsg);
void init_msgD2_lock(void);
void display_blob(pBlobDataHdr pHeader, u8 *blobData, u8 count);
u8 get_request_number(void);
#endif // __MESSAGE_H__

/**
 * @file connUtils.c
 * @brief    utilities used for connection oriented functions
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 1/25/13
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   1/25/13   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <assert.h>
#include <pthread.h>
#include <netdb.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <linux/types.h>
#include <sys/socket.h>
#include <sys/sendfile.h>
#include <linux/tcp.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <linux/un.h>
#include "commonTypes.h"
#include "commonState.h"
#include "commonLog.h"
#include "connUtils.h"
#include "commonDebug.h"
/**
 * max file sizes for videos
 */

/**
 * global/public variables
 */
#define SOCKET_BUFFER_SIZE_MESSAGE (1500)            /**< max size read/write socket   */
bool socketClosing               = false;                    /**< indicates closing socket     */
u32  max_socket_msg_size         = SOCKET_BUFFER_SIZE_MESSAGE;/**< general socket tx/rx size    */


/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions ********************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions **************************/

bool isConnectionValid(int fd)
{
   int error = 0;
   socklen_t len = sizeof (error);
   int retval = getsockopt (fd, SOL_SOCKET, SO_ERROR, &error, &len);
   if (retval)
   {
      ERRF("Failed (%d) getSockOpt - %s\n",retval, COMMON_ERR_STR);
      return false;
   }
   else if (error)
   {
      ERRF("Error (%d) getSockOpt - %s\n",error, COMMON_ERR_STR);
      return false;
   }

   return true;
}

/**
 * @fn          int poll_incoming_connection(int socket_fd)
 * @brief       Waits until incoming event (connection or data) occurs
 * @param	[in] socket_fd   - fd of the socket to receive data from
 * @retval 	>0 --> incoming connection detected 
 *              =0 --> stopping
 *              <0 --> error
 * @warning     semi-Blocking call - only exits if we are stopping or error in polling
 ****************************************************************************/
int poll_incoming_connection(int socket_fd)
{
   struct pollfd ufds[1];      
   ufds[0].fd = socket_fd;
   ufds[0].events = POLLIN;
   int rv;

   if (socket_fd < 0)
   {
      ERRF("%s(%d)\n",__FUNCTION__, socket_fd);
      return -1;
   }

   while(!common_system_state.stopping)
   {
      switch ((rv = poll(ufds,1,10)))
      {
         // error 
      case -1:
         ERRF("POLLIN Error <%s>(%d)\n",COMMON_ERR_STR,common_system_state.stopping);
         return -2;
         break;

         // timeout 
      case 0:
         break;

      default:
         if (ufds[0].revents & POLLIN)
            return 1;
         else if ((ufds[0].revents & POLLERR))
            return -3;
         else if ((ufds[0].revents & POLLHUP))
            return -4;
         else
         {
            ERRF("Invalid Event in Poll(%08X) - <%s>\n",ufds[0].revents, COMMON_ERR_STR);
            return -5;
         }
	      
         break;
      } // end switch //

   }   // end while 
   return 0;
}

/**
 * @fn          int poll_incoming_connection_timed(int socket_fd,u32 waitSecs)
 * @brief       Waits until connection is coming in 
 * @param	[in] socket_fd - fd of the socket to receive data from
 * @param       [in] waitSecs  - waitTime in seconds
 * @retval 	>0 --> incoming connection detected 
 *              =0 --> timeout
 *              <0 --> system error (or stopping)
 ****************************************************************************/
int poll_incoming_connection_timed(int socket_fd, u32 waitSecs)
{
   int rv;
   u32 wait25Ms = 40*waitSecs;
   struct pollfd ufds[1];      
   ufds[0].fd = socket_fd;
   ufds[0].events = POLLIN;
   if (socket_fd < 0)
   {
      ERRF("%s(%d)\n",__FUNCTION__, socket_fd);
      return -1;
   }

   while(!common_system_state.stopping)
   {
      switch ((rv = poll(ufds,1,25)))
      {
         // error 
      case -1:
         ERRF("POLLIN Error <%s>\n",COMMON_ERR_STR);
         return -2;
         break;

         // timeout 
      case 0:
         if (!(--wait25Ms))
            return 0;
	    
         break;

      default:
         if (ufds[0].revents & POLLIN)
            return 1;
         else if ((ufds[0].revents & POLLERR))
            return -3;
         else if ((ufds[0].revents & POLLHUP))
            return -4;
         else
         {
            ERRF("Invalid Event in Poll(%08X) - <%s>\n",ufds[0].revents, COMMON_ERR_STR);
            return -5;
         }
	      
         break;
      } // end switch //

   }   // end while 

   // stopping //
   return -6;
}

/**
 * @fn          bool check_socket_nonblocking(int socket_fd, bool *eventStatus)
 * @brief       checks in non-blocking fashion if there is any action on socket
 * @param	[in] socket_fd    - fd of the socket to receive data from
 * @param	[out] eventStatus - used to indicate if any events are active 
 *              on the socket - true=some type of error, false=no events
 * @retval 	0 == success, otherwise -1 == error
 ****************************************************************************/
int check_socket_nonblocking(int socket_fd, bool *eventStatus)
{
   struct timeval tv;
   fd_set fds, fdsE;

   if (socket_fd < 0)
   {
      ERRF("%s(%d)\n",__FUNCTION__, socket_fd);
      return -1;
   }

   reset_array(&tv,sizeof(tv));
   FD_ZERO(&fds);
   FD_SET(socket_fd, &fds); 
   FD_ZERO(&fdsE);
   FD_SET(socket_fd, &fdsE); 
   if ((select(socket_fd+1, &fds, NULL, &fdsE, &tv))<0)
   {
      ERRF("Select Failure <%s>\n", COMMON_ERR_STR);
      return -1;
   }
   *eventStatus = (FD_ISSET(socket_fd, &fds) || FD_ISSET(socket_fd, &fdsE));
   return 0;
}

/**
 * @fn      wait_network_bytes_available(int inPutFd, int size)
 * @brief   helper function to waiting for the amount of bytes requested 
 * @param   [in] inPutFd - fd to poll
 * @param   [in] size    - number bytes to receive
 * @retval  2 --> socket closed/tablet disconnected
 *          1 --> Stopping
 *          0 --> success
 *         -1 --> system error  
 ****************************************************************************/
int wait_network_bytes_available(int *inPutFd, int size)
{
   int bytesAvailable = 0;
   if (*inPutFd < 0)
   {
      ERRF("%s(%d)\n",__FUNCTION__,*inPutFd);
      return 3;
   }

   if ((ioctl(*inPutFd,FIONREAD,&bytesAvailable)) < 0)
   {
      ERRF("Failed first ioctl (fd:%d) number of bytes(%d) - %s\n",*inPutFd, size, COMMON_ERR_STR);
      return -1;
   }

   while ((bytesAvailable < size) && !common_system_state.stopping)
   {
      // sleep a little while //
      YIELD_5MS;

      if (*inPutFd < 0)
         return 2;

      if ((ioctl(*inPutFd,FIONREAD,&bytesAvailable)) < 0)
      {
         ERRF("Failed cont ioctl (fd:%d) number of bytes(%d) - %s\n",*inPutFd, size, COMMON_ERR_STR);
         return -1;
      }

   }

   if (common_system_state.stopping)
   {
      SYS_WARN("Stopping Waiting for %d bytes from network and (%d) bytes available\n",size,bytesAvailable);
      return 1;
   }
   return 0;
}

/**
 * @fn      wait_network_bytes_available_timed(int *inPutFd, int size, u32 waitSecs)
 * @brief   helper function to waiting for the amount of bytes requested OR timeout
 * @param   [in] inPutFd  - fd to poll
 * @param   [in] size     - number bytes to receive
 * @param   [in] waitSecs - number of seconds to wait
 * @retval  3 --> socket disconnect
 *          2 --> timeout
 *          1 --> Stopping
 *          0 --> success
 *         -1 --> system error   
 ****************************************************************************/
int wait_network_bytes_available_timed(int *inPutFd, int size, u32 waitSecs)
{
   int bytesAvailable = 0;
   u32 wait25Ms = 40*waitSecs;

   if (*inPutFd < 0)
   {
      ERRF("%s(%d)\n",__FUNCTION__,*inPutFd);
      return 3;
   }


   if ((ioctl(*inPutFd,FIONREAD,&bytesAvailable)) < 0)
   {
      ERRF("Failed first ioctl (fd:%d) number of bytes(%d) - %s\n",*inPutFd, size, COMMON_ERR_STR);
      return -1;
   }

   // wait some time //
   YIELD_25MS;

   while ((bytesAvailable < size) && !common_system_state.stopping)
   {
      if (*inPutFd < 0)
      {
         ERRF("%s2(%d)\n",__FUNCTION__,*inPutFd);
         return 3;
      }

      if ((ioctl(*inPutFd,FIONREAD,&bytesAvailable)) < 0)
      {
         ERRF("Failed cont ioctl (fd:%d) number of bytes(%d) - %s\n",*inPutFd, size, COMMON_ERR_STR);
         return -1;
      }

      if (!(--wait25Ms))
         return 2; 

      // wait some time //
      YIELD_25MS;
   }

   // check if stopping
   if (common_system_state.stopping)
   {
      DBGF("HUH - USB_STOPPING(%d)\n",common_system_state.stopping);
      return 1;
   }

   // success
   return 0;
}


/**
 * @fn      int create_tcp_server_socket(u16 port)
 * @brief   creates a tcp server socket to accept connections on
 * @param   [in] port - port to setup
 * @retval  server fd to accept conns on(IFF >0 success, otherwise error)
 */
int create_tcp_server_socket(u16 port)
{
   int server_fd = -1;
   struct sockaddr_in servaddr;
   int on = 1;
   // create a socket
   if ( (server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
   {
      SYS_CRIT("Create TCP/IP Server socket <%s>",COMMON_ERR_STR);
      return -1;
   }

   // set socket options //
   if ((setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on))))
   {
      SYS_CRIT("Create TCP/IP Set Socket Option <%s>",COMMON_ERR_STR);
      return -2;
   }

    
   // set linger to only 1 second 
   struct linger lin;
   lin.l_onoff = 1;
   lin.l_linger = 0;
   if ((setsockopt(server_fd, SOL_SOCKET, SO_LINGER, (const char *)&lin, sizeof(lin))))
   {
      SYS_CRIT("Create TCP/IP Set Socket Linger Option <%s>",COMMON_ERR_STR);
      return -22;
   }

   // bind server address to port //
   memset(&servaddr, 0, sizeof(servaddr));
   servaddr.sin_family = AF_INET;
   servaddr.sin_port = htons(port);
   servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
   if ((bind(server_fd, (struct sockaddr *)&servaddr, sizeof(servaddr))))
   {
      SYS_CRIT("Create TCP/IP Server Bind <%s>",COMMON_ERR_STR);
      return -3;
   }

   // listen on socket //
   if ((listen(server_fd, 1)))
   {
      SYS_ERR("Create TCP/IP Server Listen <%s>",COMMON_ERR_STR);
      return -3;
   }

   // set nodelay
   if ((setsockopt(server_fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on))))
      SYS_ERR("Create TCP/IP Server - Set Socket option NODELAY <%s>",COMMON_ERR_STR);

   // set mk
   on = 128*1024; // 256 
   if ((setsockopt(server_fd, SOL_SOCKET, SO_RCVBUF, &on, sizeof(on))))
      SYS_ERR("Create TCP/IP Server - Set RCV Buf (%d) <%s>",on, COMMON_ERR_STR);

   return server_fd;
}

/**
 * @fn          int accept_tcp_socket_connection(int server_socket_fd)
 * @brief       accepts tcp/ip connections on created tcp server socket from clients 
 * @details     Handles the process of accepting and checking results of accept (and handle accordingly) 
 * @param	[in] server_socket_fd   - socket to accept connection upon
 * @retval 	>0 --> new connection socket, otherwise error
 * @warning     blocking call 
 ****************************************************************************/
static int accept_tcp_socket_connection(int server_socket_fd)
{
   struct sockaddr_in address;
   socklen_t address_length = sizeof(address);
   int connection_fd, on = 1;

   if (server_socket_fd < 0)
      return -1;

   // reset address struct & call blocking accept() //
   reset_array(&address,address_length);
   if ((connection_fd = accept(server_socket_fd, (struct sockaddr *)&address, &address_length))  <= 0)
   {
      SYS_ERR("Accepting TCP/IP Connection <%s>",COMMON_ERR_STR);

#if defined (USB_MAIN) || defined (USB_SIM) 
      build_send_error_message(USB_ERROR_TCP_CONNECTION_PROCESSING);
#endif
      return -2;
   }

//   SYS_INFO("Accepted a connection from Client %s:%u",inet_ntoa(address.sin_addr),address.sin_port);

   // don't linger 
   struct linger lin;
   lin.l_onoff = 0;
   lin.l_linger = 1;

   if ((setsockopt(connection_fd, SOL_SOCKET, SO_LINGER, (const char *)&lin, sizeof(lin))))
      SYS_ERR("Setting TCP/IP Server Linger of new Connection <%s>",COMMON_ERR_STR);

   // set tcp_nodelay
   if ((setsockopt(connection_fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on))))
      SYS_ERR("Accept TCP/IP Connection - Set New Socket option NODELAY <%s>",COMMON_ERR_STR);

   // set rcv/send socket data properly
   on = 128*1024;

   if ((setsockopt(connection_fd, SOL_SOCKET, SO_RCVBUF, &on, sizeof(on))))
      SYS_ERR("Accept TCP/IP Connection - Set RCV Buf (%d) <%s>",on, COMMON_ERR_STR);

   if ((setsockopt(connection_fd, SOL_SOCKET, SO_SNDBUF, &on, sizeof(on))))
      SYS_ERR("Accept TCP/IP Connection - Set Send Buf (%d) <%s>",on, COMMON_ERR_STR);
    
   // return with new connection fd 
   return connection_fd;
}


/**
 * @fn          int accept_tcp_socket_server(int server_socket_fd)
 * @brief       Accepts tcp/ip connections on created tcp server socket from clients 
 * @details     Handles the process of accepting and checking results of accept (and handle accordingly) 
 * @param	[in] server_socket_fd   - socket to accept connection upon
 * @retval 	>0 --> new connection socket, otherwise error
 ****************************************************************************/
int accept_tcp_socket_server(int server_socket_fd)
{
   int status;

   if (server_socket_fd < 0)
      return -1;

   // wait until there is something
   if ((status = poll_incoming_connection(server_socket_fd)) > 0)
      return (accept_tcp_socket_connection(server_socket_fd));

   if (!common_system_state.stopping)
      SYS_CRIT("Poll Error in tcp accept - NOT disconnect");
   else
      SYS_ERR("Poll Error in TCP Accept and Stopping");

   return status;
}


/**
 * @fn          int receive_socket_data_simple(int socket_fd, u8 *input_data, u32 input_size)
 * @brief       receives data from bi-directional socket   (blocking call)
 * @param	[in] socket_fd   - fd of the socket to receive data from
 * @param	[in] input_data  - where to put receive data
 * @param	[in] input_size  - maximum size of data to receive
 * @retval 	>0 --> returns number of bytes receivede
 *              =0 --> client disconnected
 *              <0 --> error
 * @warning     Blocking call
 ****************************************************************************/
int receive_socket_data_simple(int socket_fd, u8 *input_data, u32 input_size)
{
   int nbytes = 0;
   if ((nbytes = read(socket_fd, input_data, input_size)) < 0)
   {
      if (!common_system_state.stopping)
         DBGF("Read Socket Simple (%d) Data <%d> - %s\n",socket_fd, nbytes, COMMON_ERR_STR);
   }
   return nbytes;
}

/**
 * @fn          int receive_socket_data(int socket_fd, u8 *input_data, u32 input_size)
 * @brief       receives data from bi-directional socket   (blocking call)
 * @param	[in] socket_fd   - fd of the socket to receive data from
 * @param	[in] input_data  - where to put receive data
 * @param	[in] input_size  - maximum size of data to receive
 * @retval 	>0 --> returns number of bytes receivede
 *              =0 --> client disconnected
 *              <0 --> error
 * @warning     Blocking call
 ****************************************************************************/
int receive_socket_data(int socket_fd, u8 *input_data, u32 input_size)
{
   // determine if there is data to be read //
   if ((poll_incoming_connection(socket_fd)) <= 0)
   {
      ERRF("RxSockData (poll) failed\n");
      return -1;
   }      
   return (receive_socket_data_simple(socket_fd, input_data,input_size));
}

/**
 * @fn          int receive_socket_data_message(int socket_fd, u8 *input_data, u32 input_size)
 * @brief       receives data from bi-directional socket   (blocking call)
 * @param	[in] socket_fd   - fd of the socket to receive data from
 * @param	[in] input_data  - where to put receive data
 * @param	[in] input_size  - maximum size of data to receive
 * @retval 	>0 --> returns number of bytes receivede
 *              =0 --> client disconnected
 *              <0 --> error
 * @warning     Blocking call
 ****************************************************************************/
int receive_socket_data_message(int socket_fd, u8 *input_data, u32 input_size)
{
   if (input_size <= max_socket_msg_size)
      return (receive_socket_data_simple(socket_fd, input_data, input_size));

   u32 bytes2Read, bytesRead; 
   u32 bytesLeft = input_size;
   u32 dataIndex = 0;
   while(bytesLeft)
   {
      if (bytesLeft < max_socket_msg_size)
         bytes2Read = bytesLeft;
      else 
         bytes2Read = max_socket_msg_size;

      if ((bytesRead = receive_socket_data_simple(socket_fd, &input_data[dataIndex], bytes2Read)) != bytes2Read)
      {
         ERRF("Failed to Read <%d> bytes (only got <%d> bytes)\n",bytes2Read, bytesRead);
         return dataIndex;
      }
	
      dataIndex += bytesRead;
      bytesLeft -= bytesRead;
   } // end while //

   return dataIndex;
}


/**
 * @fn          int send_socket_simple(int socket_fd, u8 *output_data, u32 output_size)
 * @brief       Simple Sending of data on socket (blocking call (we hope))
 * @param	[in] socket_fd    - fd of socket to use to send data upon
 * @param	[in] output_data  - data to be sent
 * @param	[in] output_size  - size of data being transmited
 *
 * @retval 	>0  --> returns number of bytes sent
 *              <=0 --> error/disconnect
 ****************************************************************************/
u32 txSocketCnt = 0;
static int send_socket_simple(int socket_fd, u8 *output_data, u32 output_size)
{
   if (socket_fd < 0)
   {
      ERRF("%s(%d)\n",__FUNCTION__,socket_fd);
      socketClosing = true;
      return -1;
   }
    
   socketClosing = false;
   int nbytes = write(socket_fd, output_data, output_size);
   //    tcdrain(socket_fd);
   if ((nbytes <= 0) && !(common_system_state.stopping))
   {
      if (nbytes < 0)
      {
         if (errno == EPIPE)
         {
            // SYS_DBG("send_socket_simple(%d) - EPIPE(%d) - shutting down socket",socket_fd, errno);
            socketClosing = true;
         }
      }
      else
      {
         DBGF("No Bytes Written to FD <%d> Wanted(%d) - %s\n", socket_fd, output_size, COMMON_ERR_STR);
      }
   }

   return nbytes;

}
/**
 * @fn           int send_socket_data(int socket_fd, u8 *output_data, u32 output_size)
 * @brief        Sends data on a bidirectional socket (blocking call)
 * @param	[in] socket_fd    - fd of socket to use to send data upon
 * @param	[in] output_data  - data to be sent
 * @param	[in] output_size  - size of data being transmited
 *
 * @retval 	>0  --> returns number of bytes sent
 *              <=0 --> error/disconnect
 ****************************************************************************/
int send_socket_data(int socket_fd, u8 *output_data, u32 output_size)
{
   // send simple way if nothing to do
   if (output_size <= SOCKET_BUFFER_SIZE_MESSAGE)
   {
      return (send_socket_simple(socket_fd, output_data, output_size));
   }
  
   int bytes2Write, bytesWritten; 
   u32 bytesLeft = output_size;
   u32 dataIndex = 0;
   while(bytesLeft)
   {
      bytes2Write = bytesLeft;

      if ((bytesWritten = send_socket_simple(socket_fd, &output_data[dataIndex], bytes2Write)) != bytes2Write)
      {
         ERRF("Failed to Write <%d> bytes (only got <%d> bytes)\n",bytes2Write, bytesWritten);
         if (bytesWritten < 0)
         {
            return bytesWritten;
         }

         return dataIndex;
      }

      dataIndex += bytesWritten;
      bytesLeft -= bytesWritten;
   }

   return dataIndex;
}

/**
 * @fn          int send_socket_data_message(int socket_fd, u8 *output_data, u32 output_size)
 * @brief       Sends data on a bidirectional socket (blocking call)
 * @param	[in] socket_fd    - fd of socket to use to send data upon
 * @param	[in] output_data  - data to be sent
 * @param	[in] output_size  - size of data being transmited
 *
 * @retval 	>0  --> returns number of bytes sent
 *              <=0 --> error/disconnect
 ****************************************************************************/
int send_socket_data_message(int socket_fd, u8 *output_data, u32 output_size)
{
   // send simple way if nothing to do
   if (output_size <= max_socket_msg_size)
      return (send_socket_simple(socket_fd, output_data, output_size));
  
   u32 bytes2Write, bytesWritten; 
   u32 bytesLeft = output_size;
   u32 dataIndex = 0;
   while(bytesLeft)
   {
      if (bytesLeft < max_socket_msg_size)
         bytes2Write = bytesLeft;
      else 
         bytes2Write = max_socket_msg_size;

      if ((bytesWritten = send_socket_simple(socket_fd, &output_data[dataIndex], bytes2Write)) != bytes2Write)
      {
         DBGF("Failed to Write <%d> bytes (only got <%d> bytes)\n",bytes2Write, bytesWritten);
         return dataIndex;
      }

      dataIndex += bytesWritten;
      bytesLeft -= bytesWritten;
   }

   return dataIndex;
}


/**
 * @fn      void common_close_socket(int *fd)
 * @brief   Handles closing a socket and resetting 
 * @param   [in] fd - fd for data to be read in 
 ****************************************************************************/\
void common_close_socket(int *fd)
{
   if (*fd > 0)
   {
      shutdown(*fd, SHUT_RDWR);
      usleep(20);
      close(*fd);
   }
   *fd = -1;
}

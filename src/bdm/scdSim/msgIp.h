/**
 * @file msgIp.h
 * @brief common IP protocol msg processing 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __MSG_MSG_IP_H__
#define __MSG_MSG_IP_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/types.h>
#include <arpa/inet.h>
#include "commonTypes.h"
#include "connUtils.h"

#include "commonTypes.h"   /**< must include this for typedefs below     */

/** IP message definitinon sent between all ct devices */
typedef struct ct_ip_msg {
   u8  protocol_id;                 
   u8  command_id;
   u8  version;
   u8  cmd_sequence_number;
   u32 cmd_data_len;
   u8  *cmd_data;
} __attribute__ ((__packed__))
ct_ip_msg_t, *pIpMsg;

////////////////////////////////////
///////// common defines ///////////
////////////////////////////////////
#define HEADER_SIZE_IP_MSGS    8

typedef struct ct_ip_exp_cmds {
   u8 expProt;
   u8 expCmd;
   u8 expCmdLen;
} __attribute__ ((__packed__))
ct_ip_exp_cmds_t, *pIpExpCmds;

#define RESET_MSG_IP(msg) (reset_array(msg,sizeof(ct_ip_msg_t))) 

typedef enum {
   GET_SET_PROTOCOL_CLIENT_TO_BDM    = 0x01,
   GET_SET_PROTOCOL_BDM_TO_CLIENT    = 0x02,
   SETUP_BLOB_PROTOCOL_CLIENT_TO_BDM = 0x11,
   SETUP_BLOB_PROTOCOL_BDM_TO_CLIENT = 0x22,
} MSG_IP_PROTOCOL_TYPES;


// blob definition used with IP Protocols as well as devices //
#define BLOB_NAME_SIZE               16
typedef struct ct_blob_data_header {
   u8  prot_version;
   u8  blob_header_size; // fixed length - should be 23?
   u16 blob_data_size;   // fixed length ==> should be 232
   u8  deviceType;
   u8  subDeviceType;
   u8  blob_name[BLOB_NAME_SIZE];
   u8  header_crc;  
} __attribute__ ((__packed__))
ct_blob_data_header_t, *pBlobDataHdr;

#define BLOB_DATA_HEADER_SIZE (sizeof(ct_blob_data_header_t))
#define BLOB_DATA_HEADER_CRC_SIZE (BLOB_DATA_HEADER_SIZE - 1)
#define D2_BLOB_IP_PACKET_SIZE (D2_TOTAL_BLOB_DATA_SIZE + BLOB_DATA_HEADER_SIZE)

/*
 *  get data Request 
 */
typedef struct ct_ip_get_req_cmd {
   u8 clientDeviceType;
   u8 serverDeviceType;
} __attribute__ ((__packed__))
ct_ip_get_req_cmd_t, *pGetReq;

#define IP_GET_DATA_REQUEST_SIZE     (sizeof(ct_ip_get_req_cmd_t))

// general routines to handle ip messages //
int  read_ip_msg(int *fd, pIpExpCmds pIpCmds, u32 sizeExpCmds, pIpMsg rxMsg);
int  build_send_ip_msg(int socket_fd, pIpMsg txMsg, pthread_mutex_t *lock);
void dump_msg_data(void *data, u32 size);
void display_ip_msg_data(pIpMsg rxMsg);
void freeIpMsgContainerData(pIpMsg msg);


#endif // __MSG_MSG_IP_H__

/**
 * @file commonDebug.h
 * @brief Base definitions for debug
 * @details Debug information that is used throughout the USB Subsystem
 */
// c++ header 
#ifndef __COMMON_DEBUG_H__
#define __COMMON_DEBUG_H__

/**
 * include usbLog here
 */
#include "commonLog.h"

/**
 * Error Printf MACRO
 */
#define ERR(fmt, args...)  { fprintf(stderr,fmt, ##args); }             /**< display in stderr */
#define ERRF(fmt, args...) { fprintf(stderr,fmt, ##args); FLUSH_SLEEP;} /**< display & flush   */

/**
 * Debug Printf MACRO
 */
#define DBGF(fmt,args...) { printf(fmt, ##args);FLUSH_SLEEP; } /**< Debug Print to printf */
#define DBG(fmt,args...)  { printf(fmt, ##args); }             /**< Debug Print to printf */

#define DBG2(fmt,args...) printf(fmt, ##args)     /**< Debug Print to printf */

/**
 * general debug flag 
 */
#undef DO_EXIT_NO_STOP                               /**< disable exit instead of stop  */
//#define  DO_EXIT_NO_STOP                           /**< enable exit instead of stop   */

/**
 * general debug flag 
 */
#undef TESTER_EXIT_ERROR                             /**< disable request to exit upon first tester error  */
//#define TESTER_EXIT_ERROR                            /**< enable  request to exit upon first tester error  */

/**
 * debug/common utilities
 */
char *display_video_input_format(u8 vidInputFormat);         /**< debug output showing input formats */
void display_all_configs (const char *info, u8 mediaTarget); /**< displays All Config parameters      */
void display_fileInfo(void *pInfoFile, u32 hostSize);        /**< displays All Config parameters      */
void display_full_heartbeat_status(void);                    /**< displays full heartbeat status      */
void usbDisplayCurrentCompile(void);                         /**< displays current compiler config    */
void display_rtos_status(void);                              /**< displays rtos status */

/**
 * utilities to display more information 
 */
char *state2Text(u8 state);                       /**< converts a state value to text      */
char *cmdID2Text(u8 cmdID);                       /**< converts a command ID to text       */
void  display_message(void  *m);                  /**< displays Msg data                   */
void  display_message_data(void *m, u8 c);        /**< displays more Msg data              */
char *displayMediaTarget(u8 target);              /**< displays media target               */
char *displayMediaType(u8 type);                  /**< displays media type                 */

char *displayConfigBrokerResponse(u8 response);  /**< query ready response                 */
char *displayNetDriveResponse(u8 response);      /**< query ready response                 */
char *displayQueryUsbReadyResponse(u8 response);  /**< query ready response                */
char *displayMediaPrefixResponse(u8 response);    /**< set media prefix Responses          */
char *displayPushPatientStartResponse(u8 response);  /**< Push Patient Start Responses     */
char *displayPushPatientResponse(u8 response);       /**< Push Patient Responses           */
char *displayPushPatientFinishResponse(u8 response); /**< Push Patient Finish Responses    */
char *displayDeletePatientFolder(u8 response);    /**< Displays Delete Patient Folder Resp */
char *displayCreateFolderResponse(u8 response);   /**< Displays Create Folder Response     */
char *displaySetEncFolderResponse(u8 response);   /**< Displays Create Folder Response     */
char *displaySendFileResponse(u8 response);       /**< Displays Send File Response         */
char *displayFileCompletionResponse(u8 response); /**< Displays File Completion Response   */
char *displayFileReceivedResponse(u8 response);   /**< Displays File Received Response     */
char *displayDisconnectResponse(u8 response);     /**< Displays Disco Response from tablet */
char *displayConnectResponse(u8 response);        /**< Displays Connect Response to tablet */
void dump_packet_data(u8 *p, u32 size);           /**< formats/dumps packet data           */ 
void displayButtonMeaning(u8 button1, u8 button2, u8 button3);

////////////////////////////////////////////////
////////////////////////////////////////////////
// Valid for RTOS, USBSIM, & TABLET HOST APPS //
////////////////////////////////////////////////
////////////////////////////////////////////////
void manage_video_configuration(void);                             /**< manages the video configuration changes  */
int wait_for_user(bool dummy_read, bool doNotDisplay);             /**< wait for user input                                */
int getFullNameString(const char *prompt, char *copyTo, u32 maxSize); /**< gets full name string                            */
int getString2(const char *prompt, char *copyTo, u8 maxSize);      /**< gets string for input w/o '?' (and no HB status)   */
int getString(const char *prompt, char *copyTo, u8 maxSize);       /**< gets string for input (does NOT display HB status) */
int getInteger(char *prompt, int *outInt);                         /**< retrieves an integer from user                     */
int yes_not_no(const char *prompt, bool *resp);                    /**< utility to yes or no answer                        */
int cli_setLogLevel(u8 component);                                 /**< cli prompt to set log level                        */

/**
 * manage cache buffer
 */
void manage_cache_flush_time(void);                                /**< api to adjust cache flush time                     */



/**
 * display unique heartbeat status 
 */
typedef void (*display_unique_status_t)(void);   // function to call back into for displaying unique status
void  register_display_common(void (*display_unique_status_t)(void), bool *hbC); /**< utility to create threads with priorites           */

/**
 * Debug display/init utilties
 */
void display_common_usb_caps(void);                                /**< displays common USB info            */
void display_rtos_report (void *r);                                /**< displays the rtos parameters report */
void display_buttons(u8 b1, u8 b2, u8 b3);                         /**< displays button values              */
void display_procedure_data(void *r);                              /**< utility to display procedure data   */
void display_procedure(void *r);                                   /**< displays procedure specific data    */
void copy_procedure(void *from, void *to);                         /**< copies a procedure from/to          */

/**
 * user Input utilities
 */
int change_network_ssid(void);                                     /**< utility to change network ssid                     */
int change_network_password(void);                                 /**< utility to change network password                 */
int getStringStatus(const char *prompt, char *copyTo, u8 maxSize); /**< displays Heartbeat status while waiting            */
int which_video_source(const char *prompt, u8 *videoSrc);          /**< retrieves selected video input                     */
int set_media_target(u8 *currentTarget);                           /**< retrieves selected media target                    */
void setSymLink(char *fullFileName, const char *to);               /**< creates a symlink                                  */

/**
 * heartbeat debugging
 */
void interpret_usb_heartbeat (void *v);   /**< decodes usb heartbeat capState      */

/**
 * init video data
 */
void init_video_config(void);
int change_vid1_input_format(void);
int toggle_vid2_input(void);
int change_video_encoding_bitrate(u8 vidSrc);
int change_video_encoding_iFrameRate(u8 vidSrc);

extern u32 maxCmdSize;

#define ENTER2CONTINUE { DBGF("\n\t--- Hit Enter to Continue ---\n"); wait_for_user (true,false); }

/**
 * utility to handle exit or stop on error
 */
void common_stop_exit(int exit_code);

extern int imageStart;              /**< debug test variable */
extern int imageComplete;           /**< debug test variable */
extern int imageStart2;             /**< debug test variable */
extern int imageComplete2;          /**< debug test variable */

extern int videoStart;              /**< debug test variable */
extern int videoComplete;           /**< debug test variable */
extern int videoStart2;             /**< debug test variable */
extern int videoComplete2;          /**< debug test variable */
extern int videoReStart;            /**< debug test variable */
extern int videoReComplete;        /**< debug test variable */

#define BROKER_IP    "192.168.10.109"
#define NETDRIVE_IP  BROKER_IP
#define MOUNT_DRIVE  "IMS"


/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
#endif //  __USB_DEBUG_H__

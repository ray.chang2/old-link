/**
 * @file msgD2.c
 * @brief Handles all bdm/d2 message processing
 * @details 
 *    Handles reception and transmission (re-assembles message - builds transmit structure)
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/15/18
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/15/18 Initial Creation.
 *
 * </pre>
 **************************************************************************************************/

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/types.h>
#include <arpa/inet.h>
#include "commonTypes.h"
#include "commonState.h"
#include "commonLog.h"

#include "msgD2.h"
#include "connUtils.h"

static pthread_mutex_t serialLock;
static bool isLockInit = false;
/////////////////////////////////////////////////
extern int doRxCrc;
////////////////////////////////////////////////
/*************************** Constant Definitions ***************************/
#define INVALID_RECEIVED_BYTES ((unsigned)-1) /**< Invalid Number of bytes - Error Detection */

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions ********************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions **************************/

const char *cmdId2TxtD2(u8 cmdID)
{
   switch (cmdID)
   {
   case CAPDEV_GENERIC_REPLY:          return "Generic Reply";
   case CAPDEV_DISCOVERY:              return "Discovery";
   case CAPDEV_HEARTBEAT:              return "Heartbeat";
   case CAPDEV_PORT_STATUS:            return "Port Status Info";
   case CAPDEV_GET_PORT_STATUS:        return "Get Port Status";
   case CAPDEV_SET_DEV_INFO:           return "Set Dev Info";
   case CAPDEV_LAVAGE_TOGGLE_EVENT:    return "Lavage Toggle";
   case CAPDEV_SCD_CMD_REQUEST:        return "SCD CMD Req";
   case CAPDEV_GET_CONFIG_BLOB:        return "Get Config Blob";
   case CAPDEV_SET_CONFIG_BLOB:        return "Set Config Blob";
   default:                            return "Unknown";
   }
}

/**
 * @fn      void freeMsgContainerData(pMsgScd msg)
 * @brief   frees any data associated with message
 * @param   [in] msg - msg to be reset
 */
void freeMsgContainerData(pMsgScd msg)
{
   if (msg)
   {
      if (msg->cmd_data)
         free(msg->cmd_data);
      RESET_MSG(msg);
   }
}

/**
 * @fn      void freeMsgContainerAll(pMsgScd msg)
 * @brief   frees and resets all data associated with message
 * @param   [in] msg - msg to be reset
 */
void freeMsgContainerAll(pMsgScd *msg)
{
   if (*msg)
   {
      freeMsgContainerData(*msg);
      free(*msg);
      *msg = NULL;
   }
}

void display_msg(pMsgScd msg)
{
   DBGF("\t...Prot <%02X> CMD<%s> LEN<%d> Data<%02X>\n",
        msg->protocol_id, 
        cmdId2TxtD2(msg->command_id),
        msg->cmd_data_len,
        *msg->cmd_data);
}

void display_blob(pBlobDataHdr pHeader, u8 *blobData, u8 count)
{
   u16 blobSize = ntohs(pHeader->blob_data_size);
   DBGF("\t---------------------------------------------\n");
   DBGF("\t...Blob Data <%p> Display[%d]...\n",blobData, count);
   DBGF("\t   Dev Type/Sub <%02X/%02X> CRC <%02X>\n",
        pHeader->deviceType, pHeader->subDeviceType, pHeader->header_crc);
   DBGF("\t   Prot<%d> H/B_Size(%d/%d)\n",
        pHeader->prot_version,
        pHeader->blob_header_size,
        blobSize);
       
   char *name = (char *)pHeader->blob_name;
   DBGF("\t   Blob Name<%s>\n",name);
  
   DBGF("\t   Blob Data Start <%p>: ",&blobData[0]);
   int i;
   for (i = 0 ; i < 4 ; i++)
   {
      DBGF("<%02X> ",blobData[i]);
   }
    
   DBGF("\n");

   DBGF("\t   blobDataEnd: ");
   u8 *blobEnd = &blobData[blobSize-5];
   for (i = 0 ; i < 4 ; i++)
   {
      DBGF("<%02X> ",blobEnd[i]);
   }
   DBGF("\n\t----------------data crc-<%02X>------------------\n",blobData[blobSize]);
}

/**
 * @fn      u8 calculate_bdm_scd_msg_crc(bdm_scd_msg_container_t *Msg)
 * @brief   Does a Checksum of an array of characters and returns two's complement CRC
 * @param	[in] Msg - container object of message that needs to be CRCd
 * @retval 	2's complement of the checksum of the received message -
 * @note	        
 * @bug  	
 * @warning      
 * @pre          describe preconditions here
 ****************************************************************************/
u8 calculate_bdm_scd_msg_crc( bdm_scd_msg_container_t *Msg  )
{
   int i;
   u8 *msg      = (u8 *)Msg;
   u8 *data     = Msg->cmd_data;
   u8  dataSize = Msg->cmd_data_len;
   u8  accum    = 0;

   for (i = 0 ; i < 4 ; i++)
      accum += msg[i];

   for (i = 0 ; i < dataSize ; i++)
      accum += data[i];

   char chksum = ~((char)accum) + 1;
   return (u8)chksum;
}


/**
 * @fn      int wait_and_read_bytes(int *inPutFd, u8 *inBytes)
 * @brief   helper function to do actual waiting/reading of bytes from socket
 * @param   [in] inPutFd - fd to receive data on received
 * @param   [in] inBytes - array to put bytes into
 * @retval   3 --> socket read error (just disco)
 *           2 --> tablet disconnect
 *           1 --> stopping 
 *           0 --> success
 *          -1 --> initial ioctl  
 *          -2 --> not used
 *          -3 --> socket read failure (non fatal)
 ****************************************************************************/
u32 totalBytesRead = 0;
static int wait_and_read_bytes(int *inPutFd, u32 size, u8 *inBytes)
{
   int nbytes,status;
   if ((status = wait_network_bytes_available(inPutFd, size)))
   {
      SYS_WARN("Wait Network Bytes failed(%d)",status);
      return status;
   }

   int size2 = size;
   if ((nbytes = receive_socket_data_simple(*inPutFd, inBytes, size)) != size2)
   {
      if (nbytes == 0)
      {
         SYS_WARN("Read '0' bytes after wait_and_readlikely disconnecting");
         return 2;
      }
      SYS_WARN("Failed (%d)to read (%d) bytes from fd(%d)",nbytes, size, *inPutFd);
      return 3;
   }
   totalBytesRead += size;
   return 0;
}

////////////////////////////////////////////////
// seeks first protocolID
////////////////////////////////////////////////
int seek_protocolID(int *fd, pMsgScd rxMsg, u8 expProtocol)
{
   bool continue_read = true;
   int  status = 0;
   while (continue_read && !common_system_state.stopping)
   {
      if ((status = wait_and_read_bytes(fd, 1, &rxMsg->protocol_id)))
      {
         DBG("\t...read protocol failure (%d)....\n",status);
         continue_read = 0;
      }
      else if (rxMsg->protocol_id == expProtocol)
      {
         //  DBG("Found Expect Protocol(%02X)\n", expProtocol);
         continue_read = 0;
      }
   }
   return status;
}

// cmdIndex is a returned value to point to the received command
int read_cmdID(int *fd, pMsgScd rxMsg, pExpCmds expCmds, u8 numCmds, u8 *cmdIndex)
{
   int i, status;
   if ((status = wait_and_read_bytes(fd, 1, &rxMsg->command_id)))
   {
      DBG("read command Status(%d)\n",status);
      return status;
   }
   for (i = 0 ; i < numCmds ; i++)
   {
      if (rxMsg->command_id == expCmds[i].expCmd)
      {
         //            DBGF("rxcmd<%02X>(%d)\n",rxMsg->command_id, i);
         *cmdIndex = i;
         return 0;
      }
   }

   DBG("\n\tdid not find expected cmd for (%02X) and protocol <%02X>\n",rxMsg->command_id, rxMsg->protocol_id);
   return 111;
}

int read_rest_header(int *fd, pMsgScd rxMsg)
{
   int status;
   if ((status = wait_and_read_bytes(fd, READ_SIZE_REMAINING_HEADER, &rxMsg->cmd_sequence_number)))
   {
      DBG("read rest Status(%d)\n",status);
      return status;
   }
   return 0;
}

int read_serial_msg(int *fd, pMsgScd *rxMsg, u8 expProtocol, pExpCmds expCmds, u8 numCmds)
{
   int status;
   pMsgScd localMsg = NULL;
   u8 index = 0;
   u8 crc_offset, total2Read;
    
   // allocate rxMsg //
   if (!(*rxMsg = (pMsgScd)calloc(sizeof(bdm_scd_msg_container_t),1)))
   {
      SYS_ALRT("%s: Malloc Error rxMsg: <%s>",__FUNCTION__, COMMON_ERR_STR);
      return -333;
   }
   else
      localMsg = *rxMsg;

   // start looking for protocol ID and go from there
   while (!common_system_state.stopping)
   {
      if ((status = seek_protocolID(fd, localMsg,expProtocol)) == 0)
      {
         // got success 
         if ((status = read_cmdID(fd, localMsg, expCmds, numCmds, &index)) == 0)
         {
            if ((status = read_rest_header(fd,localMsg)))
            {
               DBGF("Failed to read rest of header\n");
               continue;
            }
            else
            {
               // check if expected length or variable length
               if (localMsg->cmd_data_len == expCmds[index].expCmdLen)
               {
                  goto read_command_data;
               }
                    
               if (expCmds[index].expCmdLen == (u8)(-1))
               {
                  // DBGF("\tReceived Variable Length - assume OK\n");
                  goto read_command_data;
               }
                    
               DBGF("\tGot Command <%02X>(%d) - but not expected cmd data length(%d)\n",
                    localMsg->command_id,
                    localMsg->cmd_data_len,
                    expCmds[index].expCmdLen);
            }
         }
         else if (status == 111)
         {
            //  DBG("\tFailed expected cmd - go back to seek\n");
            continue;
         }
         else
         {
            DBG("Failed Reading of command(%d)\n",status);
            return status;
         }
      }
      DBGF("\t...seeking next protocolID....\n");
   } // end while //

   // error return 
   return 1234;

read_command_data:
   total2Read = localMsg->cmd_data_len + 2; // for crc and 0xFC
   crc_offset = localMsg->cmd_data_len;

   if ((localMsg->cmd_data = (u8 *)calloc(total2Read,1)) == NULL)
   {
      SYS_ALRT("calloc command data (%d) failure - %s",total2Read, COMMON_ERR_STR);
      return -6666;
   } // if failed 
   // wait for data to be received
   if ((status = wait_network_bytes_available(fd,total2Read)))
   {
      free(localMsg->cmd_data); 
      return -6667;
   }
   // now read bytes in //
   int nbytes;
   if ((nbytes = receive_socket_data_message(*fd, localMsg->cmd_data, total2Read)) != total2Read)
   {
      if (nbytes == 0)
      {
         SYS_WARN("Read data from Connection(%d) - likely disconnecting",*fd);
         status =2;
      }
      else
      {
         SYS_WARN("Failed (%d)to read (%d) bytes from fd(%d) for command data",nbytes, total2Read, *fd);
         status = 3;
      }
   } // if error in read //

   // do CRC calculation and check against - no affect on returned message
   if (doRxCrc)
   {
      u8 calcCrc = calculate_bdm_scd_msg_crc(localMsg);
      if (calcCrc != localMsg->cmd_data[crc_offset])
      {
         ERRF("Failed CRC Calc<%02X> Got <%02X>\n",calcCrc, localMsg->cmd_data[crc_offset]);
      }
   }

   // validate that we got a Framing byte
   if (localMsg->cmd_data[crc_offset+1] != 0xFC)
   {
      ERRF("No Framing 0xFC got <%02X>\n",localMsg->cmd_data[crc_offset+1]);
   }

   // returns tatus
   return 0;
}

/**
 * @fn      static int build_transmit_array(pMsgScd txMsg, u8 **outPtr)
 *
 * @brief   creates array for transmission (based upon a message)
 * 
 * @param	[in]     txMsg - command to be sent 
 * @param	[out]    outPtr - pointer to array
 *
 * @retval 	Returns status of the call:
 * 
 *               >0   --> number of bytes in the output array
 *               == 0 --> undefined return;
 *               <0   --> error in construction
 *                      -1 == calloc error
 *                      -x == <tbd>
 *
 * @note	        Calling program must free the output array 
 *               (and command_data array - if necessary)
 *
 * @warning      Free output array & manage command_data array
 ****************************************************************************/
u8 errorCode = 0;
static int build_transmit_array(pMsgScd txMsg, u8 **outPtr)
{
   u32 frame_offset, crc_offset;
   u32 total_size, command_data_size = txMsg->cmd_data_len;
   u8 *output;
   if (errorCode == 6)
      txMsg->cmd_data_len = 1;//~command_data_size;
   
   // calculate a few things //
   crc_offset   = HEADER_SIZE_SCD_MSGS  + command_data_size;
   frame_offset = crc_offset   + 1;
   total_size   = frame_offset + 1;

   // allocate output //
   if ((output = (u8 *)calloc(total_size,1)) == NULL)
   {
      SYS_ALRT("calloc External output array of %d bytes - %s",total_size, COMMON_ERR_STR);
      return -1;
   }
    
   // copy header to output array //
   memcpy(output,txMsg,HEADER_SIZE_SCD_MSGS);

   // copy command data out to array;
   if (command_data_size)
   {
      memcpy(&output[HEADER_SIZE_SCD_MSGS],txMsg->cmd_data, command_data_size);
   }
   
   // calculate CRC and put it in the last byte//
   output[crc_offset]   = calculate_array_crc(crc_offset, output);
   if (errorCode == 9)
      output[crc_offset] = ~output[crc_offset];

   output[frame_offset] = 0xFC;
   if (errorCode == 10)
      output[frame_offset] = ~output[frame_offset];
   
   // init output ptr & return with number of bytes created //
   *outPtr = output;
   return total_size;
}

/**
 * @fn      int build_send_d2_msg(int socket_fd, pMsgScd txMsg)
 * @brief   (PUBLIC API) Sends a cmd message on socket using a specified protocolID with associated command data
 * @param	[in] socket_fd  - socket to send message over
 * @param	[in] txMsg      - message to be sent
 * @retval 	Success        == 0
 *               Internal Error  < 0
 *               External Error  > 0   
 ****************************************************************************/
static u8 txCmdSequence = 1;
u8 get_request_number(void)
{
   u8 txCmdSeq = (u8)(txCmdSequence & 0xFF);
   
   // manage rollover //
   if (txCmdSequence == 0x7F)
   {
      txCmdSequence = 1;
   }
   else
   {
      txCmdSequence++;
   }
   return txCmdSeq | 0x80;   
}

int build_send_d2_msg(int socket_fd, pMsgScd txMsg)
{
   u8 *outPtr;
   int bytes2Send, bytesSent;
   int status = 0;

   if (!(isLockInit))
   {
      DBGF("\n\t...lock was never initialized....\n");
      return -1;
   }

   pthread_mutex_lock(&serialLock);
   
   // pack message 
   if ((bytes2Send = build_transmit_array(txMsg, &outPtr)) <= 0)
      return -1;

   // send it now
   bytesSent = send_socket_data(socket_fd, outPtr, bytes2Send);
   if (bytesSent != bytes2Send)
   {
      DBGF("\tFailed to Send message (cmdID:%02X)\n",txMsg->command_id);
      status = 1;
   }

   //free resources and returns tatus
   free(outPtr);
   pthread_mutex_unlock(&serialLock);
   return status;
}

void init_msgD2_lock(void)
{
   pthread_mutex_init(&serialLock,NULL);
   isLockInit = true;
}

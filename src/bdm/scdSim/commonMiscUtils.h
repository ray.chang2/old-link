/**
 * @file commonMiscUtils.h
 * @brief   Miscellaneous Utilties functionality
 * @details all the forward declarations 
 *             thread creation/deletion
 *             semaphores
 * 
 * @version .01
 * @author  Tom Morrison
 * @date 8/25/13
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   8/25/13   Initial Creation.
 *
 *</pre>
 *
 * @todo  future: timers
 * @todo  list to do items
 */

#ifndef __COMMON_MISC_UTILS_H__
#define __COMMON_MISC_UTILS_H__

#include "commonSysUtils.h"

/**
 * time related
 */
void   convert_miliSecs(u32 ms, u32 *sec, u32 *ns); 

/**
 * resets mili 
 */
static inline void reset_timeval(struct timeval *t) { t->tv_sec = 0; t->tv_usec = 0; }

/**
 * signal handler
 */
void init_common_signal_handler(void);             /**< initializes common signal handler                  */
/**
 * File Utilities
 */
void reset_array(void *p, u32 size);                                  /**< resets array                           */
/**
 * validation utilities
 */
bool isFileExist(const char *fullFileName);                           /**< checks if file/folder exists             */

/**
 * handles file manipulation 
 * @note  assumes that directory/filenames are full paths
 */
int  fileCopy(const char *to, const char *from, bool *busyFlag, u8 *watchdogCounter); /**< copies to/from with watchdog counter  */
int  fileMove(const char *to, const char *from, bool *busyFlag, u8 *watchdogCounter); /**< moves to/from with watchdog counter   */
int  dirCopy(const char *to, const char *from,  bool *busyFlag, u8 *watchdogCounter); /**< copies dir. to/from w/ watchdog count */
int  dirDelete(const char *delDir);                                                   /**< deletes directory                     */
int  makeDirectory(const char *directory);                                            /**< creates directory                     */

/**
 * utilities to calculate array crc, file size, & file crc
 */
u32 get_file_size(char *fullFilename, bool *fileExists);              /**< returns size & if exists                 */
u8  calculate_array_crc(u32 size, u8 *array);                         /**< public function for calculating crc      */


/**
 *  sleep for x seconds or if USB is stopping
 */
int  sleepSecs(u32 timeWaitSecs);       /**< sleep in seconds */

#endif //__COMMON_Misc_UTILS_H__

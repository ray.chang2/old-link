#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <errno.h>
#include <signal.h>

int new_serial_cli_task(void);
char *comport;
int doRxCrc = 1;
int main(int argc, char *argv[])
{
   // check to make sure yo ugot a host //
   if ((argc < 2) || (argc > 3))
   {
      fprintf(stderr,"usage %s <COM Port> <DoRxCrc (default:1/0(noCrc))>\n", argv[0]);
      exit(0);
   }
   else
      comport = argv[1];

   if (argc == 3)
      doRxCrc = atoi(argv[2]);

   return (new_serial_cli_task()); 
}

/*
 * CT_defines.h
 *
 *  Created on: Nov 7, 2018
 *      Author: Carlos Rodriguez
 */
/*
 * Modified on: Aug 7, 2019
 *   Author: Akshay Acharya
 */

#ifndef SRC_COMMONBSPFILES_CT_DEFINES_H_
#define SRC_COMMONBSPFILES_CT_DEFINES_H_

#define CTDEFINES_HEADER_VERSION  01

/********************DEVICE TYPES ************************************************************/
/**< Enumeration for the Connected Tower Device Types see eConnectedTowerEnums in CT_defines.h*/

typedef enum {
   eConnectedTowerUndefined,		/**< not defined */
   eConnectedTowerResection,		/**<D2EIP,BRICK 		*/
   eConnectedTowerVis,				/**< visualization 		*/
   eConnectedTowerCoblation,		/**< werewolf 		*/
   eConnectedTowerTablet,			/**< tablet 		*/
   eConnectedTowerFluidOne,		/**< d25 single 		*/
   eConnectedTowerFluidTwo,		/**< future dual pump 		*/
   eConnectedTowerRobot,			/**< future robot 		*/
   eConnectedTowerOther,			/**< the rest */
   eConnectedTowerLast				/**< end 		*/
}eConnectedTowerEnums;


/********************DEVICE SUBTYPES ************************************************************/

/**<eConnectedTowerVis Subytpes */
typedef enum {
   eConnectedTowerLENS = 1,
   eConnectedTowerLENS4K,
   eConnectedTowerArtemis,
   eConnectedTowerWirelessVis,
   eConnectedTotalVisSubTypes,
}eConnectedTowerVisSubEnums;

/**<eConnectedTowerResection Subtypes */
typedef enum {
   eConnectedTowerD2Shaver = 1,
   eConnectedTotalResectionSubTypes,
}eConnectedTowerResectionSubEnums;

/**<eConnectedTowerCoblation Subtypes */
typedef enum {
   eConnectedTowerCoblationWereWolf = 1,
   eConnectedTotalCoblationSubTypes,
}eConnectedTowerCoblationSubEnums;

/**<eConnectedTowerTablet Subtypes */
typedef enum {
   eConnectedTowerTabletIntellioTouch = 1,
   eConnectedTotalTabletSubTypes,
}eConnectedTowerTabletSubEnums;

/**<eConnectedTowerFluidOne Subtypes */
typedef enum {
   eConnectedTowerD25 = 1,
   eConnectedTotalFluid1SubTypes,
}eConnectedTowerFluid1SubEnums;

/**<eConnectedTowerFluidTwo Subtypes */
typedef enum {
   eConnectedTowerHemodiaFluid2 = 1,
   eConnectedTotalFluid2SubTypes,
}eConnectedTowerFluid2SubEnums;


/********************TRIGER BITMASKS PER DEVICE ************************************************************/

/**<eConnectedTowerFluidOne : D25 triggers */

typedef enum {
   eFluidOneTrigger_LavageToggle,	/**< Toggle Lavage */
   eFluidOneTrigger_Total			/**< end 		*/
}eFluidOneTriggerEnums;

/**<eConnectedTowerFluidTwo : Hemodia triggers */
typedef enum {
   eFluidTwoTrigger_LavageToggle,	/**< Toggle Lavage */
   eFluidTwoTrigger_Total			/**< end 		*/
}eFluidTwoTriggerEnums;


/**<eConnectedTowerVis : LENS4K triggers */
typedef enum {
   ePowerStandbyMode,				/**< Toggle PowerStandby */
   eWhiteBalanceInitiate,			/**< Initiate White Balance */
   eLightToggle,					/**< Toggle Light Mode request, note light guide must be plugged in */
   eVisualizationTriggers_Total				/**< end 		*/
}eVisualizationTriggerEnums;




/********************FUNCTION BITMASKS PER DEVICE (bit positions, if set then present)************************************************************/

/* LENS 4K*/
typedef enum {
   eFB_Visualization_CT=0,			/**< Connected Tower Functionality */
   eFB_Visualization_OSD,			/**< Connected Tower OSD Manager Service */
   eFB_Visualization_CTManager,	/**< Connected Tower Device Manager Service */
   eFB_Visualization_CTGETSET,		/**< Connected Tower GET/SET Service */
   eFB_Visualization_Triggers,		/**< Connected Tower Trigger Service see trigger enums for visualization */
   eFB_Visualization_TotalNumberOfServices
}eFunctionBitMaskVisualization;


/* D2 */
typedef enum {
   eFB_Resection_OSD =0 ,            /**< Connected Tower OSD Manager Service */
   eFB_Resection_CTManager,    /**< Connected Tower Device Manager Service */
   eFB_Resection_CTGETSET,        /**< Connected Tower GET/SET Service */
   eFB_Resection_Triggers,        /**< Connected Tower Trigger Service see trigger enums for Resection */
   eFB_Resection_Software_Update, /**< Connected Tower Software Update Service */
   eFB_Resection_Setup_BLOB,       /**< Connected Tower Setup BLOB Service */
   eFB_Resection_Device_BLOB,      /**< Connected Tower Device BLOB Service */
   eFB_Resection_TotalNumberOfServices
}eFunctionBitMaskResection;

/* WW */
typedef enum {
   eFB_Coblation_OSD =0 ,            /**< Connected Tower OSD Manager Service */
   eFB_Coblation_CTManager,    /**< Connected Tower Device Manager Service */
   eFB_Coblation_CTGETSET,        /**< Connected Tower GET/SET Service */
   eFB_Coblation_Triggers,        /**< Connected Tower Trigger Service see trigger enums for Coblation */
   eFB_Coblation_Software_Update, /**< Connected Tower Software Update Service */
   eFB_Coblation_Setup_BLOB,       /**< Connected Tower Setup BLOB Service */
   eFB_Coblation_Device_BLOB,      /**< Connected Tower Device BLOB Service */
   eFB_Coblation_TotalNumberOfServices
}eFunctionBitMaskCoblation;

/* D25 */
typedef enum {
   eFB_FluidMgmt_OSD =0 ,            /**< Connected Tower OSD Manager Service */
   eFB_FluidMgmt_CTManager,    /**< Connected Tower Device Manager Service */
   eFB_FluidMgmt_CTGETSET,        /**< Connected Tower GET/SET Service */
   eFB_FluidMgmt_Triggers,        /**< Connected Tower Trigger Service see trigger enums for FluidMgmt */
   eFB_FluidMgmt_Software_Update, /**< Connected Tower Software Update Service */
   eFB_FluidMgmt_Setup_BLOB,       /**< Connected Tower Setup BLOB Service */
   eFB_FluidMgmt_Device_BLOB,      /**< Connected Tower Device BLOB Service */
   eFB_FluidMgmt_TotalNumberOfServices
}eFunctionBitMaskFluidMgmt;

#define PIDMSG_TOTAL_DEFINED 21
typedef enum {
   ePIDMSG_GETSET_CLIENT=0x01,			/**< client to server */
   ePIDMSG_GETSET_SERVER=0x02,			/**< server to client */
   ePIDMSG_SETUPBLOB_ClIENT=0x11,		/**< client to server */
   ePIDMSG_SETUPBLOB_SERVER=0x22,		/**< server to client */
   ePIDMSG_RTOS_2_USB=0x69,				/**< tablet 		*/
   ePIDMSG_RTOS_2_TABLET=0x96,			/**< d25 single 		*/
   ePIDMSG_USB_2_RTOS=0xa5,				/**< future dual pump 		*/
   ePIDMSG_TABLET_2_RTOS=0x5a,			/**< future robot 		*/
   ePIDMSG_TABLET_2_USB=0xd9,			/**< the rest */
   ePIDMSG_USB_2_TABLET=0xb6,			/**< the rest */
   ePIDMSG_USB_2_USB=0x6a,				/**< the rest */
   ePIDMSG_OSD_CLIENT=0x66,			/**< client to server */
   ePIDMSG_OSD_SERVER=0x99,			/**< server to client */
   ePIDMSG_UPGRADE_USB_2_RTOS=0xb0,	/**< client to server */
   ePIDMSG_UPGRADE_RTOS_2_USB=0xb1,	/**< server to client */
   ePIDMSG_DEVICEINFO_CLIENT=0x43,		/**< client to server */
   ePIDMSG_DEVICEINFO_SERVER=0x44,		/**< server to client */
   ePIDMSG_DISCOVERY_CLIENT=0x33,		/**< client to server */
   ePIDMSG_DISCOVERY_SERVER=0xcc,		/**< server to client */
   ePIDMSG_TRIGGER_CLIENT=0x03,		/**< client to server */
   ePIDMSG_TRIGGER_SERVER=0x04,		/**< server to client */
}ePIDMSG_enum;


#endif /* SRC_COMMONBSPFILES_CT_DEFINES_H_ */

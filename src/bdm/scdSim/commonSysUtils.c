/**
 * @file commonSysUtils.c
 * @brief    utilities used by the Subsystem
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 3/18/15
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   3/18/15   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <execinfo.h>
#include <assert.h>
#include <pthread.h>
#include <netdb.h>
#include <poll.h>
#include <time.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/vfs.h>
#include <linux/input.h>

#include <linux/types.h>
#include <linux/sched.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/un.h>
#include "commonTypes.h"
#include "commonState.h"
#include "connUtils.h"
#include "commonSysUtils.h"
#include "commonLog.h"
/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//////////////////// Thread related functionality //////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

/**
 * @fn      void common_kill_thread(pthread_t *pThId, const char *whatThread)
 * @brief   forces the exit of threads
 * @param   [in] pThId - thread to be killed
 * @param   [in] whatThread - debug output
 ****************************************************************************/
void common_kill_thread(pthread_t thread, const char *whatThread)
{
   int status;
   SYS_DBG("Canceling <%s> thread",whatThread);

   // cancel thread //
   if ((status = pthread_cancel(thread)) != 0) 
   {
      if (status != ESRCH)
         SYS_ALRT("Invalid kill of thread(%s) - <%s>",whatThread, COMMON_ERR_STR);
      else
         SYS_INFO("<%s> thread already has exited",whatThread);
   }

   SYS_DBG("Joining <%s> thread",whatThread);
   pthread_join(thread,NULL);
   SYS_DBG("Finished Kill of <%s> thread",whatThread);
} 


/**
 * @fn      int  common_create_thread(pthread_t *pTh, COMMON_THREAD_PRIORITY_LEVELS level, void *args, void *(*thread_fn)(void *))
 * @brief   creates a thread for calling program 
 * @param   [in] pTh       - pointer to threadID to use
 * @param   [in] level     - enum priority type
 * @param   [in] args      - arguments for thread
 * @param   [in] thread_fn - actual thread function
 * @retval  0 == success, otherwise, error
 ****************************************************************/
int common_create_thread(pthread_t *pTh, UNUSED_ATTRIBUTE COMMON_THREAD_PRIORITY_LEVELS level, void *args, void *(*thread_fn)(void *))
{
   int status;
   ////////// If we are not on Baseboard - no priority attributes //////////
   pthread_attr_t tattr;
   pthread_attr_init(&tattr);
   pthread_attr_setstacksize(&tattr,THREAD_STACK_SIZE);
   //    if ((status = pthread_create(pTh, NULL, thread_fn, args)))
   if ((status = pthread_create(pTh, &tattr, thread_fn, args)))
   {
      SYS_CRIT("pthread_create <%s>", COMMON_ERR_STR);
      return -2;
   }

   // let the thread start - if it can //
   YIELD_3MS;
   return 0;
}
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////   periodic timer functions   //////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

/**
 * @fn      int create_periodic_timer(u32 milliSecs, periodInfo info)
 * @brief   creates periodic timer 
 * @param   [in]  milliSecs - periodic interval in microseconds
 * @param   [out] info      - info struct for timer
 ****************************************************************/
int create_periodic_timer(u32 milliSecs, periodInfo info)
{
   u32 sec, ns;
   struct itimerspec itval;
   // validate input params

   if (info->isRunning || (milliSecs == 0))
   {
      SYS_WARN("Invalid Input (%d/%d)",info->isRunning, milliSecs);
      return -1;
   }

   /* reset & create timer */
   RESET_PERIODIC_INFO(info);
   if ((info->timer_fd = timerfd_create(CLOCK_REALTIME, 0)) <= 0)
   {
      SYS_ERR("Unable to Create periodic timer - %s", COMMON_ERR_STR);
      return -2;
   }

   // reset structs
   info->original_ms = milliSecs;

   /* Make the timer periodic */
   convert_miliSecs(milliSecs, &sec, &ns);

   SYS_INFO("Creating Periodic Timer for %u(sec).%u(nsec)", sec,ns);
   itval.it_interval.tv_sec = sec;
   itval.it_interval.tv_nsec = ns;
   itval.it_value.tv_sec = sec;
   itval.it_value.tv_nsec = ns;
   if ((timerfd_settime (info->timer_fd, 0, &itval, NULL)))
   {
      SYS_ERR("Unable to Start periodic timer(%u ms)  - %s", milliSecs, COMMON_ERR_STR);
      return -3;
   }

   info->isRunning = true;
   return 0;
}

/**
 * @fn      int stop_periodic_timer(periodInfo info)
 * @brief   stops periodic timer
 * @param   [in] info - periodic structure to stop
 * @retval  0 == success, otherwise, error
 ****************************************************************/
int stop_periodic_timer(periodInfo info)
{
   struct itimerspec itval;
   int status = 0;
   if (!info->isRunning)
   {
      SYS_INFO("periodic Timer(%p) is already stopped",info);
      return 0;
   } 

   else if (info->timer_fd <= 0)
   {
      SYS_WARN("timer (%p) fd (%d) is invalid", info, info->timer_fd);
      status = -1;
      goto exit_now;
   }
    
   if((timerfd_gettime(info->timer_fd,&itval)))
   {
      SYS_ERR("Unable to get timer for fd <%d> - %s",info->timer_fd, COMMON_ERR_STR);
      status = -2;
      goto exit_now;
   }

   itval.it_value.tv_sec  = 0;
   itval.it_value.tv_nsec = 0;
   if ((timerfd_settime (info->timer_fd, 0, &itval, NULL)))
   {
      SYS_ERR("Unable to Reset periodic timer for fd <%d>  - %s", info->timer_fd, COMMON_ERR_STR);
      status = -3;
      goto exit_now;
   }

exit_now:
   SYS_INFO("Closing Timer with status (%d)",status);
   if (info->timer_fd > 0)
      close(info->timer_fd);
   info->timer_fd = -1;
   info->isRunning = false;
   return status;
}


/**
 * @fn      int wait_periodic_timer(periodInfo info, const char *from)
 * @brief   waits periodic timer 
 * @param   [in] info - periodic structure to stop
 * @param   [in] from  - source of call
 * @retval  0 == success, otherwise, error
 ****************************************************************/
int wait_periodic_timer(periodInfo info, const char *from)
{
   // validate info //
   if ((info == NULL) || (from == NULL))
      return -1;
   else if (!info->isRunning)
   {
      SYS_INFO("periodic Timer(%p) for (%s) is not running",info, from);
      return -2;
   } 
   else if (info->timer_fd <= 0)
   {
      SYS_WARN("timer (%p) fd (%d) for (%s) is invalid", info, info->timer_fd, from);
      return -3;
   }
   else
   {
      unsigned long long missed;
      if ((read(info->timer_fd, &missed, sizeof(missed))) <= 0)
         SYS_ERR("<%s> Timer FD (%d) Read Error(%d) - %s", from, info->timer_fd, info->isRunning, COMMON_ERR_STR);
      else if (missed)
      {
         info->wakeups_missed += (missed - 1);
         if (missed >= 5)
            SYS_NOTE("<%s> Missed Timer <%llu> Times, Total Missed <%llu>", from, missed, info->wakeups_missed);
      }
   }
   return 0;
}


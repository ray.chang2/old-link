/**
 * @file commonMiscUtils.c
 * @brief    utilities for additional usb stuff
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 7/28/16
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   7/28/16   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <execinfo.h>
#include <assert.h>
#include <pthread.h>
#include <netdb.h>
#include <poll.h>
#include <time.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/select.h>
#define _FILE_OFFSET_BITS 64
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/vfs.h>
#include <linux/input.h>

#include <linux/types.h>
#include <linux/sched.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/un.h>

#include "commonTypes.h"
#include "commonState.h"
#include "commonLog.h"
#include "connUtils.h"
#include "commonMiscUtils.h"

/*************************** Constant Definitions ***************************/
/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/
#define SPLICER_BUFFER_SIZE       (16 * THOUSAND2)  /**< size of splice buffer        */
#define SPLICER_BUFFER_SIZE2      (4 * THOUSAND2)   /**< size of splice buffer        */
#define SPLICER_MICRO_DELAY       (1256)            /**< splice delay for catching up */
#define SPLICER_WHEN_EDELAY       (5)               /**< spice delay after splicing   */ 
#define MAX_CRC_CHECK_FILE_SIZE   (MILLION2)        /**< max crc file size            */

ssize_t splice(int fd_in,  loff_t *off_in, 
	       int fd_out, loff_t *off_out, 
	       size_t len, unsigned int flags);         /**< forward declaration     */

#define SPLICER(a, b, c)     (splice(a, 0, b, 0, c, 0)) /**< defines splice shortcut */ 

u32  splicer_buffer  = SPLICER_BUFFER_SIZE;
u32  splicer_buffer2 = SPLICER_BUFFER_SIZE2;

u32  splicer_index   = SPLICER_WHEN_EDELAY;
u32  splicer_time    = SPLICER_MICRO_DELAY;

extern char *strcasestr (char *haystack, const char *needle); /**< compiler comment */

struct rusage localMemory;  /**< used to get usage     */
u8 imsErrorInjection = 0;

/**
 * @fn      void reset_xxxx_arrayX(void)
 * @brief   resets buffers 4 bytes at a time
 * @note    assumes that the passed in variables are 4 byte aligned
 ****************************************************************************/
/**
 * reset simple array
 */
void reset_array(void *p, u32 size)
{
   int i, k=(size%4), l=(size/4);
   u32 *j = (u32 *)p;
  
   for (i = 0 ; i < l ; i++)
      j[i] = 0;

   // if there are bytes that aren't reset - reset them there...
   if (k)
   {
      u8 *b = (u8 *)&j[i];
      bzero(b,k);
   }
}

/**
 * reset simple name array (32 bytes)
 */
void reset_small_array(void *p)
{
   u32 *j = (u32 *)p;
   int i;
   for (i = 0 ; i < (DEFAULT_SMALL_SIZE/4) ; i++)
      j[i] = 0;
   u8  *b = (u8  *)p;
   b[DEFAULT_SMALL_SIZE] = 0;

}

/**
 * reset simple name array (33 bytes)
 */
void reset_small_array1(void *p)
{
   u32 *j = (u32 *)p;
   int i;
   for (i = 0 ; i < (DEFAULT_SMALL_SIZE/4) ; i++)
      j[i] = 0;
}

/**
 * reset simple name array (64 bytes)
 */
void reset_name_array(void *p)
{
   u32 *j = (u32 *)p;
   int i;
   for (i = 0 ; i < (DEFAULT_NAME_SIZE/4) ; i++)
      j[i] = 0;
}

/**
 * reset simple name array + 1 buffer (65 bytes)
 */
void reset_name_array1(void *p)
{
   u32 *j = (u32 *)p;
   u8  *b = (u8  *)p;
   int i;
   for (i = 0 ; i < (DEFAULT_NAME_SIZE/4) ; i++)
      j[i] = 0;
   b[DEFAULT_NAME_SIZE] = 0;
}


/**
 * reset simple fullname array (128 bytes)
 */
void reset_fullname_array(void *p)
{
   int i;
   u32 *j = (u32 *)p;
   for (i = 0 ; i < (DEFAULT_FULLNAME_SIZE/4) ; i++)
      j[i] = 0;
}

/**
 * reset simple fullname array + 1 (129 bytes)
 */
void reset_fullname_array1(void *p)
{
   int i;
   u32 *j = (u32 *)p;
   u8  *b = (u8  *)p;
   for (i = 0 ; i < (DEFAULT_FULLNAME_SIZE/4) ; i++)
      j[i] = 0;

   b[DEFAULT_FULLNAME_SIZE] = 0;
}


/**
 * reset simple fullname2 array (257 bytes)
 */
void reset_fullname_array2(void *p)
{
   int i;
   u32 *j = (u32 *)p;
   u8  *b = (u8  *)p;
   for (i = 0 ; i < (DEFAULT_FULLNAME_SIZE2/4) ; i++)
      j[i] = 0;

   b[DEFAULT_FULLNAME_SIZE2] = 0;
}

/**
 * reset simple (command buffer) + 1 buffer (257 bytes)
 */
void reset_cmdline_array(void *p)
{
   u32 *j = (u32 *)p;
   u8  *b = (u8  *)p;
   int i;
   for (i = 0 ; i < (DEFAULT_COMMAND_LINE_SIZE/4) ; i++)
      j[i] = 0;
   b[DEFAULT_COMMAND_LINE_SIZE] = 0;
}

void copy_small(void *sI, void *dI)
{
   u32 *s = (u32 *)sI;
   u32 *d = (u32 *)dI;
   int i;
   for (i = 0 ; i < (DEFAULT_SMALL_SIZE/4) ; i++)
      d[i] = s[i];
}

void copy_name(void *sI, void *dI)
{
   u32 *s = (u32 *)sI;
   u32 *d = (u32 *)dI;
   int i;
   for (i = 0 ; i < (DEFAULT_NAME_SIZE/4) ; i++)
      d[i] = s[i];
}

void copy_fullname(void *sI, void *dI)
{
   u32 *s = (u32 *)sI;
   u32 *d = (u32 *)dI;
   int i;
   for (i = 0 ; i < (DEFAULT_FULLNAME_SIZE/4) ; i++)
      d[i] = s[i];
}

void copy_fullname2(void *sI, void *dI)
{
   u32 *s = (u32 *)sI;
   u32 *d = (u32 *)dI;
   int i;
   for (i = 0 ; i < (DEFAULT_FULLNAME_SIZE2/4) ; i++)
      d[i] = s[i];
}

/**
 * @fn      void segfault_handler(int sig) 
 * @brief   signal handler to handle segfault events
 * @param   [in] signal - signal that is active
 * @retval  none
 ****************************************************************************/
static void segfault_handler(int sig) 
{
   void *array[10];
   int numBack;
    
   // get void*'s for all entries on the stack
   numBack = backtrace(array, 10);
    
   // print out all the frames to stderr
   fprintf(stderr, "\n\nSegfault(%d):\n--------\n",sig);
   backtrace_symbols_fd(array, numBack, fileno(stderr));
   YIELD_250MS;
   exit(-3);
}


/**
 * @fn      void sig_handler(int signal)
 * @brief   signal handler to receive almost all signal events
 * @param   [in] signal - signal that is active
 * @retval  none
 ****************************************************************************/
static int quitting = 0;
bool isSignalTerm = false;
static void sig_handler(int signal) 
{
   switch(signal)
   {
   case SIGTERM:
      SYS_NOTE("SIGTERM Received (%d/%d)", isSignalTerm,quitting);
      isSignalTerm = true;
      if (quitting)
      {
         SYS_ERR("Got SIGTERM AND WE WERE STOPPING - exit NOW");
         YIELD_10MS;
         exit(2);
      }
      common_stop_exit(-666);
      quitting  = true;
      YIELD_250MS;
      return;

   case SIGINT:
      SYS_NOTE("SIGINT Received (%d)",quitting);
      if (quitting)
      {
         SYS_ERR("Got SIGINT AND WE WERE STOPPING - exit NOW");
         YIELD_10MS;
         exit(1);
      }

      common_stop_exit(-666);
      quitting  = true;
      YIELD_250MS;
      return;
      break;

   case SIGPIPE:
      //      SYS_INFO("SigPipe - ignore");
      break;

   default:
      ERR("Unexpected signal <%d>\n",signal);
      exit(-1);
   } // switch //

}

/**
 * @fn      void init_common_signal_handler(void)
 * @brief   signal handler to receive all interrupt signal
 ****************************************************************************/
void init_common_signal_handler(void)
{
   // install signint handler 
   struct sigaction sa;
   memset (&sa, 0, sizeof (sa));
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sa.sa_handler = &sig_handler;
   sigaction (SIGINT, &sa, NULL);

   memset (&sa, 0, sizeof (sa));
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sa.sa_handler = &sig_handler;
   sigaction (SIGTERM, &sa, NULL);

   // install sigpipe handler 
   memset (&sa, 0, sizeof (sa));
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sa.sa_handler = &sig_handler;
   sigaction (SIGPIPE, &sa, NULL);
    
   // seg fault handler 
   memset (&sa, 0, sizeof (sa));
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sa.sa_handler = &segfault_handler;
   sigaction (SIGSEGV, &sa, NULL);
}

/**
 * @fn      void convert_miliSecs(u32 ms, u32 *sec, u32 *ns); 
 * @brief   converts milisecs to secs/nanosecs 
 * @param   [in]  ms  - milisecs to convert
 * @param   [out] sec - ptr to returned seconds value
 * @param   [out] dst - ptr to returned nanosecs value
 ***************************************************************************/
void convert_miliSecs(u32 ms, u32 *sec, u32 *ns)
{
   *sec = ms / MILLISECS_PER_SEC;
   *ns  = MILLI2NANO((ms - (SECS2MILLI(*sec))));
}

/**
 * @fn      int sleepSecs(int timeWaitSecs)
 * @brief   sleeps for x seconds OR if usb is stopping...
 * @param   [in] timeWaitSecs - time to wait
 * @retval  0  == time expired
 *          -1 == USB is stopping
 ****************************************************************/
int sleepSecs(u32 timeWaitSecs)
{
   u32 i;
   for (i = 0 ; ((i <timeWaitSecs) && !common_system_state.stopping) ; i++)
   {
      YIELD_500MS;
      if (!common_system_state.stopping)
         YIELD_500MS;
   }

   if (i < timeWaitSecs )
      return -1;
   else
      return 0;
}

/**
 * @fn      bool isFileExist(const char *fullFileName)
 * @brief   determines if video file exists 
 * @param   [in] fullFileName - path/filename to file to validate if exists
 * @retval  returns (1) if exists, otherwise false (0)
 ****************************************************************************/
bool isFileExist(const char *fullFileName)
{
   if (fullFileName[0] == 0)
      return false;

   if (strstr(fullFileName,".."))
      return false;

   if ((access(fullFileName, F_OK)))
      return false;
   return true;
}

/**
 * @fn      u32 get_file_size(char *fullFileName,bool *fileExists)
 * @brief   detects if file exists and returns size
 * @param   [in]  fullFileName - filename to crc check
 * @param   [out] fileExists   - output indicating if file exists
 * @retval  Actual Filesize
 ****************************************************************************/
u32 get_file_size(char *fullFileName, bool *fileExists)
{
   struct stat buf;
   if ((!isFileExist(fullFileName)))
   {
      *fileExists  = false;
      SYS_ERR("File <%s> does NOT exist",fullFileName);
      return 0;
   }

   // indicate file exists
   *fileExists  = true;

   ///////////////////////////////////////////////////////////////////////////////////
   // Must open file to get size (because STAT doesn't work for CIFS mounted drives)
   ///////////////////////////////////////////////////////////////////////////////////
   int fd = -1;
   if ((fd = open(fullFileName,O_RDONLY)) <= 0)
   {
      SYS_ERR("Get Size - Fail Open <%s> - <%s>", fullFileName, COMMON_ERR_STR);
      return 0;
   }

   if ((fstat(fd,&buf)))
   {
      SYS_ERR("Get Size - Fail fstat (%d) of  <%s> - <%s>", fd, fullFileName, COMMON_ERR_STR);
      buf.st_size = 0;
   }

   close(fd);
   return ((u32)buf.st_size);
}

/**
 * @fn      unsigned char calculate_array_crc(u32 size, u8 *array)
 * @brief   Does a Checksum of an array of characters and returns two's complement CRC
 * 
 * @param	[in] size  - size of array
 * @param        [in] array - array to be checksumed 
 *
 * @retval 	2's complement of the checksum of the received message
 ****************************************************************************/
u8 calculate_array_crc(u32 size, u8 *array)
{
   char chksum;
   u8   accum=0;
   u32  i;

   for (i = 0 ; i < size ; i++)
      accum += array[i];
   chksum = ~((char)accum) + 1;
   return (u8)chksum;
}

/**
 * @fn      int fileCopy(const char *to, const char *from, bool *busyFlag, u8 *watchdogCounter)
 * @brief   copies file to new file
 * @param   [in] to              - new file to create
 * @param   [in] from            - original file being copied
 * @param   [in] busyFlag        - flag to set when actually doing copy 
 * @param   [in] watchdogCounter - pointer to counter to reset
 * @retval  0 == success, otherwise, error
 * @note    Assumes full file name is being passed into it
 * @note    counter & busyflag can be NULL        
 ****************************************************************************/
int fileCopy(const char *to, const char *from, bool *busyFlag, u8 *watchdogCounter)
{
   int status = 0, in, out, p[2];
   //    bool wasInPush = false;
 
   // open input
   RESET_POINTER_VALUE(watchdogCounter);
   SET_POINTER_VALUE(busyFlag);
   if ((in = open(from, O_RDONLY)) <= 0)
   {
      SYS_ERR("File Copy Open From (%s) Error: %s",from,COMMON_ERR_STR);
      status = -1;
      goto exit_copy;
   }

   // try a few times to see
   if ((out = creat(to,S_IRWXU | S_IRWXG | S_IRWXO )) <= 0)
   {
      SYS_ERR("Failed to open destination file (%s) - %s",to,COMMON_ERR_STR);
        
      close(in);
      status = -2;
      goto exit_copy;
   }

   // Create a pipe for splicing
   if ((pipe(p)))
   {
      SYS_ERR("Failed File Copy Pipe: %s",COMMON_ERR_STR);
      close(in);
      close(out);
      status = -3;
      goto exit_copy;
   }

   if (imsErrorInjection)
   {
      if (imsErrorInjection == 1)
      {
         DBGF("\tStalling\n");
         ENTER2CONTINUE;
      }
      imsErrorInjection--;
   }

   // do optimized copy //
   int i = 0;
   while(SPLICER(p[0], out, SPLICER(in, p[1], splicer_buffer))>0)
   {
      if (!(++i % splicer_index)) 
      {
         RESET_POINTER_VALUE(watchdogCounter);
         SLEEP_uS(splicer_time);
      }
   }

   RESET_POINTER_VALUE(watchdogCounter);
   fsync(out);
   RESET_POINTER_VALUE(watchdogCounter);
   close(in);
   close(p[0]);
   close(p[1]);
   close(out);
   RESET_POINTER_VALUE(watchdogCounter);
   FLUSH_SLEEP;
   RESET_POINTER_VALUE(watchdogCounter);
   RESET_POINTER_VALUE(busyFlag);
exit_copy:
   RESET_POINTER_VALUE(watchdogCounter);
   RESET_POINTER_VALUE(busyFlag);
   return status;
}

/**
 * @fn      int makeDirectory(const char *directory)
 * @brief   creates directory at requested location
 * @param   [in] directory - directory to create
 * @retval  0 == success, otherwise error
 * @note    assumes parameters are full path names
 ****************************************************************************/
int makeDirectory(const char *directory)
{
   int status;
    
   // build full directory name
   if (isFileExist((char *)directory))
   {
      SYS_INFO("%s already exists",directory);
      return 0;
   }

   // validate directory 
   if (strstr(directory,".."))
   {
      SYS_ERR("Invalid Directory <%s> ('/../') to create",directory);
      return -2;
   }

   // perform make //
   if ((status = mkdir(directory, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)))
   {
      SYS_ERR("Creating directory <%s>: <%s>",directory,COMMON_ERR_STR);
      if (errno == EEXIST)
         return 0;
   }
   return status;
}

/**
 * @fn      int dirCopy(const char *to, const char *from, bool *busyFlag, u8 *watchdogCounter)
 * @brief   copies directory of files to new directory
 * @param   [in] to              - directory to be copied to
 * @param   [in] from            - directory to be copied from
 * @param   [in] busyFlag        - flag to set when actually doing copy 
 * @param   [in] watchdogCounter - pointer to counter to reset
 * @retval  0 == success, otherwise, error
 * @note    assumes parameters are full path names
 * @note    watchdog counter and busy flag can be NULL
 ****************************************************************************/
int  dirCopy (const char *to, const char *from, bool *busyFlag, u8 *watchdogCounter)
{
   char cmdLine[DEFAULT_COMMAND_LINE_SIZE1];
   reset_cmdline_array(cmdLine);
   snprintf(cmdLine,DEFAULT_COMMAND_LINE_SIZE, "cp -rf \"%s/* %s/.\"",from, to);
   if ((system(cmdLine)))
      ERR("Error copying <%s> to <%s> - %s (%p)(%p)\n", from, to, COMMON_ERR_STR, busyFlag,watchdogCounter);
   return 0;
}

/**
 * @fn      int dirDelete(const char *delDir)
 * @brief   Deletes all files in directory (and delete directory)
 * @param   [in] dir        - full Path name to be deleted
 * @retval  0 == success, otherwise, error
 * @note    assumes parameters are full path names
 ****************************************************************************/
int dirDelete(const char *delDir)
{
   char cmdLine[DEFAULT_COMMAND_LINE_SIZE1];
   reset_cmdline_array(cmdLine);
   snprintf(cmdLine,DEFAULT_COMMAND_LINE_SIZE, "rm -rf \"%s\"",delDir);
   if ((system(cmdLine)))
   {
      ERR("Error Deleting <%s>(%s) - %s\n", delDir, cmdLine, COMMON_ERR_STR);
      return -1;
   }
   return 0;
}
/**
 * @fn      int fileMove(const char *to, const char *from, bool *busyFlag, u8 *watchdogCounter)
 * @brief   copies file to new file and deletes old one
 * @param   [in] to              - new file to create
 * @param   [in] from            - original file being copied
 * @param   [in] busyFlag        - flag to set when actually doing copy 
 * @param   [in] watchdogCounter - pointer to counter to reset
 * @retval  0 == success, otherwise, error
 * @note    assumes parameters are full path names
 * @note    busy flag & watchdog counter can be NULL
 ****************************************************************************/
int fileMove(const char *to, const char *from, bool *busyFlag, u8 *watchdogCounter)
{
   bool exists;
   u32  fileFromSize = get_file_size((char *)from, &exists);

   if (!exists || (fileFromSize == 0))
   {
      SYS_ERR("Failing to Access 'From' File <%s>",from);
      return -1;
   }

   // copy file 
   if ((fileCopy(to, from, busyFlag, watchdogCounter)))
   {
      SYS_ERR("FM: Failed Copy File to <%s>(%u)",to,fileFromSize);
      return -2;
   }

   // now delete old file
   if ((remove(from)))
   {
      SYS_CRIT("Trying Delete file <%s> : <%s> - we may have temp files saved?",from, COMMON_ERR_STR);
      return -5;
   }
   return 0;
}

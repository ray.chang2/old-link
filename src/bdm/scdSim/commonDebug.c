/**
 * @file commonDebug.c
 * @brief all debug related processing
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 5/25/13
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   5/25/13   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>

#include "commonTypes.h"
#include "commonState.h"
#include "connUtils.h"
#include "commonLog.h"
#include "commonDebug.h"

/**
 * @fn      void usb_stop_exit(int exit_code)
 * @brief   displays current status and then exits (if appropriate) 
 ****************************************************************/
static bool stop_exit = false;
void common_stop_exit(int exit_code)
{
   char exitr[32];

   // check if we have called this already //
   if (stop_exit)
      return;

   stop_exit = true;
   bzero(exitr,sizeof(exitr));

   snprintf(exitr,sizeof(exitr),"EXITING(%d)",exit_code);
   YIELD_250MS;;
   _exit(-1);
}

/////////////////////////////////////////////
// Valid when CLI, Tablet, or RTOS Enabled //
/////////////////////////////////////////////
/**
 * local variables used for display external status
 */
static display_unique_status_t displayExternalStatus = NULL;
static bool *hbChange = NULL;
static bool registered_status = false;

/**
 * @fn     void flushStdIn(void)
 * @brief  flushes stdIn for cli
 * @param  n/a
 ***********************************************************************/
static void flushStdIn(void)
{
   int c; 
   while (1)
   {
      c = getchar();
      if (c == '\n')
         return;
      else if (c == EOF)
         return;
   }
}

/**
 * @fn      int wait_for_user(bool dummy_read, bool doNotDisplay)
 * @brief   waits for input
 * @param   [in] dummy_read   - read the input because we do NOT care about it
 * @param   [in] doNotDisplay - do NOT display periodic HB Status
 * @retval  0 == success, otherwise, error and stopping
 ****************************************************************************/
int wait_for_user(bool dummy_read, bool doNotDisplay)
{
   u32 i = 0;
   struct pollfd ufds[1];      
   ufds[0].fd = 0;
   ufds[0].events = POLLIN;
   int rv;
     
   while(!common_system_state.stopping)
   {
      switch ((rv = poll(ufds,1,1000)))
      {
      case -1:
         SYS_INFO("Pollin stdin error - %s",COMMON_ERR_STR);
         return -1;
         break;  // error

      case 0:
         break; // timeout 
	     
      default:
         if (dummy_read)
            flushStdIn();
         return 0;
         break; // got something //
      } // end switch //
	 
      // do display if appropriate //

      if (registered_status)
      {
         if (!(i++ % 20) || *hbChange)
         {
            if (!doNotDisplay)
            {
               DBG("\n");
               //     display_common_usb_caps();
               if (displayExternalStatus)
                  displayExternalStatus();
            } // if OK to display //
            FLUSH_SLEEP; 
         } // if time to do something or something change
         *hbChange = false;
      } // if registered to display status
   }   // end while 
     
   return -3;
}

/**
 * @fn      int yes_not_no(const char *prompt, bool *resp)
 * @brief   Queries User for new password, and sets into the common_system_state accordingly
 * @param   [in] prompt - prompt the user for yes/no
 * @param   [out] resp  - value of the response
 * @retval  returns 0 on success, otherwise, it error
 ****************************************************************************/
int yes_not_no(const char *prompt, bool *resp)
{
   char yesNo[4];
   char promptBuf[2*DEFAULT_FULLNAME_SIZE];
   reset_array(promptBuf,sizeof(promptBuf));
   snprintf(promptBuf,sizeof(promptBuf),"%s(default is yes)",prompt);
   if ((getString(promptBuf,yesNo,sizeof(yesNo))))
      return -1;

   // set default value (true) and determine actual selection
   *resp = true;
   if (yesNo[0] == 'n')
      *resp = false;
   return 0;
}

/**
 * @fn      int privateGetString(const char *prompt, char *copyTo, u8 maxSize, bool doNotDisplay)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt      - prompt for the string 
 * @param   [out] copyTo      - location to copy string to
 * @param   [in/out] maxSize  - maxSize of copyTo 
 * @param   [in] doNotDisplay - display status while waiting
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
static int privateGetString(const char *prompt, char *copyTo, u8 maxSize, bool doNotDisplay)
{
   char *userInput = (char *)alloca(maxSize+1);
   char *result = NULL;
   u32 i;

tryPrivateStringAgain:

   // reset userInput
   reset_array(copyTo,maxSize);
   DBGF("\n%s\n >  ",prompt);

   if ((wait_for_user(false,doNotDisplay)))
      return -1;

   // read data in //
   if ((result = fgets(userInput, maxSize, stdin)) == NULL)
   {
      perror("fgets");
      ERR("Failed to Read Input - Lets try that again\n");
      ENTER2CONTINUE;
      goto tryPrivateStringAgain;
   }

   // remove the '\n' and copy string over //
   char *fn = userInput;
   strsep(&fn,"\n");
   for (i = 0 ; i < strlen(userInput) ; i++)
   {
      int b = (int)userInput[i];
      if ((isalnum(b)))
         continue;
      else if ((userInput[i] == '_') ||(userInput[i] == '-'))
         continue;

      ERR("Illegal Character [%d]<%c> in input <%s> - only Alpha Numeric Character (or '_'/'-') are accepted\n",i, b, userInput);
      ENTER2CONTINUE;
      goto tryPrivateStringAgain;
   }

   strncpy(copyTo,userInput,maxSize);
   return 0;
}

/**
 * @fn      int privateGetFullFileNameString(const char *prompt, char *copyTo, u8 maxSize, bool doNotDisplay)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt      - prompt for the string 
 * @param   [out] copyTo      - location to copy string to
 * @param   [in/out] maxSize  - maxSize of copyTo 
 * @param   [in] doNotDisplay - display status while waiting
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
static int privateGetFullFileNameString(const char *prompt, char *copyTo, u8 maxSize, bool doNotDisplay)
{
   char *userInput = (char *)alloca(maxSize+1);
   char *result = NULL;
   u32 i;

tryPrivateStringAgain:

   // reset userInput
   reset_array(copyTo,maxSize);
   DBGF("\n%s\n >  ",prompt);

   if ((wait_for_user(false,doNotDisplay)))
      return -1;

   // read data in //
   if ((result = fgets(userInput, maxSize, stdin)) == NULL)
   {
      perror("fgets");
      ERR("Failed to Read Input - Lets try that again\n");
      ENTER2CONTINUE;
      goto tryPrivateStringAgain;
   }

   // remove the '\n' and copy string over //
   char *fn = userInput;
   strsep(&fn,"\n");
   for (i = 0 ; i < strlen(userInput) ; i++)
   {
      int b = (int)userInput[i];
      if ((isalnum(b)))
         continue;
      else if ((userInput[i] == '_') ||(userInput[i] == '-') ||(userInput[i] == '.'))
         continue;

      ERR("Illegal Character [%d]<%c> in input <%s> - only Alpha Numeric Character (or '_'/'-') are accepted\n",i, b, userInput);
      ENTER2CONTINUE;
      goto tryPrivateStringAgain;
   }

   strncpy(copyTo,userInput,maxSize);
   return 0;
}

/**
 * @fn      int getString(const char *prompt, char *copyTo, u8 maxSize)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt     - prompt for the string 
 * @param   [out] copyTo     - location to copy string to
 * @param   [in/out] maxSize - maxSize of copyTo 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
int getString(const char *prompt, char *copyTo, u8 maxSize)
{
   return (privateGetString(prompt, copyTo, maxSize, true));
}

/**
 * @fn      int getString(const char *prompt, char *copyTo, u8 maxSize)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt     - prompt for the string 
 * @param   [out] copyTo     - location to copy string to
 * @param   [in/out] maxSize - maxSize of copyTo 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
int getFullNameString(const char *prompt, char *copyTo, u32 maxSize)
{
   return (privateGetFullFileNameString(prompt, copyTo, maxSize, true));
}


/**
 * @fn      int getString2(const char *prompt, char *copyTo, u8 maxSize)
 * @brief   retrieves a string and places it in the where (no '?'
 * @param   [in]  prompt     - prompt for the string 
 * @param   [out] copyTo     - location to copy string to
 * @param   [in/out] maxSize - maxSize of copyTo 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
int getString2(const char *prompt, char *copyTo, u8 maxSize)
{
   return (privateGetString(prompt, copyTo, maxSize, false));
}

/**
 * @fn      int getInteger(char *prompt, int *outInt)
 * @brief   prompts user for integer and returns with status (and number in 'out')
 * @param   [in]  prompt - prompt for integer retrieval
 * @param   [out] outInt - Integer retrieved
 * @retval  returns 0 on success, otherwise, it error
 ****************************************************************************/
int getInteger(char *prompt, int *outInt)
{
   char integer[16];
   u32 i,j;
   int k = -1;
tryAgainInteger:
   // reset result - just to 
   reset_array(integer,16);
   *outInt = -1;
     
   // get string with prompt //
   getString(prompt,integer,sizeof(integer));

   for (i = 0 ; i < strlen(integer) ; i++)
   {
      j = (int)integer[i];
      if ((isdigit(j)))
         continue;

      ERR("Invalid character [%d]<%c> in Integer(%s) - Lets Try that Again\n",i, j, integer);
      ENTER2CONTINUE;
      goto tryAgainInteger;
   }	 

   // convert //
   *outInt = strtoul(integer, NULL,10);
   if (*outInt == k)
   {
      perror("strtoul");
      ERR("Invalid Decimal Integer(%s) - Lets Try that Again\n",integer);
      ENTER2CONTINUE;
      goto tryAgainInteger;
   }
   return 0;
}

/**
 * @fn      int setLogLevel(COMMON_LOG_COMPONENTS component)
 * @brief   debug cli interface to change log levels
 * @param   [in] component - component to change 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
static int setLogLevel(COMMON_LOG_COMPONENTS component)
{
   bool yesNotNo;
   COMMON_LOG_TYPES type;
   LOG_LEVELS level = COMMON_LOG_EMERGENCY;
   const char *componentName = getLogComponentString(component);
   const char *typeName ;
   static char *promptL = "\t\t0) EMERGENCY\n"
      "\t\t1) ALERT\n"
      "\t\t2) CRITICAL\n"
      "\t\t3) ERROR\n"
      "\t\t4) WARNING\n"
      "\t\t5) NOTICE\n"
      "\t\t6) INFO\n"
      "\t\t7) DEBUG\n";

    
   //////////////////////////////////////////
   // check if we want to change component //
   //////////////////////////////////////////
   DBG("You have selected %s. The Current Log Levels are:\n"
       "\tConsole: %s\n"
       "\tFile:    %s\n\n",
       componentName,
       commonGetDisplayLogLevel(component,COMMON_LOG_CONSOLE_TYPE),
       commonGetDisplayLogLevel(component,COMMON_LOG_FILE_TYPE));

   if ((yes_not_no("Do you REALLY want to Change Log Levels",&yesNotNo)))
      return -1; // error return
   else if (!yesNotNo)
   {
      DBG("OK, no harm, no foul\n");
      return 0;
   }

   //////////////////////////////////////////
   // get log type (Console or File Type)  //
   //////////////////////////////////////////
   if((yes_not_no("Change Console LogLevel (default==y) or File LogLevel(n)",&yesNotNo)))
      return -2;

   if (yesNotNo)
      type = COMMON_LOG_CONSOLE_TYPE;
   else
      type = COMMON_LOG_FILE_TYPE;

   typeName = getLogTypeString(type);

   ////////////////////////////////
   // get log level to change to //
   ////////////////////////////////
   int logLevel;
   SYS_NOTE("Change %s's %s Log Level to",componentName,typeName);
   if((getInteger(promptL,&logLevel)))
      return -3;
   switch (logLevel)
   {
   case 0:
      level = COMMON_LOG_EMERGENCY;
      break;
   case 1:
      level = COMMON_LOG_ALERT;
      break;
   case 2:
      level = COMMON_LOG_CRITICAL;
      break;
   case 3:
      level = COMMON_LOG_ERROR;
      break;
   case 4:
      level = COMMON_LOG_WARNING;
      break;
   case 5:
      level = COMMON_LOG_NOTICE;
      break;
   case 6:
      level = COMMON_LOG_INFO;
      break;
   case 7:
      level = COMMON_LOG_DEBUG;
      break;
   default:
      SYS_ERR("Unknown Log Level <%d>",level);
      return -4;
   }

   if (commonSetLogLevel(component, level, type))
   {
      SYS_ERR("%s:%s: Failed to Change Log Level (%d)",componentName,typeName,level);
      sleep(1);
      return -5;
   }

   SYS_INFO("Successfully Change Log Level");
   sleep(1);
   return 0;
}

/**
 * @fn      int cli_getSetLogLevel(u8 component)
 * @brief   public cli interface for modifying log level for comp
 * @param   [in] component - component to change 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
int cli_setLogLevel(u8 component)
{
   return (setLogLevel((COMMON_LOG_COMPONENTS)component));
}


/**
 * @fn     void register_display_common(display_unique_status_t displayExtFn, bool *changeHB)
 * @brief  registers callbacks for common display caps
 * @param  [in] displayExtFn - function ptr to be called when display_common_caps
 * @param  [in] changeHB     - ptr to flag indicating a change in HB Status
 ****************************************************************************/
void register_display_common(display_unique_status_t displayExtFn, bool *changeHB)
{
   hbChange = changeHB;
   displayExternalStatus = displayExtFn;
   registered_status = true;
}

/**
 * @fn      void display_common_usb_caps(void)
 * @brief   debug display of default caps
 ****************************************************************************/
void display_common_usb_caps(void)
{
}

/**
 * @fn      int getStringStatus(const char *prompt, char *copyTo, u8 maxSize)
 * @brief   retrieves a string and places it in the where
 * @param   [in]  prompt     - prompt for the string 
 * @param   [out] copyTo     - location to copy string to
 * @param   [in/out] maxSize - maxSize of copyTo 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
int getStringStatus(const char *prompt, char *copyTo, u8 maxSize)
{
   return (privateGetString(prompt, copyTo, maxSize, false));
}

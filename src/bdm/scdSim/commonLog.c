/**
 * @file commonLog.c
 * @brief all logging related processing
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 8/25/18
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   8/25/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <stdarg.h>
#include <assert.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>
#include "commonTypes.h"
#include "commonState.h"
#include "connUtils.h"
#include "commonLog.h"
#include "commonSysUtils.h"
#include "msgD2.h"

static periodic_info_t log_info = {0,0,0,0};              /**< watchdog timer information          */
/**
 * local/private variables 
 */
static LOG_LEVELS commonLogLevel[MAX_LOG_TYPE][COMMON_MAX_COMPONENTS] = {{COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL }, 
                                                                         {COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL }};
static LOG_LEVELS maxLogLevel[COMMON_MAX_COMPONENTS]                = {COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL, COMMON_INIT_LOG_LEVEL };
static pthread_t monitorThread;
static bool common_log_init_done  = false; /**< flag to indicate logging has been started    */
static bool common_log_closed     = false; /**< flag to indicate logging has been terminated */

bool early_log_printf_log      = false;  /**< flag to indicate we should print to screen   */
bool early_log_printf_err      = true;  /**< flag to indicate we should print to screen   */
bool log_with_time_file        = false; /**< log variable */
bool log_with_time_console     = false; /**< log variable */

/**
 * used to contain log info
 */
typedef struct log_struct {                        /**< define log control structure      */
   u32   numArchived;                               /**< number of past archive files      */
   u32   fflusher;                                  /**< counter to force fflush           */
   char *logFileName;                               /**< base log file name used           */
   char  logTempFileName[DEFAULT_FULLNAME_SIZE21];  /**< full log filename/path            */
   char  logFullFileName[DEFAULT_FULLNAME_SIZE21];  /**< full log filename/path            */
   char  pd2[2];                                    /**< preserve 32 bit alignment         */
   char *componentName;                             /**< Component being logged            */
   FILE *fHandle;                                   /**< file Handle used to write         */
   int   fdHandle;                                  /**< file descriptor for file (sync)   */
   pthread_mutex_t fileLock;                        /**< lock for multi-thread op          */
} log_struct_t,                                    /**< typedef of log control struct     */
   *pLog;                                           /**< typedef ptr to log control struct */

/**
 * actual container structure
 */
static log_struct_t logger[COMMON_MAX_COMPONENTS];

/**
 * @fn     void change_insertion_time_in_log_entry(void)
 * @brief  prompts and changes log time insertion into log entries
 ****************************************************************************/
void change_insertion_time_in_log_entry(void)
{
   char choice[4];
   char promptBuf[DEFAULT_COMMAND_LINE_SIZE1];
   reset_array(promptBuf,sizeof(promptBuf));
   snprintf(promptBuf,sizeof(promptBuf),"Which Log Destination to Change\n"
            "\t1) Console - Currently %s Insert Time\n"
            "\t2) File    - Currently %s Insert Time",
            log_with_time_console ? "Does":"Does Not", 
            log_with_time_file    ? "Does":"Does Not");

   // get choice //
   if ((getString(promptBuf,choice,sizeof(choice))))
   {
      ERR("Error in Getting Choice\n");
      return;
   }

   // set default value (true) and determine actual selection
   switch (choice[0])
   {
   case '1':
      if (log_with_time_console)
         log_with_time_console = 0;
      else
         log_with_time_console = 1;
      break;

   case '2':
      if (log_with_time_file)
         log_with_time_file = 0;
      else
         log_with_time_file = 1;
      break;

   default:
      DBG("Unknown Option (%c) - ignore\n",choice[0]);
      break;
   }
}

/**
 * @fn      const char *getLogTypeString(COMMON_LOG_TYPES type)
 * @brief   debug returns log Type String for printing
 * @param   [in] type - type to get display for
 * @retval  log type string
 ****************************************************************************/
const char *getLogTypeString(COMMON_LOG_TYPES type)
{
   switch (type)
   {
   case COMMON_LOG_FILE_TYPE:     return "File";
   case COMMON_LOG_CONSOLE_TYPE:  return "Console";
   default:                    return "Unknown Type";
   } // end switch //
}

/**
 * @fn      const char *getLogComponentString(COMMON_LOG_COMPONENTS component)
 * @brief   returns the component name string for printing
 * @param   [in] component - component we are getting the name for 
 * @retval  component name string
 * @note    Semi-private function
 ****************************************************************************/
const char *getLogComponentString(COMMON_LOG_COMPONENTS component)
{
   switch(component)
   {
   case COMMON_UCM:  return "UCM";
   case COMMON_RCM:  return "RCM";
   case COMMON_TCM:  return "TCM";
   case COMMON_SYS:  return "SYS";
   default:       return "Invalid Source Component";
   } // end switch */
}

/**
 * @fn      const char *getLogLevelString(LOG_LEVELS level)
 * @brief   returns log level String printing
 * @param   [in] level  - level to display
 * @retval  log level string
 ****************************************************************************/
const char *getLogLevelString(LOG_LEVELS level)
{
   switch (level)
   {
   case COMMON_LOG_EMERGENCY:  return "EMERGENCY Log Level";
   case COMMON_LOG_ALERT:      return "ALERT Log Level";
   case COMMON_LOG_CRITICAL:   return "CRITICAL Log Level";
   case COMMON_LOG_ERROR:      return "ERROR Log Level";
   case COMMON_LOG_WARNING:    return "WARNING Log Level";
   case COMMON_LOG_NOTICE:     return "NOTICE Log Level";
   case COMMON_LOG_INFO:       return "INFO Log Level";
   case COMMON_LOG_DEBUG:      return "DEBUG Log Level";
   default:                 return "UNKNOWN LOG LEVEL";
   }
}

/**
 * @fn      const char *getLevelString(LOG_LEVELS level)
 * @brief   returns level String printing
 * @param   [in] level  - level to display
 * @retval  log level string
 ****************************************************************************/
static const char *getLevelString(LOG_LEVELS level)
{
   switch (level)
   {
   case COMMON_LOG_EMERGENCY:  return "EMERGENCY";
   case COMMON_LOG_CRITICAL:   return "CRITICAL";
   case COMMON_LOG_INFO:       return "INFO ";
   case COMMON_LOG_ALERT:      return "ALERT";
   case COMMON_LOG_DEBUG:      return "DEBUG";
   case COMMON_LOG_ERROR:      return "ERROR";
   case COMMON_LOG_NOTICE:     return "NOTICE";
   case COMMON_LOG_WARNING:    return "WARNING";
   default:                 return "UNKNOWN";
   }
}

/**
 * @fn      bool validateLogParams(COMMON_LOG_COMPONENTS component, COMMON_LOG_TYPES type, LOG_LEVELS level, const char *from)
 * @brief   validates the input component, type, & level
 * @param   [in] component - log component
 * @param   [in] type      - log file type (file or console)
 * @param   [in] level     - log level
 * @param   [in] from      - debug printf from
 * @retval - 
 ****************************************************************************/
bool validateLogParams(COMMON_LOG_COMPONENTS component, COMMON_LOG_TYPES type, LOG_LEVELS level, const char *from)
{
   int status = 0;

   // validate component 
   switch(component)
   {
   case COMMON_UCM:
   case COMMON_RCM:
   case COMMON_TCM:
   case COMMON_SYS:
      break;
   default:       
      ERR("<%s>: Unknown Component<%d> ",from, component);
      status++;
      break;
   } // end switch */

   // validate type
   switch (type)
   {
   case COMMON_LOG_FILE_TYPE:     
   case COMMON_LOG_CONSOLE_TYPE: 
      break;
   default:
      if (status)
      {
         ERR(" Unknown Type (%d)",type);
      }
      else
         ERR("<%s> Unknown Type (%d)",from,type);
      status++;

      break;
   }

   // validate level //
   switch (level)
   {
   case COMMON_LOG_EMERGENCY:
   case COMMON_LOG_ALERT:    
   case COMMON_LOG_CRITICAL: 
   case COMMON_LOG_ERROR:    
   case COMMON_LOG_WARNING:  
   case COMMON_LOG_NOTICE:   
   case COMMON_LOG_INFO:     
   case COMMON_LOG_DEBUG:    
      break;
   default:               
      if (status)
      {
         ERR(" Unknown Level (%d)",level);
      }
      else
         ERR("<%s> Unknown Level (%d)",from,type);
      status++;
      break;
   }

   // if errors - indicate that
   if (status)
   {
      ERR(" <%d> Total Errors\n",status);
      return false;
   }

   // valid 
   return true;
}
/**
 * @fn      const char *usbGetDisplayLogLevel(USB_LOG_COMPONENTS component, USB_LOG_TYPES type)
 * @brief   Displays log level of console
 * @param   [in] component - component to get log level for
 * @param   [in] type      - log file type (file or console)
 ****************************************************************************/
const char *commonGetDisplayLogLevel(COMMON_LOG_COMPONENTS component, COMMON_LOG_TYPES type)
{
   // validate input parameters
   if (!(validateLogParams(component, type, COMMON_LOG_DEBUG, __FUNCTION__)))
      return "Invalid Input into commonGetDisplayLogLevel";

   // if console 
   return (getLogLevelString(commonLogLevel[type][component]));
}

/**
 * @fn      int commonSetLogLevel(COMMON_LOG_COMPONENTS component, LOG_LEVELS level, COMMON_LOG_TYPES type)
 * @brief   Sets the log level for the caller 
 * @param   [in] component - component to get log level for
 * @param   [in] level  - valid log level
 * @param   [in] type   - log file type (file or console)
 ****************************************************************************/
int commonSetLogLevel(COMMON_LOG_COMPONENTS component, LOG_LEVELS level, COMMON_LOG_TYPES type)
{
   // validate input parameters
   if (!(validateLogParams(component, type, level, __FUNCTION__)))
      return -1;

   // keep old one - for debug, and set new one //
   LOG_LEVELS oldLevel = commonLogLevel[type][component];
   commonLogLevel[type][component] = level;

   // debug output
   DBG("Set Log Level for Component <%s> Type <%s> from <%s> to <%s>\n", 
       getLogComponentString(component),
       getLogTypeString(type), 
       getLogLevelString(oldLevel), 
       getLogLevelString(level));

   if (commonLogLevel[COMMON_LOG_FILE_TYPE][component] >=  commonLogLevel[COMMON_LOG_CONSOLE_TYPE][component])
      maxLogLevel[component] = commonLogLevel[COMMON_LOG_FILE_TYPE][component];
   else
      maxLogLevel[component] = commonLogLevel[COMMON_LOG_CONSOLE_TYPE][component];

   return 0;
}


/**
 * @fn      static int common_init_log_locks(pthread_mutex_t *lock, char *prompt)
 * @brief   sets the log level for file output
 * @param   [in] lock   - pointer to lock to be initialized
 * @param   [in] prompt - debug callee
 * @retval  0 == success, -1 == error
 ****************************************************************************/
static int common_init_log_locks(pthread_mutex_t *lock, char *prompt)
{
   if ((pthread_mutex_init(lock,NULL)))
   {
      ERR("error in init of %s: <%s>\n",prompt,COMMON_ERR_STR);
      return -1;
   }
   return 0;
}

/**
 * @fn      void common_log_lock_all(void)
 * @brief   locks and flushes all open log files
 ****************************************************************************/
static void common_log_lock_all(void)
{
   int i;
   for (i = COMMON_UCM ; i < COMMON_MAX_COMPONENTS ; i++)
   {
      pthread_mutex_lock(&logger[i].fileLock);
      fsync(logger[i].fdHandle);
   }
}

/**
 * @fn      void common_log_unlock_all(void)
 * @brief   unlocks logs
 ****************************************************************************/
static void common_log_unlock_all(void)
{
   int i;
   for (i = COMMON_UCM ; i < COMMON_MAX_COMPONENTS ; i++)
      pthread_mutex_unlock(&logger[i].fileLock);
}

/**
 * @fn      int common_open_log_file(pLog pHandle,char *fullFileName)
 * @brief   truncates/opens log file requested
 * @param   [in] pHandle - pointer to log struct 
 * @param   [in] fullFileName - file to be opened
 * @retval  0 == success, -1 == error
 ****************************************************************************/
static int common_open_log_file(pLog pHandle,char *fullFileName)
{
   if ((pHandle->fHandle = fopen(fullFileName, "wb")) == NULL)
   {
      ERR("Error Opening Log File <%s>: <%s>\n",fullFileName,COMMON_ERR_STR);
      return -1;
   }
   if ((pHandle->fdHandle = fileno(pHandle->fHandle)) == -1)
   {
      ERR("<%s> - fileno error %s\n",fullFileName,COMMON_ERR_STR);
      return -2;
   }    
   return 0;
}

/**
 * @fn      void archive_usb_log(char *archiveDirectory, char *fileExtension, char **fileName, bool includeAll)
 * @brief   archives all current logs
 * @param   [in]  archiveDirectory - directory to generate log file to (if NULL --> then just in /tmp directory)
 * @param   [in]  fileExtension    - file extension to be put at end of file (random value if NULL)
 * @param   [out] fileName         - file name created by this log file (can be NULL)
 * @param   [in]  includeAll       - indicates inclusion of all logs - not just current
 * @retval  0 == success, otherwise, error
 * @note:   fileName size assumed to be at least 128bytes
 ****************************************************************************/
static void archive_usb_log(char *archiveDirectory, char *fileExtension, char **fileName, bool includeAll)
{
   int i;
   bool doArchive = false;
   char fileTo[DEFAULT_FULLNAME_SIZE21];
   char cmdLine[DEFAULT_COMMAND_LINE_SIZE1];
   reset_array(fileTo,sizeof(fileTo));
   reset_array(cmdLine, sizeof(cmdLine));

   // make sure directory is made
   if ((!isFileExist(TEMP_ARCHIVE_LOG_PATH)))
      makeDirectory(TEMP_ARCHIVE_LOG_PATH);

   // lock & flush //
   common_log_lock_all();

   // validate
   if ((!isFileExist(LOG_ACTIVE_PATH)))
   {
      ERR("No Log Directory <%s>\n",LOG_ACTIVE_PATH);
      goto exit_archive;
   }

   // test if anything there //
   snprintf(fileTo, DEFAULT_FULLNAME_SIZE2, "%s/usb_ucm.log",LOG_ACTIVE_PATH);
   if ((isFileExist(fileTo)))
   {
      doArchive = true;
      // copy all current files into archive directory
      for (i = COMMON_UCM ; i < COMMON_MAX_COMPONENTS ; i++)
      {
         if ((isFileExist(logger[i].logFullFileName)))
         {
            snprintf(fileTo,sizeof(fileTo),"%s/%s", TEMP_ARCHIVE_LOG_PATH, logger[i].logFileName);
            fileCopy(fileTo,logger[i].logFullFileName, NULL, NULL);
         }
      }
   } // copy standard logs //

   // copy all temp archived files //
   if ((isFileExist(LOG_ARCHIVE_PATH)))
      dirCopy(TEMP_ARCHIVE_LOG_PATH, LOG_ARCHIVE_PATH, NULL, NULL);
      
   // if indicate - copy old archived log files
   if (includeAll && (isFileExist(LOG_OLD_ARCHIVE_PATH)))
   {
      doArchive = true;
      dirCopy(TEMP_ARCHIVE_LOG_PATH, LOG_OLD_ARCHIVE_PATH, NULL, NULL);
   }

   if (!doArchive)
   {
      SYS_NOTE("Nothing to Archive");
      return;
   }

   // determine the full file name 
   char logFileName[DEFAULT_FULLNAME_SIZE21];
   if (fileExtension == NULL)
   {
      struct timeval tv;
      gettimeofday(&tv,NULL);
      snprintf(logFileName, DEFAULT_FULLNAME_SIZE2, "commonLogArchive.tgz.%08X",(u32)tv.tv_usec);
   }
   else
      snprintf(logFileName, DEFAULT_FULLNAME_SIZE2, "commonLogArchive.tgz.%s",fileExtension);

   // format full command line and execute
   if (archiveDirectory == NULL) 
      snprintf(cmdLine,DEFAULT_COMMAND_LINE_SIZE,"tar -czf /tmp/%s -C /tmp %s 2&>1 >/dev/null", logFileName,  TEMP_ARCHIVE_DIR);
   else
      snprintf(cmdLine,DEFAULT_COMMAND_LINE_SIZE,"tar -czf %s/%s   -C /tmp %s 2&>1 >/dev/null", archiveDirectory, logFileName, TEMP_ARCHIVE_DIR);

   if (system(cmdLine))
      ERR("Failed actual archive of log files\n");

   // determine if copy to filename
   if (fileName)
   {
      char *p = *fileName;
      if (archiveDirectory == NULL)
         snprintf(p,DEFAULT_FULLNAME_SIZE2,"/tmp/%s",logFileName);
      else
         snprintf(p,DEFAULT_FULLNAME_SIZE2,"%s/%s",archiveDirectory,logFileName);
   }

   // remove 
   reset_array(cmdLine, sizeof(cmdLine));
   snprintf(cmdLine,DEFAULT_FULLNAME_SIZE2,"rm -rf %s",TEMP_ARCHIVE_LOG_PATH);
   if (system(cmdLine))
      ERR("eee> Failed to remove temp archive <eee \n");

   // unlock all - so more things can happen
exit_archive:
   common_log_unlock_all();
}


/**
 * @fn      void rotate_logFile(pLog pLogger)
 * @brief   Performs the rotating of a log file
 * @param   [in] pLogger - pointer to log file structure
 ****************************************************************************/
static void rotate_logFile(UNUSED_ATTRIBUTE pLog pLogger)
{
   return;
}

/**
 * @fn      void *commonLogMonitor(void *args)
 * @brief   monitors and rotates log files as well as flushing logs
 * @param   [in] args - not used
 * @retval  NULL - not really used
 ****************************************************************************/
static void *commonLogMonitor(UNUSED_ATTRIBUTE void *args)
{
   static u32 check_rotate_count   = 0;
   int status;
   // validate input
   if (common_log_closed || !common_log_init_done)
   {
      ERR("Entering commonLogMonitor Init/Closed (%d/%d)\n", common_log_init_done, common_log_closed);
      pthread_exit(NULL);
      return NULL;
   }

   while (!common_system_state.stopping)
   {
      // sleeps configured amount of time //
      if ((status = wait_periodic_timer(&log_info, "Logging Monitor")))
      {
         SYS_ERR("Log Monitor Wait Timer(%d) Error(%d)", log_info.timer_fd, status);
         goto exit_log_monitor;
      }

      // check if we need to flush logs explicitly to file
      int i, j = 0;
      if (!(++check_rotate_count % CHECK_LOG_ROTATE_COUNT))
      {
         // check all for 
         for (i = COMMON_UCM ; i < COMMON_MAX_COMPONENTS ; i++)
         {
            struct stat st;
            fsync(logger[i].fdHandle);
            if ((fstat(logger[i].fdHandle,&st)))
            {
               SYS_ERR("eeee> Error FStat'ing log file <%s> - %s",logger[i].logTempFileName, COMMON_ERR_STR);
               goto exit_log_monitor;
            }
            else if (st.st_size > MAX_LOG_SIZE)
            {
               SYS_NOTE("Rotating LogFile <%s> because too big <%d> > Max Size(%d)",logger[i].logFileName, (u32)st.st_size, MAX_LOG_SIZE);
               rotate_logFile(&logger[i]);
               j++;
            }
         } // end while checking all files //
		
         if (j)
            SYS_NOTE("Rotated <%d> Log files",j);
		
      } // if check rotate //
   }// while !stopping & continuing
exit_log_monitor:
   ERR("EXITING LOG Monitor(%d)\n",common_system_state.stopping);
   stop_periodic_timer(&log_info);
   common_stop_exit(11);
   pthread_exit(NULL);
   return NULL;
}
/**
 * @fn      int common_init_log(void)
 * @brief   initializes logging
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
int common_init_log(void)
{
   int i;
   char cmdLine[DEFAULT_COMMAND_LINE_SIZE1];
   u32 archiveNumber;
   int archiveFd = -1;
   reset_array(cmdLine,sizeof(cmdLine));

   // validate only once through
   if (common_log_init_done)
   {
      ERR("<%s> - Log already initialized\n",__FUNCTION__);
      return -1;
   }

   // Create a file in temp directory 
   // make sure that the path to log file is there //
   if ((!isFileExist(TEMP_LOG_WRITE_PATH)))
   {
      if ((makeDirectory(TEMP_LOG_WRITE_PATH)))
      {
         ERR("eee> Error Creating <%s> for logging<eee\n",TEMP_LOG_WRITE_PATH);
         return -2;
      }
   }

   // create archive paths accordingly
   if (1)
   {
      if ((!isFileExist(LOG_ACTIVE_PATH)))
      {
         DBG("Creating Permanent Log Directory <%s> \n",LOG_ACTIVE_PATH);
         if ((makeDirectory(LOG_ACTIVE_PATH)))
         {
            ERR("eee> Error Creating <%s> for logging<eee\n",LOG_ACTIVE_PATH);
            return -2;
         }
      }

      // make sure that the path to log file is there //
      if ((!isFileExist(LOG_OLD_ARCHIVE_PATH)))
      {
         DBG("Old Log Archive <%s> does NOT exist\n",LOG_OLD_ARCHIVE_PATH);
         if ((makeDirectory(LOG_OLD_ARCHIVE_PATH)))
            return -3;
      }

      // check 
      if ((!isFileExist(LOG_OLD_ARCHIVE_FILE)))
      {
         archiveNumber = 1;
         archiveFd =  open(LOG_OLD_ARCHIVE_FILE,O_CREAT|O_RDWR, S_IRWXO);
         if ((write(archiveFd,&archiveNumber,sizeof(archiveNumber))) <= 0)
            ERR("eee> Error initializing archive Number file");
      }
      else
      {
         // close & re-open READONLY
         archiveFd =  open(LOG_OLD_ARCHIVE_FILE,O_RDWR);
         if ((read(archiveFd,&archiveNumber,sizeof(archiveNumber))) <= 0)
            ERR("eee> Error reading archive Number file");
         archiveNumber += 1;
         lseek(archiveFd,0,SEEK_SET);
         if ((write(archiveFd,&archiveNumber,sizeof(archiveNumber))) <= 0)
            ERR("eee> Error writing archive Number into file");
      }

      close(archiveFd);


      // check for archive path - and archive to a different name
      if (isFileExist(LOG_ARCHIVE_PATH))
      {      
         snprintf(cmdLine,DEFAULT_COMMAND_LINE_SIZE,"%d",archiveNumber);
         archive_usb_log(LOG_OLD_ARCHIVE_PATH, cmdLine, NULL, false);
         reset_array(cmdLine, sizeof(cmdLine));
         snprintf(cmdLine,DEFAULT_COMMAND_LINE_SIZE,"rm -rf %s*",LOG_ARCHIVE_PATH);
         if (system(cmdLine))
            ERR("eee>init Failed to archive old files <eee \n");
      } // if archive directory exists //

        // create archive directory //
      if ((makeDirectory(LOG_ARCHIVE_PATH)))
         return -5;
   } // if SD Flash to check

   // reset struct
   bzero(logger,sizeof(logger));
    
   // setup component name (mainly for debug) 
   logger[COMMON_UCM].componentName = COMMON_UCM_COMPONENT_NAME;
   logger[COMMON_RCM].componentName = COMMON_RCM_COMPONENT_NAME;
   logger[COMMON_TCM].componentName = COMMON_TCM_COMPONENT_NAME;
   logger[COMMON_SYS].componentName = COMMON_SYS_COMPONENT_NAME;

   logger[COMMON_UCM].logFileName   = UCM_LOG_FILE;
   logger[COMMON_RCM].logFileName   = RCM_LOG_FILE;
   logger[COMMON_TCM].logFileName   = TCM_LOG_FILE;
   logger[COMMON_SYS].logFileName   = SYSTEM_LOG_FILE;

   // setup log and file handles //
   for (i = COMMON_UCM ; i < COMMON_MAX_COMPONENTS ; i++)
   {
      logger[i].fHandle  = NULL;
      logger[i].fdHandle = -1;
      snprintf(logger[i].logTempFileName,DEFAULT_FULLNAME_SIZE2, "%s/%s", TEMP_LOG_WRITE_PATH, logger[i].logFileName);
      snprintf(logger[i].logFullFileName,DEFAULT_FULLNAME_SIZE2, "%s/%s", LOG_ACTIVE_PATH,     logger[i].logFileName);
      if ((common_init_log_locks(&logger[i].fileLock, logger[i].componentName)))
         return -7;
      if ((common_open_log_file (&logger[i],  logger[i].logTempFileName)))
         return -8;
   }

   // deal with inserting time
   log_with_time_file    = true;
   log_with_time_console = false;

   // now set state accordingly //
   common_log_init_done    = true;
   early_log_printf_err = false;

   ///////////////////////////
   // Start Watchdog Thread //
   ///////////////////////////
   if ((create_periodic_timer(LOG_MONITOR_FREQ_MS, &log_info)))
      return -99;
   else if ((common_create_thread(&monitorThread, MAINT_PRIORITY, NULL, commonLogMonitor)))
   {
      SYS_ERR("Creating USB Logging Maintenance Thread");
      return -9;
   }

   SYS_NOTE("Successfully Initialized/Started Logging");
   common_log_closed = false;
   return 0;
}

/**
 * @fn      void common_shutdown_log(void)
 * @brief   Stops all logging
 ****************************************************************************/
void common_shutdown_log(void)
{
   int i;
   DBG("\n\t--->USB Log Shutdown<----\n");
   // validate only once through
   if (!common_log_init_done || common_log_closed)
   {
      ERR("<%s> - Init/Closed (%d/%d)\n",__FUNCTION__, common_log_init_done, common_log_closed);
      return;
   }

   // indicate shutdown //
   common_log_closed        = true;

   // lock all files
   common_log_lock_all();

   // loop through components and close log files //
   for (i = COMMON_UCM ; i < COMMON_MAX_COMPONENTS ; i++)
   {
      //	DBG("Shutting down log for <%s> <%p>\n",logger[i].componentName, logger[i].fHandle);
      if (logger[i].fHandle == NULL)
         continue;
      if ((fclose(logger[i].fHandle)))
         perror("\tclosing component log file");
      if ((pthread_mutex_unlock(&logger[i].fileLock)))
         perror("\tclosing & tried to lock log file");
      if ((pthread_mutex_destroy(&logger[i].fileLock)))
         perror("\tdestroying log mutex");
      logger[i].fHandle  = NULL;
      logger[i].fdHandle = -1;
   }

   DBG("Waiting for Log Monitor Thread to Stop (%d)\n",common_system_state.stopping);
   YIELD_300MS;
   if ((pthread_cancel(monitorThread)))
   {
      if (!common_system_state.stopping)
         perror("\tError Cancel Thread");
   }

   DBG("Waiting for Log Monitor Thread Exit\n");
   pthread_join(monitorThread,NULL);
   DBG("Monitor Thread has exited - USB Log is shutdown\n");
   common_log_init_done = false;
}


/**
 * @fn      void commonLogMessage(COMMON_LOG_COMPONENTS component, LOG_LEVELS logLevel, const char *format,...)
 * @brief   Primary Log Function to writes to file and prints to console (if appropriate)
 * @param   [in] component - log file location to log to
 * @param   [in] logLevel  - logLevel of this message
 * @param   [in] format    - formatted string to be evaluated
 * @param   [in] ...       - arguments used in creating final log string 
 ****************************************************************************/
void commonLogMessage(COMMON_LOG_COMPONENTS component, LOG_LEVELS logLevel, const char *format,...)
{
   struct timeval tv;
   if (log_with_time_console || log_with_time_file)
      gettimeofday(&tv,NULL);

   // validate input parameters
   if (!(validateLogParams(component, COMMON_LOG_CONSOLE_TYPE, logLevel, __FUNCTION__)))
      return;

   // make sure we are initialized too //
   if ((maxLogLevel[component] < logLevel) || !common_log_init_done || common_log_closed)
      return;

   // setup current log struct  //
   pLog pLogger = &logger[component];

   // now we are going to do something
   va_list arglist;
   char    vargsBuffer[512];
   char    fileBuffer[512];
   int     logSize;

   bzero(fileBuffer,sizeof(fileBuffer));
   bzero(vargsBuffer,sizeof(vargsBuffer));
    
   // format the input //
   va_start(arglist, format);
   vsnprintf(vargsBuffer,sizeof(vargsBuffer),format,arglist);
   va_end(arglist);

   // format final log string 
   if (log_with_time_file)
      logSize = snprintf(fileBuffer,sizeof(fileBuffer),"%d.%08X %s : %s\n",
                         (u32)tv.tv_sec, (u32)tv.tv_usec,
                         getLevelString(logLevel), 
                         vargsBuffer);
   else
      logSize = snprintf(fileBuffer,sizeof(fileBuffer),"%s : %s\n",
                         getLevelString(logLevel), 
                         vargsBuffer);

   if (logSize >= 512)
      ERR("Truncation of Log File(%u/%u Max)\n",logSize, (u32)sizeof(fileBuffer));

   // Write to File if Level OK //
   if (logLevel <= commonLogLevel[COMMON_LOG_FILE_TYPE][component])
   {
      // lock 
      if ((pthread_mutex_trylock(&pLogger->fileLock)))
         goto do_not_write_logfile;

      // if still valid handle
      if (pLogger->fHandle)
         fwrite(fileBuffer, logSize, 1, pLogger->fHandle);
      else
         ERR("\tERROR> File Handle for <%s> is NULL <ERROR\n",pLogger->componentName);
	
      // unlock //
      if ((pthread_mutex_unlock(&pLogger->fileLock)))
         perror("\tlogMsg - unlocking log file");
   }
do_not_write_logfile:
   // send to console IFF Level is OK //
   if (logLevel <= commonLogLevel[COMMON_LOG_CONSOLE_TYPE][component])
   {
      if (log_with_time_console)
      {
         DBG("%d:%05X %s %s : %s\n",
             (u32)tv.tv_sec, (u32)tv.tv_usec,
             getLogComponentString(component), getLevelString(logLevel), vargsBuffer);
      }
      else
         DBG("%s %s : %s\n",getLogComponentString(component), getLevelString(logLevel), vargsBuffer);
   }
}

/**
 * @fn      void handle_archive_request(MEDIA_TARGETS mediaTarget)
 * @brief   handles archive requests for whomever is calling
 * @param   [in] mediaTarget - target type for resultant file 
 * @retval  0 == success, otherwise, error
 ****************************************************************************/
void handle_archive_request(UNUSED_ATTRIBUTE u8 target)
{
}

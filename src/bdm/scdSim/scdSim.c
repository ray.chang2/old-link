/**
 * @file scdSim.c
 * @brief scd simulator
 * @details 
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/25/18
 * 
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/25/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>

#include "commonTypes.h"
#include "commonState.h"
#include "commonLog.h"

#include "connUtils.h"
#include "msgD2.h"
#include "commSerUtils.h"
#include "commonDebug.h"
#include "zmq.h"
#include "dsc.h"

extern int doRxCrc;

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

/**
 * static/private/local variables
 */
static bool changeHBStatus      = false;
static int  serial_fd           = -1;
static bool isBdmConnected      = false;
static u32  rxHeartbeatCount    = 0;
static u32  txHeartbeatCount    = 0;
static u32  rxHeartbeatMiss     = 0;
/**
 * global variable(s)
 */
common_state_t common_system_state;    

/////// expected commands for filtering //////
#define VARIABLE_LENGTH_MSG_DATA_SIZE  (-1)
#define NUM_SUPPORTED_BDM2SCD_CMDS 11
bdm_scd_exp_cmds_t scdRxCmds[NUM_SUPPORTED_BDM2SCD_CMDS] = 
{
   // standard stuff
   {BDM_TO_SCD_GENERIC_RESPONSE,         GENERIC_REPLY_DATA_LENGTH},           // 3 byte status
   {BDM_TO_SCD_DISCOVERY_REQUEST,        BDM_TO_SCD_DISCOVERY_REQ_LEN},        // discovery/version

   {BDM_TO_SCD_HEARTBEAT_CMD,            HB_BDM_SCD_DATA_LENGTH},              // hb struct
    
   {BDM_TO_SCD_GET_PORT_STATUS,          BDM_TO_SCD_GET_PORT_STATUS_DATA_LEN  }, // no data payload
   {BDM_TO_SCD_PORT_STATUS_RESP,         BDM_TO_SCD_PORT_STATUS_RESP_DATA_LEN }, // no data payload
    
   {BDM_TO_SCD_SET_DEVICE_INFO_REQUEST,  SET_DEVICE_INFO_DATA_LEN             }, // dev info
   {BDM_TO_SCD_TRIGGER_OUT_EVENT_RESP,   ONE_BYTE_ACK_RESPONSE_DATA_LEN       }, // 1 BYTE ACK/NACK
   {BDM_TO_SCD_SCD_CMD_REQ,              BDM_TO_SCD_SCD_CMD_LEN               }, // 1 BYTE CMD
   
   {BDM_TO_SCD_GET_BLOB_CONFIG_REQUEST,  1/*GET_BLOB_DATA_REQUEST_DATA_LEN*/  }, // 1 bytes
   {BDM_TO_SCD_SET_BLOB_CONFIG_REQUEST,  VARIABLE_LENGTH_MSG_DATA_SIZE},      //  variable length

   {BDM_TO_SCD_SERIAL_NUMBER_RESPONSE,   1                                    }, // 1 byte ack

};

/////////////////////////////////
////////// local cache //////////
/////////////////////////////////
set_scd_device_info_t   localHbpf;
hb_bdm_scd_t            localRxHBStatus;
hb_scd_bdm_t            localTxHBStatus;
scd_bdm_port_status_t   localPortStatus;
scd_to_bdm_discovery_t  localD2DiscoveryData;
scd_bdm_serial_number_t localSerialNumber;

// local blob container for data
blob_data_container_t blobCont;

bool nackSetBlob           = false;
bool timeoutGetPortStatus  = false;
bool timeoutGetPortStatus2 = false;
// control //
u8 expectedNextBlobNumberRx  = 0;
u8 expectedBlobNumberTxAck   = 0;
u8 totalBlobNumberRx         = 0;
u8 totalBlobNumberTx         = 0;
u32 bytesBlobRead            = 0;
u32 bytesBlobSent            = 0;
u32 bytesLeftTx              = 0;
/////////////////////////////////
/////////////////////////////////

////////////////////////////////////
// initialize data that is cached //
////////////////////////////////////
void init_blob_data(void)
{
   int j;

   bzero(&blobCont, sizeof(blobCont));

   bzero((u8 *)&blobCont.blobHeader,sizeof(blobCont.blobHeader));
   blobCont.blobHeader.prot_version     = 0x10;
   blobCont.blobHeader.blob_header_size = sizeof(ct_blob_data_header_t);
   blobCont.blobHeader.blob_data_size   = ntohs(D2_BLOB_DATA_SIZE);
   blobCont.blobHeader.deviceType       = DEVICE_TYPE_SHAVER;
   blobCont.blobHeader.subDeviceType    = 1;

   // set blob name //
   char *buf = (char *)blobCont.blobHeader.blob_name;
   snprintf(buf,16,"DYONICSPOWER II");

   // alloc blob data //
   blobCont.pFullBlobData = (u8 *)calloc(D2_TOTAL_BLOB_DATA_SIZE,1);
   for (j = 0 ; j < D2_BLOB_DATA_SIZE ; j++)
   {
      blobCont.pFullBlobData[j] = (u8)(j+12);
   }
   
   blobCont.pPartialBlob[0] = (u8 *)&blobCont.blobHeader;
   blobCont.pPartialBlob[1] = &blobCont.pFullBlobData[0];
   blobCont.pPartialBlob[2] = &blobCont.pFullBlobData[MAX_OPAQUE_DATA_PACKET];
   blobCont.pPartialBlob[3] = &blobCont.pFullBlobData[MAX_OPAQUE_DATA_PACKET*2];

   blobCont.blobDataSize[0] = D2_BLOB1_DATA_SIZE;
   blobCont.blobDataSize[1] = D2_BLOB2_DATA_SIZE;
   blobCont.blobDataSize[2] = D2_BLOB3_DATA_SIZE;
   blobCont.blobDataSize[3] = D2_BLOB4_DATA_SIZE; // for the CRC

   blobCont.blobHeader.header_crc        = calculate_array_crc(BLOB_DATA_HEADER_CRC_SIZE, (u8 *)&blobCont.blobHeader);
   blobCont.pFullBlobData[D2_BLOB_DATA_SIZE] = calculate_array_crc(D2_BLOB_DATA_SIZE, blobCont.pFullBlobData);

   
   display_blob(&blobCont.blobHeader, blobCont.pFullBlobData, 0);
}

void init_local_data(void)
{
   u32 i;
   u8  ln = 0x66;
   
   bzero(&localD2DiscoveryData, sizeof(localD2DiscoveryData));
   bzero(&localTxHBStatus,      sizeof(localTxHBStatus));
   bzero(&localRxHBStatus,      sizeof(localRxHBStatus));
   bzero(&localPortStatus,      sizeof(localPortStatus));
   bzero(&localHbpf,            sizeof(localHbpf));

   localD2DiscoveryData.version_number    = 0x21;
   localD2DiscoveryData.device_state      = 1; //ready
   localD2DiscoveryData.devType           = DEVICE_TYPE_SHAVER;
   localD2DiscoveryData.devSubType        = 2;

   localPortStatus.porta_display          = 0xC1; // RPM/lowspeed/forward/up/down enabled
   localPortStatus.porta_speed_run        = PORT_RUNNING_MASK | 0x10;
   localPortStatus.portb_display          = 0xC5; // RPM/lowspeed/forward/up/down enabled
   localPortStatus.portb_speed_run        = PORT_RUNNING_MASK | 0x20;
   localPortStatus.port_ab_err_warn       = 0;
   localPortStatus.settings_popups        = 0x18;

   // local serial number stuff
   bzero(&localSerialNumber,sizeof(localSerialNumber));
   localSerialNumber.serialType = PORTA_SERIAL_TYPE;
   for (i = 0 ; i < NUM_SERIAL_NUMBER ; i++)
   {
      localSerialNumber.serial_number[i] = ln++;
   }

   init_blob_data();
}

void send_port_status(void)
{
   bdm_scd_msg_container_t txMsg;
   txMsg.protocol_id  = PROTOCOL_SCD_TO_BDM;
   txMsg.command_id   = SCD_TO_BDM_PORT_STATUS;
   txMsg.cmd_data            = (u8 *)&localPortStatus;
   txMsg.cmd_data_len        = SCD_PORT_STATUS_DATA_LEN;
   txMsg.cmd_sequence_number = get_request_number();
   
//   display_msg(&txMsg);
   build_send_d2_msg(serial_fd, &txMsg);
}

void send_serial_number(void)
{
   bdm_scd_msg_container_t txMsg;
   txMsg.protocol_id  = PROTOCOL_SCD_TO_BDM;
   txMsg.command_id   = SCD_TO_BDM_SERIAL_NUMBER_STATUS;
   txMsg.cmd_data     = (u8 *)&localSerialNumber;
   txMsg.cmd_data_len = SCD_SERIAL_NUMBER_DATA_LEN;
   txMsg.cmd_sequence_number = get_request_number();
   build_send_d2_msg(serial_fd, &txMsg);
   DBG("Sent Serial Number\n");
}

/**
 * @fn      void *d2_tx_heartbeat(void *args)
 * @brief   sends heartbeats to RCM
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
static u32 waiting4DiscoveryCount = 0;
static void *scd_tx_heartbeat(void *args)
{
   u32 waitRespCount = 0;
   while (!common_system_state.stopping)
   {
      switch (common_system_state.state)
      {
      default:
         ERRF("\n\t...unknown state <%02X> in tx heartbeat....\n",common_system_state.state);
         exit(-22);
         break;
            
      case WAITING_FOR_DISCOVERY_COMPLETION:
      {
         waiting4DiscoveryCount++;
         if (waiting4DiscoveryCount && !(waiting4DiscoveryCount % 500))
         {
            DBGF("\n\t...Still Waiting for Discovery Request <%u>...\n",
                 waiting4DiscoveryCount);
            timeoutGetPortStatus = false;
            timeoutGetPortStatus2 = false;
         }
         YIELD_100MS;
         continue;
      }
      break;
            
      case WAITING_CMD_RESPONSE:
         waitRespCount++;
         if (waitRespCount < 10)
         {
            YIELD_100MS;
            continue;
         }
         else
         {
            DBGF("\t...Timeout waiting for Command <%02X>... Start sending HB again\n",common_system_state.waitCmdID);
            common_system_state.state = DISCOVERED_IDLE;
         }
         break;
         
      case DISCOVERED_IDLE:
         waitRespCount       = 0;
         break;
      }

      // send it here //
      if (common_system_state.state != WAITING_FOR_DISCOVERY_COMPLETION)
      {
         if (++rxHeartbeatMiss > MAX_NUMBER_RX_TIMEOUT_MISS)
         {
            if (isBdmConnected)
               DBGF("\n\tLOSS of BDM Heartbeat Connection - waiting for Discovery\n");
            isBdmConnected = false;
            timeoutGetPortStatus = false;
            timeoutGetPortStatus2 = false;
            common_system_state.state = WAITING_FOR_DISCOVERY_COMPLETION;
         }
      }
      YIELD_100MS;
   } // end while() //
  
   return NULL;
}

/**
 * @fn      int process_bdm_to_scd_messages(pMsg rxMsg)
 * @brief   processes rx messages for rtos 
 * @param   [in]  rxMsg - message to be interpreted
 * @retval  returns 0 on success, otherwise, it error
 */
int process_bdm_to_scd_messages(pMsgScd rxMsg)
{
   int status = 0;
   u8 *txData = NULL;

   // validate that we have valid protocol
   if (rxMsg->protocol_id != PROTOCOL_BDM_TO_SCD)
   {
      ERRF("Invalid Protocol ID <%02X> for BDM to SCD Port - should send NACK and then exit\n",rxMsg->protocol_id);
      return -2;
   }


   // setup message 
   bdm_scd_msg_container_t txMsg;
   u8 ackReply               = 0;
   u8  requestNumber         = rxMsg->cmd_sequence_number & 0x7F;
   txMsg.protocol_id         = PROTOCOL_SCD_TO_BDM;
   txMsg.cmd_data            = (u8 *)&ackReply;
   txMsg.cmd_sequence_number = requestNumber;
   txMsg.cmd_data_len        = ONE_BYTE_ACK_RESPONSE_DATA_LEN;
   
   // determine if valid protocol 
   switch(rxMsg->command_id)
   {
   case BDM_TO_SCD_GET_BLOB_CONFIG_REQUEST:
   {
      // calculate how many blobs - see if we are lucky
      u8 whichBlob = rxMsg->cmd_data[0];
      DBGF("\t~~~ Get Blob (%d) Request(%02X) ~~~\n",whichBlob, requestNumber);
      switch(whichBlob)
      {
      case 1:
         display_blob(&blobCont.blobHeader, blobCont.pFullBlobData, requestNumber);
      case 2:
      case 3:
      case 4:
      {
         u8 blobSize = blobCont.blobDataSize[whichBlob-1];
         txData      = (u8 *)calloc(blobSize,1); // blob num + blob data + 1
         txData[0]   = whichBlob;
         memcpy(&txData[1], blobCont.pPartialBlob[whichBlob-1], blobSize);
         
         txMsg.command_id          = SCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE;
         txMsg.cmd_data_len        = blobSize+1; // for the blob number
         txMsg.cmd_data            = txData;

         // set waiting for response //
         DBG("Sending Get Blob <%d>\n",txMsg.cmd_data_len);
         build_send_d2_msg(serial_fd, &txMsg);
         free(txData);
         status = 0;
      }
      break;
            
      default:
         ERRF("Unexpected Blob Num <%d> - should send a NACK\n",whichBlob);
         status = 1;
         break;
      }
   }
   break;

   case BDM_TO_SCD_SET_BLOB_CONFIG_REQUEST:
   {
      scd_blob_data_ack_t blobAck;
      
      u8 whichBlob = rxMsg->cmd_data[0];
      u8 *pData    = &rxMsg->cmd_data[1];
      u8 dataSize  = rxMsg->cmd_data_len-1;
      blobAck.packetNumber = whichBlob;
      blobAck.status       = 0;
      
      txMsg.command_id          = SCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE;
      txMsg.cmd_data_len        = sizeof(blobAck); // for the blob number
      txMsg.cmd_data            = (u8 *)&blobAck;

      DBGF("\n\t~~~ Set Blob (%d) / RN(%02X) ~~~\n",whichBlob, requestNumber);
      assert(whichBlob);
      
      switch(whichBlob)
      {
      case 4:
      case 1:
      case 2:
      case 3:
      {
         if (nackSetBlob)
         {
            DBG("Nack a set Blob\n");
            nackSetBlob = false;
            blobAck.status = 1;
         }
         else
         {
            u8 blobSize = blobCont.blobDataSize[whichBlob-1];

            if (dataSize == blobSize)
            {
               blobAck.status = 0;
               memcpy(blobCont.pPartialBlob[whichBlob-1], pData, blobSize);         
            }
            else
            {
               ERRF("Unexpected dataSize (%d) vs. Expected(%d)\n", dataSize, blobSize);
               blobAck.status = 1;
            }
         
            
            // set waiting for response //
//         display_msg(&txMsg);
            free(txData);
            status = 0;
         }
      }
      break;
            
      default:
         ERRF("Unexpected Blob Num <%d> - should send a NACK\n",whichBlob);
         blobAck.status = 1;
         status = 1;
         break;
      }

      build_send_d2_msg(serial_fd, &txMsg);

      // check if its the last one and status == 0
      if ((whichBlob == NUMBER_D2_DATA_BLOBS) && (blobAck.status == 0))
      {
         display_blob(&blobCont.blobHeader, blobCont.pFullBlobData, requestNumber);
         send_port_status(); // send status //
      }         
   }
   break;
            
   case BDM_TO_SCD_SET_DEVICE_INFO_REQUEST:
   {
      localHbpf.hand_blade_pump_foot = rxMsg->cmd_data[0];
      DBG("\t...SetDevInfo <%02X>......\n", rxMsg->cmd_data[0]);
              
      // send response
      txMsg.cmd_data_len = ONE_BYTE_ACK_RESPONSE_DATA_LEN;
      txMsg.command_id   = SCD_TO_BDM_SET_DEVICE_INFO_RESPONSE;
      build_send_d2_msg(serial_fd, &txMsg);
      send_port_status(); // send status //
   }
   break;

   case BDM_TO_SCD_SERIAL_NUMBER_RESPONSE:
   {
//      DBG("Receive Serial Number Response Status: <%s>",rxMsg->cmd_data[0] ? "Failure" : "Success");      
   }
   break;
   case BDM_TO_SCD_PORT_STATUS_RESP:
   {
//      DBG("Rx Port Status <%s> Ressponse\n",rxMsg->cmd_data[0] ? "Failure" : "Success");
   }
   break;
            
   case BDM_TO_SCD_GET_PORT_STATUS:
   {
      DBG("Get Port Status Request(%d)\n",timeoutGetPortStatus);
      if (timeoutGetPortStatus)
      {
         DBG("Testing Timeout\n");
      }
      else if (timeoutGetPortStatus2)
      {
         DBG("1 Timeout");
         timeoutGetPortStatus2 = false;
      }
      else 
      {
         txMsg.command_id   = SCD_TO_BDM_GET_PORT_STATUS_RESP;
         txMsg.cmd_data     = (u8 *)&localPortStatus;
         txMsg.cmd_data_len = SCD_PORT_STATUS_DATA_LEN;
         build_send_d2_msg(serial_fd, &txMsg);
      }         
   }
   break;

   case BDM_TO_SCD_SCD_CMD_REQ:
   {
      u8 cmd = rxMsg->cmd_data[0];
//      DBG("Got SCD Command Request<%02X>\n",cmd);
      txMsg.cmd_data_len = SCD_TO_BDM_SCD_CMD_RESP_LEN;
      txMsg.command_id   = SCD_TO_BDM_SCD_CMD_RESP;         
//         display_msg(&txMsg);
      build_send_d2_msg(serial_fd, &txMsg);
      switch(cmd)
      {
      case PORTA_UP:
         localPortStatus.porta_speed_run++;
         break;
      case PORTA_DOWN:
         if (localPortStatus.porta_speed_run & 0x7F)
            localPortStatus.porta_speed_run--;
         else
            DBG("SPEED PORTA ALREADY 0\n");
         break;
      case PORTA_DELTA_OSC_MODE:
         DBG("PORTA OSC\n");
         break;
      case PORTB_UP:
         localPortStatus.portb_speed_run++;
         break;
      case PORTB_DOWN:
         if (localPortStatus.portb_speed_run & 0x7F)
            localPortStatus.portb_speed_run--;
         else
            DBG("SPEED PORTB ALREADY 0\n");
         break;
      case PORTB_DELTA_OSC_MODE:
         DBG("PORTB OSC\n");
         break;
      case OK_BUTTON:
         DBG("OK\n");
         break;
      case EXIT_SETTINGS:
         DBG("Exit\n");
         break;
      default:
         DBG("Unexpected SCD Cmd\n");
         return status;
         break;
      }

//      DBGF("\tNew Port Speed<%02X/%02X>\n",localPortStatus.porta_speed_run, localPortStatus.portb_speed_run);
      send_port_status(); // send status //
   }
   break;
            
   case BDM_TO_SCD_TRIGGER_OUT_EVENT_RESP:
   {
      // got a good reply
      u8 cmdReply = *rxMsg->cmd_data;
      DBGF("\ttttt> Got Expected Trigger Out Response (%02X) <tttt\n",cmdReply);
   }
   break;

   case BDM_TO_SCD_GENERIC_RESPONSE:
   {
      pGenReply pReply = (pGenReply)rxMsg->cmd_data;
      DBGF("....Got Gen Ack Status <%d> in <%d> state for Cmd <%02X>...\n", pReply->status, common_system_state.state,pReply->cmd_id);
//      send_serial_number();
   }
   break;
            
   case BDM_TO_SCD_DISCOVERY_REQUEST:
   {
      pBdm2ScdDisc pDiscReq = (pBdm2ScdDisc)rxMsg->cmd_data;
      if (common_system_state.state >=  DISCOVERED_IDLE)
      {
         DBGF("Unexpected (%d) discovery request <version: %02X> in <%d>- responding...\n",
              timeoutGetPortStatus, pDiscReq->version_number, common_system_state.state);
         isBdmConnected = false;
      }
      else
      {
         DBGF("\t...We have gotten (%d)discovery request <%02X> in <%d> responding...\n",
              timeoutGetPortStatus, pDiscReq->version_number, common_system_state.state);
      }
      timeoutGetPortStatus = false;
      nackSetBlob = false;
            
      txMsg.command_id             = SCD_TO_BDM_DISCOVERY_RESPONSE;
      txMsg.cmd_data_len           = SCD_TO_BDM_DISCOVERY_RESP_LENGTH;
      txMsg.cmd_data               = (u8 *)&localD2DiscoveryData;
      build_send_d2_msg(serial_fd, &txMsg);
      common_system_state.state = DISCOVERED_IDLE;
      waiting4DiscoveryCount    = 0;
      send_serial_number();
   }
   break;
      
   case BDM_TO_SCD_HEARTBEAT_CMD:
   {
      // copy hb data over 
      memcpy(&localRxHBStatus, rxMsg->cmd_data, sizeof(localRxHBStatus));

      // increment receive right now //
      rxHeartbeatCount++;
      rxHeartbeatMiss = 0;

      // check if we are reconnecting
      if (isBdmConnected == false)
      {
         DBGF("\n\t....BDM is (RE)connected....\n");
         send_port_status(); // send status //
      }
         
      isBdmConnected = true;

      // initialize things that don't change
      u8 devStatus       = (common_system_state.state >= DISCOVERED_IDLE ? 1 : 0);
      txMsg.protocol_id  = PROTOCOL_SCD_TO_BDM;
      txMsg.command_id   = SCD_TO_BDM_HEARTBEAT_CMD;
      txMsg.cmd_data_len = HB_SCD_BDM_DATA_LENGTH;
      txMsg.cmd_data     = (u8 *)&devStatus;
      build_send_d2_msg(serial_fd, &txMsg);
      txHeartbeatCount++;
   } // if heartbeat //
   break; 

   default:
      ERRF("Unexpected BDM to SCD Command (%d) received\n",rxMsg->command_id);
      break;
   } // end switch cmd //

   return status;
}

/**
 * @fn      void external_display_function (void)
 * @brief   displays external status
 * @retval  void
 ********************************************************/
extern u32 totalBytesRead;
void external_display_function(void)
{
   // determine if we are timed out //
   if (isBdmConnected)
   {
      DBG("BDM IS connected");
   }
   else
      DBG("No BDM Connection(%08X)",totalBytesRead);

   // reset these anyways //
   DBG(" --->SELECT AN OPTION> ");
   changeHBStatus = false;

}


/**
 * @fn      void serial_cli_task(void)
 * @brief   RTOS CLI Task used as a unit test
 * @details Sends/Receives messages from serial rx task (via a socket)
 * @retval  NULL (exits when told to)
 */
extern u8 errorCode;
int new_serial_cli_task(void)
{
   pthread_t threadID;
   char cmdBuf[4];

   bzero((u8 *)&common_system_state,sizeof(common_state_t));
   common_system_state.state = WAITING_FOR_DISCOVERY_COMPLETION;
     
   // init common signal handler
   init_common_signal_handler();
     
   // reset structs */
   bzero(&common_system_state,sizeof(common_system_state));
   //     bzero(ecmCtl,sizeof(ecmCtl));

   // initialize logging 
   if (common_init_log())
   {
      ERR("Error initializing Logs\n");
      return -111;
   }

   init_msgD2_lock();
   init_local_data();
     
   //init debug display
   register_display_common(external_display_function, &changeHBStatus);

   // register setup serial port 
   if ((init_serial_connection(&serial_fd)))
   {
      ERRF("RTOS Error in initializing Serial Connection\n");
      goto rtos_exit_cli;
   } // error in RCM //

   DBGF("Serial Port Initialized <%d>\n",serial_fd);

   // create data to pass between threads //
   pSerialRxArgs pArgs;
   if ((pArgs = calloc(1,sizeof(serial_rx_args_t))) == NULL)
   {
      ERRF("Failed to calloc rx Serial args: <%s>\n",COMMON_ERR_STR);
      goto rtos_exit_cli;
   }

   // create a generic socket read thread //
   pArgs->serialProcessData = process_bdm_to_scd_messages;
   pArgs->serial_fd         = &serial_fd;
   pArgs->expectedProtocol  = PROTOCOL_BDM_TO_SCD;
   pArgs->pCmds             = &scdRxCmds[0];
   pArgs->numCmds           = NUM_SUPPORTED_BDM2SCD_CMDS;
     
   if ((common_create_thread(&threadID, LOWEST_LOW, (void *)pArgs, serial_rx)))
   {
      ERRF("Error in creating rtos_serial_ messages thread\n");
      goto rtos_exit_cli;
   }

   //create heartbeat // 
   pthread_t hbTxThrd;
   if ((common_create_thread(&hbTxThrd, MEDIUM_MEDIUM, NULL, scd_tx_heartbeat)))
   {
      ERRF("Error in creating TX heartbeat Thread\n");
      goto rtos_exit_cli;  
   }

   // enter main command loop //
   bdm_scd_msg_container_t txMsg;
   txMsg.protocol_id = PROTOCOL_SCD_TO_BDM;

   sleepSecs(1);
   while (!common_system_state.stopping)
   {
      bzero(cmdBuf,sizeof(cmdBuf));
      DBG("\t   ------------Serial DM CLI-----------\n\n"	
          "\t   1) Change RCM Log Level\n"
          "\t   2) Change SYS Log Level\n"
          "\t   ---------------------------------------\n"
          "\t   We will %sperform Rx CRC Check\n"
          "\t   ---------------------------------------\n"
          "\t   q) Quit - exit entire program",
          (doRxCrc ? "" : "not "));
      DBG("\n\t----------------------------------------------------------\n");      


      if ((getStringStatus("",cmdBuf,sizeof(cmdBuf))))
         goto rtos_exit_cli;

      // determine request //
      //	 bool yesNotNo = true;
      //         yesNotNo = true;
      switch (cmdBuf[0])
      {
         ///////////////////////
         // Change Log Levels //
         ///////////////////////
      case '1':
         cli_setLogLevel(COMMON_RCM);
         break;

      case '2':
         cli_setLogLevel(COMMON_SYS);
         break;

         ///////////////////////
         //  Configuration    //
         ///////////////////////
      case '3':
      {
         DBG("\n1 Timeout Test for Port Status\n");
         timeoutGetPortStatus2 = true;
      }
      break;

      case '5':
         DBG("send nack set_blob\n");
         nackSetBlob = true;
         break;
      case '4':
         DBG("\nTimeout Test for Port Status\n");
         timeoutGetPortStatus = true;
         break;

      case '6':
         errorCode = 6;
         send_port_status();
         break;
      case '9':
         errorCode = 9;
         send_port_status();
         break;
      case 'a':
         errorCode = 10;
         send_port_status();
         break;
      case 't':
      case 'T':
      {
         txMsg.command_id              = SCD_TO_BDM_TRIGGER_OUT_EVENT;
         txMsg.cmd_data_len            = 0;//SCD_TO_BDM_TRIGGER_OUT_LENGTH;
         txMsg.cmd_data                = NULL; 
         txMsg.cmd_sequence_number = get_request_number();
         build_send_d2_msg(serial_fd, &txMsg);
      }
      break;
      //////////////////////////
      //   Exit Serial RTOS   //
      //////////////////////////
      case 'q':
      case 'Q':
         RCM_NOTE("Exiting main program - user prompted");
         goto rtos_exit_cli;
         break; // 'q' //

             
      default:
         RCM_INFO("unknown input(%c)",cmdBuf[0]);
         break; // default //
      } // end switch command //
	     
   } // end while not stopping
	   
rtos_exit_cli:
   DBGF("Exiting SCD(%d) - Close socket & wait for read thread to exit",common_system_state.stopping);
   common_system_state.stopping = true;
   common_close_socket(&serial_fd);
   pthread_join(threadID,NULL);
   pthread_join(hbTxThrd,NULL);
   DBGF("RTOS Read thread has exited - close log\n");
   common_shutdown_log();
   DBGF("---- Exiting RTOS CLI Client -----\n");
   exit(0);
}

/**
 * @file commonLog.h
 * @brief Base definitions for logging (including to both console & file)
 */
#ifndef __COMMON_LOG_H__
#define __COMMON_LOG_H__
#include "commonDebug.h"

#define NETWORK_DRIVE_MEDIA "."
/**
 * log file path definitions
 */

#define TEMP_ARCHIVE_DIR      "workingArchiveDir"                   /**< defines archive action dir     */  
#define TEMP_ARCHIVE_LOG_PATH "./workingArchiveDir"                 /**< defines archive action dir     */  
#define TEMP_LOG_WRITE_PATH   "./tmpUsbLogs"                        /**< defines path for log files     */
#define LOG_ACTIVE_PATH       "./usbLogs"                           /**< defines path for logs files    */
#define LOG_ARCHIVE_PATH      "./usbLogs/usbLogArchive"             /**< defines path for log archive   */
#define LOG_OLD_ARCHIVE_PATH  "./usbLogs/oldArchives"               /**< defines where old archives are */
#define LOG_OLD_ARCHIVE_FILE  "./usbLogs/oldArchives/archiveNumber" /**< defines where old archives are*/
#define LOG_ARCHIVE_PATH_RAM  "./tmpUsbLogs/usbLogArchive"          /**< defines path for log archive    */

/**
 * log file names
 */
#define UCM_LOG_FILE    "usb_ucm.log"           /**< fileName for UCM Log   */
#define RCM_LOG_FILE    "usb_rcm.log"           /**< fileName for RCM Log   */
#define TCM_LOG_FILE    "usb_tcm.log"           /**< fileName for TCM Log   */
#define SYSTEM_LOG_FILE "usb_sys.log"           /**< fileName for SYS Log   */
/**
 * component names
 */
#define COMMON_UCM_COMPONENT_NAME "UCM_SUBSYSTEM"  /**< Component Name for UCM */
#define COMMON_RCM_COMPONENT_NAME "RCM_SUBSYSTEM"  /**< Component Name for RCM */
#define COMMON_TCM_COMPONENT_NAME "TCM_SUBSYSTEM"  /**< Component Name for TCM */
#define COMMON_SYS_COMPONENT_NAME "SYS_SUBSYSTEM"  /**< Component Name for SYS */

/**
 * General defines for logging
 */
#define MAX_NUM_LOGS               16              /**< Max Number Log Files to archive  */
#define MAX_LOG_SIZE               1024*1024       /**< 1MB Max Log Size before rotate   */

/**
 * logs levels 
 */
typedef enum {
   COMMON_LOG_EMERGENCY = 0,
   COMMON_LOG_ALERT,
   COMMON_LOG_CRITICAL,
   COMMON_LOG_ERROR,
   COMMON_LOG_WARNING,
   COMMON_LOG_NOTICE,
   COMMON_LOG_INFO,
   COMMON_LOG_DEBUG,
   COMMON_LOG_MAX,
} LOG_LEVELS;

/**
 * define default verification log level 
 */
#define VERIFICATION_LOG_LEVEL COMMON_LOG_DEBUG

/**
 * default init type
 */
#define COMMON_INIT_LOG_LEVEL COMMON_LOG_INFO     /**< default/initialized log level */

/**
 * Component Types
 */
typedef enum {
   COMMON_UCM = 0,
   COMMON_RCM,
   COMMON_TCM,
   COMMON_SYS,
   COMMON_MAX_COMPONENTS,
} COMMON_LOG_COMPONENTS;                         /**< component values (0..3)   */

/**
 * log types (file or console or ??)
 */
typedef enum {
   COMMON_LOG_FILE_TYPE,
   COMMON_LOG_CONSOLE_TYPE,
   MAX_LOG_TYPE,
} COMMON_LOG_TYPES;

/**
 * public routines
 */
void commonLogMessage(COMMON_LOG_COMPONENTS component,  LOG_LEVELS logLevel, const char *format,...);  /**< primary logging interface                */
int  commonSetLogLevel(COMMON_LOG_COMPONENTS component, LOG_LEVELS value,    COMMON_LOG_TYPES type);      /**< sets log level for src, level & output   */
const char *commonGetDisplayLogLevel(COMMON_LOG_COMPONENTS component, COMMON_LOG_TYPES type);             /**< gets string to display for src loglevel  */
void change_insertion_time_in_log_entry(void);                                                   /**< toggles insert time into log entry var   */
void handle_archive_request(u8 target);                                                          /**< handle archive requests                  */

/**
 * init/shutdown routines
 */
int  common_init_log(void);              /**< initializes logging */
void common_shutdown_log(void);          /**< shuts down logging  */

/**
 * UCM log error shortcut
 */
#define UCM_EMR(fmt,args...)  ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_UCM, COMMON_LOG_EMERGENCY, fmt,##args);}) /**< UCM Shortcut */
#define UCM_ALRT(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_UCM, COMMON_LOG_ALERT,     fmt,##args);}) /**< UCM Shortcut */
#define UCM_CRIT(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_UCM, COMMON_LOG_CRITICAL,  fmt,##args);}) /**< UCM Shortcut */
#define UCM_ERR(fmt,args...)  ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_UCM, COMMON_LOG_ERROR,     fmt,##args);}) /**< UCM Shortcut */
#define UCM_WARN(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_UCM, COMMON_LOG_WARNING,   fmt,##args);}) /**< UCM Shortcut */
#define UCM_NOTE(fmt,args...) ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_UCM, COMMON_LOG_NOTICE,    fmt,##args);}) /**< UCM Shortcut */
#define UCM_INFO(fmt,args...) ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_UCM, COMMON_LOG_INFO,      fmt,##args);}) /**< UCM Shortcut */
#define UCM_DBG(fmt,args...)  ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_UCM, COMMON_LOG_DEBUG,     fmt,##args);}) /**< UCM Shortcut */

/**
 * TCM log error shortcuts 
 */
#define RCM_EMR(fmt,args...)  ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_RCM, COMMON_LOG_EMERGENCY, fmt,##args);}) /**< RCM Shortcut */
#define RCM_ALRT(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_RCM, COMMON_LOG_ALERT,     fmt,##args);}) /**< RCM Shortcut */
#define RCM_CRIT(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_RCM, COMMON_LOG_CRITICAL,  fmt,##args);}) /**< RCM Shortcut */
#define RCM_ERR(fmt,args...)  ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_RCM, COMMON_LOG_ERROR,     fmt,##args);}) /**< RCM Shortcut */
#define RCM_WARN(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_RCM, COMMON_LOG_WARNING,   fmt,##args);}) /**< RCM Shortcut */
#define RCM_NOTE(fmt,args...) ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_RCM, COMMON_LOG_NOTICE,    fmt,##args);}) /**< RCM Shortcut */
#define RCM_INFO(fmt,args...) ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_RCM, COMMON_LOG_INFO,      fmt,##args);}) /**< RCM Shortcut */
#define RCM_DBG(fmt,args...)  ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_RCM, COMMON_LOG_DEBUG,     fmt,##args);}) /**< RCM Shortcut */

/**
 * TCM log error shortcuts 
 */
#define TCM_EMR(fmt,args...)  ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_TCM, COMMON_LOG_EMERGENCY, fmt,##args);}) /**< TCM Shortcut */
#define TCM_ALRT(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_TCM, COMMON_LOG_ALERT,     fmt,##args);}) /**< TCM Shortcut */
#define TCM_CRIT(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_TCM, COMMON_LOG_CRITICAL,  fmt,##args);}) /**< TCM Shortcut */
#define TCM_ERR(fmt,args...)  ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_TCM, COMMON_LOG_ERROR,     fmt,##args);}) /**< TCM Shortcut */
#define TCM_WARN(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_TCM, COMMON_LOG_WARNING,   fmt,##args);}) /**< TCM Shortcut */
#define TCM_NOTE(fmt,args...) ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_TCM, COMMON_LOG_NOTICE,    fmt,##args);}) /**< TCM Shortcut */
#define TCM_INFO(fmt,args...) ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_TCM, COMMON_LOG_INFO,      fmt,##args);}) /**< TCM Shortcut */
#define TCM_DBG(fmt,args...)  ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_TCM, COMMON_LOG_DEBUG,     fmt,##args);}) /**< TCM Shortcut */

/**
 * system log error shortcuts 
 */
#define SYS_EMR(fmt,args...)  ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_SYS, COMMON_LOG_EMERGENCY, fmt,##args);}) /**< SYS Shortcut */
#define SYS_ALRT(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_SYS, COMMON_LOG_ALERT,     fmt,##args);}) /**< SYS Shortcut */
#define SYS_CRIT(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_SYS, COMMON_LOG_CRITICAL,  fmt,##args);}) /**< SYS Shortcut */
#define SYS_ERR(fmt,args...)  ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_SYS, COMMON_LOG_ERROR,     fmt,##args);}) /**< SYS Shortcut */
#define SYS_WARN(fmt,args...) ({if (early_log_printf_err) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_SYS, COMMON_LOG_WARNING,   fmt,##args);}) /**< SYS Shortcut */
#define SYS_NOTE(fmt,args...) ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_SYS, COMMON_LOG_NOTICE,    fmt,##args);}) /**< SYS Shortcut */
#define SYS_INFO(fmt,args...) ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_SYS, COMMON_LOG_INFO,      fmt,##args);}) /**< SYS Shortcut */
#define SYS_DBG(fmt,args...)  ({if (early_log_printf_log) {ERR("\t"); ERR(fmt,##args); ERR("\n");} else commonLogMessage(COMMON_SYS, COMMON_LOG_DEBUG,     fmt,##args);}) /**< SYS Shortcut */

/**
 * more debug info
 */
const char *getLogTypeString(COMMON_LOG_TYPES type);                 /**< Log Type string          */
const char *getLogComponentString(COMMON_LOG_COMPONENTS component);  /**< get component string     */
const char *getLogLevelString(LOG_LEVELS level);                  /**< returns log level string */

/**
 * Extern 
 */
extern bool early_log_printf_err;
extern bool early_log_printf_log;

extern u32  cache_flush_count;

#endif //  __COMMON_LOG_H__

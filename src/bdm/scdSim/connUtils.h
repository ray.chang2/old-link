/**
 * @file connUtils.h
 * @brief   Utilties for connection related functionality (connect/accept/send/receive)
 * @details all the forward declarations 
 * 
 * @version .01
 * @author  Tom Morrison
 * @date 8/25/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   8/25/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __CONN_UTILS_H__
#define __CONN_UTILS_H__

#include "semaphore.h"
#include "commonMiscUtils.h"

/**
 * 
 * Socket utilities 
 */
int check_socket_nonblocking(int socket_fd, bool *eventStatus);                 /**< non-blocking detection of incoming data/conn info  */
int wait_network_bytes_available(int *inPutFd, int size);                       /**< waits forever (or stopping) for 'size' bytes  to be available on fd */
int poll_incoming_connection      (int socket_fd);                              /**< polls incoming fd for incoming data/connection     */
int poll_incoming_connection_timed(int socket_fd,u32 waitSecs);                 /**< a timed wait for incoming data                     */
int wait_network_bytes_available_timed(int *inPutFd, int size, u32 waitSecs);        /**< waits until timeout or 'size' bytes  to become available on fd */
int send_socket_data              (int socket_fd, u8 *output,      u32 output_size); /**< sends data to a socket                             */
int send_socket_data_message      (int socket_fd, u8 *output,      u32 output_size); /**< sends data to a socket                             */
int receive_socket_data           (int socket_fd, u8 *input,       u32 input_size);  /**< receives data from a socket                        */
int receive_socket_data_message   (int socket_fd, u8 *input,       u32 input_size);  /**< receives data from a socket                        */

int receive_socket_data_simple(int socket_fd, u8 *input_data, u32 input_size);

int create_tcp_server_socket(u16 port);                                      /**> creates a new tcp/ip socket server to accept connections     */
int accept_tcp_socket_server (int server_socket_fd);                         /**< accepts new tcp connection                                   */

/**
 * externs 
 */
extern bool socketClosing;
extern u32  useSendSocket_max;
extern u32  transmit_sendFile_blockSize;
extern u32  transmit_sendSock_blockSize;
extern u32  receiveFile_blockSize;
extern u32  sendFile_uSleep;
extern u32  sendSocket_uSleep;
extern u32  max_sendSocket_size;
extern u32  max_fileSize_ram;
extern u32  max_fileSize_flash;
extern u32  timeout_fileCompletion;
extern u32  timeout_fileReceived;

/**
 * stats
 */
extern u32  sendFileStatsTime;
extern u32  recvFileStatsTime;
extern bool display_sendFile_stats;
extern bool display_recvFile_stats;

/**
 * timeout responses from Tablet
 */
extern u32  timeout_fileCompletion;                
extern u32  timeout_fileReceived;
extern bool debugStall;

/**
 * timeout for waiting for connection message
 */
#define TIMEOUT_WAIT_TABLET_CONNECT_SECS 1

/**
 * socket port defintions for TCM
 */
#define TCM_TABLET_COMMAND_SERVER_PORT        0x6996  /**< MAIN tablet server port */
#define TCM_TABLET_JPEG_REQUEST_PORT          0x6998  /**< Jpeg Transfer Port      */
#define TCM_TABLET_VIDEO_REQUEST_PORT         0x6999  /**< Low Priority Port       */
#define TCM_EXTERNAL_APPLICATION_SERVER_PORT  0x6997  /**< External App server port*/

/**
 * client ID definitions
 */
#define RCM_CLIENTID       "RCM"   /**< RCM ClientID                 */
#define TCM_CLIENTID       "TCM"   /**< TCM ClientID                 */
#define UCM_CLIENTID       "UCM"   /**< UCM ClientID                 */
#define TABLET_CLIENTID    "TAB"   /**< Tablet ClientID              */
#define TABLET_BULK        "BLK"   /**< Tablet Bulk ClientID         */
#define TABLET_JPEG        "JPG"   /**< Tablet Jpeg ClientID         */
#define TABLET_VIDEO       "VID"   /**< Tablet VIDEO ClientID        */
#define SERIAL_CLIENT      "SER"   /**< Serial Port client ID        */

/**
 * socketIDS
 */
#define SERIAL_IPC_SOCKET "serial_reader_fifo"  /**< debug serial IPC socket name */
#define RCM_CLI_SOCKET    "rcm_cli_socket"      /**< unix domain name socket name */
#define RCM_CLI_ID        "CLI"                 /**< connection identifier        */

/**
 * utility socket and resource functions
 */
bool isConnectionValid(int fd);
void common_close_socket(int *fd);                 /**< utility to close socket and reset fd          */   

/**
 * debug output 
 */
#ifdef DISPLAY_LOCAL_IP_ADDRESS
void displayLocalEth0(void);                   /**< display ip address of eth0   */
#endif // DISPLAY_LOCAL_IP_ADDRESS

#endif //__USB_CONN_UTILS_H__

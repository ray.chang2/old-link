/**
 * @file serialUtils.h
 * @brief Serial Utils for rtos
 * 
 * @version .01
 * @author  Tom Morrison
 * @date 5/25/13
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   8/25/18   Initial Creation.
 *
 *</pre>
 */

#ifndef __SERIAL_UTILS_H__
#define __SERIAL_UTILS_H__
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

/** 
 * functions implemented or required 
 */
int  init_serial_connection(int *serial_fd);  /**< inits the serial port */
void *serial_rx(void *args);                  /**< main serial rx task   */

/**
 * dispatch function called
 */
typedef int (*dispatch_function_t2)(pMsgScd rxMsg);  /**< function prototype for dispatch function */

/**
 * serial RX args 
 */
typedef struct serial_rx_args 
{
   dispatch_function_t2 serialProcessData;
   int     *serial_fd;
   u8       expectedProtocol;
   u8       numCmds;
   pExpCmds pCmds;
} serial_rx_args_t, *pSerialRxArgs;
 

#endif //__SERIAL_UTILS_H__

/**
 * @file bdmWWPrivate.h
 * @brief private include for WW utils directory
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __BDM_WW_PRIVATE_H__
#define __BDM_WW_PRIVATE_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgGetSetCommon.h"
#include "msgWW.h"

/**
 * private/local variables
 */
extern common_state_t common_system_state;

extern int  serial_fd, setupBlobClientFd;

// blob related variables flags //
extern ct_blob_data_header_t blobHeader;
extern u8 *pBlobData;

// this is related to transmit / receive operations 

// pending commands 
extern u32 pendingActions;

#define SEND_SET_SETUP_BLOB_WCD_MASK       BIT_MASK(2)
#define SEND_GET_SETUP_BLOB_WCD_MASK       BIT_MASK(3)

#define RESPOND_GET_SETUP_BLOB_CLIENT_MASK BIT_MASK(16)
#define RESPOND_SET_SETUP_BLOB_CLIENT_MASK BIT_MASK(17)

#define LED_ON  1
#define LED_OFF 0

void turn_serial_led(int led_status);
void send_set_blob_wcd(u8 whichBlob);
void send_get_blob_wcd(u8 whichBlob);
void send_set_setup_blob_wcd(void);
void send_get_setup_blob_wcd(void);
void respond_get_setup_blob_client(void);
void respond_set_setup_blob_client(u8 setStatus);
void check_rx_sequence_setupBlob(u8 rxSeq, u8 cmdId);
void send_get_port_status_wcd(void);
int  getStringStatus(const char *prompt, char *copyTo, u8 maxSize);
void send_set_therapy_settings_wcd(void);
void send_ack_for_cmd(u8 command_id, u8 requestNumber, u8 ackByte);
void send_get_request(u8 getRequest);

const char *cmdId2TxtWW(u8 cmdID);


#endif // __BDM_PRIVATE_H__

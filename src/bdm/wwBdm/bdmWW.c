/**
 * @file bdmWW.c
 * @brief bdm for WW
 * @details
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/10/18
 *
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/10/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/syslog.h>
#include <sys/types.h>
#include <linux/types.h>
#include <termios.h>

#include "commonTypes.h"
#include "commonState.h"
#include "cliCommon.h"
#include "connUtils.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgSerial.h"
#include "msgIp.h"
#include "msgWW.h"
#include "msgGetSetWW.h"
#include "broker_api.h"
#include "bdmWWPrivate.h"
#include "WW_channels.h"
#include "ctb_db_table_api.h"

#include "zmq.h"
#include "dsc.h"

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

/**
 * static/private/local variables
 */
int  serial_fd           = -1;
int  getSetClientFd      = -1;
int  setupBlobClientFd   = -1;

pthread_mutex_t serialMsgLock;

bool isWWConnected       = false;
bool isWWReady           = false;
u32  rxHeartbeatCount    = 0;
u32  txHeartbeatCount    = 0;
u32  rxHeartbeatMiss     = 0;

extern int doCli;

/**
 * dispatch of setup blobs)
 */
ct_exp_cmds_t bdmIpSetupBlobCmds[2] =
{
   {SETUP_BLOB_PROTOCOL_CLIENT_TO_BDM, SETUP_BLOB_GET_CMD,   0},
   {SETUP_BLOB_PROTOCOL_CLIENT_TO_BDM, SETUP_BLOB_SET_CMD,  -1},
};


/**
 *  dispatch of serial commands received
 */
#define NUM_SUPPORTED_WCD_BDM_CMDS 12
ct_exp_cmds_t bdmRxCmds[NUM_SUPPORTED_WCD_BDM_CMDS] =
{
      // standard stuff /
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_GENERIC_RESPONSE,          GENERIC_REPLY_DATA_LENGTH           },  // 3 byte status
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_DISCOVERY_RESPONSE,        CAPDEV_TO_BDM_DISCOVERY_RESP_LENGTH },  // disc resp
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_HEARTBEAT_CMD,             HB_WCD_BDM_DATA_LENGTH              },  // hb status
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_PORT_STATUS,               WCD_PORT_STATUS_DATA_LEN            },  // port status
   
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_SET_THERAPY_RESPONSE,      ONE_BYTE_ACK_RESPONSE_DATA_LEN      },  // 1 BYTE ACK of set
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_THERAPY_SETTINGS_STATUS,   WCD_THERAPY_SETTINGS_DATA_LEN       },  // therapy settings (either async or get response

   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_GET_ERROR_INFO_RESP,       WCD_ERROR_STATUS_DATA_LEN           },  // Get Error Info Response
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_GET_AMBIENT_SETTINGS_RESP, WCD_AMBIENT_STATUS_DATA_LEN         },  // get ambient settings response
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_GET_WAND_LIFE_INFO_RESP,   WAND_LIFE_INFO_DATA_LEN             },  // get wand  life info resp
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_GET_GENERATOR_INFO_RESP,   GENERATOR_SERIAL_INFO_DATA_LEN      },  // get generator info response 
   
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE,  VARIABLE_LENGTH_MSG_DATA_SIZE       },  // variable length response - ignore length
   {PROTOCOL_WCD_TO_BDM, WCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE,  SET_BLOB_DATA_RESPONSE_DATA_LEN     },    // blob data rx ack/nack
};

/////////////////////////////////
////////// local cache //////////
/////////////////////////////////
bool isConfigLock = false;
pthread_mutex_t localConfigLock;

capdev_to_bdm_discovery_t  localWWDiscoveryData;
hb_bdm_wcd_t               localTxHBStatus;
hb_wcd_bdm_t               localRxHBStatus;
wcd_bdm_port_status_t      localPortStatus;
wcd_therapy_settings_t     localTherapySettings;
generator_info_t           localGenInfo;
wand_life_info_t           localWandLife;
ambient_settings_t         localAmbientSettings;
error_notification_t       localErrorInfo;
ctn_ww_get_response_data_t localGetResponseData; // this is external(get/set) port status
ct_blob_data_header_t      localBlobHeader;

// main function
static const char* version = "01.00.00"; //follows major.minor.patch scheme(https://semver.org)
char *comport;
int doCli = 1;

// osd coordinates - timeStamp (unused) + 4 byte field to extract/put into blob
OSD_coords_t localCoords = {0, 0x55, 0x44, 0x33, 0x22};

DscMsg_ListDcptVirtual_t localListMsg; 
bool isvalidClientList = false;

// wait flags 
u8 waitDevInfo    = 0;
u8 waitGetPort    = 0;
u8 waitTherapyGet = 0;
u8 waitAmbient    = 0;
u8 waitErrorInfo  = 0;
u8 waitGetInfo    = 0;
u8 waitWandLife   = 0;
u8 waitSetTherapy = 0;

// flags for pending //
u32 pendingActions  = 0;

// blob related variables flags //
ct_blob_data_header_t blobHeader;
u8 *pBlobData = NULL;

///////////////////
// pub/sub stuff //
///////////////////
void *pHandlePub = NULL;

////////////////
// blob rx/tx //
////////////////
static u32 num_rx_get_data_blobs = 0;
static u32 num_tx_set_data_blobs = 0;

///////////////////////////////////
// initialize data //
///////////////////////////////////
void reset_local_data(void)
{
   isWWReady         = false;
   isWWConnected     = false;
   isvalidClientList = false;
   bzero(&localPortStatus,      sizeof(localPortStatus));
   bzero(&localGenInfo,         sizeof(generator_info_t));
   bzero(&localWandLife,        sizeof(wand_life_info_t));
   bzero(&localAmbientSettings, sizeof(ambient_settings_t));
   bzero(&localErrorInfo,       sizeof(error_notification_t));
   bzero(&localWWDiscoveryData, sizeof(localWWDiscoveryData));
   bzero(&localTxHBStatus,      sizeof(localTxHBStatus));
   bzero(&localRxHBStatus,      sizeof(localRxHBStatus));
   bzero(&localTherapySettings, sizeof(localTherapySettings));   
   bzero(&localListMsg,        CLIENT_LIST_MSG_LENGTH);
}

///////////// reset all the wait flags ////////
void reset_wait_flags(void)
{
   waitDevInfo    = 0;
   waitGetPort    = 0;
   waitAmbient    = 0;
   waitErrorInfo  = 0;
   waitGetInfo    = 0;
   waitWandLife   = 0;
   waitSetTherapy = 0;
   pendingActions = 0;   
}

void init_local_data(void)
{
   // initialize local data lock
   pthread_mutex_init(&localConfigLock,NULL);
   isConfigLock = true;
   reset_wait_flags();
   reset_local_data();

   // reset both of these //
   localTherapySettings.ab_coag_smart_sp = 0x33;
   localTherapySettings.ab_coag_18_sp    = 0x81;
   localTherapySettings.amb_thres_sp     = 0x55;
   localTherapySettings.gen_set          = 0x66;
   localTherapySettings.amb_set          = 0x77;
   localTherapySettings.reflex_timer     = 0x31;
   localTherapySettings.pump_settings    = 0x88;
   localTherapySettings.therapy_settings = 0x99;

   if ((pHandlePub = bm_publisher()) == NULL)
   {
      DBG("Failure to create publisher handler\n");
   }

   // kludge doesn't link otherwise??
#if 1
   void *handle = zmq_ctx_new();
   zmq_ctx_destroy(handle);
   handle = NULL;
#endif
}
/**
 * @fn      void dispatch_setupBlob_msg(pIpMsg rxMsg)
 * @brief   processes setup Blob Messages from clients
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void dispatch_setupBlob_msg(pIpMsg rxMsg)
{
   // determine if set or get 
   if (rxMsg->command_id == SETUP_BLOB_SET_CMD)
   {
      u16 total_blob_data_size, totalExpectedSize, osdCoordsOffset;
      memcpy(&blobHeader, rxMsg->cmd_data, BLOB_DATA_HEADER_SIZE);
      total_blob_data_size = ntohs(blobHeader.blob_data_size) + 1;
      osdCoordsOffset      = total_blob_data_size + BLOB_DATA_HEADER_SIZE;
      totalExpectedSize    = osdCoordsOffset + BLOB_COORDS_SIZE;
      assert(!(total_blob_data_size % MAX_OPAQUE_DATA_PACKET));
      num_tx_set_data_blobs = (1 + (total_blob_data_size / MAX_OPAQUE_DATA_PACKET));
      
      // make sure its the right size - I trust the app, just not Apple
      if (totalExpectedSize != rxMsg->cmd_data_len)
      {
         logger.error("rxCmdLen(%d) != expected RxBlobSize (%d)", rxMsg->cmd_data_len, totalExpectedSize);
         respond_set_setup_blob_client(1);
         return;
      }

      // copy the coordinates and publish to osd
      memcpy(&localCoords.portA_x, &rxMsg->cmd_data[osdCoordsOffset], BLOB_COORDS_SIZE);
      logger.highlight("OSD COORDS received from get/set client (%02X/%02X/%02X/%02X)",
                       localCoords.portA_x, localCoords.portA_y, 
                       localCoords.portB_x, localCoords.portB_y);
      
      if ((bm_send(pHandlePub, (u8 *)BROKER_BDM_COORDS, &localCoords, sizeof(localCoords))) < 0)
      {
         logger.error("Failed to send Coordinates to OSD Client");
      }

      // copy the data blob over 
      pBlobData = calloc(total_blob_data_size+8,1);
      assert(pBlobData);
      memcpy(pBlobData, &rxMsg->cmd_data[BLOB_DATA_HEADER_SIZE], total_blob_data_size);
      send_set_setup_blob_wcd();
   } // if set
   else
   {
      send_get_setup_blob_wcd();
   }
}

/**
 * @fn      void *bdm_setupBlob_server(void *args)
 * @brief   receives connects/messages from SetupBlob Clients
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
extern pthread_mutex_t msgSetupBlobLock;
void *bdm_setupBlob_server(UNUSED_ATTRIBUTE void *args)
{
   ct_ip_msg_t rxMsg;
   bool continueClientConnection;
   int status;
    
   pthread_mutex_init(&msgSetupBlobLock,NULL);

   int setupBlobServerFd = create_tcp_server_socket(SETUP_BLOB_PROTOCOL_SOCKET);
   if (setupBlobServerFd <= 0)
   {
      logger.error("Couldn't create setupBlob Server Socket");
      return NULL;
   }
    
   logger.debug("Entering BDM SETUP BLOB Server");
    
   while (1)
   {
      if (common_system_state.state > WAITING_FOR_DISCOVERY_COMPLETION)
      {
         logger.highlight("Start Accepting Next Connection from SetupBlob Client");
         // accept next command 
         setupBlobClientFd = accept_tcp_socket_connection(setupBlobServerFd);
         if (setupBlobClientFd < 0)
         {              
            common_close_socket(&setupBlobServerFd);
            return NULL;
         }

         logger.highlight("New setupblob Connection <%d>",setupBlobClientFd);

         // process commands while connected
         continueClientConnection = true;
         while (continueClientConnection)
         {
            // wait for next message
            if (isConnectionValid(setupBlobClientFd) && (common_system_state.state > WAITING_FOR_DISCOVERY_COMPLETION))
            {
               bzero(&rxMsg, sizeof(rxMsg));
               if ((status = read_ip_msg(&setupBlobClientFd, bdmIpSetupBlobCmds, 2, &rxMsg)))
               {
                  if (status != 2)
                  {
                     logger.error("Error (%d) in reading msg setupblob - disconnect client",status);
                  }
                  common_close_socket(&setupBlobClientFd);
                  continueClientConnection = false;
                  freeIpMsgContainerData(&rxMsg);
               } // error reading message from server
               else
               {
                  // process if valid setup message //
                  dispatch_setupBlob_msg(&rxMsg);
                  freeIpMsgContainerData(&rxMsg);
               } // process get message
            }
            else
            {
               logger.error("Closing Client Connection (%d) State(%d)",setupBlobClientFd,common_system_state.state);
               common_close_socket(&getSetClientFd);
               continueClientConnection = false;
            } // if closing or the state went back to Discovery process //

         } // while valid connection
      } // if scd is connected
      else
      {
         YIELD_250MS;
      }
   } // while contining 
   return NULL;
}

/**
 * @fn      void *bdm_wait_clientList(UNUSED_ATTRIBUTE void * args)
 * @brief   waits for client lists from discovery client
 * @param   [in] args - Not Used
 * @retval  returns NULL
 **************************************************************/
void *bdm_wait_clientList(UNUSED_ATTRIBUTE void * args)
{
   void *pHandleSub = NULL;
   
   // create a handle and do a subscription
   if ((pHandleSub = bm_init_sub_handle(BROKER_DISCOVERY_LIST)) == NULL)
   {
      logger.error("Failure to create client list subscriber handler");
      return NULL;
   }

   while (1)
   {
      int  status;
      u32  bytesRead;   
      u8   filter[BROKER_FILTER_MAX];
      void *list = NULL;

      // get client list
      if ((status = bm_receive(pHandleSub, filter, (void*) &list, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive for scd cmd",status);
         return NULL;
      }

      if (isWWConnected == false)
      {
         logger.debug("Got a Client List in Not Ready State");
         continue;
      }

      // make sure we have enough bytes and copy over (free resources)
      assert(bytesRead >= sizeof(localListMsg));
      memcpy(&localListMsg, list, sizeof(localListMsg));
      free(list);

      // debug and set valid client list and check for d25 
      logger.highlight("local list received (%d clients)",localListMsg.num_of_devices);
      isvalidClientList = true;
   }
   return NULL;
}
/**
 * @fn      void *bdm_wait_osd(UNUSED_ATTRIBUTE void * args)
 * @brief   waits for osd coordinate updates from OSD Client
 * @param   [in] args - Not Used
 * @retval  returns NULL
 **************************************************************/
void *bdm_wait_osd(UNUSED_ATTRIBUTE void * args)
{
   u32 bytesRead;   
   void *pHandleSub = NULL;
   u8 filter[BROKER_FILTER_MAX];
   
   // create a handle
   if ((pHandleSub = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return NULL;
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSub, BROKER_OSD_COORDS, strlen(BROKER_OSD_COORDS))))
   {
      logger.error("Failed to subscribe to osd coords status");
      return NULL;
   }

   void *coords = NULL;
   while (1)
   {
      int status;
      if ((status = bm_receive(pHandleSub, filter, (void*) &coords, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive for scd cmd",status);
         return NULL;
      }

      assert(bytesRead >= OSD_COORDS_SIZE);
      memcpy(&localCoords, coords, OSD_COORDS_SIZE);
      logger.highlight("OSD COORDS received (%02X/%02X/%02X/%02X)",
                       localCoords.portA_x, localCoords.portA_y, 
                       localCoords.portB_x, localCoords.portB_y);

      free(coords);
   }
   return NULL;
}

/**
 * @fn      void *bdm_wait_setTherapy(UNUSED_ATTRIBUTE void * args)
 * @brief   waits for set SetTherapy commands to be received
 * @param   [in] args - Not Used
 * @retval  returns NULL
 **************************************************************/
void *bdm_wait_setTherapy(UNUSED_ATTRIBUTE void * args)
{
   u32 bytesRead;   
   void *pHandleSub = NULL;
   u8 filter[BROKER_FILTER_MAX];
   logger.highlight("Entering Wait Set Cmd(%s)", WW_SET_CMD);;

   // create a handle
   if ((pHandleSub = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return NULL;
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSub, WW_SET_CMD, strlen(WW_SET_CMD))))
   {
      logger.error("Failed to subscribe to wait scd command");
      return NULL;
   }

   u8 *setCmd = NULL;
   while (1)
   {
      int status;
      if ((status = bm_receive(pHandleSub, filter, (void*) &setCmd, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive for scd cmd",status);
         return NULL;
      }

      assert(bytesRead == sizeof(localTherapySettings));
      memcpy(&localTherapySettings, setCmd, bytesRead);
      send_set_therapy_settings_wcd();
      free(setCmd);
   }
   return NULL;
}


/**
 * @fn      void *bdm_publish_discovery_heartbeat(void *args)
 * @brief   sends heartbeat messages 
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *bdm_publish_discovery_heartbeat(UNUSED_ATTRIBUTE  void *args)
{
   static bool sentFirstOne = false;
   while (!common_system_state.stopping)
   {
      // only 
      YIELD_500MS;
      
      // debug output
      if (isWWConnected)
      {
         if (!sentFirstOne)
         {
            logger.debug("Sending first device ready message");
            sentFirstOne = true;
         }
      }
      else
      {
         sentFirstOne = false;
      }

      
      // now send heartbeatsend heartbeat
      if (pHandlePub != NULL)
      {      
         capDev_disc_heartbeat_t capDevInfo = {0x69BD33CC, isWWConnected};
         if ((bm_send(pHandlePub, (u8 *)BROKER_CAPDEV_HEARBEAT, &capDevInfo, sizeof(capDevInfo))) < 0)
         {
            logger.error("Failed to send ww discovery == false");
         }
      }
      else
      {
         logger.error("no pub handle to publish discovery heartbeat");
      }
      
   } // end while //
   return NULL;
}
/**
 * @fn      void send_discovery(void)
 * @brief   sends discovery message discovery
 * @param   n/a
 * @retval  n/a
 **************************************************************/
static bdm_to_capdev_discovery_t discReq = { DEFAULT_VERSION_BYTE };
void send_discovery(void)
{
//   logger.info("(Re?)Start sending discovery");
   bdm_serial_msg_container_t txMsg;   
   bzero(&txMsg, sizeof(txMsg));   
   txMsg.protocol_id    = PROTOCOL_BDM_TO_WCD;
   txMsg.command_id     = BDM_TO_WCD_DISCOVERY_REQUEST;
   txMsg.cmd_data_len   = BDM_TO_CAPDEV_DISCOVERY_REQ_LEN;
   txMsg.cmd_data       = (u8 *)&discReq;         
   txMsg.request_number = GET_CMD_REQUEST_NUMBER();
   bzero(&localWWDiscoveryData,sizeof(localWWDiscoveryData));
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void publish_connected_status
 * @brief   publishes current connected status to whomever is listening
 * @param   state - state to set
 * @retval  n/a
 **************************************************************/
void publish_connected_status(bool state)
{
   // set connected state properly
   isWWConnected = state;

   // check if handle valid, and if so, publish status
   if (pHandlePub != NULL)
   {      
      logger.highlight("Publishing Connected State(%d)",state);
      if ((bm_send(pHandlePub, (u8 *)WW_CONNECTED, &isWWConnected, sizeof(isWWConnected))) < 0)
      {
         logger.error("Failed to send ww connected");
      }
   }
   else
   {
      logger.warning("Trying to publish connected status when handle not available");
   }
}

/**
 * @fn      void publish_port_status(bool reset)
 * @brief   publishes current port status to whomever is listening
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void publish_port_status(bool reset)
{
   // check if we are resetting the status
   if (reset)
   {
      bzero(&localGetResponseData, sizeof(localGetResponseData));
   }

   // check if handle is valid
   if (pHandlePub != NULL)
   {      
      logger.highlight("Publishing Port Status");
      if ((bm_send(pHandlePub, (u8 *)WW_PORT_STATUS, &localGetResponseData, sizeof(localGetResponseData))) < 0)
      {
         logger.error("Failed to Publish WW Get Response DAta");
      }
   }
   else
   {
      logger.warning("Trying to publish port status when handle not available");
   }
}

/**
 * @fn      void restart_discovery(void)
 * @brief   restarts discovery
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void restart_discovery(void)
{
   logger.highlight("restarting discovery");

   // reset state
   common_system_state.state = WAITING_FOR_DISCOVERY_COMPLETION;
   reset_local_data();
   reset_wait_flags();
   
   // send connected state
   publish_connected_status(false);

   // send reset'ed port status
   publish_port_status(true);

   // clear any messages and expected actions
   clear_all_backup_msgs();

   // send command
   send_discovery();
}

/**
 * @fn      void process_timeouts(void)
 * @brief   restarts discovery
 * @param   n/a
 * @retval  n/a
 * @note    DO NOT TRY TO FREE PBACK - its on a list
 **************************************************************/
void process_timeouts(void)
{
   struct timeval tv;
   pMsgSerial pBack = NULL;
   u32 numRetries   = 0;
   while ((get_num_msg_backed()) && (pBack = get_next_timeout_msg(txHeartbeatCount, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT,&numRetries)))
   {
      gettimeofday(&tv,NULL);
      // check if tried more than once 
      if (numRetries >= 3)
      {
         logger.error("Timeout waiting for response to <%s> go back to Discovery(%ld.%ld)",cmdId2TxtWW(pBack->command_id),tv.tv_sec, tv.tv_usec);
         
         // failed to get message - going to back to messages
         if ((clear_backup_serial_msg(pBack->request_number)))
         {
            logger.error("Failed to clear timeout message");
         }
         restart_discovery();
      }
      else
      {
         logger.highlight("Timeout - Resending Cmd/RN <%s/%02X>\n",cmdId2TxtWW(pBack->command_id), pBack->request_number);
         pBack->request_number |= REQUEST_NUMBER_CMD_BIT_MASK;
         build_send_serial_msg(serial_fd, pBack, &serialMsgLock);
      }
   } // while there are messages that are timed out //
}

/**
 * @fn      void send_heartbeat
 * @brief   sends heartbeats to capital device
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void send_heartbeat(void)
{
   bdm_serial_msg_container_t txMsg;
   bzero(&txMsg, sizeof(txMsg));
   txMsg.protocol_id    = PROTOCOL_BDM_TO_WCD;
   txMsg.command_id     = BDM_TO_WCD_HEARTBEAT_CMD;
   txMsg.cmd_data_len   = HB_BDM_WCD_DATA_LENGTH;
   txMsg.cmd_data       = (u8 *)&localTxHBStatus; 
   txMsg.request_number = GET_CMD_REQUEST_NUMBER();
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void *bdm_tx_heartbeat(void *args)
 * @brief   sends heartbeats to wcd
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
u32 waiting4DiscoveryCount = 0;
void *bdm_tx_heartbeat(UNUSED_ATTRIBUTE  void *args)
{
   while (!common_system_state.stopping)
   {
      txHeartbeatCount++;
      if (common_system_state.state == WAITING_FOR_DISCOVERY_COMPLETION)
      {
         if (!(txHeartbeatCount % SEND_HEARTBEAT_COUNT))
         {
            send_discovery();
         }
      }
      else
      {
         // check if we should send heartbeat 
         if (!(txHeartbeatCount % SEND_HEARTBEAT_COUNT))
         {
            send_heartbeat();
         }
         
         if (get_num_msg_backed())
         {
            process_timeouts();
         } // see if there is anything for me to do //
         
         // check if we aren't in discovery
         rxHeartbeatMiss++;
         if (rxHeartbeatMiss > MAX_NUMBER_RX_TIMEOUT_MISS)
         {
            if (isWWConnected)
            {
               logger.debug("LOSS of WCD Heartbeat Connection (%d) after Rx(%d)",isWWReady, rxHeartbeatCount);
            }
            restart_discovery();
         }
      } // else send heartbeat
      TIMEOUT_HEARTBEAT;
   } // end while !stopping 
   return NULL;
}

/**
 * @fn      int update_local_port_status(u8 *rxData)
 * @brief   processes updates to the local port status to compare 
            against what is already there  
 * @param   [in] rxData - incoming port status to be copied
 * @retval  returns 0 on success, otherwise, it error
 */
int update_local_port_status(u8 *rxData)
{
   bool changes = false;
   u8 *pLocalStatus = (u8 *)&localPortStatus;
   
   // compare and copy over if there 
   for (u32 i = 0 ; i < sizeof(localPortStatus) ; i++)
   {
      if (rxData[i] != pLocalStatus[i])
      {
         logger.debug("PS: [%d] change <%02X to %02X>", i,pLocalStatus[i], rxData[i]);
         pLocalStatus[i] = rxData[i];
         changes = true;
      }
   }
   
   localGetResponseData.wand_info          = localPortStatus.wand_info;
   localGetResponseData.legacy_therapy_max = localPortStatus.legacy_therapy_max;
   localGetResponseData.dev_mode           = localPortStatus.dev_mode;
   localGetResponseData.foot_info          = localPortStatus.foot_info;
   memcpy(localGetResponseData.wand_name, localPortStatus.wand_name, 12);
   
   // check if changes
   if (changes)
   {
      publish_port_status(false);
   }
   
   return 0;
}

/**
 * @fn      int update_heartbeat_status(u8 *rxData)
 * @brief   processes updates to the local port status to compare 
            against what is already there  
 * @param   [in] rxData - incoming port status to be copied
 * @retval  returns 0 on success, otherwise, it error
 */
int update_heartbeat_status(u8 *hbData)
{
   bool changes = false;
   u8 *pLocalHBData = (u8 *)&localRxHBStatus;
   
   // compare and copy over if there 
   for (u32 i = 0 ; i < sizeof(localRxHBStatus) ; i++)
   {
      if (hbData[i] != pLocalHBData[i])
      {
         logger.debug("HB:[%d] change <%02X to %02X>", i,pLocalHBData[i], hbData[i]);
         pLocalHBData[i] = hbData[i];
         changes = true;
      }
   }

   localTherapySettings.gen_set            = localRxHBStatus.gen_set;
   localTherapySettings.amb_set            = localRxHBStatus.amb_set;
   localTherapySettings.amb_thres_sp       = localRxHBStatus.amb_thres_sp;
   localTherapySettings.ab_coag_smart_sp   = localRxHBStatus.ab_coag_smart_sp;
   localTherapySettings.ab_coag_18_sp      = localRxHBStatus.ab_coag_18_sp;

   localGetResponseData.ab_coag_smart_sp    = localRxHBStatus.ab_coag_smart_sp;
   localGetResponseData.ab_coag_18_sp       = localRxHBStatus.ab_coag_18_sp;
   localGetResponseData.therapy_set         = localRxHBStatus.therapy_set;
   localGetResponseData.amb_thres_sp        = localRxHBStatus.amb_thres_sp;
   localGetResponseData.gen_set             = localRxHBStatus.gen_set;
   localGetResponseData.amb_set             = localRxHBStatus.amb_set;
   localGetResponseData.amb_temp            = localRxHBStatus.amb_temp;
   localGetResponseData.leg_amb_set         = localRxHBStatus.leg_amb_set;
   localGetResponseData.leg_amb_temp        = localRxHBStatus.leg_amb_temp;
   localGetResponseData.error_priority_code = localRxHBStatus.error_priority_code;
   

   // check if changes
   if (changes && (pHandlePub != NULL))
   {
      logger.highlight("HB Change - Publishing Port Status");
      publish_port_status(false);
   }
   
   return 0;
}

/**
 * @fn    void process_nack_cmd(pMsgSerial rxMsg)
 * @brief processes the receipt of nack command 
 * @param   n/a
 * @retval  n/a
 * @note    DO NOT TRY TO FREE PBACK - its on a list
 */
void process_nack_cmd(pMsgSerial rxMsg)
{
   pMsgSerial pBack = NULL;
   u32 numRetries = 0;
   logger.warning("Got NACK for <%s>",cmdId2TxtWW(rxMsg->command_id));
   if ((pBack = get_copy_requestNumber_msg(rxMsg->request_number, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT, &numRetries)))
   {
      // check for errors;
      if (numRetries >= 3)
      {
         logger.warning("too many nack responses for response to <%s>",cmdId2TxtWW(rxMsg->command_id));
         if ((clear_backup_serial_msg(pBack->request_number)))
         {
            logger.error("Failed to clear timeout message");
         }
         restart_discovery();
      }
      else
      {
         logger.highlight("ProcessNack - Resending NACK Cmd/RN <%s/%02X>",cmdId2TxtWW(rxMsg->command_id), rxMsg->request_number);
         pBack->request_number |= REQUEST_NUMBER_CMD_BIT_MASK;
         build_send_serial_msg(serial_fd, pBack, &serialMsgLock);
      }
   }
   else
   {
      logger.warning("Unexpected Nack (%d) - cannot find in backlog\n",rxMsg->request_number);
      restart_discovery();
      return;
   }
   
}

/**
 * @fn      int process_wcd_to_bdm_messages(pMsg rxMsg)
 * @brief   processes rx messages for rtos 
 * @param   [in]  rxMsg - message to be interpreted
 * @retval  returns 0 on success, otherwise, it error
 */
int process_wcd_to_bdm_messages(pMsgSerial rxMsg)
{
   int status = 0;
   u8 requestNumber = rxMsg->request_number;
   u8 ackResponse = 0;

   if (rxMsg->protocol_id != PROTOCOL_WCD_TO_BDM)
   {
      logger.error("unknown protocol (%02x) for rxMsg (%02x)",rxMsg->protocol_id, rxMsg->command_id);
      send_nack_cmd(NACK_GENERAL_ERROR, rxMsg->command_id, rxMsg->request_number);
      restart_discovery();
      return 0;
   }
   
   switch(rxMsg->command_id)
   {
   case WCD_TO_BDM_SET_THERAPY_RESPONSE:
   {
      logger.debug("Set Therapy Status (%d) Response(%d)",*rxMsg->cmd_data,waitSetTherapy);

      if (waitSetTherapy)
      {
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
         }
         waitSetTherapy--;
      }
      else
      {
         logger.warning("Got unexpected Set Therapy Response");
      }
   }
   break;

   case WCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE:
   {
      u8 whichBlob     = rxMsg->cmd_data[0];
      if (get_num_msg_backed() == 0)
      {
         logger.warning("unexpected get blob(%d)??",whichBlob);
         restart_discovery();
         return 0;
      }
      // debug //
      logger.debug("[%02X] Get Blob(%d/%d) Response",requestNumber, whichBlob, num_rx_get_data_blobs);
      
      u8 *pData   = &rxMsg->cmd_data[1];
      u8 dataSize = rxMsg->cmd_data_len-1;
      u8 nextBlob = whichBlob+1;
      if ((clear_backup_serial_msg(rxMsg->request_number)))
      {
         logger.error("<%s> Fail to find (%d) in saved messages", cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
         process_nack_cmd(rxMsg);
         return 0; // don't do anything else
      }
      else
      {
         if (whichBlob == 1)
         {
            memcpy(&blobHeader, pData, dataSize);
            u32 rx_blob_data_size = ntohs(blobHeader.blob_data_size)+1;
            logger.highlight("Get Blob Header/Data <%d/%d>", blobHeader.blob_header_size, rx_blob_data_size);
            if (((rx_blob_data_size) % MAX_OPAQUE_DATA_PACKET))
            {
               logger.warning("BlobDataSize+1(%d) is invalid",rx_blob_data_size, MAX_OPAQUE_DATA_PACKET);
               restart_discovery();
               return 0; // don't do anything else
            }
            else
            {
               num_rx_get_data_blobs = (rx_blob_data_size / MAX_OPAQUE_DATA_PACKET) + 1; // for blob header
               if (pBlobData)
               {
                  logger.debug("freeing previous version of blob");
                  free(pBlobData);
               }
               pBlobData = calloc(rx_blob_data_size,1);            
               send_get_blob_wcd(nextBlob);
            } // if handling 
         } // if first blob
         else
         {
            u8 blobIndex   = (whichBlob-2);
            u16 dataOffset = (blobIndex * MAX_OPAQUE_DATA_PACKET);
            assert(pBlobData);
            assert(dataSize == MAX_OPAQUE_DATA_PACKET);
            memcpy(&pBlobData[dataOffset], pData, MAX_OPAQUE_DATA_PACKET);
            if (whichBlob == num_rx_get_data_blobs)
            {
               display_blob(&blobHeader, pBlobData, requestNumber);
               logger.highlight("Finished Getting Blob (%d/%d)for Client",whichBlob, num_rx_get_data_blobs);
               if (pendingActions & RESPOND_GET_SETUP_BLOB_CLIENT_MASK)
               {
                  respond_get_setup_blob_client();
               } // send good response to client        
            } // if done
            else
            {
               send_get_blob_wcd(nextBlob);
            } // send next
         } // success 
      } // if expected blob
   }
   break;
      
   case WCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE:
   {
      if (get_num_msg_backed() == 0)
      {
         logger.error("unexpected set blob response\n");
         restart_discovery();
         return 0; // don't do anything
      }
      else
      {
         pBlobDataAck pAck = (pBlobDataAck)rxMsg->cmd_data;
         u8 whichBlob      = pAck->packetNumber;
         if (pAck->status)
         {
            logger.warning("[%02X]Got NACK (%d) for Set Blob (%d)",rxMsg->request_number, pAck->status, whichBlob);
            process_nack_cmd(rxMsg);
            if (common_system_state.state == WAITING_FOR_DISCOVERY_COMPLETION)
            {
               if (pendingActions & RESPOND_SET_SETUP_BLOB_CLIENT_MASK)
               {
                  logger.highlight("Sending error set response to setup blob client");
                  respond_set_setup_blob_client(1);
               }
            }
         }
         else 
         {
            if ((clear_backup_serial_msg(rxMsg->request_number)))
            {
               logger.error("[%02X]<%s> Fail Clear(%d) - in SetSetupBlob", rxMsg->request_number, cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
               restart_discovery();
               return 0; // don't do anything
            }

            logger.debug("[%02X] Set Blob <%d>", requestNumber, whichBlob);
            if (whichBlob < num_tx_set_data_blobs)
            {
               send_set_blob_wcd(whichBlob+1);
            }
            else if (pendingActions & RESPOND_SET_SETUP_BLOB_CLIENT_MASK)
            {
               logger.highlight("Finish Sending Blob to WCD - send status back to client");
               respond_set_setup_blob_client(0);
            }
            else
            {
               logger.highlight("completed local set setupblob");
            }
         } // end ack sktatus
      } // unsolicited response 
   }
   break;

   case WCD_TO_BDM_GENERIC_RESPONSE:
   {
      pGenReply pReply = (pGenReply)rxMsg->cmd_data;
      logger.debug("Got Gen Ack Status <%d> in <%d> state for Cmd <%02X>",
                   pReply->status, common_system_state.state,pReply->cmd_id);
   }
   break;
            
   case WCD_TO_BDM_DISCOVERY_RESPONSE:
   {
      memcpy(&localWWDiscoveryData, rxMsg->cmd_data, CAPDEV_TO_BDM_DISCOVERY_RESP_LENGTH);
      pCapdev2BdmDisc pDiscResp = (pCapdev2BdmDisc)rxMsg->cmd_data;
      logger.debug("DiscResp: Ver<%2X> Device <is %s ready> DevType<%d> DevSubType(%d) Local State<%d> WaitCnt(%d)",
                   pDiscResp->version_number,
                   (pDiscResp->device_state ? "" : "not"),
                   (u8)pDiscResp->devType,
                   pDiscResp->devSubType,
                   common_system_state.state,
                   waiting4DiscoveryCount);

      waiting4DiscoveryCount = 0;
      rxHeartbeatMiss        = 0;
      rxHeartbeatCount       = 0;
      ////////////////////////////////////////
      ////////////////////////////////////////
      // TAM:FIXME - remove this DEBUG code //
      ////////////////////////////////////////
      ////////////////////////////////////////
#if 1
      if ((pDiscResp->version_number == DEFAULT_VERSION_BYTE) || (pDiscResp->version_number == TEST_VALIDATE_VERSION))
#else
      if (pDiscResp->version_number == DEFAULT_VERSION_BYTE)
#endif         
      ////////////////////////////////////////
      ////////////////////////////////////////
      ////////////////////////////////////////
      ////////////////////////////////////////
      {
         if (common_system_state.state == WAITING_FOR_DISCOVERY_COMPLETION)
         {
            common_system_state.state = DISCOVERED_IDLE;
            send_get_request(BDM_TO_WCD_GET_GENERATOR_INFO);
         }
         else
         {
            logger.error("Unexpected Discovery Response in State <%d>",common_system_state.state);
         }
      }
      else
      {
         logger.error("Unsupported version (%02X) from WW",pDiscResp->version_number);
      }
   }
   break;
            
   case WCD_TO_BDM_HEARTBEAT_CMD:
   {
      // increment receive right now //
      rxHeartbeatCount++;
      rxHeartbeatMiss = 0;      
      if (isWWConnected == false)
      {
         logger.debug("WCD is (RE)connected");
      }
              
      // check if this is first time - we publish accordingly
      if (!isWWReady && (localRxHBStatus.devStatus & HB_DEVICE_READY_BIT_MASK))
      {         
         isWWReady = true;
         logger.highlight("WCD HAS BECOME Ready & Connected");
         publish_connected_status(true);
      }

      update_heartbeat_status(rxMsg->cmd_data);
      
   } // if heartbeat //
   break; 

   case WCD_TO_BDM_THERAPY_SETTINGS_STATUS:
   {
      // this is where I do a compare to check status //
      memcpy(&localTherapySettings, rxMsg->cmd_data, WCD_THERAPY_SETTINGS_DATA_LEN);
      localRxHBStatus.gen_set          = localTherapySettings.gen_set;
      localRxHBStatus.amb_set          = localTherapySettings.gen_set;
      localRxHBStatus.ab_coag_smart_sp = localTherapySettings.ab_coag_smart_sp;
      localRxHBStatus.ab_coag_18_sp    = localTherapySettings.ab_coag_18_sp;
      localRxHBStatus.amb_thres_sp     = localTherapySettings.amb_thres_sp;

      if (waitTherapyGet)
      {
         logger.debug("Get Therapy Settings Response");
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
         }
         waitTherapyGet--;
      }
      else
      {
         logger.debug("Async Therapy Settings Received");
         send_ack_for_cmd(BDM_TO_WCD_THERAPY_STATUS_RESPONSE, requestNumber, ackResponse);
      }
   }
   break;
      
   case WCD_TO_BDM_PORT_STATUS:
   {
      logger.debug("WCD Port Status(%d)",waitGetPort);
      if ((update_local_port_status(rxMsg->cmd_data)))
      {
         status = 6;
         break;
      }

      // determine if waiting for Port status
      if (waitGetPort)
      {
         waitGetPort--;
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
         }
      }
      else
      {
         logger.debug("Unsolicited Port Status - send ACK response");
         send_ack_for_cmd(BDM_TO_WCD_PORT_STATUS_RESP, requestNumber, ackResponse);
      }
      
   }
   break;
   
   case WCD_TO_BDM_GET_ERROR_INFO_RESP:
   {
      logger.debug("Get Error Info Response (%d)",waitErrorInfo);
      memcpy(&localErrorInfo, rxMsg->cmd_data, sizeof(localErrorInfo));

      if (waitErrorInfo)
      {
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
         }
         waitErrorInfo--;
      }
   }
   break;
      
   case WCD_TO_BDM_GET_AMBIENT_SETTINGS_RESP:
   {
      logger.debug("Get Ambient Settings Response(%d)",waitAmbient);
      memcpy(&localAmbientSettings, rxMsg->cmd_data, sizeof(localAmbientSettings));
      if (waitAmbient)
      {
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
         }
         waitAmbient--;
      }
      else
      {
         logger.warning("Unexpected Ambient Settings");
      }
   }
   break;
      
   case WCD_TO_BDM_GET_WAND_LIFE_INFO_RESP:
   {
      logger.debug("Get Wand Life Response(%d)",waitGetInfo);
      memcpy(&localWandLife, rxMsg->cmd_data, sizeof(localWandLife));
      if (waitWandLife)
      {
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
         }
         waitGetInfo--;
      }
      else
      {
         logger.warning("Unexpected Wand Life");
      }
   }
   break;
      
   case WCD_TO_BDM_GET_GENERATOR_INFO_RESP:
   {
      memcpy(&localGenInfo, rxMsg->cmd_data, sizeof(localGenInfo));
      if (waitDevInfo)
      {
         waitDevInfo--;
         logger.debug("Expected Get Generator Info Response - sending Get Port Status");
         if ((clear_backup_serial_msg(rxMsg->request_number)))
         {
            logger.error("<%s> Fail Clear(%d)", cmdId2TxtWW(rxMsg->command_id), get_num_msg_backed());
         }
         send_get_request(BDM_TO_WCD_GET_PORT_STATUS);

      }
      else
      {
         logger.debug("Unsoliticed Generator Info Response");
         send_ack_for_cmd(BDM_TO_WCD_GENERATOR_INFO_RESP, requestNumber, 0);
      }
   }
   break;


   default:
      logger.warning("Unexpected BDM to WCD Command (%d) received",rxMsg->command_id);
      break;
   } // end switch //
   return status;
}

/**
 * @fn      int bdmWW_task(void)
 * @brief   bdm task for all bridge processing
 * @retval  NULL (exits when told to)
 */
int bdmWW_task(void)
{
   int status;
   pthread_t threadID;
   char cmdBuf[4];
     
   // init common signal handler
   init_common_signal_handler();
     
   // reset structs */
   bzero(&common_system_state,sizeof(common_system_state));

   // initialize serial lock and serial date
   pthread_mutex_init(&serialMsgLock,NULL);
   init_local_data();

   // register setup serial port
   if ((status = init_serial_connection(&serial_fd, comport, 0)))
   {
      logger.error("Error (%d)in initializing Serial Connection",status);
      goto bdm_exit_task;
   } // error in bdm //
     
   logger.debug("Serial Port Initialized <%d>",serial_fd);

   // create data to pass between threads //
   pSerialRxArgs pArgs;
   if ((pArgs = calloc(1,sizeof(serial_rx_args_t))) == NULL)
   {
      logger.error("Failed to calloc rx Serial args: <%s>",COMMON_ERR_STR);
      goto bdm_exit_task;
   }

   // create a generic socket read thread //
   pArgs->serialProcessData = process_wcd_to_bdm_messages;
   pArgs->serial_fd         = &serial_fd;
   pArgs->pCmds             = &bdmRxCmds[0];
   pArgs->numCmds           = NUM_SUPPORTED_WCD_BDM_CMDS;
   pArgs->stopFlag          = &common_system_state.stopping;
   if ((common_create_thread(&threadID, HIGHEST_PRIORITY, (void *)pArgs, serial_rx)))
   {
      logger.error("Error in creating serial_ messages thread");
      goto bdm_exit_task;
   }

   //create heartbeat // 
   pthread_t hbTxThrd;
   if ((common_create_thread(&hbTxThrd, MEDIUM_LOW, NULL, bdm_tx_heartbeat)))
   {
      logger.error("Error in creating TX CMD SOCKET Thread");
      goto bdm_exit_task;  
   }

   pthread_t dsc_hb_Thrd;
   if ((common_create_thread(&dsc_hb_Thrd, MEDIUM_MEDIUM, NULL, bdm_publish_discovery_heartbeat)))
   {
      logger.error("Error in creating discovery heartbeat message Thread");
      goto bdm_exit_task;  
   }

   pthread_t set_osd_thread;
   if ((common_create_thread(&set_osd_thread, MEDIUM_LOW, NULL, bdm_wait_osd)))
   {
      logger.error("Error in creating set osd Thread");
      goto bdm_exit_task;  
   }
   
   pthread_t set_cmd;
   if ((common_create_thread(&set_cmd, MEDIUM_LOW, NULL, bdm_wait_setTherapy)))
   {
      logger.error("Error in creating Set command thread");
      goto bdm_exit_task;  
   }

   pthread_t clientList;
   if ((common_create_thread(&clientList, MEDIUM_LOW, NULL, bdm_wait_clientList)))
   {
      logger.error("Error in creating clientList command thread");
      goto bdm_exit_task;  
   }

   pthread_t bdmSetupBlobThread;
   int surgeon_profile_from_db;
   if (getField_surgeon_profile(&surgeon_profile_from_db) < 0) {
       syslog(LOG_ERR, "bdmWW: configure services value read error from db");
       goto bdm_exit_task;
   }
   else if (surgeon_profile_from_db) {
       if ((common_create_thread(&bdmSetupBlobThread, MEDIUM_LOW, NULL, bdm_setupBlob_server)))
       {
          syslog(LOG_ERR, "Error in creating setupblob server");
          goto bdm_exit_task;
       }
   }
   else {
       syslog(LOG_ERR, "bdmWW: setupBlob service is disabled!!!");
   }
    
   // enter main command loop //
   if (doCli)
   {
      YIELD_500MS;
   }

   u16 blobSize = ntohs(localBlobHeader.blob_data_size);
   while (!common_system_state.stopping)
   {
      if (doCli == 0)
      {
         sleep(1);
         continue;
      }

      bzero(cmdBuf,sizeof(cmdBuf));
      DBG("\t   ------------Serial DM CLI (%d blob size)-----------\n\n"  
          "\t   ---------------------------------------\n"
          "\t   6) Get Port Status from WCD\n"
          "\t   7) Set Therapy Settings to WCD\n"
          "\t   8) Get Blob from WCD\n"
          "\t   9) Send Blob to WCD\n"
          "\t   q) Quit - exit entire program",
          (blobSize));
      DBG("\n\t----------------------------------------------------------\n");      


      if ((getStringStatus("",cmdBuf,sizeof(cmdBuf))))
         goto bdm_exit_task;


      // determine request //
      //  bool yesNotNo = true;
      //         yesNotNo = true;
      switch (cmdBuf[0])
      {
         ///////////////////////
         // Change Log Levels //
         ///////////////////////
      case '1' :
      {
         send_get_request(BDM_TO_WCD_GET_THERAPY_SETTINGS);
      }      
      break;
      
      case '2':
      {
         send_get_request(BDM_TO_WCD_GET_ERROR_INFO);
      }      
      break;
      
      case '3':
      {
         send_get_request(BDM_TO_WCD_GET_AMBIENT_SETTINGS);
      }      
      break;
      
      case '4':
      {
         send_get_request(BDM_TO_WCD_GET_WAND_LIFE_INFO);
      }      
      break;

      case '5':
      {
         send_get_request(BDM_TO_WCD_GET_GENERATOR_INFO);
      }      
      break;

      case '6':
      {
         send_get_request(BDM_TO_WCD_GET_PORT_STATUS);
      }      
      break;

      case '7':
      {
         send_set_therapy_settings_wcd();
      }
      break;
      
      case '8':
      {
         logger.debug("---Get Blob Request---");
         send_get_blob_wcd(FIRST_DATA_BLOB_NUMBER);
      }
      break;

      case '9':
      {
         if (pBlobData)
         {         
            logger.debug("---Set Blob Request---");
            num_tx_set_data_blobs = num_rx_get_data_blobs;
            send_set_blob_wcd(FIRST_DATA_BLOB_NUMBER);
         }
         else
         {
            logger.error("There is no valid blob data to send to capital device - please do a get before trying a set");
         }
      }
      break;
               
      //////////////////////////
      //   Exit Serial RTOS   //
      //////////////////////////
      case 'q':
      case 'Q':
         DBGF("Exiting main program - user prompted\n");
         goto bdm_exit_task;
         break; // 'q' //

      default:
         DBGF("unknown input(%c)\n",cmdBuf[0]);
         break; // default //
      } // end switch command //
         
   } // end while not stopping
       
bdm_exit_task:
   logger.highlight("Exiting BDM(%d) - Close socket & wait for read thread to exit",common_system_state.stopping);
   common_system_state.stopping = true;
   common_close_socket(&serial_fd);
   pthread_join(threadID,NULL);
   pthread_join(hbTxThrd,NULL);
   logger.highlight("---- Exiting BDM_TASK -----\n");
   exit(0);
}

int main(int argc, char *argv[])
{
   if (argc > 1 && !strcmp(argv[1],"-V")) {
       printf("%s\n", version);
       return 0;
   }

   // check to make sure yo ugot a host //
   if ((argc < 2) || (argc > 3))
   {
      fprintf(stderr,"usage %s <COM Port> <(optional) 'Do CLI' (0/1(cli is enabled by default))>\n", argv[0]);
      exit(0);
   }
   else
      comport = argv[1];
    
   if (argc >= 3)
      doCli = atoi(argv[2]);
  
   return (bdmWW_task()); 
}

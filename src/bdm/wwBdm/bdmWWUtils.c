/**
 * @file bdmWWUtils.c
 * @brief bdm WW utils
 * @details
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <ctype.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>

#include "commonTypes.h"
#include "commonState.h"
#include "connUtils.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgSerial.h"
#include "msgWW.h"
#include "broker_api.h"
#include "bdmWWPrivate.h"

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/
extern pthread_mutex_t serialMsgLock;
extern pthread_mutex_t msgGetSetLock;
extern bool isWWConnected;
extern u32  txHeartbeatCount;
extern OSD_coords_t localCoords;

extern u8 waitDevInfo;
extern u8 waitGetPort;
extern u8 waitTherapySet;
extern u8 waitTherapyGet;
extern u8 waitErrorInfo;
extern u8 waitGetInfo;
extern u8 waitWandLife;
extern u8 waitAmbient;
extern u8 waitSetTherapy;

/********************************************************
/////////////////////////////////////////////////////////
*** GET_SET SETUP BLOB UTILITIES 
/////////////////////////////////////////////////////////
********************************************************/
/**
 * @fn      void send_set_blob_wcd(u8 whichBlob)
 * @brief   handles sending the next set setup blob segment
 * @param   whichBlob - which part of blob to send
 * @retval  n/a
 **************************************************************/
void send_set_blob_wcd(u8 whichBlob)
{  
   bdm_serial_msg_container_t txMsg;
   u8 dataSize;
   u8 *txData  = NULL;
   if (whichBlob == 1)
   {
      dataSize = (sizeof(blobHeader) + 1);
      txData   = (u8 *)calloc(dataSize,1);
      memcpy(&txData[1],&blobHeader, dataSize-1);
      logger.highlight("Data Blob Size <%d>\n",ntohs(blobHeader.blob_data_size));
   }
   else
   {
      assert(pBlobData);
      u16 blobIndex = ((whichBlob-2)*MAX_OPAQUE_DATA_PACKET);
      u8 *pSource    = &pBlobData[blobIndex];
      dataSize      = MAX_OPAQUE_DATA_PACKET+1;
      txData        = (u8 *)calloc(dataSize,1);
      memcpy(&txData[1],pSource,MAX_OPAQUE_DATA_PACKET);
   }
   
   txData[0] = whichBlob;
   txMsg.protocol_id             = PROTOCOL_BDM_TO_WCD;
   txMsg.command_id              = BDM_TO_WCD_SET_BLOB_CONFIG_REQUEST;
   txMsg.cmd_data                = txData;
   txMsg.cmd_data_len            = dataSize; // for blob number
   txMsg.request_number          = getNextTxSeqSerial();
   
   // build data payload //
   if (whichBlob == FIRST_DATA_BLOB_NUMBER)
   {
      display_blob(&blobHeader, pBlobData, txMsg.request_number);
   }   
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void send_get_blob_wcd(u8 whichBlob)
 * @brief   handles sending the next get setupblob request
 * @param   whichBlob - which part of blob to get
 * @retval  n/a
 **************************************************************/
void send_get_blob_wcd(u8 whichBlob)
{
   bdm_serial_msg_container_t txMsg;
   txMsg.protocol_id    = PROTOCOL_BDM_TO_WCD;
   txMsg.command_id     = BDM_TO_WCD_GET_BLOB_CONFIG_REQUEST;
   txMsg.cmd_data_len   = 1;
   txMsg.cmd_data       = &whichBlob;
   txMsg.request_number = getNextTxSeqSerial();

   // backup waiting //
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void send_set_setup_blob(void)
 * @brief   initiates set setup blob to WW (and sets flags)
 * @retval  n/a
 **************************************************************/
void send_set_setup_blob_wcd(void)
{
   pendingActions |= RESPOND_SET_SETUP_BLOB_CLIENT_MASK;
   pendingActions &= ~SEND_SET_SETUP_BLOB_WCD_MASK;
   send_set_blob_wcd(FIRST_DATA_BLOB_NUMBER);
}

/**
 * @fn      void send_get_setup_blob(void)
 * @brief   initiates set setup blob to WW (and sets flags)
 * @retval  n/a
 **************************************************************/
void send_get_setup_blob_wcd(void)
{
   pendingActions |= RESPOND_GET_SETUP_BLOB_CLIENT_MASK;
   pendingActions &= ~SEND_GET_SETUP_BLOB_WCD_MASK;
   send_get_blob_wcd(FIRST_DATA_BLOB_NUMBER);
}

/**
 * @fn      void respond_get_setup_blob_client(void)
 * @brief   sends a response to the setup blob client requesting the get
 * @retval  n/a
 ******************************x********************************/
pthread_mutex_t msgSetupBlobLock;
void respond_get_setup_blob_client(void)
{
   ct_ip_msg_t txMsg;
   u32 blobCopySize = ntohs(blobHeader.blob_data_size) + 1;
   u32 osdOffset    = blobCopySize + BLOB_DATA_HEADER_SIZE;
   u32 totalTx      = osdOffset + BLOB_COORDS_SIZE;
   
   // initialize message default (get)
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = SETUP_BLOB_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = SETUP_BLOB_GET_RESPONSE;
   txMsg.cmd_data_len        = totalTx;
   txMsg.cmd_sequence_number = getNextTxSeqSetupBlob();
   
   // allocate data to be sent //
   txMsg.cmd_data = calloc(totalTx+8,1); // 
   assert(txMsg.cmd_data != NULL);

   // copy data over
   memcpy(txMsg.cmd_data, &blobHeader, BLOB_DATA_HEADER_SIZE);
   memcpy(&txMsg.cmd_data[BLOB_DATA_HEADER_SIZE], pBlobData, blobCopySize);
   memcpy(&txMsg.cmd_data[osdOffset], &localCoords.portA_x, BLOB_COORDS_SIZE);
   
   // send the packet
   if ((build_send_ip_msg(setupBlobClientFd,&txMsg, &msgSetupBlobLock)))
   {
      logger.error("Failed to send set setup blob data response");
   }

   // free allocated memory //
   freeIpMsgContainerData(&txMsg);
   pendingActions &= ~RESPOND_GET_SETUP_BLOB_CLIENT_MASK;
}

/**
 * @fn      void respond_get_setup_blob_client(u8 setStatus)
 * @brief   responds with status to the setup blob client that sent 'set' request
 * @retval  n/a
 **************************************************************/
void respond_set_setup_blob_client(u8 setStatus)
{
   ct_ip_msg_t txMsg;
    
   // initialize message default (get)
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = SETUP_BLOB_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = SETUP_BLOB_SET_RESPONSE;
   txMsg.cmd_data_len        = SET_SETUP_BLOB_RESPONSE_SIZE;
   txMsg.cmd_sequence_number = getNextTxSeqSetupBlob();
   txMsg.cmd_data            = &setStatus;
   logger.debug("Sent Set Setblob Response (%d) to client",setStatus);

   // send the packet
   if ((build_send_ip_msg(setupBlobClientFd,&txMsg, &msgSetupBlobLock)))
   {
      logger.error("Failed to send set setup blob data response\n");
   }
   
   // reset the need to respond to set
   pendingActions &= ~RESPOND_SET_SETUP_BLOB_CLIENT_MASK;
}
/********************************************************
/////////////////////////////////////////////////////////
********************************************************/

/********************************************************
/////////////////////////////////////////////////////////
*** GET_SET UTILITIES 
/////////////////////////////////////////////////////////
********************************************************/
/**
 * @fn      void send_get_request(u8 getRequest)
 * @brief   handles sending a request to capdev
 * @param   getRequest - command ID to send
 * @retval  n/a
 **************************************************************/
void send_get_request(u8 getRequest)
{
   logger.debug("---Sending <%s>(%02X)---", cmdId2TxtWW(getRequest),getRequest);
   
   bdm_serial_msg_container_t txMsg;
   bzero(&txMsg, sizeof(txMsg));

   // use different flag for get generator info
   switch(getRequest)
   {
   case BDM_TO_WCD_GET_PORT_STATUS:
      waitGetPort++;
      break;
   case BDM_TO_WCD_GET_THERAPY_SETTINGS:
      waitTherapyGet++;
      break;
   case BDM_TO_WCD_GET_ERROR_INFO:
      waitErrorInfo++;
      break;
   case BDM_TO_WCD_GET_AMBIENT_SETTINGS:
      waitAmbient++;
      break;
   case BDM_TO_WCD_GET_WAND_LIFE_INFO:
      waitWandLife++;
      break;
   case BDM_TO_WCD_GET_GENERATOR_INFO:
      waitDevInfo++;
      break;
   default:
      logger.error("Unkonwn get request\n");
      return;
      break;
   } // end switch /
   
   txMsg.protocol_id    = PROTOCOL_BDM_TO_WCD;
   txMsg.command_id     = getRequest;
   txMsg.cmd_data_len   = 0;
   txMsg.request_number = getNextTxSeqSerial();
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void send_set_therapy_settings_wcd(void)
 * @brief   handles sending set therapy request
 * @retval  n/a
 **************************************************************/
extern wcd_therapy_settings_t    localTherapySettings;
void send_set_therapy_settings_wcd(void)
{
   // make sure that we don't have anything pending
   bdm_serial_msg_container_t txMsg;
   txMsg.protocol_id    = PROTOCOL_BDM_TO_WCD;
   txMsg.command_id     = BDM_TO_WCD_SET_THERAPY_SETTINGS;
   txMsg.cmd_data_len   = WCD_THERAPY_SETTINGS_DATA_LEN;
   txMsg.cmd_data       = (u8 *)&localTherapySettings;
   txMsg.request_number = getNextTxSeqSerial();
   add_backup_serial_msg(&txMsg, txHeartbeatCount+COMMAND_TIMEOUT_HB_COUNT);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
   
   logger.debug("Sent Set Therapy Command");
   waitSetTherapy++;
}

/**
 * @fn      void send_ack_for_cmd(u8 command_id, u8 requestNumber, u8 ackByte)
 * @brief   handles sending an ack for a command to capdev
 * @param   command_id - command id for message to wCD
 * @param   requestNumber - requestNumber associated with this request
 * @param   ackByte - status byte for request
 * @retval  n/a
 **************************************************************/
void send_ack_for_cmd(u8 command_id, u8 requestNumber, u8 ackByte)
{
   bdm_serial_msg_container_t txMsg;
   logger.debug("Sending Ack (%d) for Command <%s>", ackByte, cmdId2TxtWW(command_id));
   txMsg.protocol_id    = PROTOCOL_BDM_TO_WCD;
   txMsg.command_id     = command_id;
   txMsg.cmd_data       = (u8 *)&ackByte;
   txMsg.cmd_data_len   = ONE_BYTE_ACK_RESPONSE_DATA_LEN;
   txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      const char *cmdId2TxtWW(u8 cmdID)
 * @brief   debug utility to return ascii string representation of the binary command
 * @param   cmdID - command id to display
 * @retval  cmdID as a string
 **************************************************************/
const char *cmdId2TxtWW(u8 cmdID)
{
   switch (cmdID)
   {
   case CAPDEV_WW_GENERIC_REPLY:                 return "Generic Reply";
   case CAPDEV_WW_DISCOVERY:                     return "Discovery";
   case CAPDEV_WW_HEARTBEAT:                     return "Heartbeat";
   case CAPDEV_WW_PORT_STATUS:                   return "Port Status Info";
   case CAPDEV_WW_GET_PORT_STATUS:               return "Get Port Status";

   case CAPDEV_WW_SET_THERAPY_SETTINGS:          return "Set Therapy Settings";

   case CAPDEV_WW_THERAPY_SETTINGS_STATUS:       return "Therapy Settings";
   case CAPDEV_WW_GET_THERAPY_SETTINGS_STATUS:   return "Get Therapy Settings";

   case CAPDEV_WW_GET_ERROR_INFO:                return "Get Error Info";
   case CAPDEV_WW_GET_AMBIENT_SETTINGS:          return "Ambient Setting";
   case CAPDEV_WW_GET_WAND_LIFE_INFO:            return "Wand Life Info";
   case CAPDEV_WW_GET_GENERATOR_SERIAL_INFO:     return "Get Generator Info";
         
   case CAPDEV_WW_GET_CONFIG_BLOB:               return "Get Blob";
   case CAPDEV_WW_SET_CONFIG_BLOB:               return "Set Blob";
      
   default:                                      return "Unknown";
   }
}



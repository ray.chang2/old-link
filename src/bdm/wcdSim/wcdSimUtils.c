/**
 * @file scdSimUtils.c
 * @brief scd sim utils
 * @details
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <ctype.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>

#include "zmq.h"
#include "dsc.h"

#include "commonTypes.h"
#include "commonState.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgSerial.h"
#include "msgWW.h"
#include "connUtils.h"
#include "wcdSimPrivate.h"

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

extern pthread_mutex_t serialMsgLock;
extern pthread_mutex_t msgGetSetLock;
extern bool isWWConnected;

/********************************************************
/////////////////////////////////////////////////////////
*** GET_SET UTILITIES 
/////////////////////////////////////////////////////////
********************************************************/
/**
 * @fn      void send_therapy_settings_bdm(bool expectResponse, u8 requestNumber)
 * @brief   send therapy settings to bdm
 * @param   expectResponse - expecting a response
 * @param   requestNumber - request number for get resoinse 
 * @retval  n/a
 **************************************************************/
extern wcd_therapy_settings_t localTherapySettings;
extern bool waitGetPortStatus;
void send_therapy_settings_bdm(bool expectResponse, u8 requestNumber)
{
   bdm_serial_msg_container_t txMsg;
   
   logger.highlight("STS(%d/%d)",expectResponse,requestNumber);
   txMsg.protocol_id         = PROTOCOL_WCD_TO_BDM;
   txMsg.command_id          = WCD_TO_BDM_THERAPY_SETTINGS_STATUS;
   txMsg.cmd_data_len        = WCD_THERAPY_SETTINGS_DATA_LEN;
   txMsg.cmd_data            = (u8 *)&localTherapySettings;
   if (expectResponse)
   {
      txMsg.request_number = getNextTxSeqSerial();
      waitGetPortStatus = true;
   }
   else
   {
      txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
   }
   
   logger.debug("Sending [%02X]Therapy Settings Status to BDM exp<%d>", txMsg.request_number, expectResponse);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void send_port_status_bdm(bool expectResponse, u8 requestNumber);
 * @brief   n/a
 * @param   expectResponse - expecting a response
 * @param   requestNumber - request number if locally started
 * @retval  n/a
 **************************************************************/
extern wcd_bdm_port_status_t     localPortStatus;
void send_port_status_bdm(bool expectResponse, u8 requestNumber)
{
   
   bdm_serial_msg_container_t txMsg;
   txMsg.protocol_id         = PROTOCOL_WCD_TO_BDM;
   txMsg.command_id          = WCD_TO_BDM_PORT_STATUS;
   txMsg.cmd_data_len        = WCD_PORT_STATUS_DATA_LEN;
   txMsg.cmd_data            = (u8 *)&localPortStatus;
   if (expectResponse)
   {
      txMsg.request_number = getNextTxSeqSerial();
      waitGetPortStatus = true;
   }
   else
   {
      txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
   }
   
   logger.debug("Sending Port Status to BDM <%d>(%02X)", expectResponse, txMsg.request_number);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

void send_ack_for_cmd(u8 command_id, u8 requestNumber, u8 ackByte)
{
   bdm_serial_msg_container_t txMsg;
   logger.debug("Sending [%02X] Ack (%d) for <%s>", requestNumber, ackByte, cmdId2TxtWW(command_id));

   txMsg.protocol_id    = PROTOCOL_WCD_TO_BDM;
   txMsg.command_id     = command_id;
   txMsg.cmd_data       = (u8 *)&ackByte;
   txMsg.cmd_data_len   = ONE_BYTE_ACK_RESPONSE_DATA_LEN;
   txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
   build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
}

/**
 * @fn      void check_rx_sequence_setupBlob(u8 rxSeq, u8 cmdId)
 * @brief   handles checking of rx sequence from setupblob client
 * @param   rxSeq - the sequence received
 * @param   cmdId - the cmd associated with this request
 * @retval  n/a
 **************************************************************/
u8 expectedRxSetupBlobSeq = 1;
u32 rxExpCount = 0;
void check_rx_sequence_setupBlob(u8 rxSeq, u8 cmdId)
{
   if (rxSeq != expectedRxSetupBlobSeq)
   {
      if (0)//rxExpCount && !(rxExpCount % 8))
      {
         logger.debug("[%d]UnExp RX SEQ<%02X> EXP(%02X) - setupblob cmd<%d>",
                      rxExpCount, rxSeq, expectedRxSetupBlobSeq, cmdId);
      }
      rxExpCount++;
      
      if (rxSeq == 255)
      {
         expectedRxSetupBlobSeq = 1;
      }
      else
      {
         expectedRxSetupBlobSeq = rxSeq+1;
      }
   }
   else
   {
      if (expectedRxSetupBlobSeq == 255)
      {
         expectedRxSetupBlobSeq = 1;
      }
      else
      {
         expectedRxSetupBlobSeq++;
      }
   }
}


/**
 * @fn      u8 getNextTxSeqSetupBlob(void)
 * @brief   deals with next tx sequence
 * @retval  n/a
 **************************************************************/
u8 nextTxSetupBlobSeq     = 1;
u8 getNextTxSeqSetupBlob(void)
{
   u8 retValue = nextTxSetupBlobSeq;
   if (nextTxSetupBlobSeq == 255)
   {
      nextTxSetupBlobSeq = 1;
   }
   else
   {
      nextTxSetupBlobSeq++;
   }
   return (retValue);
}

// returns string of what the command was 
const char *cmdId2TxtWW(u8 cmdID)
{
   switch (cmdID)
   {
   case CAPDEV_WW_GENERIC_REPLY:                 return "Generic Reply";
   case CAPDEV_WW_DISCOVERY:                     return "Discovery";
   case CAPDEV_WW_HEARTBEAT:                     return "Heartbeat";
   case CAPDEV_WW_PORT_STATUS:                   return "Port Status Info";
   case CAPDEV_WW_GET_PORT_STATUS:               return "Get Port Status";

   case CAPDEV_WW_SET_THERAPY_SETTINGS:          return "Set Therapy Settings";

   case CAPDEV_WW_THERAPY_SETTINGS_STATUS:       return "Therapy Settings";
   case CAPDEV_WW_GET_THERAPY_SETTINGS_STATUS:   return "Get Therapy Settings";

   case CAPDEV_WW_GET_ERROR_INFO:                return "Get Error Info";
   case CAPDEV_WW_GET_AMBIENT_SETTINGS:          return "Ambient Setting";
   case CAPDEV_WW_GET_WAND_LIFE_INFO:            return "Wand Life Info";
   case CAPDEV_WW_GET_GENERATOR_SERIAL_INFO:     return "Get Generator Info";
         
   case CAPDEV_WW_GET_CONFIG_BLOB:               return "Get Blob";
   case CAPDEV_WW_SET_CONFIG_BLOB:               return "Set Blob";
      
   default:                                      return "Unknown";
   }
}



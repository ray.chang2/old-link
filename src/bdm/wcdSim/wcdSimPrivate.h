/**
 * @file wcdSimPrivate.h
 * @brief private include for utils directory
 *
 * @version .01
 * @author  Tom Morrison
 * @date 12/15/18
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver	Who  Date	  Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   12/15/18   Initial Creation.
 *
 *</pre>
 *
 * @todo  list to do items
 * @todo  list to do items
 */

#ifndef __WCD_SIM_PRIVATE_H__
#define __WCD_SIM_PRIVATE_H__

#include "commonTypes.h"   /**< must include this for typedefs below     */
#include "msgGetSetCommon.h"
#include "msgWW.h"

/**
 * private/local variables
 */
extern common_state_t common_system_state;

extern int  serial_fd;

extern u8  numberRetries;
extern bdm_serial_msg_container_t pendingWcdCmd;
extern u8 *pendingTxData;

// blob related variables flags //
extern ct_blob_data_header_t  localBlobHeader;
extern u8 *localBlobData;

// this is related to transmit / receive operations 
extern u8 expectedNextBlobNumberRx, expectedBlobNumberTxAck;
extern u8 totalBlobNumberRx, totalBlobNumberTx;
extern u16 bytesBlobRead;
extern u32 bytesBlobSent, bytesLeftTx;

#define LED_ON  1
#define LED_OFF 0

extern bdm_serial_msg_container_t pendingWcdCmd;
void turn_serial_led(int led_status);
void backup_wcd_txMsg(pMsgSerial txMsg);
void check_rx_sequence_setupBlob(u8 rxSeq, u8 cmdId);
void send_port_status_bdm(bool expectResponse, u8 requestNumber);
void send_therapy_settings_bdm(bool expectResponse, u8 requestNumber);
int getStringStatus(const char *prompt, char *copyTo, u8 maxSize);
const char *cmdId2TxtWW(u8 cmdID);
void send_ack_for_cmd(u8 command_id, u8 requestNumber, u8 ackByte);

#endif // __WCD_SIM_PRIVATE_H__

/**
 * @file wcdSim.c
 * @brief scd sim for WW
 * @details
 *
 * @version .01
 * @author  Tom Morrison
 * @date 11/10/18
 *
 *
 *<pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.01 tm   11/10/18   Initial Creation.
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <assert.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#include <linux/types.h>
#include <termios.h>

#include "dsc.h"

#include "commonTypes.h"
#include "commonState.h"
#include "cliCommon.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgSerial.h"
#include "msgIp.h"
#include "msgWW.h"
#include "connUtils.h"
#include "wcdSimPrivate.h"

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/

/**
 * static/private/local variables
 */
int  serial_fd           = -1;

pthread_mutex_t serialMsgLock;

bool isWWConnected       = false;
bool isWWReady           = false;
u32  rxHeartbeatCount    = 0;
u32  txHeartbeatCount    = 0;
u32  rxHeartbeatMiss     = 0;
u8   expectedCmdSequence = 1;

extern int doRxCrc;

/*
 * dispatch table for serial commands 
 */
#define NUM_SUPPORTED_WCD_BDM_CMDS 15
//#define NUM_SUPPORTED_WCD_BDM_CMDS 11
ct_exp_cmds_t bdmRxCmds[NUM_SUPPORTED_WCD_BDM_CMDS] =
{
      // standard stuff /
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_GENERIC_RESPONSE,          GENERIC_REPLY_DATA_LENGTH            },    // 3 byte status
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_DISCOVERY_REQUEST,         BDM_TO_CAPDEV_DISCOVERY_REQ_LEN      },    // disc resp
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_HEARTBEAT_CMD,             HB_BDM_WCD_DATA_LENGTH               },    // hb status
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_PORT_STATUS_RESP,          BDM_TO_WCD_PORT_STATUS_RESP_DATA_LEN },    // port status
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_GET_PORT_STATUS,           BDM_TO_WCD_GET_PORT_STATUS_DATA_LEN  },    // port status
   
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_SET_THERAPY_SETTINGS,      WCD_THERAPY_SETTINGS_DATA_LEN        },    // Set Therapy Request
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_THERAPY_STATUS_RESPONSE,   ONE_BYTE_ACK_RESPONSE_DATA_LEN       },    // ack therapy status sent async
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_GET_THERAPY_SETTINGS,      ZERO_LENGTH_MSG_DATA                 },    // get therapy request

   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_GET_ERROR_INFO,            ZERO_LENGTH_MSG_DATA                 },    // get therapy request
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_GET_AMBIENT_SETTINGS,      ZERO_LENGTH_MSG_DATA                 },    // get therapy request
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_GET_WAND_LIFE_INFO,        ZERO_LENGTH_MSG_DATA                 },    // get therapy request
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_GET_GENERATOR_INFO,        ZERO_LENGTH_MSG_DATA                 },    // get therapy request

   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_GET_BLOB_CONFIG_REQUEST,   GET_BLOB_DATA_REQUEST_DATA_LEN       },    // Get Blob Request   
   {PROTOCOL_BDM_TO_WCD, BDM_TO_WCD_SET_BLOB_CONFIG_REQUEST,   VARIABLE_LENGTH_MSG_DATA_SIZE        },    // Set Blob (variable length)
};

/////////////////////////////////
////////// local cache //////////
/////////////////////////////////
bool waitGetPortStatus   = false;
bool isConfigLock        = false;

pthread_mutex_t localConfigLock;

capdev_to_bdm_discovery_t localWWDiscoveryData;
hb_wcd_bdm_t              localTxHBStatus;
hb_bdm_wcd_t              localRxHBStatus;
wcd_bdm_port_status_t     localPortStatus;
wcd_therapy_settings_t    localTherapySettings;

generator_info_t          localGenInfo;
wand_life_info_t          localWandLife;
ambient_settings_t        localAmbientSettings;
error_notification_t      localErrorInfo;

// blob related variables flags //
ct_blob_data_header_t  blobHeader;
u8 pBlobData[1392];// = NULL;

u8 expectedNextBlobNumberRx, expectedBlobNumberTxAck;
u8 totalBlobNumberRx, totalBlobNumberTx;
u16 bytesBlobRead;
u32 bytesBlobSent, bytesLeftTx;

static u32 num_rx_data_blobs = 0;

u8 errorCode = 0;
////////////////////////////////////
// initialize data that is cached //
////////////////////////////////////
void init_blob_data(void)
{
   int j;
   bzero((u8 *)&blobHeader,sizeof(blobHeader));
   blobHeader.prot_version     = 0x10;
   blobHeader.blob_header_size = sizeof(ct_blob_data_header_t);
   blobHeader.blob_data_size   = ntohs(WW_BLOB_DATA_SIZE);
   blobHeader.deviceType       = DEVICE_TYPE_SHAVER;
   blobHeader.subDeviceType    = 1;

   // set blob name //
   char *buf = (char *)blobHeader.blob_name;
   snprintf(buf,16,"WEREWOLF123");

   // alloc blob data //
//   pBlobData = (u8 *)calloc(WW_TOTAL_BLOB_DATA_SIZE+1,1); // include the crc
   bzero(pBlobData,sizeof(pBlobData));
   for (j = 0 ; j < WW_BLOB_DATA_SIZE ; j++)
      pBlobData[j] = (u8)(j+11);

   blobHeader.header_crc       = calculate_array_crc(BLOB_DATA_HEADER_CRC_SIZE, (u8 *)&blobHeader);
   pBlobData[WW_BLOB_DATA_SIZE] = calculate_array_crc(WW_BLOB_DATA_SIZE, (u8 *)pBlobData);
   display_blob(&blobHeader, pBlobData, 0x66);
   num_rx_data_blobs = (WW_BLOB_DATA_SIZE / MAX_OPAQUE_DATA_PACKET) + 2;
}

///////////////////////////////////
// initialize data //
///////////////////////////////////
void init_local_data(void)
{
   // initialize local data lock
   pthread_mutex_init(&localConfigLock,NULL);
   isConfigLock = true;

   // reset both of these //
   bzero(&localWWDiscoveryData, sizeof(localWWDiscoveryData));
   localWWDiscoveryData.version_number = 0x21;
   localWWDiscoveryData.device_state   = 1;
   localWWDiscoveryData.devType        = 2;
   localWWDiscoveryData.devSubType     = 3;

   bzero(&localTherapySettings,sizeof(localTherapySettings));
   bzero(&localTxHBStatus, sizeof(localTxHBStatus));
   
   localTherapySettings.ab_coag_smart_sp = localTxHBStatus.ab_coag_smart_sp = 3;
   localTherapySettings.ab_coag_18_sp    = localTxHBStatus.ab_coag_18_sp    = 18;
   localTherapySettings.amb_thres_sp     = localTxHBStatus.amb_thres_sp     = 5;
   localTherapySettings.gen_set          = localTxHBStatus.gen_set          = 6;
   localTherapySettings.amb_set          = localTxHBStatus.amb_set          = 7;
   localTherapySettings.reflex_timer     = 13;
   localTherapySettings.pump_settings    = 14;
   localTherapySettings.therapy_settings = 15;

   localTxHBStatus.devStatus             = 1;
   localTxHBStatus.wandInfo              = 2;
   localTxHBStatus.therapy_set           = 4;
   localTxHBStatus.amb_temp              = 8;
   localTxHBStatus.leg_amb_set           = 21;
   localTxHBStatus.leg_amb_temp          = 22;
   localTxHBStatus.error_priority_code   = 32;

   
   bzero(&localRxHBStatus,      sizeof(localRxHBStatus));
   
   bzero(&localPortStatus,      sizeof(localPortStatus));
   strncpy((char *)localPortStatus.wand_name,"WCDWAND_2", 12);
   localPortStatus.foot_info          = 0x11;
   localPortStatus.wand_info          = 0x22;
   localPortStatus.legacy_therapy_max = 0x33;
   localPortStatus.wand_cap_info      = 0x24;
   localPortStatus.wand_lot_num       = 0x44;
   localPortStatus.wand_id_num        = 0x55;

   bzero(&localGenInfo,         sizeof(generator_info_t));
   strncpy((char *)localGenInfo.serialNum, "Ser1234568764", 16);
   localGenInfo.app_version_major = 'A';
   localGenInfo.app_version_minor = '1';
   localGenInfo.gui_version_major = 'B';
   localGenInfo.gui_version_minor = '2';

   bzero(&localWandLife,        sizeof(wand_life_info_t));
   localWandLife.wand_life_low  = 0x1221;
   localWandLife.wand_life_med  = 0x1222;
   localWandLife.wand_life_high = 0x1223;
   localWandLife.wand_life_vac  = 0x1224;
   localWandLife.wand_life_coag = 0x1225;
   
   bzero(&localAmbientSettings, sizeof(ambient_settings_t));
   localAmbientSettings.amb_set       = 0xA0;
   localAmbientSettings.amb_temp      = 0xA1;
   localAmbientSettings.handle_temp   = 0xA2;
   localAmbientSettings.amb_thresh_sp = 0xA3;
   localAmbientSettings.leg_amb_set   = 0xA4;
   localAmbientSettings.leg_amb_temp  = 0xA5;
      
   bzero(&localErrorInfo, sizeof(error_notification_t));
   localErrorInfo.errors_priority = 0x32;
   localErrorInfo.misc_errors     = 0x68;
   
   // header information
   init_blob_data();
}

/**
 * @fn      void *bdm_tx_heartbeat(void *args)
 * @brief   sends heartbeats to bdm
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
u32 waiting4DiscoveryCount = 0;
void *bdm_tx_heartbeat(UNUSED_ATTRIBUTE  void *args)
{
   u32 waitRespCount = 0;
    
   while (!common_system_state.stopping)
   {
      switch (common_system_state.state)
      {
      default:
         logger.error("Unknown state <%02X> for in heartbeat task",
                      common_system_state.state);
         exit(-22);
         break;
            
      case WAITING_FOR_DISCOVERY_COMPLETION:
         waiting4DiscoveryCount++;
         if (!(waiting4DiscoveryCount % 50000))
         {
            DBGF("\n\t...Still Waiting for Discovery Request <%u>...\n",
                 waiting4DiscoveryCount);
         }
         YIELD_100MS;
         continue;
         break;

      case WAITING_CMD_RESPONSE:
         waitRespCount++;
         if (waitRespCount < 10)
         {
            YIELD_100MS;
            continue;
         }
         else
         {
            DBGF("\t...WCD Timeout waiting for Command <%02X>... Start sending HB again\n",common_system_state.waitCmdID);
            common_system_state.state = DISCOVERED_IDLE;
         }
         break;
      case DISCOVERED_IDLE:
         waitRespCount = 0;
         break;
      }
        
      // send it here //
      if (common_system_state.state != WAITING_FOR_DISCOVERY_COMPLETION)
      {
         if (++rxHeartbeatMiss > MAX_NUMBER_RX_TIMEOUT_MISS)
         {
            if (isWWConnected)
               DBGF("\n\tLOSS of BDM Heartbeat Connection - waiting for Discovery\n");
            isWWConnected = false;
            common_system_state.state = WAITING_FOR_DISCOVERY_COMPLETION;
         }
      } // if we aren't in discovery
      YIELD_100MS;
   }
   return NULL;
}

/**
 * @fn      int process_bdm_to_wcd_messages(pMsg rxMsg)
 * @brief   processes rx messages for rtos 
 * @param   [in]  rxMsg - message to be interpreted
 * @retval  returns 0 on success, otherwise, it error
 */
int process_bdm_to_wcd_messages(pMsgSerial rxMsg)
{
   int status = 0;

   //////////////////////////////////
   // first validate sequence      //
   // re-sync if necessary         //
   //////////////////////////////////
   u8 requestNumber = rxMsg->request_number;

   // determine if proper protocol (should be)
   bdm_serial_msg_container_t txMsg;
   txMsg.protocol_id  = PROTOCOL_WCD_TO_BDM;
   
   if (rxMsg->protocol_id == PROTOCOL_BDM_TO_WCD)
   {
      switch(rxMsg->command_id)
      {
      //////////////////////////////////////////
      //////////////////////////////////////////
      //////////////////////////////////////////
      case BDM_TO_WCD_GET_BLOB_CONFIG_REQUEST:
      {
         u8 whichBlob         = rxMsg->cmd_data[0];
         txMsg.command_id     = WCD_TO_BDM_GET_BLOB_CONFIG_RESPONSE;
         txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
         logger.debug("Get Blob (%d) Request(%02X)",whichBlob, requestNumber);
         
         u8 dataSize;
         u8 *txData  = NULL;
         if (whichBlob == 1)
         {
            dataSize = (sizeof(blobHeader) + 1);
            txData   = (u8 *)calloc(dataSize,1);
            assert(txData);
            memcpy(&txData[1],&blobHeader, dataSize-1);            
         }
         else
         {
//            assert(pBlobData);
            u16 blobIndex = ((whichBlob-2)*MAX_OPAQUE_DATA_PACKET);
            u8 *pSource    = &pBlobData[blobIndex];
            dataSize      = MAX_OPAQUE_DATA_PACKET+1;
            txData        = (u8 *)calloc(dataSize,1);
            memcpy(&txData[1],pSource,MAX_OPAQUE_DATA_PACKET);
         }

         txData[0]          = whichBlob;
         txMsg.cmd_data     = txData;
         txMsg.cmd_data_len = dataSize; 
   
         // build data payload //
         if (whichBlob == FIRST_DATA_BLOB_NUMBER)
         {
            display_blob(&blobHeader, pBlobData, txMsg.request_number);
         }   
         logger.debug("---Send Get Blob Response <%d> to BDM (size(%d)) ---", whichBlob, dataSize);
         build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
         free(txData);
      }
      break;

      case BDM_TO_WCD_SET_BLOB_CONFIG_REQUEST:
      {
         u8 txData[2];
         u8 whichBlob = rxMsg->cmd_data[0];
         u8 *pData    = &rxMsg->cmd_data[1];
         u8 dataSize  = rxMsg->cmd_data_len-1;
         logger.debug("[%02X]Set Blob(%d)(%d)",requestNumber, whichBlob,num_rx_data_blobs);
         
         txMsg.command_id     = WCD_TO_BDM_SET_BLOB_CONFIG_RESPONSE;
         txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
         txData[0]            = whichBlob;
         txData[1]            = 0;
         txMsg.cmd_data       = txData;
         txMsg.cmd_data_len   = 2;
         if (whichBlob == 1)
         {
            assert(dataSize == sizeof(blobHeader));
            memcpy(&blobHeader, pData, dataSize);
            bzero(pBlobData,sizeof(pBlobData));
         } // if first blob
         else
         {
            u8 blobIndex   = (whichBlob-2);
            u16 dataOffset = (blobIndex * MAX_OPAQUE_DATA_PACKET);
            assert(pBlobData);
            assert(dataSize == MAX_OPAQUE_DATA_PACKET);
            memcpy(&pBlobData[dataOffset], pData, MAX_OPAQUE_DATA_PACKET);
            if (whichBlob == num_rx_data_blobs)
            {
               display_blob(&blobHeader, pBlobData, txMsg.request_number);
            }   
         } // not first blob
         if (errorCode)
         {
            if (errorCode == 9)
            {
               txData[1] = 1;
            }
            else if (errorCode == 10)
            {
               txMsg.request_number -= 1;
            }
            else if (errorCode == 11)
            {
               txData[0]++;
            }
            else if (errorCode == 12)
            {
               txData[1] = 1;
            }
         }                  
         build_send_serial_msg(serial_fd, &txMsg, &serialMsgLock);
         errorCode = 0;
      }
      break;
      //////////////////////////////////////////
      //////////////////////////////////////////
      //////////////////////////////////////////

   //////////////////////////////////////////
   //////////////////////////////////////////
   //////////////////////////////////////////
   case BDM_TO_WCD_GENERIC_RESPONSE:
   {
      pGenReply pReply = (pGenReply)rxMsg->cmd_data;
      if (common_system_state.state != WAITING_CMD_RESPONSE)
      {
         logger.error("Unsolicited GenAck Response <%02X>",pReply->status);
      }

      logger.debug("Got Gen NACK Status <%d> in <%d> state for Cmd <%02X>", pReply->status, common_system_state.state,pReply->cmd_id);
              
      common_system_state.state   = DISCOVERED_IDLE;
      common_system_state.waitCmd = false;
      common_system_state.waitCmdID = 0;
   }
   break;
            
   case BDM_TO_WCD_DISCOVERY_REQUEST:
   {
      u8 version = *rxMsg->cmd_data;
      if (common_system_state.state >=  DISCOVERED_IDLE)
      {
         logger.warning("Unexpected discovery request <%02X> in <%d>- responding...",
                        version, common_system_state.state);
         if (errorCode)
         {
            logger.highlight("turning off port status");
            errorCode = 0;
         }
         isWWConnected = false;
      }
      else
      {
         logger.info("...EXPECTED Discovery request Version <%02X> ...", version);
      }
            
      txMsg.command_id     = WCD_TO_BDM_DISCOVERY_RESPONSE;
      txMsg.cmd_data_len   = CAPDEV_TO_BDM_DISCOVERY_RESP_LENGTH;
      txMsg.cmd_data       = (u8 *)&localWWDiscoveryData;
      txMsg.request_number = getNextTxSeqSerial();
      build_send_serial_msg(serial_fd, &txMsg,&serialMsgLock);

      common_system_state.state = DISCOVERED_IDLE;
      waiting4DiscoveryCount    = 0;
      rxHeartbeatMiss           = 0;
      rxHeartbeatCount          = 0;
   }
   break;
            
   case BDM_TO_WCD_HEARTBEAT_CMD:
   {
      // increment receive right now //
      rxHeartbeatCount++;
      rxHeartbeatMiss = 0;
      if (isWWConnected == false)
      {
         logger.debug("BDM is (RE)connected");
      }
              
      isWWConnected = true;                  
      memcpy(&localRxHBStatus, rxMsg->cmd_data, sizeof(localRxHBStatus));

      txMsg.command_id     = WCD_TO_BDM_HEARTBEAT_CMD;
      txMsg.cmd_data_len   = HB_WCD_BDM_DATA_LENGTH;
      txMsg.cmd_data       = (u8 *)&localTxHBStatus;
      txMsg.request_number = getNextTxSeqSerial();
      build_send_serial_msg(serial_fd, &txMsg,&serialMsgLock);
   } // if heartbeat //
   break; 

   case BDM_TO_WCD_PORT_STATUS_RESP:
   {
      
      logger.debug("WCD Port Status(%d) RESPONSE <%s>",
                   waitGetPortStatus, rxMsg->cmd_data[0] ? "Failure" : "Success");
      waitGetPortStatus = false;
   }
   break;

   case BDM_TO_WCD_GET_PORT_STATUS:
   {
      logger.debug("WCD Port Status_REQUEST(%d)",errorCode);
      if (errorCode)
      {
         logger.highlight("IGNORNING port status for timeout testing");
      }
      else
      {
         send_port_status_bdm(false, requestNumber);
      }
      errorCode = 0;
   }
   break;

   case BDM_TO_WCD_SET_THERAPY_SETTINGS:
   {
      logger.debug("Got Set Therapy Request - send response");
      memcpy(&localTherapySettings, rxMsg->cmd_data, WCD_THERAPY_SETTINGS_DATA_LEN);
      localTxHBStatus.gen_set            = localTherapySettings.gen_set;
      localTxHBStatus.amb_set            = localTherapySettings.gen_set;
      localTxHBStatus.ab_coag_smart_sp   = localTherapySettings.ab_coag_smart_sp;
      localTxHBStatus.ab_coag_18_sp      = localTherapySettings.ab_coag_18_sp;
      localTxHBStatus.amb_thres_sp       = localTherapySettings.amb_thres_sp;
      send_ack_for_cmd(WCD_TO_BDM_SET_THERAPY_RESPONSE, requestNumber, 0);
//      send_port_status_bdm(true,0);
   }
   break;

   case BDM_TO_WCD_THERAPY_STATUS_RESPONSE:
   {
      logger.debug("WCD Therapy Status_RESPONSE <%s>",rxMsg->cmd_data[0] ? "Failure" : "Success");
   }
   break;
      
   case BDM_TO_WCD_GET_THERAPY_SETTINGS:
   {
      logger.debug("Get Therapy Settings Request(%d/%p)",rxMsg->cmd_data_len, rxMsg->cmd_data);
      send_therapy_settings_bdm(false,requestNumber);
   }
   break;
      
   case BDM_TO_WCD_GET_ERROR_INFO:
   {
      logger.debug("Get Error Info(02X)(%d/%p)",requestNumber,rxMsg->cmd_data_len, rxMsg->cmd_data);
      txMsg.command_id     = WCD_TO_BDM_GET_ERROR_INFO_RESP;
      txMsg.cmd_data_len   = WCD_ERROR_STATUS_DATA_LEN;
      txMsg.cmd_data       = (u8 *)&localErrorInfo;
      txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
      build_send_serial_msg(serial_fd, &txMsg,&serialMsgLock);
   }
   break;

   case BDM_TO_WCD_GET_AMBIENT_SETTINGS:
   {
      logger.debug("Get Ambient Settings(%d/%p)",rxMsg->cmd_data_len, rxMsg->cmd_data);
      txMsg.command_id     = WCD_TO_BDM_GET_AMBIENT_SETTINGS_RESP;
      txMsg.cmd_data_len   = WCD_AMBIENT_STATUS_DATA_LEN;
      txMsg.cmd_data       = (u8 *)&localAmbientSettings;
      txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
      build_send_serial_msg(serial_fd, &txMsg,&serialMsgLock);
   }
   break;

   case BDM_TO_WCD_GET_WAND_LIFE_INFO:
   {
      logger.debug("Get Wand Life(%d/%p)",rxMsg->cmd_data_len, rxMsg->cmd_data);
      txMsg.command_id     = WCD_TO_BDM_GET_WAND_LIFE_INFO_RESP;
      txMsg.cmd_data_len   = WAND_LIFE_INFO_DATA_LEN;
      txMsg.cmd_data       = (u8 *)&localWandLife;
      txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
      build_send_serial_msg(serial_fd, &txMsg,&serialMsgLock);
   }
   break;
      
   case BDM_TO_WCD_GET_GENERATOR_INFO:
   {
      logger.debug("Get Generator Info(%d/%d/%p)",rxMsg->cmd_data_len, rxMsg->request_number, rxMsg->cmd_data);
      txMsg.command_id     = WCD_TO_BDM_GET_GENERATOR_INFO_RESP;
      txMsg.cmd_data_len   = GENERATOR_SERIAL_INFO_DATA_LEN;
      txMsg.cmd_data       = (u8 *)&localGenInfo;
      txMsg.request_number = (requestNumber & REQUEST_NUMBER_RESPONSE_BIT_MASK);
      build_send_serial_msg(serial_fd, &txMsg,&serialMsgLock);
   }
   break;

      default:
         logger.warning("Unexpected BDM to WCD Command (%d) received",rxMsg->command_id);
         break;
      } // end switch //
   } // end if usb_to_rtos protocol 
   else
   {
      logger.debug("unknown protocol (%02x) for rxMsg (%02x)",rxMsg->protocol_id, rxMsg->command_id);
      status = -6;
   } //else unknown type //

   return status;
}

/**
 * @fn      int bdmWcd_task(void)
 * @brief   bdm task for all bridge processing
 * @retval  NULL (exits when told to)
 */
extern char *comport;
int bdmWcd_task(void)
{
   pthread_t threadID;
   char cmdBuf[4];
     
   // init common signal handler
   init_common_signal_handler();
     
   // reset structs */
   bzero(&common_system_state,sizeof(common_system_state));

   // initialize serial lock and serial date
   pthread_mutex_init(&serialMsgLock,NULL);
   init_local_data();
     
   // register setup serial port 
   if ((init_serial_connection(&serial_fd, comport, 0)))
   {
      logger.error("RTOS Error in initializing Serial Connection");
      goto bdm_exit_task;
   } // error in bdm //
     
   logger.debug("Serial Port Initialized <%d>",serial_fd);

   // create data to pass between threads //
   pSerialRxArgs pArgs;
   if ((pArgs = calloc(1,sizeof(serial_rx_args_t))) == NULL)
   {
      logger.error("Failed to calloc rx Serial args: <%s>",COMMON_ERR_STR);
      goto bdm_exit_task;
   }

   // create a generic socket read thread //
   pArgs->serialProcessData = process_bdm_to_wcd_messages;
   pArgs->serial_fd         = &serial_fd;
   pArgs->pCmds             = &bdmRxCmds[0];
   pArgs->numCmds           = NUM_SUPPORTED_WCD_BDM_CMDS;
   pArgs->stopFlag          = &common_system_state.stopping;
   if ((common_create_thread(&threadID, HIGHEST_PRIORITY, (void *)pArgs, serial_rx)))
   {
      logger.error("Error in creating serial_ messages thread");
      goto bdm_exit_task;
   }

   //create heartbeat // 
   pthread_t hbTxThrd;
   if ((common_create_thread(&hbTxThrd, MEDIUM_LOW, NULL, bdm_tx_heartbeat)))
   {
      logger.error("Error in creating TX CMD SOCKET Thread");
      goto bdm_exit_task;  
   }

   // enter main command loop //
   YIELD_500MS;
   YIELD_500MS;

   u16 blobSize = ntohs(blobHeader.blob_data_size);
   while (!common_system_state.stopping)
   {
      bzero(cmdBuf,sizeof(cmdBuf));
      DBG("\t   ------------Serial wcd SIM CLI (%d/%d blob size)-----------\n\n"  
          "\t   ---------------------------------------\n"
          "\t   6) Send Port Status from WCD\n"
          "\t   7) Send Therapy Settings from WCD\n"
          "\t   q) Quit - exit entire program",
          (blobSize),
          ((blobSize/MAX_OPAQUE_DATA_PACKET)+2))
      DBG("\n\t----------------------------------------------------------\n");      


      if ((getStringStatus("",cmdBuf,sizeof(cmdBuf))))
         goto bdm_exit_task;


      // determine request //
      //  bool yesNotNo = true;
      //         yesNotNo = true;
      switch (cmdBuf[0])
      {
         ///////////////////////
         // Change Log Levels //
         ///////////////////////
      case '6':
      {
         logger.debug("---Sending PORT STATUS (async)---");
         send_port_status_bdm(true,0);
      }      
      break;

      case '7':
      {
         logger.debug("---Sending Therapy Settings STATUS (async)---");
         send_therapy_settings_bdm(true,0);
      }
      break;

      case '8':
      {
         logger.highlight("--- ignore next get port status ----");
         errorCode = 1;
      }
      break;
      case '9':
      {
         logger.highlight("--- send nack for command ----");
         errorCode = 9;
      }
      break;
      case 'a':
      {
         logger.highlight("--- send wrong request Number for get command ----");
         errorCode = 10;
      }
      break;
      case 'b':
      {
         logger.highlight("--- send wrong blob ----");
         errorCode = 11;
      }
      break;

      case 'c':
      {
         logger.highlight("--- send nack for set blob ----");
         errorCode = 12;
      }
      break;
      case 'd':
      {
         logger.highlight("--- force timeout for get port status ----");
         errorCode = 13;
      }
      break;
      //////////////////////////
      //   Exit Serial RTOS   //
      //////////////////////////
      case 'q':
      case 'Q':
         DBGF("Exiting main program - user prompted\n");
         goto bdm_exit_task;
         break; // 'q' //

      default:
         DBGF("unknown input(%c)\n",cmdBuf[0]);
         break; // default //
      } // end switch command //
         
   } // end while not stopping
       
bdm_exit_task:
   logger.highlight("Exiting BDM(%d) - Close socket & wait for read thread to exit",common_system_state.stopping);
   common_system_state.stopping = true;
   common_close_socket(&serial_fd);
   pthread_join(threadID,NULL);
   pthread_join(hbTxThrd,NULL);
   logger.highlight("---- Exiting BDM_TASK -----\n");
   exit(0);
}

char *comport;
int main(int argc, char *argv[])
{
   // check to make sure yo ugot a host //
   if (argc != 2)
   {
      fprintf(stderr,"usage %s <COM Port>\n", argv[0]);
      exit(0);
   }
   else
      comport = argv[1];
    
   return (bdmWcd_task()); 
}

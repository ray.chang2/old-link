/**
 * @file getSetD2.c
 * @brief getset server for D2
 * @details
 *
 * @version 0.1.0
 * @author  Tom Morrison
 * @date    8/16/19
 *
 *
 *<pre>
 *
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -----------------------------------------------
 * 00.00.01 tm   1/12/19   Initial Creation.
 * 00.01.00 tm   8/16/19   Release for code review
 *
 *</pre>
 *
 */

/****************************** Include Files *******************************/
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include <sys/types.h>
#include <linux/types.h>

#include "commonTypes.h"
#include "commonState.h"
#include "logger.h"
#include "msgCommon.h"
#include "msgIp.h"
#include "msgGetSetD2.h"
#include "broker_api.h"
#include "D2_channels.h"

#include "dsc.h"

/***************************** Type Definitions *****************************/
/****************** Macros (Inline Functions) Definitions *******************/
/*************************** Function Prototypes ****************************/
/*************************** Variable Definitions ***************************/
/**
 * static/private/local variables
 */
static const char* version = "01.00.00"; //follows major.minor.patch scheme(https://semver.org)
int  getSetClientFd      = -1;
u8   expectedCmdSequence = 1;

// public publish handle
void *pHandlePub;

// indicates if we are using cli or not
int doCli = 1;

/**
 * global variable(s)
 */
extern common_state_t common_system_state;

// dispatch
ct_exp_cmds_t bdmIpGetSetCmds[2] =
{
   {GET_SET_PROTOCOL_CLIENT_TO_BDM, CLIENT_BDM_GET_CMD,  IP_GET_DATA_REQUEST_SIZE},
   {GET_SET_PROTOCOL_CLIENT_TO_BDM, CLIENT_BDM_SET_CMD,  SET_REQ_DATA_SIZE_D2},
};

/////////////////////////////////
////////// local cache //////////
/////////////////////////////////
scd_bdm_port_status_t  localPortStatus;
u32 portStatusSize  = sizeof(localPortStatus);
bool isD2Connected = false;

// local thread for receive socket
pthread_mutex_t msgGetSetLock;

/**
 * @fn      void init_local_data(void)
 * @brief   initializes local data
 * @param   n/a
 * @retval  n/a
 **************************************************************/
void init_local_data(void)
{
   // reset both of these //
   bzero(&localPortStatus, sizeof(localPortStatus));
   isD2Connected = false;
   
   if ((pHandlePub = bm_publisher()) == NULL)
   {
      logger.error("Failure to create publisher handler");
   }
   void *context = zmq_ctx_new();
   zmq_ctx_destroy(context);
}

/**
 * @fn      void respond_set_request_client(u8 setStatus)
 * @brief   responds to client with set command status
 * @retval  n/a
 **************************************************************/
void respond_set_request_client(u8 setStatus, u8 clientType)
{
   int status;
   ct_ip_msg_t txMsg;
   ctn_set_resp_cmd_t setResp;
        
   // build response 
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = GET_SET_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = BDM_CLIENT_SET_RESPONSE;
   txMsg.cmd_data_len        = SET_REQ_RESPONSE_SIZE;
   txMsg.cmd_data            = (u8 *)(&setResp);
   txMsg.cmd_sequence_number = getNextTxGetSetSeq();
   
   setResp.clientDeviceType  = clientType;
   setResp.serverDeviceType  = DEVICE_TYPE_SHAVER;
   setResp.setStatus         = setStatus;
   logger.highlight("D2BDM Sent SET Response (%d) with Sequence number (%d)", setStatus, txMsg.cmd_sequence_number);
   if ((status = build_send_ip_msg(getSetClientFd,&txMsg, &msgGetSetLock)) != 0)
   {
      logger.error("Error (%d) for sending a SetDeviceResponse to GetSet client",status);
   }
}

/**
 * @fn      void respond_get_request_client(u8 setStatus)
 * @brief   responds with get status to the client 
 * @retval  n/a
 **************************************************************/
void respond_get_request_client(u8 getStatus, u8 clientType)
{
   int status;
   ct_ip_msg_t txMsg;
   ctn_get_req_response_t respData;
   
   RESET_MSG_IP(&txMsg);
   txMsg.protocol_id         = GET_SET_PROTOCOL_BDM_TO_CLIENT;
   txMsg.command_id          = BDM_CLIENT_GET_RESPONSE;
   txMsg.cmd_data            = (u8 *)&respData;
   txMsg.cmd_data_len        = sizeof(respData);
   txMsg.cmd_sequence_number = getNextTxGetSetSeq();
   respData.clientDeviceType = clientType;
   respData.serverDeviceType = DEVICE_TYPE_SHAVER;
   respData.getStatus        = getStatus;
   if (getStatus == 0)
   {
      memcpy(&respData.getData, &localPortStatus, sizeof(localPortStatus));
   }

   // logger.highlight("D2BDM Sent GET Response (%d) with Sequence number (%d)", getStatus, txMsg.cmd_sequence_number);
   if ((status = build_send_ip_msg(getSetClientFd,&txMsg, &msgGetSetLock)) != 0)
   {
      logger.error("Error (%d) for sending a GetRequestResponse to GetSet client",status);
      return;
   }
}


/**
 * @fn      void *getSet_d2_wait_portStatus(UNUSED_ATTRIBUTE void *args)
 * @brief   thread to wait for localPortStatus messages from the bdmD2
 * @param   [in] args - Not Used 
 * @retval  returns NULL (required for the compiler)
 **************************************************************/
void *getSet_d2_wait_portStatus(UNUSED_ATTRIBUTE void *args)
{
   u32 bytesRead;   
   void *portStatus = NULL;
   void *pHandleSubP = NULL;
   u8 filter[BROKER_FILTER_MAX];
   // create a handle
   if ((pHandleSubP = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return (void *)(NULL);
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSubP, D2_STATUS, strlen(D2_STATUS))))
   {
      logger.error("Failed to subscribe to D2 Port Status");
      return (void *)(NULL);
   }

   // continue for ever
   while (1)
   {
      int status;
      if ((status = bm_receive(pHandleSubP, filter, (void*) &portStatus, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive",status);
         return NULL;
      }
      assert(bytesRead >= portStatusSize);
      memcpy(&localPortStatus, portStatus, portStatusSize);
      logger.debug("Got (%d) Port Status <%x> from BDM",bytesRead,localPortStatus.porta_speed_run);
      free(portStatus);
   }
   
   return NULL;
}

/**
 * @fn      void *getSet_d2_wait_connected(UNUSED_ATTRIBUTE void *args)
 * @brief   thread to wait d2 connected status from D2 
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *getSet_d2_wait_connected(UNUSED_ATTRIBUTE void *args)
{
   u32 bytesRead;
   void *pHandleSubC  = NULL;
   u8 filter[BROKER_FILTER_MAX];
   
   // create a handle
   if ((pHandleSubC = bm_subscriber()) == NULL)
   {
      logger.error("Failure to create publisher handler");
      return NULL;
   }

   // subscribe to port status
   if ((bm_subscribe(pHandleSubC, D2_CONNECTED, strlen(D2_CONNECTED))))
   {
      logger.error("Failed to subscribe to D2 Port Status");
      return NULL;
   }

   u8 *pConn;
   while (1)
   {
      int status;
      if ((status = bm_receive(pHandleSubC, filter, (void*) &pConn, &bytesRead, 0)) <= 0)
      {
         logger.error("Failed (%d) bm_receive",status);
         return NULL;
      }
      isD2Connected = *pConn;
      logger.debug("D2 Connected State  <%d>",isD2Connected);
      free(pConn);
   }
   
   return NULL;
}

/**
 * @fn      void dispatch_get_set_msg(int *fd, pIpMsg rxMsg)
 * @brief   parses incoming commands and responds accordingly
 * @param   [in] fd    - ptr to fd to send responses
 * @param   [in] rxMsg - ptr to full received message
 * @retval  n/a
 **************************************************************/
void dispatch_get_set_msg(pIpMsg rxMsg)
{
   // prepare default message
   u8 status = 0;

   if (rxMsg->command_id == CLIENT_BDM_GET_CMD)
   {
      // get pointer to request
      pGetReq pGetCmd  = (pGetReq)rxMsg->cmd_data;

      // validate that the get is for us //
      if (pGetCmd->serverDeviceType != DEVICE_TYPE_SHAVER)
      {
         logger.error("Not SCD Device Type <%d>  Expected<%d>",pGetCmd->serverDeviceType, DEVICE_TYPE_SHAVER);
         status = 1;
      }
      
      respond_get_request_client(status,pGetCmd->clientDeviceType);

   } // GET_CMD  //
   else if (rxMsg->command_id == CLIENT_BDM_SET_CMD)
   {
      // get details of what to set add to pending
      pSetReq pReq = (pSetReq)rxMsg->cmd_data;
      // deal with it
      if (pReq->hand_blade_pump_foot)
      {
         logger.info("Sending HPBF <%02X> to BDM",pReq->hand_blade_pump_foot);
         if ((bm_send(pHandlePub, (u8 *)D2_SET_HPBF_CMD, &pReq->hand_blade_pump_foot, sizeof(pReq->hand_blade_pump_foot))) < 0)
         {
            logger.error("Failed to Publish hbpf command (%02X)",pReq->hand_blade_pump_foot);
            status = 1;
         }
      }
      if (pReq->scd_cmd_request && (pReq->scd_cmd_request < MAX_SCD_COMMAND))
      {
         logger.info("Sending SCD CMD <%02X> to BDM",pReq->scd_cmd_request);
         if ((bm_send(pHandlePub, (u8 *)D2_SET_SCD_CMD, &pReq->scd_cmd_request, sizeof(pReq->scd_cmd_request))) < 0)
         {
            logger.error("Failed to Publish Port Status");
            status = 1;
         }
      }
      respond_set_request_client(status, pReq->clientDeviceType);
      
   } // SET_CMD //
   else
   {
      logger.error("Unexpected Cmd <%02X> Received",rxMsg->command_id);
   }
}

/**
 * @fn      void *bdm_getSet_server(void *args)
 * @brief   receives connects/messages from GetSet Clients
 * @param   [in] args - Not Used 
 * @retval  returns NULL
 **************************************************************/
void *bdm_getSet_server(UNUSED_ATTRIBUTE void *args)
{
   ct_ip_msg_t rxMsg;
   bool continueClientConnection;
   int status;
   pthread_mutex_init(&msgGetSetLock,NULL);

   int getSetServerFd = create_tcp_server_socket(GET_SET_PROTOCOL_SOCKET);
   if (getSetServerFd <= 0)
   {
      logger.error("couldn't create getSet Server Socket");
      return NULL;
   }
    
   logger.debug("Entering BDM GET/SET Server(%d)",isD2Connected);
   while (1)
   {
      if (isD2Connected)
      {
         logger.debug("Start accepting next connection from get/set client");
            
         // accept next command 
         getSetClientFd = accept_tcp_socket_connection(getSetServerFd);
         if (getSetClientFd < 0)
         {
            common_close_socket(&getSetServerFd);
            return NULL;
         }

         logger.debug("New GetSet Connection <%d>",getSetClientFd);

         // process commands while connected
         continueClientConnection = true;
         while (continueClientConnection)
         {
            // wait for next message
            if (isConnectionValid(getSetClientFd) && (isD2Connected))
            {
                    
               if ((status = read_ip_msg(&getSetClientFd, bdmIpGetSetCmds, 2, &rxMsg)))
               {
                  logger.error("Error (%d)in read msg - disconnect client",status);
                  common_close_socket(&getSetClientFd);
                  continueClientConnection = false;
               } // error reading message from server
               else
               {
                  dispatch_get_set_msg(&rxMsg);
                  freeIpMsgContainerData(&rxMsg);
               } // process get message
            }
            else
            {
               logger.error("Closing Client Connection (%d) State(%d)",getSetClientFd,common_system_state.state);
               common_close_socket(&getSetClientFd);
               continueClientConnection = false;
            } // if closing or the state went back to Discovery process //

         } // while valid connection
      } // if scd is connected
      else
      {
         YIELD_250MS;
      }
   } // while contining 
   return NULL;
}

/**
 * @fn      int getSetD2_task(void)
 * @brief   primary task - waits until exit)
 * @retval  status (if error)
 */
int getSetD2_task(void)
{
   // init common signal handler
   init_common_signal_handler();
     
   // reset structs */
   bzero(&common_system_state,sizeof(common_system_state));

   init_local_data();
     
   pthread_t getSetThread;
   if ((common_create_thread(&getSetThread, MEDIUM_HIGH, NULL, bdm_getSet_server)))
   {
      logger.error("Error in creating Get Set Server Thread");
      goto bdm_exit_task;  
   }

   pthread_t waitPortStatus;
   if ((common_create_thread(&waitPortStatus, MEDIUM_HIGH, NULL, getSet_d2_wait_portStatus)))
   {
      logger.error("Error in creating Get Set Server Thread");
      goto bdm_exit_task;  
   }

   pthread_t wait_connected;
   if ((common_create_thread(&wait_connected, MEDIUM_HIGH, NULL, getSet_d2_wait_connected)))
   {
      logger.error("Error in creating Get Set Server Thread");
      goto bdm_exit_task;  
   }

   // determine if we are doing no cli
   if (doCli == 0)
   {
      logger.highlight("NO CLI Interface");
   }

   // this is the forever //
   while (1)
   {
      sleep(1);
   } // end while (1)
       
bdm_exit_task:
   logger.highlight("Exiting getsetd2(%d) - Close socket & wait for read thread to exit",common_system_state.stopping);
   common_system_state.stopping = true;
   logger.highlight("---- Exiting GetSetD2 -----\n");
   exit(0);
   return 0;
}

int main(int argc, char *argv[])
{
   if (argc > 1 && !strcmp(argv[1],"-V")) {
       printf("%s\n", version);
       return 0;
   }

   if (argc > 2)
   {
      fprintf(stderr,"usage %s<(optional) 'Do CLI' (0/1(cli is enabled by default))>\n", argv[0]);
      exit(0);
   }
   
   if (argc == 2)
   {
      doCli = atoi(argv[1]);      
   }

   return (getSetD2_task()); 
}

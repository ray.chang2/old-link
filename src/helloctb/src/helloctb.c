#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "add.h"
#include "helloctb.h"

void* thread_func(void* thread_param)
{
    sleep(1);
    printf("hello from thread %d: 5 + 3 = %d \n",
            *(int*)thread_param, add(5, 3));
    return NULL;
}

int main(void)
{
    int id[THREAD_NUM];
    pthread_t thread[THREAD_NUM];

    for (int i = 0; i < THREAD_NUM; i++ ) {
        id[i] = i;
        if(pthread_create(&thread[i], NULL, thread_func, &id[i])) {
            fprintf(stderr, "Error creating thread %d\n", i);
            return 1;
        }
    }

    for (int i = 0; i < THREAD_NUM; i++ ) {
        if(pthread_join(thread[i], NULL)) {
            fprintf(stderr, "Error joining thread %d\n", i);
            return 2;
        }
    }

    return 0;
}

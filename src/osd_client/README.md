# OSD Protocol Client Test Application

## How to build:

### For PC

    make clean
    make

## For Bridge

   # First go to the contower directory
   cd /some_location_in_your_pc/contower
   . setup-imx-enviroment
   # Then go back to this directory
   cd -
   make clean
   make

## How to test 12 icons for OSD Manager

Run the following three commands in three Linux boxes, one for each:

    ./build/osd_client -v -d 0 -n 4 192.168.1.1
    ./build/osd_client -v -d 1 -n 4 192.168.1.1
    ./build/osd_client -v -d 2 -n 4 192.168.1.1

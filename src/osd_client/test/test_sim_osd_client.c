#include <stdio.h>
#include <string.h>
#include "snunit.h"
#include "osd.h"
#include "util.h"

int tests_run = 0;

int foo = 7;
int bar = 4;

/******************************************************************************
 *   Request Messages
 ******************************************************************************/

static char * test_create_reg_request_msg() {
    uint8_t expected_crc;
    uint32_t command_data_len;
    OsdMsg_RegRqst* msg;
    const char* test_icon = "test icon";

    msg = create_reg_request_msg(123, DEVICE_TYPE_SHAVER, test_icon);

    sn_assert2_uint(msg->netid, 123);
    sn_assert2_uint(msg->device_type, DEVICE_TYPE_SHAVER);
    sn_assert2_uint(msg->header.protocol_id, OSD_PROTO_ID_C2S);
    sn_assert2_int(msg->header.protocol_id, OSD_PROTO_ID_C2S);
    sn_assert2_uint(msg->header.command, OSD_CMD_CLIENT_REGISTRATION_REQUEST);

    command_data_len = ntohl(msg->header.data_len_nw);
    sn_assert2_int(command_data_len,
            1 /*netid*/ + 1 /*device_type*/ + OSDMGR_ICONSTRSIZEBYTES);
    sn_assert2_int(strcmp((char*)msg->icon_name, test_icon), 0);

    expected_crc = calc_crc((const uint8_t*)msg, IFULLMSG_HEADER_SIZE + command_data_len);
    sn_assert2_int(expected_crc, msg->crc);

    return 0;
}

static char * test_create_pop_icon_msg() {
    uint8_t expected_crc;
    uint32_t command_data_len;
    OsdMsg_PopulateIcon* msg;
    char* icon_data = "test data";
    uint8_t uid = 1;

    msg = create_pop_icon_msg(122, uid, (uint8_t*)icon_data, strlen(icon_data));
    sn_assert2_uint(msg->netid, 122);
    sn_assert2_uint(msg->uid, uid);

    command_data_len = ntohl(msg->header.data_len_nw);
    sn_assert2_uint((uint32_t)(2 + strlen(icon_data)), command_data_len);

    sn_assert2_int(memcmp(msg->icon, icon_data,strlen(icon_data)), 0);

    expected_crc = calc_crc((const uint8_t*)msg, IFULLMSG_HEADER_SIZE + command_data_len);
    sn_assert2_int(expected_crc,
            ((uint8_t*)msg)[IFULLMSG_HEADER_SIZE + command_data_len]);

    return 0;
}

static char * test_create_and_update_status_update_msg() {
    uint8_t expected_crc;
    uint32_t command_data_len;
    OsdMsg_StatusUpdate* msg;
    uint8_t netid = 144;
    uint8_t uid = 1;

    msg = create_status_update_msg(netid, uid);
    sn_assert2_uint(msg->netid, netid);
    sn_assert2_uint(msg->uid, uid);

    command_data_len = ntohl(msg->header.data_len_nw);
    sn_assert2_uint((uint32_t)(OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN), command_data_len);

    uint32_t bitmask = u32_from_4u8(
            msg->bm0,
            msg->bm1,
            msg->bm2,
            msg->bm3);
    uint32_t icon_bar_text_reg = u32_from_4u8(
            msg->icon_bar_text_reg0,
            msg->icon_bar_text_reg1,
            msg->icon_bar_text_reg2,
            msg->icon_bar_text_reg3);
    sn_assert2_uint(bitmask, 0);
    sn_assert2_uint(icon_bar_text_reg, 0);

    expected_crc = calc_crc((const uint8_t*)msg, IFULLMSG_HEADER_SIZE + command_data_len);
    sn_assert2_int(expected_crc,
            ((uint8_t*)msg)[IFULLMSG_HEADER_SIZE + command_data_len]);

    Osd_StatusUpdateData sud = {
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF,
            0xFFFFFFFF,
            "update"
    };
    update_status_update_msg(msg, netid, uid, &sud);
    sn_assert2_uint(netid, msg->netid);
    sn_assert2_uint(uid, msg->uid);
    sn_assert2_int(strcmp((char*)msg->text, "update"), 0);
    return 0;
}

/******************************************************************************
 *   Response Messages
 *****************************************************************************/

static char * test_check_reg_response_msg() {
    // not needed?
    return 0;
}

static char * test_check_pop_icon_response_msg() {
    // not needed?
    return 0;
}

static char * test_check_status_response_msg() {
    // not needed?
    return 0;
}

/******************************************************************************
 *   Misc Tests
 *****************************************************************************/

static char * test_ip_str_to_int() {
    char *ip = "0.0.0.0";
    uint8_t n = ip_str_to_int(ip);
    sn_assert("error, all zero ip should return 0", n == 0);
    return 0;
}

static char * test_calc_crc() {
    uint8_t res;

    uint8_t buf[2] = {1, 0};
    res = calc_crc(buf, 2);
    sn_assert("error", res == 255);

    uint8_t buf2[2] = {0, 0};
    res = calc_crc(buf2, 2);
    sn_assert("error", res == 0);

    return 0;
}

static char * test_u32_from_4u8() {
    sn_assert2_uint(0, u32_from_4u8(0, 0, 0, 0));
    sn_assert2_uint(1, u32_from_4u8(1, 0, 0, 0));
    sn_assert2_uint(0x01020304, u32_from_4u8(4, 3, 2, 1));
    sn_assert2_uint(0xa55a0000, u32_from_4u8(0, 0, 0x5a, 0xa5));

    return 0;
}

/*****************************************************************************/

static char * all_tests() {
    sn_run_test(test_create_reg_request_msg);
    sn_run_test(test_create_pop_icon_msg);
    sn_run_test(test_create_and_update_status_update_msg);

    sn_run_test(test_check_reg_response_msg);
    sn_run_test(test_check_pop_icon_response_msg);
    sn_run_test(test_check_status_response_msg);

    sn_run_test(test_ip_str_to_int);
    sn_run_test(test_calc_crc);
    sn_run_test(test_u32_from_4u8);
    return 0;
}

int main(int argc, char **argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf(GREEN"ALL TESTS PASSED\n"RESET);
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}

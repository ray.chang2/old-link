#ifndef UNITTEST_H
#define UNITTEST_H

#define RED "\033[0;31m"
#define GREEN "\033[0;32m"
#define YELLOW "\033[0;33m"
#define GRAY "\033[1;30m"
#define RESET "\033[0m"

#define TESTBUFSZ 128
#define sn_assert(message, test) do { \
    static char buf[TESTBUFSZ]; \
    sprintf(buf, "%sAssertion failed [%s|%d] %s: %s%s", RED, \
            __FILE__, __LINE__, #test, message, RESET); \
    if (!(test)) return buf; } while (0)

#define sn_assert2_int(actual, expected) do { \
    static char buf[TESTBUFSZ]; \
    sprintf(buf, "%s|%d| %sAssertion failed: expected: %d, actual: %d%s",\
            __FILE__, __LINE__, \
            RED, \
            expected, actual, RESET); \
    if (!((expected) == (actual))) return buf; } while (0)

#define sn_assert2_uint(actual, expected) do { \
    static char buf[TESTBUFSZ]; \
    sprintf(buf, "%s|%d|: %sAssertion failed: expected: %u, actual: %u%s",\
            __FILE__, __LINE__, \
            RED, \
            expected, actual, RESET); \
    if (!((expected) == (actual))) return buf; } while (0)

#define sn_run_test(test) do { \
    char *message = test(); \
    tests_run++; \
    if (message) return message; } while (0)

extern int tests_run;

#endif

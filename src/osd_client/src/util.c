#include <inttypes.h>
#include <stdbool.h>
#include "util.h"


uint32_t u32_from_4u8(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3)
{
    return
         (uint32_t)b0 +
        ((uint32_t)b1 << 8) +
        ((uint32_t)b2 << 16)+
        ((uint32_t)b3 << 24);
}

/*
 * OSDManagerCT_Ext.h
 *
 *  Created on: Sep 8, 2018
 *      Author: Carlos Rodriguez
 */

#ifndef SRC_COMMONBSPFILES_OSDMANAGERCT_EXT_H_
#define SRC_COMMONBSPFILES_OSDMANAGERCT_EXT_H_



// #include "usbMessage.h"
// #include "CT_defines.h"

#define OSDMANAGER_VERSION 0x5 /**< version of osdmanager protocol */


#define OSDMANAGER_SENDTOSERVER_PID			0x66 /**< input sync byte for this message protocol */
#define OSDMANAGER_RCVFROMSERVER_PID		0x99 /**< output sync byte for this message protocol */
#define OSDMANAGER_TEXTSTRLENGTH			8

/**< Enumeration for the Connected Tower Device Types see eConnectedTowerEnums in CT_defines.h*/

/**< returns for OSD register message */
#define OSDMGR_ERR_REG_REGISTERED 0
#define OSDMGR_ERR_REG_NO_SPACE 1
#define OSDMGR_ERR_REG_INVALID_DEVICE 2
#define OSDMGR_ERR_REG_DUPE_NAME 3

/**< Color enumerations for ICONS, see coloroverideval_ , this is for changing
 * all non transparent colors to a single color, for ex to indicate state*/
typedef enum{
	eBlack=0,
	eRed,
	eGreen,
	eWhite,
	eOrangeDark,
	eDarkGrey,
	eBlue,
	eYellow,
}eIconColorEnums;

/**< Color enumerations for text */
typedef enum{
	etxtRed=0,
	etxtDefaultOrange,
	etxtLightOrange,
	etxtYellow,
	etxtGreen,
	etxtMutedGreen,
	etxtBlue,
	etxtDarkBlue,
	etxtMagenta,
	etxtLightPurple,
	etxtLightBlue,
	etxtMidGrey,
	etxtOliveGreen,
	etxtBurntRed,
	etxtPurple,
	etxtWhite
}eTextColorEnums;

///////////////////////////MESSAGES////////////////////////////////////////////////////


/**<           REGISTRATION REQUEST AND REPLY ***************************************/

#define OSDMGR_ICONSTRSIZEBYTES 16

//OSD MANAGER:        REGISTRATION REQUEST
typedef struct extRtosRegRequest /**< Registration Request Data Content*/
{
  u8  NETID;		/**< this is the computer net address YYY in XXX.XXX.XXX.YYY in the connected tower network of the client, helps baseboard */
  u8  devicetype; 	/**< device type for connected tower see eConnectedTowerEnums */
  u8  iConName[OSDMGR_ICONSTRSIZEBYTES]  __attribute__ ((__packed__));  /**< name of icon to populate in icon settings  */
 } __attribute__ ((__packed__))
	extRtosRegRequest_t,           /**< typedef   		*/
	*pextRtosRegRequest;           /**< typedef ptr   	*/

typedef struct /**< defines Registration Request message       */
{
  network_message_header_t 	header    __attribute__ ((__packed__));
  extRtosRegRequest_t	 	data      __attribute__ ((__packed__));
  u8 						checksum  __attribute__ ((__packed__));
} extRtosRegRequest_msg                      __attribute__ ((packed));

//OSD MANAGER:        REGISTRATION REQUEST REPLY
typedef struct rtosExtRegRequestRply /**< Registration Request Reply Data Content*/
{
  u8  iConName[OSDMGR_ICONSTRSIZEBYTES]  __attribute__ ((__packed__));  /**< name of icon to populate in icon settings  */
  u8  UID			__attribute__ ((__packed__));						/**< unique id must be resent each time from now on by client  (note this is indx for rtos) */
  u8  NETID			__attribute__ ((__packed__));          				/**< this is the computer net address YYY in XXX.XXX.XXX.YYY in the connected tower network of the client */
  u8  Result		__attribute__ ((__packed__));        				/**< 0= success 1= no slots fail 2 = invalid request 3 = fail name exists */
} __attribute__ ((__packed__))
	rtosExtRegRequestRply_t;           /**< typedef    	*/

typedef struct /**< defines Registration Request message Reply      */
{
  network_message_header_t 	header    	__attribute__ ((__packed__));
  rtosExtRegRequestRply_t	 	data    __attribute__ ((__packed__));
  u8 						checksum  	__attribute__ ((__packed__));
} rtosExtRegRequestRply_msg             __attribute__ ((packed));




/**<           POPULATE ICON AND REPLY      ******************************************/

//OSD MANAGER:        POPULATE ICON REQUEST Data
typedef struct extRtosPopRequest /**< Registration Request Data Content*/
{
	u8  NETID			__attribute__ ((__packed__));
	u8  UID				__attribute__ ((__packed__));          				/**< unique ID, this will be required to send each request by client */
    u8  *pData			__attribute__ ((__packed__));  						/**< icon data placeholder */
} __attribute__ ((__packed__))
	extRtosPopRequest_t,           /**< typedef   		*/
	*pextRtosPopRequest;           /**< typedef ptr   	*/

typedef struct /**< defines POPULATE ICON message       */
{
  network_message_header_t 	header    __attribute__ ((__packed__));
  extRtosPopRequest_t	 	data      __attribute__ ((__packed__));
  u8 						checksum  __attribute__ ((__packed__));
} extRtosPopRequest_msg                      __attribute__ ((packed));

//OSD MANAGER:        POPULATE ICON REPLY
typedef struct rtosExtPopIconRply /**< Registration Request Reply Data Content*/
{
  u8  UID			__attribute__ ((__packed__));          				/**< unique ID, this will be required to send each request by client */
  u8  NETID 		__attribute__ ((__packed__));  						/**< last XXX in computer IP for CT network */
  u8  Result		__attribute__ ((__packed__));        				/**< 0= success 1= invalid timed out 2 = bad msg */
} __attribute__ ((__packed__))
	rrtosExtPopIconRply_t;           /**< typedef    	*/

typedef struct /**< defines POPULATE ICON message Reply      */
{
  network_message_header_t 	header    	__attribute__ ((__packed__));
  rrtosExtPopIconRply_t	 	data    __attribute__ ((__packed__));
  u8 						checksum  	__attribute__ ((__packed__));
} rtosExtPopIconRply_msg             __attribute__ ((packed));





/**<                STATUS UPDATE MESSAGE AND NAK *************************************/
/**< Icon control register IMPORTANT ON SERVER BITFIELDS ARE bit 0 in first struct member*/
typedef union iconCntrReg {
	u32 reg;
    struct {
    	u32 hposition		:10;    			/**<  LSB : horizontal start position from upper left (note this is multiplied by 4 for 4K dislay) */
    	u32 vposition		:10;				/**<  vertical start position from upper left (note this is multiplied by 4 for 4K dislay)*/
    	u32 lefthide    	 :1;				/**<  set to overide pixel alpha for left side */
    	u32 coloroverride	 :2;				/**<  set to overide icon color and use the override value b0=left b1=right */
    	u32 coloroverideval_left	 :3;		/**<  if override set then use this value on left see eIconColorEnums*/
    	u32 coloroverideval_right	 :3;		/**<  if override set then use this value on left see eIconColorEnums*/
    	u32 size			 :1;    			/**<  Scaling of the icon. 0: 64x64 pixels. 1: 128x128  */
    	u32 toggle			 :1;				/**<  If 1, the left and right halves of the icon will toggle on and off automatically */
    	u32 righthide		 :1;				/**<  set to overide pixel alpha for right side */
    } fields;

} iconCntrReg_t;

/**< Progress bar control register */
typedef union iconBarReg {
	u32 reg;
    struct {
    	u32 visible			:1;    /**< If set then the icon's progress bar will be shown */
    	u32 location		:2;		/**<  where the bar is displayed see OSD_ICON_BAR_LOCATION_LSB_POSN  */
    	u32 percFull		:6;		/**<  0= full(100%) 63=empty(0%) */
    	u32 color			:6;     /**< Color to use for the progress bar from the icon colour LUT,see eIconColorEnums */
    	u32 _unused			:17;	/**< MSB : future */
    } fields;

} iconBarReg_t;

/**< Text Control Registers */
typedef union iconBarTextReg {
	u32 reg;
    struct {
    	u32 need			:2;    /**< does the icon need a text block*/
    	u32 index			:8;		/**<  which text bock is associated with the icon  */
    	u32 _unused			:22;	/**< future maybe a pointer to the text block in question */
    } fields;

} iconBarTextReg_t;

typedef union unControlReg1 {
    struct {
    	u32 hpos  		: 11; /**< LSB upper left horiz position on screen of text (note will be X2)*/
    	u32 vpos  		: 11; /**< upper left vert position on screen of text(note will be X2)*/
        u32 strLen 		: 6;  /**< numchars in string to display*/
        u32 colorIndx 	: 4;  /**< color index for text, see eTextColorEnums*/
    } fields;
    u32 reg;
} unControlReg1_t;

typedef union unControlReg2 {
    struct {
    	u32 invertColor 		: 1;  /**< used to invert transparency for highlighting of text*/
    	u32 alpha  				: 4; /**< transparancy of text 0= invis 15=most opaque*/
    	u32 charspacing  		: 4; /**< spacing between text*/
        u32 scale 				: 2;  /**< 2'b00: X1, 2'b01: X2, 2'b10: X4, 2'b11: X8.*/
        u32 shrink				: 2;  /**< 0 = dont shrink any side , 1 shrink left side, 2 = shrink right side, 3 =shrink both sides */
        u32 unused				:19;	//future
    } fields;
    u32 reg;
} unControlReg2_t;

typedef union unAlphaRectControlReg1 {
    struct {
    	u32 hpos 				: 12;  /**< horiz pos 0-3839*/
    	u32 vpos  				: 12; /**<  vpos 0-2159*/
    	u32 alpha  				: 8; /**< alpha of background 0(invis)-255(darkest)*/
    } fields;
    u32 reg;
} unAlphaRectControlReg1_t;

typedef union unAlphaRectControlReg2 {
    struct {
    	u32 height 			: 12;  /**< height of box 0-2159*/
    	u32 width  			: 12; /**<  width of box 0-3839*/
    	u32 unused  		: 8; /**< future*/
    } fields;
    u32 reg;
} unAlphaRectControlReg2_t;

//defines for funcBitMask in extRtosStatus_t
#define OSDMGR_EXTSTATUS_UPDATE_iconCntrReg 		0x01
#define OSDMGR_EXTSTATUS_UPDATE_iconBarReg  		0x02
#define OSDMGR_EXTSTATUS_UPDATE_iconBarTextReg  	0x04
#define OSDMGR_EXTSTATUS_UPDATE_unControlReg1  		0x08
#define OSDMGR_EXTSTATUS_UPDATE_unControlReg2  		0x10
#define OSDMGR_EXTSTATUS_UPDATE_updatetextstr  		0x20
#define OSDMGR_EXTSTATUS_UPDATE_updatealpharect1  	0x40
#define OSDMGR_EXTSTATUS_UPDATE_updatealpharect2  	0x80

/**< External Device to OSD Manager server STatus Message */
typedef struct extRtosStatus /**< Registration Request Data Content*/
{
	u8  NETID			__attribute__ ((__packed__));
	u8  UID				__attribute__ ((__packed__));          				/**< unique ID, this will be required to send each request by client */
	u32 funcBitMask 	__attribute__ ((__packed__));  						/**< Index see #define OSDMGR_EXTSTATUS_UPDATE_XXXXXX*/
	u32 uiconCntrReg 	__attribute__ ((__packed__));  						/**< display icon or hide it */
	u32	uiconBarReg   	__attribute__ ((__packed__));  						/**< whether to override color */
	u32 uiconBarTextReg __attribute__ ((__packed__));  						/**< whether to update progress bar */
	u32 unControlReg1	__attribute__ ((__packed__));  						/**< whether to update text  */
	u32 unControlReg2  	__attribute__ ((__packed__));  						/**< whether to update text */
	u32 unAlphaRectControlReg1		__attribute__ ((__packed__));  						/**< OSD_ALPHA_RECTANGLE_00_CONFIG1_ADDR  */
	u32 unAlphaRectControlReg2  	__attribute__ ((__packed__));  						/**< OSD_ALPHA_RECTANGLE_00_CONFIG2_ADDR */
	u8 strText[OSDMANAGER_TEXTSTRLENGTH]		__attribute__ ((__packed__));
} __attribute__ ((__packed__))
	extRtosStatus_t,           /**< typedef   		*/
	*pextRtosStatus;           /**< typedef ptr   	*/

/**< NAK RESPONSE IF A STATUS MESSAGE IS REJECTED BY SERVER */
typedef struct rtosExtRtosStatusRply /**< Status Reply Data Content: Note Only sent as a NAK*/
{
  u8  NETID			__attribute__ ((__packed__));
  u8  UID			__attribute__ ((__packed__));          			/**< unique ID, this will be required to send each request by client */
  u8  err 		__attribute__ ((__packed__));  						/**< Index */
  } __attribute__ ((__packed__))
  ExtRtosStatusRply_t;           /**< typedef    	*/

typedef struct /**< nak response      */
{
  network_message_header_t 	header    	__attribute__ ((__packed__));
  ExtRtosStatusRply_t	 	data    __attribute__ ((__packed__));
  u8 						checksum  	__attribute__ ((__packed__));
} rtosExtStatusRply_msg             __attribute__ ((packed));



/**<     DROP CONNECTION REQUESTL:  NOTE THIS IS ONLY A MESSAGE BETWEEN BASEBOARD AND RTOS, NOTE EXPOSED TO
 * 		 OSDManager Clients.  this is used to close race conditions to invalidate a dropped icon
 *       NOTE THIS IS ON PROTOCOL 0x69, NOT THE OSDMANAGER PROTOCOL
 */

typedef struct rtos_usb_drop_icon /**< Registration Request Data Content*/
{
	u8  UDID			__attribute__ ((__packed__));
	u8  NETID				__attribute__ ((__packed__));
} __attribute__ ((__packed__))
	rtos_usb_drop_icon_t,           /**< typedef   		*/
	*prtos_usb_drop_icon;           /**< typedef ptr   	*/


typedef struct /**< RTOS->BASEBOARDmessage structure to request to drop this icon off osdmanager list see USB_RTOS_REQUEST_DROP_ICON_RESPONSE */
{
  network_message_header_t 		header    __attribute__ ((__packed__));
  rtos_usb_drop_icon_t			data	  __attribute__ ((__packed__));
  u8 							checksum  __attribute__ ((__packed__));
} rtos_usb_drop_icon_msg                      __attribute__ ((packed));

typedef struct /**< BASEBOARD->RTOS structure , reply. note that if not received, RTOS will dump it after some time
the basebaord responds to the rtos when the connection is dropped with the NETID for OSDmanager client*/
{
  network_message_header_t 		header    __attribute__ ((__packed__));
  rtos_usb_drop_icon_t			data	  __attribute__ ((__packed__));
  u8 							checksum  __attribute__ ((__packed__));
} rtos_usb_drop_icon_msgrply              __attribute__ ((packed));


#endif /* SRC_COMMONBSPFILES_OSDMANAGERCT_EXT_H_ */

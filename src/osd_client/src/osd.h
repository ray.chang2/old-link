#ifndef OSD_H
#define OSD_H

#include <inttypes.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define OSDMGR_ICONSTRSIZEBYTES  16

#define OSD_SVR_LISTENING_TCP_PORT 0x6997 // 27031

#define OSD_PROTO_ID_C2S 0x66
#define OSD_PROTO_ID_S2C 0x99

#define MAX_PACKET_SIZE 1024
#define MAX_PACKET_BODY_SIZE (MAX_PACKET_SIZE - 8 - 1)  // minus header, but including crc

#if __GNUC__ >= 5
#define STATIC_ASSERT(expr, msg) _Static_assert(expr, msg)
#else
#define STATIC_ASSERT(expr, msg)
#endif

enum {
    ACK = 1,
    NACK = 0
};

enum {
    DEVICE_TYPE_RESERVED = 0,
    DEVICE_TYPE_SHAVER = 1,             // D2
    DEVICE_TYPE_VIDEO = 2,
    DEVICE_TYPE_COBLATION = 3,          // Werewolf
    DEVICE_TYPE_TABLET = 4,
    DEVICE_TYPE_FLUID_MANAGEMENT = 5,   // D25
    DEVICE_TYPE_MAX
} IFULL_DEVICE_TYPE;

#define OSD_CLI_MSG_ID_START 0xF0
#define OSD_SVR_MSG_ID_START 0xF8
// #define OSD_CMD(offset)  (OSD_MSG_ID_START + offset)
enum {
    OSD_CMD_CLIENT_REGISTRATION_REQUEST = OSD_CLI_MSG_ID_START,
    OSD_CMD_CLIENT_POPULATE_ICON,
    OSD_CMD_CLIENT_STATUS_UPDATE,

    OSD_CMD_SERVER_REGISTRATION_RESPONSE = OSD_SVR_MSG_ID_START,
    OSD_CMD_SERVER_STATUS_UPDATE_RESPONSE,
    OSD_CMD_0xFA,  // not used
    OSD_CMD_0xFB,  // not used
    OSD_CMD_SERVER_POPULATE_ICON_RESPONSE,
};

typedef struct  __attribute__((packed)) IFullMsgHeader {
    uint8_t protocol_id;
    uint8_t command;
    uint16_t padding;
    uint32_t data_len_nw;
    uint8_t data[0];  // stub member, length unknown
    // uint8_t crc;  // after data. Commented out since data length unknown.
    // BTW, we call it CRC for legacy reasons.
    // It actually is just a checksum
} IFullMsgHeader, IFullMsg;
#define IFULLMSG_HEADER_SIZE 8
STATIC_ASSERT(IFULLMSG_HEADER_SIZE == sizeof(IFullMsg), "IFullMsgHeader size error");

/******************************************************************************
 *                   OSD Messages                                             *
 ******************************************************************************/

typedef struct  __attribute__((packed)) OsdMsg {
    IFullMsgHeader header;
    uint8_t body[MAX_PACKET_BODY_SIZE];
} OsdMsg;

// Registration Request
#define OSD  15
typedef struct  __attribute__((packed)) OsdMsg_RegRqst {
    IFullMsgHeader header;
    uint8_t netid;
    uint8_t device_type;
    uint8_t icon_name[OSDMGR_ICONSTRSIZEBYTES];
    uint8_t crc;   // present in the end
} OsdMsg_RegRqst;
#define OSD_MSG_REG_REQ_PAYLOAD_LEN (OSDMGR_ICONSTRSIZEBYTES + 2)
STATIC_ASSERT(8 + OSD_MSG_REG_REQ_PAYLOAD_LEN + 1== sizeof(OsdMsg_RegRqst), "OsdMsg_RegRqst size error");

// Registration Response
typedef struct  __attribute__((packed)) OsdMsg_RegResp {
    IFullMsgHeader header;
    uint8_t icon_name[16];
    uint8_t uid;
    uint8_t netid;
    uint8_t result;
    uint8_t crc;
} OsdMsg_RegResp;
#define OSD_MSG_REG_RESP_PAYLOAD_LEN 19
STATIC_ASSERT(8 + OSD_MSG_REG_RESP_PAYLOAD_LEN + 1== sizeof(OsdMsg_RegResp), "OsdMsg_RegResp size error");

// Populate ICON
#define MAX_ICON_LEN  800
typedef struct  __attribute__((packed)) OsdMsg_PopulateIcon {
    IFullMsgHeader header;
    uint8_t netid;
    uint8_t uid;
    uint8_t icon[0];  // placeholder. length specified in header.
    // uint8_t crc;   // present in the end
} OsdMsg_PopulateIcon;

// Populate ICON Response
typedef struct  __attribute__((packed)) OsdMsg_PopIconResp {
    IFullMsgHeader header;
    uint8_t uid;
    uint8_t netid;
    uint8_t result;
    uint8_t crc;
} OsdMsg_PopIconResp;
#define OSD_MSG_POP_ICON_RESP_PAYLOAD_LEN 3
STATIC_ASSERT(8 + OSD_MSG_POP_ICON_RESP_PAYLOAD_LEN + 1== sizeof(OsdMsg_PopIconResp), "OsdMsg_PopIconResp size error");

// Status Update
typedef struct  __attribute__((packed)) OsdMsg_StatusUpdate {
    // all "U32" fields are little endian
    IFullMsgHeader header;
    uint8_t netid;		// 0
    uint8_t uid;                                                                    // 1
    uint8_t bm0, bm1, bm2, bm3;			// 2, 3, 4, 5
    uint8_t icon_cntr_reg0, icon_cntr_reg1, icon_cntr_reg2, icon_cntr_reg3;         // 6, 7, 8, 9
    uint8_t icon_bar_reg0, icon_bar_reg1, icon_bar_reg2, icon_bar_reg3;             // 10, 11, 12, 13
    uint8_t icon_bar_text_reg0, icon_bar_text_reg1, icon_bar_text_reg2, icon_bar_text_reg3; // 14, 15, 16, 17
    uint8_t ctrl_reg1_0, ctrl_reg1_1, ctrl_reg1_2, ctrl_reg1_3;                     // 18, 19, 20, 21
    uint8_t ctrl_reg2_0, ctrl_reg2_1, ctrl_reg2_2, ctrl_reg2_3;                     // 22, 23, 24, 25
	uint8_t	alpha_rect1_0, alpha_rect1_1, alpha_rect1_2, alpha_rect1_3;				// 26, 27, 28, 29
	uint8_t	alpha_rect2_0, alpha_rect2_1, alpha_rect2_2, alpha_rect2_3;				// 30, 31, 32, 33
    uint8_t text[8];                                                				// 34-41
    uint8_t crc;
} OsdMsg_StatusUpdate;
#define OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN 42
STATIC_ASSERT(8 + OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN + 1== sizeof(OsdMsg_StatusUpdate), "OsdMsg_StatusUpdate size error");

// Status Update Response (NAK)
typedef struct  __attribute__((packed)) OsdMsg_StatusUpdateResp {
    IFullMsgHeader header;
    uint8_t netid;
    uint8_t uid;
    uint8_t error;
    uint8_t crc;
} OsdMsg_StatusUpdateResp;
#define OSD_MSG_STATUS_UPDATE_RESP_PAYLOAD_LEN 3
STATIC_ASSERT(8 + OSD_MSG_STATUS_UPDATE_RESP_PAYLOAD_LEN + 1== sizeof(OsdMsg_StatusUpdateResp), "OsdMsg_StatusUpdateResp size error");

typedef struct {
    uint32_t bitmask;
    uint32_t icon_cntr_reg;
    uint32_t icon_bar_reg;
    uint32_t icon_bar_text_reg;
    uint32_t icon_cr1;
    uint32_t icon_cr2;
    const char* text;
} Osd_StatusUpdateData;

/******************************************************************************
 *                   Interface Variables & Functions                          *
 ******************************************************************************/

extern const char* DEVICE_TYPE_NAMES[];
extern const char* OSD_CLIENT_MSG_NAMES[];
extern const char* OSD_SERVER_MSG_NAMES[];

uint32_t ip_str_to_int(const char* s);

uint8_t calc_crc(const uint8_t* buf, uint32_t len);
uint32_t full_msg_len(uint32_t data_size);

bool verify_msg_format(const IFullMsg* msg);
int get_msg_len(const IFullMsg* msg);
void print_ifull_msg_short(const IFullMsg* msg);
void print_ifull_msg(const IFullMsg* msg);

OsdMsg_RegRqst* create_reg_request_msg(uint8_t netid, uint8_t device_type, const char* icon_name);
OsdMsg_PopulateIcon* create_pop_icon_msg(
        uint8_t netid,
        uint8_t uid,
        const uint8_t* icon, uint32_t icon_len);
OsdMsg_StatusUpdate* create_status_update_msg(uint8_t netid, uint8_t uid);
void update_status_update_msg(OsdMsg_StatusUpdate* msg,
        uint8_t netid,
        uint8_t uid,
        const Osd_StatusUpdateData* sud);

#endif

#include <fcntl.h>
#include <getopt.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <netdb.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#include "osd.h"
#include "util.h"

#include "D2_channels.h"
#include "WW_channels.h"
#include "broker_api.h"
#include "msgD2.h"
#include "ctb_data.h"
#include "ctb_db_table_api.h"

static const char* version = "01.01.00"; //follows major.minor.patch scheme(https://semver.org)

// #define	OSD_DUMP_BSD_BYTES				1
// #define	OSD_DEBUG					1
// #define	OSD_DEBUG_LOOP					1
// #define	OSD_DEBUG_MESSAGES				1
// #define	OSD_DEBUG_ICON					1
// #define	OSD_DEBUG_PROTOCOL			1
// #define	OSD_DEBUG_MEMORY_CORRUPTION		1
// #define OSD_COMPUTE_TIMING_INFORMATION      1

#define OSD_PERFORMANCE        1

#define MSECS_PER_SEC       1000
#define USECS_PER_MSEC      1000
#define USECS_PER_SEC       (MSECS_PER_SEC * USECS_PER_MSEC)

#define	PROGRESS_BAR_DISABLE			0
#define	PROGRESS_BAR_ENABLE				1

#define	PROGRESS_BAR_LOCATION_LEFT			3
#define	PROGRESS_BAR_LOCATION_RIGHT			2
#define	PROGRESS_BAR_LOCATION_TOP			1
#define	PROGRESS_BAR_LOCATION_BOTTOM		0

#define	CONNECED_TOWER_CAPITAL_DEVICE_D2		"/etc/D2"
#define	CONNECED_TOWER_CAPITAL_DEVICE_WW		"/etc/WW"

#define	OSD_CAPITAL_DEVICE_D2			1
#define	OSD_CAPITAL_DEVICE_WEREWOLF		2

#define	OSD_WW_ICON_WIDTH				134
#define	OSD_WW_ICON_HEIGHT				110
#define	OSD_WW_ICON_HEIGHT_TOPAZ		70
#define	OSD_WW_ICON_WIDTH_TOPAZ			155

#define WW_LEGACY_WAND      1
#define WW_SMART_WAND_2     2
#define WW_SMART_WAND_1     4

//
// Bit setting for the Werewolf Status messages - Byte 3 of a 26 byte message
// ABLATE_COAG_SMART_SP bit settings
//

#define	MONITOR_PIXELS_X				940
#define	MONITOR_PIXELS_Y				540

#define	REAL_MONITOR_PIXELS_X			3840
#define	REAL_MONITOR_PIXELS_Y			2160

#define	OSD_ASPECT_RATIO_X				9.4
#define	OSD_ASPECT_RATIO_Y				5.4


// The following structure was stolen from msgGetSetWW.h becasue including that file
// causes the OSD to fail compilation becasue of conflicts with a strucure of the same name
// on the D@ side.

typedef struct ctn_ww_get_response_data {
   u8 wand_info;               // from port status;
   u8 legacy_therapy_max;      // from port status;
   u8 dev_mode;                // from port status;
   u8 ab_coag_smart_sp;        // from HB
   u8 ab_coag_18_sp;           // from HB
   u8 therapy_set;             // from HB
   u8 amb_thres_sp;            // from HB
   u8 gen_set;                 // from HB
   u8 amb_set;                 // from HB
   u8 amb_temp;                // from HB
   u8 leg_amb_set;             // from HB
   u8 leg_amb_temp;            // from HB
   u8 error_priority_code;     // from HB
   u8 foot_info;               // from port status
   u8 wand_name[12];           // from port status
} ctn_ww_get_response_data_t, *pGetWW;

// Major Werewolf bit Settings - byte 5 of 26 bytes
typedef struct _therpy_settings
{
	uint32_t		therapy_mode:3;
	uint32_t		coag_enable:1;
	uint32_t		ablate_enable:1;
	uint32_t		not_needed:1;		// unused bits
	uint32_t		ablate_active:1;	// ablate is active
	uint32_t		coag_active:1;		// coag is active
} therapy_settings_t;


// Sub (Minor?) Werewolf bit Settings - byte 5 of 26 bytes
typedef struct _ablate_coblation_bits
{
	uint32_t		ablate_low:2;	// 0=none, 1=low, 2=med, 3=hi
	uint32_t		ablate_medium:2;	// 0=none, 1=low, 2=med, 3=hi
	uint32_t		ablate_high:2;	// 0=none, 1=low, 2=med, 3=hi
	uint32_t		coblation_set_point:2;	// 0=none, 1=low, 2=med, 3=hi
} ablate_coag_smart_sp_t;

// Ambient settnigs; It's the whole byte, but lets have a struct for it anyway
#define	AMBIENT_RED			0
#define	AMBIENT_ORANGE		1
#define	AMBIENT_GREEN		2

typedef struct _ambient_settings
{
	uint8_t		notification:1;
	uint8_t		feature:1;
	uint8_t		color:2;
	uint8_t		capability:1;
	uint8_t		volume_level:3;
} ambient_settings_t;

typedef struct _active_wand
{
	uint8_t		primary_wand:4;
	uint8_t		smart_wand:1;
	uint8_t		legacy_wand:1;
	uint8_t		topaz_timer:1;
	uint8_t		reflex_timer:1;
} active_wand_t;

// Sub (minor) Settings - byte 3 of 26 bytes

//
// Fixups so we can include the OSDManagerCT_Ext.h header file
//

#define	u8 uint8_t
#define	u32 uint32_t
#define	network_message_header_t	IFullMsgHeader

#include "OSDManagerCT_Ext.h"


#define C_RED "\033[0;31m"
#define C_YELLOW "\033[0;33m"
#define C_BRIGHTWHITE "\033[1;37;40m"
#define C_GRAY "\033[1;30m"
#define C_GREEN "\033[0;32m"
#define C_END "\033[0m"

#define TIMEOUT_MS 2000
#define RECEIVE_TIMEOUT_MS 400
#define SLEEP_MS 200 // must be smaller than other RECEIVE_TIMEOUT_MS and other timeouts
#define HB_INTERVAL_MS 600

#define	ICON_ORIENTATION_VERTICAL		0
#define	ICON_ORIENTATION_HORIZONTAL		1

#define	TEXT_ORIENTATION_VERTICAL		0
#define	TEXT_ORIENTATION_HORIZONTAL		1

#define	ICON_WIDTH_SPACING				5
#define	ICON_HEIGHT_SPACING				5

// Default values for the ports
#define	PORT_ICON_COLORS	2
#define	PORT_TEXT_COLORS	1

#define MAX_SIM_DEVICES 4
#define DEVICE_ID_MAX (MAX_SIM_DEVICES -1)     // this is different from device type
#define MAX_ICONS_PER_DEVICE 4
#define ICON_ID_MAX (MAX_ICONS_PER_DEVICE -1)

#define	OSD_MAX_ICONS_PER_ICON		8

#define D2_MIN_X    1
#define D2_MIN_Y    1
#define D2_MAX_X    85
#define D2_MAX_Y    82

#define WW_MIN_X    1
#define WW_MIN_Y    1
#define WW_MAX_X    87
#define WW_MAX_Y    74

#define	CRASH {char *p=NULL; *p = 0;}

enum Text_Orientation {TEXT_VERTICAL=1, TEXT_HORIZONTAL};

enum {
    STATE_DISCONNECTED,
    STATE_CONNECTED,
};

typedef	struct	_coordinates
{
	uint32_t	x;
	uint32_t	y;
} XY_coordinates_t;

typedef struct	_port_display
{
	uint8_t		units:2;
	uint8_t		blade:2;
	uint8_t		mode:2;
	uint8_t		up_arrow:1;
	uint8_t		down_arrow:1;
} port_display_t;

typedef struct	_port_speed_run
{
	uint8_t		speed:7;
	uint8_t		state:1;
} port_speed_run_t;

typedef	struct	_icon_init
{
	int32_t						fd;
	uint32_t					nbytes[OSD_MAX_ICONS_PER_ICON], x, y, bitmap_size;
	uint32_t					width[OSD_MAX_ICONS_PER_ICON];
	uint32_t					height[OSD_MAX_ICONS_PER_ICON];
	uint8_t						*bitmap[OSD_MAX_ICONS_PER_ICON];
	uint8_t						text[OSD_MAX_ICONS_PER_ICON][9];
	uint8_t						name[OSD_MAX_ICONS_PER_ICON][32];
	uint32_t					icon_color[OSD_MAX_ICONS_PER_ICON];
	uint32_t					text_color[OSD_MAX_ICONS_PER_ICON];
	uint32_t					text_scale[OSD_MAX_ICONS_PER_ICON];
	uint32_t					bar_visible[OSD_MAX_ICONS_PER_ICON];
	uint32_t					bar_location[OSD_MAX_ICONS_PER_ICON];
	uint32_t					visibility[OSD_MAX_ICONS_PER_ICON];
	XY_coordinates_t			rect_coords[OSD_MAX_ICONS_PER_ICON];
	XY_coordinates_t			icon_offset[OSD_MAX_ICONS_PER_ICON];
	XY_coordinates_t			text_offset[OSD_MAX_ICONS_PER_ICON];
} icon_init_t;

typedef struct OsdClientConnection {
	char					id[32];
    uint8_t 				netid;
#if defined(OSD_PERFORMANCE)
    uint8_t                 modified;
    struct	timeval			last_update;
#endif
    struct 					in_addr addr;
    int 					state, uid, bitmap_size, nbytes, device;
	uint8_t					*bitmap;
    pthread_t 				tcp_thread;
    char 					icon_name[OSDMGR_ICONSTRSIZEBYTES];
	icon_init_t				*idata;
    OsdMsg_StatusUpdate		*msg_status_update;
} OsdClientConnection;

// scd_bdm_port_status_t		*d2_status=NULL;
// ctn_ww_get_response_data_t	*werewolf_status=NULL;

union bdm_bits
{
    scd_bdm_port_status_t       d2;
    ctn_ww_get_response_data_t  ww;
};

typedef	struct	_icon
{
	char					id[32];
	int						fd, x, y;
    uint32_t                nicons;
    union bdm_bits          bits;   // For future use in tuning performance
    OsdClientConnection		*icon[OSD_MAX_ICONS_PER_ICON];
} icon_group_t;

typedef	struct	_CD_icons
{
	char					id[32];
	int						fd;
	uint32_t				nicons, visibility[OSD_MAX_ICONS_PER_ICON]; // is the icon ever displayed ??
	icon_group_t			*pgroup[OSD_MAX_ICONS_PER_ICON];
} CD_Icons;

typedef struct	_icon_colors
{
	int		text_color_1;
	int		text_color_2;
	int		text_color_3;
} icon_colors_t;

XY_coordinates_t			coords[2];

XY_coordinates_t			area_coords_A[9] = {
	{100,20},  {425, 20},  {800, 20},
	{20, 230}, {425, 230}, {800, 230},
	{20, 450}, {425, 450}, {800, 450}
};

icon_colors_t	D2_port_A_text_colors[2] =
{
	{etxtDefaultOrange, etxtDefaultOrange, etxtRed},
	{etxtDefaultOrange, etxtBlue, etxtRed}
};

icon_colors_t	D2_port_B_text_colors[2] =
{
	{etxtDefaultOrange, etxtDefaultOrange, etxtRed},
	{etxtDefaultOrange, etxtYellow, etxtRed}
};

// populate_WW_icon_data(&idata, fd, x, y, eWhite, etxtDefaultOrange, etxtGreen);
icon_colors_t	WW_text_colors[3] =
{
	{eWhite, etxtDefaultOrange, etxtGreen},
	{eBlue, etxtBlue, etxtGreen},
	{eBlue, etxtDefaultOrange, etxtGreen}
};

typedef struct  _min_max_coords
{
    XY_coordinates_t    min, max;
} min_max_coords_t; 


uint8_t						icon_orientation = ICON_ORIENTATION_VERTICAL;
uint8_t						text_orientation = ICON_ORIENTATION_HORIZONTAL;
iconCntrReg_t				icon_control_register;
iconBarReg_t				bar_control_register;
iconBarTextReg_t			bar_control_register2;
unControlReg1_t 			text_control_register;
unControlReg2_t 			text_control_register2;
unAlphaRectControlReg1_t	alpha_control_register;
unAlphaRectControlReg2_t	alpha_control_register2;



uint32_t	dump_icon_on_bdm_message=0, bitmap_size = 0, bounding_box_per_icon = 0;

void initialize_bar_control_registers(iconBarReg_t *r1, iconBarTextReg_t *r2);
void initialize_text_control_registers(unControlReg1_t *r1, unControlReg2_t *r2);
void initialize_alpha_control_registers(unAlphaRectControlReg1_t *r1, unAlphaRectControlReg2_t *r2);
int     update_icons(CD_Icons *icon, UNUSED_ATTRIBUTE uint32_t timeout);

static int wait_for_tcp_packet_with_timeout(int , uint8_t* , uint32_t , uint32_t );

uint8_t	port_icon_colors[4] = {eOrangeDark, eGreen, etxtDefaultOrange, etxtGreen};
uint8_t	port_ID_colors[2] = {etxtBlue, etxtYellow};

// char			*port_active[] = {"PORT A", "PORT B", "NODEVICE"};
// char			*port_units[] = {"        ", "RPM", "RATE", "PCT" };
char			*port_format_string[] = {"XXXXXXXX", "%4d RPM", "%3d RATE", "%3d PCT"};
unsigned char	port_units_multiplier[4] = {1, 100, 1, 1};


uint8_t shaver_icon[] =
{
        0xff,0xf0,0xff,0xf0,0x61,0xf0,0x02,0x3f,
        0x02,0xf0,0x02,0x3f,0x39,0xf0,0x02,0x3f,
        0x03,0xf0,0x03,0x3f,0x37,0xf0,0x03,0x3f,
        0x04,0xf0,0x02,0x3f,0x01,0x6f,0x35,0xf0,
        0x03,0x3f,0x05,0xf0,0x03,0x3f,0x01,0x6f,
        0x33,0xf0,0x04,0x3f,0x06,0xf0,0x03,0x3f,
        0x01,0x6f,0x31,0xf0,0x05,0x3f,0x06,0xf0,
        0x04,0x3f,0x01,0x6f,0x2e,0xf0,0x06,0x3f,
        0x08,0xf0,0x04,0x3f,0x01,0x6f,0x2c,0xf0,
        0x07,0x3f,0x08,0xf0,0x05,0x3f,0x01,0x6f,
        0x2a,0xf0,0x07,0x3f,0x09,0xf0,0x07,0x3f,
        0x28,0xf0,0x08,0x3f,0x0a,0xf0,0x07,0x3f,
        0x01,0x6f,0x25,0xf0,0x01,0x6f,0x07,0x3f,
        0x0b,0xf0,0x08,0x3f,0x01,0x6f,0x23,0xf0,
        0x09,0x3f,0x0c,0xf0,0x08,0x3f,0x01,0x6f,
        0x20,0xf0,0x01,0x6f,0x09,0x3f,0x0d,0xf0,
        0x09,0x3f,0x01,0x6f,0x1e,0xf0,0x01,0x6f,
        0x0a,0x3f,0x0d,0xf0,0x0b,0x3f,0x01,0x6f,
        0x1b,0xf0,0x01,0x6f,0x0b,0x3f,0x0e,0xf0,
        0x0b,0x3f,0x01,0x6f,0x19,0xf0,0x0c,0x3f,
        0x10,0xf0,0x0b,0x3f,0x01,0x6f,0x16,0xf0,
        0x01,0x6f,0x0d,0x3f,0x10,0xf0,0x0c,0x3f,
        0x01,0x6f,0x14,0xf0,0x01,0x6f,0x0d,0x3f,
        0x11,0xf0,0x0e,0x3f,0x12,0xf0,0x01,0x6f,
        0x0e,0x3f,0x12,0xf0,0x0e,0x3f,0x01,0x6f,
        0x0e,0xf0,0x02,0x6f,0x0e,0x3f,0x13,0xf0,
        0x0f,0x3f,0x01,0x6f,0x0c,0xf0,0x01,0x6f,
        0x10,0x3f,0x14,0xf0,0x0f,0x3f,0x01,0x6f,
        0x0a,0xf0,0x01,0x6f,0x11,0x3f,0x14,0xf0,
        0x11,0x3f,0x08,0xf0,0x01,0x6f,0x11,0x3f,
        0x16,0xf0,0x11,0x3f,0x01,0x6f,0x05,0xf0,
        0x01,0x6f,0x12,0x3f,0x16,0xf0,0x12,0x3f,
        0x01,0x6f,0x05,0xf0,0x01,0x6f,0x11,0x3f,
        0x15,0xf0,0x12,0x3f,0x01,0x6f,0x07,0xf0,
        0x01,0x6f,0x10,0x3f,0x15,0xf0,0x11,0x3f,
        0x01,0x6f,0x09,0xf0,0x01,0x6f,0x10,0x3f,
        0x14,0xf0,0x10,0x3f,0x0c,0xf0,0x01,0x6f,
        0x0f,0x3f,0x13,0xf0,0x0f,0x3f,0x01,0x6f,
        0x0e,0xf0,0x02,0x6f,0x0e,0x3f,0x12,0xf0,
        0x0e,0x3f,0x01,0x6f,0x11,0xf0,0x01,0x6f,
        0x0d,0x3f,0x12,0xf0,0x0d,0x3f,0x01,0x6f,
        0x13,0xf0,0x01,0x6f,0x0d,0x3f,0x10,0xf0,
        0x0d,0x3f,0x16,0xf0,0x01,0x6f,0x0c,0x3f,
        0x10,0xf0,0x0b,0x3f,0x01,0x6f,0x18,0xf0,
        0x02,0x6f,0x0b,0x3f,0x0e,0xf0,0x0b,0x3f,
        0x01,0x6f,0x1a,0xf0,0x02,0x6f,0x0a,0x3f,
        0x0e,0xf0,0x0a,0x3f,0x1d,0xf0,0x02,0x6f,
        0x0a,0x3f,0x0c,0xf0,0x0a,0x3f,0x1f,0xf0,
        0x02,0x6f,0x09,0x3f,0x0c,0xf0,0x08,0x3f,
        0x01,0x6f,0x22,0xf0,0x02,0x6f,0x07,0x3f,
        0x0b,0xf0,0x08,0x3f,0x01,0x6f,0x24,0xf0,
        0x02,0x6f,0x07,0x3f,0x0a,0xf0,0x07,0x3f,
        0x01,0x6f,0x26,0xf0,0x02,0x6f,0x06,0x3f,
        0x0a,0xf0,0x06,0x3f,0x29,0xf0,0x02,0x6f,
        0x06,0x3f,0x08,0xf0,0x05,0x3f,0x01,0x6f,
        0x2c,0xf0,0x06,0x3f,0x08,0xf0,0x04,0x3f,
        0x01,0x6f,0x2e,0xf0,0x06,0x3f,0x06,0xf0,
        0x04,0x3f,0x01,0x6f,0x30,0xf0,0x05,0x3f,
        0x06,0xf0,0x02,0x3f,0x01,0x6f,0x33,0xf0,
        0x05,0x3f,0x04,0xf0,0x02,0x3f,0x01,0x6f,
        0x35,0xf0,0x04,0x3f,0x04,0xf0,0x02,0x3f,
        0x38,0xf0,0x03,0x3f,0x02,0xf0,0x02,0x3f,
        0xff,0xf0,0xff,0xf0,0x61,0xf0
};


const uint8_t coblation_icon[] = {
    0xff ,0xf0 ,0xdf ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0x3c ,0xf0 ,
    0x04 ,0x3f ,0x3c ,0xf0 ,0x04 ,0x3f ,0xe5 ,0xf0 ,
    0x12 ,0x3f ,0x0f ,0xf0 ,0x11 ,0x3f ,0x0e ,0xf0 ,
    0x13 ,0x3f ,0x0d ,0xf0 ,0x12 ,0x3f ,0x0e ,0xf0 ,
    0x14 ,0x3f ,0x0b ,0xf0 ,0x13 ,0x3f ,0x0e ,0xf0 ,
    0x15 ,0x3f ,0x09 ,0xf0 ,0x14 ,0x3f ,0x1e ,0xf0 ,
    0x06 ,0x3f ,0x07 ,0xf0 ,0x06 ,0x3f ,0x2e ,0xf0 ,
    0x05 ,0x3f ,0x07 ,0xf0 ,0x05 ,0x3f ,0x30 ,0xf0 ,
    0x05 ,0x3f ,0x05 ,0xf0 ,0x05 ,0x3f ,0x32 ,0xf0 ,
    0x05 ,0x3f ,0x03 ,0xf0 ,0x05 ,0x3f ,0x34 ,0xf0 ,
    0x05 ,0x3f ,0x01 ,0xf0 ,0x05 ,0x3f ,0x36 ,0xf0 ,
    0x09 ,0x3f ,0x38 ,0xf0 ,0x07 ,0x3f ,0x3a ,0xf0 ,
    0x05 ,0x3f ,0x3c ,0xf0 ,0x03 ,0x3f ,0x3e ,0xf0 ,
    0x01 ,0x3f ,0xff ,0xf0 ,0xff ,0xf0 ,0xe4 ,0xf0
};

const char* g_text_list[MAX_SIM_DEVICES] = {
    "D2 ", "D25", "VID", "WWF"
};
const uint8_t g_device_type_list[MAX_SIM_DEVICES] = { // use device id to index this
    DEVICE_TYPE_SHAVER,
    DEVICE_TYPE_FLUID_MANAGEMENT,
    DEVICE_TYPE_VIDEO,
    DEVICE_TYPE_COBLATION,
};
const char* g_device_name_list[MAX_SIM_DEVICES] = { // use device id to index this
    "Shaver",
    "D25",
    "LENS",
    "Werewolf",
};
const uint8_t* g_icon_list[MAX_SIM_DEVICES] = {
    shaver_icon,
    shaver_icon,
    coblation_icon,
    coblation_icon,
};
const size_t g_icon_size_list[MAX_SIM_DEVICES] = {
    sizeof(shaver_icon),
    sizeof(shaver_icon),
    sizeof(coblation_icon),
    sizeof(coblation_icon),
};
uint8_t g_uid_list[MAX_ICONS_PER_DEVICE];

static bool cmd_verbose = false;
static const char* cmd_server_ip = "192.168.0.114";
static int cmd_device_id = 4;
static int cmd_icon_cnt = 1;
static const char* g_device_name = NULL;
static uint32_t g_device_type = DEVICE_TYPE_RESERVED;

/*
 * Constants & Types
 */


static void usage();
static int parse_cmd_line(int argc, char *argv[]);
static void change_state(OsdClientConnection* con, int state);



void nop() {}


#if defined(OSD_DUMP_BSD_BYTES) 
///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
dump_bytes(char *label, uint8_t *ptr, uint32_t n)
{
uint32_t	i;

	printf("%s: ", label);
	for(i=0; i<n; i++, ptr++)
		printf("%x ", *ptr);
	printf("\n");
}
#endif

#if defined(OSD_DEBUG_ICON) || defined(OSD_DEBUG_PROTOCOL) || defined(OSD_DEBUG_MESSAGES)
///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
dump_icon(OsdMsg_StatusUpdate *ps)
{
iconCntrReg_t				*picr;
iconBarReg_t				*pbar;
iconBarTextReg_t			*pbar2;
unControlReg1_t				*ptext;
unControlReg2_t				*ptext2;
unAlphaRectControlReg1_t	*pbox;
unAlphaRectControlReg2_t	*pbox2;
uint32_t					*pbitmask;
uint8_t						text[12];


	pbitmask = (uint32_t*) &ps->bm0;
	picr = (iconCntrReg_t*) &ps->icon_cntr_reg0;
	pbar = (iconBarReg_t*) &ps->icon_bar_reg0;
	pbar2 = (iconBarTextReg_t*) &ps->icon_bar_text_reg0;
	ptext = (unControlReg1_t*) &ps->ctrl_reg1_0;
	ptext2 = (unControlReg2_t*) &ps->ctrl_reg2_0;
	pbox = (unAlphaRectControlReg1_t*) &ps->alpha_rect1_0;
	pbox2 = (unAlphaRectControlReg2_t*) &ps->alpha_rect2_0;

	dump_icon_on_bdm_message = 0;

	printf(" --- Icon Dump:	netid: %x,  UID: %x, bitmask: %x\n", ps->netid, ps->uid, *pbitmask);

	printf("	BOX: (%d,%d) (%d,%d), al: %d, uu: %x\n", pbox->fields.hpos/4, pbox->fields.vpos/4,
		pbox2->fields.width/4, pbox2->fields.height/4, pbox->fields.alpha, pbox2->fields.unused);

	printf("	ICR: (%d,%d)), lh: %d, rh: %d, colorov: %x/%x/%x, sz: %x, tg: %x\n",
		picr->fields.hposition, picr->fields.vposition, picr->fields.lefthide, picr->fields.righthide,
		picr->fields.coloroverride, picr->fields.coloroverideval_left, picr->fields.coloroverideval_right,
		picr->fields.size, picr->fields.toggle);

	printf("	BAR: vis: %x, loc: %x, pf: %x, color: %x, uu: %x, need: %x, index: %x, uu: %x\n",
		pbar->fields.visible, pbar->fields.location, pbar->fields.percFull, pbar->fields.color, pbar->fields._unused,
		pbar2->fields.need, pbar2->fields.index, pbar2->fields._unused);

	memset(text, 0, 12);
	snprintf((char*) text, 8, "%s", ps->text);

	printf("	TXT: [%s] (%d,%d), strlen: %d, color: %x, iCol: %x, al: %x, cs: %d, sc: %d, sh: %d, uu: %x\n",
		text, ptext->fields.hpos/2, ptext->fields.vpos/2, ptext->fields.strLen, ptext->fields.colorIndx, 
		ptext2->fields.invertColor, ptext2->fields.alpha, ptext2->fields.charspacing,
		ptext2->fields.scale, ptext2->fields.shrink, ptext2->fields.unused);

#if defined(OSD_DEBUG_PROTOCOL)
	uint8_t		*c = (uint8_t*) ps;
	printf("\n --- %d protocol bytes:  ", sizeof(OsdMsg_StatusUpdate));

	for(int32_t i=0; i<sizeof(OsdMsg_StatusUpdate); i++)
		printf("%x ", *(c++));

	printf("\n\n");
#endif

	return;
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
dump_D2_message(scd_bdm_port_status_t *d2_status)
{
port_display_t			*da = NULL, *db = NULL;
// port_speed_run_t		*ps;

	da = (port_display_t*) &d2_status->porta_display;
	db = (port_display_t*) &d2_status->portb_display;

	printf("%ld: ===== New Message =====\n", time(NULL));
	printf("Port Display:  	A: %x	B: %x\n", da->units, db->units);
	printf("Port Blade:	A: %x, 	B: %x\n", da->blade, db->blade);
	printf("Port Mode:	A: %x, 	B: %x\n", da->mode, db->mode);
	printf("Port Up Arrow:	A: %x, 	B: %x\n", da->up_arrow, db->down_arrow);
	printf("Port DN Arrow:	A: %x, 	B: %x\n", da->up_arrow, db->down_arrow);
}

static void init_osd_status_update(Osd_StatusUpdateData* sud, int device_id, 
	int icon_id)
{
#define BAR_0_PERCENT 7
#define BAR_50_PERCENT 0x1907
#define BAR_80_PERCENT 0x1997

#define H_TRUE_SPACE 256

    // Icon size: 128x128
    // Icon position will be X4 for 4K
#define ICON_TRUEVPOS_64_TRUEHPOS_0 0x69804000
#define ICON_INC (H_TRUE_SPACE/4)

    // Text position will be X2 for 4K
#define TEXT_LEN3_RED0_TRUEV_188_HPOS_0 0xC30000
#define TEXT_COLOR_INC 0x10000000
#define TEXT_INC (H_TRUE_SPACE/2)

    assert(sud);

    // fixed/determined settings
    sud->bitmask = 0x3F;
    sud->icon_cr2 = 0x21E;
    sud->icon_bar_text_reg = 0xD1;
    sud->text = g_text_list[device_id];

    // randomized settings
    if (icon_id & 1) {
        sud->icon_bar_reg = BAR_50_PERCENT;
    } else {
        sud->icon_bar_reg = BAR_80_PERCENT;
    }

    // positioned settings
    sud->icon_cntr_reg = ICON_TRUEVPOS_64_TRUEHPOS_0 +
        (MAX_ICONS_PER_DEVICE +1) * device_id * ICON_INC +
        ICON_INC * icon_id;
    sud->icon_cr1 = TEXT_LEN3_RED0_TRUEV_188_HPOS_0 +
        (MAX_ICONS_PER_DEVICE +1) * device_id * TEXT_INC +
        TEXT_INC * icon_id +
        TEXT_COLOR_INC * (device_id * MAX_SIM_DEVICES + icon_id);
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
initialize_bar_control_registers(iconBarReg_t *r1, iconBarTextReg_t *r2)
{
	r1->fields.location = 1;
	r1->fields.percFull = 32;
	r1->fields.color = etxtDefaultOrange;
	r1->fields.visible = 0;
	r1->fields._unused = 1;

	r2->fields._unused = 1;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//

void
set_rectangle_dimansions(OsdClientConnection *picon, uint32_t width, uint32_t height)
{
unAlphaRectControlReg2_t	*pbox = (unAlphaRectControlReg2_t*) &picon->msg_status_update->alpha_rect2_0;

	pbox->fields.width = width * 4;
	pbox->fields.height = height * 4;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//

void
initialize_icon_control_registers(iconCntrReg_t  *r1)
{
	r1->fields.hposition = 36;
	r1->fields.vposition = 316;
	r1->fields.lefthide = 0;
	// r1->fields.coloroverride = 0;     //  CCCCC: was 3, set to 0 for experimental purposes
	r1->fields.coloroverride = 3;     //  CCCCC: was 3, set to 0 for experimental purposes
	r1->fields.coloroverideval_left = eOrangeDark;
	r1->fields.coloroverideval_right = eOrangeDark;
	r1->fields.size = 1;
	r1->fields.toggle = 0;
	r1->fields.righthide = 0;
}

#if 0
///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
// NOTE:	Not currently used
//

void
initialize_text_control_registers(unControlReg1_t *r1, unControlReg2_t *r2)
{
	r1->fields.hpos = 132;
	r1->fields.vpos = 670;
	r1->fields.strLen = 0;
	r1->fields.colorIndx = etxtDefaultOrange;

	r2->fields.invertColor = 0;
	r2->fields.alpha = 15;
	r2->fields.charspacing = 2;
	r2->fields.scale = text_scale;	//  TTTTT change the text sie here, originally was 1
	r2->fields.unused = 1;
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void 
initialize_alpha_control_registers(unAlphaRectControlReg1_t*r1, unAlphaRectControlReg2_t *r2)
{
	r1->fields.alpha = 255;

	r2->fields.unused = 1;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

int32_t
create_socket()
{
uint32_t	on = 1, sockfd;
struct 		sockaddr_in svr_addr;
struct 		hostent *server;


    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == 0) {
        printf("ERROR opening socket: %s", strerror(errno));
        return -1;
    }

    // set nodelay
    if (setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on))) {
        printf("Error: set socket option NODELAY failed\n");
        return -1;
    }

    server = gethostbyname(cmd_server_ip);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host %s\n", cmd_server_ip);
		close(sockfd);
		sockfd = -1;
		return -1;
    }

    bzero((char *) &svr_addr, sizeof(svr_addr));
    svr_addr.sin_family = AF_INET;
    // svr_addr.sin_addr = con->addr;

    bcopy(server->h_addr,
          (char *)&svr_addr.sin_addr.s_addr,
          (size_t)server->h_length);

    svr_addr.sin_port = htons(OSD_SVR_LISTENING_TCP_PORT);

    //
    //              connect to the OSD server
    //

    if(cmd_verbose) printf("Connecting to %s (port %d) as %s\n",
            cmd_server_ip, OSD_SVR_LISTENING_TCP_PORT, g_device_name);
    if (connect(sockfd, (struct sockaddr *) &svr_addr, sizeof(svr_addr)) < 0) {
        perror("ERROR connecting to server");
		return -1;
    }

    if(cmd_verbose) printf("Connected\n");

	return sockfd;
}


///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_text_location(OsdClientConnection *picon, uint32_t x, uint32_t y)
{
unControlReg1_t				*ptext;

	ptext = (unControlReg1_t*) &picon->msg_status_update->ctrl_reg1_0;

	ptext->fields.hpos = x * 2;
	ptext->fields.vpos = y * 2;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_hide(OsdClientConnection *picon, uint32_t mode)
{
iconCntrReg_t				*picr;

	picr = (iconCntrReg_t*) &picon->msg_status_update->icon_cntr_reg0;
	
	if(mode == 0)
	{
		picr->fields.lefthide = 1;
	}
	else if(mode == 1)
	{
		picr->fields.righthide = 1;
	}
	else if(mode <= 3)
	{
		picr->fields.righthide = 0;
		picr->fields.lefthide = 0;
	}
	else 
	{
		picr->fields.righthide = 1;
		picr->fields.lefthide = 1;
	}
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_invisible(OsdClientConnection *picon)
{
iconCntrReg_t				*picr;

	picr = (iconCntrReg_t*) &picon->msg_status_update->icon_cntr_reg0;

	picr->fields.lefthide = 1;
	picr->fields.righthide = 1;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_visibility(OsdClientConnection *picon, uint32_t value)
{
unAlphaRectControlReg1_t	*pbox;
unControlReg2_t				*ptext2;


	pbox = (unAlphaRectControlReg1_t*) &picon->msg_status_update->alpha_rect1_0;
	ptext2 = (unControlReg2_t*) &picon->msg_status_update->ctrl_reg2_0;

	if(value == 0)
	{
		set_icon_hide(picon, 4);
		pbox->fields.alpha = 0;
		ptext2->fields.alpha = 0;
	}
	else
	{
		set_icon_hide(picon, 3);
		pbox->fields.alpha = 255;
		ptext2->fields.alpha = 15;
	}
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_color(OsdClientConnection *picon, uint32_t color)
{
iconCntrReg_t				*picr;

	picr = (iconCntrReg_t*) &picon->msg_status_update->icon_cntr_reg0;

	picr->fields.coloroverideval_left = color;
	picr->fields.coloroverideval_right = color;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_text_color(OsdClientConnection *picon, uint32_t color)
{
unControlReg1_t				*ptext;

	ptext = (unControlReg1_t*) &picon->msg_status_update->ctrl_reg1_0;

	ptext->fields.colorIndx = color;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_text_visibility(OsdClientConnection *picon, uint32_t value)
{
unControlReg2_t				*ptext2;


	ptext2 = (unControlReg2_t*) &picon->msg_status_update->ctrl_reg2_0;

	if(value == 0)
		ptext2->fields.alpha = 0;
	else
		ptext2->fields.alpha = 0xf;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_visible(OsdClientConnection *picon)
{
iconCntrReg_t				*picr;
unControlReg2_t				*ptext2;
unAlphaRectControlReg1_t	*pbox;


	picr = (iconCntrReg_t*) &picon->msg_status_update->icon_cntr_reg0;
	ptext2 = (unControlReg2_t*) &picon->msg_status_update->ctrl_reg2_0;
	pbox = (unAlphaRectControlReg1_t*) &picon->msg_status_update->alpha_rect1_0;

	picr->fields.lefthide = 1;
	picr->fields.righthide = 1;

	ptext2->fields.alpha = 0xF;
	pbox->fields.alpha = 255;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_progress_bar(OsdClientConnection *picon, uint32_t state, uint32_t location)
{
iconCntrReg_t	*piconreg = (iconCntrReg_t*) &picon->msg_status_update->icon_cntr_reg0;
iconBarReg_t	*pbar = (iconBarReg_t*) &picon->msg_status_update->icon_bar_reg0;

	pbar->fields.location = location;
	pbar->fields.percFull = 63;
	pbar->fields.color = piconreg->fields.coloroverideval_left;

	if(state == PROGRESS_BAR_ENABLE)
	{
		pbar->fields.visible = 1;
	}
	else
	{
		pbar->fields.visible = 0;
	}

}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_icon_text_field(OsdClientConnection *picon, uint8_t *text)
{
unControlReg1_t				*ptext;

	ptext = (unControlReg1_t*) &picon->msg_status_update->ctrl_reg1_0;

	// snprintf((char*) picon->msg_status_update->text, OSDMANAGER_TEXTSTRLENGTH, "%s", text); 
	memset(picon->msg_status_update->text, 0, OSDMANAGER_TEXTSTRLENGTH);
	memcpy(picon->msg_status_update->text, text, strlen((char*) text));
	ptext->fields.strLen = strlen((char*) text);
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

int32_t
register_icon(OsdClientConnection *picon, int fd)
{
int32_t					n, rc, status=0;
uint8_t 				buffer[MAX_PACKET_SIZE];
IFullMsg				*msg;


	//
	//	send out Registration Request
	//

	OsdMsg_RegRqst* msg1 = create_reg_request_msg(picon->netid, picon->device, (char*) picon->icon_name);

	assert(msg1);

	n = write(fd, msg1, full_msg_len(ntohl(msg1->header.data_len_nw)));
	if (n < 0) {
		perror("ERROR writing to client socket");
		change_state(picon, STATE_DISCONNECTED);
		return -1;
	}
	printf(C_GREEN"Sent Registration Request\n"C_END);
	if (cmd_verbose) print_ifull_msg((IFullMsg*)msg1);
	free(msg1);

	//
	//              receive Registration Response
	//

	if(cmd_verbose) printf("Waiting for Registration Response.\n");

	rc = wait_for_tcp_packet_with_timeout(fd, buffer, MAX_PACKET_SIZE, TIMEOUT_MS);
	if (rc < 0) {
		return -1;
	}

	msg = (IFullMsg*) buffer;

	if (msg->command != OSD_CMD_SERVER_REGISTRATION_RESPONSE) {
		printf("Unexpected server reply, expected  0x%02X, got 0x%02X\n",
		OSD_CMD_SERVER_REGISTRATION_RESPONSE, msg->command);
		return -1;
	}

	printf(C_GREEN"Registration Response received.\n"C_END);
	OsdMsg_RegResp* resp = (OsdMsg_RegResp*) msg;

	picon->uid = resp->uid;

	//
	//              send out Pop ICON Request
	//

printf("register_icon: bitmap size: %d\n", picon->nbytes);
	OsdMsg_PopulateIcon* msg2 = create_pop_icon_msg(picon->netid, picon->uid, 
		picon->bitmap, picon->nbytes);

	assert(msg2);

	n = write(fd, msg2, full_msg_len(ntohl(msg2->header.data_len_nw)));
	if (n < 0) {
		perror("ERROR writing to client socket");
		change_state(picon, STATE_DISCONNECTED);
		return -1;
	}

	printf(C_GREEN"Sent Populate ICON Request.\n"C_END);
	if (cmd_verbose) print_ifull_msg((IFullMsg*)msg2);
	free(msg2);

	//
	//             receive Pop ICON Response
	//

	if(cmd_verbose) printf("Waiting for Populate Icon Response.\n");

	rc = wait_for_tcp_packet_with_timeout(fd, buffer, MAX_PACKET_SIZE, TIMEOUT_MS);
	if (rc < 0) 
	{
		printf("Receive TCP packet error: return value = %d\n", rc);
		return -1;
	}

	OsdMsg_PopIconResp* pir = (OsdMsg_PopIconResp*) buffer;

	if (pir->header.command != OSD_CMD_SERVER_POPULATE_ICON_RESPONSE) {
		printf("Unexpected server reply, expected  0x%02X, got 0x%02X\n",
		OSD_CMD_SERVER_POPULATE_ICON_RESPONSE, pir->header.command);
		return -1;
	}

	printf(C_GREEN"Pop ICON Response received. NetID:%u UID:%u. "C_END"\n",
		pir->netid, pir->uid);

	if (pir->result != 0 )
	{
		switch (pir->result) 
		{
			default:
			case 1:
				printf(C_RED"Unknow error\n"C_END);
				break;
			case 2:
				printf(C_RED"UID does not match owner\n"C_END);
				break;
			case 3:
				printf(C_RED"Invalid UID\n"C_END);
				break;
		}
	} 

	return status;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
// 
//


int32_t
update_icon(int32_t fd, OsdClientConnection *picon)
{
struct	timeval     curtime;
int32_t		        rc, n;
uint8_t 	        buffer[MAX_PACKET_SIZE];


    // zero out this bit for the next time around
    picon->modified = 0;
    gettimeofday(&curtime, NULL);
    memcpy(&picon->last_update, &curtime, sizeof(curtime));

	//
	// Calculate the CRC of the message
	//

	picon->msg_status_update->crc = calc_crc((uint8_t*) picon->msg_status_update, 
		sizeof(IFullMsgHeader) + OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN);

	//
	// Send the message
	//

#if defined(OSD_DEBUG_ICON)
	dump_icon(picon->msg_status_update);
#endif

    n = write(fd, picon->msg_status_update, full_msg_len(ntohl(picon->msg_status_update->header.data_len_nw)));

   	if (n < 0) 
	{
   		printf("%d: ERROR writing to client socket", (unsigned int) time(NULL));
   		change_state(picon, STATE_DISCONNECTED);
   		return -1;
	}

	//
	//              receive status update response
	//
   	rc = wait_for_tcp_packet_with_timeout(fd, buffer, MAX_PACKET_SIZE, 
		RECEIVE_TIMEOUT_MS);

   	if (rc < 0) 
	{
		printf("%d: wait_for_tcp_packet_with_timeout: %d\n", (unsigned int) time(NULL), rc);
	}
	else if (rc == 0) 
	{
		OsdMsg_StatusUpdateResp* sur = (OsdMsg_StatusUpdateResp*) buffer;
		if (sur->header.command == OSD_CMD_SERVER_STATUS_UPDATE_RESPONSE) 
        {
			printf(C_RED"RTOS -> EXT: "
				"Status Update Reponse/NAK: NetID:%u, UID:%u"C_END,
				sur->netid, sur->uid);
				print_ifull_msg((IFullMsg*)sur);
				return -1;
		}
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

OsdClientConnection *
create_icon(int32_t fd, uint8_t *name, uint8_t *bitmap, uint32_t nbytes)
{
struct timeval          curtime;
OsdClientConnection 	*picon;
int32_t					status;


	picon = calloc(1, sizeof(OsdClientConnection));
	if(picon == NULL)
	{
		// TODO: log an error and exit
		return NULL;
	}

	strcpy(picon->id, "OsdClientConnection");

	// Fill in some stuff for the ICON
    struct sockaddr_in cli_addr;
    socklen_t len = sizeof(struct sockaddr_in);
    getsockname(fd, (struct sockaddr *) &cli_addr, &len);

    picon->netid = (uint8_t)(((uint32_t) cli_addr.sin_addr.s_addr) >> 24);

	picon->bitmap = bitmap;
	picon->bitmap_size = 128;
	picon->nbytes = nbytes;
	strcpy(picon->icon_name, (char*) name);
	picon->device = 1;

    gettimeofday(&curtime, NULL);
    curtime.tv_sec -= 3;
    memcpy(&picon->last_update, &curtime, sizeof(curtime));

	//
	// Register the Icon with the LENS 4K
	//

	status = register_icon(picon, fd);
	if(status < 0)
	{
		// TODO: log an error and exit
		printf("Failed to register icon: %s\n", picon->icon_name);
		return NULL;
	}


#if defined(USE_THE_NEW_STUFF)
	picon->msg_status_update = calloc(1, sizeof(OsdMsg_StatusUpdate));
	if(picon->msg_status_update == NULL)
	{
		// TODO: log an error and exit
		return NULL;
	}

    picon->msg_status_update->netid = picon->netid;
    picon->msg_status_update->uid = picon->uid;
    picon->msg_status_update->header.protocol_id = OSD_PROTO_ID_C2S;
    picon->msg_status_update->header.command = OSD_CMD_CLIENT_STATUS_UPDATE;
    picon->msg_status_update->header.data_len_nw = htonl(OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN);
    picon->msg_status_update->crc = calc_crc((uint8_t*)picon->msg_status_update, sizeof(IFullMsgHeader) + OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN);
#else
	//
	//				Use the old stuff from Peng's original code
	//              send out status update
	//

	OsdMsg_StatusUpdate* msg3;
	if (!picon->msg_status_update) 
	{
		msg3 = create_status_update_msg(picon->netid, picon->uid);

		assert(msg3);

		picon->msg_status_update = msg3;
	}

    Osd_StatusUpdateData sud;
    init_osd_status_update(&sud, cmd_device_id, 0);

    update_status_update_msg(picon->msg_status_update,
    	picon->netid, picon->uid, &sud);
#endif

#if 0
		status = modify_icon(picon, DEVICE_TYPE_SHAVER, x, y, width, height, bitmap_size);
		if(status < 0)
		{
			// TODO: log an error and exit
			printf("Failed to create icon: %s\n", (char*) picon->icon_name);
			return NULL;
		}
#endif

	return picon;
}

///////////////////////////////////////////////////////////////////////////////
// set_icon_text_field(OsdClientConnection *picon, uint8_t *text)
// set_icon_visible(OsdClientConnection *picon)
// set_icon_text_visibility(OsdClientConnection *picon, uint32_t value)
// set_text_color(OsdClientConnection *picon, uint32_t color)
// set_icon_color(OsdClientConnection *picon, uint32_t color)
// set_icon_visibility(OsdClientConnection *picon, uint32_t value)
// set_icon_invisible(OsdClientConnection *picon)
// set_icon_hide(OsdClientConnection *picon, uint32_t mode)
// set_icon_text_location(OsdClientConnection *picon, uint32_t x, uint32_t y)
// 

///////////////////////////////////////////////////////////////////////////////

void
do_special_LENS4K_math(OsdClientConnection *picon)
{
unAlphaRectControlReg1_t	*pbox;
unAlphaRectControlReg2_t	*pbox2;
unControlReg1_t	*ptext; 
// unControlReg2_t	*ptext2;

	ptext = (unControlReg1_t*) &picon->msg_status_update->ctrl_reg1_0;
	// ptext2 = (unControlReg2_t*) &picon->msg_status_update->ctrl_reg2_0;

	pbox = (unAlphaRectControlReg1_t*) &picon->msg_status_update->alpha_rect1_0;
	pbox2 = (unAlphaRectControlReg2_t*) &picon->msg_status_update->alpha_rect2_0;

	pbox->fields.hpos *= 4;
	pbox->fields.vpos *= 4;
	pbox2->fields.width *= 4;
	pbox2->fields.height *= 4;

	ptext->fields.hpos  *= 2;
	ptext->fields.vpos  *= 2;

	// Now mod 4 the text positions to see if we can clean up the text a little bit
	if(getenv("OSD_TEXT_4BIT"))
	{
		printf("Forcing all text coords on 4 bit boundaries\n");
		ptext->fields.hpos  &= ~3;
		ptext->fields.vpos  &= ~3;
	}


}

#if 0
///////////////////////////////////////////////////////////////////////////////
void
set_icon_position(OsdClientConnection *picon, uint8_t x, uint8_t y)
{


}
#endif



///////////////////////////////////////////////////////////////////////////////
void
set_rect_dimensions(OsdClientConnection *picon, uint32_t x, uint32_t y, uint32_t width, uint32_t height, uint32_t alpha)
{
unAlphaRectControlReg1_t	*pbox;
unAlphaRectControlReg2_t	*pbox2;

	pbox = (unAlphaRectControlReg1_t*) &picon->msg_status_update->alpha_rect1_0;
	pbox2 = (unAlphaRectControlReg2_t*) &picon->msg_status_update->alpha_rect2_0;
	
	// Set the X,Y coords of the box/rectangle
	pbox->fields.hpos = x;
	pbox->fields.vpos = y;
	pbox->fields.alpha = alpha;

	pbox2->fields.width = width;
	pbox2->fields.height = height;
	pbox2->fields.unused = 1;
}

///////////////////////////////////////////////////////////////////////////////
void
set_text_scale(OsdClientConnection *picon, uint8_t scale)
{
unControlReg2_t	*ptext2;

	ptext2 = (unControlReg2_t*) &picon->msg_status_update->ctrl_reg2_0;

	ptext2->fields.scale = scale;	// CCCCC change the text scale here. Was 1
}

///////////////////////////////////////////////////////////////////////////////
void
set_text_coords(OsdClientConnection *picon, uint32_t x, uint32_t y)
{
unControlReg1_t	*ptext; 

	ptext = (unControlReg1_t*) &picon->msg_status_update->ctrl_reg1_0;

	ptext->fields.hpos = x;
	ptext->fields.vpos = y;
}

///////////////////////////////////////////////////////////////////////////////
void
set_text_register(OsdClientConnection *picon, uint8_t *txt, uint32_t x, uint32_t y, uint32_t color, uint32_t scale)
{
unControlReg1_t	*ptext; 
unControlReg2_t	*ptext2;

	ptext = (unControlReg1_t*) &picon->msg_status_update->ctrl_reg1_0;
	ptext2 = (unControlReg2_t*) &picon->msg_status_update->ctrl_reg2_0;

	set_icon_text_field(picon, txt);

	set_text_coords(picon, x, y);
	// ptext->fields.hpos = x;
	// ptext->fields.vpos = y;
	ptext->fields.colorIndx = color;

	ptext2->fields.invertColor = 0;
	ptext2->fields.shrink = 1;
	ptext2->fields.charspacing = 2;
	ptext2->fields.alpha = 15;
	ptext2->fields.unused = 1;

	set_text_scale(picon, scale);
}

///////////////////////////////////////////////////////////////////////////////
void
set_icon_register(OsdClientConnection *picon, uint32_t x, uint32_t y, uint32_t color)
{
iconCntrReg_t	*picr;

	picr = (iconCntrReg_t*) &picon->msg_status_update->icon_cntr_reg0;

	// Set the X,Y coords of the icon
	// picr->fields.hposition = pbox->fields.hpos + pbox2->fields.width/2 - bitmap_size/8;
	// picr->fields.vposition = pbox->fields.vpos + 0*ICON_HEIGHT_SPACING;
	picr->fields.hposition = x;
	picr->fields.vposition = y;

	// other ICON reister settings
	picr->fields.toggle = 0;
	picr->fields.size = 1;
	picr->fields.lefthide = 0;
	picr->fields.righthide= 0;
	// picr->fields.coloroverride = 0;   // CCCCC: wa 3 originally, set to 0 temp...
	picr->fields.coloroverride = 3;   // CCCCC: wa 3 originally, set to 0 temp...
	picr->fields.coloroverideval_left = color;
	picr->fields.coloroverideval_right = color;

	picon->device = DEVICE_TYPE_SHAVER;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_bar_visibility(OsdClientConnection *picon, uint32_t visibility)
{
iconBarReg_t				*pbar;

	pbar = (iconBarReg_t*) &picon->msg_status_update->icon_bar_reg0;

	pbar->fields.visible = visibility;
}

#if 0
///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_bar_location(OsdClientConnection *picon, uint32_t location)
{
}
#endif

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
setup_basic_icon_data(OsdClientConnection *icon, icon_init_t *data, int n)
{

	if(strcmp(icon->id, "OsdClientConnection"))
		CRASH;

		uint32_t *pbitmask = (uint32_t*) &icon->msg_status_update->bm0;
		*pbitmask = 0xFF;

		set_text_color(icon, data->text_color[n]);

		set_rect_dimensions(icon, data->rect_coords[n].x, 
			data->rect_coords[n].y, data->width[n], data->height[n], 255);

		set_text_register(icon, data->text[n], 
			data->rect_coords[n].x + data->text_offset[n].x, 
			data->rect_coords[n].y + data->text_offset[n].y, 
			data->text_color[n], data->text_scale[n]);

		set_icon_register(icon, data->rect_coords[n].x + data->icon_offset[n].x, 
			data->rect_coords[n].y + data->icon_offset[n].y, data->icon_color[n]);

		set_bar_visibility(icon, data->bar_visible[n]);

		if(n > 0) set_icon_visible(icon);

#if defined(OSD_DEBUG)
		dump_icon(icon->msg_status_update);
#endif

		do_special_LENS4K_math(icon);
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

icon_group_t *
create_icon_group(uint32_t n, icon_init_t *data)
{
icon_group_t		*group;


	group = calloc(1, sizeof(icon_group_t));
	if(group == NULL)
		return NULL;

	strcpy(group->id, "icon_group_t");

	group->nicons = n;

	for(uint32_t i=0; i<n; i++)
	{
		// Create the first icon
		group->icon[i] = create_icon(data->fd, (uint8_t*) data->name[i], 
			data->bitmap[i], data->nbytes[i]);

		if(group->icon[i] == NULL)
		{
			free(group);
			return NULL;
		}

		setup_basic_icon_data(group->icon[i], data, i);
	}

	return group;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_iconpair_from_bdm_data(icon_group_t *pair, port_display_t *pd, port_speed_run_t *ps)
{

#if defined(OSD_DUMP_BSD_BYTES)
	dump_bytes("ENTER: set_iconpair_from_bdm_data: ", (uint8_t*) pd, sizeof(port_display_t));
	printf("\n");
#endif

#if defined(OSD_DEBUG_MEMORY_CORRUPTION)
	if(strcmp(pair->id, "icon_group_t"))
		CRASH;
#endif

    pair->icon[0]->modified = 1;

	// make the icon either visible or invisible depending on the value
	// of what is in the 'units' variable

	if(pd->units == 0)
	{
		set_icon_visibility(pair->icon[0], pd->units);
		set_icon_text_visibility(pair->icon[1], 0);

		// nothing more needs to be done
		return;
	}

	set_icon_visibility(pair->icon[0], 1);
	set_icon_hide(pair->icon[0], pd->mode);

	// no matter what though, the ICON for icon[1] should never be visible
	// so we need to undo thsat here and now
	set_icon_text_visibility(pair->icon[1], pd->units);

	// If units is equal to 0, then the icon will not be displayed, so
	// nothing more needs to be done. The code to handle visibility 
	// is just above ...

	if(pd->units == 0)
		return;

	// Do the ROMs and color

	if(ps->state == 0)
	{
		set_icon_color(pair->icon[0], eOrangeDark);
		set_text_color(pair->icon[0], etxtDefaultOrange);
	}
	else
	{
		set_icon_color(pair->icon[0], eGreen);
		set_text_color(pair->icon[0], etxtGreen);
	}

	// Now do the speed
	uint8_t	speed[9];
	snprintf((char*) speed, OSDMANAGER_TEXTSTRLENGTH+1, port_format_string[pd->units],
		ps->speed * port_units_multiplier[pd->units]);

	set_icon_text_field(pair->icon[0], speed);
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
set_WW_iconpair_from_bdm_data(icon_group_t *pair, ctn_ww_get_response_data_t *werewolf)
{
therapy_settings_t		*ptherapy = (therapy_settings_t*) &werewolf->therapy_set;
active_wand_t			*wand = (active_wand_t*) &werewolf->wand_info;
ambient_settings_t		*ambient_settings = (ambient_settings_t*) &werewolf->amb_set;
uint8_t					*temp = (uint8_t*) &werewolf->amb_temp;
uint8_t					buffer[16], *ablate_coag_submode = &werewolf->ab_coag_smart_sp;
// uint8_t					*legacy_ablate = (uint8_t*) &werewolf->leg_amb_set;
uint8_t					ablate_shift_mask[4] = {0, 0x3, 0xc, 0x30};
uint32_t				ablate_shift_bits[4] = {0, 0, 2, 4};
char	                *coag_set_point[4] = {".", ".", "+", "+"};
unsigned char	        legacy_ablate_value;
iconCntrReg_t	        *picr;
char                    *therapy_modes[4][4] = 
{
	{"", "", "", ""},
	{"", "LOW -", "LOW .", "LOW +"},
	{"", "MED -",  "MED .",  "MED +"},
	{"", "HI -", "HI .", "HI +"}
};


#if defined(OSD_DUMP_BSD_BYTES)
	dump_bytes("ENTER: set_WW_iconpair_from_bdm_data: ", (uint8_t*) werewolf, sizeof(ctn_ww_get_response_data_t));
	printf("\n");
#endif

	if(ptherapy->therapy_mode & 0x4)
		return;

	// 
	// We're visible.
	//

	set_icon_visibility(pair->icon[0], 1);
	set_icon_visibility(pair->icon[1], 1);
	set_icon_visibility(pair->icon[2], 1);

    pair->icon[0]->modified = 1;
    pair->icon[1]->modified = 1;
    pair->icon[2]->modified = 1;

	//
	// Now determine what wand is active and do the text accordingly
	//

#if 0
uint32_t	            x=0, y=0;

    if(getenv("X1"))
        x = atoi(getenv("X1"));
    if(getenv("Y1"))
        y = atoi(getenv("Y1"));
#endif

	if(wand->primary_wand == WW_SMART_WAND_1 || wand->primary_wand == WW_SMART_WAND_2)
	{
    char        *ptr;
	uint32_t	therapy_index, smart_scale = 2;

		// smart wand

		// 
		// Ablate Icon
		// 

		therapy_index = (*ablate_coag_submode & ablate_shift_mask[ptherapy->therapy_mode]) >> ablate_shift_bits[ptherapy->therapy_mode];

        snprintf((char*) buffer, sizeof(buffer), "%s", therapy_modes[ptherapy->therapy_mode][therapy_index]);
        if((ptr=strstr((char*) buffer, ".")) != NULL)
        {
            *ptr = 0x1F;
        }

        set_icon_text_field(pair->icon[0], (uint8_t*) buffer);

		set_text_scale(pair->icon[0], smart_scale);

		picr = (iconCntrReg_t*) &pair->icon[0]->msg_status_update->icon_cntr_reg0;

		set_text_coords(pair->icon[0], (picr->fields.hposition + 35) * 2, (picr->fields.vposition + 10) * 2);

		// 
		// coblation icon
		// 

		// need the last 2 bits in this byte
		uint32_t	index = *ablate_coag_submode >> 6;
		set_icon_text_field(pair->icon[1], (uint8_t*) coag_set_point[index]);
		set_text_scale(pair->icon[1], 2);

		picr = (iconCntrReg_t*) &pair->icon[1]->msg_status_update->icon_cntr_reg0;

        snprintf((char*) buffer, sizeof(buffer), "%s", coag_set_point[index]);
        if((ptr=strstr((char*) buffer, ".")) != NULL)
        {
            *ptr = 0x1F;
            set_icon_text_field(pair->icon[1], (uint8_t*) buffer);
        }

		set_text_coords(pair->icon[1], (picr->fields.hposition + 52) * 2, (picr->fields.vposition + 10) * 2);

		// 
		// Now do the Temperature icon
		//

		uint8_t		temperature[9];

		temperature[8] = 0;  // set it to NULL;

		if(werewolf->amb_set & 0x2)
		    snprintf((char*) temperature, sizeof(temperature), "%d", (int) *temp);
        else
		    snprintf((char*) temperature, sizeof(temperature), "%s", "NA");

		// snprintf((char*) temperature, sizeof(temperature), "%d", (int) *temp);
		set_icon_text_field(pair->icon[2], (uint8_t*) temperature);
		set_text_scale(pair->icon[2], smart_scale);

		picr = (iconCntrReg_t*) &pair->icon[2]->msg_status_update->icon_cntr_reg0;
		set_text_coords(pair->icon[2], (picr->fields.hposition + 35) * 2, (picr->fields.vposition + 10) * 2);

		// Now lets see if any wand is active or not. If active, display the scroll bar icon,
		// otherwise disable the progress bar
		if(ptherapy->ablate_active)
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

        // Now lets see if any wand is active or not. If active, display the scroll bar icon
        if(ptherapy->coag_active)
            set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
        else
            set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

		set_rectangle_dimansions(pair->icon[0], OSD_WW_ICON_WIDTH, OSD_WW_ICON_HEIGHT);

		set_icon_visibility(pair->icon[0], 1);
		set_icon_visibility(pair->icon[1], 1);
		set_icon_visibility(pair->icon[2], 1);

		// Now set the text color from the BDM data.
		if(ambient_settings->color == AMBIENT_RED)
			set_text_color(pair->icon[2], etxtRed);
		else if(ambient_settings->color == AMBIENT_ORANGE)
			set_text_color(pair->icon[2], etxtDefaultOrange);
		else if(ambient_settings->color == AMBIENT_GREEN)
			set_text_color(pair->icon[2], etxtGreen);

		// Now lets see if any wand is active or not. If active, display the scroll bar icon,
		// otherwise disable the progress bar
		if(ptherapy->ablate_active)
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

		if(ptherapy->coag_active)
			set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);
	}
	else if(wand->primary_wand == 1 && wand->topaz_timer)
	{
	char		buffer[16];
	uint32_t	legacy_scale = 3;

		// legacy topaz wand
        uint32_t    x = 35;

		legacy_ablate_value = werewolf->ab_coag_18_sp & 0xf;
		snprintf(buffer, sizeof(buffer), "%d", legacy_ablate_value);
		set_icon_text_field(pair->icon[0], (uint8_t*) buffer);
		set_text_scale(pair->icon[0], legacy_scale);

		picr = (iconCntrReg_t*) &pair->icon[0]->msg_status_update->icon_cntr_reg0;
		set_text_coords(pair->icon[0], (picr->fields.hposition + x) * 2, (picr->fields.vposition) * 2);

		// 
		// Now do the coblation icon
		//

		legacy_ablate_value = werewolf->ab_coag_18_sp >> 4;
		set_icon_text_field(pair->icon[1], (uint8_t*) "500 MS");

		picr = (iconCntrReg_t*) &pair->icon[1]->msg_status_update->icon_cntr_reg0;
		set_text_coords(pair->icon[1], (picr->fields.hposition + x) * 2, (picr->fields.vposition+ 10) * 2);

		set_text_scale(pair->icon[0], legacy_scale);
		set_text_scale(pair->icon[1], 2);

		set_rectangle_dimansions(pair->icon[0], OSD_WW_ICON_WIDTH_TOPAZ, OSD_WW_ICON_HEIGHT_TOPAZ);

		// Now lets see if any wand is active or not. If active, display the scroll bar icon,
		// otherwise disable the progress bar
		if(ptherapy->ablate_active)
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

		if(ptherapy->coag_active)
			set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

		set_icon_visibility(pair->icon[0], 1);
		set_icon_visibility(pair->icon[1], 1);
		set_icon_visibility(pair->icon[2], 0);
	}
	else if(wand->primary_wand == 1 && wand->legacy_wand)
	{
    XY_coordinates_t    text_offsets[4] = { {50,10}, {50,10}, {50,8}, {35,10} }; 
	uint32_t	        legacy_scale = 2;
	char		        buffer[16];


#if 0
        // Use these env variables to test positioning aand text size
        // before committing.
        if(getenv("SCALE"))
            legacy_scale = atoi(getenv("SCALE"));

        if(getenv("XX"))
            text_offsets[legacy_scale].x = atoi(getenv("XX"));

        if(getenv("YY"))
            text_offsets[legacy_scale].y = atoi(getenv("YY"));
#endif

		// legacy wand
   		// u8 leg_amb_set;             // from HB
   		// u8 leg_amb_temp;            // from HB
		// ab_coag_18_sp

        // x = 35; y = 10;

		legacy_ablate_value = werewolf->ab_coag_18_sp & 0xf;
		snprintf(buffer, sizeof(buffer), "%d", legacy_ablate_value);
		set_icon_text_field(pair->icon[0], (uint8_t*) buffer);
		set_text_scale(pair->icon[0], legacy_scale);

		picr = (iconCntrReg_t*) &pair->icon[0]->msg_status_update->icon_cntr_reg0;
		set_text_coords(pair->icon[0], (picr->fields.hposition + text_offsets[legacy_scale].x) * 2, 
            (picr->fields.vposition + text_offsets[legacy_scale].y) * 2);
		// 
		// Now do the coblation icon
		//

		legacy_ablate_value = werewolf->ab_coag_18_sp >> 4;
		snprintf(buffer, sizeof(buffer), "%d", legacy_ablate_value);
		set_icon_text_field(pair->icon[1], (uint8_t*) buffer);

		picr = (iconCntrReg_t*) &pair->icon[1]->msg_status_update->icon_cntr_reg0;
		set_text_scale(pair->icon[1], legacy_scale);
		set_text_coords(pair->icon[1], (picr->fields.hposition + text_offsets[legacy_scale].x) * 2, 
            (picr->fields.vposition + text_offsets[legacy_scale].y) * 2);

		// 
		// Now do the Temperature icon
		//

		uint8_t		temperature[9];
		uint8_t		*temp = (uint8_t*) &werewolf->leg_amb_temp;
		temperature[8] = 0;  // set it to NULL;

		if(werewolf->amb_set & 0x2)
		    snprintf((char*) temperature, sizeof(temperature), "%d", (int) *temp);
        else
		    snprintf((char*) temperature, sizeof(temperature), "%s", "NA");

		set_icon_text_field(pair->icon[2], (uint8_t*) temperature);
		set_text_scale(pair->icon[2], legacy_scale);

		// Now lets see if any wand is active or not. If active, display the scroll bar icon,
		// otherwise disable the progress bar
		if(ptherapy->ablate_active)
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

		// Now lets see if any wand is active or not. If active, display the scroll bar icon
		if(ptherapy->coag_active)
			set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

		picr = (iconCntrReg_t*) &pair->icon[2]->msg_status_update->icon_cntr_reg0;
		set_text_coords(pair->icon[2], (picr->fields.hposition + text_offsets[legacy_scale].x) * 2, 
            (picr->fields.vposition + text_offsets[legacy_scale].y) * 2);
		// 

		set_rectangle_dimansions(pair->icon[0], OSD_WW_ICON_WIDTH - 25, OSD_WW_ICON_HEIGHT);

		set_icon_visibility(pair->icon[0], 1);
		set_icon_visibility(pair->icon[1], 1);
		set_icon_visibility(pair->icon[2], 1);

		// Now lets see if any wand is active or not. If active, display the scroll bar icon,
		// otherwise disable the progress bar
		if(ptherapy->ablate_active)
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[0], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

		if(ptherapy->coag_active)
			set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_ENABLE, PROGRESS_BAR_LOCATION_LEFT);
		else
			set_icon_progress_bar(pair->icon[1], PROGRESS_BAR_DISABLE, PROGRESS_BAR_LOCATION_LEFT);

		// Now set the text color from the BDM data.
		if(ambient_settings->color == AMBIENT_RED)
			set_text_color(pair->icon[2], etxtRed);
		else if(ambient_settings->color == AMBIENT_ORANGE)
			set_text_color(pair->icon[2], etxtDefaultOrange);
		else if(ambient_settings->color == AMBIENT_GREEN)
			set_text_color(pair->icon[2], etxtGreen);
	}
	else
	{
		set_icon_visibility(pair->icon[0], 0);
		set_icon_visibility(pair->icon[1], 0);
		set_icon_visibility(pair->icon[2], 0);
	}
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

char*
read_file(char *filename, int *nbytes)
{
int 			n, fd;
char			*buffer;
struct	stat	s;


	if((fd=open(filename, O_RDONLY)) == -1)
	{
		perror("open");
		printf("open failed on file: %s\n", filename);
		exit(1);
	}

	if(stat(filename, &s) < 0)
	{
		perror("stat");
		printf("could not stat file: %s\n", filename);
		exit(1);
	}

	buffer = calloc(1, s.st_size + 32);

	if(buffer == NULL)
	{
		perror("malloc");
		printf("could not allocate memory for file: %s\n", filename);
		exit(1);
	}

	if((n=read(fd, buffer, s.st_size)) < 0)
	{
		perror("read");
		printf("could not read file: %s\n", filename);
		exit(1);
	}

	close(fd);

	if(n != s.st_size)
	{
		printf("did not read all bytes from file: %d - %d\n", n, (int) s.st_size);
		exit(1);
	}

	*nbytes = n;
	return buffer;
}	

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
populate_D2_icon_data(icon_init_t *idata, int32_t fd, uint32_t x, uint32_t y, 
	uint32_t color1, uint32_t color2)
{
int		nbytes;


	if(y > 242) 
	{
		printf("ENTER: populate_D2_icon_data: resetting y coordinate from %d to 242\n", y);
		y = 242; // TODO: Get around a visualization bug
	}

	x = (float) x * OSD_ASPECT_RATIO_X;
	y = (float) y * OSD_ASPECT_RATIO_Y;

    idata->fd = fd;
    idata->x = x;
    idata->y = y;

    idata->rect_coords[0].x = idata->x;
    idata->rect_coords[0].y = idata->y;

    idata->rect_coords[1].x = idata->x;
    idata->rect_coords[1].y = idata->y;

    idata->width[0] = 145;
    idata->height[0] = 70;

    idata->width[1] = 0;
    idata->height[1] = 0;

    idata->bitmap_size = 128;

    idata->bitmap[0] = (uint8_t*) read_file("/sn/icons/shaver.bin", &nbytes);
    idata->nbytes[0] = nbytes;

    idata->bitmap[1] = (uint8_t*) read_file("/sn/icons/shaver.bin", &nbytes);
    idata->nbytes[1] = nbytes;

    idata->icon_offset[0].x = idata->width[0]/2 - idata->bitmap_size/8 - 0;
    idata->icon_offset[0].y = 0;

    idata->text_offset[0].x = 0 - 0;
    idata->text_offset[0].y = idata->bitmap_size/8 + 16;

    idata->text_offset[1].x = idata->icon_offset[0].x - 10;
    idata->text_offset[1].x = 45;
    idata->text_offset[1].y = idata->bitmap_size/8 + 42;

	idata->icon_color[0] = eOrangeDark;
	idata->icon_color[1] = eOrangeDark;

    idata->text_color[0] = color1;
    idata->text_color[1] = color2;

    idata->text_scale[0] = 2;
    idata->text_scale[1] = 1;

    idata->bar_visible[0] = 0;
    idata->bar_visible[1] = 0;

    idata->bar_location[0] = 0;
    idata->bar_location[1] = 0;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
populate_WW_icon_data(icon_init_t *idata, int32_t fd, uint32_t x, uint32_t y, 
	uint32_t color1, uint32_t color2, uint32_t color3)
{
int				nbytes;


	if(y > 237) 
	{
		printf("ENTER: populate_WW_icon_data: resetting y coordinate from %d to 242\n", y);
		y = 237; // TODO: Get around a visualization bug
	}

	x = (float) x * OSD_ASPECT_RATIO_X;
	y = (float) y * OSD_ASPECT_RATIO_Y;

    idata->fd = fd;
    idata->x = x;
    idata->y = y;

    idata->rect_coords[0].x = idata->x;
    idata->rect_coords[1].x = idata->x;
    idata->rect_coords[2].x = idata->x;

    idata->rect_coords[0].y = idata->y;
    idata->rect_coords[1].y = idata->y;
    idata->rect_coords[2].y = idata->y;

    idata->width[0] = OSD_WW_ICON_WIDTH;
    idata->height[0] = OSD_WW_ICON_HEIGHT;

    idata->width[1] = 0;
    idata->height[1] = 0;

    idata->width[2] = 0;
    idata->height[2] = 0;

    idata->bitmap_size = 128;

    idata->bitmap[0] = (uint8_t*) read_file("/sn/icons/coblation.bin", &nbytes);
    idata->nbytes[0] = nbytes;

    idata->bitmap[1] = (uint8_t*) read_file("/sn/icons/coag.bin", &nbytes);
    idata->nbytes[1] = nbytes;

	if(idata->bitmap[1] == NULL)
	{
		printf("Error reading icon files: %d\n", nbytes);
		exit(1);
	}

	if(getenv("OSD_COBLATION") != NULL)
    	idata->bitmap[2] = (uint8_t*) read_file(getenv("OSD_COBLATION"), &nbytes);
	else
    	idata->bitmap[2] = (uint8_t*) read_file("/sn/icons/teardrop.bin", &nbytes);

    idata->nbytes[2] = nbytes;

	if(idata->bitmap[2] == NULL)
	{
		printf("Error reading icon files: %d\n", (int) nbytes);
		exit(1);
	}

	idata->icon_color[0] = eYellow;
	idata->icon_color[1] = eBlue;
	idata->icon_color[2] = eDarkGrey;

    idata->icon_offset[0].x = 11;
    idata->icon_offset[0].y = 2;
    idata->icon_offset[1].x = 11;
    idata->icon_offset[1].y = idata->bitmap_size/8 + 15;
    idata->icon_offset[1].y = idata->bitmap_size/8 + 21;
    idata->icon_offset[2].x = 11;
    idata->icon_offset[2].y = 2* (idata->bitmap_size/8) + 35;
    idata->icon_offset[2].y = 2* (idata->bitmap_size/8) + 46;

    idata->text_offset[0].x = idata->bitmap_size/8 + 30;
    idata->text_offset[1].x = idata->bitmap_size/8 + 30;
    idata->text_offset[2].x = idata->bitmap_size/8 + 30;

    idata->text_offset[0].y = 10;
    idata->text_offset[1].y = 41;
    idata->text_offset[2].y = 85;

    idata->text_color[0] = color1;
    idata->text_color[1] = color2;
    idata->text_color[2] = color3;

    idata->text_scale[0] = 2;
    idata->text_scale[1] = 2;
    idata->text_scale[2] = 2;

    idata->bar_visible[0] = 0;
    idata->bar_visible[1] = 0;
    idata->bar_visible[2] = 0;

    idata->bar_location[0] = 0;
    idata->bar_location[1] = 0;
    idata->bar_location[2] = 0;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

CD_Icons*
create_D2_icon_group()
{
int32_t			fd, status, x, y;
icon_init_t		idata;
CD_Icons		*icon;


	icon = (CD_Icons*) malloc(sizeof(CD_Icons));

	if(icon == NULL)
		return NULL;

	strcpy(icon->id, "CD_Icons");

	fd = create_socket();
	if(fd < 0)
	{
		// TODO: log an error and exit
		printf("Could not connect to LENS 4K\n");
		return NULL;
	}

	status = get_osd_port_a_coordinates(&x, &y);
	if(status < 0)
	{
		printf("get_osd_port_a_coordinates returns %d\n", status);
		return NULL;
	}

	populate_D2_icon_data(&idata, fd, x, y, etxtDefaultOrange, etxtBlue);

    strcpy((char*) idata.text[0], "0000 RPM");
    strcpy((char*) idata.text[1], "PORT A");
    strcpy((char*) idata.name[0], "Shaver A");
    strcpy((char*) idata.name[1], "Shaver PortA");

	// create the icon pair
	icon->pgroup[0] = create_icon_group(2, &idata);
	if(icon->pgroup[0] == NULL)
	{
		printf("icon->pgroup[0] == NULL\n");
		return NULL;
	}

	icon->pgroup[0]->x = x;
	icon->pgroup[0]->y = y;

	icon->fd = fd;
	icon->nicons = 2;

	x = 1000000;

	status = get_osd_port_b_coordinates(&x, &y);
	if(status < 0)
	{
		printf("get_osd_port_a_coordinates returns %d\n", status);
		return NULL;
	}

	populate_D2_icon_data(&idata, fd, x, y, etxtDefaultOrange, etxtYellow);

	strcpy((char*) idata.text[0], "0000 RPM");
	strcpy((char*) idata.text[1], "PORT B");
	strcpy((char*) idata.name[0], "Shaver B");
	strcpy((char*) idata.name[1], "Shaver PortB");

	// create the icon pair
	icon->pgroup[1] = create_icon_group(2, &idata);
	if(icon->pgroup[1] == NULL)
	{
		printf("icon->pgroup[1] == NULL\n");
		return NULL;
	}

	icon->pgroup[1]->x = x;
	icon->pgroup[1]->y = y;

	icon->fd = fd;
	icon->nicons = 2;

	// Set the default colors for text and icons
	set_icon_color(icon->pgroup[0]->icon[0], eOrangeDark);
	// set_text_color(icon->pgroup[0]->icon[1], etxtDefaultOrange);
	set_icon_visibility(icon->pgroup[0]->icon[0], 0);
	set_icon_visibility(icon->pgroup[0]->icon[1], 0);
	set_icon_hide(icon->pgroup[0]->icon[0], 4);
	set_icon_hide(icon->pgroup[0]->icon[1], 4);


	// Set the default colors for text and icons
	set_icon_color(icon->pgroup[1]->icon[0], eOrangeDark);
	// set_text_color(icon->pgroup[1]->icon[1], etxtDefaultOrange);
	set_icon_visibility(icon->pgroup[1]->icon[0], 0);
	set_icon_visibility(icon->pgroup[1]->icon[1], 0);
	set_icon_hide(icon->pgroup[1]->icon[0], 4);
	set_icon_hide(icon->pgroup[1]->icon[1], 4);
	return icon;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

CD_Icons*
create_WW_icon_group()
{
int32_t			nicons = 3, fd, x, y, status;
icon_init_t		idata;
CD_Icons		*icon;


	icon = (CD_Icons*) malloc(sizeof(CD_Icons));

	if(icon == NULL)
		return NULL;

	fd = create_socket();
	if(fd < 0)
	{
		// TODO: log an error and exit
		printf("Could not connect to LENS 4K\n");
		return NULL;
	}

	status = get_osd_ww_icon_loc_coordinates(&x, &y);
	if(status < 0)
	{
		// log an error
		// return -1;
	}

	populate_WW_icon_data(&idata, fd, x, y, eWhite, eBlue, etxtGreen);

    strcpy((char*) idata.text[0], "XXX");
    strcpy((char*) idata.text[1], "YYY");
    strcpy((char*) idata.text[2], "ZZZ");

    strcpy((char*) idata.name[0], "WWW");
    strcpy((char*) idata.name[1], "QQQ");
    strcpy((char*) idata.name[2], "EEE");

	idata.icon_color[0] = eYellow;
	idata.icon_color[1] = eBlue;
	idata.icon_color[2] = eDarkGrey;

	// create the icon pair
	icon->pgroup[0] = create_icon_group(nicons, &idata);
	if(icon->pgroup[0] == NULL)
	{
		printf("icon[0] == NULL\n");
		return NULL;
	}

	icon->pgroup[0]->x = x;
	icon->pgroup[0]->y = y;

	icon->nicons = 1;
	icon->fd = fd;

	set_icon_visibility(icon->pgroup[0]->icon[0], 0);
	set_icon_visibility(icon->pgroup[0]->icon[1], 0);
	set_icon_visibility(icon->pgroup[0]->icon[2], 0);

	set_icon_hide(icon->pgroup[0]->icon[0], 4);
	set_icon_hide(icon->pgroup[0]->icon[1], 4);
	set_icon_hide(icon->pgroup[0]->icon[2], 4);

	return icon;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

int
save_icon(char *data, int n, char *filename)
{
int 			nbytes, fd;
// char			*penv, *buffer;
struct	stat	s;


	if((fd=open(filename, O_WRONLY|O_CREAT, 0755)) == -1)
	{
		perror("open");
		printf("open failed on file: %s\n", filename);
		exit(1);
	}

	if((nbytes=write(fd, data, n)) < 0)
	{
		perror("write");
		printf("could not write to file: %s\n", filename);
		exit(1);
	}

	close(fd);

	if(stat(filename, &s) < 0)
	{
		perror("stat");
		printf("could not stat file: %s\n", filename);
		exit(1);
	}

	if(nbytes != s.st_size)
	{
		printf("did not save all of file: %s --- %d - %d\n", filename, nbytes, (int) s.st_size);
		exit(1);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

int
db_read_XY_coords(OSD_coords_t *coords)
{
int		status, x, y;

	if(access("/etc/D2", F_OK) == 0)
	{
		status = get_osd_port_a_coordinates(&x, &y);
		if(status < 0)
			return status;

		coords->portA_x = x;
		coords->portA_y = y;

		status = get_osd_port_b_coordinates(&x, &y);
		if(status < 0)
			return status;

		coords->portB_x = x;
		coords->portB_y = y;
	}
	if(access("/etc/WW", F_OK) == 0)
	{
		status = get_osd_ww_icon_loc_coordinates(&x, &y);
		if(status < 0)
			return status;

		coords->portA_x = x;
		coords->portA_y = y;
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
compute_elapsedtime(struct timeval *start, uint32_t *msecs)
{
struct  timeval         end;
int                     secs;


    gettimeofday(&end, NULL);

    secs = end.tv_sec - start->tv_sec;

    if(end.tv_usec < start->tv_usec)
    {
        secs--;
        *msecs = (secs * MSECS_PER_SEC) + (USECS_PER_SEC - start->tv_usec + end.tv_usec) / MSECS_PER_SEC;
    }
    else
        *msecs = (secs * MSECS_PER_SEC) + (end.tv_usec - start->tv_usec) / MSECS_PER_SEC;

    // printf("compute_elapsedtime: %d:%d -- %d:%d  === %d\n", start->tv_sec, start->tv_usec,
        // end.tv_sec, end.tv_usec, *msecs);
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

void
reset_coordinates(OSD_coords_t *osd_coords, min_max_coords_t *coord_limits)
{
    if(osd_coords->portA_x < coord_limits->min.x) osd_coords->portA_x = coord_limits->min.x;
    if(osd_coords->portA_x > coord_limits->max.x) osd_coords->portA_x = coord_limits->max.x;

    if(osd_coords->portA_y < coord_limits->min.y) osd_coords->portA_y = coord_limits->min.y;
    if(osd_coords->portA_y > coord_limits->max.y) osd_coords->portA_y = coord_limits->max.y;

    // The following is necessary for the D2.
    // If we're a WW link, the following isn't necessary as there is only 1 icon.
    // But it's probably faster just do this rather than query the file system
    // for the existence of /etc/WW
    if(osd_coords->portB_x < coord_limits->min.x) osd_coords->portB_x = coord_limits->min.x;
    if(osd_coords->portB_x > coord_limits->max.x) osd_coords->portB_x = coord_limits->max.x;

    if(osd_coords->portB_y < coord_limits->min.y) osd_coords->portB_y = coord_limits->min.y;
    if(osd_coords->portB_y > coord_limits->max.y) osd_coords->portB_y = coord_limits->max.y;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

int 
main(int argc, char *argv[])
{
uint32_t                    poll_msecs=200;
uint8_t						timeout = 0, capital_device=0;
uint32_t                    got_message = 0;
uint8_t                     filter[BROKER_FILTER_MAX+1];
int32_t						nbytes, status=0, rc;
CD_Icons					*icon=NULL;
void						*handle=NULL, *pubhandle=NULL;
icon_init_t					idata[2];
icon_group_t				*group;
scd_bdm_port_status_t		*d2_status=NULL;
ctn_ww_get_response_data_t	*werewolf_status=NULL;
min_max_coords_t            coord_limits;
OSD_coords_t	            coords;


#if defined(OSD_COMPUTE_TIMING_INFORMATION)
struct	timeval				start, end;
uint32_t                    msecs=0;
#endif

   if (argc > 1 && !strcmp(argv[1],"-V")) {
       printf("%s\n", version);
       return 0;
   }

    printf("OSD Manager Protocol Client. Last modified at %s %s\n", __DATE__, __TIME__);

	pubhandle = bm_publisher();
	handle = bm_subscriber();

    rc = parse_cmd_line(argc, argv);
    if (rc != 0) {
		// TODO: log an error and exit
        return 0;
    }

   	strcpy((char*) idata[0].text[0], "0000 RPM");
   	strcpy((char*) idata[0].text[1], "PORT A");
   	strcpy((char*) idata[0].name[0], "Shaver A");
   	strcpy((char*) idata[0].name[1], "Shaver PortA");

   	strcpy((char*) idata[1].text[0], "0000 RPM");
   	strcpy((char*) idata[1].text[1], "PORT B");
   	strcpy((char*) idata[1].name[0], "Shaver B");
   	strcpy((char*) idata[1].name[1], "Shaver PortB");

	//
	// Are we talking to a D2 or a Werewolf ?
	//

	if((access(CONNECED_TOWER_CAPITAL_DEVICE_D2, F_OK) == -1) && (access(CONNECED_TOWER_CAPITAL_DEVICE_WW, F_OK) == -1))
	{
		printf("No Capital Device ID File in /etc\n");
		exit(1);
	}

	if(access(CONNECED_TOWER_CAPITAL_DEVICE_D2, F_OK) == 0)
	{
		capital_device = OSD_CAPITAL_DEVICE_D2;
		icon = create_D2_icon_group();

        coord_limits.min.x = D2_MIN_X;
        coord_limits.min.y = D2_MIN_Y;
        coord_limits.max.x = D2_MAX_X;
        coord_limits.max.y = D2_MAX_Y;
	}
	else if(access(CONNECED_TOWER_CAPITAL_DEVICE_WW, F_OK) == 0)
	{
		capital_device = OSD_CAPITAL_DEVICE_WEREWOLF;
		icon = create_WW_icon_group();

        coord_limits.min.x = WW_MIN_X;
        coord_limits.min.y = WW_MIN_Y;
        coord_limits.max.x = WW_MAX_X;
        coord_limits.max.y = WW_MAX_Y;
	}

	// From this point forward, the icons are assumed to be created and ready to roll

	if(icon == NULL)
	{
		printf("Could not create icon(s) for the Capital Device\n");
		return 0;
	}

#if defined(OSD_DEBUG)
	printf("main: done with icon intiialization\n");
	dump_icon(icon->pgroup[0]->icon[0]->msg_status_update);
	dump_icon(icon->pgroup[0]->icon[1]->msg_status_update);
#endif

	// Lets subcsribe to some messages

	if(capital_device == OSD_CAPITAL_DEVICE_D2)
		bm_subscribe(handle, D2_STATUS, strlen(D2_STATUS));
	else
		bm_subscribe(handle, WW_PORT_STATUS, strlen(WW_PORT_STATUS));

	bm_subscribe(handle, BROKER_BDM_COORDS, strlen(BROKER_BDM_COORDS));
	bm_subscribe(handle, BROKER_DSC_WIFI_STATE, strlen(BROKER_DSC_WIFI_STATE));

	if(bm_subscribe(handle, BROKER_DATABASE_MODIFIED, strlen(BROKER_DATABASE_MODIFIED)) < 0)
	{
		printf("bm_subscribe BROKER_DATABASE_MODIFIED erorr\n");
		exit(1);
	}

	//
	// The OSD is also a publisher of the coordinates
	// publish a few PING messages first, then publish the icon X,Y coordinates 
	// that were read out of the database
	//

	bm_send(pubhandle, (uint8_t*) BROKER_PING, "", 0);
	bm_send(pubhandle, (uint8_t*) BROKER_PING, "", 0);

	coords.portA_x = icon->pgroup[0]->x;
	coords.portA_y = icon->pgroup[0]->y;

	if(capital_device == OSD_CAPITAL_DEVICE_D2)
	{
		coords.portB_x = icon->pgroup[1]->x;
		coords.portB_y = icon->pgroup[1]->y;
	}

	printf("OSD_COORDS: (%d,%d), (%d,%d)\n", coords.portA_x,coords.portA_y,coords.portB_x,coords.portB_y);

	bm_send(pubhandle, (uint8_t*) BROKER_OSD_COORDS, &coords, sizeof(coords));

	while(1)
	{
	void		*broker_message;

		timeout = got_message = 0;

        //
        // See if there's a message waiting to be read
        //

        status = bm_poll(handle, poll_msecs);

        if(status > 0)   // NOTE: This if statement controls the bm_receive below
        {
            status = bm_receive(handle, filter, (void*) &broker_message, 
                (uint32_t*) &nbytes, BROKER_NONBLOCKING);

            if(status > 0)
                got_message = 1;
        }

		if(got_message)
		{
		OSD_coords_t	*osd_coords;

			if(!strncmp((char*) filter, WW_PORT_STATUS, strlen(WW_PORT_STATUS)))
			{
				werewolf_status = (ctn_ww_get_response_data_t*) broker_message;
			}
			else if(!strncmp((char*) filter, BROKER_DSC_WIFI_STATE, strlen(BROKER_DSC_WIFI_STATE)))
			{
            char    *value = (char*) broker_message;

                if(*value == 0)
                    exit(0);
            }
			else if(!strncmp((char*) filter, D2_STATUS, strlen(D2_STATUS)))
			{
				d2_status = (scd_bdm_port_status_t*) broker_message;
			}
			else if(!strncmp((char*) filter, BROKER_BDM_COORDS, strlen(BROKER_BDM_COORDS)) ||
				!strncmp((char*) filter, BROKER_DATABASE_MODIFIED, strlen(BROKER_DATABASE_MODIFIED)))
			{
			uint32_t    i, j;


				if(!strncmp((char*) filter, BROKER_DATABASE_MODIFIED, strlen(BROKER_DATABASE_MODIFIED)))
				{
					memset(&coords, 0, sizeof(coords));
					db_read_XY_coords(&coords);
					osd_coords = &coords;

					bm_send(pubhandle, (uint8_t*) BROKER_OSD_COORDS, osd_coords, sizeof(coords));
				}
				else 
				{
					osd_coords = broker_message;
                }

                reset_coordinates(osd_coords, &coord_limits);

				dump_icon_on_bdm_message = 1;

				if(d2_status || werewolf_status)
				{
#if defined(OSD_COMPUTE_TIMING_INFORMATION)
		gettimeofday(&start, NULL);
#endif
					for(i=0; i<icon->nicons; i++)
					{
						group = icon->pgroup[i];

						for(j=0; j<group->nicons; j++)
						{
							if(i == 0)
							{
								idata[i].x = osd_coords->portA_x;
								idata[i].y = osd_coords->portA_y;

								if(capital_device == OSD_CAPITAL_DEVICE_D2)
									populate_D2_icon_data(&idata[i], icon->fd, idata[i].x + 0, 
										idata[i].y + 0, D2_port_A_text_colors[j].text_color_1, 
										D2_port_A_text_colors[j].text_color_2);
								else if(capital_device == OSD_CAPITAL_DEVICE_WEREWOLF)
								{
									populate_WW_icon_data(&idata[i], icon->fd, idata[i].x + 0, 
										idata[i].y + 0, WW_text_colors[j].text_color_1, 
										WW_text_colors[j].text_color_2,
										WW_text_colors[j].text_color_3);
								}
							}
							else if(i == 1)
							{
								idata[i].x = osd_coords->portB_x;
								idata[i].y = osd_coords->portB_y;

								populate_D2_icon_data(&idata[i], icon->fd, idata[i].x + 0, 
									idata[i].y + 0, D2_port_B_text_colors[j].text_color_1, 
										D2_port_B_text_colors[j].text_color_2);
							}

							setup_basic_icon_data(group->icon[j], &idata[i], j);

							group->x = idata[i].x;
							group->y = idata[i].y;
						}
					}	
#if defined(OSD_COMPUTE_TIMING_INFORMATION)
		        compute_elapsedtime(&start, &msecs);
		        printf("Back from Populate Icons: Elapsed Time: %d, # of icons updated: %d\n", 
                    msecs, 0);
#endif
				}
			}

			if(capital_device == OSD_CAPITAL_DEVICE_D2)
			{
				// now compare our data for each of the 2 D2 icons with what we got fro mthe BDM 
				// in a prevoius message. If nothg has changed for an icon, no need to update it,
				// unless it's about to expire.

#if defined(OSD_COMPUTE_TIMING_INFORMATION)
		        gettimeofday(&start, NULL);
#endif
				set_iconpair_from_bdm_data(icon->pgroup[0], 
					(port_display_t*) &d2_status->porta_display, 
					(port_speed_run_t*) &d2_status->porta_speed_run);

				set_iconpair_from_bdm_data(icon->pgroup[1], 
					(port_display_t*) &d2_status->portb_display, 
					(port_speed_run_t*) &d2_status->portb_speed_run);

#if defined(OSD_COMPUTE_TIMING_INFORMATION)
		        compute_elapsedtime(&start, &msecs);
		        printf("Back from set_iconpair_from_bdm_data: Elapsed Time: %d, # of icons updated: %d\n", 
                    msecs, 0);
#endif
			}
			else if(capital_device == OSD_CAPITAL_DEVICE_WEREWOLF)
			{
#if defined(OSD_COMPUTE_TIMING_INFORMATION)
		        gettimeofday(&start, NULL);
#endif

				set_WW_iconpair_from_bdm_data(icon->pgroup[0], werewolf_status);

#if defined(OSD_COMPUTE_TIMING_INFORMATION)
		        compute_elapsedtime(&start, &msecs);
		        printf("Back from set_WW_iconpair_from_bdm_data: Elapsed Time: %d, # of icons updated: %d\n", 
                    msecs, 0);
#endif
			}
		}

        status = bm_poll(handle, 0);
        if(status > 0)
            continue;

#if defined(OSD_COMPUTE_TIMING_INFORMATION)
		gettimeofday(&start, NULL);
#endif

        status = update_icons(icon, timeout);
        if(status < 0)
        {
            // log an error
            return status;
        }

#if defined(OSD_COMPUTE_TIMING_INFORMATION)
		compute_elapsedtime(&start, &msecs);
		printf("Update Icon(s) Elapsed Time: %d, # of icons updated: %d\n", msecs, status);
#endif
	}
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
// NOTES:   YYY
//

int
update_icons(CD_Icons *icon, UNUSED_ATTRIBUTE uint32_t timeout)
{
icon_group_t	        *group;
uint32_t                i, j, msecs;
int32_t                 status=0, count=0;


    //
    // If we timed out that means that no icons changed, so lets pick the oldest 
    // one to have been updated and see if we want o to update it now.
    //

    // An icon was updated.
    for(i=0; i<icon->nicons; i++)
	{
        group = icon->pgroup[i];

        // in this for loop, we only update icons that were modified
        for(j=0; j<group->nicons; j++)
        {
            // If an icon was modified, no need to check the timing. then 
            // no nede to check the timing. Just update it
            if(icon->pgroup[i]->icon[j]->modified == 0)
                continue;

            // This icon was modified. Or needs to be updated becasue it the last
            // time it was upated was over 2 seconds ago. So lets update it
            count++;
            status = update_icon(icon->fd, icon->pgroup[i]->icon[j]);

            if(status < 0)
            {
                // TODO: log an error and exit
                return status;
            }
        }

        // in this for loop, we update icons that are in danger of timing out.
        for(j=0; j<group->nicons; j++)
        {
            // If an icon was modified, no need to check the timing. then 
            // no nede to check the timing. Just update it
            if(icon->pgroup[i]->icon[j]->modified == 0)
            {
                // This icon was not modified. But lets check how many milli-seconds
                // elapsed since it was last updated 
                compute_elapsedtime(&icon->pgroup[i]->icon[j]->last_update, &msecs);
#if 0
                if(msecs > oldest_msecs)
                {
                    oldest_icon = icon->pgroup[i]->icon[j];
                    oldest_msecs = msecs;
                }
#endif

                // If this icon hasen't been updated in over 2 seconds, then
                // then lets update it now
                if(msecs < 2000)
                    continue;
            }

            // This icon was modified. Or needs to be updated becasue it the last
            // time it was upated was over 2 seconds ago. So lets update it
            count++;
            status = update_icon(icon->fd, icon->pgroup[i]->icon[j]);

            if(status < 0)
            {
                // TODO: log an error and exit
                return status;
            }
        }
    }

    return count;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

static void usage()
{
    printf("Usage:\n");
    printf("./osd_client [-hV] [-d device_id] [-n icon_cnt] --horizontal --tv --tc {color} --lic {color} --ric {color} <server_ip>\n");
    printf("\n");
    printf("-V display version information\n");
    printf("id: Device ID. 0 to 3 for D2, D25, LENS, Werewolf\n");
    printf("icon_cnt: number of icons per device\n");
	printf("--horizontal: layout of the icons (default is vertical\n");
	printf("--vertical: layout of the icons (default is vertical\n");
	printf("--tc : text color index\n");
	printf("--tv : text orientation vertical (default is horizontal)\n");
	printf("--lih : left icon hide\n");
	printf("--rih : right icon hide\n");
	printf("--liv : left icon color index\n");
	printf("--ric : right icon color index\n");
	printf("--bb: rectangle per icon\n");
	printf("--ts {n} : text scale\n");
	printf("--cb {n} : character spacing\n");
	printf("--ra {n} : rectangle alpha value\n");
	printf("--bl {n} : icon bar location value\n");
	printf("--bp {n} : icon bar percent full value\n");
	printf("--bc {n} : icon bar color \n");
	printf("--bv {n} : icon bar visible \n");
	printf("-x {x value}\n");
	printf("-y {y value}\n");
	printf("--64 {bitmaps are 64x64}\n");
	printf("--128 {bitmaps are 128x128}\n");
    printf("\n");
    printf("Example:\n");
    printf("./osd_client 192.168.1.1\n");
    printf("./osd_client -d 2 -n 4 192.168.1.1\n");
    printf("./osd_client -h\n");
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

// return non-zero to indicate error or exit condition
static int parse_cmd_line(int argc, char *argv[]) {
    opterr = 0;
    int rc = 0, optindex=0;
    int c;
	struct option	osdopts[] = 
	{
		{"hide", 0, 0, 1},
		{"vertical", 0, 0, 2},
		{"horizontal", 0, 0, 3},
		{"tv", 0, 0, 4},
		{"tc", 1, 0, 5},
		{"lic", 1, 0, 6},
		{"ric", 1, 0, 7},
		{"bb", 0, 0, 8},
		{"cs", 1, 0, 9},
		{"ts", 1, 0, 10},
		{"ra", 1, 0, 11},
		{"bl", 1, 0, 12},
		{"bp", 1, 0, 13},
		{"bc", 1, 0, 14},
		{"bv", 0, 0, 15},
		{"lih", 0, 0, 16},
		{"rih", 0, 0, 17},
		{"x1", 1, 0, 18},
		{"y1", 1, 0, 19},
		{"x2", 1, 0, 20},
		{"y2", 1, 0, 21},
		{"64", 0, 0, 64},
		{"128", 0, 0, 128},
		{0, 0, NULL, 0}
	};

    while ((c = getopt_long(argc, argv, "vthd:n:", osdopts, &optindex)) != -1) {
        switch (c) {
            case 'v':
                cmd_verbose = true;
				printf("XXXXXXXXXXXXXXXXX cmd_verbose = %d\n", cmd_verbose);
                break;
            case 'h':
                usage();
                rc = 1;
                break;
            case 'd':
                cmd_device_id = atol(optarg);
                if (cmd_device_id > DEVICE_ID_MAX) {
                    printf("Error: max icon ID is %d. Got %d\n",
                            DEVICE_ID_MAX, cmd_device_id);
                    exit(-1);
                }
                break;
            case 'n':
                cmd_icon_cnt = atol(optarg);
                if (cmd_icon_cnt > ICON_ID_MAX + 1) {
                    printf("Error: max icon number is %d. Got %d\n",
                            ICON_ID_MAX + 1, cmd_icon_cnt);
                    exit(-1);
                }
                break;
            case 1:
				icon_control_register.fields.lefthide = 1;
				break;
            case 2:
				icon_orientation = ICON_ORIENTATION_VERTICAL;
				break;
            case 3:
				icon_orientation = ICON_ORIENTATION_HORIZONTAL;
				break;
            case 4:
				text_orientation = TEXT_ORIENTATION_VERTICAL;
				break;
            case 5:
				text_control_register.fields.colorIndx = atoi(optarg);
				break;
            case 6:
				icon_control_register.fields.coloroverideval_left = atoi(optarg);
				break;
            case 7:
				icon_control_register.fields.coloroverideval_right = atoi(optarg);
				break;
            case 8:
				bounding_box_per_icon = 1;
				break;
            case 9:
				text_control_register2.fields.charspacing = atoi(optarg);
				break;
            case 10:
				text_control_register2.fields.scale = atoi(optarg);
				break;
            case 11:
				alpha_control_register.fields.alpha = atoi(optarg);
				break;
            case 12:
				bar_control_register.fields.location = atoi(optarg);
				break;
            case 13:
				bar_control_register.fields.percFull = atoi(optarg);
				break;
            case 14:
				bar_control_register.fields.color = atoi(optarg);
				break;
            case 15:
				bar_control_register.fields.visible = 1;
				break;
            case 16:
				icon_control_register.fields.lefthide = 1;
				break;
            case 17:
				icon_control_register.fields.righthide = 1;
				break;
            case 't':
				icon_control_register.fields.toggle = 1;
				break;
            case 'x':
				alpha_control_register.fields.hpos = atoi(optarg);
				break;
            case 'y':
				alpha_control_register.fields.vpos = atoi(optarg);
				break;
			case 18:
				coords[0].x = atoi(optarg);
				break;
			case 19:
				coords[0].y = atoi(optarg);
				break;
			case 20:
				coords[1].x = atoi(optarg);
				break;
			case 21:
				coords[1].y = atoi(optarg);
				break;
            case 64:
				bitmap_size = 64;
				break;
            case 128:
				bitmap_size = 128;
				break;
            default:
                printf("************************ Unknown option: -%c\n", c);
                rc = 1;
				usage();
                break;
        }
    }

    if (rc == 0) {
        if (optind < argc) {
            cmd_server_ip = argv[optind];
        } else {
            printf("Server IP not provided.\n");
            rc = 1;
        }
    }

    if (rc == 0) {
        g_device_type = g_device_type_list[cmd_device_id];
    }

    return rc;
}

static void change_state(OsdClientConnection* con, int state)
{
    con->state = state;
    if(con->state == STATE_CONNECTED) {
        printf("state: Connected\n");
    } else {
        printf("state: Disconnected\n");
    }
}

///////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//

static int wait_for_tcp_packet_with_timeout(int sockfd, uint8_t* buffer,
        uint32_t buf_len, uint32_t timeout_ms)
{
    const int RT_TIMEOUT = 1;

    int n;
    int sleep_cnt = timeout_ms/SLEEP_MS;
    do {
        n = recv(sockfd, buffer, buf_len, MSG_DONTWAIT);
        if (n < 0) {
            if (errno == EWOULDBLOCK || errno == EAGAIN) {
                // no data for now. will try again
#if 0
                if(sleep_cnt != 0 && cmd_verbose) {
                    printf(C_GRAY"sleep_cnt:%d\n"C_END, sleep_cnt);
                }
#endif
            } else {
                printf("%d: recv(): ERROR reading from socket: %s\n", 
					(unsigned int) time(NULL), strerror(errno));
                return -1;
            }
        } else if( n == 0) { // connection closed
            printf("Client closed connection: sleep_cnt = %d\n", sleep_cnt);
            return -1;
        } else if( n == (int)buf_len) {
            printf("package size exceeding limit");
            return -1;
        } else {
            IFullMsg* msg = (IFullMsg*) buffer;
            if(!verify_msg_format(msg)) {
                printf("Server msg format error");
                return -1;
            }
            if (cmd_verbose) print_ifull_msg(msg);
            if (msg->protocol_id != OSD_PROTO_ID_S2C) {
                if (cmd_verbose) printf("Server Response: Unknown protocol : %02X.\n", msg->protocol_id);
                return -1;
            }
            break;  // message received
            if (msg->command == OSD_CMD_SERVER_REGISTRATION_RESPONSE) {
                // pass
            } else if (msg->command == OSD_CMD_SERVER_POPULATE_ICON_RESPONSE) {
                // pass
            } else if (msg->command == OSD_CMD_SERVER_STATUS_UPDATE_RESPONSE) {
                if (cmd_verbose) printf("Server Status Response received: %02X.\n", msg->command);
                OsdMsg_StatusUpdateResp* resp = (OsdMsg_StatusUpdateResp*) msg;
                if(resp->error != 0) {
                    printf("Received Status Update NAK (error: %d) "
                            "from RTOS.\n", resp->error);
                    return -1;
                }
            } else {
                if (cmd_verbose) printf("Server Response: Unknown command : %02X.\n", msg->command);
                return -1;
            }
        }

        sleep_cnt -= 1;
        if (sleep_cnt > 0) 
		{
			// printf("###### USLEEP(%d)\n", SLEEP_MS * 1000);
			usleep(SLEEP_MS * 1000); 
		}
    } while(sleep_cnt > 0);

    if (sleep_cnt == 0 && cmd_verbose) {
        printf(C_RED"recv() timeout\n"C_END);
        return RT_TIMEOUT;
    } else if (sleep_cnt == -1) {
        if(cmd_verbose) printf(C_GRAY"Non-blocking recv() returns empty\n"C_END);
        return RT_TIMEOUT;
    }
    return 0;
}


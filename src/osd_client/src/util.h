#ifndef UTIL_H
#define UTIL_H

#include <assert.h>
#include <stdlib.h>

#define RED "\033[0;31m"
#define GREEN "\033[0;32m"
#define YELLOW "\033[0;33m"
#define GRAY "\033[1;30m"
#define RESET "\033[0m"

#define assert_msg(msg, args...) assert(msg, ##args)

#define assert_equal_ptr(expected, actual) do { \
    char* p1 = (char*)(expected); \
    char* p2 = (char*)(actual); \
    if (p1 != p2) { \
        printf("%sAssertion failed [%s:%d]\nexpected: %s:0x%p, actual: %s:0x%p%s\n", \
                RED, __FILE__, __LINE__, \
                #expected, p1, #actual, p2, RESET); \
        abort(); \
    }}while (0)

uint32_t u32_from_4u8(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3);

#endif

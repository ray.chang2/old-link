#include <inttypes.h>                                                            
#include <stdio.h>                                                            
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <malloc.h>

#include "osd.h"

#if 0
// Must map to IFULL_DEVICE_TYPE
const char* DEVICE_TYPE_NAMES[] = {
    "_RESERVED",
    "D2 / Shaver",
    "LENS / Video",
    "Werewolf / Coblation",
    "Tablet",
    "D25 / Fluid Management",
    "_MAX"
};
#endif

const char* OSD_CLI_MSG_NAMES[] = {
        "Registration Request",             // 0xF0
        "Populate ICON",                    // 0xF1
        "Status Update",                    // 0xF2
};

const char* OSD_SVR_MSG_NAMES[] = {
    "Registration Response",            // 0xF8
    "Status Update Response/NAK",           // 0xF9
    "UKN 0xFA",
    "UKN 0xFB",
    "Populate ICON Response",           // 0xFC
};

/**
 * Calculate CRC for `len` of data in `buf`
 *
 */
uint8_t calc_crc(const uint8_t* buf, uint32_t len)
{
    int sum = 0;
    uint32_t i;
    for(i=0; i<len; i++) {
        sum += buf[i];
    }
    uint8_t checksum = (uint8_t) (~(sum % 256) + 1);
    return checksum;
}

static bool verify_crc(uint8_t* buf, uint32_t len)
{
    uint8_t n = calc_crc(buf, len -1);
    return n == buf[len -1];
}

bool verify_msg_format(const IFullMsg* msg)
{
    uint8_t* buf = (uint8_t*) msg;
    uint32_t data_len = ntohl(msg->data_len_nw);
    uint32_t len = 8 + data_len + 1;
    if (len>=MAX_PACKET_SIZE) {
        printf("Message len error: %d\n", len);
        print_ifull_msg_short(msg);
        return false;
    }
    if(!verify_crc(buf, len)) {
        printf("Message crc error\n");
        print_ifull_msg(msg);
        return false;
    }

    return true;
}

static void print_ifull_msg_header(const IFullMsg* msg)
{
    uint32_t data_len = ntohl(msg->data_len_nw);

    if (msg->protocol_id == OSD_PROTO_ID_C2S) {
        printf("Ext -> RTOS: %s: ", OSD_CLI_MSG_NAMES[msg->command - OSD_CLI_MSG_ID_START]);
    } else if (msg->protocol_id == OSD_PROTO_ID_S2C) {
        printf("RTOS -> Ext: %s: ", OSD_SVR_MSG_NAMES[msg->command - OSD_SVR_MSG_ID_START]);
    } else {
        printf("Unknown Protocol: %02X\n", msg->protocol_id);
    }
    printf("%02X %02X * * %d - ",
           msg->protocol_id, msg->command, data_len);
}

void print_ifull_msg_safe(const IFullMsg* msg, uint32_t len_limit)
{
    uint32_t i;
    uint32_t data_len = ntohl(msg->data_len_nw);

    print_ifull_msg_header(msg);
    if (len_limit != 0 && data_len > len_limit) {
        data_len =  len_limit;
    }
    printf("[");
    for (i=0; i<data_len; i++) {
        printf("%02X ", msg->data[i]);
    }
    printf("]\n");
}

void print_ifull_msg_short(const IFullMsg* msg)
{
    print_ifull_msg_safe(msg, 20);
}

void print_ifull_msg(const IFullMsg* msg)
{
    uint32_t i;
    uint32_t data_len = ntohl(msg->data_len_nw);

    print_ifull_msg_header(msg);
    printf("[");
    for (i=0; i<data_len; i++) {
        printf("%02X ", msg->data[i]);
    }
    printf("] - %02X\n", msg->data[data_len]);  // CRC
}

uint32_t ip_str_to_int(const char* s)
{
    struct in_addr ip;

    inet_pton(AF_INET, s, &ip);
    return ip.s_addr;
}

/**
 * Give data payload length, return full msg length
 */
uint32_t full_msg_len(uint32_t data_len)
{
    // assumption:
    // * The message is packed
    // * data_len % 4 == 0
    return (uint32_t)(IFULLMSG_HEADER_SIZE + data_len + 1); // 1 for CRC;
}

int get_msg_len(const IFullMsg* msg)
{
    uint32_t data_len = ntohl(msg->data_len_nw);
    return full_msg_len(data_len);
}

/******************************************************************************
 *                   OSD Messages                                             *
 ******************************************************************************/
OsdMsg_RegRqst* create_reg_request_msg(uint8_t netid, uint8_t device_type, const char* icon_name)
{
    uint32_t icon_name_len;
    uint32_t body_len;
    OsdMsg_RegRqst* msg;

printf("Sizeof:  OsdMsg = %d,  OsdMsg_RegRqst = %d\n", 
	(int) sizeof(OsdMsg), (int) sizeof(OsdMsg_RegRqst));

    msg = (OsdMsg_RegRqst*)malloc(sizeof(OsdMsg));
    if (!msg) return NULL;
    bzero(msg, sizeof(OsdMsg));

    msg->netid = netid;
    msg->device_type = device_type;

    // ensure the last byte is 0
    icon_name_len = strlen(icon_name);
    if (icon_name_len >= OSDMGR_ICONSTRSIZEBYTES -1) {
        icon_name_len = OSDMGR_ICONSTRSIZEBYTES;
    }
    strncpy((char*)msg->icon_name, icon_name, (size_t)icon_name_len);

    msg->header.protocol_id = OSD_PROTO_ID_C2S;
    msg->header.command = OSD_CMD_CLIENT_REGISTRATION_REQUEST;
    body_len = 2 + OSDMGR_ICONSTRSIZEBYTES;
    msg->header.data_len_nw = htonl(body_len);

    msg->crc = calc_crc((uint8_t*)msg, sizeof(IFullMsgHeader) + body_len);

    return msg;
}

OsdMsg_PopulateIcon* create_pop_icon_msg(
        uint8_t netid,
        uint8_t uid,
        const uint8_t* icon, uint32_t icon_len)
{
    uint8_t* crc_ptr;
    uint32_t body_len;
    OsdMsg_PopulateIcon* msg;
    
    msg = (OsdMsg_PopulateIcon*)malloc(sizeof(OsdMsg)); // force msg to be an instance of OsdMsg
    if (!msg) return NULL;
    bzero(msg, sizeof(OsdMsg));
    msg->netid = netid;
    msg->uid = uid;

    if (icon_len >= MAX_ICON_LEN) {
        icon_len = MAX_ICON_LEN;
    }
    memcpy(msg->icon, icon, (size_t)icon_len);

    msg->header.protocol_id = OSD_PROTO_ID_C2S;
    msg->header.command = OSD_CMD_CLIENT_POPULATE_ICON;
    body_len = 1 /*netid*/ + 1 /*uid*/ + icon_len;
    msg->header.data_len_nw = htonl(body_len);

    crc_ptr = (uint8_t*)msg + sizeof(IFullMsgHeader) + body_len;
    *crc_ptr = calc_crc((uint8_t*)msg, sizeof(IFullMsgHeader) + body_len);

    return msg;
}

OsdMsg_StatusUpdate* create_status_update_msg(uint8_t netid, uint8_t uid)
{
    OsdMsg_StatusUpdate* msg;

    msg = (OsdMsg_StatusUpdate*)malloc(sizeof(OsdMsg_StatusUpdate));
    if (!msg) return NULL;
    bzero(msg, sizeof(OsdMsg_StatusUpdate));
    msg->netid = netid;
    msg->uid = uid;

    msg->header.protocol_id = OSD_PROTO_ID_C2S;
    msg->header.command = OSD_CMD_CLIENT_STATUS_UPDATE;
    msg->header.data_len_nw = htonl(OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN);
    msg->crc = calc_crc((uint8_t*)msg, sizeof(IFullMsgHeader) + OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN);

    return msg;
}

void write_u32_le_buf(uint8_t* buf, uint32_t data)
{
    buf[0] = (uint8_t) (data & 0xFF);
    data >>= 8;
    buf[1] = (uint8_t) (data & 0xFF);
    data >>= 8;
    buf[2] = (uint8_t) (data & 0xFF);
    data >>= 8;
    buf[3] = (uint8_t) (data & 0xFF);
}

void update_status_update_msg(OsdMsg_StatusUpdate* msg,
        uint8_t netid,
        uint8_t uid,
        const Osd_StatusUpdateData* sud)
{
    uint8_t* buf;
    int i = 0;

    buf = (uint8_t*)msg + sizeof(IFullMsgHeader);
    buf[i++] = netid;
    buf[i++] = uid;
    write_u32_le_buf(&buf[i], sud->bitmask); i += 4;
    write_u32_le_buf(&buf[i], sud->icon_cntr_reg); i += 4;
    write_u32_le_buf(&buf[i], sud->icon_bar_reg); i += 4;
    write_u32_le_buf(&buf[i], sud->icon_bar_text_reg); i += 4;
    write_u32_le_buf(&buf[i], sud->icon_cr1); i += 4;
    write_u32_le_buf(&buf[i], sud->icon_cr2); i += 4;
    strncpy((char*)&buf[i], sud->text, 8);
    msg->crc = calc_crc((uint8_t*)msg, sizeof(IFullMsgHeader) + OSD_MSG_STATUS_UPDATE_PAYLOAD_LEN);
}

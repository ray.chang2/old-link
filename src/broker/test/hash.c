
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>


#define HASHTABLE_IMPLEMENTATION
#include "hashtable.h"

#define		FILTER_LENGTH_MAX		8
#define		CRASH	{char *p=NULL; *p = 'x';}


typedef	struct	_hashstruct
{
	uint32_t	index;
	char		*str;
	uint8_t		filter[8];
	uint32_t	a, b, c, d, e, f, g, h;
} HashStruct;


void nop(){}

///////////////////////////////////////////////////////////////////////////////
//
//
//

HashStruct*
generate_hashstruct(uint32_t index, uint8_t *filter)
{
HashStruct	*phs;

	phs = malloc(sizeof(HashStruct));

	phs->str = "HaShTaBlE"; // an ID string to be certain what we're looking at in memory
	phs->a = random();
	phs->b = random();
	phs->c = random();
	phs->d = random();
	phs->index = index;

	memcpy(phs->filter, filter, FILTER_LENGTH_MAX);

	return phs;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//

uint8_t*
generate_filter(uint32_t length)
{
uint8_t		lettercase[2] = {'a', 'A'};
uint8_t		*filter;
uint32_t	r;


	filter = malloc(FILTER_LENGTH_MAX);

	for(int32_t j=0; j<FILTER_LENGTH_MAX; j++)
	{
		r = random();
		filter[j] = lettercase[r & 1] + (r % 26);
	}

	return filter;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//

int32_t
find_filter_name(uint8_t **filters, uint32_t n, uint8_t *filter)
{

	for(uint32_t i=0; i<n; i++)
	{
		if(!strcmp(filters[i], filter))
			return i;	
	}

	return -1;
}

///////////////////////////////////////////////////////////////////////////////
//
//
//

int
main(int argc, char **argv)
{
uint64_t		*key, m=1, n=10;
uint32_t		verbose=0, debug=0, loops=1;
uint8_t			**filters, *filter;
int32_t			i;
hashtable_t		hashtable;
HashStruct		**hs;



	hashtable_init(&hashtable, sizeof(HashStruct), n, 0);

	for(i=1; i<argc; i++)
	{
		if(!strcmp(argv[i], "-n"))
			n = atoi(argv[++i]);
		else if(!strcmp(argv[i], "-m"))
			m = atoi(argv[++i]);
		else if(!strcmp(argv[i], "-d"))
			debug = 1;
		else if(!strcmp(argv[i], "-v"))
			verbose = 1;
	}

	filters = malloc(n * sizeof(uint8_t*));
	hs = malloc(n * sizeof(HashStruct*));

	srandom(time(NULL));

	//
	// fill in 'n' unique filter names
	//

	if(verbose) printf("generating %ld unique filter names\n", n);

	for(i=0; i<n; i++)
	{
		filters[i] = generate_filter(FILTER_LENGTH_MAX);

		if(debug) printf("filter = %s\n", filters[i]);

		//
		// Make sure that it's unique.
		//

		for(int32_t k=0; k<i; k++)
		{
			if(!strcmp(filters[i], filters[k]))
			{
				printf("INFO: name not unique: %s (%d) --- %s (%d)\n", filters[i], i, filters[k], k);

				// decrement 'i' and get back to the top of the loop
				i--;
			}
		}
	}

	//
	// now we have 'n' unique filter names. Lets start adding them to the hash tabe.
	//

	if(verbose) printf("Adding %ld filter names to the hash table\n", n);

	for(i=0; i<n; i++)
	{
		hs[i] = generate_hashstruct(i, filters[i]);

		filter = filters[i];
		key = (uint64_t*) filter;

		// and into the hashtable she goes ...
		hashtable_insert(&hashtable, *key, hs[i]);
	}

	if(debug)
	{
	uint8_t		str[FILTER_LENGTH_MAX+1];

		for(i=0; i<n; i++)
		{
			memcpy(str, hs[i]->filter, FILTER_LENGTH_MAX);
			str[FILTER_LENGTH_MAX] = 0;
			printf("Entry %d: %d, %s, %8s\n", i, hs[i]->index, hs[i]->str, str);
		}
	}

	//
	// now lets file all 'n' filter names in the hashtable. Lets do it randomly
	// And lets attempt to find it n * n*m times. So each filter will be found on
	// average n*m time. And once found, lets compare with the original.
	//

	if(verbose) printf("Search, Destroy and Create %ld names in the hash table\n", m*n);

	for(i=0; i<m*n; i++)
	{
	uint32_t		index;
	uint8_t			*filter;
	int32_t			status;
	HashStruct		*phs;


		index = random() % n;

		filter = filters[index];
		key = (uint64_t*) filter;

		phs = hashtable_find(&hashtable, *key);

		if(phs == NULL)
		{
			printf("*** FATAL ERROR: could not find entry %d -- %s\n", index, filter);
			exit(1);
		}

		if(phs->index != index) CRASH;
		if(phs->index != hs[index]->index) CRASH;
		if(phs->a != hs[index]->a) CRASH;

		if(debug) printf("	delete filter [%d]: %s\n", index, filter);

		// Now delete this element
		hashtable_remove(&hashtable, *key);

		free(hs[index]);

		// now create a new entry and add it to the hashtable

		while(1)
		{
			filter = generate_filter(FILTER_LENGTH_MAX);

			status = find_filter_name(filters, n, filter);

			if(status == -1)
				break;

			if(debug ) printf("WHOA ---- Duplicate filter name: %s -- %s  (%d)\n", 
				filter, filters[index], status);

			free(filter);
		}

		free(filters[index]);
		filters[index] = filter;
		hs[index] = generate_hashstruct(index, filters[index]);
		key = (uint64_t*) filter;
		// and into the hashtable she goes ...
		hashtable_insert(&hashtable, *key, hs[index]);
	}
}




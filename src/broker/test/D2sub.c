#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include "zmq.h"

#include "D2_channels.h"
#include "broker_api.h"

#include "msgD2.h"


///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

int
main(int argc, char **argv)
{
int32_t					verbose=0, i, retval=0, value;
void					*handle;
scd_bdm_port_status_t	*tm;
char					filter[BROKER_FILTER_MAX];


	handle = bm_subscriber();

	for(i=1; i<argc; i++)
	{
		if(!strcmp(argv[i], "-v"))
			verbose = 1;
		else
		{
			//
			// Subscribe to some messages
			//

			printf("subscribe: %s\n", argv[i]);
			retval = bm_subscribe(handle, argv[i], strlen(argv[i]));
		}
	}

	if(argc < 2)
	{
		printf("Error: Missing value\n");
		exit(1);
	}

	for(i=1; i<argc; i++)
	{
		if(argv[i][0] == '-')
			continue;

	}

	//
	// Start receiving messages
	//

	printf("Starting up @ %d\n", (int) time(NULL));

	while(1)
	{
	uint8_t		filter[BROKER_FILTER_MAX];
	uint32_t	nbytes;


		// bm_receive will allocate memory for the data
		retval = bm_receive(handle, filter, (void*) &tm, &nbytes, 0);

		if(retval < 0)
		{
			if(verbose) printf("Error from bm_receive: %d\n", retval);
			continue;
		}

		printf("Got a Message: %s: %2x %2x %2x %2x %2x %2x \n",
			filter, tm->porta_display, tm->porta_speed_run, tm->portb_display, tm->portb_speed_run,
			tm->port_ab_err_warn, tm->settings_popups);

		// free the memory that was allocated for the message
		free(tm);
	}

	return 0;
}





#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "broker_api.h"
#include "D2_channels.h"
#include "WW_channels.h"
#include "pubsub.h"


#define		PUBLISHER_FILTER_MAX	16

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

int
main(int argc, char **argv)
{
int32_t			verbose=0, n;
uint32_t		i;
OSD_coords_t	coords;



	for(i=1; i<(uint32_t) argc; i++)
	{
		if(!strcmp(argv[i], "-v"))
			verbose = 1;
		else if(!strcmp(argv[i], "-c"))
		{
			coords.portA_x = atoi(argv[++i]);
			coords.portA_y = atoi(argv[++i]);
			coords.portB_x = atoi(argv[++i]);
			coords.portB_y = atoi(argv[++i]);
		}
	}

	//
	// Start publishing messages
	//

	void *fd = bm_publisher();

	if(fd == NULL)
	{
		perror("bm_publisher");
		printf("Error back from bm_publisher\n");
		exit(1);
	}

	srandom(time(NULL));

	coords.timestamp = time(NULL);

	n = bm_send(fd, (uint8_t*) BROKER_BDM_COORDS, &coords, sizeof(coords));

	if(verbose) 
	{
		printf("Sent message %d: id: %s -- %d\n", 
			i, (char*) BROKER_BDM_COORDS, (int) coords.timestamp);
	}

	if(n < 0)
	{
		printf("Error from bm_send_message: %d\n", n);
		perror("bm_send");
		exit(1);
	}

	sleep(5);

	return 0;
}







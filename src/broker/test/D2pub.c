#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>


#include "broker_api.h"

#include "D2_channels.h"
#include "msgD2.h"


typedef struct  _port_display
{
   uint8_t     units:2;
   uint8_t     blade:2;
   uint8_t     mode:2;
   uint8_t     up_arrow:1;
   uint8_t     down_arrow:1;
} port_display_t;

typedef struct  _port_speed_run
{
   uint8_t     speed:7;
   uint8_t     state:1;
} port_speed_run_t;

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			main	
//
// DESCRIPTION:		Sends out a message to any subscribers to the "D2 Status" channel.
//					Run it as follows:   $ ./D2pub [-v] [-s {n}]
//					
//					Command line arguments:		-v  		-- verbosity
//												-s {n}  	-- delay between sending messages
//


int
main(int argc, char **argv)
{
   int32_t					port=-1, nap=1000000, verbose=0, i, n;
   uint8_t					plugged=0, running = 0, mode=2;
   scd_bdm_port_status_t	tm;
   char					filter[BROKER_FILTER_MAX];


   memset(&tm, 0, sizeof(tm));

   for(i=1; i<argc; i++)
   {
      if(!strcmp(argv[i], "-v"))
         verbose = 1;
      else if(!strcmp(argv[i], "-forward"))
         mode = 0;
      else if(!strcmp(argv[i], "-reverse"))
         mode = 1;
      else if(!strcmp(argv[i], "-oscillate1"))
         mode = 2;
      else if(!strcmp(argv[i], "-oscillate2"))
         mode = 3;
      else if(!strcmp(argv[i], "-plugged"))
         plugged = 1;
      else if(!strcmp(argv[i], "-p"))
         port = atoi(argv[++i]);
      else if(!strcmp(argv[i], "-running"))
         running = 1;
      else if(!strcmp(argv[i], "-m"))
         nap = atoi(argv[++i]) * 1000;
      else
      {
         printf("bad argument: %s\n", argv[i]);
         exit(1);
      }
   }

   //
   // Start publishing messages
   //

   void *handle = bm_publisher();

   if(handle == NULL)
   {
      perror("bm_publisher");
      printf("Error back from bm_publisher\n");
      exit(1);
   }

   //
   // fill the channel/filter with 6 chars. All chars the same
   // e.g. 'aaaaaa', 'bbbbbb', etc ...
   //

   sprintf(filter, "%s", D2_STATUS);

   srandom(time(NULL));

   for(i=0; ; i++)
   {
      port_display_t		*pdisplay;
      port_speed_run_t		*ps;

      tm.porta_display = tm.portb_display = 0;

      if(port == 0)
      {
         pdisplay = (port_display_t *)&tm.porta_display;
      }
      else if(port == 1)
      {
         pdisplay = (port_display_t *)&tm.portb_display;
      }
		
      if(pdisplay)
      {
         pdisplay->mode = mode;
         if(plugged) pdisplay->units = random() % 3 + 1;
      }

      if(port == 0)
         ps = (port_speed_run_t *)&tm.porta_speed_run;
      else if(port == 1)
         ps = (port_speed_run_t *)&tm.portb_speed_run;

      if(ps)
      {
         ps->speed = random() % 3000 / 100;
         ps->state = running;
      }

      tm.port_ab_err_warn = random() % 256;
      tm.settings_popups = random() % 256;

      //
      // send this mesaag to the D2_STATUS channel.
      // D2_STATUS is defined in broker/D2_channel.h
      //

      n = bm_send(handle, filter, &tm, sizeof(tm));

      if(n < 0)
      {
         printf("Error from bm_send_message: %d\n", n);
         perror("bm_send");
         exit(1);
      }

      if(verbose) 
      {
         printf("%ld: Sent a Message: %s: %2x %2x %2x %2x %2x %2x \n",
                time(NULL), filter, tm.porta_display, tm.porta_speed_run, 
                tm.portb_display, tm.portb_speed_run,
                tm.port_ab_err_warn, tm.settings_popups);
      }

      if(nap) usleep(nap);
   }

   return 0;
}





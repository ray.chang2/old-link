
#include <pthread.h>


typedef struct	_pubsub
{
	uint8_t		id[BROKER_FILTER_MAX + 1];
	pid_t		pid;
	pthread_t	tid;
	time_t		timestamp;
	uint32_t	seq;
	uint32_t	a;
	uint32_t	b;
	uint32_t	c;
	uint32_t	d;
	uint32_t	e;
	uint32_t	f;
	uint32_t	g;
	uint8_t		data[10000];
} PubSub;


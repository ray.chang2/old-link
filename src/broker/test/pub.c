#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "broker_api.h"
#include "D2_channels.h"
#include "pubsub.h"


#define		PUBLISHER_FILTER_MAX	16

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

int
main(int argc, char **argv)
{
int32_t			nap=0, verbose=0, i, n, filter=0;
PubSub			tm;
char			*filters[PUBLISHER_FILTER_MAX];
uint32_t		max=0, filter_index=0;



	for(i=1; i<argc; i++)
	{
		if(!strcmp(argv[i], "-v"))
			verbose = 1;
		else if(!strcmp(argv[i], "-s"))
			nap = atoi(argv[++i]);
		else if(!strcmp(argv[i], "-m"))
			max = atoi(argv[++i]);
		else filters[filter_index++] = argv[i];
	}

	//
	// Start publishing messages
	//

	void *fd = bm_publisher();

	if(fd == NULL)
	{
		perror("bm_publisher");
		printf("Error back from bm_publisher\n");
		exit(1);
	}

	srandom(time(NULL));

	for(i=0; ; i++)
	{
	char	ch;

		memset(&tm, 0, sizeof(tm));

		if(filter_index == 0)
		{
			// fill the ID with 6 chars. All chars the same
			// e.g. 'aaaaaa', 'bbbbbb', etc ...
			ch = 'a' + (random() % 26);
			memset(&tm.id, ch, 6);
		}
		else
		{
		uint32_t	index = random() % filter_index;

			memcpy(&tm.id, filters[index], strlen(filters[index]));
		}

		tm.timestamp = time(NULL);
		tm.seq = i;
		tm.pid = getpid();
		tm.a = 0xdeadbeef;
		tm.g = 0xdeadbeef;

		n = bm_send(fd, tm.id, &tm, sizeof(tm));

		if(n < 0)
		{
			printf("Error from bm_send_message: %d\n", n);
			perror("bm_send");
			exit(1);
		}

		if(verbose) 
		{
			printf("Sent message %d: id: %s -- %d, seq: %d\n", 
				i, (char*) tm.id, (int) tm.timestamp, tm.seq);
		}

		if(nap)
			sleep(nap);

		if(max > 0 && i == max - 1)
			break;
	}

	return 0;
}


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/syslog.h>

#include "broker_api.h"
#include "D2_channels.h"
#include "WW_channels.h"
#include "pubsub.h"


#define		PUBLISHER_FILTER_MAX	16

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

int
main(int argc, char **argv)
{
int32_t			verbose=0, n;
uint32_t		i;



	for(i=1; i<(uint32_t) argc; i++)
	{
		if(!strcmp(argv[i], "-v"))
			verbose = 1;
	}

    syslog(LOG_ERR, "pubreboot entered...");
	//
	// Start publishing messages
	//

	void *fd = bm_publisher();

	if(fd == NULL)
	{
    	syslog(LOG_ERR, "pubreboot ERROR back from bm_publisher - -exiting");
		exit(1);
	}

	n = bm_send(fd, (uint8_t*) BROKER_PING, NULL, 0);
	n = bm_send(fd, (uint8_t*) BROKER_PING, NULL, 0);

    syslog(LOG_ERR, "pubreboot sending BROKER_REBOOT message");

	n = bm_send(fd, (uint8_t*) BROKER_REBOOT, NULL, 0);

	if(n < 0)
	{
		printf("Error from bm_send_message: %d\n", n);
		perror("bm_send");
		exit(1);
	}

	sleep(10);

	if(verbose) 
	{
		printf("Sent message %d: id: %s\n", 
			i, (char*) WW_CONNECTED);
	}
    syslog(LOG_ERR, "pubreboot exiting successfully");

	return 0;
}







#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "broker_api.h"
#include "D2_channels.h"
#include "WW_channels.h"
#include "WW_channels.h"
#include "pubsub.h"


#define		PUBLISHER_FILTER_MAX	16

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

int
main(int argc, char **argv)
{
int32_t			verbose=0, n;
uint32_t		i, connected=0;



	for(i=1; i<(uint32_t) argc; i++)
	{
		if(!strcmp(argv[i], "-v"))
			verbose = 1;
		else connected = atoi(argv[i]);
	}

	//
	// Start publishing messages
	//

	void *fd = bm_publisher();

	if(fd == NULL)
	{
		perror("bm_publisher");
		printf("Error back from bm_publisher\n");
		exit(1);
	}

	i = 0;
	n = bm_send(fd, (uint8_t*) BROKER_PING, &i, 1);

	i = 0;
	// n = bm_send(fd, (uint8_t*) D2_CONNECTED, &connected, 1);
	n = bm_send(fd, (uint8_t*) WW_CONNECTED, &connected, 1);

	if(verbose) 
	{
		printf("Sent message %d: id: %s\n", 
			i, (char*) WW_CONNECTED);
	}

	if(n < 0)
	{
		printf("Error from bm_send_message: %d\n", n);
		perror("bm_send");
		exit(1);
	}

	sleep(5);

	return 0;
}







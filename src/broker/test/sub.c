
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include "zmq.h"

#include "broker_api.h"
#include "D2_channels.h"
#include "pubsub.h"



///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

int
main(int argc, char **argv)
{
int32_t			count=0, verbose=0, i, retval=0, filter=0;
void			*fd;
PubSub			*tm;



	for(i=1; i<argc; i++)
	{
		if(!strcmp(argv[i], "-v"))
			verbose = 1;
		else if(!strcmp(argv[i], "-f"))
			filter = 1;
		else if(argv[i][0] == '-')
		{
			printf("bas argument: %s\n", argv[i]);
			exit(1);
		}
	}

	//
	// Subscribe to some messages
	//

	fd = bm_subscriber();

	for(i=1; i<argc; i++)
	{
		if(argv[i][0] == '-')
			continue;

		printf("subscribe: %s\n", argv[i]);
		retval = bm_subscribe(fd, argv[i], strlen(argv[i]));
	}

	//
	// Start receiving messages
	//

	printf("Starting up @ %d\n", (int) time(NULL));

	while(1)
	{
	uint8_t		filter[BROKER_FILTER_MAX];
	uint32_t	nbytes;


		memset(&tm, 0, sizeof(tm));

		//
		// bm_receive wil lallocate memory which tm will point to
		//

		memset(filter, 0, BROKER_FILTER_MAX + 1);

		retval = bm_receive(fd, filter, (void*) &tm, &nbytes, 0);

		if(retval < 0)
		{
			if(verbose) printf("Error from bm_receive: %d\n", retval);
			continue;
		}

		if(verbose) printf("Message: %12d: filter: %s timestamp: %d, sequence #: %d \n",
						count, filter, (int) tm->timestamp, (int) tm->seq);

		count++;

		//
		// free the memory bm_receive allocated for us
		//

		free(tm);
	}

	return 0;
}





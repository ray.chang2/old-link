
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "broker_api.h"
#include "D2_channels.h"
#include "pubsub.h"


#define		PUBLISHER_FILTER_MAX	64


uint32_t	nap=0, verbose = 0;

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

void*
pub_thread(void *arg)
{
   int32_t		n;
   PubSub		pubsub_data;
   char		*type = (char*) arg;

   if(verbose)
      printf("Enter: pub_thread: %s\n", type);

   memset(&pubsub_data, 0, sizeof(pubsub_data));
   strcpy(pubsub_data.id, type);
   pubsub_data.tid = pthread_self();

   //
   // Start publishing messages
   //

   void *fd = bm_publisher();

   if(fd == NULL)
   {
      perror("bm_publisher");
      printf("Error back from bm_publisher\n");
      exit(1);
   }

   while(1)
   {
      n = bm_send(fd, type, &pubsub_data, sizeof(pubsub_data));

      if(n < 0)
      {
         perror("bm_send");
         printf("Error back from bm_send: %d\n", n);
         exit(1);
      }

      if(verbose)
         printf("pub_thread: sent a message: %s\n", type);

      if(nap)
         sleep(nap);
   }
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

void*
sub_thread(void *arg)
{
   int32_t		n;
   PubSub		*pubsub_data;
   char		*type = (char*) arg;


   if(verbose)
      printf("Enter: sub_thread: %s\n", type);

   //
   // Start publishing messages
   //

   void *fd = bm_subscriber();

   if(fd == NULL)
   {
      perror("bm_subscriber");
      printf("Error back from bm_subscriber\n");
      exit(1);
   }

   bm_subscribe(fd, type, strlen(type));

   if(verbose)
      printf("sub_thread: subscribe to: %s\n", type);

   while(1)
   {
      uint8_t		filter[BROKER_FILTER_MAX];
      uint32_t nbytes;

      n = bm_receive(fd, filter, (void **)&pubsub_data, &nbytes, 0);

      if(n < 0)
      {
         perror("bm_receive");
         printf("Error back from bm_receive: %d\n", n);
         exit(1);
      }

      if(verbose)
         printf("sub_thread: got a message: %s\n", filter);
   }
}

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

int main(int argc, char **argv)
{
   pthread_t spids[PUBLISHER_FILTER_MAX], ppids[PUBLISHER_FILTER_MAX];
   uint32_t  npids=0, i, n, filter=0;
   PubSub    tm;
   char	     *pubs[PUBLISHER_FILTER_MAX];
   char	     *subs[PUBLISHER_FILTER_MAX];
   uint32_t  max=0, pub_filter_index=0, sub_filter_index=0;



   for(i=1; i<argc; i++)
   {
      if(!strcmp(argv[i], "-v"))
         verbose = 1;
      else if(!strcmp(argv[i], "-n"))
         nap = atoi(argv[++i]);
      else if(!strcmp(argv[i], "-v"))
         verbose = 1;
      else if(!strcmp(argv[i], "-m"))
         max = atoi(argv[++i]);
      else if(!strcmp(argv[i], "-p"))
         pubs[pub_filter_index++] = argv[++i];
      else if(!strcmp(argv[i], "-s"))
         subs[sub_filter_index++] = argv[++i];
   }

   srandom(time(NULL));

   // Start the subscriber threads
   for(i=0; i<sub_filter_index; i++)
   {
      pthread_create(&spids[i], NULL, sub_thread, subs[i]);
   }

   // Start the publisher threads
   for(i=0; i<pub_filter_index; i++)
   {
      pthread_create(&ppids[i], NULL, pub_thread, pubs[i]);
   }

   // just sleep forever, need to ^C to exit this test app
   while(1)
   {
      sleep(10);
      printf("main: sleeping for 10\n");
   }

   return 0;
}


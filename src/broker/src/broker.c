#include <unistd.h>
#include <time.h>
#include <fcntl.h>

#include "broker_api.h"

#define HASHTABLE_IMPLEMENTATION
#include "hashtable.h"

static const char* version = "01.00.00"; //follows major.minor.patch scheme(https://semver.org)

void nop(){}

typedef struct  _subscriber
{
   uint8_t     option;
   uint8_t     filter[BROKER_FILTER_MAX + 1];
} subscription_message_t;


typedef struct	_cached_message
{
   time_t		timestamp;
   uint8_t     filter[BROKER_FILTER_MAX + 1];
   void		*data;
   uint32_t	nbytes;
   uint32_t	count;
} cached_message_t;


void
blog(uint8_t *filter, void *data, uint32_t n)
{
   uint8_t		*fmt = (uint8_t*) "%x ";
   uint8_t		tbuf[128];
   uint32_t	i, m;
   int			fd;
   time_t		t;
   uint8_t		*p;
   uint8_t		buffer[512];

   memset(tbuf, 0, sizeof(tbuf));
   memset(buffer, 0, sizeof(buffer));

   if((fd = open("/var/log/broker.log", O_APPEND|O_RDWR|O_CREAT, 0744)) < 0)
   {
      perror("open");
      return;
   }

   if(!strcmp((char*) filter, "INFO"))
      fmt = (uint8_t*) "%c";

   float	f = sizeof(buffer) * 0.8;

   t = time(NULL);
   p = (uint8_t*) ctime(&t);
   p[strlen((char*) p) - 1] = 0;

   snprintf((char*) buffer, sizeof(buffer), "%s: %s (%u): ", p, filter, n);

   p = data;

   m = (n>30?16:n);

   for(i=0; i<m; i++)
   {
      snprintf((char*) tbuf, sizeof(tbuf), (char*) fmt, (uint8_t) p[i]);
      strcat((char*) buffer, (char*) tbuf);

      if((float) strlen((char*) buffer) > f)
      {
         snprintf((char*) tbuf, sizeof(tbuf), "  strlen(buffer) = %d", 
                  (int) strlen((char*) buffer));
         strcat((char*) buffer, (char*) tbuf);

         strcat((char*) buffer, (char*) "  --- BREAK\n");
         break;
      }
   }

   strcat((char*) buffer, "\n");

   write(fd, (char*) buffer, strlen((char*) buffer));
   close(fd);
}


#if 0
//
// Need to remove support for LVC for the time being
//

#define	ZMQ_LVC_SUPPORT		1
#endif


#if 0//defined(TEST_ZMQ_SUBSCRIPTION_TIMING)
///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

#define	BROKER_MAX_PIDS		32

int32_t
test_subscription_timing(void *data, uint32_t nbytes)
{
   PubSub		*pmessage = (PubSub*) data;
   static		pid_t		pids[BROKER_MAX_PIDS];
   static		uint32_t	npids=0;


   // Any more room for new pids?
   if(npids >= BROKER_MAX_PIDS)
      return 0;

   //
   // The number of bytes in this message needs to be the same as the PubSub structure
   // otherwise we may not be dealing with a PubSub structure, so just return -1;
   //

#if defined(BROKER_DEBUG)
   printf("test_subscription_timing: %d\n", pmessage->seq);
#endif

   if(nbytes != sizeof(PubSub))
      return -1;

   for(uint32_t i=0; i<npids; i++)
   {
      // if this pid is already in the array of pids, then no need to check it
      // we can only ever check the first message from that process.
      if(pids[i] == pmessage->pid)
         return 0;
   }

   // add this pid to the array of pids
   pids[npids++] = pmessage->pid;

   // now the real test. The sequence number needs to be 0. If not, then return the sequence number
   if(pmessage->seq != 0)
      return pmessage->seq;

   // otherwise we're good, so just return 0
   return 0;
}

#endif

///////////////////////////////////////////////////////////////////////////////
// ROUTINE:			
//
// DESCRIPTION:		
//
//

int
main(int argc, char **argv)
{
   uint8_t					*msg;
   char					buffer[256];
   uint64_t				*key;
   int32_t					status, i, verbose=0, debug=1;
   bm_broker_t				broker;	
   zmq_pollitem_t			polldata[2];
   cached_message_t		cached, *pcached;
   hashtable_t				hashtable;

   if (argc > 1 && !strcmp(argv[1],"-V")) {
      printf("%s\n", version);
      return 0;
   }

   msg = (void*) "broker up and running";
   blog((uint8_t*) "INFO", (void*) msg, strlen((char*) msg));

   memset(&polldata, 0, sizeof(polldata));

   hashtable_init(&hashtable, sizeof(cached_message_t), 256, 0);

   for(i=1; i<argc; i++)
   {
      if(!strcmp(argv[i], "-v"))
         verbose = 1;
      else if(!strcmp(argv[i], "-d"))
         debug = 1;
   }

   //
   // Start publishing messages
   //

   if(debug) printf("broker: calling bm_broker()\n");

   status = bm_broker(&broker);
   if(status < 0)
   {
      perror("bm_broker");
      printf("Error from bm_subscribe: errno = %d\n", errno);
      return -1;
   }

   if(debug) printf("broker: back from bm_broker()\n");

#if 1
   status = bm_subscribe(broker.publishers, "", 0);
   if(status < 0)
   {
      perror("bm_subscribe");
      printf("Error from bm_subscribe: status = %d, errno = %d\n", status, errno);
      return -1;
   }
#endif

   if(debug) printf("broker: back from bm_subscribe()\n");

#if defined(ZMQ_LVC_SUPPORT)
   zhash_t	*cache = zhash_new();
#endif

   polldata[0].events = ZMQ_POLLIN;
   polldata[0].socket = broker.publishers->socket;

   polldata[1].events = ZMQ_POLLIN;
   polldata[1].socket = broker.subscribers->socket;

   msg = (void*) "broker ready";
   blog((uint8_t*) "INFO", msg, strlen((char*) msg));

   while(1)
   {
      int32_t n;
      if(verbose) printf("broker: calling zmq_poll\n");

      n = zmq_poll((zmq_pollitem_t*) &polldata, 2, -1);

      if(verbose) 
         printf("Broker: poll: returns: %d::  %x, %x\n", 
                n, polldata[0].revents, polldata[1].revents);

      if(n < 0)
      {
         return 1;
      }
      else if(n == 0)
      {
         // if(verbose) printf("Broker: no message\n" );
         continue;
      }

      if(polldata[0].revents & ZMQ_POLLIN)
      {
         uint32_t	nbytes;
         uint8_t		filter[BROKER_FILTER_MAX + 1];
         void		*data;

         memset(filter, 0, sizeof(filter));

         n = bm_receive(broker.publishers, filter, &data, &nbytes, 0);

         // 
         // If an error occurs, we most likely don't want to exit, or continue, 
         // but log an error message in a log file somewhere.
         //

         if(n < 0)
            continue;

#if 0 //defined(TEST_ZMQ_SUBSCRIPTION_TIMING)
         status = test_subscription_timing(data, nbytes);

         if(status == -1)
         {
            if(verbose) printf("test_subscription_timing data size anomally: %u -- %d\n", 
                               nbytes, (int) sizeof(PubSub));
            return 2;
         }
         else if(status > 0)
         {
            if(verbose) printf("test_subscription_timing sequence number anomally: %d \n", status);
            return 1;
         }
#endif

         // Do not log the PING messages
         if(strcmp((char*) filter, BROKER_PING))
         	blog(filter, data, nbytes);

         n = bm_send(broker.subscribers, filter, data, nbytes);

#if 0
         if(debug)
            blog(filter, data, nbytes);
#endif

         key = (uint64_t*) filter;
         pcached = hashtable_find(&hashtable, *key);

         if(pcached)
         {
            if(verbose) printf("Freeing a cached message: %s\n", filter);
            if(pcached->nbytes > sizeof(pcached->data))
               free(pcached->data);

            hashtable_remove(&hashtable, *key);
         }

         cached.timestamp = time(NULL);
         memcpy(cached.filter, filter, BROKER_FILTER_MAX);
         cached.filter[BROKER_FILTER_MAX] = 0;
         cached.nbytes = nbytes;
         cached.count = 0;

         if(pcached) 
            cached.count = pcached->count;

         if(nbytes > sizeof(cached.data))
         {
            cached.data = malloc(nbytes);
            memcpy(cached.data, data, nbytes);
            free(data);
         }
         else
            cached.data = data;

         if(verbose) printf("Caching a message: %s\n", filter);
         hashtable_insert(&hashtable, *key, &cached);

         // 
         // If an error occurs, we most likely don't want to exit, but log an error 
         // message in a log file somewhere.
         //

         if(n < 0)
         {
            if(verbose) printf("Broker: ERROR sending message of %u bytes\n", nbytes);
         }

         if(verbose) printf("Broker: published a message of %u bytes\n", nbytes);

         // sleep(1);
      }

      if(polldata[1].revents & ZMQ_POLLIN)
      {
         zmq_msg_t zmsg;
         subscription_message_t	sub;

         memset(&cached, 0, sizeof(cached_message_t));
         zmq_msg_init(&zmsg);

         //
         // Any new subscribers since startup ?
         //

         // n = zmq_recv(broker.subscribers->socket, data, 128, ZMQ_DONTWAIT);
         n = zmq_msg_recv(&zmsg, broker.subscribers->socket, 0);

         if(n < 0)
         {
            if(verbose) printf ("zmq_recv returns: %d \n", n);
            continue;
         }

         // Lets get the subscription data into our subscription structure so we can 
         // make sense of it all.
         memcpy(&sub, zmq_msg_data(&zmsg), n);

         if(verbose) printf("Subscription Message: %x --- %s\n", sub.option, sub.filter);

         zmq_msg_close(&zmsg);

         key = (uint64_t *) sub.filter;
         pcached = hashtable_find(&hashtable, *key);

         if(sub.option == 1) 
         {
            if(verbose) printf("broker: NEW SUBSCRIBER: %s\n", sub.filter);

            if(pcached == NULL)
            {
               // Send a NULL message to indicate NO DATA ???
               printf("*** Should the broker send a NULL messagae key: %lld  filter: %s   ***\n", (long long) *key, sub.filter);
            }
            else
            {
               pcached = hashtable_find(&hashtable, *key);
               pcached->count++;

               snprintf(buffer, sizeof(buffer), "LVC: New subscriber message to [%s] -- count: %u\n", 
                        pcached->filter, pcached->count);
               blog((uint8_t*) buffer, NULL, 0);

               n = bm_send(broker.subscribers, pcached->filter, pcached->data, pcached->nbytes);
			
               if(debug)
                  blog(sub.filter, pcached->data, pcached->nbytes);
            }
         }
         else
         {
            if(pcached != NULL)
            {
               pcached->count--;
               if(verbose) printf("LVC: LOST a subscriber to %s -- count: %u\n", sub.filter, pcached->count);

#if 0
               if(verbose) printf("LVC: Feeong Cached Memory for %s -- count: %u\n", sub.filter, pcached->count);

               if(pcached->nbytes > sizeof(pcached->data))
                  free(pcached->data);

               hashtable_remove(&hashtable, *key);
#endif
            }
         }
      }
   }

   return 0;
}





cmake_minimum_required(VERSION 3.12)

project(broker, VERSION 1.0.1 LANGUAGES C)

set(CPPCHECK_LOCAL_IGNORE_FLAGS
        -U SQLITE_ALLOW_COVERING_INDEX_SCAN
        -U fdatasync
        -U SQLITE_ALLOW_COVERING_INDEX_SCAN
        -U SQLITE_BITMASK_TYPE
        -U SQLITE_OS_TRACE_PROC
        -U SQLITE_DEFAULT_AUTOVACUUM
        -U SQLITE_DEFAULT_AUTOVACUUM
        -U SQLITE_DEFAULT_CACHE_SIZE
        -U SQLITE_DEFAULT_FILE_PERMISSIONS
        -U SQLITE_DEFAULT_JOURNAL_SIZE_LIMIT
        -U SQLITE_DEFAULT_MEMSTATUS
        -U SQLITE_DEFAULT_PAGE_SIZE
        -U SQLITE_DEFAULT_SECTOR_SIZE
        -U SQLITE_DEFAULT_PCACHE_INITSZ
        -U fts5YYERRORSYMBOL
        -U SQLITE_DEFAULT_WORKER_THREADS
        -U SQLITE_DEFAULT_SYNCHRONOUS
        -U SQLITE_DEFAULT_WAL_SYNCHRONOUS
        -U fts5YYWILDCARD
        -U SQLITE_ALLOW_COVERING_INDEX_SCAN
        -U SQLITE_BITMASK_TYPE
        -U SQLITE_OS_TRACE_PROC
        -U SQLITE_MAX_VARIABLE_NUMBER
        -U SQLITE_MAX_SCHEMA_RETRY
        -U SQLITE_MAX_MEMORY
        -U SQLITE_MAX_LIKE_PATTERN_LENGTH
        -U SQLITE_MEMORY_BARRIER
        -U CTIMEOPT_VAL2_
        -U osFstat
        )

execute_process(COMMAND cppcheck
        ${CPPCHECK_FLAGS}
        ${CPPCHECK_IGNORE_FLAGS}
        ${CPPCHECK_LOCAL_IGNORE_FLAGS}
        ${GENERAL_INCLUDE_FLAGS}
        ${CMAKE_CURRENT_SOURCE_DIR}/broker.c)

add_executable(broker broker.c)
 
target_include_directories(broker PRIVATE
                           ../libBridgeCommon/inc ../inc
                           ../bdm/include)

target_compile_options(broker PRIVATE ${WARN_OPTIONS})
target_link_libraries(broker pthread BridgeCommon zmq )
install(TARGETS broker RUNTIME DESTINATION ${DESTDIR})


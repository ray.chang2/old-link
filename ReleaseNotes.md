# Connected Tower Bridge Software Release Notes

## v0.2.0 (2018-11-05)
- Re-enable UART2
- Serial LED (Orange) working in linux
- Fix Orange LED lighting up during u-boot

## v0.1.0 (2018-11-01)
- Initial versioned release
- Wi-Fi working
- UART3 working (removed UART2)

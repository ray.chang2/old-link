# Bridge Application Building Process

## Install pre-requisitives on Ubuntu 16.04

We need to install some tools for building the `contower` code. For details,
reference `build/docker/Dockerfile`.

The procedure described in the Dockerfile including installing the Bridge SDK,
is built from Yocto building process (using `bitbake core-image-base -c
populate_sdk`). It supports ZeroMQ and SQLite natively.  And it allows us to
add additional libraries quickly and conveniently.

If you apply the process in the Dockerfile, you don't need to install the
SDK separately. But when you do need to install it separately for some reason,
like updating it, see the next section.

## Install Bridge SDK

To install Bridge SDK, download it from here and run it:
<https://s3.amazonaws.com/ctbtools/fsl-imx-xwayland-glibc-x86_64-core-image-base-cortexa7hf-neon-toolchain-4.9.88-2.0.0.sh>

The SDK will install a new toolchain at `/opt/fs-imx-xwayland`. However, you
don't put one of the bin directories into your path as you might do to other
cross-toolchains. Instead, each time you wish to use the SDK in a new shell
session, you need to source the environment setup script, e.g:

    source /opt/fsl-imx-xwayland/4.9.88-2.0.0/environment-setup-cortexa7hf-neon-poky-linux-gnueabi

_(The `setup-imx-enviroment` script desribed below includes the above source
call.)_

This will set up many developmenet environment variables like `CC`, `AR`, `LD`
etc, for you to use.

For example, to compile a C file `hwzmq_client.c`, you can do this:

    CC -c hwzmq_client.c $(pkg-config --cflags libzmq)
    CC -o hwzmq_client hwzmq_client.o $(pkg-config --libs libzmq)


## Clone CTB (Connected Tower Bridge) Code

Run these command to clone the repository and also init it (clone GoogleTest
repo):

    git clone https://bitbucket.org/djiang2014/contower
    cd contower

## Building SD image  

In order to build SD image stop here and follow the steps under ./doc/platform.md in a new terminal window.
Do not follow below steps.

## Init Code Dependencies

    source scripts/setup-environment
    ctb init        # only need to do once

## Build Bridge Code

To build bridge applications:

    source setup-imx-environment
    ctb build imx       # or just ctb build

To build PC version applications:

    source setup-pc-environment
    ctb build pc

## Git Development Workflows

The `master` branch is intended to be deployable all the time. And we want to reduce merge effor by minimizing long-living feature branches. So we suggest we integrate our feature branches as frequently as possible. For larger features, consider use a [Feature Toggle](https://martinfowler.com/articles/feature-toggles.html) to merge your branch constantly while it's still in development.

First, let's check out the code:

    $ git clone git@bitbucket.org:djiang2014/contower.git
    ( Now you have a `contower` directory)

Then, make changes under this directory, You may use your preferred branching strategies, or consider the following ones:

### Direct Commit without Review

For minor changes that you think you don't need a review. You don't need to create a branch.

    (if you are not aleady in master branch)
    $ git checkout master
    (# update from upstream)
    $ git pull
    (make your changes...)
    $ git commit -m "module: my new improvement"
    $ git push

You are done. Your code is committed to `master` directly. The BitBucket build pipeline will check and build your code. The whole team will receive email notifications if your new commit has caused a failed build or broken the tests.

### Using a Feature Branch for Review

To avoid the broken commits and to improve code quality, you can have your code peer-reviewed before commiting to `master` branch, which also adds the benefits of running automatic build in your branch instead of `master`. You can to create a feature branch like this:

    (if you are not already in master branch:)
    $ git checkout master
    (update from upstream:)
    $ git pull
    $ git checkout -b my_awesome_feature
    (make your changes...)
    $ git commit -m "my_awesome_feature: change 1"
    (make further changes...)
    $ git commit -m "my_awesome_feature: change 2"
    (push your branch to BitBucket)
    $ git push origin my_awesome_feature

Now in BitBucket, create a Pull Request. Note this Pull Request doesn't have to mean you've finished the feature. You are encourged to merge (send Pull Request) daily if not more frequently to avoid future merge difficulties. If you haven't finished the feature, You can set the Pull Request to not merge to the master after the review is finished .

Note it's useful to create a Pull Request even if you don't think there's a need for review. Pull Request allows easy side-by-side view of your changes, and also as mentioned can put your new commit through build and tests. And if you find something wrong, you can `git commit` more changs, which will update the same pull request automatically. When closing the Pull Request and merging the branch, all commits in the pull request will be squashed to a single commit. This makes the commit history cleaner.

#### Clean up after the Merging the Pull Request

If the Pull Reqeust has been set up such that the feature branch will _not_ be deleted after merging, you can keep working on your feature branch.

If the Pull Reqeust has been set up such that the feature branch will be deleted after merging. Do this to clean up your local feature branch:

    $ git checkout master
    (fetch remote branches and delete non-existent remote branches)
    $ git fetch -p
    $ git pull
    (You can stop here. Or delete your obsolete feature branch if you are sure everthing has been merged)
    $ git branch -D my_awesome_feature

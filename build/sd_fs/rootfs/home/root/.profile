# echo "Running $HOME/.profile"

alias ls='ls --color'
alias ll='ls -l'
alias la='ls -a'
alias s='screen'
alias gs='git status -s'
alias gx='git tree2'
alias gx8='git tree2 -8'
alias gx20='git tree2 -20'

echo -e "$(cat /proc/version)"
echo -e "\nCurrent date: $(date)"

export PATH=/sn:/home/root/bin:$PATH

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

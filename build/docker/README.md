This Dockerfile is used to generate a Ubuntu 16.10 image with all Yocto prerequisites installed. The BitBucket build pipeline will reference this image for CI.

To download this this image so you can test it manually:

    docker pull smithnephew/ctb

CREATE TABLE `common_ref` ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `type` varchar(10), `code` varachar(20), `description` varchar(40), `mod_by` INTEGER, `mod_time` datetime, status integer );
CREATE TABLE "device_config" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `mode` INTEGER, `ssid` varchar ( 40 ), `password` varchar (120 ), `wifi_profile` INTEGER, `channel` INTEGER, `mod_by` INTEGER, `mod_time` datetime, status integer, FOREIGN KEY(`wifi_profile`) REFERENCES `common_ref`(`id`), FOREIGN KEY(`channel`) REFERENCES `common_ref`(`id`) );
CREATE TABLE "device_information" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, device varchar(20),`device_type` INTEGER, `device_name` varchar ( 30 ), `software_version` varchar ( 20 ), `sku` varchar ( 20 ), `serial` varchar ( 20 ), `hardware_part` varchar ( 20 ), `hardware_part_rev` varchar ( 20 ), `mac_address` varchar ( 20 ), `fw_icon` varchar ( 300 ), `rw_icon` varchar ( 300 ), `osc_icon` varachr ( 300 ),status integer,`mod_by` INTEGER, `mod_time` datetime, FOREIGN KEY(`device_type`) REFERENCES `common_ref`(`id`) );
CREATE TABLE "osd_configuration" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `port_a_x` INTEGER, `port_a_y` INTEGER, `port_b_x` INTEGER,`port_b_y` INTEGER,status integer, `mod_by` INTEGER, `mod_time` datetime, FOREIGN KEY(`mod_by`) REFERENCES `user`(`id`));
CREATE TABLE "service_configuration" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `connected_network` INTEGER, `discovery_mgr` INTEGER, `remote_app_dsp` INTEGER, `remote_control_app` INTEGER, `surgeon_profile` INTEGER, `ext_trigger` INTEGER, `auto_updates` INTEGER, status integer, `mod_by` INTEGER, `mod_time` datetime  );
CREATE TABLE "user" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `code` varchar ( 20 ), `user_name` varchar ( 80 ), `user_type` INTEGER, `user_password` varchar ( 120 ),status integer);

insert into common_ref (type,code,description) values('wifi','US','USA');
insert into common_ref (type,code,description) values('wifi','EU','Europe');

insert into common_ref (type,code,description) values('channel','1','1');
insert into common_ref (type,code,description) values('channel','2','2');
insert into common_ref (type,code,description) values('channel','3','3');
insert into common_ref (type,code,description) values('channel','4','4');
insert into common_ref (type,code,description) values('channel','5','5');
insert into common_ref (type,code,description) values('channel','6','6');
insert into common_ref (type,code,description) values('channel','7','7');
insert into common_ref (type,code,description) values('channel','8','8');
insert into common_ref (type,code,description) values('channel','9','9');
insert into common_ref (type,code,description) values('channel','10','10');
insert into common_ref (type,code,description) values('channel','11','11');

insert into common_ref (type,code,description) values('devtype','1','Video');
insert into common_ref (type,code,description) values('devtype','2','Shaver');
insert into common_ref (type,code,description) values('devtype','3','Coblation');
insert into common_ref (type,code,description) values('devtype','4','Tablet');

insert into common_ref (type,code,description) values('network','host','Host');
insert into common_ref (type,code,description) values('network','ap','AP');

insert into user (code,user_name, user_type, user_password,status ) values('Admin','administrator',1,'$2a$10$CFnrL4hODSwHUvnbfD/lYeODOnDscONLu4vFEAaS4TptnN.zWa/v6',0);
insert into user (code,user_name, user_type, user_password, status) values('Dev','smn-devlopment',2,'$2a$10$/Zrqpf.RAu2jhp1nh0TOIebvTemuf6K7hBybrzsFeP55F3/P.j6BS',0);
insert into user (code,user_name, user_type, user_password, status) values('Read','readonly user',3,'$2a$10$ulkevBINQpgMX.6CSLb.deml9KREA8n.LUhe3J96iNx71olGnjQna',0);


insert into device_config (id, mode,ssid,password, status, mod_by) values(1, 0, 'BALSON', '12345678',1,1);
insert into device_config (id, mode,ssid,password, wifi_profile, status,channel, mod_by) values(2, 1,'SMNAP','12345678',1,0,3, 1);

insert into service_configuration ( connected_network, discovery_mgr, remote_app_dsp, remote_control_app, surgeon_profile, ext_trigger, auto_updates, status,mod_by) values(1,1,1,1,1,1,1,0,2);
insert into device_information(device,device_type, device_name,software_version,sku,serial, hardware_part, hardware_part_rev, mac_address,fw_icon, rw_icon,osc_icon,status,mod_by) values('d2',15,'DYONICS II BRIDGE','v123','456','c9834234','234','v.13','0.123.45.AB.CD.EF','/usr/01.png','/usr/02.png','/usr/03.png',0,2);
insert into osd_configuration(id, port_a_x, port_a_y, port_b_x, port_b_y, status, mod_by, mod_time) values (1, 0, 58, 0, 70, 0, 1, 0);
.rekey "" secret secret
VACUUM;

/*###############################################################################
#   Filename: ctb_db_commonref_api.c
#
#   APIs to access elements of the User table in the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  2/14/2019   Initial Version
#  01.00  CG team  2/26/2019   Version shared with S&N Team.
##############################################################################*/

#include "ctb_db_api.h"

/*------------------------------------------------------------------------------
#   APIs for Common Ref table 
------------------------------------------------------------------------------*/

int get_commonref_type(char * type, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_getField(COMMON_REF_TABLE, 
			      COMMON_REF_TYPE, 
			      (void *) type, 
			      size,
			      COMMON_REF_ID, 
			      (void *) &primary_key);
	return retval;
}

int set_commonref_type(char * type, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_setField(COMMON_REF_TABLE, 
			      COMMON_REF_TYPE, 
			      (void *) type, 
			      size,
			      COMMON_REF_ID, 
			      (void *) &primary_key);
	return retval;
}

int get_commonref_code(char * code, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_getField(COMMON_REF_TABLE, 
			      COMMON_REF_CODE, 
			      (void *) code, 
			      size,
			      COMMON_REF_ID, 
			      (void *) &primary_key);
	return retval;
}

int set_commonref_code(char * code, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_setField(COMMON_REF_TABLE, 
			      COMMON_REF_CODE, 
			      (void *) code, 
			      size,
			      COMMON_REF_ID, 
			      (void *) &primary_key);
	return retval;
}

int get_commonref_description(char * description, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_getField(COMMON_REF_TABLE, 
			      COMMON_REF_DESCRIPTION, 
			      (void *) description, 
			      size,
			      COMMON_REF_ID, 
			      (void *) &primary_key);
	return retval;
}

int set_commonref_description(char * description, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_setField(COMMON_REF_TABLE, 
			      COMMON_REF_DESCRIPTION, 
			      (void *) description, 
			      size,
			      COMMON_REF_ID, 
			      (void *) &primary_key);
	return retval;
}

int get_commonref_modby(int * modby, int primary_key)
{
        int retval = 0;

        retval = ctb_getField(COMMON_REF_TABLE,
                              COMMON_REF_MOD_BY,
                              (void *) modby,
                              COMMON_REF_MOD_BY_SIZE,
                              COMMON_REF_ID, 
			      (void *) &primary_key);
        return retval;
}

int set_commonref_modby(int modby, int primary_key)
{
        int retval = 0;

        retval = ctb_setField(COMMON_REF_TABLE,
                              COMMON_REF_MOD_BY,
                              (void *) &modby,
                              COMMON_REF_MOD_BY_SIZE,
                              COMMON_REF_ID, 
			      (void *) &primary_key);
        return retval;
}

int get_commonref_modtime(long int * modtime, int primary_key)
{
        int retval = 0;

        retval = ctb_getField(COMMON_REF_TABLE,
                              COMMON_REF_MOD_TIME,
                              (void *) modtime,
                              COMMON_REF_MOD_TIME_SIZE,
                              COMMON_REF_ID, 
			      (void *) &primary_key);
        return retval;
}

int set_commonref_modtime(long int modtime, int primary_key)
{
        int retval = 0;

        retval = ctb_setField(COMMON_REF_TABLE,
                              COMMON_REF_MOD_TIME,
                              (void *) &modtime,
                              COMMON_REF_MOD_TIME_SIZE,
                              COMMON_REF_ID, 
			      (void *) &primary_key);
        return retval;
}

int get_commonref_status(int * status, int primary_key)
{
        int retval = 0;

        retval = ctb_getField(COMMON_REF_TABLE,
                              COMMON_REF_STATUS,
                              (void *) status,
                              COMMON_REF_STATUS_SIZE,
                              COMMON_REF_ID, 
			      (void *) &primary_key);
        return retval;
}

int set_commonref_status(int status, int primary_key)
{
        int retval = 0;

        retval = ctb_setField(COMMON_REF_TABLE,
                              COMMON_REF_STATUS,
                              (void *) &status,
                              COMMON_REF_STATUS_SIZE,
                              COMMON_REF_ID, 
			      (void *) &primary_key);
        return retval;
}


/* END OF FILE: ctb_db_commonref_api.c */

/*###############################################################################
#   Filename: ctb_data.h
#
#   Header file for
#   Data Access Layer APIs for accessing the data in the sqlite3 database.
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   MODIFICATION HISTORY
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  1/21/2019   Initial Creation
#  01.00  CG team  2/21/2019   Version shared with S&N Team.
#  01.01  CG team  3/18/2019   Reference to verify_query_input function deleted.
##############################################################################*/
#ifndef CTB_DATA_H_
#define CTB_DATA_H_

#include "ctb_db_api.h"


/*------------------------------------------------------------------------------
#   Data declarations
------------------------------------------------------------------------------*/
/* CTB Table size */
#define CTB_NUM_TABLES  		7
#define COMREF_TABLE_PARAM_NUM          8       
#define DEVCONF_TABLE_PARAM_NUM         10
#define DEVINFO_TABLE_PARAM_NUM         19
#define OSDCONF_TABLE_PARAM_NUM         11
#define SERVICECONF_TABLE_PARAM_NUM     12
#define USER_TABLE_PARAM_NUM            7
#define TABLE_PARAM_NUM			19

/* Database operation return codes */
#define DB_CREATE_TABLE_SUCCESS         201
#define DB_SELECT_SUCCESS               202
#define DB_INSERT_SUCCESS               203
#define DB_DELETE_SUCCESS               204
#define DB_UPDATE_SUCCESS               205

/* Database Table Names IDs*/
#define COMMON_REF_TABLE_ID     0       
#define DEVICE_CONFIG_TABLE_ID  1
#define DEVICE_INFO_TABLE_ID    2
#define OSD_CONF_TABLE_ID       3
#define SERVICE_CONF_TABLE_ID   4
#define USER_TABLE_ID           5

typedef struct TABLES{
	char * column[TABLE_PARAM_NUM];
	int    type[TABLE_PARAM_NUM];
	int    size[TABLE_PARAM_NUM];
}tables;

extern char *  ctb_tablenames[CTB_NUM_TABLES];
extern tables ctb_tables[CTB_TABLE_COUNT];


/*------------------------------------------------------------------------------
# Macro 
------------------------------------------------------------------------------*/

#define verify_params()  \
		{							\
			if (NULL == tablename)				\
                		return DB_TABLE_NAME_NULL;		\
        		if (NULL == columnname)				\
                		return DB_TABLE_COLUMNNAME_NULL;	\
        		if (NULL == value)				\
                		return DB_TABLE_PARAMVALUE_NULL;	\
			if (NULL != column_ref)				\
			{						\
				if (NULL == column_ref_value)		\
		                        return DB_COLUMNREF_VALUE_NULL;	\
			}						\
		}


/*------------------------------------------------------------------------------
#  Function declarations 
------------------------------------------------------------------------------*/

int get_column_details(char * tablename, char * columnname, 
		          int * type, int * size);

int create_generic_select_query(
                char * query,
                char * tablename,
                char * columnname,
                char * column_ref,
                int    ref_type,
                void * column_ref_value);

int create_generic_update_query(
                char * query,
                char * tablename,
                char * columnname,
                int    type,
                void * value,
		int    value_size,
                char * column_ref,
                void * column_ref_value,
                int    ref_type);

#endif  //CTB_DATA_H_

/* END OF FILE */

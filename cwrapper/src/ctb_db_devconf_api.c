/*###############################################################################
#   Filename: ctb_db_devconf_api.c
#
#   APIs to access elements of the Service Configuration table in the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  2/12/2019   Initial Version
#  01.00  CG team  2/26/2019   Version shared with S&N team.
#  01.02  CG team  5/06/2019   Added APIs for setMode, getMode, getCredentials
#                              setCredentials.
##############################################################################*/

#include "ctb_db_api.h"

/*------------------------------------------------------------------------------
#   APIs for Device Config table 
------------------------------------------------------------------------------*/

int get_devconf_mode(int * mode, int primary_key)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_CONFIG_TABLE, 
			      DEV_CONF_MODE, 
			      (void *) mode, 
			      DEV_CONF_MODE_SIZE, 
			      DEV_CONF_ID, 
			      (void *) &primary_key);
	return retval;
}

int set_devconf_mode(int mode, int primary_key)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_CONFIG_TABLE, 
			      DEV_CONF_MODE, 
			      (void *) &mode, 
			      DEV_CONF_MODE_SIZE, 
			      DEV_CONF_ID, 
			      (void *) &primary_key);
	return retval;
}

int get_devconf_ssid(char * ssid, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_CONFIG_TABLE, 
			      DEV_CONF_SSID, 
			      (void *) ssid, 
			      size,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
	return retval;
}

int set_devconf_ssid(char * ssid, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_CONFIG_TABLE, 
			      DEV_CONF_SSID, 
			      (void *) ssid, 
			      size,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
	return retval;
}

int get_devconf_password(char * password, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_getField(DEVICE_CONFIG_TABLE, 
			      DEV_CONF_PASSWORD, 
			      (void *) password, 
			      size,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
	return retval;
}

int set_devconf_password(char * password, int size, int primary_key)
{
	int retval = 0;

	retval = ctb_setField(DEVICE_CONFIG_TABLE, 
			      DEV_CONF_PASSWORD, 
			      (void *) password, 
			      size,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
	return retval;
}

int get_devconf_wifi_profile(int * wifi_profile, int primary_key)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_WIFI_PROFILE,
                              (void *) wifi_profile,
                              DEV_CONF_WIFI_PROFILE_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int set_devconf_wifi_profile(int wifi_profile, int primary_key)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_WIFI_PROFILE,
                              (void *) &wifi_profile,
                              DEV_CONF_WIFI_PROFILE_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int get_devconf_channel(int * channel, int primary_key)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_CHANNEL,
                              (void *) channel,
                              DEV_CONF_CHANNEL_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int set_devconf_channel(int channel, int primary_key)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_CHANNEL,
                              (void *) &channel,
                              DEV_CONF_CHANNEL_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int get_devconf_modby(int * modby, int primary_key)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_MOD_BY,
                              (void *) modby,
                              DEV_CONF_MOD_BY_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int set_devconf_modby(int modby, int primary_key)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_MOD_BY,
                              (void *) &modby,
                              DEV_CONF_MOD_BY_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int get_devconf_modtime(long int * modtime, int primary_key)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_MOD_TIME,
                              (void *) modtime,
                              DEV_CONF_MOD_TIME_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int set_devconf_modtime(long int modtime, int primary_key)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_MOD_TIME,
                              (void *) &modtime,
                              DEV_CONF_MOD_TIME_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int get_devconf_status(int * status, int primary_key)
{
        int retval = 0;

        retval = ctb_getField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_STATUS,
                              (void *) status,
                              DEV_CONF_STATUS_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int set_devconf_status(int status, int primary_key)
{
        int retval = 0;

        retval = ctb_setField(DEVICE_CONFIG_TABLE,
                              DEV_CONF_STATUS,
                              (void *) &status,
                              DEV_CONF_STATUS_SIZE,
			      DEV_CONF_ID, 
			      (void *) &primary_key);
        return retval;
}

int setMode(int mode)
{
	int rc = 0;
	int primary_key =  1;  /* AP */

	if (MODE_OPERATION_HOST_VALUE == mode)
	{
		rc = set_devconf_status(NETWORK_MODE_ACTIVE,
			                primary_key);
		rc = set_devconf_status(NETWORK_MODE_INACTIVE,
			                (primary_key+1));
	}
	else if (MODE_OPERATION_AP_VALUE == mode)
	{
		rc = set_devconf_status(NETWORK_MODE_INACTIVE,
			                (primary_key));
		rc = set_devconf_status(NETWORK_MODE_ACTIVE,
			                (primary_key+1));
	}
	else
	{
		return DB_INVALID_MODE_VALUE;
	}

	return rc;
}

int getMode(int * mode)
{
	int rc = 0;
	int primary_key =  1;  /* AP */
	int net_status = -1;

	if (NULL == mode)
	{
		return DB_INVALID_PARAM;
	}

	rc = get_devconf_status(&net_status, primary_key);
	if (rc >= DB_OPERATION_OK)
	{
		if(net_status)
		{
			rc = get_devconf_mode(mode, primary_key);
		}
		else
		{
			rc = get_devconf_mode(mode, (primary_key+1));
		}	
	}		

	return rc;
}

int getCredentials(device_config_st * devconf_st)
{
	int rc = 0;

	if(NULL == devconf_st)
		return DB_INVALID_PARAM;

	if((MODE_OPERATION_AP_VALUE == devconf_st->mode) || 
	   (MODE_OPERATION_HOST_VALUE == devconf_st->mode))
	{
		rc = get_devconf_ssid(devconf_st->ssid, 
			      	      DEV_CONF_SSID_SIZE,
			              (devconf_st->mode+1));
		rc = get_devconf_password(devconf_st->password, 
			                  DEV_CONF_PASSWORD_SIZE,
			                  (devconf_st->mode+1));
		rc = get_devconf_wifi_profile(&devconf_st->wifi_profile, 
				              (devconf_st->mode+1));
		rc = get_devconf_channel(&devconf_st->channel, 
			                 (devconf_st->mode+1));
		rc = get_devconf_modby(&devconf_st->mod_by, 
			               (devconf_st->mode+1));
		rc = get_devconf_modtime(&devconf_st->mod_time, 
			                 (devconf_st->mode+1));
		rc = get_devconf_status(&devconf_st->status, 
			                (devconf_st->mode+1));

		return rc;
	}
	else
	{
		return DB_INVALID_MODE_VALUE;
	}
}

int setCredentials(device_config_st * devconf_st)
{
	int rc = 0;

	if(NULL == devconf_st)
		return DB_INVALID_PARAM;

	if((MODE_OPERATION_AP_VALUE == devconf_st->mode) || 
	   (MODE_OPERATION_HOST_VALUE == devconf_st->mode))
	{
		rc = set_devconf_ssid(devconf_st->ssid, 
			      	      DEV_CONF_SSID_SIZE,
			              (devconf_st->mode+1));
		rc = set_devconf_password(devconf_st->password, 
			                  DEV_CONF_PASSWORD_SIZE,
			                  (devconf_st->mode+1));
		rc = set_devconf_wifi_profile(devconf_st->wifi_profile, 
				              (devconf_st->mode+1));
		rc = set_devconf_channel(devconf_st->channel, 
			                 (devconf_st->mode+1));
		rc = set_devconf_modby(devconf_st->mod_by, 
			               (devconf_st->mode+1));
		rc = set_devconf_modtime(devconf_st->mod_time, 
			                 (devconf_st->mode+1));
		rc = set_devconf_status(devconf_st->status, 
			                (devconf_st->mode+1));

		return rc;
	}
	else
	{
		return DB_INVALID_MODE_VALUE;
	}
}

/* END OF FILE: ctb_db_devconf_api.c */

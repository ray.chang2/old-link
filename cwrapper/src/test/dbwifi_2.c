#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "ctb_data.h"
#include "ctb_db_table_api.h"

int32_t
main(int32_t argc, char **argv)
{
int	 		i, status, mode=0;
char			ssid[DEV_CONF_SSID_SIZE], pass[DEV_CONF_PASSWORD_SIZE];
device_config_st	creds;


	memset(&creds, 0, sizeof(creds));
	memset(ssid, 0, sizeof(ssid));
	memset(pass, 0, sizeof(pass));

	for(i=1; i<argc; i++)
	{
		if((0 == strcmp(argv[i], "ap")))
			creds.mode = MODE_OPERATION_AP_VALUE;
		else if((0 == strcmp(argv[i], "host")))
			creds.mode = MODE_OPERATION_HOST_VALUE;
		else 
		{
			printf("unknown parameter: %s\n", argv[i]);
			exit(1);
		}

		strcpy(creds.ssid, argv[++i]);
		strcpy(creds.password, argv[++i]);

		status = setCredentials(&creds);

		if(status < 0)
		{
			printf("database error: %d\n", status);
			exit(1);
		}
	}

	creds.mode = MODE_OPERATION_AP_VALUE;
	getCredentials(&creds);
	printf("AP: SSID:  %s, PWD: %s\n", creds.ssid, creds.password);

	creds.mode = MODE_OPERATION_HOST_VALUE;
	getCredentials(&creds);
	printf("HOST: SSID:  %s, PWD: %s\n", creds.ssid, creds.password);
}




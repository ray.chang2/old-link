

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


#include "ctb_data.h"
#include "ctb_db_table_api.h"


int32_t
main(int32_t argc, char **argv)
{
int		i, status, mode=0;
char	ssid[DEV_CONF_SSID_SIZE], pass[DEV_CONF_PASSWORD_SIZE];


	memset(ssid, 0, sizeof(ssid));
	memset(pass, 0, sizeof(pass));

	for(i=1; i<argc; i++)
	{
		status = set_devconf_ssid(argv[i], sizeof(ssid));
		if(status < 0) printf("error from set_devconf_ssid(): %d\n", status);
		status = set_devconf_password(argv[++i], sizeof(pass));
		if(status < 0) printf("error from set_devconf_password(): %d\n", status);
	}

	get_devconf_ssid(ssid, sizeof(ssid));
	get_devconf_password(pass, sizeof(pass));
	printf("SSID: %s, PWD: %s\n", ssid, pass);

}




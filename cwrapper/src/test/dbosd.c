

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


#include "ctb_data.h"
#include "ctb_db_table_api.h"




int32_t
main(int32_t argc, char **argv)
{
int32_t		i, status=0;
int32_t		x, y;


	for(i=1; i<argc; i++)
	{
		if(!strcmp(argv[i], "-a"))
		{
			x = atoi(argv[++i]);
			y = atoi(argv[++i]);
			status = set_osd_port_a_coordinates(x, y);
		}
		else if(!strcmp(argv[i], "-b"))
		{
			x = atoi(argv[++i]);
			y = atoi(argv[++i]);
			status = set_osd_port_b_coordinates(x, y);
		}
	}

	status = get_osd_port_a_coordinates(&x, &y);
	printf("PORT A Coords: (%d, %d)  status: %d\n", x, y, status);

	status = get_osd_port_b_coordinates(&x, &y);
	printf("PORT B Coords: (%d, %d)  status: %d\n", x, y, status);


}




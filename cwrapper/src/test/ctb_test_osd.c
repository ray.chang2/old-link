#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ctb_db_table_api.h"

time_t modtime;
struct tm *modtime_s;
char modtime_str[80];

void test_get_osdconf_api()
{
	int rc;
	int intval= 0;
	int port_a_x = 0;
	int port_a_y = 0;
	int port_b_x = 0;
	int port_b_y = 0;
	long int osd_modtime = 0;

	rc = 0;

	rc = get_osd_port_a_coordinates(&port_a_x, &port_a_y);
	if(rc >= 0)
		printf("[port_a_coordinates]: [%d][%d] \n", port_a_x, port_a_y);

	rc = get_osd_port_b_coordinates(&port_b_x, &port_b_y);
	if(rc >= 0)
		printf("[port_b_coordinates]: [%d][%d] \n", port_b_x, port_b_y);

	intval = 0;
	rc = get_osd_modby(&intval);
	if(rc > 0)
		printf("[mod_by]: [%d] \n", intval);

	osd_modtime = 0;
	rc = get_osd_modtime(&osd_modtime);
	if(rc > 0)
		printf("[mod_time]: [%ld]\n", osd_modtime);
	modtime = (time_t) osd_modtime;
	modtime_s = localtime(&modtime);
	strftime(modtime_str, sizeof(modtime_str), 
		"%a %Y-%m-%d %H:%M:%S %z", modtime_s);
	printf("Mod Time: %s\n", modtime_str);

	intval = 0;
	rc = get_osd_status(&intval);
	if(rc > 0)
		printf("[status]: [%d] \n", intval);
}

void test_set_osdconf_api()
{
	int rc = 0;
	int intval= 0;
	long int osd_modtime = 0;
	int port_a_x = 0;
	int port_a_y = 50;
	int port_b_x = 0;
	int port_b_y = 70;

	rc = set_osd_port_a_coordinates(port_a_x, port_a_y);
	if(rc > 0)
		printf("[port_a_coordinates]: [%d][%d] \n", port_a_x, port_a_y);

	rc = set_osd_port_b_coordinates(port_b_x, port_b_y);
	if(rc > 0)
		printf("[port_b_coordinates]: [%d][%d] \n", port_b_x, port_b_y);

	intval = 1;
	rc = set_osd_modby(intval);
	printf("Set [mod_by] result: %d, [%d] \n", rc, intval);

	modtime = time(NULL);
	osd_modtime = (long int) modtime;
	rc = set_osd_modtime(osd_modtime);
	printf("Set [mod_time] result: %d, [%ld]\n", rc, osd_modtime);

	intval = 1; 
	rc = set_osd_status(intval);
	printf("Set [status] Result : %d, [%d] \n", rc, intval);
}


int main()
{
	test_get_osdconf_api();
	test_set_osdconf_api();

	return 0;
}



#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


#include "ctb_data.h"
#include "ctb_db_table_api.h"



char	*smode[2] = {MODE_OPERATION_AP, MODE_OPERATION_HOST};

int32_t
main(int32_t argc, char **argv)
{
	char mode[DEV_CONF_MODE_SIZE]="";

	if(argc > 1)
	{
		if(!strcmp(argv[1], "ap"))
			set_devconf_mode(MODE_OPERATION_AP,
					 DEV_CONF_MODE_SIZE);
		
		if(!strcmp(argv[1], "host"))
			set_devconf_mode(MODE_OPERATION_HOST,
					DEV_CONF_MODE_SIZE);
	}

	get_devconf_mode(mode, DEV_CONF_MODE_SIZE);

	if(!(strncmp(mode,MODE_OPERATION_AP, DEV_CONF_MODE_SIZE) || 
			strncmp(mode, MODE_OPERATION_HOST, DEV_CONF_MODE_SIZE)))
	{
		printf("Invalide Mode in Database: %s\n", mode);
		exit(1);
	}

	printf("Current Database Mode: %s\n", mode);

}




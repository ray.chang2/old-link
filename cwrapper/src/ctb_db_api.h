/*###############################################################################
#   Filename: ctb_db_api.h
#
#   Header file for
#   Data Access Layer for accessing the data in the sqlite3 database.
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  1/21/2019   Initial Creation
#  01.01  CG team  2/21/2019   Version shared with S&N Team.
#  01.01  CG team  2/26/2019   Version shared with S&N Team on 26th Feb 2019.
#  01.02  CG team  3/13/2019   Fixed bug with the common ref table colulmnnames.
#  01.03  CG team  3/19/2019   Added OSD_CONF_PORT_A_X, OSD_CONF_PORT_A_Y,
#                              OSD_CONF_PORT_B_X, OSD_CONF_PORT_B_Y constants
#                              for the APIs added to get x & y coordiantes for
#                              Port A and Port B.
#  01.04  CG team  4/16/2019   Code changes to open DB from /sn/webserver/db
#                              location.
#  01.05  CG team  5/06/2019   Added new error codes for network mode APIs.
#                              Added #defines for network mode and values.
##############################################################################*/
#ifndef CTB_DB_API_H_
#define CTB_DB_API_H_

#define SQLITE3_SEE 1

#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*------------------------------------------------------------------------------
#   Data declarations
------------------------------------------------------------------------------*/

/* List of Connected Tower Bridge Devices */
#define LENS_4K		"LENS_4K"
#define DYONICS_II	"DYONICS_II"
#define WEREWOLF	"Werewolf"
#define TABLET_APP	"Tablet_App"

/* Status Definitions */
#define FACTORY 0
#define ACTIVE	1

/* Mode of Operation */
#define MODE_OPERATION_AP		"AP"
#define MODE_OPERATION_HOST		"HOST"
#define MODE_OPERATION_AP_VALUE		1
#define MODE_OPERATION_HOST_VALUE	0
#define NETWORK_MODE_INACTIVE		0
#define NETWORK_MODE_ACTIVE		1

/* Device Types */
#define VIDEO		1
#define SHAVER		2
#define COBLATION	3
#define TABLET		4

/* User types */
#define ADMIN_LEVEL 	1
#define DEVELOPER	2
#define READONLY	3

/* Common Ref definitions */
#define WIFI_TYPE	"wifi"
#define CHANNEL_TYPE	"channel"
#define CODE_US		"US"
#define CODE_EU		"EU"
#define DESC_USA	"USA"
#define DESC_EUROPE	"Europe"

/* SERVICE CONFIGURATIONS */
#define SERVICE_ENABLE		1
#define SERVICE_DISABLE		0
#define CONNECTED_NETWORK	201
#define DISCOVERY_MGR		202
#define REMOTE_APP_DSP		203
#define REMOTE_CONTROL_APP	204
#define SURGEON_PROFILE 	205
#define EXTERNAL_TRIGGERS	206
#define AUTO_UPDATE		207

/* Database definitions */
#define DB_FILE_NAME 		"/sn/webserver/db/ctb.db"
#define DB_FACTORY_FILE_NAME 	"/sn/webserver/db/ctb-factory.db"
#define KEY			"secret"
#define KEY_SIZE		6

/* CTB Database data type */
#define INT        	1
#define CHAR       	2
#define DATETIME   	3
#define INVALID_TYPE 	-1
#define TRUE		1
#define FALSE		0

/* Database Operation Types */
#define DB_CREATE_TABLE         301
#define DB_SELECT               302
#define DB_GET                  303
#define DB_INSERT               304
#define DB_DELETE               305
#define DB_UPDATE               306
#define DB_SET                  307

#define DEFAULT_PRIMARY_KEY_VALUE 1

/* Database operation return codes */
#define DB_OK				0
#define DB_OPERATION_OK			0
#define DB_VERIFY_OK			0
#define DB_QUERY_OK			0

/* Database operation error codes */
#define DB_OPEN_FAILED				-1
#define DB_INVALID_OPERATION			-2
#define DB_TABLE_NAME_NULL			-3
#define DB_TABLE_COLUMNNAME_NULL 		-4	
#define DB_INVALID_TABLENAME			-5
#define DB_INVALID_COLUMNNAME			-6
#define DB_TABLE_PARAMVALUE_NULL		-7
#define DB_INVALID_VALUE_SIZE			-8
#define DB_INVALID_PATH_FIELD_VALUE_SIZE	-9
#define DB_INVALID_COLUMN_REF_NAME		-10
#define DB_COLUMNREF_VALUE_NULL			-11
#define DB_INVALID_PARAM			-12
#define DB_RETURNED_NO_VALUE			-13
#define DB_SET_OPERATION_FAILED			-14
#define DB_INVALID_MODE_VALUE			-15

/* Database Table Names */
#define COMMON_REF_TABLE	"common_ref"
#define DEVICE_CONFIG_TABLE 	"device_config"
#define DEVICE_INFO_TABLE	"device_information"
#define OSD_CONF_TABLE		"osd_configuration"
#define SERVICE_CONF_TABLE	"service_configuration"
#define USER_TABLE		"user"

#define COMMON_REF_TABLE_FACTORY	"common_ref_FACT"
#define DEVICE_CONFIG_TABLE_FACTORY 	"device_config_FACT"
#define DEVICE_INFO_TABLE_FACTORY	"device_information_FACT"
#define OSD_CONF_TABLE_FACTORY		"osd_configuration_FACT"
#define SERVICE_CONF_TABLE_FACTORY	"service_configuration_FACT"
#define USER_TABLE_FACTORY		"user_FACT"

#define CTB_TABLE_COUNT		6
#define CTB_TABLE_FACTORY_COUNT	6

#define MAX_TABLENAME_SIZE 	30
#define MAX_COLUMNNAME_SIZE 	30
#define MIN_FIELD_SIZE		20
#define MAX_FIELD_SIZE		80
#define PATH_FIELD_SIZE		300

/* Common Ref Column size */
#define COMMON_REF_ID_SIZE		sizeof(int)
#define COMMON_REF_TYPE_SIZE		10
#define COMMON_REF_CODE_SIZE		MIN_FIELD_SIZE
#define COMMON_REF_DESCRIPTION_SIZE	40
#define COMMON_REF_MOD_BY_SIZE		sizeof(int)
#define COMMON_REF_MOD_TIME_SIZE	sizeof(long int)
#define COMMON_REF_STATUS_SIZE		sizeof(int)

/* Device Config Column size */
#define DEV_CONF_ID_SIZE		sizeof(int)
#define DEV_CONF_MODE_SIZE		sizeof(int)
#define DEV_CONF_SSID_SIZE		40
#define DEV_CONF_PASSWORD_SIZE		120
#define DEV_CONF_WIFI_PROFILE_SIZE	sizeof(int)
#define DEV_CONF_CHANNEL_SIZE		sizeof(int)
#define DEV_CONF_MOD_BY_SIZE		sizeof(int)
#define DEV_CONF_MOD_TIME_SIZE		sizeof(long int)
#define DEV_CONF_STATUS_SIZE		sizeof(int)

/* Device Information Column size */
#define DEV_INFO_ID_SIZE		sizeof(int)
#define DEV_INFO_DEVICE_TYPE_SIZE	sizeof(int)
#define DEV_INFO_DEVICE_NAME_SIZE	30
#define DEV_INFO_SOFTWARE_VERSION_SIZE	MIN_FIELD_SIZE
#define DEV_INFO_SKU_SIZE		MIN_FIELD_SIZE
#define DEV_INFO_SERIAL_SIZE		MIN_FIELD_SIZE
#define DEV_INFO_HARDWARE_PART_SIZE	MIN_FIELD_SIZE
#define DEV_INFO_HARDWARE_PART_REV_SIZE	MIN_FIELD_SIZE
#define DEV_INFO_MAC_ADDRESS_SIZE	40
#define DEV_INFO_ICON_PATH_SIZE		PATH_FIELD_SIZE
#define DEV_INFO_FW_ICON_SIZE		PATH_FIELD_SIZE
#define DEV_INFO_RW_ICON_SIZE		PATH_FIELD_SIZE
#define DEV_INFO_OSC_ICON_SIZE		PATH_FIELD_SIZE
#define DEV_INFO_ABLATE_ICON_SIZE 	PATH_FIELD_SIZE
#define DEV_INFO_COAG_ICON_SIZE		PATH_FIELD_SIZE
#define DEV_INFO_AMBIENT_TEMP_ICON_SIZE PATH_FIELD_SIZE
#define DEV_INFO_FACTORY_DATE_SIZE	sizeof(long int)
#define DEV_INFO_MOD_BY_SIZE		sizeof(int)
#define DEV_INFO_MOD_TIME_SIZE		sizeof(long int)
#define DEV_INFO_STATUS_SIZE		sizeof(int)

/* OSD configuration Column size */
#define OSD_CONF_ID_SIZE		sizeof(int)
#define OSD_CONF_PORT_A_LOC_SIZE	sizeof(int)
#define OSD_CONF_PORT_B_LOC_SIZE	sizeof(int)
#define OSD_CONF_WW_ICON_LOC_SIZE	sizeof(int)
#define OSD_CONF_MOD_BY_SIZE		sizeof(int)
#define OSD_CONF_MOD_TIME_SIZE		sizeof(long int)
#define OSD_CONF_STATUS_SIZE		sizeof(int)

/* Service configuration Column size */
#define SER_CONF_FIELD_SIZE		sizeof(int)
#define SER_CONF_ID_SIZE		SER_CONF_FIELD_SIZE
#define SER_CONF_CONNECTED_NETWORK_SIZE	SER_CONF_FIELD_SIZE
#define SER_CONF_DISCOVERY_MGR_SIZE	SER_CONF_FIELD_SIZE
#define SER_CONF_REMOTE_APP_DSP_SIZE	SER_CONF_FIELD_SIZE
#define SER_CONF_REMOTE_CONTROL_APP_SIZE	SER_CONF_FIELD_SIZE
#define SER_CONF_SURGEON_PROFILE_SIZE	SER_CONF_FIELD_SIZE
#define SER_CONF_EXT_TRIGGER_SIZE	SER_CONF_FIELD_SIZE
#define SER_CONF_AUTO_UPDATES_SIZE	SER_CONF_FIELD_SIZE
#define SER_CONF_MOD_BY_SIZE		SER_CONF_FIELD_SIZE
#define SER_CONF_MOD_TIME_SIZE		sizeof(long int)
#define SER_CONF_STATUS_SIZE		SER_CONF_FIELD_SIZE

/* User Column size */
#define USER_ID_SIZE			sizeof(int)
#define USER_CODE_SIZE			MIN_FIELD_SIZE
#define USER_NAME_SIZE			80
#define USER_TYPE_SIZE			sizeof(int)
#define USER_PASSWORD_SIZE		120	
#define USER_STATUS_SIZE		sizeof(int)

#define MAX_QUERY_SIZE		2048
#define REF_QUERY_SIZE		350

/* Common Reference Table Column names */
#define COMMON_REF_ID		"id"
#define COMMON_REF_TYPE		"type"
#define COMMON_REF_CODE		"code"
#define COMMON_REF_DESCRIPTION	"description"
#define COMMON_REF_MOD_BY	"mod_by"	
#define COMMON_REF_MOD_TIME	"mod_time"
#define COMMON_REF_STATUS	"status"

/* Device Config Table Column names */
#define DEV_CONF_ID		"id"
#define DEV_CONF_MODE		"mode"
#define DEV_CONF_SSID		"ssid"
#define DEV_CONF_PASSWORD	"password"
#define DEV_CONF_WIFI_PROFILE	"wifi_profile"
#define DEV_CONF_CHANNEL	"channel"
#define DEV_CONF_MOD_BY		"mod_by"
#define DEV_CONF_MOD_TIME	"mod_time"
#define DEV_CONF_STATUS		"status"

/* Device Information Table Column names */
#define DEV_INFO_ID 			"id"
#define DEV_INFO_DEVICE_TYPE 		"device_type"
#define DEV_INFO_DEVICE_NAME 		"device_name"
#define DEV_INFO_SOFTWARE_VERSION 	"software_version"
#define DEV_INFO_SKU 			"sku"
#define DEV_INFO_SERIAL 		"serial"
#define DEV_INFO_HARDWARE_PART 		"hardware_part"
#define DEV_INFO_HARDWARE_PART_REV 	"hardware_part_rev"
#define DEV_INFO_MAC_ADDRESS 		"mac_address"
#define DEV_INFO_FW_ICON 		"fw_icon"
#define DEV_INFO_RW_ICON 		"rw_icon"
#define DEV_INFO_OSC_ICON 		"osc_icon"
#define DEV_INFO_ABLATE_ICON 		"ablate_icon"
#define DEV_INFO_COAG_ICON 		"coag_icon"
#define DEV_INFO_AMBIENT_TEMP_ICON 	"ambient_temp_icon"
#define DEV_INFO_MOD_BY 		"mod_by"
#define DEV_INFO_MOD_TIME 		"mod_time"
#define DEV_INFO_STATUS 		"status"

/* OSD Configuration Table Column names */
#define OSD_CONF_ID		"id"
#define OSD_CONF_PORT_A_X	"port_a_x"
#define OSD_CONF_PORT_A_Y 	"port_a_y"
#define OSD_CONF_PORT_B_X	"port_b_x"
#define OSD_CONF_PORT_B_Y	"port_b_y"
#define OSD_CONF_WW_ICON_LOC_X	"ww_icon_loc_x"
#define OSD_CONF_WW_ICON_LOC_Y	"ww_icon_loc_y"
#define OSD_CONF_MOD_BY		"mod_by"
#define OSD_CONF_MOD_TIME	"mod_time"
#define OSD_CONF_STATUS		"status"

/* Service Configuration Table Column names */
#define SER_CONF_ID			"id"
#define SER_CONF_CONNECTED_NETWORK	"connected_network"
#define SER_CONF_DISCOVERY_MGR		"discovery_mgr"
#define SER_CONF_REMOTE_APP_DSP		"remote_app_dsp"
#define SER_CONF_REMOTE_CONTROL_APP	"remote_control_app"
#define SER_CONF_SURGEON_PROFILE	"surgeon_profile"
#define SER_CONF_EXT_TRIGGER		"ext_trigger"
#define SER_CONF_AUTO_UPDATES		"auto_updates"
#define SER_CONF_MOD_BY			"mod_by"
#define SER_CONF_MOD_TIME		"mod_time"
#define SER_CONF_STATUS			"status"

/* User Table Column names */
#define USER_ID 	"id"
#define USER_CODE	"code"
#define USER_NAME	"user_name"
#define USER_TYPE	"user_type"
#define USER_PASSWORD	"user_password"
#define USER_STATUS	"status"

/* Structure for DeviceConfig Table */
typedef struct DEVICE_CONFIG_STRUCT{
	int  	  id;
	int  	  mode;
	char 	  ssid[DEV_CONF_SSID_SIZE];
	char 	  password[DEV_CONF_PASSWORD_SIZE];
	int  	  wifi_profile;
	int  	  channel;
	int  	  mod_by;
	long int  mod_time;
	int  	  status;
}device_config_st;

/*------------------------------------------------------------------------------
#   API declarations
------------------------------------------------------------------------------*/
/* Generic APIs 
 * Use these APIs which have multiple row entries - like user Table, 
 * common_ref table.
 */
int ctb_getField(   char * tablename,
                    char * columnname,
                    void * value,
                    int    value_size,
                    char * column_ref,
                    void * column_ref_value);

int ctb_setField(   char * tablename,
                    char * columnname,
                    void * value,
                    int    value_size,
                    char * column_ref,
                    void * column_ref_value);

#endif  //CTB_DB_API_H_

/* END OF FILE */

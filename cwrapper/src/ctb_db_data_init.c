/*###############################################################################
#   Filename: ctb_db_data_init.c
#
#   Initialize static data for accessing the data in the sqlite3 database.
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  1/21/2019   Initial Version
#  01.00  CG team  2/21/2019   Version shared with S&N Team.
#  01.01  CG team  3/19/2019   Updated OSD Configuration table data for APIs to
#                              get x & y coordinates for Port A and Port B.
#  01.02  CG team  3/25/2019   Fixed defect in OSD configuration init data.
##############################################################################*/

#include "ctb_db_api.h"
#include "ctb_data.h"

char * ctb_tablenames[CTB_NUM_TABLES] = {
                COMMON_REF_TABLE, 
                DEVICE_CONFIG_TABLE, 
                DEVICE_INFO_TABLE, 
                OSD_CONF_TABLE, 
                SERVICE_CONF_TABLE, 
                USER_TABLE, 
	       	NULL
};

tables ctb_tables[] = {
        /* Common Ref Table data */
	{
	    {
		COMMON_REF_ID, 
		COMMON_REF_TYPE, 
		COMMON_REF_CODE,
		COMMON_REF_DESCRIPTION, 
		COMMON_REF_MOD_BY,
		COMMON_REF_MOD_TIME, 
		COMMON_REF_STATUS,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	        NULL, NULL	
	    },
	    {
		INT, CHAR, CHAR, CHAR, INT, DATETIME, INT,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	    },
	    {
		COMMON_REF_ID_SIZE,
		COMMON_REF_TYPE_SIZE, 
		COMMON_REF_CODE_SIZE,
		COMMON_REF_DESCRIPTION_SIZE, 
		COMMON_REF_MOD_BY_SIZE,
		COMMON_REF_MOD_TIME_SIZE, 
		COMMON_REF_STATUS_SIZE,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	    }
	},

	/* Device Config Table data */
	{
	    {
	        DEV_CONF_ID, 
		DEV_CONF_MODE, 
		DEV_CONF_SSID, 
		DEV_CONF_PASSWORD,
		DEV_CONF_WIFI_PROFILE, 
		DEV_CONF_CHANNEL, 
		DEV_CONF_MOD_BY,
		DEV_CONF_MOD_TIME, 
		DEV_CONF_STATUS,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
	    },
	    {
		INT, INT, CHAR, CHAR, INT, INT, INT, DATETIME, INT,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	    },
	    {
	        DEV_CONF_ID_SIZE, 
		DEV_CONF_MODE_SIZE, 
		DEV_CONF_SSID_SIZE, 
		DEV_CONF_PASSWORD_SIZE,
		DEV_CONF_WIFI_PROFILE_SIZE, 
		DEV_CONF_CHANNEL_SIZE, 
		DEV_CONF_MOD_BY_SIZE,
		DEV_CONF_MOD_TIME_SIZE, 
		DEV_CONF_STATUS_SIZE,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	    }
	},

	/* Device Information Table Data */
	{
	    {
		DEV_INFO_ID, 
		DEV_INFO_DEVICE_TYPE, 
		DEV_INFO_DEVICE_NAME,
		DEV_INFO_SOFTWARE_VERSION, 
		DEV_INFO_SKU, 
		DEV_INFO_SERIAL,
		DEV_INFO_HARDWARE_PART, 
		DEV_INFO_HARDWARE_PART_REV,
		DEV_INFO_MAC_ADDRESS, 
		DEV_INFO_FW_ICON, 
		DEV_INFO_RW_ICON,
		DEV_INFO_OSC_ICON, 
		DEV_INFO_ABLATE_ICON, 
		DEV_INFO_COAG_ICON,
		DEV_INFO_AMBIENT_TEMP_ICON, 
		DEV_INFO_MOD_BY, 
		DEV_INFO_MOD_TIME, 
		DEV_INFO_STATUS,
        	NULL
	    },
	    {
		INT, INT, CHAR, CHAR, CHAR, CHAR, CHAR, CHAR, CHAR, 
		CHAR, CHAR, CHAR, CHAR, CHAR, CHAR, INT, DATETIME, INT,
		0
	    },
	    {
		DEV_INFO_ID_SIZE, 
		DEV_INFO_DEVICE_TYPE_SIZE, 
		DEV_INFO_DEVICE_NAME_SIZE,
		DEV_INFO_SOFTWARE_VERSION_SIZE, 
		DEV_INFO_SKU_SIZE, 
		DEV_INFO_SERIAL_SIZE,
		DEV_INFO_HARDWARE_PART_SIZE, 
		DEV_INFO_HARDWARE_PART_REV_SIZE,
		DEV_INFO_MAC_ADDRESS_SIZE, 
		DEV_INFO_ICON_PATH_SIZE, 
		DEV_INFO_ICON_PATH_SIZE,
		DEV_INFO_ICON_PATH_SIZE, 
		DEV_INFO_ICON_PATH_SIZE, 
		DEV_INFO_ICON_PATH_SIZE,
		DEV_INFO_ICON_PATH_SIZE, 
		DEV_INFO_MOD_BY_SIZE, 
		DEV_INFO_MOD_TIME_SIZE, 
		DEV_INFO_STATUS_SIZE,
		0
	    }
	},

	/* OSD Configuration Table Data */
	{
	    {
		OSD_CONF_ID, 
		OSD_CONF_PORT_A_X, 
		OSD_CONF_PORT_A_Y, 
		OSD_CONF_PORT_B_X,
		OSD_CONF_PORT_B_Y,
		OSD_CONF_WW_ICON_LOC_X,
		OSD_CONF_WW_ICON_LOC_Y,
		OSD_CONF_MOD_BY, 
		OSD_CONF_MOD_TIME, 
		OSD_CONF_STATUS,
        	NULL, NULL, NULL, NULL, NULL, 
		NULL, NULL, NULL, NULL
	    },
	    {
		INT, INT, INT, INT, INT, INT, INT,
		INT, DATETIME, INT,
		0, 0, 0, 0, 0, 0, 0, 0, 0
	    },
	    {
		OSD_CONF_ID_SIZE, 
		OSD_CONF_PORT_A_LOC_SIZE, 
		OSD_CONF_PORT_B_LOC_SIZE,
		OSD_CONF_PORT_A_LOC_SIZE, 
		OSD_CONF_PORT_B_LOC_SIZE,
		OSD_CONF_WW_ICON_LOC_SIZE,
		OSD_CONF_WW_ICON_LOC_SIZE,
		OSD_CONF_MOD_BY_SIZE, 
		OSD_CONF_MOD_TIME_SIZE, 
		OSD_CONF_STATUS_SIZE,
		0, 0, 0, 0, 0, 0, 0, 0, 0 
	    }
	},

	/* Service Configuration Table Data */
	{
	    {
		SER_CONF_ID,
		SER_CONF_CONNECTED_NETWORK,
		SER_CONF_DISCOVERY_MGR,
		SER_CONF_REMOTE_APP_DSP,
		SER_CONF_REMOTE_CONTROL_APP,
		SER_CONF_SURGEON_PROFILE,
		SER_CONF_EXT_TRIGGER,
		SER_CONF_AUTO_UPDATES,
		SER_CONF_MOD_BY,
		SER_CONF_MOD_TIME,
		SER_CONF_STATUS,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
	    },
	    {
	   	INT, INT, INT, INT, INT, INT, INT, INT, INT, DATETIME, INT,
		0, 0, 0, 0, 0, 0, 0, 0
	    },
	    {
		SER_CONF_ID_SIZE,
		SER_CONF_CONNECTED_NETWORK_SIZE,
		SER_CONF_DISCOVERY_MGR_SIZE,
		SER_CONF_REMOTE_APP_DSP_SIZE,
		SER_CONF_REMOTE_CONTROL_APP_SIZE,
		SER_CONF_SURGEON_PROFILE_SIZE,
		SER_CONF_EXT_TRIGGER_SIZE,
		SER_CONF_AUTO_UPDATES_SIZE,
		SER_CONF_MOD_BY_SIZE,
		SER_CONF_MOD_TIME_SIZE,
		SER_CONF_STATUS_SIZE,
		0, 0, 0, 0, 0, 0, 0, 0
	    }
	},

	/* User Id Table Data */
	{
	    {
		USER_ID,
		USER_CODE,
		USER_NAME,
		USER_TYPE,
		USER_PASSWORD,
		USER_STATUS,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL
	    },
	    {
		INT, CHAR, CHAR, INT, CHAR, INT,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	    },
	    {
		USER_ID_SIZE,
		USER_CODE_SIZE,
		USER_NAME_SIZE,
		USER_TYPE_SIZE,
		USER_PASSWORD_SIZE,
		USER_STATUS_SIZE,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	    }
	}
};


/* END OF FILE: ctb_db_data_init.c */

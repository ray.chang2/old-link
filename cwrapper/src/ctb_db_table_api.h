/*###############################################################################
#   Filename: ctb_db_table_api.h
#
#   APIs to access elements of the tablein the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  2/12/2019   Initial Version
#  01.00  CG team  2/21/2019   Version shared with S&N team.
#  01.01  CG team  2/26/2019   Version shared with S&N team on 26 Feb 2019.
#  01.02  CG team  3/19/2019   Added function declarations for OSD APIs to
#                              get x & y coordinates for Port A and Port B.
#  01.03  CG team  3/25/2019   Added function declarations for OSD APIs to set
#                              x & y coordinates for Port A and Port B.
#  01.04  CG team  5/06/2019   Added APIs for network mode - setMode, getMode
#                              setCredentials, getCredentials.
#  01.05  CG team  5/14/2019   Made the set_osd_ww_icon_loc_coordinates API
#                              consistent with the D2 APIs for OSD.
##############################################################################*/
#ifndef _CTB_DB_TABLE_API_H_
#define _CTB_DB_TABLE_API_H_

#include <stdio.h>
#include <string.h>
#include "ctb_db_api.h"

/*------------------------------------------------------------------------------
#   API definitions
------------------------------------------------------------------------------*/
/* Common Ref table APIs*/
int get_commonref_type(char * type, int size, int primary_key);
int set_commonref_type(char * type, int size, int primary_key);
int get_commonref_code(char * code, int size, int primary_key);
int set_commonref_code(char * code, int size, int primary_key);
int get_commonref_description(char * description, int size, int primary_key);
int set_commonref_description(char * description, int size, int primary_key);
int get_commonref_modby(int * modby, int primary_key);
int set_commonref_modby(int modby, int primary_key);
int get_commonref_modtime(long int * modtime, int primary_key);
int set_commonref_modtime(long int modtime, int primary_key);
int get_commonref_status(int * status, int primary_key);
int set_commonref_status(int status, int primary_key);

/* Device Config Table APIs */
int get_devconf_mode(int * mode, int primary_key);
int set_devconf_mode(int mode, int primary_key);
int get_devconf_ssid(char * ssid, int size, int primary_key);
int set_devconf_ssid(char * ssid, int size, int primary_key);
int get_devconf_password(char * password, int size, int primary_key);
int set_devconf_password(char * password, int size, int primary_key);
int get_devconf_wifi_profile(int * wifi_profile, int primary_key);
int set_devconf_wifi_profile(int wifi_profile, int primary_key);
int get_devconf_channel(int * channel, int primary_key);
int set_devconf_channel(int channel, int primary_key);
int get_devconf_modby(int * modby, int primary_key);
int set_devconf_modby(int modby, int primary_key);
int get_devconf_modtime(long int * modtime, int primary_key);
int set_devconf_modtime(long int modtime, int primary_key);
int get_devconf_status(int * status, int primary_key);
int set_devconf_status(int status, int primary_key);
int getMode(int * mode);
int setMode(int mode);
int getCredentials(device_config_st * devconf_st);
int setCredentials(device_config_st * devconf_st);

/* Device Information table APIs*/
int get_devinfo_device_type(int * device_type);
int set_devinfo_device_type(int device_type);
int get_devinfo_device_name(char * device_name, int size);
int set_devinfo_device_name(char * device_name, int size);
int get_devinfo_software_version(char * software_version, int size);
int set_devinfo_software_version(char * software_version, int size);
int get_devinfo_sku(char * sku, int size);
int set_devinfo_sku(char * sku, int size);
int get_devinfo_serial(char * serial, int size);
int set_devinfo_serial(char * serial, int size);
int get_devinfo_hardware_part(char * hardware_part, int size);
int set_devinfo_hardware_part(char * hardware_part, int size);
int get_devinfo_hardware_part_rev(char * hardware_part_rev, int size);
int set_devinfo_hardware_part_rev(char * hardware_part_rev, int size);
int get_devinfo_mac_address(char * mac_address, int size);
int set_devinfo_mac_address(char * mac_address, int size);
int get_devinfo_fw_icon(char * fw_icon, int size);
int set_devinfo_fw_icon(char * fw_icon, int size);
int get_devinfo_rw_icon(char * rw_icon, int size);
int set_devinfo_rw_icon(char * rw_icon, int size);
int get_devinfo_osc_icon(char * osc_icon, int size);
int set_devinfo_osc_icon(char * osc_icon, int size);
int get_devinfo_modby(int * modby);
int set_devinfo_modby(int modby);
int get_devinfo_modtime(long int * modtime);
int set_devinfo_modtime(long int modtime);
int get_devinfo_status(int * status);
int set_devinfo_status(int status);
int get_devinfo_ablate_icon(char * ablate_icon, int size);
int set_devinfo_ablate_icon(char * ablate_icon, int size);
int get_devinfo_coag_icon(char * coag_icon, int size);
int set_devinfo_coag_icon(char * coag_icon, int size);
int get_devinfo_ambient_temp_icon(char * ambient_temp_icon, int size);
int set_devinfo_ambient_temp_icon(char * ambient_temp_icon, int size);

/* OSD configuration table APIs */
int get_osd_modby(int * modby);
int set_osd_modby(int  modby);
int get_osd_modtime(long int * modtime);
int set_osd_modtime(long int  modtime);
int get_osd_status(int * status);
int set_osd_status(int  status);
int get_osd_port_a_coordinates(int * x, int * y);
int get_osd_port_b_coordinates(int * x, int * y);
int set_osd_port_a_coordinates(int x, int y);
int set_osd_port_b_coordinates(int x, int y);
int get_osd_ww_icon_loc_coordinates(int * x, int * y);
int set_osd_ww_icon_loc_coordinates(int x, int y);

/* Service Configuration APIs */
int getField_connected_network(int * connected_network);
int getField_discovery_mgr(int * discovery_mgr);
int getField_remote_app_dsp(int * remote_app_dsp);
int getField_remote_control_app(int * remote_control_app);
int getField_surgeon_profile(int * surgeon_profile);
int getField_ext_trigger(int * ext_trigger);
int getField_auto_updates(int * auto_updates);
int get_servconf_modby(int * modby);
int set_servconf_modby(int modby);
int get_servconf_modtime(long int * modtime);
int set_servconf_modtime(long int modtime);
int get_servconf_status(int * status);
int set_servconf_status(int status);
int enable_disable_servconf_field(char  * fieldname, int value);
int setField_connected_network(int connected_network);
int setField_discovery_mgr(int discovery_mgr);
int setField_remote_app_dsp(int remote_app_dsp);
int setField_remote_control_app(int remote_control_app);
int setField_surgeon_profile(int surgeon_profile);
int setField_ext_trigger(int ext_trigger);
int setField_auto_updates(int auto_updates);

/* user table APIs */
int get_user_code(char * user_code, int size, int primary_key_value);
int set_user_code(char * user_code, int size, int primary_key_value);
int get_user_name(char *user_name, int size, int primary_key_value);
int set_user_name(char *user_name, int size, int primary_key_value);
int get_user_type(int * user_type, int primary_key_value);
int set_user_type(int user_type, int primary_key_value);
int get_user_password(char * user_password, int size, int primary_key_value);
int set_user_password(char * user_password, int size, int primary_key_value);
int get_user_status(int * status, int primary_key_value);
int set_user_status(int status, int primary_key_value);


#endif /* _CTB_DB_TABLE_API_H_*/
/* END OF FILE: ctb_db_table_api.h */

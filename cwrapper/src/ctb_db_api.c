/*###############################################################################
#   Filename: ctb_db_api.c
#
#   APIs to access elements of the User table in the sqlite3 DB
#
#   Copyright [2018] Smith-Nephew Inc.
#
#   Ver   Who      Date        Change
#  -----  ----     ----------  -----------------------------------------------
#  00.01  CG team  1/21/2019   Initial Creation.
#  00.02  CG team  2/18/2019   Added size check for parameters passed.
#  01.00  CG team  2/21/2019   Version shared with S&N Team
#  01.01  CG team  2/25/2019   Fixed defect in ctb_getField & ctb_setField
#                              - comparison used instead of assignment.
#  01.01  CG team  2/26/2019   Version shared with S&N Team on 26th Feb 2019.
#  01.02  CG team  3/14/2019   Fixed code review comments
#  01.03  CG team  3/25/2019   Added code for SEE (sqlite3 DB ecnryption) 
#  01.04  CG team  4/16/2019   Code changes to open db from /sn/webserver/db
#                              location.
#  01.05  CG team  5/6/2019    Updated error handling for Update operation and 
#                              for select operations to return consistent 
#                              return values.
#  01.06  CG team  6/4/2019    The user may pass value_size for Get API's 
#                              more than the size defined in the shcema for a
#                              field, API should return field value for size
#                              as defined in the DB schema.
##############################################################################*/

#include <stdio.h>
#include <string.h>
#include "ctb_db_api.h"
#include "ctb_data.h"

#ifdef SQLITE3_SEE
static int db_activation_status = 0;  /* SEE activation to be done once */
char key[] = "secret";
#endif
					 
/*------------------------------------------------------------------------------
#   API definitions
------------------------------------------------------------------------------*/

int ctb_getField(char * tablename, 
		 char * columnname, 
		 void * value, 
		 int    value_size, 
		 char * column_ref,
		 void * column_ref_value)
{
	int rc = 0;
	int op_rc = 0;
	int retval = FALSE;
	char query[MAX_QUERY_SIZE] = "";
	int type = INVALID_TYPE;
	int ref_type = INVALID_TYPE;
	sqlite3 * db;
	sqlite3_stmt *res;
	int clsize = 0;
	int clrefsize = 0;
	int data_count = 0;

	/* Variables to return values from query */
	int *intval = 0;
	/*char charval[PATH_FIELD_SIZE] = "";*/
	char * tmp_charval;
	long int *tval = 0;

	/* Verify the parameters passed */

	verify_params();

	/* Get the Column Type and size details */
	rc = get_column_details(tablename, columnname, &type, &clsize);
	if (rc < DB_OK)
		return rc;

	/* Verify column value size. Should be at least equal to the size
	 * defined in the DB schema */

	if(value_size < clsize)	
		return DB_INVALID_VALUE_SIZE;

	/* If column_ref is valid, get the Column Ref Type and size details */
	if(NULL != column_ref)
	{
		rc = get_column_details(tablename, column_ref, 
				&ref_type, &clrefsize);
		if (rc < DB_OK)
			return rc;

		/* Verify column Ref value size */
		if(CHAR == ref_type)
		{
			if((int)strlen((char *)column_ref_value) > clrefsize)
				return DB_INVALID_VALUE_SIZE;
		}
	}

	/* This is a generic query being created - can be applied 
	 * to get value of a column element of any of the CTB 
	 * table.
 	 */
	if((NULL != column_ref) && (INVALID_TYPE != ref_type))
	{
		rc = create_generic_select_query( query,
						  tablename,
						  columnname,
						  column_ref,
						  ref_type,
						  column_ref_value);
		if (rc < DB_QUERY_OK)
			return rc;
	}
	else
		sprintf(query, "SELECT %s FROM %s WHERE id = '%d' ;", 
			    columnname,
			    tablename,
			    DEFAULT_PRIMARY_KEY_VALUE);

#ifdef SQLITE3_SEE
        if(!db_activation_status)
        {
                (void) sqlite3_activate_see("7bb07b8d471d642e");
                db_activation_status = 1;
        }
#endif

	rc = sqlite3_open_v2(DB_FILE_NAME, &db,	
			SQLITE_OPEN_READWRITE, NULL);
	if(SQLITE_OK != rc)
	{
		(void) sqlite3_close(db);
		return DB_OPEN_FAILED;
	}

#ifdef SQLITE3_SEE
        rc = sqlite3_key(db, key, strlen(key));
        if(SQLITE_OK != rc)
        {
                (void) sqlite3_close(db);
                return DB_OPEN_FAILED;
        }
#endif
	
	rc = sqlite3_prepare_v2(db, query, -1, &res, 0);
	if (SQLITE_OK != rc)
	{
		(void) sqlite3_close(db);
		return DB_INVALID_OPERATION;
	}

	op_rc = sqlite3_step(res);

	/* Expect only one column from one row to be returned 
	 * from the above call to sqlite3_step(). Check this 
	 * condition
	 */

	if (((SQLITE_ROW == op_rc) || (SQLITE_DONE == op_rc)) && (NULL != res))
	{
	    data_count = sqlite3_data_count(res);
	    if(data_count != 0)
	    {
		switch (type)
		{
			case INT:
				intval = (int *) value;
				*intval = sqlite3_column_int(res, 0);
				retval = TRUE;
				break;

			case CHAR:
				tmp_charval = 
					(char *) sqlite3_column_text(res, 0);
				if(NULL != tmp_charval)
				{
					strncpy((char *) value, tmp_charval,
						        (size_t) clsize);
				}
				retval = TRUE;
				break;

			case DATETIME:
				tval = (long int *) value;
				*tval = sqlite3_column_int(res, 0);
				retval = TRUE;
				break;

			default:
				rc =  DB_INVALID_OPERATION;
				break;
		}
	    }
	    else
	    {
		if(FALSE == retval)
		{
			(void) sqlite3_finalize(res);
			(void) sqlite3_close(db);
			return DB_RETURNED_NO_VALUE;
		}
	    }
	}

	(void) sqlite3_finalize(res);
	(void) sqlite3_close(db);
	return rc;
}

int ctb_setField(char * tablename, 
		 char * columnname, 
		 void * value, 
		 int    value_size, 
		 char * column_ref,
		 void * column_ref_value)
{
	int rc = 0;
	char query[MAX_QUERY_SIZE] = "";
	int type = INVALID_TYPE;
	int ref_type = INVALID_TYPE;
	sqlite3 * db;
	sqlite3_stmt *res;
	int clsize = 0;
	int clrefsize = 0;

	/* Verify the parameters passed */

	verify_params();

	/* Get the Column Type and size details */
	rc = get_column_details(tablename, columnname, &type, &clsize);
	if (rc < DB_OK)
		return rc;

	/* Verify column value size */
	if(value_size > clsize)	
		return DB_INVALID_VALUE_SIZE;
	if(CHAR == type)
	{
		if((int)strlen((char *)value) > clsize)
			return DB_INVALID_VALUE_SIZE;
	}

	/* If column_ref is valid, get the Column Ref Type and size details */
	if(NULL != column_ref)
	{
		rc = get_column_details(tablename, column_ref, 
				&ref_type, &clrefsize);
		if (rc < DB_OK)
			return rc;

		/* Verify column Ref value size */
		if(CHAR == ref_type)
		{
			if((int)strlen((char *)column_ref_value) > clrefsize)
				return DB_INVALID_VALUE_SIZE;
		}
	}

	/* This is a generic query being created - can be applied 
	 * to set value of a column element of any of the CTB 
	 * tables.
 	 */
	rc = create_generic_update_query( query,
                			  tablename,
                			  columnname,
  			                  type,
			                  value,
					  value_size,
			                  column_ref,
			                  column_ref_value,
			                  ref_type);
	if (rc < DB_QUERY_OK)
		return rc;

#ifdef SQLITE3_SEE
        if(!db_activation_status)
        {
                (void) sqlite3_activate_see("7bb07b8d471d642e");
                db_activation_status = 1;
        }
#endif

	rc = sqlite3_open_v2(DB_FILE_NAME, &db,
			SQLITE_OPEN_READWRITE, NULL);
	if(SQLITE_OK != rc)
	{
		(void) sqlite3_close(db);
		return DB_OPEN_FAILED;
	}

#ifdef SQLITE3_SEE
        rc = sqlite3_key(db, key, strlen(key));
        if(SQLITE_OK != rc)
        {
                (void) sqlite3_close(db);
                return DB_OPEN_FAILED;
        }
#endif
	
	rc = sqlite3_prepare_v2(db, query, -1, &res, 0);
	if (SQLITE_OK != rc)
	{
		(void) sqlite3_close(db);
		return DB_INVALID_OPERATION;
	}

	rc = sqlite3_step(res);
	if(SQLITE_ERROR == rc)
	{
		rc = DB_SET_OPERATION_FAILED;
	}
	else if (SQLITE_DONE == rc) 
	{
		rc = DB_OPERATION_OK;
	}

	(void) sqlite3_finalize(res);
	(void) sqlite3_close(db);
	return rc;
}


/* END OF FILE: ctb_db_api.c */

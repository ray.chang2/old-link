# Q & A

## How to rebuild OS images (if I need to add anything)

1. Set up your build environment. Start with stock Ubuntu 16.04. Then look at
the Docker file under `contower/platform/docker`. It contains all the steps
you need to set up the building host. Actually that Dockerfile is used by
BitBucket Pipleline to continuously build each commit in the cloud.

2. In your building host console, cd `contower/platform` and do the following
commands:

        mkdir build-yocto
        ./gen_base_sd_image.sh build-yocto 1 5 # this goes through step 1 to 5

3. Step 6 has to be manually done currently. Following section 6.3 in the
attached document, with the following notes:

    - Adjust the building path accordingly. The document uses
    `~/projects/fsl-release-bsp/build-imx6ul-fb`, we are using
    `contower/platform/build-yocto`, where `contower` is the checked-out
    BitBucket source code of
    [contower](https://bitbucket.org/djiang2014/contower).

    - Before running any `bitbake` commands, you need to go to
    `contower/platform/build-yocto` and execute
    `source setup-environment build-yocto` first.

4. Then run step 7 to 10:

        ./gen_base_sd_image.sh build-yocto 7 10

5. Now we have the base SD image `core-image-base-imx6ulevk.sdcard.bz2` in
`contower/sd_images`. You can flash this image to SD card and try it on CTB.

## How to load new OS image onto target(s)

See the previous answer. Basically you flash the SD card with the OS image.
TFTP ram loading will be tried in the future.

## How to build an application (SDK interface)

See README.md in `contower`

## How to load an application onto target(s)

Use `scp`  or NFS mount. Currently I am using scp most of the time. You can
put your application under `contower/root/sn`, and run `make run_sync`.
It will sync the `contower/root` folder with four kits with names of `lens`,
`d2`, `d25` and `werewolf`.

## How to use Docker or Dockerfile under `platform/docker`

To run `CTB` docker Ubuntu image, first install docker if you haven’t already
done so
([Example instructions](https://medium.com/@Grigorkh/how-to-install-docker-on-ubuntu-16-04-3f509070d29c)).
 Then use either of the following two methods:

- Run CTB docker image by pulling it from DockerHub and run it:

        docker pull smithnephew/ctb
        docker run -it smithnephew/ctb bash

- Build CTB docker image

        cd platform    # do this under `contower` project directory
        make
        make run

Running docker image will give you a light-weight Virtual Machine like
building environment. I am mainly using it for BitBucket Pipeline. If you want
(and you should) do development on your native Ubuntu, open the Dockerfile and
look at all those `RUN` lines. Those are bash commands that set up the Ubuntu.
You can just run those commands one by one in your Ubuntu command line.

# How to test

To build an SD image:

    # (under build/platform)
    make base_sd

    cd ../..

    . setup-imx-environment

    ctb init

    ctb build imx

    # To Bump an SD image version: modify `version.txt` under project root.

    ctb install

    make CD=D2        // to make a D2 Image
        or
    make CD=WW        // to make a Werewolf Image

    # Suppose SD in in /dev/sdb:
    make bmap_flash_sd SDDEV=/dev/sdb

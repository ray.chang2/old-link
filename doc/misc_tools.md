# Software and Utilities that are installed on CTB

## Python Native Development

You can write Python 2.x or 3.x code natively on the CTB dev board. `pip` and `pip3` are preinstalled and can be used for installing additional packages.

## Node.js Native Development

You can write Node.js code natively on the CTB dev board. `npm` can be used for installing additional packages.

### Sync host pc with the kit

When the SSH keys have been properly set up (_TODO: how?_) , run the following command under the root directory will sync host with all four remote devices:

    ctb sync

### Monitoring the device presence with Avahi

    ctb avahi

### Netdata (TODO)

### Slack Integration (TODO)

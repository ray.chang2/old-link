## Style Guidelines

For now, we recommend using [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html) for new C code in this repository. General rules:

- Indents are two spaces. No tabs should be used anywhere.
- Each line must be at most 80 characters long.
- Comments can be `//` or `/*` but `//` is most commonly used.
- File names should be `lower_case.c` or `lower-case.c`

Here's [an sample code](https://gist.github.com/davidzchen/9187878) that's following this style.

[cpplint](https://github.com/google/styleguide/tree/gh-pages/cpplint) is a Google tool to check your code against `Google C++ Style Guide`. It takes a list of files as input. For full usage instructions,
please see the output of:

    ./cpplint.py --help

### Additional Guide Lines

A standard commit message format enhances the "glancibility" of quick understanding what has been changed. We recommend to follow the commit message convention used by Yocto project. Here is an example from Yocto's `Poky` project:

    * 094c167-(2018-03-21 15:49:05) mesa: Upgrade 17.3.6 -> 17.3.7
    * 80c7ca2-(2018-03-22 09:15:49) kernel-dev: Clean up of "bsp_name" placeholder.
    * ce8c964-(2018-03-21 09:04:24) bsp-guide: Updated BSP terminolgy and BBLAYERS ordering
    * dcde1a9-(2018-03-21 08:27:20) ref-manual: Added term "container layer"
    * b3732ab-(2018-03-21 07:30:14) dev-manual: Cleaned up layer naming terminology
    * fcbe721-(2018-03-20 16:15:16) documentation: Updated the section on creating a general script

As indicated above, the commit messages should follow the following format:

    <module>: <do> thing

The format means this commit will "do" this "thing" for "module". For example in our project we might have:

`autobuild: fix a bug in ct_build.sh`



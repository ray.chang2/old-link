# Git Usage

## Git Development Workflows

The `master` branch is intended to be deployable all the time. And we want to reduce merge effor by minimizing long-living feature branches. So we suggest we integrate our feature branches as frequently as possible. For larger features, consider use a [Feature Toggle](https://martinfowler.com/articles/feature-toggles.html) to merge your branch constantly while it's still in development.

First, let's check out the code:

    $ git clone git@bitbucket.org:djiang2014/contower.git
    ( Now you have a `contower` directory)

Then, make changes under this directory, You may use your preferred branching strategies, or consider the following ones:

### Direct Commit without Review

For minor changes that you think you don't need a review. You don't need to create a branch.

    (if you are not aleady in master branch)
    $ git checkout master
    (# update from upstream)
    $ git pull
    (make your changes...)
    $ git commit -m "module: my new improvement"
    $ git push

You are done. Your code is committed to `master` directly. The BitBucket build pipeline will check and build your code. The whole team will receive email notifications if your new commit has caused a failed build or broken the tests.

### Using a Feature Branch for Review

To avoid the broken commits and to improve code quality, you can have your code peer-reviewed before commiting to `master` branch, which also adds the benefits of running automatic build in your branch instead of `master`. You can to create a feature branch like this:

    (if you are not already in master branch:)
    $ git checkout master
    (update from upstream:)
    $ git pull
    $ git checkout -b my_awesome_feature
    (make your changes...)
    $ git commit -m "my_awesome_feature: change 1"
    (make further changes...)
    $ git commit -m "my_awesome_feature: change 2"
    (push your branch to BitBucket)
    $ git push origin my_awesome_feature

Now in BitBucket, create a Pull Request. Note this Pull Request doesn't have to mean you've finished the feature. You are encourged to merge (send Pull Request) daily if not more frequently to avoid future merge difficulties. If you haven't finished the feature, You can set the Pull Request to not merge to the master after the review is finished .

Note it's useful to create a Pull Request even if you don't think there's a need for review. Pull Request allows easy side-by-side view of your changes, and also as mentioned can put your new commit through build and tests. And if you find something wrong, you can `git commit` more changs, which will update the same pull request automatically. When closing the Pull Request and merging the branch, all commits in the pull request will be squashed to a single commit. This makes the commit history cleaner.

### Clean up after the Merging the Pull Request

If the Pull Reqeust has been set up such that the feature branch will _not_ be deleted after merging, you can keep working on your feature branch.

If the Pull Reqeust has been set up such that the feature branch will be deleted after merging. Do this to clean up your local feature branch:

    $ git checkout master
    (fetch remote branches and delete non-existent remote branches)
    $ git fetch -p
    $ git pull
    (You can stop here. Or delete your obsolete feature branch if you are sure everthing has been merged)
    $ git branch -D my_awesome_feature

## (Optional) How to get automatic notifications

Currently we are using [Slack](http://slack.com "Slack") for quick automatic notifications for source code commits and build reports. The following steps are needed to achieve so:

1. Penghe Geng will email-invite you to `Endoscopy R&D` Slack channel.
2. You follow the instructions in the received email to install Slack.
3. You enter `contower` channel to receive the communications.

Notifications you will receive:

- Whenever there's a Bitbucket code commit
- Whenever there's a Jira issue created, assigned or updated otherwise
- Bitbucket Pipeline CI notifications. When there's a build failure, there will also be an email sent to the team.

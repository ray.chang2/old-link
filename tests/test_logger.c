#include "logger.h"

int main(void)
{
    logger.enable_exit = false;
    logger.debug("test logger.debug\n");
    logger.info("test logger info\n");
    logger.highlight("test logger highlight\n");
    logger.warning("test logger.warning\n");
    logger.error("test logger.error\n");
    logger.critical("test logger.critical\n");
    logger.fatal("test logger.fatal\n");
    logger.error_internal("test logger.error_internal\n");
    return 0;
}

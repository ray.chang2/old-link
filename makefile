###############################################################################
#
#   Makefile for building CTB SD image
#
#   (Named "makefile" instead of Makefile to avoid CMake overwriting)
#
#   Copyright [2018] Smith-Nephew Inc.
#
###############################################################################
include version.txt
export

SHELL := /bin/bash

export ROOT_DIR := $(shell pwd)
SCRIPT_DIR := $(ROOT_DIR)/scripts
PLATFORM_DIR := $(ROOT_DIR)/build/platform
SDIMG_DIR := $(ROOT_DIR)/build/sd_images
SDFS_DIR := $(ROOT_DIR)/build/sd_fs
BOOTFS_DIR := $(SDFS_DIR)/bootfs
ROOTFS_DIR := $(SDFS_DIR)/rootfs

etcher_dir := etcher_pkg
CTB_ETCHER_PKG_DIR := $(SDIMG_DIR)/$(etcher_dir)

BASE_SD := $(SDIMG_DIR)/core-image-base-imx6ulevk.sdcard
BASE_SD_BZ2 := $(BASE_SD).bz2

device_name := $(shell cat $(ROOTFS_DIR)/etc/hostname)
version_string := $(major)_$(minor)_$(build)
deploy_name := $(device_name)_ctb_sd_$(version_string)_$(shell git rev-parse --short HEAD)_$(shell date -I)
CTB_SD := $(SDIMG_DIR)/$(deploy_name)_$(CD).img
CTB_WW_SD := $(SDIMG_DIR)/$(deploy_name).img
CTB_ETCHER_ZIP := $(SDIMG_DIR)/$(deploy_name)_$(CD).zip

TMP_SD := $(SDIMG_DIR)/tmp.img

.PHONY: all clean help
.PHONY: build_yocto
.PHONY: install-git-hooks
.PHONY: build_deploy_sd base_sd

all: build_deploy_sd

help:
	@echo -e "Common Usage:"
	@echo -e "  make"
	@echo -e "  make all  # same as make"
	@echo -e "  make build_deploy_sd  # same as make all"
	@echo -e "  make clean  # delete deloy images only"
	@echo -e "  make build_yocto"
	@echo -e "  make install-git-hooks"

clean:
	@-for d in mnt_boot mnt_root $(SDDEV)1 $(SDDEV)2; do \
		if mount|grep $$d >/dev/null; then \
			sudo umount -v $$d 2>/dev/null; \
		fi; \
		[[ -d $$d ]] && sudo rmdir $$d || :; \
	done; \
	sudo losetup -v -D;
	cd $(SDIMG_DIR) && \
	rm -vf *.img *.img.bz2 *.bmap *.zip

image_clean:
	cd $(SDIMG_DIR) && git clean -dfx .

build_yocto:
	cd $(PLATFORM_DIR) && make

install-git-hooks:
	for f in scripts/git-hooks/*; do \
		cp $$f .git/hooks/; \
	done

.PHONY: bmap_flash_sd_clean
bmap_flash_sd_clean: clean build_deploy_sd bmap_flash_sd

.PHONY: bmap_flash_sd
bmap_flash_sd:
	@if [[ -z "$(SDDEV)" ]] ; then echo "Error: SDDEV must be defined"; exit 1; else \
		if ! [[ -b "$(SDDEV)" ]] ; then echo "$(SDDEV) is not a block device!"; exit 1; else \
			echo "Flashing $(SDDEV) with $<"; \
			sudo bmaptool copy $(CTB_SD) $(SDDEV); \
			echo "Done"; \
		fi; \
	fi

.PHONY: build_deploy_sd
build_deploy_sd:
	echo "Building Deploy SD card image"
	@ if ! [[ -e build/sd_fs/rootfs/sn/ctb_late_init.sh ]]; then \
		echo "Error: Important files not found in <root>/sn directory"; \
		echo "Did you forget to run 'ctb install'?"; \
		exit 1; \
		fi
	$(MAKE) $(CTB_SD).bz2
	$(MAKE) $(CTB_ETCHER_ZIP)

$(CTB_SD).bz2: $(CTB_SD)
	bzip2 -c $< >$@
	@echo "$@ created"

$(CTB_ETCHER_ZIP): $(CTB_SD) $(CTB_SD).bmap
	cd $(CTB_ETCHER_PKG_DIR); \
	rm -f *.img || :; \
	ln $(CTB_SD) && \
	cp $(CTB_SD).bmap .meta/image.bmap && \
	cp $(SDIMG_DIR)/manifest.json.template .meta/manifest.json && \
	source $(ROOT_DIR)/version.txt && \
	sed -i -e "s/NAME/$(deploy_name)/" \
		-e "s/VERSION/$$major.$$minor.$$build/" .meta/manifest.json && \
	cd $(SDIMG_DIR) && \
	zip -r $@ $(etcher_dir)

.PHONY: quick_deply_sd
quick_deply_sd: $(CTB_SD).bmap

$(CTB_SD).bmap: $(CTB_SD)
	bmaptool create $< > $@

$(CTB_SD): $(BASE_SD)
	@echo "Generating CTB deploy image by overlaying rootfs with base image..."
	sudo $(SCRIPT_DIR)/gen_ctb_sd_image.sh $< $(SDFS_DIR) $(TMP_SD)
	@echo "Doing sparse copy to reduce future programming time"
	cp --sparse=always $(TMP_SD) $@
	@echo "Fixing UBOOT and Creating a $(CD) friendly filesystem"
	$(SCRIPT_DIR)/mkimg -i $@ -c $(CD)
	@echo "$@ created"

$(BASE_SD): $(BASE_SD_BZ2)
	bunzip2 -ck $< > $@

$(BASE_SD_BZ2):
	@if ! [[ -f $@ ]]; then $(MAKE) base_sd; fi

.PHONY: base_sd
base_sd:
	make -C $(PLATFORM_DIR) install_yocto_sd_bz2 || :
	@[[ -e $(BASE_SD_BZ2) ]] || { echo "make $(BASE_SD_BZ2) failed"; exit 1; }
